<?php

require_once("$CFG->libdir/formslib.php");
 
class contactus_form extends moodleform {
 
    function definition() {
        global $CFG;
        $mform = $this->_form; // Don't forget the underscore! 
        //$mform->addElement('header', 'contactus', get_string('contactus', 'local_contactus'));
        
        $mform->addElement('html','<h1 class="headtext1">CONTACT US</h1><br><br>');
        
        $mform->addElement('html','<h3 class="headtext2">GET IN TOUCH</h3><br>');
        
        $mform->addElement('html','<p class="headtext1">Learn more about our e-learning platform with live classroom for Schools.<br/>Send us your inquiry now and we\'ll get in touch with you shortly</p><br>');
        
        $mform->addElement('html','<div class="row col-md-12">');
        $mform->addElement('html','<div class="col-md-6">');
        $mform->addElement('html','<img src='.$CFG->wwwroot.'/local/contactus/pix/contact.jpg'.'>');
        $mform->addElement('html','</div>');
        $mform->addElement('html','<div class="col-md-6">');
        $mform->addElement('text', 'name', '', array('placeholder' => 'Enter Your Name'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required');
        
        $mform->addElement('text', 'email', '', array('placeholder' => 'Enter Your Email'));
        $mform->setType('email', PARAM_EMAIL);
        $mform->addRule('email', null, 'required');
        
        $mform->addElement('text', 'mobno', '', array('placeholder' => 'Enter Your Phone Number'));
        $mform->setType('mobno', PARAM_ALPHANUM);
        $mform->addRule('mobno', null, 'required');
        
//        $mform->addElement('text', 'subject', '', array('placeholder' => ''));
//        $mform->setType('subject', PARAM_TEXT);
//        $mform->addRule('subject', null, 'required');
        
        $mform->addElement('textarea', 'message', '','wrap="virtual" rows="5" cols="50" placeholder="Your Inquiry"');
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required');
        $this->add_action_buttons($cancel = true, $submitlabel='Submit'); 
        
        $mform->addElement('html','</div>');
        $mform->addElement('html','</div>');
        $mform->addElement('html','<p class="headtext1"> Or send us an e-mail to info@allinschool.com</p><br>');

        
        
            
    }
    
    function validation($data, $files) {
        global $DB;
        
        $errors = array();
        
        if (empty(trim($data['name']))){
            $errors['name'] ="Please enter name";
        }
        
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] ="Please enter a valid email";
        }
        
        if (strlen($data['mobno']) > 10){
            $errors['mobno'] = "Please enter 10 digit mobile number";
        } else if (!filter_var($data['mobno'], FILTER_SANITIZE_NUMBER_INT)){
            $errors['mobno'] = "Please enter integer mobile number";
        }
        
//        if (empty(trim($data['subject']))){
//            $errors['subject'] ="Please enter subject";
//        }
        
        if (empty(trim($data['message']))){
            $errors['message'] ="Please enter message";
        }
        return $errors;
    }
} 

