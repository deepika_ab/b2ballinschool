<?php
require_once('../../config.php');
require_once($CFG->dirroot.'/local/contactus/form/index_form.php');
$context = context_system::instance();
global $CFG, $DB;
//require_login();
$PAGE->set_context($context);
$PAGE->set_url('/local/contactus/index.php');
//$PAGE->set_heading('Contact Us');
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Contact Us');
$PAGE->requires->jquery();
$PAGE->requires->css(new moodle_url('/local/contactus/css/style.css'));

echo $OUTPUT->header();

$contactusobj = new contactus_form();

if ($formdata = $contactusobj->get_data()) { 
        //send mail to admin/admins
        //$supportuser = core_user::get_support_user();
        //$supportuser->maildisplay = true;
        //$to = 'info@allinschool.com';
        $to = 'nihar.cbsh@gmail.com';
        //$from = "From: ".$formdata->name."<".$formdata->email.">";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <info@allinschool.com>' . "\r\n";

        $subject = 'Course Enquiry';
        $body = '';
        $body .= 'Dear Admin<br/><br/>';
        $body .= 'Please find the below enquiry details<br/><br/>';
        $body .= '<body>
        <table style="font-family:Calibri; font-size:16px;" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
                <th colspan="2" align="center" bgcolor="#a21d22" style="padding:10px; color:#ffffff;" nowrap>Enquiry</th>
            </tr>
            <tr> 
              <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black; padding:10px; color:#583333;" nowrap>Name</td>
              <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->name . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Email</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->email . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Mobile No.</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->mobno . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Subject</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->subject . '</td>
            </tr>
            
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Message</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->message . '</td>
            </tr>';
        $body .= '</tbody>
        </table></body>';
        //$body .= '<p style="font-family: Calibri;"><br/>Regards,<br/>All IN School Team</p>';
        mail($to,$subject,$body,$headers);
        //email_to_user($userobj, $supportuser, $subject, '', $body);
        redirect(new moodle_url('/course/index.php'),'Thank you for contacting us. We will get back to you.', 10);
    //}
    
}

$contactusobj->display();

echo $OUTPUT->footer();
