<?php

class __Mustache_b6c1247241edfedb9e182c179ecab5df extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<ul class="nav nav-tabs" role="tablist">
';
        // 'tabs' section
        $value = $context->find('tabs');
        $buffer .= $this->section8b847c09d45a3a1c2e9f301741f14ef7($context, $indent, $value);
        $buffer .= $indent . '</ul>';

        return $buffer;
    }

    private function section87689ec1ccddfae452be09d6dbfe92a1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'show active';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'show active';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section03a2cb78adf693fb240638cbbc7ea15e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'true';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'true';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8b847c09d45a3a1c2e9f301741f14ef7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        <li class="nav-item">
            <a 
                id="{{ category }}-tab"
                data-toggle="tab"
                role="tab"
                class="nav-link {{# selected }}show active{{/ selected }}"
                href="#{{ category }}"
                aria-controls="{{ category }}"
                aria-selected="{{# selected }}true{{/ selected }}{{^ selected }}false{{/selected}}"
                ><i class="fa {{ icon }}"></i> {{ label }}</a>
        </li>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '        <li class="nav-item">
';
                $buffer .= $indent . '            <a 
';
                $buffer .= $indent . '                id="';
                $value = $this->resolveValue($context->find('category'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '-tab"
';
                $buffer .= $indent . '                data-toggle="tab"
';
                $buffer .= $indent . '                role="tab"
';
                $buffer .= $indent . '                class="nav-link ';
                // 'selected' section
                $value = $context->find('selected');
                $buffer .= $this->section87689ec1ccddfae452be09d6dbfe92a1($context, $indent, $value);
                $buffer .= '"
';
                $buffer .= $indent . '                href="#';
                $value = $this->resolveValue($context->find('category'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"
';
                $buffer .= $indent . '                aria-controls="';
                $value = $this->resolveValue($context->find('category'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"
';
                $buffer .= $indent . '                aria-selected="';
                // 'selected' section
                $value = $context->find('selected');
                $buffer .= $this->section03a2cb78adf693fb240638cbbc7ea15e($context, $indent, $value);
                // 'selected' inverted section
                $value = $context->find('selected');
                if (empty($value)) {
                    
                    $buffer .= 'false';
                }
                $buffer .= '"
';
                $buffer .= $indent . '                ><i class="fa ';
                $value = $this->resolveValue($context->find('icon'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"></i> ';
                $value = $this->resolveValue($context->find('label'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</a>
';
                $buffer .= $indent . '        </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
