<?php

class __Mustache_30cbc53a5fb3d3ec008fb336331af22b extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<li>
';
        $buffer .= $indent . '    <a class="card-link" href="';
        $value = $this->resolveValue($context->find('link'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '">';
        $value = $this->resolveValue($context->find('fullname'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</a> (';
        $value = $this->resolveValue($context->find('email'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= ')
';
        $buffer .= $indent . '</li>
';

        return $buffer;
    }
}
