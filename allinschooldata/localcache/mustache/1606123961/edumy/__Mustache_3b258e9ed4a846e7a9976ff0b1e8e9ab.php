<?php

class __Mustache_3b258e9ed4a846e7a9976ff0b1e8e9ab extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'description' section
        $value = $context->find('description');
        $buffer .= $this->sectionAc77a07619c47caf15626eb44993ebaf($context, $indent, $value);
        $buffer .= $indent . '<ul class="mb0">
';
        $buffer .= $indent . '  <li>
';
        $buffer .= $indent . '    <span class="flaticon-appointment"></span> ';
        $value = $this->resolveValue($context->find('formattedtime'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '  </li>
';
        // 'normalisedeventtypetext' section
        $value = $context->find('normalisedeventtypetext');
        $buffer .= $this->section210ba9135c8f2172c24495516eead436($context, $indent, $value);
        // 'location' section
        $value = $context->find('location');
        $buffer .= $this->sectionB5db4a11b93fc69e2ac12d95a95cdf6c($context, $indent, $value);
        // 'isactionevent' section
        $value = $context->find('isactionevent');
        $buffer .= $this->section7cd9fc036f9bee3001027eb5fe043ebd($context, $indent, $value);
        // 'iscategoryevent' section
        $value = $context->find('iscategoryevent');
        $buffer .= $this->section1866d0180f41d3f7826eefadcc73b3ce($context, $indent, $value);
        // 'iscourseevent' section
        $value = $context->find('iscourseevent');
        $buffer .= $this->section2ef5dabcd0d518f3715e9f80322ff03c($context, $indent, $value);
        // 'groupname' section
        $value = $context->find('groupname');
        $buffer .= $this->section373b0a847f5caea43a4554d3d7d05221($context, $indent, $value);
        // 'subscription' section
        $value = $context->find('subscription');
        $buffer .= $this->section1c76c129028cab49425ccc5d756518ec($context, $indent, $value);
        $buffer .= $indent . '</ul>
';

        return $buffer;
    }

    private function sectionAc77a07619c47caf15626eb44993ebaf(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
  <p>{{{.}}}</p>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '  <p>';
                $value = $this->resolveValue($context->last(), $context);
                $buffer .= $value;
                $buffer .= '</p>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section210ba9135c8f2172c24495516eead436(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-clock"></span> {{normalisedeventtypetext}}
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> ';
                $value = $this->resolveValue($context->find('normalisedeventtypetext'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB5db4a11b93fc69e2ac12d95a95cdf6c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-placeholder"></span> {{{.}}}
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-placeholder"></span> ';
                $value = $this->resolveValue($context->last(), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7cd9fc036f9bee3001027eb5fe043ebd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-clock"></span> <a href="{{course.viewurl}}">{{{course.fullname}}}</a>
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> <a href="';
                $value = $this->resolveValue($context->findDot('course.viewurl'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->findDot('course.fullname'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1866d0180f41d3f7826eefadcc73b3ce(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-clock"></span> {{{category.nestedname}}}
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> ';
                $value = $this->resolveValue($context->findDot('category.nestedname'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2ef5dabcd0d518f3715e9f80322ff03c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-clock"></span> <a href="{{url}}">{{{course.fullname}}}</a>
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> <a href="';
                $value = $this->resolveValue($context->find('url'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->findDot('course.fullname'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section373b0a847f5caea43a4554d3d7d05221(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <li>
      <span class="flaticon-clock"></span> <a href="{{url}}">{{{course.fullname}}}</a>
    </li>
    <li>
      <span class="flaticon-clock"></span> {{{groupname}}}
    </li>
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> <a href="';
                $value = $this->resolveValue($context->find('url'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->findDot('course.fullname'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '    </li>
';
                $buffer .= $indent . '    <li>
';
                $buffer .= $indent . '      <span class="flaticon-clock"></span> ';
                $value = $this->resolveValue($context->find('groupname'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section87b7d971d18ed241a537f3f78c441cb1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'subscriptionsource, core_calendar, {{{subscriptionname}}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'subscriptionsource, core_calendar, ';
                $value = $this->resolveValue($context->find('subscriptionname'), $context);
                $buffer .= $value;
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE542351e70dbbd77506914606b62c33e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
          <a href="{{subscriptionurl}}">{{#str}}subscriptionsource, core_calendar, {{{subscriptionname}}}{{/str}}</a>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '          <a href="';
                $value = $this->resolveValue($context->find('subscriptionurl'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section87b7d971d18ed241a537f3f78c441cb1($context, $indent, $value);
                $buffer .= '</a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEfc800de760eb83c8cef0cd3851389ab(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
      <li>
        <span class="flaticon-clock"></span>
        {{#subscriptionurl}}
          <a href="{{subscriptionurl}}">{{#str}}subscriptionsource, core_calendar, {{{subscriptionname}}}{{/str}}</a>
        {{/subscriptionurl}}
        {{^subscriptionurl}}
          {{#str}}subscriptionsource, core_calendar, {{{subscriptionname}}}{{/str}}
        {{/subscriptionurl}}
      </li>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '      <li>
';
                $buffer .= $indent . '        <span class="flaticon-clock"></span>
';
                // 'subscriptionurl' section
                $value = $context->find('subscriptionurl');
                $buffer .= $this->sectionE542351e70dbbd77506914606b62c33e($context, $indent, $value);
                // 'subscriptionurl' inverted section
                $value = $context->find('subscriptionurl');
                if (empty($value)) {
                    
                    $buffer .= $indent . '          ';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section87b7d971d18ed241a537f3f78c441cb1($context, $indent, $value);
                    $buffer .= '
';
                }
                $buffer .= $indent . '      </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1c76c129028cab49425ccc5d756518ec(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    {{#displayeventsource}}
      <li>
        <span class="flaticon-clock"></span>
        {{#subscriptionurl}}
          <a href="{{subscriptionurl}}">{{#str}}subscriptionsource, core_calendar, {{{subscriptionname}}}{{/str}}</a>
        {{/subscriptionurl}}
        {{^subscriptionurl}}
          {{#str}}subscriptionsource, core_calendar, {{{subscriptionname}}}{{/str}}
        {{/subscriptionurl}}
      </li>
    {{/displayeventsource}}
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'displayeventsource' section
                $value = $context->find('displayeventsource');
                $buffer .= $this->sectionEfc800de760eb83c8cef0cd3851389ab($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
