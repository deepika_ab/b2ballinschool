<?php

class __Mustache_979719b55c75f55b7b56d3ed61a5e23d extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<h2>';
        $value = $this->resolveValue($context->findDot('path.name'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</h2>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="row">
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <a href="editgroup.php?learningpath=';
        $value = $this->resolveValue($context->findDot('path.id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '">
';
        $buffer .= $indent . '            <i class="fa fa-plus fa-2x text-muted"></i>
';
        $buffer .= $indent . '            <span class="h5 text-muted">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionFbdea7194afce6c80273fb464df82c4b($context, $indent, $value);
        $buffer .= '</span>
';
        $buffer .= $indent . '        </a>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <form class="form-inline">
';
        $buffer .= $indent . '            <div class="form-group">
';
        $buffer .= $indent . '                <label for="category">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section68f27f67b0897a49c939b0c339b3b3bc($context, $indent, $value);
        $buffer .= '</label>
';
        $buffer .= $indent . '                <select id="program" class="form-control">
';
        // 'programlicenses' section
        $value = $context->find('programlicenses');
        $buffer .= $this->sectionDb9c058ee1818506e325daa8d5d3c8da($context, $indent, $value);
        $buffer .= $indent . '                </select>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="form-group">
';
        $buffer .= $indent . '                <label for="coursefilter">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionF64223af4e9d19a9eff9f4ef8561370d($context, $indent, $value);
        $buffer .= '</label>
';
        $buffer .= $indent . '                <input id="coursefilter" class="form-control" type="text" name="coursefilter">
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="form-group">
';
        $buffer .= $indent . '                <label for="category">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section4d8db80e018d8573f5576776723f4a5a($context, $indent, $value);
        $buffer .= '</label>
';
        $buffer .= $indent . '                <select id="category" class="form-control">
';
        // 'categories' section
        $value = $context->find('categories');
        $buffer .= $this->section408b55af643527f15bc846783a6ec07d($context, $indent, $value);
        $buffer .= $indent . '                </select>
';
        $buffer .= $indent . '           </div>
';
        $buffer .= $indent . '        </form>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="row m-t-1 m-b-1">
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <div class="card bg-light">
';
        $buffer .= $indent . '            <div class="card-header">
';
        $buffer .= $indent . '                ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section9885c29d4044064df2c0088620120c55($context, $indent, $value);
        $buffer .= '<br />
';
        $buffer .= $indent . '                <small>
';
        $buffer .= $indent . '                    ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section2fcb4348c986cbb9f4584ba68dd764ec($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                </small>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="card-body card-block">
';
        $buffer .= $indent . '                <p class="card-text">
';
        // 'groups' section
        $value = $context->find('groups');
        $buffer .= $this->sectionFf9f8d1f3d633238c557f47546d35486($context, $indent, $value);
        $buffer .= $indent . '                </p>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <div class="card bg-light">
';
        $buffer .= $indent . '            <div class="card-header">
';
        $buffer .= $indent . '                ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section206e22a0c503c6d4c62e20ee188c239f($context, $indent, $value);
        $buffer .= '<br />
';
        $buffer .= $indent . '                <small>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section16c8c38b374a4f9bbafb54424912664b($context, $indent, $value);
        $buffer .= '</small>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="card-body card-block">
';
        $buffer .= $indent . '                <p class="card-text">
';
        $buffer .= $indent . '                    <div id="noprospective" class="alert alert-warning" style="display: none">
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionF8d8295ad22fbddf1b33016edb5b4792($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                    </div>
';
        $buffer .= $indent . '                    <ul id="prospectivelist">
';
        $buffer .= $indent . '                    </ul>
';
        $buffer .= $indent . '                </p>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';
        $buffer .= $indent . '
';
        $value = $this->resolveValue($context->find('done'), $context);
        $buffer .= $indent . $value;
        $buffer .= '
';

        return $buffer;
    }

    private function sectionFbdea7194afce6c80273fb464df82c4b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'addgroup, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'addgroup, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section68f27f67b0897a49c939b0c339b3b3bc(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'licenseprogram, block_iomad_company_admin ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'licenseprogram, block_iomad_company_admin ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDb9c058ee1818506e325daa8d5d3c8da(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <option value="{{ id }}" {{ selected }}>{{ name }}</option>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <option value="';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" ';
                $value = $this->resolveValue($context->find('selected'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '>';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</option>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF64223af4e9d19a9eff9f4ef8561370d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'filter, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'filter, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4d8db80e018d8573f5576776723f4a5a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'category ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'category ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section408b55af643527f15bc846783a6ec07d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <option value="{{ id }}">{{ name }}</option>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <option value="';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</option>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9885c29d4044064df2c0088620120c55(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'coursesinpath, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'coursesinpath, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2fcb4348c986cbb9f4584ba68dd764ec(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'clicktoremove, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'clicktoremove, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFf9f8d1f3d633238c557f47546d35486(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        {{> local_iomad_learningpath/groupcard }}
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                if ($partial = $this->mustache->loadPartial('local_iomad_learningpath/groupcard')) {
                    $buffer .= $partial->renderInternal($context, $indent . '                        ');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section206e22a0c503c6d4c62e20ee188c239f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'prospectivecourses, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'prospectivecourses, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section16c8c38b374a4f9bbafb54424912664b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'clicktoadd, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'clicktoadd, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF8d8295ad22fbddf1b33016edb5b4792(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'noprospectivecourses, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'noprospectivecourses, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
