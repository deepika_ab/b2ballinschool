<?php

class __Mustache_bf2131e29c9bbaa98d626d8506c6ae55 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="groupcard card">
';
        $buffer .= $indent . '    <div class="card-block">
';
        $buffer .= $indent . '        <div class="row">
';
        $buffer .= $indent . '            <div class="col-md-10">
';
        $buffer .= $indent . '                <h4 class="card-title">';
        $value = $this->resolveValue($context->find('name'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</h4>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="col-md-2 text-right">
';
        $buffer .= $indent . '                <a href="editgroup.php?learningpath=';
        $value = $this->resolveValue($context->findDot('path.id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '&id=';
        $value = $this->resolveValue($context->find('id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" title="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section004c38b366ed0a8370eee7cbc64f1ff2($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '                    <i class="fa fa-cog fa-2x text-muted"></i>
';
        $buffer .= $indent . '                </a>
';
        $buffer .= $indent . '                <a href="editgroup.php?learningpath=';
        $value = $this->resolveValue($context->findDot('path.id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '&delete=';
        $value = $this->resolveValue($context->find('id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" title="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section1244de9b0121709dfe9a612097596e20($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '                    <i class="fa fa-trash fa-2x text-muted"></i>
';
        $buffer .= $indent . '                </a>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        <div class="row">
';
        $buffer .= $indent . '            <div class="col-md-12">
';
        $buffer .= $indent . '                <div class="nogroupcourses alert alert-warning" data-groupid="';
        $value = $this->resolveValue($context->find('id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" style="display: none">
';
        $buffer .= $indent . '                    ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionE733668ad77474120ec8f56d495a04bd($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        <ul class="pathcourselist" data-groupid="';
        $value = $this->resolveValue($context->find('id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '"> </ul>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function section004c38b366ed0a8370eee7cbc64f1ff2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'editgroup, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'editgroup, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1244de9b0121709dfe9a612097596e20(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'deletegroup, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'deletegroup, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE733668ad77474120ec8f56d495a04bd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nocoursesingroup, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nocoursesingroup, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
