<?php

class __Mustache_73d9be38b9deb9c20375734c2891ab01 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section97733653bf1c952864da0273b8ee9c2c($context, $indent, $value);

        return $buffer;
    }

    private function sectionA7dd6239edc50d320fdeb8e02a4e8689(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    ga(\'set\', \'anonymizeIp\', true);';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '
';
                $buffer .= $indent . '    ga(\'set\', \'anonymizeIp\', true);';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section97733653bf1c952864da0273b8ee9c2c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
    ga(\'create\', \'{{analyticsid}}\', {\'siteSpeedSampleRate\': 50});{{#anonymizeip}}
    ga(\'set\', \'anonymizeIp\', true);{{/anonymizeip}}
    ga(\'send\', {{{addition}}});
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
';
                $buffer .= $indent . '    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
';
                $buffer .= $indent . '    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
';
                $buffer .= $indent . '    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
';
                $buffer .= $indent . '    ga(\'create\', \'';
                $value = $this->resolveValue($context->find('analyticsid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '\', {\'siteSpeedSampleRate\': 50});';
                // 'anonymizeip' section
                $value = $context->find('anonymizeip');
                $buffer .= $this->sectionA7dd6239edc50d320fdeb8e02a4e8689($context, $indent, $value);
                $buffer .= '
';
                $buffer .= $indent . '    ga(\'send\', ';
                $value = $this->resolveValue($context->find('addition'), $context);
                $buffer .= $value;
                $buffer .= ');
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
