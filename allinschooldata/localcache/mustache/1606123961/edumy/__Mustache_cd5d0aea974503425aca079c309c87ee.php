<?php

class __Mustache_cd5d0aea974503425aca079c309c87ee extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<h3>';
        $value = $this->resolveValue($context->findDot('group.name'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</h3>
';
        $buffer .= $indent . '
';
        $value = $this->resolveValue($context->find('form'), $context);
        $buffer .= $indent . $value;
        $buffer .= '
';
        $buffer .= $indent . '
';

        return $buffer;
    }
}
