<?php

class __Mustache_3fc76686a1136c5f42295b7825237627 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        // 'ispaths' inverted section
        $value = $context->find('ispaths');
        if (empty($value)) {
            
            $buffer .= $indent . '    <div class="alert alert-warning">';
            // 'str' section
            $value = $context->find('str');
            $buffer .= $this->sectionDf7c5298fbfeae8d8ec007890bcf3ec1($context, $indent, $value);
            $buffer .= '</div>
';
        }
        // 'ispaths' section
        $value = $context->find('ispaths');
        $buffer .= $this->section23d0e5c6593adf64f7c782e9d151b20f($context, $indent, $value);
        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="row top-space">
';
        $buffer .= $indent . '    <a href="';
        $value = $this->resolveValue($context->find('linknew'), $context);
        $buffer .= $value;
        $buffer .= '" class="btn btn-success"><i class="fa fa-plus"></i> ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionFc501e95566fe6294073eac456cbef24($context, $indent, $value);
        $buffer .= '</a>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function sectionDf7c5298fbfeae8d8ec007890bcf3ec1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nopaths, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nopaths, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section305e41a80d737d550124b6019ec33f0e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'name, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'name, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA37a67d524174c7f9757deb74bba3f38(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'description, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'description, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD3b51939ded2c24489088db187ad779f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'actions, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'actions, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionD3007bce4514a0aa788add7863b19657(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <img width="50" src="{{{ linkthumbnail }}}">
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <img width="50" src="';
                $value = $this->resolveValue($context->find('linkthumbnail'), $context);
                $buffer .= $value;
                $buffer .= '">
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section421c4bac536f0f2eed0d6d596365d276(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'closeeye, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'closeeye, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section44e838da90d82f7ca0c706a5c58ea023(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <a class="lp_active" href="" data-id="{{ id }}" data-toggle="tooltip" data-state="1" title="{{# str }}closeeye, local_iomad_learningpath{{/ str }}">
                            <i class="fa fa-eye fa-lg"></i>
                        </a>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <a class="lp_active" href="" data-id="';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-toggle="tooltip" data-state="1" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section421c4bac536f0f2eed0d6d596365d276($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                            <i class="fa fa-eye fa-lg"></i>
';
                $buffer .= $indent . '                        </a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB04410e322c1628b6e466e18bad4f198(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'openeye, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'openeye, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3d2bc453804f6b6a40780ab0f029c901(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'editcourselist, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'editcourselist, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section49962fd85caf43997a4fbd0f1c99a3ce(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'editpath, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'editpath, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9419428029c25b3de5513aedf70ba5c1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'editstudents, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'editstudents, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAbcab312acea568c683868fff7e7d6cc(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'copypath, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'copypath, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0a16184dccdab5cbc12ed2d1a7b30321(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'deletepath, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'deletepath, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section417705eeed153bef2f4ffd880f79d316(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="row manage_list">
                <div class="col-sm-1">
                    {{# linkthumbnail }}
                        <img width="50" src="{{{ linkthumbnail }}}">
                    {{/ linkthumbnail }}
                </div>
                <div class="col-sm-4">{{ name }}</div>
                <div class="col-sm-4"><small>{{{ description }}}</small></div>
                <div class="col-sm-3">
                    {{# active }}
                        <a class="lp_active" href="" data-id="{{ id }}" data-toggle="tooltip" data-state="1" title="{{# str }}closeeye, local_iomad_learningpath{{/ str }}">
                            <i class="fa fa-eye fa-lg"></i>
                        </a>
                    {{/ active }}
                    {{^ active }}
                        <a class="lp_active" href="" data-id="{{ id }}" data-toggle="tooltip" data-state="0" title="{{# str }}openeye, local_iomad_learningpath{{/ str }}">
                            <i class="fa fa-eye-slash fa-lg"></i>
                        </a>
                    {{/ active }}
                    &nbsp;<a href="{{{ linkcourses }}}" data-toggle="tooltip" title="{{# str }}editcourselist, local_iomad_learningpath{{/ str }}"><i class="fa fa-list fa-lg"></i></a>
                    &nbsp;<a href="{{{ linkedit }}}" data-toggle="tooltip" title="{{# str }}editpath, local_iomad_learningpath{{/ str }}"><i class="fa fa-cog fa-lg"></i></a>
                    &nbsp;<a href="{{{ linkstudents }}}" data-toggle="tooltip" title="{{# str }}editstudents, local_iomad_learningpath{{/ str }}"><i class="fa fa-users fa-lg"></i></a>
                    &nbsp;<a href="" class="lp_copy text-warning" data-id="{{ id }}" data-toggle="tooltip" title="{{# str }}copypath, local_iomad_learningpath{{/ str }}">
                        <i class="fa fa-copy fa-lg"></i></a>
                    &nbsp;<a href="" class="lp_delete text-danger" data-id="{{ id }}" data-toggle="tooltip" title="{{# str }}deletepath, local_iomad_learningpath{{/ str }}">
                        <i class="fa fa-trash fa-lg"></i></a>
                </div>
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="row manage_list">
';
                $buffer .= $indent . '                <div class="col-sm-1">
';
                // 'linkthumbnail' section
                $value = $context->find('linkthumbnail');
                $buffer .= $this->sectionD3007bce4514a0aa788add7863b19657($context, $indent, $value);
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '                <div class="col-sm-4">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</div>
';
                $buffer .= $indent . '                <div class="col-sm-4"><small>';
                $value = $this->resolveValue($context->find('description'), $context);
                $buffer .= $value;
                $buffer .= '</small></div>
';
                $buffer .= $indent . '                <div class="col-sm-3">
';
                // 'active' section
                $value = $context->find('active');
                $buffer .= $this->section44e838da90d82f7ca0c706a5c58ea023($context, $indent, $value);
                // 'active' inverted section
                $value = $context->find('active');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                        <a class="lp_active" href="" data-id="';
                    $value = $this->resolveValue($context->find('id'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '" data-toggle="tooltip" data-state="0" title="';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionB04410e322c1628b6e466e18bad4f198($context, $indent, $value);
                    $buffer .= '">
';
                    $buffer .= $indent . '                            <i class="fa fa-eye-slash fa-lg"></i>
';
                    $buffer .= $indent . '                        </a>
';
                }
                $buffer .= $indent . '                    &nbsp;<a href="';
                $value = $this->resolveValue($context->find('linkcourses'), $context);
                $buffer .= $value;
                $buffer .= '" data-toggle="tooltip" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section3d2bc453804f6b6a40780ab0f029c901($context, $indent, $value);
                $buffer .= '"><i class="fa fa-list fa-lg"></i></a>
';
                $buffer .= $indent . '                    &nbsp;<a href="';
                $value = $this->resolveValue($context->find('linkedit'), $context);
                $buffer .= $value;
                $buffer .= '" data-toggle="tooltip" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section49962fd85caf43997a4fbd0f1c99a3ce($context, $indent, $value);
                $buffer .= '"><i class="fa fa-cog fa-lg"></i></a>
';
                $buffer .= $indent . '                    &nbsp;<a href="';
                $value = $this->resolveValue($context->find('linkstudents'), $context);
                $buffer .= $value;
                $buffer .= '" data-toggle="tooltip" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section9419428029c25b3de5513aedf70ba5c1($context, $indent, $value);
                $buffer .= '"><i class="fa fa-users fa-lg"></i></a>
';
                $buffer .= $indent . '                    &nbsp;<a href="" class="lp_copy text-warning" data-id="';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-toggle="tooltip" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionAbcab312acea568c683868fff7e7d6cc($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                        <i class="fa fa-copy fa-lg"></i></a>
';
                $buffer .= $indent . '                    &nbsp;<a href="" class="lp_delete text-danger" data-id="';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-toggle="tooltip" title="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section0a16184dccdab5cbc12ed2d1a7b30321($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                        <i class="fa fa-trash fa-lg"></i></a>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section23d0e5c6593adf64f7c782e9d151b20f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div id="manage_learningpaths">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4"><b>{{# str }}name, local_iomad_learningpath{{/ str }}</b></div>
            <div class="col-sm-4"><b>{{# str }}description, local_iomad_learningpath{{/ str }}</b></div>
            <div class="col-sm-3"><b>{{# str }}actions, local_iomad_learningpath{{/ str }}</b></div>
        </div>
        {{# paths }}
            <div class="row manage_list">
                <div class="col-sm-1">
                    {{# linkthumbnail }}
                        <img width="50" src="{{{ linkthumbnail }}}">
                    {{/ linkthumbnail }}
                </div>
                <div class="col-sm-4">{{ name }}</div>
                <div class="col-sm-4"><small>{{{ description }}}</small></div>
                <div class="col-sm-3">
                    {{# active }}
                        <a class="lp_active" href="" data-id="{{ id }}" data-toggle="tooltip" data-state="1" title="{{# str }}closeeye, local_iomad_learningpath{{/ str }}">
                            <i class="fa fa-eye fa-lg"></i>
                        </a>
                    {{/ active }}
                    {{^ active }}
                        <a class="lp_active" href="" data-id="{{ id }}" data-toggle="tooltip" data-state="0" title="{{# str }}openeye, local_iomad_learningpath{{/ str }}">
                            <i class="fa fa-eye-slash fa-lg"></i>
                        </a>
                    {{/ active }}
                    &nbsp;<a href="{{{ linkcourses }}}" data-toggle="tooltip" title="{{# str }}editcourselist, local_iomad_learningpath{{/ str }}"><i class="fa fa-list fa-lg"></i></a>
                    &nbsp;<a href="{{{ linkedit }}}" data-toggle="tooltip" title="{{# str }}editpath, local_iomad_learningpath{{/ str }}"><i class="fa fa-cog fa-lg"></i></a>
                    &nbsp;<a href="{{{ linkstudents }}}" data-toggle="tooltip" title="{{# str }}editstudents, local_iomad_learningpath{{/ str }}"><i class="fa fa-users fa-lg"></i></a>
                    &nbsp;<a href="" class="lp_copy text-warning" data-id="{{ id }}" data-toggle="tooltip" title="{{# str }}copypath, local_iomad_learningpath{{/ str }}">
                        <i class="fa fa-copy fa-lg"></i></a>
                    &nbsp;<a href="" class="lp_delete text-danger" data-id="{{ id }}" data-toggle="tooltip" title="{{# str }}deletepath, local_iomad_learningpath{{/ str }}">
                        <i class="fa fa-trash fa-lg"></i></a>
                </div>
            </div>
        {{/ paths }}
    </div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div id="manage_learningpaths">
';
                $buffer .= $indent . '        <div class="row">
';
                $buffer .= $indent . '            <div class="col-sm-1"></div>
';
                $buffer .= $indent . '            <div class="col-sm-4"><b>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section305e41a80d737d550124b6019ec33f0e($context, $indent, $value);
                $buffer .= '</b></div>
';
                $buffer .= $indent . '            <div class="col-sm-4"><b>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionA37a67d524174c7f9757deb74bba3f38($context, $indent, $value);
                $buffer .= '</b></div>
';
                $buffer .= $indent . '            <div class="col-sm-3"><b>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionD3b51939ded2c24489088db187ad779f($context, $indent, $value);
                $buffer .= '</b></div>
';
                $buffer .= $indent . '        </div>
';
                // 'paths' section
                $value = $context->find('paths');
                $buffer .= $this->section417705eeed153bef2f4ffd880f79d316($context, $indent, $value);
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFc501e95566fe6294073eac456cbef24(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'addpath, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'addpath, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
