<?php

class __Mustache_d7e15305049a83ff544e7ef01a57a475 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'companies' section
        $value = $context->find('companies');
        $buffer .= $this->section1342b07843b8eb875d53620b6e8f9c75($context, $indent, $value);

        return $buffer;
    }

    private function sectionB2034de34dd4e72c29ce2e2b1d4cf165(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'ml-5';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'ml-5';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA096f5abfcab2beb2743290fe3cd56a6(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'childcompany, local_report_companies, {{{ parent }}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'childcompany, local_report_companies, ';
                $value = $this->resolveValue($context->find('parent'), $context);
                $buffer .= $value;
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA20e6246204d8502d38b183d48987c69(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <div class="alert alert-success">
                    {{# str }}childcompany, local_report_companies, {{{ parent }}}{{/ str }}
                </div>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <div class="alert alert-success">
';
                $buffer .= $indent . '                    ';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionA096f5abfcab2beb2743290fe3cd56a6($context, $indent, $value);
                $buffer .= '
';
                $buffer .= $indent . '                </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9a573b075935eb48adba0bc55fbeca35(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nomanagers, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nomanagers, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section41808530794502673caafd2187ddc8e5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <div class="alert alert-warning">{{# str }}nomanagers, local_report_companies{{/ str }}</div>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <div class="alert alert-warning">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section9a573b075935eb48adba0bc55fbeca35($context, $indent, $value);
                $buffer .= '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEfb404a3712d2358c0d2fb8de61bcfee(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'coursemanagers, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'coursemanagers, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7c0acd8e9a5b70ca3351936088d32516(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'totalusercount, local_report_companies, {{ companymanagerscount }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'totalusercount, local_report_companies, ';
                $value = $this->resolveValue($context->find('companymanagerscount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5da1fb06537597690bfb838997c737ac(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                            {{> local_report_companies/userlist }}
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                if ($partial = $this->mustache->loadPartial('local_report_companies/userlist')) {
                    $buffer .= $partial->renderInternal($context, $indent . '                            ');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4ad12dc1edd89459e22a8bfeefdea4ee(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'departmentmanagers, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'departmentmanagers, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4f8112f786a15694f939c992579bbf92(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'totalusercount, local_report_companies, {{ departmentmanagerscount }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'totalusercount, local_report_companies, ';
                $value = $this->resolveValue($context->find('departmentmanagerscount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEe39754d5ead4038903ed2e4bbd63529(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nousers, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nousers, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFbf53fde060db15b832794d37f4857a1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <div class="alert alert-warning">{{# str }}nousers, local_report_companies{{/ str }}</div>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <div class="alert alert-warning">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionEe39754d5ead4038903ed2e4bbd63529($context, $indent, $value);
                $buffer .= '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section073085b57c60396ad8c5bc91db7aece5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'courseusers, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'courseusers, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF638fb6a78e2da5da7f06d3ae459a9b4(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'totalusercount, local_report_companies, {{ userscount }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'totalusercount, local_report_companies, ';
                $value = $this->resolveValue($context->find('userscount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5650a6aff25ad858c6f12004b9cfbc54(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nocourses, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nocourses, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDbdbe1071913541cb41bc544bf922b8b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <div class="alert alert-warning">{{# str }}nocourses, local_report_companies{{/ str }}</div>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <div class="alert alert-warning">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section5650a6aff25ad858c6f12004b9cfbc54($context, $indent, $value);
                $buffer .= '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionCc13d3b068fac69360dd206ed640f56d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'courses, local_report_companies';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'courses, local_report_companies';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDf85940dc3e13834d659193bc0aca98a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'totalcoursecount, local_report_companies, {{ coursescount }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'totalcoursecount, local_report_companies, ';
                $value = $this->resolveValue($context->find('coursescount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1342b07843b8eb875d53620b6e8f9c75(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div class="card mb-2 {{# parent }}ml-5{{/ parent }}">
        <div class="card-body">
            <div class="alert alert-primary">
                <h4 class="card-title"><b>{{ name }}</b></h5>
            </div>
            {{# parent }}
                <div class="alert alert-success">
                    {{# str }}childcompany, local_report_companies, {{{ parent }}}{{/ str }}
                </div>
            {{/ parent }}
            {{# nomanagers }}
                <div class="alert alert-warning">{{# str }}nomanagers, local_report_companies{{/ str }}</div>
            {{/ nomanagers }}
            {{^ nomanagers }}
                <h5 class="card-subtitle mt-2">{{# str }}coursemanagers, local_report_companies{{/ str }}</h5>
                <p class="text-muted">{{# str }}totalusercount, local_report_companies, {{ companymanagerscount }}{{/ str }}</p>
                <div>
                    <ul class="list-unstyled">
                    {{# companymanagers }}
                            {{> local_report_companies/userlist }}
                    {{/ companymanagers }}
                    </ul>
                </div>
                
                <h5 class="card-subtitle mt-2">{{# str }}departmentmanagers, local_report_companies{{/ str }}</h5>
                <p class="text-muted">{{# str }}totalusercount, local_report_companies, {{ departmentmanagerscount }}{{/ str }}</p>
                <div>
                    <ul class="list-unstyled">
                    {{# departmentmanagers }}
                            {{> local_report_companies/userlist }}
                    {{/ departmentmanagers }}
                    </ul>
                </div>
            {{/ nomanagers }}

            {{# nousers }}
                <div class="alert alert-warning">{{# str }}nousers, local_report_companies{{/ str }}</div>
            {{/ nousers }}
            {{^ nousers }}
                <h5 class="card-subtitle mt-2">{{# str }}courseusers, local_report_companies{{/ str }}</h5>
                <p class="text-muted">{{# str }}totalusercount, local_report_companies, {{ userscount }}{{/ str }}</p>
            {{/ nousers }}

            {{# nocourses }}
                <div class="alert alert-warning">{{# str }}nocourses, local_report_companies{{/ str }}</div>
            {{/ nocourses }}
            {{^ nocourses }}
                <h5 class="card-subtitle mt-2">{{# str }}courses, local_report_companies{{/ str }}</h5>
                <p class="text-muted">{{# str }}totalcoursecount, local_report_companies, {{ coursescount }}{{/ str }}</p>
            {{/ nocourses }}
        </div>
    </div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div class="card mb-2 ';
                // 'parent' section
                $value = $context->find('parent');
                $buffer .= $this->sectionB2034de34dd4e72c29ce2e2b1d4cf165($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '        <div class="card-body">
';
                $buffer .= $indent . '            <div class="alert alert-primary">
';
                $buffer .= $indent . '                <h4 class="card-title"><b>';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</b></h5>
';
                $buffer .= $indent . '            </div>
';
                // 'parent' section
                $value = $context->find('parent');
                $buffer .= $this->sectionA20e6246204d8502d38b183d48987c69($context, $indent, $value);
                // 'nomanagers' section
                $value = $context->find('nomanagers');
                $buffer .= $this->section41808530794502673caafd2187ddc8e5($context, $indent, $value);
                // 'nomanagers' inverted section
                $value = $context->find('nomanagers');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                <h5 class="card-subtitle mt-2">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionEfb404a3712d2358c0d2fb8de61bcfee($context, $indent, $value);
                    $buffer .= '</h5>
';
                    $buffer .= $indent . '                <p class="text-muted">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section7c0acd8e9a5b70ca3351936088d32516($context, $indent, $value);
                    $buffer .= '</p>
';
                    $buffer .= $indent . '                <div>
';
                    $buffer .= $indent . '                    <ul class="list-unstyled">
';
                    // 'companymanagers' section
                    $value = $context->find('companymanagers');
                    $buffer .= $this->section5da1fb06537597690bfb838997c737ac($context, $indent, $value);
                    $buffer .= $indent . '                    </ul>
';
                    $buffer .= $indent . '                </div>
';
                    $buffer .= $indent . '                
';
                    $buffer .= $indent . '                <h5 class="card-subtitle mt-2">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section4ad12dc1edd89459e22a8bfeefdea4ee($context, $indent, $value);
                    $buffer .= '</h5>
';
                    $buffer .= $indent . '                <p class="text-muted">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section4f8112f786a15694f939c992579bbf92($context, $indent, $value);
                    $buffer .= '</p>
';
                    $buffer .= $indent . '                <div>
';
                    $buffer .= $indent . '                    <ul class="list-unstyled">
';
                    // 'departmentmanagers' section
                    $value = $context->find('departmentmanagers');
                    $buffer .= $this->section5da1fb06537597690bfb838997c737ac($context, $indent, $value);
                    $buffer .= $indent . '                    </ul>
';
                    $buffer .= $indent . '                </div>
';
                }
                $buffer .= $indent . '
';
                // 'nousers' section
                $value = $context->find('nousers');
                $buffer .= $this->sectionFbf53fde060db15b832794d37f4857a1($context, $indent, $value);
                // 'nousers' inverted section
                $value = $context->find('nousers');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                <h5 class="card-subtitle mt-2">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section073085b57c60396ad8c5bc91db7aece5($context, $indent, $value);
                    $buffer .= '</h5>
';
                    $buffer .= $indent . '                <p class="text-muted">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionF638fb6a78e2da5da7f06d3ae459a9b4($context, $indent, $value);
                    $buffer .= '</p>
';
                }
                $buffer .= $indent . '
';
                // 'nocourses' section
                $value = $context->find('nocourses');
                $buffer .= $this->sectionDbdbe1071913541cb41bc544bf922b8b($context, $indent, $value);
                // 'nocourses' inverted section
                $value = $context->find('nocourses');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                <h5 class="card-subtitle mt-2">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionCc13d3b068fac69360dd206ed640f56d($context, $indent, $value);
                    $buffer .= '</h5>
';
                    $buffer .= $indent . '                <p class="text-muted">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionDf85940dc3e13834d659193bc0aca98a($context, $indent, $value);
                    $buffer .= '</p>
';
                }
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
