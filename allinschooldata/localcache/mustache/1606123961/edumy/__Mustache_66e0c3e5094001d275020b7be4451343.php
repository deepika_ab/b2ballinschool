<?php

class __Mustache_66e0c3e5094001d275020b7be4451343 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<h2>';
        $value = $this->resolveValue($context->findDot('path.name'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</h2>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="row">
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <div class="form-group">
';
        $buffer .= $indent . '            <label for="userfilter">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionF64223af4e9d19a9eff9f4ef8561370d($context, $indent, $value);
        $buffer .= '</label>
';
        $buffer .= $indent . '            <input id="userfilter" class="form-control" type="text" name="userfilter">
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="row m-t-1 m-b-1">
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <div class="card bg-light">
';
        $buffer .= $indent . '            <div class="card-header">
';
        $buffer .= $indent . '                ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section473e535479fc43f57cf42718ad25f072($context, $indent, $value);
        $buffer .= '<br />
';
        $buffer .= $indent . '                <small>
';
        $buffer .= $indent . '                    ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section2fcb4348c986cbb9f4584ba68dd764ec($context, $indent, $value);
        $buffer .= '.
';
        $buffer .= $indent . '                </small>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="card-body card-block">
';
        $buffer .= $indent . '                <p class="card-text">
';
        $buffer .= $indent . '                    <div id="nopathusers" class="alert alert-warning" style="display: none">
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section252401a55062f60f22a7a23c795278de($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                    </div>
';
        $buffer .= $indent . '                    <ul id="pathuserlist">
';
        $buffer .= $indent . '                    </ul>
';
        $buffer .= $indent . '                </p>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    <div class="col-sm-6">
';
        $buffer .= $indent . '        <div class="card bg-light">
';
        $buffer .= $indent . '            <div class="card-header">
';
        $buffer .= $indent . '                ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section21cd4e87deef725aedcb5eed96a806b8($context, $indent, $value);
        $buffer .= '<br />
';
        $buffer .= $indent . '                <small>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section16c8c38b374a4f9bbafb54424912664b($context, $indent, $value);
        $buffer .= '</small>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '            <div class="card-body card-block">
';
        $buffer .= $indent . '                <p class="card-text">
';
        $buffer .= $indent . '                    <div id="noprospective" class="alert alert-warning" style="display: none">
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionE7fb6b423253b72d264461f898b45e08($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                    </div>
';
        $buffer .= $indent . '                    <div id="toomanyprospective" class="alert alert-warning" style="display: none">
';
        $buffer .= $indent . '                        ';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section93a910dc8a84ac4f661d0503ff8da3bf($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                    </div>
';
        $buffer .= $indent . '                    <ul id="prospectivelist">
';
        $buffer .= $indent . '                    </ul>
';
        $buffer .= $indent . '                <p>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';
        $buffer .= $indent . '
';
        $value = $this->resolveValue($context->find('done'), $context);
        $buffer .= $indent . $value;
        $buffer .= '
';

        return $buffer;
    }

    private function sectionF64223af4e9d19a9eff9f4ef8561370d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'filter, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'filter, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section473e535479fc43f57cf42718ad25f072(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'selectedstudents, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'selectedstudents, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2fcb4348c986cbb9f4584ba68dd764ec(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'clicktoremove, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'clicktoremove, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section252401a55062f60f22a7a23c795278de(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'nopathusers, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'nopathusers, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section21cd4e87deef725aedcb5eed96a806b8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'prospectivestudents, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'prospectivestudents, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section16c8c38b374a4f9bbafb54424912664b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'clicktoadd, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'clicktoadd, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE7fb6b423253b72d264461f898b45e08(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'noprospectiveusers, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'noprospectiveusers, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section93a910dc8a84ac4f661d0503ff8da3bf(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'toomanyprospectiveusers, local_iomad_learningpath';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'toomanyprospectiveusers, local_iomad_learningpath';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
