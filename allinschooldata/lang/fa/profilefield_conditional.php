<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'profilefield_conditional', language 'fa', branch 'MOODLE_34_STABLE'
 *
 * @package   profilefield_conditional
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['apply'] = 'تایید';
$string['conditionalhelp'] = 'چگونگی وارد کردن گزینه‌ها';
$string['conditionalhelp_help'] = 'لطفا گزینه‌های فهرست را به‌صورت خط به خط وارد کنید. بعد از این کار می‌توانید با کلیک بر روی دکمهٔ «پیکربندی شرایط» تعیین کنید که بازای انتخاب هر گزینه، وارد کردن چه فیلدهای دیگری اجباری باشد و چه فیلدهای دیگری پنهان شوند.';
$string['configurecondition'] = 'پیکربندی شرایط';
$string['emptycondition'] = 'باید شرایط گزینه‌ها را پیکربندی کنید.';
$string['extradata'] = 'فرم ارسال شده شامل فیلدهایی است که بر اساس مقداری که برای این گزینه انتخاب کرده‌اید باید خالی باشند.';
$string['hidden'] = 'مخفی';
$string['hiddeninitially'] = 'مخفی بودن اولیه گزینه‌ها';
$string['hiddeninitially_help'] = '* بله - هیچکدام از فیلدهایی که تحت شرایطی پنهان خواهند شد در ابتدا نمایش داده نخواهند شد. بسته به گزینه‌ای که کاربر انتخاب می‌کند، برخی از فیلدها در فرم ظاهر می‌شوند.
* خیر - تمام فیلدها در ابتدا نمایش داده می‌شوند. پس از اینکه کاربر گزینه‌ای را انتخاب کرد، برخی از فیلدهای موجود پنهان می‌شوند.';
$string['hiddenrequired'] = 'حداقل یک گزینه وجود دارد که هم به‌عنوان گزینهٔ اجباری و هم مخفی تعیین شده است!';
$string['menuoption'] = 'گزینهٔ انتخابی';
$string['notaprofilefield'] = 'عضی از فیلدهایی که در پیکربندی شرایط به آنها ارجاع داده شده است وجود ندارند. لطفا شرایط را بررسی کنید. به‌خاطر داشته باشید که اگر شرایط از نظر شما مشکلی ندارد، بر روی دکمهٔ «تایید» کلیک کن.';
$string['notice'] = 'اگر بیش از یک فیلد شرطی دارید، لطفا دقت مضاعفی بفرمایید تا شرایط تعیین‌شده برای این فیلدها با یکدیگر تداخل نداشته باشد. لطفا بررسی کنید تا حالتی پیش نیاید که یک فیلد در آن واحد هم مخفی و هم اجباری با.';
$string['optionconditionmismatch'] = 'بعد از آخرین باری که شرایط گزینه‌ها را پیکربندی کردید، تغییراتی در گزینه‌های فهرست داده‌اید. لطفا بررسی کنید که شرایط گزینه‌ها معتبر با.';
$string['pluginname'] = 'منوی باز شونده شرطی';
$string['required'] = 'اجباری';
$string['requiredbycondition1'] = 'پر کردن این فیلد در حالتی که {$a->field1} {$a->value1} است اجباری است';
$string['requiredbycondition2'] = 'با توجه به مقدار این گزینه، فیلد {$a->field2} نمی‌تواند خالی بماند.';
