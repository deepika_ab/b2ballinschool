<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_bcu', language 'fa', branch 'MOODLE_29_STABLE'
 *
 * @package   theme_bcu
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['backcolor'] = 'رنگ پس‌زمینه';
$string['backcolordesc'] = 'رنگ پس‌زمینه را تعیین کنید.';
$string['buttoncolour'] = 'رنگ دکمه‌ها';
$string['buttoncolourdesc'] = 'رنگ دکمه‌های اصلی استفاده شده در سایت.';
$string['buttonhovercolour'] = 'رنگ دکمه‌ها (در هنگام اشاره)';
$string['buttonhovercolourdesc'] = 'رنگی که هنگامی که روی دکمه می‌رویم رنگ دکمه به آن تغییر می‌کند.';
$string['choosereadme'] = '<div class="clearfix">
<div class="well">
<h2>BCU Responsive</h2>
<p><img class=img-polaroid src="bcu/pix/screenshot.png" /></p>
</div>
<div class="well">
<h3>درباره</h3>
<p>این یک پوستهٔ bootstrap تغییرکردهٔ مودل است که style ها و renderer ها را از پوسته‌های والدش به‌ارث می‌برد.</p>
<h3>والدین</h3>
<p>این پوسته از روی پوستهٔ Bootstrap که برای مودل ۲٫۶ ساخته شده بود ساخته شده است.</p>
</div></div>';
$string['colourdesc'] = 'رنگ‌هایی که مایلید در این پوسته استفاده شوند را می‌توانید انتخاب کنید.';
$string['coloursettings'] = 'رنگ‌ها';
$string['coloursettingsheading'] = 'رنگ‌های اصلی استفاده شده در پوسته را تعییر دهید.';
$string['customcss'] = 'CSS سفارشی';
$string['customcssjssettings'] = 'CSS و جاوااسکریپت سفارشی';
$string['enablebadges'] = 'مدال‌ها';
$string['enablebadgesdesc'] = 'نمایش دادن یک پیوند به مدال‌های کاربران';
$string['frontpagerendererdefaultimagedesc'] = 'تصویر پیش‌فرض که چنانچه تصویری برای درس پیدا نشود از آن استفاده خواهد شد. (تنها در حالت کاشی با روکش اعمال می‌شود)';
$string['frontpagerendereroption1'] = 'کاشی';
$string['frontpagerendereroption2'] = 'کاشی با روکش';
$string['fullscreen'] = 'تمام‌صفحه';
$string['hideblocks'] = 'پنهان‌کردن بلوک‌ها';
$string['linkcolor'] = 'رنگ پیوندها';
$string['linkcolordesc'] = 'با استفاده از کد html هگزادسیمال، رنگ پیوندها را در این پوسته تعیین کنید.';
$string['linkhover'] = 'رنگ روی پیوندها';
$string['linkhoverdesc'] = 'با استفاده از کد html هگزادسیمال، رنگ پیوندها را در این پوسته (در حالتی که mouse روی آنها است) تعیین کنید.';
$string['logo'] = 'لوگو';
$string['maincolor'] = 'رنگ اصلی';
$string['maincolordesc'] = 'رنگ اصلی بلوک‌ها و پایین صفحه';
$string['p3url'] = 'آدرس اسلاید ۳';
$string['rendereroverlaycolour'] = 'رنگ روکش';
$string['rendereroverlaycolourdesc'] = 'رنگ روکش در حالتی که renderer «کاشی با روکش» انتخاب شده باشد.';
$string['rendereroverlayfontcolour'] = 'رنگ قلم روکش';
$string['rendereroverlayfontcolourdesc'] = 'رنگ قلم در هنگامی renderer «کاشی با روکش» فعال باشد و روی یک باکس درس برویم.';
$string['showblocks'] = 'نمایش بلوک‌ها';
$string['sitetitle'] = 'عنوان سایت';
$string['sitetitledesc'] = 'نمایش / پنهان کردن عنوان سایت';
$string['standardview'] = 'نمای استاندارد';
