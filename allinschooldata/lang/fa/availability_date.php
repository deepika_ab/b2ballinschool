<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability_date', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability_date
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['description'] = 'جلوگیری از دسترسی تا (یا از) یک تاریخ و ساعت مشخص';
$string['direction_before'] = 'تاریخ';
$string['direction_from'] = 'از';
$string['direction_label'] = 'جهت';
$string['direction_until'] = 'تا';
$string['full_from'] = 'هم‌اکنون بعد از <strong>{$a}</strong> باشد';
$string['full_from_date'] = 'هم‌اکنون <strong>{$a}</strong> یا بعد از آن باشد';
$string['full_until'] = 'هم‌اکنون پیش از <strong>{$a}</strong> باشد';
$string['full_until_date'] = 'هم‌اکنون پیش از پایان <strong>{$a}</strong> باشد';
$string['pluginname'] = 'محدودیت بر اساس تاریخ';
$string['privacy:metadata'] = 'پلاگین محدودیت بر اساس تاریخ هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['short_from'] = 'از <strong>{$a}</strong> در دسترس است';
$string['short_from_date'] = 'از <strong>{$a}</strong> در دسترس است';
$string['short_until'] = 'تا <strong>{$a}</strong> در دسترس است';
$string['short_until_date'] = 'تا پایان <strong>{$a}</strong> در دسترس است';
$string['title'] = 'تاریخ';
