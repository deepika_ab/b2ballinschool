<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_shibboleth', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   auth_shibboleth
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_shibboleth_login'] = 'ورود shibboleth';
$string['auth_shibboleth_manual_login'] = 'ورود دستی';
$string['auth_shib_logout_return_url'] = 'آدرس بازگشت جایگزین پس از خروج از سایت';
$string['auth_shib_logout_return_url_description'] = 'آدرسی که کاربران shibboleth پس از خروج از سایت باید به آنجا هدایت شوند را وارد کنید.<br />اگر خالی گذاشته شود، کاربران به محلی که مودل کاربران را هدایت می‌کند هدایت خواهند شد';
$string['auth_shib_only'] = 'تنها shibboleth';
$string['privacy:metadata'] = 'پلاگین شناسایی Shiboleth هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
