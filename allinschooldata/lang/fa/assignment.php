<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignment', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignment
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['assignment:addinstance'] = 'اضافه کردن یک تکلیف جدید';
$string['assignmentdisabled'] = 'فعالیت قدیمی «تکلیف ۲٫۲» غیرفعال است.';
$string['assignment:exportownsubmission'] = 'خروجی گرفتن از تکالیف شخصی';
$string['assignment:exportsubmission'] = 'خروجی گرفتن از تکالیف';
$string['assignment:grade'] = 'تصحیح تکلیف';
$string['assignmentneedsupgrade'] = 'فعالیت قدیمی «تکلیف ۲٫۲» غیرفعال شده است. لطفا از مدیر سایت بخواهید که ابزار ارتقای تکلیف را برای تمام تکلیف‌های قدیمی این سایت اجرا کند.';
$string['assignment:submit'] = 'تحویل تکلیف';
$string['assignment:view'] = 'دیدن تکلیف';
$string['messageprovider:assignment_updates'] = 'اطلاعیه‌های مربوط به تکلیف‌ها (۲٫۲)';
$string['modulename'] = 'تکلیف ۲٫۲ (غیرفعال)';
$string['modulename_help'] = 'یک ماژول فعالیت قدیمی که از مودل حذف شده است.';
$string['modulenameplural'] = 'تکالیف ۲٫۲ (غیرفعال)';
$string['page-mod-assignment-submissions'] = 'صفحه تحویل تکلیف';
$string['page-mod-assignment-view'] = 'صفحه اصلی ماژول تکلیف';
$string['page-mod-assignment-x'] = 'هر صفحه‌ای از ماژول تکلیف';
$string['pluginadministration'] = 'مدیریت تکلیف ۲٫۲ (غیرفعال)';
$string['pluginname'] = 'تکلیف ۲٫۲ (غیرفعال)';
