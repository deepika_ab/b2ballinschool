<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'glossary', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   glossary
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcomment'] = 'اظهار نظر';
$string['addentry'] = 'تعریف یک اصطلاح جدید';
$string['addingcomment'] = 'اظهار نظر';
$string['alias'] = 'کلمهٔ کلیدی';
$string['aliases'] = 'کلمات کلیدی';
$string['aliases_help'] = 'برای هر یک از اصطلاحات واژه‌نامه می‌توان لیستی از کلمات کلیدی (یا نام‌های مستعار) تعریف کرد. اگر اصطلاحی به صورت پیوند شدن خودکار تعریف شده باشد، آنگاه تمام کلمات کلیدی‌اش نیز به صورت خودکار پیوند داده خواهند شد.

هر یک از کلمات کلیدی را در یک خط جداگانه (با کاما جدا نکنید) بنویسید.';
$string['allcategories'] = 'همهٔ دسته‌ها';
$string['allentries'] = 'همه';
$string['allowcomments'] = 'مجاز بودن اظهار نظر در مورد اصطلاحات';
$string['allowcomments_help'] = 'در صورت مجاز بودن، تمام شرکت کنندگانی که مجوز ایجاد نظر را داشته باشند قادر به اضافه کردن نظر به اصطلاحات واژه‌نامه خواهند بود.';
$string['allowduplicatedentries'] = 'مجاز بودن اصطلاحات تکراری';
$string['allowduplicatedentries_help'] = 'در صورت مجاز بودن، چندین اصطلاح در واژه‌نامه می‌توانند نام یکسانی داشته باشند.';
$string['allowprintview'] = 'مجاز بودن نسخهٔ آمادهٔ چاپ';
$string['allowprintview_help'] = 'در صورت فعال بودن، پیوندی به یک نسخهٔ آمادهٔ چاپ از واژه‌نامه به شاگردان نشان داده خواهد شد. این پیوند در هر صورت برای اساتید موجود است.';
$string['andmorenewentries'] = 'و {$a} اصطلاح جدید دیگر.';
$string['answer'] = 'پاسخ';
$string['approvaldisplayformat'] = 'نحوهٔ نمایش برای تایید کردن';
$string['approvaldisplayformat_help'] = 'در هنگام تایید اصطلاحات واژه‌نامه ممکن است بخواهید از نحوهٔ نمایش متفاوتی استفاده شود';
$string['approve'] = 'تایید';
$string['areaattachment'] = 'پیوست‌ها';
$string['areaentry'] = 'تعاریف';
$string['areyousuredelete'] = 'مطمئنید که می‌خواهید این مورد را حذف کنید؟';
$string['areyousuredeletecomment'] = 'آیا مطمئنید که می‌خواید این نظر را حذف کنید؟';
$string['areyousureexport'] = 'آیا مطمئن هستید که می‌خواهید این اصطلاح را به واژه‌نامه زیر صادر کنید';
$string['ascending'] = 'به صورت صعودی';
$string['attachment'] = 'فایل ضمیمه';
$string['attachment_help'] = 'در صورت تمایل می‌توانید یک یا چند فایل را به یک اصطلاح در واژه‌نامه ضمیمه کنید.';
$string['author'] = 'تعریف کننده';
$string['authorview'] = 'بر اساس نویسنده';
$string['back'] = 'بازگشت';
$string['cantinsertcat'] = 'درج دسته در پایگاه داده با شکست مواجه شد';
$string['cantinsertrec'] = 'درج رکورد در پایگاه داده با شکست مواجه شد';
$string['cantinsertrel'] = 'درج رابطهٔ دسته-اصطلاح در پایگاه داده با شکست مواجه شد';
$string['casesensitive'] = 'بزرگ و کوچک بودن حروف این اصطلاح مهم است';
$string['casesensitive_help'] = 'این تنظیم تعیین می‌کند که آیا تطابق دقیق حروف بزرگ و کوچک (در اصطلاح‌های لاتین) در هنگام پیوند دهی خودکار به یک اصطلاح لازم است یا خیر.';
$string['cat'] = 'دسته‌ها';
$string['categories'] = 'دسته‌ها';
$string['category'] = 'دسته';
$string['categorydeleted'] = 'دستهٔ مورد نظر حذف شد';
$string['categoryview'] = 'بر اساس دسته‌ها';
$string['changeto'] = '{$a}';
$string['cnfallowcomments'] = 'تعیین می‌کند که آیا در یک واژه‌نامه به صورت پیش‌فرض اظهار نظر مجاز است یا خیر';
$string['cnfallowdupentries'] = 'تعیین می‌کند که آیا در یک واژه‌نامه به صورت پیش‌فرض وجود اصطلاحات تکراری مجاز است یا خیر';
$string['cnfapprovalstatus'] = 'وضعیت پیش‌فرض تایید یک اصطلاح اضافه شده توسط یک شاگرد را تعیین می‌کند';
$string['cnfcasesensitive'] = 'تعیین می‌کند که در هنگام پیوند داده شدن به یک اصطلاح، بزرگ و کوچک بودن حروف به صورت پیش‌فرض مهم است یا خیر';
$string['cnfdefaulthook'] = 'گزینش پیش‌فرض جهت نمایش در زمانی که واژه‌نامه برای اولین بار مشاهده می‌شود را انتخاب کنید.';
$string['cnfdefaultmode'] = 'قاب پیش‌فرض جهت نمایش در زمانی که واژه‌نامه برای اولین بار مشاهده می‌شود را انتخاب کنید.';
$string['cnffullmatch'] = 'مشخص می‌کند که یک اصطلاح، زمانی که به صورت پیوند در می‌آید، باید به صورت کلمهٔ کامل در متن پیدا شود یا اینکه در قسمتی از کلمات متن باشد نیز کافی است';
$string['cnflinkentry'] = 'تعیین می‌کند که آیا به صورت پیش‌فرض باید به یک اصطلاح پیوند داده شود یا خیر';
$string['cnflinkglossaries'] = 'تعیین می‌کند که آیا باید به اصطلاحات یک واژه‌نامه به‌صورت پیش‌فرض پیوند داده شود یا خیر';
$string['cnfrelatedview'] = 'نحوهٔ نمایش مورد استفاده برای پیوند دادن خودکار و نمایش اصطلاح را انتخاب کنید.';
$string['cnfshowgroup'] = 'تعیین نمائید که آیا جدا کنندهٔ گروه‌ها باید نمایش داده شود یا خیر.';
$string['cnfsortkey'] = 'تعیین نمائید که به صورت پیش‌فرض مرتب‌سازی بر اساس چه باشد.';
$string['cnfsortorder'] = 'ترتیب پیش‌فرض در مرتب‌سازی را انتخاب نمائید.';
$string['cnfstudentcanpost'] = 'تعیین می‌کند که به صورت پیش‌فرض آیا شاگردان می‌توانند اصطلاح جدید اضافه کنند یا خیر';
$string['comment'] = 'نظر';
$string['commentdeleted'] = 'نظر مربوطه حذف شد.';
$string['comments'] = 'نظرات';
$string['commentson'] = 'نظرات داده شده در مورد';
$string['commentupdated'] = 'نظر مربوطه به‌روز شد.';
$string['completionentries'] = 'شاگرد باید این تعداد داده وارد کند:';
$string['completionentriesgroup'] = 'نیاز به ورود داده';
$string['concept'] = 'اصطلاح';
$string['concepts'] = 'اصطلاحات';
$string['configenablerssfeeds'] = 'این گزینه امکان استفاده از RSS feed ها را برای همهٔ واژه‌نامه‌ها فراهم می‌کند. اگرچه باز هم لازم است به صورت دستی این قابلیت را برای هر واژه‌نامه فعال کنید.';
$string['current'] = 'در حال حاضر {$a} مرتب شده است';
$string['currentglossary'] = 'واژه‌نامه فعلی';
$string['date'] = 'تاریخ';
$string['dateview'] = 'بر اساس تاریخ';
$string['defaultapproval'] = 'تایید شده به صورت پیش‌فرض';
$string['defaultapproval_help'] = 'اگر بر روی خیر تنظیم شده باشد، اصطلاحات واژه‌نامه پیش از اینکه توسط همه قابل مشاهده باشند باید توسط یک استاد تایید شوند.';
$string['defaulthook'] = 'انتخاب پیش‌فرض';
$string['defaultmode'] = 'حالت پیش‌فرض';
$string['defaultsortkey'] = 'مرتب‌سازی پیش‌فرض بر اساس';
$string['defaultsortorder'] = 'ترتیب پیش‌فرض';
$string['definition'] = 'تعریف';
$string['definitions'] = 'تعاریف';
$string['deleteentry'] = 'حذف اصطلاح';
$string['deletenotenrolled'] = 'حذف اصطلاحات تعریف شده توسط کاربرانی که عضو نیستند';
$string['deletingcomment'] = 'در حال حذف نظر';
$string['deletingnoneemptycategory'] = 'حذف این دسته موجب حذف اصطلاحاتی که شامل شده است نمی‌شود - آنها به‌عنوان دسته‌بندی‌نشده علامت‌گذاری می‌شوند.';
$string['descending'] = 'به صورت نزولی';
$string['destination'] = 'مقصد';
$string['destination_help'] = 'اصطلاحات می‌توانند وارد و به واژه‌نامه فعلی یا یک واژه‌نامه جدید اضافه شوند. در حالت دوم، یک واژه‌نامه جدید بر اساس اطلاعات فایل XML ایجاد خواهد شد.';
$string['displayformat'] = 'نحوهٔ نمایش';
$string['displayformatcontinuous'] = 'متوالی، بدون نام تعریف کننده';
$string['displayformatdefault'] = 'به‌طور پیش‌فرض برابر با نحوهٔ نمایش';
$string['displayformatdictionary'] = 'ساده، لغت‌نامه وار';
$string['displayformatencyclopedia'] = 'دانشنامه';
$string['displayformatentrylist'] = 'لیست اصطلاحات';
$string['displayformatfaq'] = 'سوالات متداول';
$string['displayformatfullwithauthor'] = 'کامل، به همراه نام تعریف کننده';
$string['displayformatfullwithoutauthor'] = 'کامل، بدون نام تعریف کننده';
$string['displayformat_help'] = '۷ نوع مختلف نمایش وجود دارد:

* دانشنامه - مانند «کامل، همراه با نام تعریف کننده» ولی با این تفاوت که تصاویر ضمیمه در متن تعریف‌ها نمایش داده می‌شوند
* ساده، لغت‌نامه وار - تعریف کنندگان اصطلاحات نشان داده نمی‌شوند و ضمیمه‌ها به صورت پیوند نمایش داده می‌شوند
* سؤالات متداول - کلمات«سؤال» و «پاسخ» به ترتیب به ابتدای اصطلاح و تعریف آن اضافه می‌شوند
* کامل، بدون نام تعریف کننده - یک نحوهٔ نمایش تالار گفتگو مانند که نام کاربری که اصطلاح را تعریف کرده است نشان نمی‌دهد و ضمیمه‌ها به صورت پیوند نمایش داده می‌شوند
* کامل، به همراه نام تعریف کننده - یک نحوهٔ نمایش تالار گفتگو مانند که نام کاربری که اصطلاح را تعریف کرده است نشان می‌دهد و ضمیمه‌ها به صورت پیوند نمایش داده می‌شوند
* لیست اصطلاحات - اصطلاح‌حا به صورت پیوند لیست می‌شوند
* متوالی، بدون نام تعریف کننده - اصطلاحات به صورت پشت سر هم نمایش داده می‌شوند';
$string['displayformats'] = 'نحوه‌های نمایش';
$string['displayformatssetup'] = 'تنظیم نحوه‌های نمایش';
$string['duplicatecategory'] = 'دستهٔ تکراری';
$string['duplicateentry'] = 'تکراری';
$string['editalways'] = 'ویرایش دائمی';
$string['editalways_help'] = 'این تنظیم تعیین می‌کند که آیا اصطلاح‌ها برای همیشه قابل ویرایش هستند یا اینکه شاگردان فقط برای مدت محدود تعیین شده‌ای (معمولاً ۳۰ دقیقه) فرصت دارند تا اصطلاح‌هایی که تعریف کرده‌اند را ویرایش کنند.';
$string['editcategories'] = 'ویرایش دسته‌ها';
$string['editentry'] = 'ویرایش اصطلاح';
$string['editingcomment'] = 'در حال ویرایش نظر';
$string['entbypage'] = 'تعداد اصطلاحات در هر صفحه';
$string['entries'] = 'اصطلاح';
$string['entrieswithoutcategory'] = 'اصطلاحات دسته‌بندی نشده';
$string['entry'] = 'مورد';
$string['entryalreadyexist'] = 'این مورد قبلاً تعریف شده است';
$string['entryapproved'] = 'اصطلاح مورد نظر تایید شد';
$string['entrydeleted'] = 'اصطلاح مورد نظر حذف شد';
$string['entryexported'] = 'صدور اصطلاح با موفقیت انجام شد';
$string['entryishidden'] = '(این اصطلاح در حال حاظر پنهان است)';
$string['entryleveldefaultsettings'] = 'تنظیمات پیش‌فرض در سطح اصطلاح';
$string['entrysaved'] = 'اصطلاح مورد نظر ذخیره شد';
$string['entryupdated'] = 'اصطلاح مورد نظر به‌روز شد';
$string['entryusedynalink'] = 'به این اصطلاح باید به صورت خودکار پیوند ایجاد شود';
$string['entryusedynalink_help'] = 'در صورتی که قابلیت پیونددهی خودکار توسط یک مدیر در سطح سایت فعال شده باشد و این گزینه هم انتخاب شده باشد، این اصطلاح در در هر جایی از درس که ظاهر شود به صورت خودکار پیوند خواهد شد.';
$string['errcannoteditothers'] = 'شما نمی‌توانید اصطلاحات تعریف شده توسط دیگران را ویرایش کنید.';
$string['errconceptalreadyexists'] = 'این اصطلاح قبلاً تعریف شده است. وجود اصطلاحات تکراری در این واژه‌نامه مجاز نمی‌باشد.';
$string['errdeltimeexpired'] = 'نمی‌توانید این مورد را پاک کنید. زمان مجاز سپری شده است!';
$string['erredittimeexpired'] = 'مهلت ویرایش این اصطلاح به پایان رسیده است.';
$string['errorparsingxml'] = 'تجزیه کردن فایل با خطا مواجه شد. مطمئن شوید که فایل مورد نظر یک فایل XML معتبر باشد.';
$string['eventcategorydeleted'] = 'دسته حذف شد';
$string['evententrydeleted'] = 'اصطلاح حذف شد';
$string['explainaddentry'] = 'تعریف یک اصطلاح جدید در این واژه‌نامه.<br />اصطلاح و تعریف جزو موارد ضروری هستند.';
$string['explainall'] = 'نمایش همهٔ اصطلاحات در یک صفحه';
$string['explainalphabet'] = 'می‌توانید از شاخص زیر برای مشاهدهٔ واژه‌نامه استفاده کنید';
$string['explainexport'] = 'جهت صدور اصطلاحات واژه‌نامه روی دکمهٔ زیر کلیک کنید.<br />اصطلاحات صادر شده را هر زمان که بخواهید می‌توانید در این درس یا درس‌های دیگر وارد کنید.<p>لطفاً توجه داشته باشید که فایل‌های ضمیمه (مانند تصاویر) و اسامی تعریف کنندگان صادر نمی‌شود.</p>';
$string['explainimport'] = 'باید فایلی که می‌خواهید اصطلاحات از آن وارد شوند را به همراه ضوابط ورود اصطلاحات مشخص نمائید.<p>درخواست خود را بفرستید و نتایج را بررسی کنید.</p>';
$string['explainspecial'] = 'نمایش اصطلاحاتی که با یکی از حروف الفبا شروع نمی‌شوند';
$string['exportedentry'] = 'اصطلاح صادره';
$string['exportentries'] = 'صدور اصطلاحات';
$string['exportentriestoxml'] = 'صدور اصطلاحات به فایل XML';
$string['exportfile'] = 'صدور اصطلاحات به فایل';
$string['exportglossary'] = 'صدور واژه‌نامه';
$string['exporttomainglossary'] = 'صدور به واژه‌نامه اصلی';
$string['filetoimport'] = 'فایل ورودی';
$string['filetoimport_help'] = 'فایل XML ای که شامل اصطلاحات مورد نظر برای ورود است را انتخاب کنید.';
$string['fillfields'] = 'اصطلاح و تعریف جزو موارد ضروری هستند.';
$string['filtername'] = 'پیونددهی خودکار واژه‌نامه';
$string['fullmatch'] = 'تطابق در کل کلمه';
$string['fullmatch_help'] = 'این تنظیم تعیین می‌کند که آیا فقط کل کلمات پیوند خواهد شد یا خیر. به عنوان مثال اصطلاح «ساخت» موجود در واژه‌نامه، در کلمهٔ «ساختمان» پیوند نخواهد شد.';
$string['glossary:addinstance'] = 'اضافه‌کردن یک واژه‌نامهٔ جدید';
$string['glossary:approve'] = 'تایید و لغو تایید اصطلاحات تایید شده';
$string['glossary:comment'] = 'اظهار نظر';
$string['glossary:export'] = 'صدور اصطلاحات';
$string['glossary:exportentry'] = 'صدور یک اصطلاح';
$string['glossary:exportownentry'] = 'صدور یک اصطلاح متعلق به خود';
$string['glossary:import'] = 'ورود اصطلاحات از فایل صادره';
$string['glossaryleveldefaultsettings'] = 'تنظیمات پیش‌فرض در سطح واژه‌نامه';
$string['glossary:managecategories'] = 'مدیریت دسته‌ها';
$string['glossary:managecomments'] = 'مدیریت نظرات';
$string['glossary:manageentries'] = 'مدیریت اصطلاحات';
$string['glossary:rate'] = 'ارزیابی اصطلاحات';
$string['glossarytype'] = 'نوع واژه‌نامه';
$string['glossarytype_help'] = 'یک واژه‌نامه اصلی، واژه‌نامه‌ایی است که می‌توان اصطلاحات واژه‌نامه‌های فرعی را به آن وارد کرد. در هر درس فقط یک واژه‌نامه اصلی می‌تواند وجود داشته باشد. اگر قابلیت ورود اصطلاحات واژه‌نامه مورد نیاز نباشد، تمام واژه‌نامه‌های درس می‌توانند از نوع واژه‌نامه فرعی تعریف شوند.';
$string['glossary:view'] = 'مشاهدهٔ واژه‌نامه';
$string['glossary:viewallratings'] = 'مشاهدهٔ امتیازهای خامی توسط تک تک افراد داده شده است';
$string['glossary:viewanyrating'] = 'مشاهدهٔ امتیاز کل دریافتی همه';
$string['glossary:viewrating'] = 'مشاهدهٔ امتیاز کل دریافتی خود';
$string['glossary:write'] = 'تعریف اصطلاحات جدید';
$string['guestnoedit'] = 'مهمان‌ها مجاز به ویرایش واژه‌نامه‌ها نیستند';
$string['importcategories'] = 'وارد کردن دسته‌ها';
$string['importedcategories'] = 'دسته‌های وارد شده';
$string['importedentries'] = 'اصطلاحات وارد شده';
$string['importentries'] = 'ورود اصطلاحات';
$string['importentriesfromxml'] = 'ورود اصطلاحات از فایل XML';
$string['includegroupbreaks'] = 'شامل جداکنندهٔ گروه‌ها';
$string['isglobal'] = 'آیا این واژه‌نامه سراسری است؟';
$string['isglobal_help'] = 'یک واژه‌نامه سراسری شامل اصطلاحاتی است که از سرتاسر سایت (و نه فقط از درسی که واژه‌نامه در آن قرار دارد) به آن‌ها پیوند داده شده است. فقط مدیرها می‌توانند یک واژه‌نامه را به عنوان سراسری تعیین کنند.';
$string['letter'] = 'الفبا';
$string['linkcategory'] = 'پیوند دار کردن این دسته به صورت خودکار';
$string['linkcategory_help'] = 'اگر پیوند دهی خودکار واژه‌نامه فعال شده باشد و این هم گزینه فعال باشد، نام دسته در هر جایی از درس که ظاهر شود به صورت خودکار پیوند خواهد شد. هنگامی که کاربری بر روی پیوند نام دسته کلیک کند، به صفحهٔ «مرور بر اساس دسته» در واژه‌نامه خواهد رفت.';
$string['linking'] = 'پیوند دادن خودکار';
$string['mainglossary'] = 'واژه‌نامه اصلی';
$string['maxtimehaspassed'] = 'متأسفانه مهلت ویرایش این نظر ({$a}) به پایان رسیده است!';
$string['modulename'] = 'واژه‌نامه';
$string['modulename_help'] = 'ماژول فعالیت واژه‌نامه امکان ایجاد و نگهداری لیستی از تعاریف (مانند یک لغت‌نامه)، یا جمع‌آوری و سازماندهی منابع یا اطلاعات را برای شرکت کنندگان فراهم می‌کند.

استاد می‌تواند اجازه دهد که بتوان به اصطلاحات داخل واژه‌نامه فایل ضمیمه کرد. تصاویری که ضمیمه اصطلاحات شده باشند در کنار متن اصطلاح نمایش داده می‌شوند. اصطلاحات می‌توانند جستجو شوند یا بر مبنای حروف الفبا، طبقه، تاریخ یا نویسنده‌هایشان لیست شوند. اصطلاح‌ها می‌توانند به طور پیش‌فرض تایید شده باشند یا اینکه نیازمند تایید شدن توسط استاد باشند تا بتوانند توسط همه قابل مشاهده باشند.

اگر فیلتر پیوند دهی خودکار واژه‌نامه فعال باشد، هر گاه که اصطلاحات یا عبارت‌های درون واژه‌نامه در جایی از درس به کار روند، آن اصطلاحات و عبارت‌ها به طور خودکار به تعریفشان در واژه‌نامه پیوند خواهند شد.

استاد می‌تواند قابلیت نظر دادن در مورد اصطلاحات تعریف شده در واژه‌نامه را فعال کند. همچنین اصطلاحات تعریف شده در واژه‌نامه می‌توانند توسط استاد یا شاگردان (ارزشیابی همکلاسان از کار یکدیگر) امتیاز دهی شوند. امتیازها می‌توانند جمع‌آوری شوند تا یک نمره نهایی را تشکیل دهند که در دفتر نمره ثبت شود.

واژه‌نامه‌ها کاربردهای بسیاری دارند، مثلا

* یک بانک حاصل از کارمشارکتی از عبارت‌های کلیدی
* به عنوان یک محل «آشنایی با شما» که شاگردان جدید نام و معرفی خود را به آن اضافه می‌کنند
* به عنوان یک مجموعه «نکات مفید» شامل شیوه‌های مناسب عمل در موضوعات عملی
* محل به اشتراک گذاری فیلم‌ها، تصاویر یا فایل‌های صوتی مفید
* به عنوان یک مرجع قابل مرور از اصول برای به خاطر سپاری آنها';
$string['modulenameplural'] = 'واژه‌نامه‌ها';
$string['newentries'] = 'اصطلاحات جدید واژه‌نامه';
$string['newglossary'] = 'یک واژه‌نامه جدید';
$string['newglossarycreated'] = 'واژه‌نامه جدید ایجاد شد.';
$string['newglossaryentries'] = 'اصطلاحات واژه‌نامه جدید:';
$string['nocomment'] = 'نظری داده نشده است';
$string['nocomments'] = '(در مورد این اصطلاح نظری داده نشده است)';
$string['noconceptfound'] = 'اصطلاح یا تعریفی وجود ندارد.';
$string['noentries'] = 'اصطلاحی در این قسمت پیدا نشد';
$string['noentry'] = 'اصطلاحی وجود ندارد.';
$string['nopermissiontodelcomment'] = 'نمی‌توانید نظر سایر کاربران را پاک کنید!';
$string['nopermissiontodelinglossary'] = 'در این واژه‌نامه نمی‌توانید اظهار نظر کنید!';
$string['nopermissiontoviewresult'] = 'فقط می‌توانید نتایج اصطلاح‌هایی که خود تعریف کرده‌اید را ببینید';
$string['notapproved'] = 'اصطلاح هنوز تایید نشده است.';
$string['notcategorised'] = 'دسته‌بندی نشده';
$string['numberofentries'] = 'تعداد اصطلاحات';
$string['onebyline'] = '(در خطوط جداگانه)';
$string['page-mod-glossary-edit'] = 'صفحهٔ اضافه کردن/ویرایش داده‌های واژه‌نامه';
$string['page-mod-glossary-view'] = 'صفحهٔ مشاهدهٔ داده در واژه‌نامه';
$string['page-mod-glossary-x'] = 'هر صفحه‌ای از ماژول واژه‌نامه';
$string['pluginadministration'] = 'مدیریت واژه‌نامه';
$string['pluginname'] = 'واژه‌نامه';
$string['popupformat'] = 'قالب پنجره باز شونده';
$string['printerfriendly'] = 'نمایش نسخهٔ آمادهٔ چاپ';
$string['printviewnotallowed'] = 'نمایش نسخهٔ آمادهٔ چاپ مجاز نیست';
$string['question'] = 'سؤال';
$string['rejectedentries'] = 'اصطلاحات پذیرفته نشده';
$string['rejectionrpt'] = 'گزارش عدم پذیرش‌ها';
$string['resetglossaries'] = 'حذف اصطلاحات متعلق به';
$string['resetglossariesall'] = 'حذف اصطلاحات همهٔ واژه‌نامه‌ها';
$string['rssarticles'] = 'تعداد مقالات اخیر در RSS';
$string['rssarticles_help'] = 'این تنظیم، تعداد اصطلاحات واژه‌نامه که در RSS feed شامل می‌شوند را تعیین می‌کند. به طور کلی مقادیر بین ۵ و ۲۰ قابل قبول هستند.';
$string['rsssubscriberss'] = 'نمایش RSS feed برای اصطلاحات واژه‌نامه «{$a}»';
$string['rsstype'] = 'RSS feed مربوط به این فعالیت';
$string['rsstype_help'] = 'برای فعال کردن RSS feed برای این فعالیت، یکی از گزینه‌های «اصطلاحات به همراه تعریف کننده» یا «اصطلاحات بدون تعریف کننده» را برای شامل شدن در feed انتخاب کنید.';
$string['search:activity'] = 'واژه‌نامه - اطلاعات فعالیت';
$string['search:entry'] = 'واژه‌نامه - مدخل‌ها';
$string['searchindefinition'] = 'جستجو در متن کامل';
$string['secondaryglossary'] = 'واژه‌نامه فرعی';
$string['showall'] = 'نمایش پیوند «همه»';
$string['showall_help'] = 'در صورت فعال بودن، کاربران می‌توانند تمام اصلاحات را یکجا مرور کنند.';
$string['showalphabet'] = 'نمایش پیوندهای الفبا';
$string['showalphabet_help'] = 'در صورت فعال بودن، کاربران می‌توانند واژه‌نامه را بر اساس حروف الفبا مرور کنند.';
$string['showspecial'] = 'نمایش پیوند «حروف خاص»';
$string['showspecial_help'] = 'در صورت فعال بودن، کاربران می‌توانند واژه‌نامه را بر اساس حروف خاص مانند @ یا # مرور کنند.';
$string['sortby'] = 'مرتب شدن بر اساس';
$string['sortbycreation'] = 'بر اساس زمان تعریف';
$string['sortbylastupdate'] = 'بر اساس زمان آخرین تغییر';
$string['sortchronogically'] = 'ترتیب نمایش';
$string['special'] = 'حروف خاص';
$string['standardview'] = 'بر اساس الفبا';
$string['studentcanpost'] = 'شاگردان بتوانند اصطلاح تعریف کنند';
$string['totalentries'] = 'کل اصطلاحات';
$string['usedynalink'] = 'پیوند دادن خودکار به اصطلاحات واژه‌نامه';
$string['usedynalink_help'] = 'اگر قابلیت پیوند دهی خودکار توسط یک مدیر در سطح سایت فعال شده باشد و این گزینه هم فعال باشد، فرم «تعریف یک اصطلاح جدید» شامل گزینه‌ای برای فعال کردن پیوند دادن به اصطلاح مورد تعریف در هر جایی از درس که نام یا کلمات کلیدی اصطلاح ظاهر شده‌اند خواهد بود.';
$string['waitingapproval'] = 'در انتظار تایید';
$string['warningstudentcapost'] = '(فقط در صورتیکه واژه‌نامه «اصلی» نباشد اعمال می‌شود)';
$string['withauthor'] = 'اصطلاحات به همراه تعریف کننده';
$string['withoutauthor'] = 'اصطلاحات بدون تعریف کنده';
$string['writtenby'] = 'توسط';
$string['youarenottheauthor'] = 'شما این نظر را نداده‌اید، بنابراین مجاز به ویرایش آن نیستید.';
