<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'calendarsystem_jalali', language 'fa', branch 'MOODLE_26_STABLE'
 *
 * @package   calendarsystem_jalali
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['am'] = 'صبح';
$string['am_caps'] = 'صبح';
$string['month1'] = 'فروردین';
$string['month10'] = 'دی';
$string['month11'] = 'بهمن';
$string['month12'] = 'اسفند';
$string['month2'] = 'اردیبهشت';
$string['month3'] = 'خرداد';
$string['month4'] = 'تیر';
$string['month5'] = 'مرداد';
$string['month6'] = 'شهریور';
$string['month7'] = 'مهر';
$string['month8'] = 'آبان';
$string['month9'] = 'آذر';
$string['name'] = 'خورشیدی';
$string['pluginname'] = 'تقویم هجری شمسی';
$string['pm'] = 'عصر';
$string['pm_caps'] = 'عصر';
$string['weekday0'] = 'یکشنبه';
$string['weekday1'] = 'دوشنبه';
$string['weekday2'] = 'سه‌شنبه';
$string['weekday3'] = 'چهارشنبه';
$string['weekday4'] = 'پنج‌شنبه';
$string['weekday5'] = 'جمعه';
$string['weekday6'] = 'شنبه';
