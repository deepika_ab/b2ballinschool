<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignfeedback_editpdf', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignfeedback_editpdf
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['editpdf'] = 'یادداشت‌نویسی روی PDF';
$string['enabled'] = 'یادداشت‌نویسی روی PDF';
$string['filter'] = 'فیلتر کردن نظرها...';
$string['generatingpdf'] = 'در حال ایجاد PDF ...';
$string['green'] = 'سبز';
$string['line'] = 'خط';
$string['navigatenext'] = 'صفحهٔ بعد';
$string['navigateprevious'] = 'صفحهٔ قبل';
$string['oval'] = 'بیضی';
$string['pagenumber'] = 'صفحه {$a}';
$string['pagexofy'] = 'صفحه {$a->page} از {$a->total}';
$string['pathtogspathdesc'] = 'لطفا در نظر داشته باشید که برای یادداشت‌نویسی روی PDF، مسیر ghostscript باید در {$a} تعیین شده باشد.';
$string['pen'] = 'قلم';
$string['pluginname'] = 'یادداشت‌نویسی روی PDF';
$string['rectangle'] = 'مستطیل';
$string['searchcomments'] = 'جستجوی نظرها';
$string['select'] = 'انتخاب';
$string['viewfeedbackonline'] = 'مشاهده PDF تصحیح‌شده...';
