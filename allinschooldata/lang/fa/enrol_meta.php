<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_meta', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_meta
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addgroup'] = 'اضافه‌کردن به گروه';
$string['creategroup'] = 'ساختن گروه جدید';
$string['meta:config'] = 'پیکربندی نمونه‌های ثبت‌نام فراپیوند درس';
$string['meta:unenrol'] = 'لغو ثبت نام کاربران تعلیق شده';
$string['nosyncroleids'] = 'نقش‌هایی که همگام نمی‌شوند';
$string['nosyncroleids_desc'] = 'به طور پیش‌فرض تمام انتساب‌های نقش صورت گرفته در سطح درس از درس‌های والد به فرزند همگام می‌شوند. نقش‌هایی که اینجا انتخاب شده‌اند در فرآیند همگام‌سازی شامل نخواهند شد. نقش‌های موجود برای همگام‌سازی به هنگام اجرای بعدی cron به‌روز خواهند شد.';
$string['pluginname'] = 'فراپیوند درس';
$string['pluginname_desc'] = 'پلاگین ثبت نام فراپیوند درس، ثبت نام‌ها و نقش‌های دو درس مجزا را با یکدیگر همگام می‌کند.';
$string['syncall'] = 'همگام کردن تمام کاربران ثبت نام شده';
$string['syncall_desc'] = 'اگر فعال شده باشد تمام کاربران ثبت نام شده همگام خواهند شد، حتی اگر در درس والد نقشی نداشته باشند. ولی اگر غیر فعال باشد تنها کاربرانی که حداقل یک نقش همگام‌شده داشته باشد در درس فرزند ثبت نام می‌شود.';
