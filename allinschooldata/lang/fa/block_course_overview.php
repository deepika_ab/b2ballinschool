<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_course_overview', language 'fa', branch 'MOODLE_37_STABLE'
 *
 * @package   block_course_overview
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityoverview'] = 'شما {$a}هایی دارید که نیازمند توجه هستند';
$string['addfavourite'] = 'اضافه کردن موردعلاقه‌ها';
$string['alwaysshowall'] = 'همیشه همه درس‌ها نشان داده شوند';
$string['collapseall'] = 'بستن همه لیست‌های درسی';
$string['course_overview:addinstance'] = 'اضافه‌کردن یک بلوک جدید مرور درس';
$string['course_overview:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید مرور درس به میز کار';
$string['courses'] = 'درس‌ها';
$string['defaulttab'] = 'برگه پیش‌فرض';
$string['expandall'] = 'بازکردن همه لیست‌های درسی';
$string['favourites'] = 'مورد علاقه‌ها';
$string['fullpath'] = 'همه طبقه‌ها و زیر طبقه‌ها';
$string['help'] = 'راهنما';
$string['makefavourite'] = 'موردعلاقه خود قرار دهید';
$string['message'] = 'پیام';
$string['messages'] = 'پیام‌ها';
$string['morecoursestext'] = 'لیست کردن درس‌های بیشتر';
$string['movecourse'] = 'انتقال درس: {$a}';
$string['movecoursehere'] = 'انتقال درس به اینجا';
$string['movetofirst'] = 'انتقال درس {$a} به بالا';
$string['nextpage'] = 'صفحه بعد';
$string['nocourses'] = 'هیچ درسی برای نمایش موجود نیست';
$string['none'] = 'هیچ';
$string['numtodisplay'] = 'تعداد درس‌هایی که نشان داده می‌شوند:';
$string['onlyparentname'] = 'فقط طبقه والد';
$string['prevpage'] = 'صفحه قبل';
$string['privacy:metadata:numberofcourses'] = 'تعداد درس‌های نمایش داده شده';
$string['privacy:metadata:sortorder'] = '‌ترتیب مرتب‌سازی';
$string['removefavourite'] = 'حذف از موردعلاقه‌ها';
$string['reorderfullname'] = 'نام کامل درس';
$string['reorderid'] = 'ID درس';
$string['reorderiddesc'] = 'توضیح ID درس';
$string['reordershortname'] = 'نام کوتاه درس';
$string['setmaxcourses'] = 'تنظیم حداکثر تعداد درس‌ها';
$string['shortnameprefix'] = 'شامل {$a}';
$string['sortorder'] = 'ترتیب مرتب‌سازی';
$string['unfavourite'] = 'حذف مورد علاقه';
$string['usersetmaxcourses'] = 'تعداد درس‌ها برای نمایش';
$string['view_edit_profile'] = '(نمایش و ویرایش مشخصات فردی)';
$string['welcome'] = 'خوش آمدید {$a}';
$string['youhavemessages'] = 'شما {$a} پیام نخوانده دارید';
$string['youhavenomessages'] = 'شما پیام خوانده نشده‌ای، ندارید';
