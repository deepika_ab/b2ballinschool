<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'cache', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   cache
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'اقدامات';
$string['addinstance'] = 'اضافه‌کردن نمونه';
$string['addstore'] = 'اضافه‌کردن انبارهٔ {$a}';
$string['addstoresuccess'] = 'یک انبارهٔ جدید {$a} اضافه شد.';
$string['area'] = 'ناحیه';
$string['cacheadmin'] = 'مدیریت cache';
$string['cacheconfig'] = 'پیکربندی';
$string['cachedef_calendar_subscriptions'] = 'آبونمان‌های تقویم';
$string['cachedef_capabilities'] = 'لیست توانایی‌های سیستم';
$string['cachedef_completion'] = 'وضعیت تکمیل فعالیت';
$string['cachedef_config'] = 'تنظیمات پیکربندی';
$string['cachedef_coursecat'] = 'لیست طبقه‌های درسی برای کاربر خاص';
$string['cachedef_coursecatrecords'] = 'رکوردهای طبقه‌های درسی';
$string['cachedef_coursecattree'] = 'درخت طبقه‌های درسی';
$string['cachedef_coursecontacts'] = 'لیست مسئولین درس';
$string['cachedef_externalbadges'] = 'مدال‌های خارجی برای کاربر خاص';
$string['cachedef_grade_categories'] = 'کوئری‌های طبقهٔ نمره';
$string['cachedef_groupdata'] = 'اطلاعات گروه درس';
$string['cachedef_langmenu'] = 'لیست زبان‌های موجود';
$string['cachedef_string'] = 'cache عبارت زبانی';
$string['cachedef_suspended_userids'] = 'لیست کاربران تعلیق‌شده بازای هر درس';
$string['cachedef_yuimodules'] = 'تعریف‌های ماژول‌های YUI';
$string['cachelock_file_default'] = 'پیش‌فرض قفل‌کردن';
$string['cachestores'] = 'انباره‌های cache';
$string['caching'] = 'cache کردن';
$string['component'] = 'کامپوننت';
$string['defaultmappings'] = 'انباره‌های مورد استفاده هنگامی که هیچ نگاشتی موجود نیست';
$string['definition'] = 'تعریف';
$string['deletelock'] = 'پاک کردن قفل';
$string['deletelockconfirmation'] = 'آیا مطمئن هستید که می‌خواهید قفل «{$a}» را حذف کنید؟';
$string['deletelockhasuses'] = 'شما نمی‌توانید این قفل را پاک کنید زیرا توسط یک یا چند انباره در حال استفاده است.';
$string['deletelocksuccess'] = 'قفل با موفقیت پاک شد';
$string['deletestore'] = 'پاک کردن انباره';
$string['deletestoreconfirmation'] = 'آیا مطمئن هستید که می‌خواهید انبارهٔ «{$a}» را حذف کنید؟';
$string['deletestoresuccess'] = 'انبارهٔ cache با موفقیت پاک شد';
$string['editdefinitionmappings'] = 'نگاشت‌های انباره تعریف {$a}';
$string['editstore'] = 'ویرایش انباره';
$string['editstoresuccess'] = 'انبارهٔ cache با موفقیت ویرایش شد.';
$string['invalidplugin'] = 'پلاگین نامعتبر';
$string['invalidstore'] = 'انبارهٔ cache نامعتبری فراهم شده است';
$string['lockdefault'] = 'پیش‌فرض';
$string['locking'] = 'قفل‌کردن';
$string['locking_help'] = 'قفل‌کردن مکانیزمی است که به‌منظور جلوگیری از بازنویسی شدن داده‌ها، دسترسی به داده‌های cache شده را به یک پروسه در آنِ واحد محدود می‌کند. نحوهٔ قفل کردن تعیین می‌کند که قفل چطور کسب می‌شود و بررسی می‌شود.';
$string['lockname'] = 'نام';
$string['locknamedesc'] = 'نام باید غیرتکراری باشد و تنها می‌تواند از حروف <span dir="ltr" style="direction:ltr; display: inline-block;">a-zA-Z_</span> تشکیل شود.';
$string['locknamenotunique'] = 'نامی که انتخاب کرده‌اید غیرتکراری نیست. لطفا یک نام غیرتکراری انتخاب کنید.';
$string['mappingdefault'] = '(پیش‌فرض)';
$string['mappingprimary'] = 'انبارهٔ اصلی';
$string['mode'] = 'روش';
$string['mode_1'] = 'برنامه';
$string['mode_2'] = 'نشست';
$string['mode_4'] = 'درخواست';
$string['modes'] = 'روش‌ها';
$string['plugin'] = 'پلاگین';
$string['pluginsummaries'] = 'انباره‌های cache نصب‌شده';
$string['purge'] = 'پاک‌سازی';
$string['purgedefinitionsuccess'] = 'تعریف مورد نظر با موفقیت پاک‌سازی شد.';
$string['purgestoresuccess'] = 'انبارهٔ مورد درخواست با موفقیت پاک‌سازی شد.';
$string['requestcount'] = 'آزمایش با تعداد {$a} درخواست';
$string['rescandefinitions'] = 'پویش مجدد تعاریف';
$string['result'] = 'نتیجه';
$string['storeconfiguration'] = 'پیکربندی انباره';
$string['storename'] = 'نام انباره';
$string['storenamealreadyused'] = 'باید یک نام غیرتکراری برای این انباره انتخاب کنید.';
$string['storename_help'] = 'این گزینه نام انباره را تعیین می‌کند. از این نام برای شناسایی انباره در سیستم استفاده می‌شود و تنها می‌تواند شامل حروف <span dir="ltr" style="direction:ltr; display: inline-block;">a-zA-Z_</span> و فاصله باشد. این نام همچنین باید غیر تکراری باشد. اگر بخواهید از نامی استفاده کنید که قبلا استفاده شده است، خطایی دریافت خواهید کرد.';
$string['storenameinvalid'] = 'نام انباره نامعتبر است. تنها می‌توانید از <span dir="ltr" style="direction:ltr; display: inline-block;">a-zA-Z_</span> و فاصله استفاده کنید.';
$string['storeready'] = 'آماده';
$string['stores'] = 'انباره‌ها';
$string['storesummaries'] = 'نمونه انباره‌های پیکربندی‌شده';
$string['supports'] = 'پشتیبانی می‌کند';
$string['tested'] = 'آزمایش شد';
$string['testperformance'] = 'آزمایش کارایی';
$string['unsupportedmode'] = 'روش پشتیبانی‌نشده';
