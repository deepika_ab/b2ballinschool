<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_timezoneimport', language 'fa', branch 'MOODLE_29_STABLE'
 *
 * @package   tool_timezoneimport
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configintrotimezones'] = 'این صفحه به جستجوی اطلاعات جدید درباره مناطق زمانی دنیا (به همراه قوانین تغییر ساعت تابستانی) می‌پردازد و پایگاه داده شما را با این اطلاعات به‌روز خواهد کرد. محل‌های زیر به ترتیب بررسی می‌شوند: {$a} این فرآیند معمولا بسیار بی خطر است و نمی‌تواند نصب‌های معمول را مختل کند. آیا مایلید مناطق زمانی خود را به‌روز نمائید؟';
$string['importtimezones'] = 'به‌روزرسانی لیست کامل مناطق زمانی';
$string['importtimezonescount'] = '{$a->count} entries imported from {$a->source}';
$string['importtimezonesfailed'] = 'No sources found! (Bad news)';
$string['updatetimezones'] = 'به‌روزرسانی مناطق زمانی';
