<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'install', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   install
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['admindirerror'] = 'دایرکتوری مدیر تعیین شده اشتباه است';
$string['admindirname'] = 'دایرکتوری مدیر';
$string['admindirsetting'] = 'وب‌سایت‌های خیلی کمی از <span dir="ltr" style="display:inline-block;direction:ltr">/admin</span> به‌عنوان پیوند ویژه‌ای برای دستیابی به یک
control panel یا چیز دیگری استفاده می‌کنند. متأسفانه این مسئله با
محل استاندارد صفحه‌های مدیر در مودل تداخل دارد. این مشکل را می‌توانید
با تغییر نام دایرکتوری admin در فایل‌های نصب و قرار دادن
نام جدید در این قسمت برطرف نمائید. به‌عنوان مثال: <br /> <br /><b>moodleadmin</b><br /> <br />
این کار پیوندهای مدیر در مودل را اصلاح خواهد کرد.';
$string['admindirsettinghead'] = 'تعیین دایرکتوری مدیر ...';
$string['admindirsettingsub'] = 'وب‌سایت‌های خیلی کمی از <span dir="ltr" style="display:inline-block;direction:ltr">/admin</span> به‌عنوان پیوند ویژه‌ای برای دستیابی به یک
control panel یا چیز دیگری استفاده می‌کنند. متأسفانه این مسئله با
محل استاندارد صفحه‌های مدیر در مودل تداخل دارد. این مشکل را می‌توانید
با تغییر نام دایرکتوری admin در فایل‌های نصب و قرار دادن
نام جدید در این قسمت برطرف نمائید. به‌عنوان مثال: <br /> <br /><b>moodleadmin</b><br /> <br />
این کار پیوندهای مدیر در مودل را اصلاح خواهد کرد.';
$string['availablelangs'] = 'بسته‌های زبانی موجود';
$string['caution'] = 'اخطار';
$string['chooselanguage'] = 'انتخاب زبان';
$string['chooselanguagehead'] = 'انتخاب زبان';
$string['chooselanguagesub'] = 'لطفاً زبانی را به جهت استفاده در حین نصب انتخاب نمایید. زبانی که در این صفحه انتخاب می‌کنید به عنوان زبان پیش‌فرض سایت نیز مورد استفاده قرار خواهد گرفت. البته می‌توانید بعداً آن را تغییر دهید.<br />ترجمهٔ فارسی این نسخه با همکاری <a href="http://foodle.org" target="_blank">گروه فودل</a> آماده شده است.';
$string['cliadminemail'] = 'آدرس پست الکترونیک کاربر مدیر جدید';
$string['cliadminpassword'] = 'ﺮﯾﺪﻣ ﺪﯾﺪﺟ ﺏﺎﺴﺣ ﺩﻭﺭﻭ ﺰﻣﺭ';
$string['cliadminusername'] = 'ﺖﯾﺎﺳ ﺮﯾﺪﻣ ﯼﺮﺑﺭﺎﮐ ﻡﺎﻧ';
$string['clialreadyconfigured'] = '.ﺩﺭﺍﺩ ﺩﻮﺟﻭ config.php ﯼﺪﻨﺑﺮﮑﯿﭘ ﻞﯾﺎﻓ
ﺯﺍ ﺖﯾﺎﺳ ﻦﯾﺍ ﺭﺩ ﻝﺩﻮﻣ ﺐﺼﻧ ﯼﺍﺮﺑ ﺎﻔﻄﻟ
.ﺪﯿﻨﮐ ﻩﺩﺎﻔﺘﺳﺍ admin/cli/database_install.php';
$string['clialreadyinstalled'] = '.ﺩﺭﺍﺩ ﺩﻮﺟﻭ config.php ﯼﺪﻨﺑﺮﮑﯿﭘ ﻞﯾﺎﻓ
ﺯﺍ ﺖﯾﺎﺳ ﻦﯾﺍ ﺭﺩ ﻝﺩﻮﻣ ﯼﺎﻘﺗﺭﺍ ﯼﺍﺮﺑ ﺎﻔﻄﻟ
.ﺪﯿﻨﮐ ﻩﺩﺎﻔﺘﺳﺍ admin/cli/database_install.php';
$string['cliinstallfinished'] = '.ﺪﯿﺳﺭ ﻥﺎﯾﺎﭘ‌ﻪﺑ ﺖﯿﻘﻓﻮﻣ ﺎﺑ ﺐﺼﻧ';
$string['cliinstallheader'] = 'ﻥﺎﻣﺮﻓ ﻂﺧ ﻖﯾﺮﻃ ﺯﺍ {$a} ﻝﺩﻮﻣ ﺐﺼﻧ ﻪﻣﺎﻧﺮﺑ';
$string['climustagreelicense'] = 'ﺪﯾﺎﺑ ﺍﺭ ﺯﻮﺠﻣ ﺎﺑ ﺩﻮﺧ ﺖﻘﻓﺍﻮﻣ ،ﯽﻠﻣﺎﻌﺗ ﺮﯿﻏ ﺖﻟﺎﺣ ﺭﺩ
ﺪﯿﻨﮐ ﻡﻼﻋﺍ --agree-license ﺮﺘﻣﺍﺭﺎﭘ ﻥﺩﺮﮐ ﺺﺨﺸﻣ ﺎﺑ';
$string['cliskipdatabase'] = '.ﻩﺩﺍﺩ ﻩﺎﮕﯾﺎﭘ ﺐﺼﻧ ﺯﺍ ﻥﺩﺮﮐ ﺮﻈﻧ‌ﻑﺮﺻ';
$string['clitablesexist'] = '.ﺪﻨﺘﺷﺍﺩ ﺩﻮﺟﻭ ﻞﺒﻗ ﺯﺍ ﻩﺩﺍﺩ ﻩﺎﮕﯾﺎﭘ ﯼﺎﻫ‌ﻝﻭﺪﺟ
.ﺪﺑﺎﯾ ﻪﻣﺍﺩﺍ ﺪﻧﺍﻮﺗ‌ﯽﻤﻧ cli ﺐﺼﻧ';
$string['compatibilitysettings'] = 'بررسی تنظیمات PHP شما ...';
$string['compatibilitysettingshead'] = 'بررسی تنظیمات PHP شما ...';
$string['compatibilitysettingssub'] = 'کارگزار شما باید همهٔ آزمایش‌های زیر را با موفقیت پشت سر بگذراند تا بتواند مودل را درست اجرا کند';
$string['configfilenotwritten'] = 'برنامهٔ نصب، احتمالاً به دلیل قابل نوشتن نبودن دایرکتوری مودل، قادر به ایجاد فایل config.php شامل تنظیمات انتخابی شما نبود. می‌توانید کد زیر را به صورت دستی در فایلی با نام config.php در داخل دایرکتوری اصلی مودل کپی نمائید.';
$string['configfilewritten'] = 'config.php با موفقیت ایجاد شد';
$string['configurationcomplete'] = 'پیکربندی پایان یافت';
$string['configurationcompletehead'] = 'پیکربندی به اتمام رسید';
$string['configurationcompletesub'] = 'مودل تلاش کرد که پیکربندی شما را در فایلی در محل نصب مودل شما ذخیره کند.';
$string['database'] = 'پایگاه داده';
$string['databasehead'] = 'تنظیمات پایگاه داده';
$string['databasehost'] = 'میزبان پایگاه داده';
$string['databasename'] = 'نام پایگاه داده';
$string['databasepass'] = 'رمز اتصال به پایگاه داده';
$string['databaseport'] = 'پورت پایگاه داده';
$string['databasesocket'] = 'سوکت یونیکس';
$string['databasetypehead'] = 'راه‌انداز پایگاه داده را انتخاب کنید';
$string['databasetypesub'] = 'مودل از انواع مختلف کارگزارهای پایگاه داده پشتیبانی می‌کند. اگر نمی‌دانید که کدام گزینه را انتخاب کنید، لطفا با مسئول کارگزار خود تماس بگیرید.';
$string['databaseuser'] = 'کاربر پایگاه داده';
$string['dataroot'] = 'دایرکتوری داده';
$string['datarooterror'] = '«دایرکتوری داده»ای که تعیین نموده‌اید موجود یا قابل ایجاد شدن نیست. یا مسیر وارد شده را تصحیح نمائید و یا دایرکتوری مورد نظر را به صورت دستی ایجاد نمائید.';
$string['datarootpermission'] = 'ﺎﻫ‌ﻩﺩﺍﺩ ِﯼﺭﻮﺘﮐﺮﯾﺍﺩ ﺯﻮﺠﻣ';
$string['datarootpublicerror'] = '«دایرکتوری داده»ای که تعیین کرده‌اید مستقیماً از طریق وب قابل دستیابی است. باید از دایرکتوری دیگری استفاده نمائید.';
$string['dbconnectionerror'] = 'اتصال به پایگاه داده ای که مشخص نموده‌اید امکان‌پذیر نبود. لطفاً تنظیمات پایگاه دادهٔ خود را بررسی نمائید.';
$string['dbcreationerror'] = 'خطای ایجاد پایگاه داده. پایگاه دادهٔ مورد نظر با تنظیمات تعیین شده ایجاد نشد.';
$string['dbhost'] = 'کارگزار میزبان';
$string['dbpass'] = 'رمز اتصال';
$string['dbport'] = 'پورت';
$string['dbprefix'] = 'پیشوند جدول‌ها';
$string['dbtype'] = 'نوع';
$string['directorysettings'] = '<p>لطفاً محل‌های این نصب مودل را تأیید نمائید.</p>

<p><b>آدرس وب:</b>
آدرس وب کامل دسترسی به مودل را مشخص نمائید.
اگر وب‌سایت شما از طریق چند URL قابل دستیابی است، بدیهی‌ترینی
که شاگردهای شما ممکن است استفاده کنند را انتخاب نمائید. در انتهای
آدرس، نشان ممیز (slash) قرار ندهید.</p>

<p><b>دایرکتوری مودل:</b>
مسیر کامل فایل‌های مودل را تعیین نمائید.
مطمئن شوید که بزرگی/کوچکی حروف را رعایت کرده باشید.</p>

<p><b>دایرکتوری داده:</b>
شما به محلی نیاز دارید که مودل بتواند فایل‌های ارسالی را آن‌جا ذخیره کند.
این دایرکتوری باید توسط کاربر کارگزار وب قابل خواندن و نوشتن باشد
(معمولاً «nobody» یا «apache»)، ولی نباید مستقیماً از طریق وب
قابل دستیابی باشد.</p>';
$string['directorysettingshead'] = 'لطفاً محل‌های این نصب مودل را تأیید نمائید';
$string['directorysettingssub'] = '<b>آدرس وب:</b>
آدرس وب کامل دسترسی به مودل را مشخص نمائید.
اگر وب‌سایت شما از طریق چند URL قابل دستیابی است، بدیهی‌ترینی
که شاگردهای شما ممکن است استفاده کنند را انتخاب نمائید. در انتهای
آدرس، نشان ممیز (slash) قرار ندهید.
<br />
<br />
<b>دایرکتوری مودل:</b>
مسیر کامل فایل‌های مودل را تعیین نمائید.
مطمئن شوید که بزرگی/کوچکی حروف را رعایت کرده باشید.
<br />
<br />
<b>دایرکتوری داده:</b>
شما به محلی نیاز دارید که مودل بتواند فایل‌های ارسالی را آن‌جا ذخیره کند.
این دایرکتوری باید توسط کاربر کارگزار وب قابل خواندن و نوشتن باشد
(معمولاً «nobody» یا «apache»)، ولی نباید مستقیماً از طریق وب
قابل دستیابی باشد.';
$string['dirroot'] = 'دایرکتوری مودل';
$string['dirrooterror'] = 'مقدار تعیین شده برای «دایرکتوری مودل» اشتباه به نظر می‌رسد - در آن محل فایل‌های مودل وجود ندارد. مقدار زیر مجدداً تنظیم شد.';
$string['download'] = 'دریافت فایل';
$string['downloadlanguagebutton'] = 'دریافت بستهٔ زبانی «{$a}»';
$string['downloadlanguagehead'] = 'دریافت بستهٔ زبانی';
$string['downloadlanguagenotneeded'] = 'فرآیند نصب را می‌توانید با استفاده از زبان پیش‌فرض ({$a}) ادامه دهید.';
$string['downloadlanguagesub'] = 'هم‌اکنون می‌توانید یک بستهٔ زبانی دریافت کنید و فرآیند نصب را با این زبان ادامه دهید.<br /><br />اگر قادر به دریافت بستهٔ زبانی نیستید، فرآیند نصب به زبان انگلیسی ادامه خواهد یافت. (وقتی که مراحل نصب به اتمام رسید، امکان دریافت و نصب بسته‌های زبانی اضافی را خواهید داشت.)';
$string['doyouagree'] = 'آیا موافق هستید؟ (بله/خیر)';
$string['environmenthead'] = 'بررسی محیط شما ...';
$string['environmentsub'] = 'بررسی اینکه قسمت‌های مختلف سیستم شما شرایط لازم را داشته باشد';
$string['environmentsub2'] = 'هر کدام از انتشارهای مودل حداقل نیازمندی مخصوص به خود را در مورد نسخهٔ PHP‌ و وجود داشتن برخی از افزونه‌های PHP دارد.
پیش از هر نصب و ارتقا، بررسی کامل محیط انجام می‌شود. اگر نمی‌دانید چطور نسخهٔ جدید PHP را نصب کنید یا افزونه‌های PHP را فعال کنید، لطفا با مسئول کارگزار خود تماس بگیرید.';
$string['errorsinenvironment'] = 'بررسی محیط ناموفق بود!';
$string['fail'] = 'ناموفق';
$string['fileuploads'] = 'ارسال فایل';
$string['fileuploadserror'] = 'باید فعال باشد';
$string['fileuploadshelp'] = '<p>ارسال فایل روی کارگزار شما غیرفعال به نظر می‌رسد.</p>

<p>مودل هنوز هم می‌تواند نصب شود، ولی بدون این قابلیت، قادر به ارسال
فایل‌های درسی یا عکس‌های کاربران جدید نخواهید بود.</p>

<p>برای فعال کردن ارسال فایل، شما (یا مدیر سیستم شما) باید
فایل php.ini اصلی کارگزار خود را ویرایش نمائید و مقدار
<b>file_uploads</b> را به 1 تغییر دهید.</p>';
$string['inputdatadirectory'] = 'دایرکتوری داده:';
$string['inputwebadress'] = 'آدرس وب:';
$string['inputwebdirectory'] = 'دایرکتوری مودل:';
$string['installation'] = 'در حال نصب';
$string['langdownloaderror'] = 'متأسفانه زبان «{$a}» نصب نشد. فرآیند نصب به زبان انگلیسی ادامه خواهد یافت.';
$string['langdownloadok'] = 'زبان «{$a}» با موفقیت نصب شد. فرآیند نصب به این زبان ادامه خواهد یافت.';
$string['memorylimit'] = 'محدودیت حافظه';
$string['memorylimiterror'] = 'حد حافظهٔ PHP واقعاً کم تعیین شده است ... ممکن است بعداً با مشکلاتی مواجه شوید.';
$string['memorylimithelp'] = '<p>حد حافظهٔ PHP کارگزار شما هم‌اکنون {$a}  است.</p>

<p>این مسئله ممکن است در آینده سبب بروز مشکلات مربوط به حافظه برای مودل شود،
به‌خصوص اگر شما ماژول‌های فعال و/یا کاربران زیادی داشته باشید.</p>

<p>توصیه می‌کنیم که در صورت امکان PHP را با مقدار بیشتری مثل 40M پیکربندی نمائید.
روش‌های متعددی برای انجام این کار وحود دارد که می‌توانید آن‌ها را امتحان نمائید:</p>
<ol>
<li>اگر می‌توانید، PHP را با <span dir="ltr"><i>--enable-memory-limit</i></span> مجدداً compile نمائید.
این کار به مودل اجازه خواهد داد که حد حافظه را خودش تعیین کند.</li>
<li>اگر به فایل php.ini خود دسترسی دارید، می‌توانید مقدار <b>memory_limit</b>
را به چیزی مثل 40M تغییر دهید. اگر دسترسی ندارید، می‌توانید
از مدیر کارگزار خود بخواهید که این کار را برای شما انجام دهد.</li>
<li>در بعضی از کارگزارهای PHP شما می‌توانید یک فایل <span dir="ltr">.htaccess</span> محتوی خط زیر
در دایرکتوری مودل ایجاد کنید:
    <blockquote><div>php_value memory_limit 40M</div></blockquote>
<p>اگرچه، در برخی از کارگزارها انجام این کار موجب جلوگیری از کارکردن <b>همهٔ</b> صفحه‌های PHP خواهد شد
(هنگام مشاهدهٔ صفحه‌ها خطاهایی خواهید دید) و مجبور خواهید بود که فایل <span dir="ltr">.htaccess</span> را پاک کنید.</p></li>
</ol>';
$string['mysqliextensionisnotpresentinphp'] = 'PHP با افزونهٔ MySQLi به‌نحو مناسب پیکربندی نشده است تا بتواند با MySQL ارتباط برقرار کند. لطفاً فایل php.ini خود را بررسی نمائید یا PHP را مجدداً compile کنید.';
$string['nativemariadb'] = 'MariaDB ـ (native/mariadb)';
$string['nativemariadbhelp'] = '<p>پایگاه داده جایی است که اکثر تنظیمات مودل و اطلاعات در نگهداری می‌شود و باید اینجا پیکربندی شود.</p>
<p>نام پایگاه داده، نام کاربری، و رمز اتصالش فیلدهای اجباری هستند؛ پیشوند جدول اختیاری است.</p>
<p>نام پایگاه داده تنها می‌تواند از حروف الفبایی و عددی لاتین، علامت دلار ($) و خط زیرین (_) تشکیل شود.</p>
<p>اگر پایگاه داده هم‌اکنون وجود نداشته باشد، و کاربری که مشخص کرده‌اید مجوز لازم را داشته باشد، مودل اقدام به ساختن یک پایگاه دادهٔ جدید با مجوزها و تنظیمات صحیح خواهد کرد.</p>
<p>این راه‌انداز با موتور قدیمی MyISAM سازگار نیست.</p>';
$string['nativemysqli'] = 'MySQL بهبود یافته (native/mysqli)';
$string['nativemysqlihelp'] = '<p>پایگاه داده جایی است که اکثر تنظیمات و داده‌های مودل در آنجا نگهداری می‌شود و باید اینجا پیکربندی شود.</p>
<p>فیلدهای نام پایگاه داده، نام کاربری و رمز اتصال اجباری هستند؛ فیلد پیشوند جدول اختیاری است.</p>
<p>نام پایگاه داده تنها می‌تواند شامل کاراکترهای انگلیسی الفبایی و عددی، علامت دلار ($)
 و علامت زیرخط (_) باشد.</p>
<p>اگر پایگاه داده وجود نداشته باشد و کاربری که وارد کرده‌اید مجوز لازم را داشته باشد، مودل سعی می‌کند که یک پایگاه دادهٔ جدید با دسترسی‌ها و تنظیمات صحیح ایجاد کند.</p>';
$string['nativeoci'] = 'Oracle ـ (native/oci)';
$string['nativeocihelp'] = 'حال باید پایگاه داده (جایی که اکثر داده‌های مودل در آن نگهداری می‌شود) را پیکربندی کنید.
این پایگاه داده باید از قبل ساخته شده باشد و یک نام کاربری و رمز اتصال برای دسترسی به آن تعریف شده باشد. پیشوند جدول اجباری است.';
$string['nativepgsql'] = 'PostgreSQL _ (native/pgsql)';
$string['nativepgsqlhelp'] = '<p>پایگاه داده جایی است که اکثر تنظیمات و داده‌های مودل در آنجا نگهداری می‌شود و باید اینجا پیکربندی شود.</p>
<p>فیلدهای نام پایگاه داده، نام کاربری، رمز اتصال و پیشوند جدول اجباری هستند.</p>
<p>پایگاه داده باید از قبل ساخته شده باشد و کاربر تعیین‌شده باید دسترسی خواندن و نوشتن روی آن را داشته باشد.</p>';
$string['nativesqlsrv'] = 'SQL*Server Microsoft _ (native/sqlsrv)';
$string['nativesqlsrvhelp'] = 'حال باید پایگاه داده (جایی که اکثر داده‌های مودل در آن نگهداری می‌شود) را پیکربندی کنید.
این پایگاه داده باید از قبل ساخته شده باشد و یک نام کاربری و رمز اتصال برای دسترسی به آن تعریف شده باشد. پیشوند جدول اجباری است.';
$string['nativesqlsrvnodriver'] = 'درایورهای مایکروسافت برای SQL Server برای PHP نصب نیستند یا اینکه به‌درستی پیکربندی نشده‌اند.';
$string['ociextensionisnotpresentinphp'] = 'PHP با افزونهٔ OCI8 به‌نحو مناسب پیکربندی نشده است تا بتواند با Oracle ارتباط برقرار کند. لطفاً فایل php.ini خود را بررسی نمائید یا PHP را مجدداً compile کنید.';
$string['pass'] = 'موفق';
$string['paths'] = 'مسیرها';
$string['pathserrcreatedataroot'] = 'دایرکتوری داده (<string dir="ltr" style="direction:ltr;display:inline-block;">{$a->dataroot}</span>) نمی‌تواند توسط برنامهٔ نصب ایجاد شود.';
$string['pathshead'] = 'تایید مسیرها';
$string['pathsrodataroot'] = 'دایرکتوری داده قابل نوشتن نیست.';
$string['pathsroparentdataroot'] = 'دایرکتوری مادر (<string dir="ltr" style="direction:ltr;display:inline-block;">{$a->parent}</span>) قابل نوشتن نیست. دایرکتوری داده (<string dir="ltr" style="direction:ltr;display:inline-block;">{$a->dataroot}</span>) نمی‌تواند توسط برنامهٔ نصب ایجاد شود.';
$string['pathssubadmindir'] = 'وب‌سایت‌های خیلی کمی از <span dir="ltr" style="display:inline-block;direction:ltr">/admin</span> به‌عنوان پیوند ویژه‌ای برای دستیابی به یک
control panel یا چیز دیگری استفاده می‌کنند. متأسفانه این مسئله با
محل استاندارد صفحه‌های مدیر در مودل تداخل دارد. این مشکل را می‌توانید
با تغییر نام دایرکتوری admin در فایل‌های نصب و قرار دادن
نام جدید در این قسمت برطرف نمائید. به‌عنوان مثال: <em>moodleadmin</em>. این کار پیوندهای مدیر در مودل را اصلاح خواهد کرد.';
$string['pathssubdataroot'] = '<p>دایرکتوری‌ای مودل تمام فایل‌هایی که توسط کاربران ارسال می‌شود را نگهداری می‌کند.</p>
<p>این دایرکتوری باید توسط کاربر کارگزار وب (معمولا www-data یا nobody یا apache) هم قابل خواندن و هم قابل نوشتن باشد.</p>
<p>این دایرکتوری نباید مستقیما بر روی وب قابل دسترسی باشد.</p>
<p>اگر دایرکتوری در حال حاضر وجود نداشته باشد، فرایند نصب سعی می‌کند که آن را بسازد.</p>';
$string['pathssubdirroot'] = '<p>مسیر کامل دایرکتوری‌ای که محتوی کد مودل است.</p>';
$string['pathssubwwwroot'] = '<p>آدرس کامل دسترسی به مودل؛ یعنی آدرسی که کاربران برای دسترسی به مودل در نوار آدرس مرورگرشان وارد می‌کنند.</p>
<p>دسترسی به مودل از طریق چند آدرس امکان‌پذیر نیست. اگر سایت شما توسط آدرس‌های مختلفی قابل دسترسی است آنگاه ساده‌ترین آنها را انتخاب کنید و تمام آدرس‌های دیگر را روی آن ریدایرکت کنید (permanent redirect).</p>
<p>اگر سایت شما هم از طریق اینترنت و هم از طریق شبکهٔ محلی (که گاهی به آن اینترانت هم می‌گویند) قابل دسترسی است، آنگاه آدرس عمومی را در این قسمت استفاده کنید.</p>
<p>اگر آدرس فعلی درست نیست، لطفا آدرس را در نوار آدرس مرورگر خود تغییر دهید و فرایند نصب را از اول شروع کنید.</p>';
$string['pathsunsecuredataroot'] = 'محل دایرکتوری داده امن نیست';
$string['pathswrongadmindir'] = 'دایرکتوری مدیر وجود ندارد';
$string['pgsqlextensionisnotpresentinphp'] = 'PHP با افزونهٔ PGSQL به‌نحو مناسب پیکربندی نشده است تا بتواند با PostgreSQL ارتباط برقرار کند. لطفاً فایل php.ini خود را بررسی نمائید یا PHP را مجدداً compile کنید.';
$string['phpextension'] = 'افزونهٔ {$a} در PHP';
$string['phpversion'] = 'نسخهٔ PHP';
$string['phpversionhelp'] = '<p>مودل نیاز به PHP نسخهٔ حداقل 5.6.5 یا 7.1 دارد (<span dir="ltr" style="direction: ltr; display: inline-block">7.0.x</span> محدودیت‌هایی در engine اش دارد).</p>
<p>شما در حال حاضر از نسخهٔ {$a} استفاده می‌کنید</p>
<p>باید PHP را ارتقاء دهید یا از کارگزاری دارای نسخهٔ جدیدتر PHP استفاده نمائید.</p>';
$string['releasenoteslink'] = 'برای مشاهده اطلاعات مربوط به این نسخه مودل، لطفا یادداشت‌های انتشار در {$a} را ببینید';
$string['safemode'] = '';
$string['safemodeerror'] = 'مودل ممکن است با فعال بودن safe mode مشکل داشته باشد';
$string['safemodehelp'] = '<p>مودل ممکن است مشکلات متعددی در صورت فعال بودن safe mode داشته باشد. یکی از مشکلات نه‌چندان بی اهمیت
این است که امکان دارد ایجاد فایل‌های جدید مجاز نباشد.</p>

<p>safe mode معمولاً فقط توسط میزبان‌های عمومی وبی که توهم دارند فعال می‌شود،
در نتیجه احتمالاً باید یک شرکت میزبانی وب جدید برای سایت مودل خود پیدا کنید.</p>

<p>در صورت تمایل می‌توانید به نصب ادامه دهید، ولی منتظر بروز مشکلات در آینده باشید.</p>';
$string['sessionautostart'] = 'شروع خودکار session';
$string['sessionautostarterror'] = 'باید غیرفعال باشد';
$string['sessionautostarthelp'] = '<p>مودل نیازمند پشتیبانی شدن از session است و در غیر اینصورت کار نمی‌کند.</p>

<p>session ها در فایل php.ini می‌توانند فعال شوند ... به‌دنبال پارامتر session.auto_start بگردید.';
$string['sqliteextensionisnotpresentinphp'] = 'PHP به‌درستی با افزونهٔ SQLite پیکربندی نشده است. لطفا فایل php.ini خود را بررسی کرده یا اینکه PHP را دوباره کامپایل کنید.';
$string['upgradingqtypeplugin'] = 'در حال ارتقای پلاگین نوع/سؤال';
$string['welcomep10'] = '{$a->installername} (<span dir="ltr">{$a->installerversion}</span>)';
$string['welcomep20'] = 'دیدن این صفحه به معنی نصب و راه‌اندازی موفق بستهٔ
    <strong>{$a->packname} <span dir="ltr">{$a->packversion}</span></b></strong> است. تبریک!';
$string['welcomep30'] = 'این نسخهٔ <strong>{$a->installername}</strong> شامل برنامه‌هایی
برای ایجاد محیطی برای کار کردن <strong>مودل</strong> است، برای مثال:';
$string['welcomep40'] = 'بستهٔ مورد نظر همچنین شامل <strong>مودل <span dir="ltr>{$a->moodlerelease}</span> )<span dir="ltr>{$a->moodleversion}</span>(</strong> است.';
$string['welcomep50'] = 'استفاده از هر کدام از برنامه‌های این بسته توسط اجازه‌نامه‌های مربوطهٔ آن‌ها معین
شده است. بستهٔ کامل <strong>{$a->installername}</strong>
<a href="http://www.opensource.org/docs/definition_plain.html">متن‌باز</a> است و تحت
اجازه‌نامهٔ <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a> توزیع شده است.';
$string['welcomep60'] = 'صفحه‌های بعدی با مراحل ساده‌ای شما را هدایت می‌کنند تا
<strong>مودل</strong> را روی رایانهٔ خود پیکربندی و برپا کنید. می‌توانید تنظیمات
پیش‌فرض را بپذیرید یا (در صورت تمایل) آن‌ها را مطابق با نیاز خود تغییر دهید.';
$string['welcomep70'] = 'برای برپاسازی <strong>مودل</strong> بر روی دکمهٔ «بعدی» در پائین کلیک نمائید.';
$string['wwwroot'] = 'آدرس وب';
$string['wwwrooterror'] = '«آدرس وب» معتبر به نظر نمی‌رسد - به نظر نمی‌رسد که فایل‌های این مودل آنجا باشند. مقدار زیر مجدداً تنظیم شد.';
