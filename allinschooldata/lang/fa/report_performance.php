<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_performance', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   report_performance
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['check_backup'] = 'پشتیبان‌گیری خودکار';
$string['check_backup_comment_disable'] = 'کارآیی سایت می‌تواند در حین فرایند پشتیبان‌گیری تحت تاثیر قرار بگیرد. در صورت فعال‌بودن، پشتیبان‌گیری باید برای زمان‌های خلوت برنامه‌ریزی شود.';
$string['check_backup_comment_enable'] = 'کارآیی سایت می‌تواند در حین فرایند پشتیبان‌گیری تحت تاثیر قرار بگیرد. پشتیبان‌گیری باید برای زمان‌های خلوت برنامه‌ریزی شود.';
$string['check_backup_details'] = 'فعال کردن پشتیبان‌گیری خودکار، در زمانی که تعیین کرده‌اید به‌طور خودکار آرشیوهایی از تمام درس‌ها بر روی کارگزار خواهد ساخت.<p>در حین این فرایند، میزان مصرف منابع کارگزار بیشتر خواهد شد که ممکن است کارآیی را تحت تاثیر قرار دهد.</p>';
$string['check_cachejs_comment_disable'] = 'در صورت فعال‌بودن، کارآیی بارگیری صفحه‌ها بهبود می‌یابد.';
$string['check_cachejs_comment_enable'] = 'اگر غیرفعال باشد، صفحه‌ها ممکن است کند بار شوند.';
$string['check_cachejs_details'] = 'cache کردن و فشرده‌سازی جاوااسکریپت کارآیی بار شدن صفحات را بسیار بهبود می‌دهد. این کار برای سایت‌های تحت بهره‌برداری قویا توصیه می‌شود.';
$string['check_debugmsg_comment_developer'] = 'اگر روی مقداری به غیر از توسعه‌دهنده قرار داده شود، کارآیی ممکن است کمی بهبود یابد.';
$string['check_debugmsg_comment_nodeveloper'] = 'اگر روی توسعه‌دهنده قرار داده شود، کارآیی ممکن است کمی تحت تاثیر قرار گیرد.';
$string['check_debugmsg_details'] = 'رفتن به سطح توسعه‌دهنده به‌ندرت مزیتی دارد (مگر اینکه شما یک توسعه‌دهنده باشید که در این‌صورت قویا توصیه می‌شود).<p>پس از اینکه پیام خطا را دریافت کردید و آن را کپی کرده و در جایی رونوشت کردید، <strong>توصیه اکید می‌شود</strong> که تنظیم اشکال‌زدایی را به وضعیت <strong>هیچ</strong> برگردانید. پیغام‌های اشکال‌زدایی ممکن است سرنخ‌هایی به‌طور مثال دربارهٔ پیکربندی سایت شما در اختیار هکرها قرار دهد و همچنین ممکن است تاثیر منفی روی کارآیی سایت داشته باشد.</p>';
$string['check_enablestats_comment_disable'] = 'پردازش آمار می‌تواند روی کارآیی تاثیر بگذارد. در صورتی‌که فعال باشد، تنظیمات آمارگیری باید با دقت تعیین شوند.';
$string['check_enablestats_comment_enable'] = 'پردازش آمار می‌تواند روی کارآیی تاثیر بگذارد. تنظیمات آمارگیری باید با دقت تعیین شوند.';
$string['check_enablestats_details'] = 'با فعال کردن این تنظیم، log ها با اجرای cron پردازش خواهند شد و یک سری اطلاعات آماری جمع‌آوری خواهند شد. بسته به اینکه میزان ترافیک سایت شما، این کار ممکن است کمی طول بکشد.<p>در حین این فرایند، میزان مصرف منابع کارگزار بیشتر خواهد شد که ممکن است کارآیی را تحت تاثیر قرار دهد.</p>';
$string['check_themedesignermode_comment_disable'] = 'اگر فعال باشد، تصاویر و css ها cache نخواهند شد که این کار منجر به کاهش قابل توجه کارآیی خواهد شد.';
$string['check_themedesignermode_comment_enable'] = 'اگر غیرفعال باشد، تصاویر و css ها cache خواهند شد که این کار کارآیی را به‌طور قابل توجهی افزایش خواهد شد.';
$string['check_themedesignermode_details'] = 'اکثر مواقع کندی سایت‌های مودل به این دلیل است. <p>اجرای یک سایت مودل درحالی‌که حالت طراحی پوسته فعال است، CPU را به‌طور میانگین حداقل دو برابر بیشتر مشغول خواهد کرد.</p>';
$string['comments'] = 'توضیحات';
$string['disabled'] = 'غیر فعال شده است';
$string['edit'] = 'ویرایش';
$string['enabled'] = 'فعال';
$string['issue'] = 'نکته';
$string['morehelp'] = 'راهنمایی بیشتر';
$string['performancereportdesc'] = 'این گزارش نکاتی که ممکن است روی کارآیی و سرعت سایت تاثیر بگذارند را نمایش می‌دهد {$a}';
$string['performance:view'] = 'مشاهده گزارش کارآیی';
$string['pluginname'] = 'مرور کارآیی';
$string['value'] = 'مقدار';
