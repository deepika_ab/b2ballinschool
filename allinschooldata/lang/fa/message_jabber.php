<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message_jabber', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   message_jabber
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configjabberhost'] = 'کارگزاری که برای فرستادن پیام‌های خبر رسانی Jabber باید به آن متصل شد';
$string['configjabberpassword'] = 'رمزی که هنگام اتصال به کارگزار Jabber باید استفاده شود';
$string['configjabberport'] = 'درگاه مورد استفاده هنگام اتصال به کارگزار Jabber';
$string['configjabberserver'] = 'شناسهٔ میزبان XMPP (اگر با میزبان Jabber یکی است می‌تواند خالی گذاشته شود)';
$string['configjabberusername'] = 'نام کاربری مورد استفاده هنگام اتصال به کارگزار Jabber';
$string['jabberhost'] = 'میزبان Jabber';
$string['jabberid'] = 'شناسهٔ Jabber';
$string['jabberpassword'] = 'رمز اتصال Jabber';
$string['jabberport'] = 'درگاه Jabber';
$string['jabberserver'] = 'کارگزار Jabber';
$string['jabberusername'] = 'نام کاربری Jabber';
$string['notconfigured'] = 'کارگزار Jabber پیکربندی نشده است در نتیجه پیام‌های Jabber نمی‌توانند فرستاده شوند';
$string['pluginname'] = 'Jabber';
