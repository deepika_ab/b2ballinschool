<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'license', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   license
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allrightsreserved'] = 'تمام حقوق محفوظ می‌باشد';
$string['cc'] = 'کریتیو کامانز';
$string['cc-nc'] = 'کریتیو کامانز - غیر تجاری';
$string['cc-nc-nd'] = 'کریتیو کامانز - غیر تجاری و بدون اجازهٔ تغییر';
$string['cc-nc-sa'] = 'کریتیو کامانز - غیر تجاری و به اشتراک گذاری با همین مجوز';
$string['cc-nd'] = 'کریتیو کامانز - بدون اجازهٔ تغییر';
$string['cc-sa'] = 'کریتیو کامانز - به اشتراک گذاری با همین مجوز';
$string['public'] = 'مالکیت عمومی';
$string['unknown'] = 'غیره';
