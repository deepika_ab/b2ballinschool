<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_capability', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_capability
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['capabilitylabel'] = 'توانایی:';
$string['capabilityreport'] = 'گزارش توانایی‌ها';
$string['changeoverrides'] = 'تغییر بازنویسی‌های انجام شده در این قسمت';
$string['changeroles'] = 'تغییر تعریف نقش‌ها';
$string['eventreportviewed'] = 'گزارش مشاهده شد';
$string['forroles'] = 'برای نقش (های) {$a}';
$string['getreport'] = 'دریافت گزارش';
$string['intro'] = 'این گزارش، برای یک توانایی خاص، نشان می‌دهد که در تعریف (و همینطور هر جایی از سایت که ) هر یک از نقش‌ها (یا یک سری نقش انتخاب شده) چه مجوزی برای آن توانایی تعیین شده است. همچنین هر جایی از سایت که آن توانایی بازنویسی شده باشد نیز نمایش داده می‌شود.';
$string['pluginname'] = 'گزارش توانایی‌ها';
$string['reportforcapability'] = 'گزارش مربوط به توانایی «{$a}»';
$string['reportsettings'] = 'تنظیمات گزارش';
$string['roleslabel'] = 'نقش‌ها:';
