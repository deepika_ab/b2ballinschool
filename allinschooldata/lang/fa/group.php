<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'group', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   group
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addedby'] = 'اضافه‌شده توسط {$a}';
$string['addgroup'] = 'اضافه‌کردن کاربر به گروه';
$string['addgroupstogrouping'] = 'اضافه‌کردن گروه به ابرگروه';
$string['addgroupstogroupings'] = 'حذف/اضافه گروه‌ها';
$string['adduserstogroup'] = 'اضافه/حذف کردن کاربران';
$string['allocateby'] = 'اختصاص اعضا';
$string['anygrouping'] = '[هر ابرگروهی]';
$string['autocreategroups'] = 'ایجاد خودکار گروه‌ها';
$string['backtogroupings'] = 'بازگشت به ابرگروه‌ها';
$string['backtogroups'] = 'بازگشت به گروه‌ها';
$string['badnamingscheme'] = 'باید دقیقا شامل یک کاراکتر @ یا # باشد';
$string['byfirstname'] = 'به ترتیب الفبا بر اساس نام، نام خانوادگی';
$string['byidnumber'] = 'به ترتیب الفبا بر اساس کد شناسایی';
$string['bylastname'] = 'به ترتیب الفبا بر اساس نام خانوادگی، نام';
$string['createautomaticgrouping'] = 'ساختن ابرگروه خودکار';
$string['creategroup'] = 'ساختن گروه';
$string['creategrouping'] = 'ساختن ابرگروه';
$string['creategroupinselectedgrouping'] = 'ساختن گروه در ابرگروه';
$string['createingrouping'] = 'ابرگروه‌بندی کردن گروه‌های ایجاد شده به‌طور خودکار';
$string['defaultgrouping'] = 'گروه‌بندی پیش‌فرض';
$string['defaultgroupingname'] = 'ابرگروه';
$string['defaultgroupname'] = 'گروه';
$string['deleteallgroupings'] = 'حذف تمام ابرگروه‌ها';
$string['deleteallgroups'] = 'حذف تمامی گروه‌ها';
$string['deletegroupconfirm'] = 'آیا مطمئن هستید که می‌خواهید گروه «{$a}» را حذف کنید؟';
$string['deletegrouping'] = 'حذف ابرگروه';
$string['deletegroupingconfirm'] = 'آیا مطمئن هستید که می‌خواهید ابرگروه «{$a}» را حذف کنید؟ (گروه‌های داخل ابرگروه حذف نخواهند شد.)';
$string['deletegroupsconfirm'] = 'آیا مطمئن هستید که می‌خواهید این گروه‌ها را حذف کنید؟';
$string['deleteselectedgroup'] = 'حذف گروه انتخاب‌شده';
$string['editgroupingsettings'] = 'ویرایش تنظیمات ابرگروه';
$string['editgroupsettings'] = 'ویرایش تنظیمات گروه';
$string['enrolmentkey'] = 'کلید عضویت';
$string['enrolmentkeyalreadyinuse'] = 'این کلید عضویت برای گروه دیگری استقاده شده است.';
$string['enrolmentkey_help'] = 'کلید عضویت باعث می‌شود دسترسی به درس تنها به کاربرانی که کلید را می‌دانند محدود شود. اگر یک کلید عضویت گروه تعیین شود، آنگاه وارد کردن آن کلید نه تنها به کاربران اجازه می‌دهد که وارد درس شود، بلکه آنها را به‌طور خودکار عضو این گروه هم خواهد کرد.

توجه: کلید عضویت گروه باید در تنظیمات ثبت‌نام خود فعال شده باشد و یک کلید عضویت هم باید برای درس تعیین شده باشد.';
$string['erroraddremoveuser'] = 'خطا در اضافه/حذف کردن کاربر {$a} به گروه';
$string['erroreditgroup'] = 'خطا در ساختن/به‌روز کردن گروه {$a}';
$string['erroreditgrouping'] = 'خطا در ساختن/به‌روز کردن ابرگروه {$a}';
$string['errorinvalidgroup'] = 'خطا، گروه نامعتبر {$a}';
$string['errorremovenotpermitted'] = 'شما مجوز حذف کردن عضوی که به‌طور خودکار به گروه اضافه شده است ({$a}) را ندارید';
$string['errorselectone'] = 'لطفا قبل از انتخاب این گزینه فقط یک گروه را انتخاب کنید';
$string['errorselectsome'] = 'لطفا قبل از انتخاب این گزینه یک یا چند گروه را انتخاب کنید';
$string['evenallocation'] = 'توجه: برای یکسان نگاه داشتن اعضای منسوب به گروه‌ها، تعداد واقعی اعضا در هر گروه با عددی که شما وارد کرده‌اید متفاوت است.';
$string['eventgroupcreated'] = 'گروه ساخته شد';
$string['eventgroupdeleted'] = 'گروه حذف شد';
$string['eventgroupingcreated'] = 'ابرگروه ساخته شد';
$string['eventgroupingdeleted'] = 'ابرگروه حذف شد';
$string['eventgroupinggroupassigned'] = 'گروه به ابرگروه منسوب شد';
$string['eventgroupinggroupunassigned'] = 'انتساب گروه به ابرگروه لغو شد';
$string['eventgroupingupdated'] = 'ابرگروه به‌روز شد';
$string['eventgroupmemberadded'] = 'عضو گروه اضافه شد';
$string['eventgroupmemberremoved'] = 'عضو گروه حذف شد';
$string['eventgroupupdated'] = 'گروه به‌روز شد';
$string['existingmembers'] = 'اعضای فعلی: {$a}';
$string['filtergroups'] = 'فیلترکردن گروه‌ها بر اساس:';
$string['group'] = 'گروه';
$string['groupaddedsuccesfully'] = 'گروه {$a} با موفقیت اضافه شد';
$string['groupaddedtogroupingsuccesfully'] = 'گروه {$a->groupname} با موفقیت به ابرگروه {$a->groupingname} اضافه شد';
$string['groupby'] = 'ساخته شدن خودکار بر اساس';
$string['groupdescription'] = 'توصیف گروه';
$string['groupinfo'] = 'اطلاعات مربوط به گروه انتخاب‌شده';
$string['groupinfomembers'] = 'اطلاعات مربوط به اعضای انتخاب‌شده';
$string['groupinfopeople'] = 'اطلاعات مربوط به افراد انتخاب‌شده';
$string['grouping'] = 'ابرگروه';
$string['groupingaddedsuccesfully'] = 'ابرگروه {$a} با موفقیت اضافه شد';
$string['groupingdescription'] = 'توصیف ابرگروه';
$string['grouping_help'] = 'ابرگروه مجموعه‌ای از چند گروه متعلق به یک درس است. اگر ابرگروهی انتخاب شده باشد، آنگاه شاگردانی که عضو گروه‌های داخل آن ابرگروه باشند می‌توانند با هم کار کنند.';
$string['groupingname'] = 'نام ابرگروه';
$string['groupingnameexists'] = 'ابرگروهی با نام «{$a}» در این درس وجود دارد، لطفا نام دیگری انتخاب کنید.';
$string['groupings'] = 'ابرگروه‌ها';
$string['groupingsection_help'] = 'ابرگروه مجموعه‌ای از چند گروه متعلق به یک درس است. اگر اینجا ابرگروهی انتخاب شده باشد، آنگاه تنها شاگردانی که عضو گروه‌های داخل آن ابرگروه باشند به این قسمت دسترسی خواهند داشت.';
$string['groupingsonly'] = 'تنها ابرگروه‌ها';
$string['groupmemberdesc'] = 'نقش استاندارد برای عضو یک گروه';
$string['groupmembers'] = 'اعضای گروه';
$string['groupmemberssee'] = 'دیدن اعضای گروه';
$string['groupmembersselected'] = 'اعضای گروه انتخاب شده';
$string['groupmode'] = 'نحوهٔ گروه‌بندی';
$string['groupmodeforce'] = 'تحمیل نحوهٔ گروه‌بندی';
$string['groupmodeforce_help'] = 'اگر اجبار به گروه‌بندی باشد، در این صورت نحوه گروه‌بندی درس به تمام فعالیت‌ها در این درس اعمال خواهد شد و از نحوه گروه‌بندی در هر فعالیت چشم‌پوشی خواهد شد.';
$string['groupmode_help'] = '۳ انتخاب برای این گزینه وجود دارد:

* بدون گروه
* گروه‌های کاملا جدا از هم - هر یک از اعضای گروه تنها می‌تواند گروه خود را ببینند؛ سایر گروه‌ها نامرئی هستند
* گروه‌های مرئی - هر یک از اعضای گروه در گروه خود فعالیت می‌کند، ولی می‌توانند گروه‌های دیگر را نیز ببیند

«نحوهٔ گروه‌بندی» تعریف شده در سطح درس، «نحوهٔ گروه‌بندی» پیش‌فرض در تمام فعالیت‌های درون درس نیز می‌باشد. تمام فعالیت‌هایی که از گروه‌بندی پشتیبانی می‌کنند، می‌توانند «نحوهٔ گروه‌بندی» را برای خودشان تعیین کنند. هر چند اگر «نحوهٔ گروه‌بندی» در سطح درس به صورت تحمیلی تعیین شده باشد، تنظیم تعیین شده برای فعالیت‌ها نادیده گرفته می‌شود.';
$string['groupmy'] = 'گروه من';
$string['groupname'] = 'نام گروه';
$string['groupnameexists'] = 'گروهی با نام «{$a}» در این درس وجود دارد، لطفا نام دیگری انتخاب کنید.';
$string['groupnotamember'] = 'متاسفیم، شما عضو آن گروه نیستید';
$string['groups'] = 'گروه‌ها';
$string['groupscount'] = 'گروه‌ها({$a})';
$string['groupsettingsheader'] = 'گروه‌ها';
$string['groupsgroupings'] = 'گروه‌ها &amp; گروه‌بندی‌ها';
$string['groupsinselectedgrouping'] = 'گروه‌های عضو:';
$string['groupsnone'] = 'بدون گروه';
$string['groupsonly'] = 'فقط گروه‌ها';
$string['groupspreview'] = 'پیش‌نمایش گروه‌ها';
$string['groupsseparate'] = 'گروه‌های کاملا جدا از هم';
$string['groupsvisible'] = 'گروه‌های مرئی';
$string['grouptemplate'] = 'گروه @';
$string['importgroups'] = 'وارد کردن گروه‌ها';
$string['importgroups_help'] = 'گروه‌ها می‌توانند از طریق فایل متنی وارد شوند. قالب فایل باید به این صورت باشد:

* هر خط فایل شامل یک رکورد است
* هر رکورد دنباله‌ای از داده‌ها است که با حرف کاما از یکدیگر جدا شده‌اند
* رکورد اول شامل لیستی از نام فیلدها است که پیکربندی ادامهٔ فایل را تعیین می‌کند
* فیلد groupname اجباری است
* فیلدهای description و enrolmentkey و picture و hidepicture اختیاری هستند';
$string['includeonlyactiveenrol'] = 'تنها ثبت‌نام‌های فعال شامل شوند';
$string['includeonlyactiveenrol_help'] = 'درصورتی‌که فعال باشد، کاربران تعلیق‌شده در گروه‌ها شامل نخواهند شد.';
$string['javascriptrequired'] = 'این صحفه به فعال بودن جاوااسکریپت احتیاج دارد.';
$string['membersofselectedgroup'] = 'اعضای:';
$string['mygroups'] = 'گروه‌های من';
$string['namingscheme'] = 'الگوی نام‌گذاری';
$string['namingscheme_help'] = 'از علامت @ می‌توانید برای برای ساختن گروه‌هایی که در نامشان از حروف لاتین استفاده شده است استفاده کنید. به‌طور مثال «گروه @» منجر به تولید گروه‌هایی با نام‌های گروه A، گروه B، گروه C، ... خواهد شد.

از علامت # می‌توانید برای ساختن گروه‌هایی که در نامشان از عدد استفاده شده است استفاده کنید. به‌طور مثال «گروه #» منجر به تولید گروه‌هایی با نام‌های گروه 1، گروه 2، گروه 3، ... خواهد شد.';
$string['newgrouping'] = 'ابرگروه جدید';
$string['newpicture'] = 'عکس جدید';
$string['newpicture_help'] = 'عکسی در قالب JPG یا PNG انتخاب کنید. عکس  انتخاب‌شده به‌صورت مربع بریده و به اندازهٔ ۱۰۰ در ۱۰۰ پیکسل تبدیل خواهد شد.';
$string['noallocation'] = 'بدون تخصیص';
$string['nogroup'] = 'بدون گروه';
$string['nogrouping'] = 'بدون ابرگروه';
$string['nogroups'] = 'تا به حال هیچ گروهی در این درس وجود ندارد';
$string['nogroupsassigned'] = 'هیچ گروهی تعیين نشده است';
$string['nopermissionforcreation'] = 'نمی‌شود گروه «{$a}» را ساخت زیرا شما مجوز لازم را ندارید.';
$string['nosmallgroups'] = 'جلوگیری از داشتن یک گروه کوچک در آخر';
$string['notingroup'] = 'نادیده گرفتن کاربرانی که عضو گروه‌ها هسند';
$string['notingrouping'] = '[عضو هیچ ابرگروهی نیستند]';
$string['notingrouplist'] = '[عضو هیچ گروهی نیستند]';
$string['number'] = 'تعداد گروه‌ها/اعضای هر گروه';
$string['numgroups'] = 'تعداد گروه‌ها';
$string['nummembers'] = 'تعداد اعضای هر گروه';
$string['othergroups'] = 'سایر گروه‌ها';
$string['overview'] = 'مرور';
$string['potentialmembers'] = 'اعضای بالقوه: {$a}';
$string['potentialmembs'] = 'اعضای بالقوه';
$string['printerfriendly'] = 'نمای مناسب برای چاپ';
$string['random'] = 'به‌طور تصادفی';
$string['removefromgroup'] = 'حذف کاربر از گروه {$a}';
$string['removefromgroupconfirm'] = 'آیا واقعا می‌خواهید کاربر «{$a->user}» را از گروه «{$a->group}» حذف کنید؟';
$string['removegroupfromselectedgrouping'] = 'حذف گروه از ابرگروه';
$string['removegroupingsmembers'] = 'حذف تمامی گروه‌ها از گروه‌بندی‌ها';
$string['removegroupsmembers'] = 'حذف اعضای تمام گروه‌ها';
$string['removeselectedusers'] = 'حذف کاربران انتخاب شده‌';
$string['selectfromgroup'] = 'انتخاب اعضا از بین گروه';
$string['selectfromgrouping'] = 'انتخاب اعضا از بین ابرگروه';
$string['selectfromrole'] = 'انتخاب اعضایی که این نقش را دارند';
$string['showgroupsingrouping'] = 'نمایش گروه‌ها در گروه‌بندی‌';
$string['showmembersforgroup'] = 'نمایش اعضای گروه';
$string['toomanygroups'] = 'برای پرکردن این تعداد گروه کاربر کافی وجود ندارد - تنها {$a} کاربر دارای تقش انتخاب‌شده هستند.';
$string['usercount'] = 'تعداد اعضا';
$string['usercounttotal'] = 'تعداد اعضا ({$a})';
$string['usergroupmembership'] = 'عضویت‌های کاربر انتخاب‌شده:';
