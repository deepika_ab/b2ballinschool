<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_lpimportcsv', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_lpimportcsv
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['competencyscale'] = 'مقیاس شایستگی:  {$a}';
$string['confirm'] = 'تایید';
$string['confirmcolumnmappings'] = 'تایید نگاشت ستون‌ها';
$string['csvdelimiter'] = 'حرفِ جداکننده CSV';
$string['description'] = 'توصیف';
$string['descriptionformat'] = 'قالب توصیف';
$string['encoding'] = 'کدگذاری';
$string['export'] = 'صدور';
$string['exportid'] = 'کدِ صادر شده (اختیاری)';
$string['exportnavlink'] = 'صدور چارچوب شایستگی به فایل';
$string['idnumber'] = 'کد شناسایی';
$string['import'] = 'وارد کردن';
$string['importfile'] = 'فایل CSV توصیف چارچوب';
$string['importfile_help'] = 'یک چارچوب شایستگی را می‌توان با استفاده از یک فایل متنی وارد کرد. قالب فایل را با ساختن یک چارچوب شایستگی در سایت و سپس صادر کردن آن به فایل می‌توانید تشخیص دهید.';
$string['importingfile'] = 'واردکردن اطلاعات فایل';
$string['invalidimportfile'] = 'قالب فایل نامعتبر است.';
$string['noframeworks'] = 'هنوز هیچ چارچوب شایستگی‌ای ایجاد نشده است';
$string['parentidnumber'] = 'کد شناسایی والد';
$string['pluginname'] = 'وارد کردن چارچوب شایستگی از فایل';
$string['processingfile'] = 'پردازش‌کردن فایل';
$string['scaleconfiguration'] = 'پیکربندی مقیاس';
$string['scalevalues'] = 'مقادیر مقیاس';
$string['shortname'] = 'نام کوتاه';
$string['taxonomy'] = 'رده‌بندی';
