<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_essential', language 'fa', branch 'MOODLE_36_STABLE'
 *
 * @package   theme_essential
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alert1'] = 'هشدار اول';
$string['alert2'] = 'هشدار دوم';
$string['alert3'] = 'هشدار سوم';
$string['alert_edit'] = 'ویرایش هشدارها';
$string['alert_general'] = 'اعلان';
$string['alert_info'] = 'اطلاعات';
$string['alertinfodesc'] = 'تنظیمات مربوط به هشدار خود را وارد کنید.';
$string['alertsdesc'] = 'این مورد در صفحه‌ی اول سایت پیام هشداری را به سه شیوه‌ی مختلف به کاربران نشان می‌دهد. یادتان باشد زمانی که دیگر به این هشدارها نیاز نداشتید، آن‌ها را غیر فعال کنید.';
$string['alertsheading'] = 'هشدارهای کاربر';
$string['alertsheadingsub'] = 'پیام‌های مهم را در صفحه‌ی اول به کاربران نشان بده';
$string['alerttext'] = 'متن هشدار';
$string['alerttextdesc'] = 'متنی که تمایل دارید در پیام هشدارتان نمایش داده شود.';
$string['alerttitle'] = 'عنوان';
$string['alerttitledesc'] = 'عنوان اصلی برای پیام هشدارتان.';
$string['alerttype'] = 'سطح';
$string['alerttypedesc'] = 'سطح/نوع مناسب پیام هشدار را برای بهتر آگاه ساختن کاربرانتان تعیین کنید';
$string['alert_warning'] = 'اخطار';
$string['allclasses'] = 'همه‌ی کلاس‌ها';
$string['allcourses'] = 'همه‌ی درس‌ها';
$string['allmodules'] = 'همه‌ی ماژول‌ها';
$string['allunits'] = 'همه‌ی واحدها';
$string['alternativecolors'] = 'رنگ‌های جایگزین {$a}';
$string['alternativethemecolor'] = 'رنگ‌ پوسته جایگزین {$a}';
$string['alternativethemecolordesc'] = 'رنگی که پوسته شما، هنگامی که رنگ‌های جایگزین {$a} انتخاب می‌شوند، باید داشته باشد.';
$string['alternativethemecolorname'] = 'نام مجموعه‌ی رنگ‌های جایگزین {$a}';
$string['alternativethemecolornamedesc'] = 'یک نام قابل تشخیص برای این مجموعه از رنگ‌های جایگزین انتخاب نمایید.';
$string['alternativethemecolors'] = 'رنگ‌های جایگزین برای پوسته';
$string['alternativethemecolorsdesc'] = 'رنگ‌های پوسته جایگزینی را که کاربر ممکن است انتخاب کند، مشخص می‌کند.';
$string['alternativethemehovercolor'] = 'رنگ پوسته جایگزین {$a}، زمانی که نشانگر موشی روی یک لینک می‌رود.';
$string['alternativethemehovercolordesc'] = 'در حالتی که رنگ‌های جایگزین {$a} برای پوسته انتخاب شده است، زمانی که نشانگر موشی روی یک لینک می‌رود، لینک باید چه رنگی شود.';
$string['alternativethemename'] = 'نام این دسته از رنگ‌ها';
$string['alternativethemenamedesc'] = 'نامی برای این دسته از رنگ‌های جایگزین انتخاب نمایید';
$string['alternativethemetextcolor'] = 'رنگ جایگزین {$a} برای متن نوشته‌ها';
$string['alternativethemetextcolordesc'] = 'رنگ جایگزین شماره {$a} برای متن نوشته‌های خود را انتخاب کنید.';
$string['alternativethemeurlcolor'] = 'رنگ جایگزین شماره {$a} برای لینک‌ها';
$string['alternativethemeurlcolordesc'] = 'رنگ جایگزین شماره {$a} برای لینک‌های خود را انتخاب کنید.';
$string['alwaysdisplay'] = 'همیشه نشان بده';
$string['frontpageblocksheading'] = 'بلوک‌های صفحهٔ اول';
$string['frontpageheading'] = 'صفحهٔ اول';
$string['marketing1'] = 'محل بازاریابی یک';
$string['marketing2'] = 'محل بازاریابی دو';
$string['marketing3'] = 'محل بازاریابی سه';
$string['marketingbuttontext'] = 'متن پیوند';
$string['marketingbuttonurl'] = 'آدرس پیوند';
$string['marketingheading'] = 'محل‌های بازاریابی';
$string['marketingheight'] = 'ارتفاع محل قرارگیری محل بازیابی (پیکسل)';
$string['marketingicon'] = 'آیکن';
$string['marketingimage'] = 'عکس';
$string['marketingtitle'] = 'عنوان';
$string['versionalerttext1'] = 'این پوسته برای نسخهٔ مودل در حال استفاده طراحی نشده‌است.';
$string['versionalerttext2'] = 'مشکلات غیرمنتظره‌ای ممکن است رخ دهند. لطفا نسخهٔ پوستهٔ مربوط به نسخهٔ مودل خود را تهیه کنید.';
$string['versionalerttitle'] = 'اخطار نسخه:';
