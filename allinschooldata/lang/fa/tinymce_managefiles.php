<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tinymce_managefiles', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tinymce_managefiles
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allfilesok'] = 'هیچ فایل غیر موجود یا استفاده‌نشده‌ای وجود ندارد';
$string['deleteselected'] = 'پاک‌کردن فایل‌های انتخاب‌شده';
$string['hasmissingfiles'] = 'اخطار! این فایل‌ها که در متن به آنها ارجاع داده شده است موجود نیستند:';
$string['manageareafiles'] = 'مدیریت فایل‌هایی که در ویرایشگر جاسازی شده‌اند';
$string['managefiles:desc'] = 'مدیریت فایل‌های جاسازی‌شده';
$string['pluginname'] = 'مدیریت فایل‌های جاسازی‌شده';
$string['refreshfiles'] = 'تازه‌سازی لیست فایل‌های غیر موجود و استفاده‌نشده';
$string['unusedfilesdesc'] = 'این فایل‌های جاسازی‌شده در متن استفاده نشده‌اند:';
$string['unusedfilesheader'] = 'فایل‌های استفاده‌نشده';
