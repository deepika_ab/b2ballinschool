<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'langconfig', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   langconfig
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alphabet'] = 'آ,ا,ب,پ,ت,ث,ج,چ,ح,خ,د,ذ,ر,ز,ژ,س,ش,ص,ض,ط,ظ,ع,غ,ف,ق,ک,گ,ل,م,ن,و,ه,ی';
$string['backupnameformat'] = '%Y%m%d-%H%M';
$string['decsep'] = '.';
$string['firstdayofweek'] = '6';
$string['iso6391'] = 'fa';
$string['iso6392'] = 'fas';
$string['labelsep'] = ':&nbsp;';
$string['listsep'] = ',';
$string['locale'] = 'fa_IR.UTF-8';
$string['localewin'] = 'Farsi_Iran.1256';
$string['localewincharset'] = 'WINDOWS-1256';
$string['oldcharset'] = 'WINDOWS-1256';
$string['strftimedate'] = '%d %B %Y';
$string['strftimedatefullshort'] = '%d/%m/%y';
$string['strftimedateshort'] = '%d %B';
$string['strftimedatetime'] = '%d %B %Y، %I:%M %p';
$string['strftimedatetimeshort'] = '%y/%m/%d، %H:%M';
$string['strftimedaydate'] = '%A، %d %B %Y';
$string['strftimedaydatetime'] = '%A، %d %B %Y، %I:%M %p';
$string['strftimedayshort'] = '%A، %d %B';
$string['strftimedaytime'] = '%a، %H:%M';
$string['strftimemonthyear'] = '%B %Y';
$string['strftimerecent'] = '%d %b، %H:%M';
$string['strftimerecentfull'] = '%a، %d %b %Y، %I:%M %p';
$string['strftimetime'] = '%I:%M %p';
$string['thisdirection'] = 'rtl';
$string['thisdirectionvertical'] = 'btt';
$string['thislanguage'] = 'فارسی';
$string['thislanguageint'] = 'Persian';
$string['thousandssep'] = ',';
