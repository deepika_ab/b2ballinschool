<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'mimetypes', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   mimetypes
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['application/epub_zip'] = 'کتاب الکترونیکی EPUB';
$string['application/msword'] = 'سند ورد';
$string['application/pdf'] = 'سند PDF';
$string['application/vnd.moodle.backup'] = 'پشتیبان مودل';
$string['application/vnd.ms-excel'] = 'صفحهٔ گستردهٔ اکسل';
$string['application/vnd.ms-excel.sheet.macroEnabled.12'] = 'ورک‌بوک اکسل ۲۰۰۷ با ماکروهای فعال';
$string['application/vnd.ms-powerpoint'] = 'ارائه پاورپوینت';
$string['application/vnd.oasis.opendocument.spreadsheet'] = 'صفحه گسترده اوپن‌داکیومنت';
$string['application/vnd.oasis.opendocument.spreadsheet-template'] = 'قالب صفحه گسترده اوپن‌داکیومنت';
$string['application/vnd.oasis.opendocument.text'] = 'سند متنی اوپن‌داکیومنت';
$string['application/vnd.oasis.opendocument.text-template'] = 'قالب متنی اوپن‌داکیومنت';
$string['application/vnd.oasis.opendocument.text-web'] = 'قالب صفحهٔ وب اوپن‌داکیومنت';
$string['application/vnd.openxmlformats-officedocument.presentationml.presentation'] = 'ارائه پاورپوینت ۲۰۰۷';
$string['application/vnd.openxmlformats-officedocument.presentationml.slideshow'] = 'نمایش اسلاید پاورپوینت ۲۰۰۷';
$string['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'] = 'صفحهٔ گستردهٔ اکسل ۲۰۰۷';
$string['application/vnd.openxmlformats-officedocument.spreadsheetml.template'] = 'قالب اکسل ۲۰۰۷';
$string['application/vnd.openxmlformats-officedocument.wordprocessingml.document'] = 'سند ورد ۲۰۰۷';
$string['application/xhtml_xml'] = 'سند XHTML';
$string['application/x-iwork-keynote-sffkey'] = 'سخنرانی آی‌ورک';
$string['application/x-iwork-numbers-sffnumbers'] = 'صفحه گسترده اعداد آی‌ورک';
$string['application/x-iwork-pages-sffpages'] = 'سند صفحه‌های آی‌ورک';
$string['application/x-javascript'] = 'کد جاوااسکریپت';
$string['application/x-mspublisher'] = 'سند پابلیشر';
$string['application/x-shockwave-flash'] = 'انیمیشن فلش';
$string['archive'] = 'آرشیو ({$a->EXT})';
$string['audio'] = 'فایل صوتی ({$a->EXT})';
$string['default'] = '';
$string['document/unknown'] = 'فایل';
$string['group:archive'] = 'فایل‌های آرشیو';
$string['group:audio'] = 'فایل‌های صوتی';
$string['group:document'] = 'فایل‌های اسناد';
$string['group:html_audio'] = 'فایل‌های صوتی که به‌طور بومی توسط مرورگرها پشتیبانی می‌شوند';
$string['group:html_video'] = 'فایل‌های ویدیویی که به‌طور بومی توسط مرورگرها پشتیبانی می‌شوند';
$string['group:image'] = 'فایل‌های تصویری';
$string['group:presentation'] = 'فایل‌های ارائه';
$string['group:sourcecode'] = 'کد منبع';
$string['group:spreadsheet'] = 'فایل‌های صفحه گسترده';
$string['group:video'] = 'فایل‌های ویدیویی';
$string['group:web_audio'] = 'فایل‌های صوتی استفاده‌شده در وب';
$string['group:web_file'] = 'فایل‌های وب';
$string['group:web_image'] = 'فایل‌های تصویری استفاده‌شده در وب';
$string['group:web_video'] = 'فایل‌های ویدیویی استفاده‌شده در وب';
$string['image'] = 'عکس ({$a->MIMETYPE2})';
$string['image/vnd.microsoft.icon'] = 'آیکن ویندوز';
$string['text/csv'] = 'مقادیر جداشده با کاما';
$string['text/html'] = 'سند HTML';
$string['text/plain'] = 'فایل متنی';
$string['text/rtf'] = 'سند RTF';
$string['text/vtt'] = 'تراک متنی ویدیوی تحت وب';
$string['video'] = 'فایل ویدیویی ({$a->EXT})';
