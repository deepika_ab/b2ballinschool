<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_mnet_hosts', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_mnet_hosts
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['error_roamcapabilityneeded'] = 'کاربران باید قابلیت «استفاده کردن از خدمات یک سرویس‌دهندهٔ خارجی از طریق شبکهٔ مودلی» را داشته باشند تا بتوانند لیست سرویس‌دهنده‌های شبکهٔ مودلی را ببینند';
$string['mnet_hosts:addinstance'] = 'اضافه‌کردن یک بلوک جدید کارگزارهای شبکه';
$string['mnet_hosts:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید کارگزارهای شبکه به میز کار';
$string['pluginname'] = 'کارگزارهای شبکه';
$string['server'] = 'کارگزار';
