<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_paypal', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_paypal
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['assignrole'] = 'انتساب نقش';
$string['businessemail'] = 'آدرس ایمیل تجاری پی‌پال';
$string['businessemail_desc'] = 'آدرس ایمیل حساب پی‌پال تجاری شما';
$string['cost'] = 'هزینهٔ ثبت نام';
$string['costerror'] = 'هزینهٔ ثبت‌نام عددی نیست';
$string['costorkey'] = 'لطفا یکی از روش‌های زیر را برای ثبت‌نام انتخاب کنید.';
$string['currency'] = 'واحد پول';
$string['defaultrole'] = 'نقش انتسابی پیش‌فرض';
$string['defaultrole_desc'] = 'نقشی که پس از ثبت‌نام از طریق پی‌پال باید به کاربران نسبت داده شود را انتخاب کنید';
$string['enrolenddate'] = 'تاریخ پایان';
$string['enrolenddate_help'] = 'در صورت فعال بودن، کاربران تنها تا این تاریخ می‌توانند ثبت‌نام شوند.';
$string['enrolenddaterror'] = 'تاریخ پایان ثبت‌نام نمی‌تواند قبل از شروع آن باشد';
$string['enrolperiod'] = 'مدت ثبت نام';
$string['enrolperiod_desc'] = 'مدت زمان پیش‌فرض معتبر بودن ثبت‌نام. اگر برابر با صفر باشد، مدت ثبت‌نام به‌طور پیش‌فرض نامحدود خواهد بود.';
$string['enrolperiod_help'] = 'مدت زمانی که ثبت‌نام معتبر است، با شروع از لحظه‌ای که کاربر ثبت‌نام می‌شود. اگر غیرفعال باشد، مدت ثبت‌نام نامحدود خواهد بود.';
$string['enrolstartdate'] = 'تاریخ شروع';
$string['enrolstartdate_help'] = 'در صورت فعال بودن، کاربران تنها از این تاریخ به بعد می‌توانند ثبت‌نام شوند.';
$string['expiredaction'] = 'اقدام انقضای ثبت‌نام';
$string['expiredaction_help'] = 'کاری که به‌هنگام منقضی شدن ثبت‌نام کاربر باید انجام شود را انتخاب کنید. لطفاً توجه کنید که برخی از داده‌ها و تنظیمات کاربر در هنگام لغو ثبت‌نام از درس پاک‌سازی می‌شوند.';
$string['mailadmins'] = 'آگاه‌سازی مدیر';
$string['mailstudents'] = 'باخبرکردن شاگردان';
$string['mailteachers'] = 'آگاه‌سازی اساتید';
$string['nocost'] = 'برای ثبت‌نام در این درس شهریه‌ای تعیین نشده است!';
$string['paypal:config'] = 'پیکربندی نمونه‌های ثبت‌نام پی‌پال';
$string['paypal:manage'] = 'مدیریت کاربران ثبت‌نام شده';
$string['paypal:unenrolself'] = 'لغو ثبت نام خود در درس';
$string['pluginname'] = 'پی‌پال';
$string['pluginname_desc'] = 'با استفاده از ماژول پی‌پال می‌توانید برای درس‌ها شهریهٔ ثبت‌نام تعیین کنید. اگر شهریهٔ درسی صفر باشد، آنگاه از شاگردان خواسته نمی‌شود که برای وارد شدن هزینه‌ای بپردازند. در این صفحه می‌توانید یک شهریهٔ کلی در سایت تعیین کنید که در اینصورت آن مبلغ به‌عنوان شهریهٔ پیش‌فرض در تمام درس‌ها عمل خواهد کرد. در تک تک درس‌ها هم می‌توانید به‌طور خاص شهریهٔ ثبت‌نام در آن درس‌ها را تعیین کنید. شهریه‌ای که در درس تعیین می‌کنید اولویت بیشتری نسبت به شهریهٔ پیش‌فرض دارد.';
$string['privacy:metadata:enrol_paypal:enrol_paypal:payment_type'] = 'اینکه آیا پرداخت توسط چک الکترونیکی (echeck) انجام شده‌است یا توسط موجودی پی‌پال، کارت اعتباری، یا انتقال وجه (instant) را نگهداری می‌کند.';
$string['status'] = 'مجاز بودن ثبت نام با استفاده از پی‌پال';
$string['status_desc'] = 'کاربران به‌طور پیش‌فرض بتوانند با استفاده از پی‌پال در درس ثبت‌نام کنند.';
$string['unenrolselfconfirm'] = 'آیا واقعا می‌خواهید ثبت‌نام خودتان در درس «{$a->course}» را لغو کنید؟';
