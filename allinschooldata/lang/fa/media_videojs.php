<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'media_videojs', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   media_videojs
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['audiocssclass'] = 'کلاس CSS برای فایل‌های صوتی';
$string['audioextensions'] = 'پسوندهای فایل‌های صوتی';
$string['configaudiocssclass'] = 'یک کلاس CSS که به المان &lt;audio&gt; اضافه خواهد شد.';
$string['configaudioextensions'] = 'لیستی از پسوندهای پشتیبانی شده برای فایل‌های صوتی (جدا شده با کاما). VideoJS سعی می‌کند هر جا ممکن باشد از پخش‌کنندهٔ داخلی مرورگر استفاده کند و برای فرمت‌هایی که مرورگر پشتیبانی نمی‌کند از پخش‌کنندهٔ فلش (Flash player) استفاده کند (در صورتی‌که مرورگر از Flash player پشتیبانی کند و همچنین تنظیم استفادهٔ اضطراری از Flash فعال باشد).';
$string['configlimitsize'] = 'چنانچه فعال باشد و عرض و ارتفاع مشخص نشده باشد، ویدیو در عرض و ارتفاع پیش‌فرض نمایش داده خواهد شد. در غیر اینصورت، با حداکثر عرض ممکن نمایش داده خواهد شد.';
$string['configuseflash'] = 'اگر فرمت ویدیویی توسط مرورگر پشتیبانی نمی‌شود، از Flash player استفاده شود. در صورتی‌که فعال باشد، VideoJS بدون بررسی کردن مرورگر برای تمام فایل‌هایی که دارای پسوندهای مشخص‌شده در بالا هستند استفاده خواهد شد. لطفا توجه کنید که Flash در مرورگرهای موبایل پشتیبانی نمی‌شود و استفاده از آن در بسیاری از مرورگرهای دسکتاپ هم توصیه نمی‌شود.';
$string['configvideocssclass'] = 'یک کلاس CSS که به المان &lt;video&gt; اضافه خواهد شد. به‌طور مثال، کلاس "vjs-big-play-centered" دکمهٔ پخش را در وسط قرار خواهد داد. برای جزئیات بیشتر (از جمله اینکه چطور از یک پوستهٔ سفارشی استفاده کنید) به docs.videojs.com مراجعه کنید.';
$string['configvideoextensions'] = 'لیستی از پسوندهای پشتیبانی شده برای فایل‌های ویدیویی (جدا شده با کاما). VideoJS سعی می‌کند هر جا ممکن باشد از پخش‌کنندهٔ داخلی مرورگر استفاده کند و برای فرمت‌هایی که مرورگر پشتیبانی نمی‌کند از پخش‌کنندهٔ فلش (Flash player) استفاده کند (در صورتی‌که مرورگر از Flash player پشتیبانی کند و همچنین تنظیم استفادهٔ اضطراری از Flash فعال باشد).';
$string['configyoutube'] = 'از VideoJS برای پخش ویدیوهای یوتیوب استفاده شود. توجه داشته باشید که VideoJS هنوز از لیست پخش یوتیوب پشتیبانی نمی‌کند.';
$string['limitsize'] = 'محدود کردن اندازه';
$string['pluginname'] = 'پخش‌کنندهٔ VideoJS';
$string['pluginname_help'] = 'یک روکش جاوااسکریپت برای فایل‌های ویدیویی که توسط پخش‌کنندهٔ بومی مرورگر پخش می‌شوند با استفادهٔ اضطراری از Flash player. (فرمت پشتیبانی به مرورگر بستگی دارد.)';
$string['useflash'] = 'استفادهٔ اضطراری از Flash';
$string['videocssclass'] = 'کلاس CSS برای ویدیو';
$string['videoextensions'] = 'پسوندهای فایل‌های ویدیویی';
$string['youtube'] = 'ویدیوهای یوتیوب';
