<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignfeedback_comments', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignfeedback_comments
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['commentinline'] = 'نوشتن نظر لابه‌لای پاسخ';
$string['commentinline_help'] = 'اگر فعال باشد، در هنگام نمره‌دهی، متن پاسخ در پنجرهٔ بازخورد کپی می‌شود تا نوشتن نظر در لابه‌لای پاسخ تحویل‌داده‌شده (احتمالا با یک رنگ متفاوت) یا ویرایش پاسخ اصلی ساده‌تر شود.';
$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['enabled'] = 'بازخورد متنی';
$string['enabled_help'] = 'اگر فعال باشد، نمره دهنده می‌تواند نظرات بازخوردی خود را برای هر تکلیف تحویل داده شده وارد کند.';
$string['pluginname'] = 'بازخورد متنی';
