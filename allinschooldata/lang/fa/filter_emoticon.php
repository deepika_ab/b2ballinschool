<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filter_emoticon', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   filter_emoticon
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['filtername'] = 'نمایش خندانک‌ها به صورت تصویر';
$string['settingformats'] = 'اعمال روی قالب‌های';
$string['settingformats_desc'] = 'فیلتر تنها در صورتی اعمال خواهد شد که متن اصلی در یکی از قالب‌های انتخاب شده وارد شده باشد.';
