<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filters', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   filters
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actfilterhdr'] = 'فیلترهای اعمال شده';
$string['addfilter'] = 'اضافه شدن فیلتر';
$string['anycategory'] = '«هر طبقه‌ای»';
$string['anycourse'] = '«هر درسی»';
$string['anyfield'] = 'هر مشخصه‌ای که';
$string['anyrole'] = '«هر نقشی»';
$string['anyvalue'] = 'هر مقداری';
$string['applyto'] = 'اعمال بر روی';
$string['categoryrole'] = 'نقش در طبقه';
$string['contains'] = 'شامل';
$string['content'] = 'محتوا';
$string['contentandheadings'] = 'محتوا و عناوین';
$string['coursecategory'] = 'طبقهٔ درسی';
$string['courserole'] = 'نقش در درس';
$string['courserolelabel'] = 'دارای نقش {$a->rolename} در درس {$a->coursename} از طبقهٔ {$a->categoryname} باشد';
$string['courserolelabelerror'] = 'خطای قسمت «{$a->label}»: درسی با نام {$a->coursename} وجود ندارد';
$string['coursevalue'] = 'مقدار درس';
$string['datelabelisafter'] = '{$a->label} بعد از {$a->after} باشد';
$string['datelabelisbefore'] = '{$a->label} قبل از {$a->before} باشد';
$string['datelabelisbetween'] = '{$a->label} بین {$a->before} و {$a->after} باشد';
$string['defaultx'] = 'پیش‌فرض ({$a})';
$string['disabled'] = 'غیر فعال';
$string['doesnotcontain'] = 'شامل نشود';
$string['endswith'] = 'خاتمه یابد به';
$string['filterallwarning'] = 'اعمال فیلترها بر روی عناوین به علاوه محتوا می‌تواند بار روی کارگزار شما را به طور بسیار زیادی افزایش دهد. لطفا در استفاده از این گزینه صرفه‌جویی کنید. استفاده اصلی از این تنظیم مربوط به فیلتر چند-زبانه می‌شود.';
$string['filtersettings'] = 'تنظیمات فیلترها';
$string['filtersettingsforin'] = 'تنظیمات فیلتر برای {$a->filter} در {$a->context}';
$string['filtersettings_help'] = 'در این صفحه می‌توانید فیلترها را در بخش خاصی از سایت فعال یا غیرفعال کنید.

برخی از فیلترها ممکن است تنظیمات خاص مربوط به خود را نیز داشته باشند که در این حالت در کنار نام آن‌ها یک پیوند به صفحهٔ تنظیماتشان نمایش داده می‌شود.';
$string['filtersettingsin'] = 'تنظیمات فیلترها در {$a}';
$string['firstaccess'] = 'زمان اولین دسترسی';
$string['globalrolelabel'] = 'دارای {$a->label} {$a->value} باشد';
$string['isactive'] = 'فعال؟';
$string['isafter'] = 'بعد از';
$string['isanyvalue'] = 'هر مقداری باشد';
$string['isbefore'] = 'قبل از';
$string['isdefined'] = 'مقداردهی شده باشد';
$string['isempty'] = 'خالی باشد';
$string['isequalto'] = 'برابر باشد با';
$string['isnotdefined'] = 'مقداردهی نشده باشد';
$string['isnotequalto'] = 'مخالف باشد با';
$string['limiterfor'] = 'محدودکنندهٔ فیلد {$a}';
$string['neveraccessed'] = 'هیچ‌وقت دسترسی نداشته';
$string['nevermodified'] = 'هیچ‌وقت تغییر نکرده';
$string['newfilter'] = 'فیلتر جدید';
$string['nofiltersenabled'] = 'هیچ پلاگین فیلتری روی این سایت فعال نشده است.';
$string['off'] = 'غیر فعال';
$string['offbutavailable'] = 'خاموش، ولی در دسترس';
$string['on'] = 'فعال';
$string['profilefilterfield'] = 'نام فیلد مشخصهٔ فردی';
$string['profilefilterlimiter'] = 'عملگر مشخصهٔ فردی اضافی';
$string['profilelabel'] = '{$a->label}: {$a->profile} {$a->operator} {$a->value}';
$string['profilelabelnovalue'] = '{$a->label}: {$a->profile} {$a->operator}';
$string['removeall'] = 'لغو همهٔ فیلترهای اعمال شده';
$string['removeselected'] = 'لغو فیلتر(های) انتخاب شده';
$string['selectlabel'] = 'مقدار «{$a->label}» {$a->operator} {$a->value}';
$string['startswith'] = 'شروع شود با';
$string['tablenosave'] = 'تغییرات جدول بالا به صورت خودکار ذخیره خواهد شد.';
$string['textlabel'] = '{$a->label} {$a->operator} {$a->value}';
$string['textlabelnovalue'] = '{$a->label} {$a->operator}';
$string['valuefor'] = 'مقدار {$a}';
