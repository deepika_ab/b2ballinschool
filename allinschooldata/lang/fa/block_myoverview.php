<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_myoverview', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_myoverview
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addtofavourites'] = 'ستاره‌دار کردن این درس';
$string['all'] = 'همه (به جز موارد حذف شده)';
$string['allincludinghidden'] = 'همه';
$string['aria:addtofavourites'] = 'ستاره برای';
$string['aria:allcourses'] = 'نمایش همه درس‌ها به جز درس‌هایی که از نمایش حذف شده‌اند';
$string['aria:allcoursesincludinghidden'] = 'نمایش همه درس‌ها';
$string['aria:card'] = 'تغییر به نمای کارتی';
$string['aria:controls'] = 'کنترل پیش‌نمایش درس';
$string['aria:courseactions'] = 'عملیات برای درس جاری';
$string['aria:courseprogress'] = 'پیشرفت درس:';
$string['aria:coursesummary'] = 'متن خلاصه درس:';
$string['aria:displaydropdown'] = 'نمایش منوی کشویی';
$string['aria:favourites'] = 'نمایش درس‌های ستاره‌دار';
$string['aria:future'] = 'نمایش درس‌های آینده';
$string['aria:hiddencourses'] = 'نمایش درس‌های حذف شده از نمایش';
$string['aria:hidecourse'] = 'حذف {$a} از نمایش';
$string['aria:lastaccessed'] = 'مرتب‌سازی درس‌ها بر اساس آخرین زمان دسترسی';
$string['aria:list'] = 'تعویض به نمایش لیستی';
$string['aria:past'] = 'نمایش درس‌های گذشته';
$string['aria:removefromfavourites'] = 'حذف ستاره برای';
$string['aria:showcourse'] = 'بازنشانی {$a} برای نمایش';
$string['aria:sortingdropdown'] = 'مرتب سازی منوی کشویی';
$string['aria:summary'] = 'تعویض به نمایش خلاصه‌وار';
$string['aria:title'] = 'مرتب‌سازی درس‌ها بر اساس نام درس';
$string['availablegroupings'] = 'فیلترهای در دسترس';
$string['card'] = 'کارت';
$string['cards'] = 'کارت‌ها';
$string['complete'] = 'کامل شده';
$string['completepercent'] = '{$a}% کامل شده';
$string['courseprogress'] = 'پیشرفت درس:';
$string['defaulttab'] = 'زبانهٔ پیش‌فرض';
$string['defaulttab_desc'] = 'زبانه‌ای که وقتی یک کاربر برای اولین بار نمای کلی درس را مشاهده می‌کند نمایش داده می‌شود. زبانهٔ فعال برای مراجعات بعدی به‌خاطر سپرده می‌شود.';
$string['displaycategories'] = 'نمایش طبقه‌ها';
$string['favourites'] = 'ستاره‌دار';
$string['future'] = 'آینده';
$string['hidden'] = 'درس‌ها برای نمایش حذف شدند';
$string['hiddencourses'] = 'برای نمایش حذف شد';
$string['hidecourse'] = 'حذف از نمایش دادن';
$string['inprogress'] = 'در جریان';
$string['lastaccessed'] = 'آخرین زمان دسترسی';
$string['list'] = 'لیست';
$string['morecourses'] = 'درس‌های بیشتر';
$string['myoverview:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید «نمای کلی درس» به میزِ کار';
$string['next30days'] = '۳۰ روز بعدی';
$string['next7days'] = '۷ روز بعدی';
$string['nocourses'] = 'درسی وجود ندارد';
$string['nocoursesfuture'] = 'هیچ درسی برای آینده وجود ندارد';
$string['nocoursesinprogress'] = 'هیچ درسِ درجریانی وجود ندارد';
$string['nocoursespast'] = 'هیچ درسی برای گذشته وجود ندارد';
$string['noevents'] = 'هیچ مهلتی برای فعالیت‌های آتی وجود ندارد';
$string['past'] = 'گذشته';
$string['pluginname'] = 'نمای کلی درس';
$string['recentlyoverdue'] = 'موارد اخیر';
$string['removefromfavourites'] = 'بی ستاره‌دار کردن این درس';
$string['show'] = 'بازیابی برای نمایش';
$string['sortbycourses'] = 'مرتب‌سازی بر اساس درس‌ها';
$string['sortbydates'] = 'مرتب‌سازی بر اساس تاریخ‌ها';
$string['summary'] = 'خلاصه';
$string['timeline'] = 'جدول زمانی';
$string['title'] = 'نام درس';
$string['viewcourse'] = 'مشاهدهٔ درس';
$string['viewcoursename'] = 'مشاهدهٔ درس {$a}';
