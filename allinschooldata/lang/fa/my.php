<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'my', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   my
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addpage'] = 'اضافه‌کردن صفحه';
$string['alldashboardswerereset'] = 'تمام میز کارها به وضعیت پیش‌فرض بازنشانی شدند.';
$string['allprofileswerereset'] = 'تمام صفحه‌های مشخصات فردی به وضعیت پیش‌فرض بازنشانی شدند.';
$string['defaultpage'] = 'صفحهٔ مودل من پیش‌فرض';
$string['defaultprofilepage'] = 'صفحهٔ مشخصات فردی پیش‌فرض';
$string['delpage'] = 'پاک کردن صفحه';
$string['managepages'] = 'مدیرت صفحات';
$string['mymoodle'] = 'میز کار';
$string['nocourses'] = 'هیچ درسی برای نمایش وجود ندارد';
$string['noguest'] = 'صفحهٔ میز کار برای کاربران مهمان در دسترس نیست';
$string['pinblocks'] = 'پیکربندی بلوک‌های سنجاق‌شده برای مودل من';
$string['pinblocksexplan'] = 'هر تنظیم بلوکی که اینجا انجام دهید در صفحهٔ مرور کلی «مودل من» توسط تمام کاربران مودل قابل مشاهده (و غیر قابل ویرایش) خواهد بود.';
$string['reseterror'] = 'خطایی در بازنشانی صفحه شما رخ داد';
$string['reseteveryonesdashboard'] = 'بازنشانی میز کار تمام کاربران';
$string['reseteveryonesprofile'] = 'بازنشانی صفحهٔ مشخصات فردی برای تمام کاربران';
$string['resetpage'] = 'بازنشانی صفحه به طراحی پیش‌فرض';
