<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_course_list', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_course_list
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['adminview'] = 'نمای مدیر';
$string['allcourses'] = 'کاربر مدیر همهٔ درس‌ها را می‌بیند';
$string['configadminview'] = 'مدیر در بلوک لیست درس‌ها باید چه ببیند؟';
$string['confighideallcourseslink'] = 'حذف پیوند «همهٔ درس‌ها» از پائین لیست درس‌ها. (این تنظیم روی مشاهدهٔ مدیر تأثیری نمی‌گذارد.)';
$string['course_list:addinstance'] = 'اضافه‌کردن یک بلوک جدید درس‌ها';
$string['course_list:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید درس‌ها به میز کار';
$string['hideallcourseslink'] = 'مخفی کردن پیوند «همهٔ درس‌ها»';
$string['owncourses'] = 'کاربر مدیر درس‌های خود را می‌بیند';
$string['pluginname'] = 'درس‌ها';
