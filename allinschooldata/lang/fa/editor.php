<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'editor', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   editor
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['about'] = 'دربارهٔ این ویرایشگر';
$string['absbottom'] = 'پائین مطلق';
$string['acteditorshhdr'] = 'ویرایشگرهای متنی موجود';
$string['address'] = 'نشانی';
$string['alertnoselectedtext'] = 'ابتدا باید متن را انخاب کنید!';
$string['alignment'] = 'تراز';
$string['alternatetext'] = 'متن جایگزین';
$string['anchorhelp'] = 'در این قسمت فقط لنگر ایجاد می‌شود. شما همچنان باید به صورت دستی یک پیوند ایجاد کنید.';
$string['anchorname'] = 'نام لنگر';
$string['anchors'] = 'لنگرها';
$string['baseline'] = 'خط مبنا';
$string['bold'] = 'ذخیم';
$string['borderthickness'] = 'ضخامت لبه';
$string['bottom'] = 'پائین';
$string['browse'] = 'مرور';
$string['cancel'] = 'انصراف';
$string['cellpadding'] = 'فاصله از کناره‌ها';
$string['cellspacing'] = 'فاصلهٔ خانه‌ها';
$string['choosechar'] = 'حرف را انتخاب کنید';
$string['chooseicon'] = 'تصویری را برای درج انتخاب کنید';
$string['close'] = 'بستن';
$string['closeafterreplace'] = 'بسته شدن پس از جایگزینی';
$string['cols'] = 'ستون‌ها';
$string['configeditorplugins'] = 'لطفاً پلاگین ویرایشگرهایی که مایلید استفاده کنید را انتخاب و آن‌ها را با ترتیب توصیه شده مرتب نمائید.';
$string['copy'] = 'کپی قسمت انتخاب شده';
$string['createanchor'] = 'ایجاد لنگر';
$string['createfolder'] = 'ایجاد پوشه';
$string['createlink'] = 'درج پیوند وب';
$string['cut'] = 'بریدن قسمت انتخاب شده';
$string['cutpastemozilla'] = 'متأسفانه، هم‌اکنون امکان استفاده از کلیدهای میان‌بر رایج (یا حتی دکمهٔ درج از حافظه) برای درج متن در این ویرایشگر را ندارید. این مسئله به خاطر وجود یک قابلیت امنیتی در بعضی از نسخه‌های مرورگرهای موزیلا و فایرفاکس است.<br /><br />سه راه‌حل شناخته شده برای شما وجود دارد: <br /> (۱) به جای CTRL-v، از SHIFT-Insert استفاده نمائید <br /> (۲) از منوی Edit->Paste مرورگر خود استفاده نمائید <br /> (۳) ترجیحات مرورگر خود را از طریق ویرایش فایل user.js تغییر دهید. <br /><br /> برای راهنمائی بیشتر بر روی دکمهٔ OK، و برای بازگشت به ویرایشگر بر روی Cancel کلیک نمائید';
$string['delete'] = 'حذف';
$string['editors'] = 'ویرایشگرهای متنی';
$string['editorsettings'] = 'مدیریت ویرایشگرها';
$string['enterurlfirst'] = 'ابتدا باید آدرسی را وارد نمائید';
$string['filebrowser'] = 'مرورگر فایل';
$string['findwhat'] = 'جستجو';
$string['fontname'] = 'نام قلم';
$string['fontsize'] = 'اندازهٔ قلم';
$string['forecolor'] = 'رنگ قلم';
$string['formatblock'] = 'قالب';
$string['fullscreen'] = 'ویرایشگر تمام‌صفحه';
$string['heading'] = 'عنوان';
$string['height'] = 'ارتفاع';
$string['hilitecolor'] = 'رنگ پس‌زمینه';
$string['horizontal'] = 'افقی';
$string['horizontalrule'] = 'خط افقی';
$string['htmlmode'] = 'تغییر به کد HTML';
$string['imageurl'] = 'آدرس تصویر';
$string['indent'] = 'افزایش تورفتگی';
$string['insertchar'] = 'درج حرف خاص';
$string['insertimage'] = 'درج تصویر';
$string['insertlink'] = 'درج پیوند';
$string['insertsmile'] = 'درج خندانک';
$string['inserttable'] = 'درج جدول';
$string['italic'] = 'مورب';
$string['itemsreplaced'] = 'مورد جایگزین شد!';
$string['justifycenter'] = 'هم‌ترازی از وسط';
$string['justifyfull'] = 'هم‌ترازی کامل';
$string['justifyleft'] = 'هم‌ترازی از چپ';
$string['justifyright'] = 'هم‌ترازی از راست';
$string['lang'] = 'زبان';
$string['layout'] = 'آرایش';
$string['left'] = 'چپ';
$string['lefttoright'] = 'جهت متن از چپ به راست';
$string['linkproperties'] = 'خصوصیات پیوند';
$string['linktarget'] = 'هدف';
$string['linktargetblank'] = 'پنجرهٔ جدید';
$string['linktargetnone'] = 'هیجکدام';
$string['linktargetother'] = 'متفاوت (خراب است)';
$string['linktargetself'] = 'همان قاب';
$string['linktargettop'] = 'همان پنجره';
$string['linktitle'] = 'عنوان';
$string['linkurl'] = 'آدرس';
$string['matchcase'] = 'بزرگ و کوچکی حروف مهم است';
$string['middle'] = 'وسط';
$string['minimize'] = 'کوچک کردن ویرایشگر';
$string['move'] = 'انتقال';
$string['multi'] = 'چند زبانه';
$string['mustenterurl'] = 'باید آدرس را وارد نمائید';
$string['nolink'] = 'جلوگیری از پیونددهی خودکار';
$string['normal'] = 'معمولی';
$string['notimage'] = 'فایل انتخاب شده یک تصویر نیست. لطفاً یک تصویر انتخاب نمائید!';
$string['notset'] = 'تعیین نشده است';
$string['ok'] = 'تایید';
$string['options'] = 'اختیارات';
$string['orderedlist'] = 'لیست ترتیبی';
$string['outdent'] = 'کاهش تورفتگی';
$string['paste'] = 'درج از حافظه';
$string['path'] = 'مسیر';
$string['percent'] = 'درصد';
$string['pixels'] = 'پیکسل';
$string['pleaseenteralt'] = 'لطفاً متن جایگزین را وارد نمائید';
$string['popupeditor'] = 'بزرگ کردن ویرایشگر';
$string['preformatted'] = 'از قبل قالب‌بندی شده';
$string['preview'] = 'پیش‌نمایش';
$string['properties'] = 'ویژگی‌ها';
$string['redo'] = 'اجرای مجدد آخرین عمل';
$string['regularexpressions'] = 'استفاده از عبارت‌های منظم';
$string['removelink'] = 'حذف پیوند';
$string['rename'] = 'تغییر نام';
$string['replaceall'] = 'جایگزینی همه';
$string['replacewith'] = 'جایگزین با';
$string['right'] = 'راست';
$string['righttoleft'] = 'جهت متن از راست به چپ';
$string['rows'] = 'سطرها';
$string['searchandreplace'] = 'جستجو و جایگزینی';
$string['searchnotfound'] = 'رشتهٔ مورد جستجو پیدا نشد!';
$string['selectcolor'] = 'رنگ را انتخاب کنید';
$string['selection'] = 'گزینش';
$string['showhelp'] = 'راهنمای استفاده از ویرایشگر';
$string['size'] = 'ابعاد';
$string['spacing'] = 'فاصله گذاری';
$string['strikethrough'] = 'خط خورده';
$string['subscript'] = 'پائین‌نویس';
$string['superscript'] = 'بالانویس';
$string['textindicator'] = 'سبک جاری';
$string['textmode'] = 'شما در حالت متنی هستید. برای رفتن به حالت WYSIEYG از دکمهٔ [<>] استفاده نمائید.';
$string['texttop'] = 'بالای متن';
$string['top'] = 'بالا';
$string['type'] = 'نوع';
$string['underline'] = 'مؤکد';
$string['undo'] = 'خنثی کردن آخرین عمل';
$string['unorderedlist'] = 'لیست گلوله‌ای';
$string['upload'] = 'ارسال';
$string['vertical'] = 'عمودی';
$string['width'] = 'عرض';
$string['wordclean'] = 'پاک سازی HTMLهای Word';
$string['zip'] = 'فشرده‌سازی';
