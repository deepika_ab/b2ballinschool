<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_glossary_random', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_glossary_random
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addentry'] = 'تعریف یک اصطلاح جدید';
$string['askaddentry'] = 'هنگامی که کاربران می‌توانند اصطلاح‌هایی را به فرهنگ اضافه کنند، پیوندی با این متن نمایش داده شود';
$string['askinvisible'] = 'هنگامی که کاربران نمی‌توانند فرهنگ را ویرایش کنند یا ببینند، این متن (بدون پیوند) نمایش داده شود';
$string['askviewglossary'] = 'هنگامی که کاربران می‌توانند فرهنگ را ببینند ولی نمی‌توانند اصطلاحی به آن اضافه کنند، پیوندی با این متن نمایش داده شود';
$string['glossary_random:addinstance'] = 'اضافه‌کردن یک بلوک جدید واژهٔ تصادفی فرهنگ';
$string['glossary_random:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید واژهٔ تصادفی فرهنگ به میز کار';
$string['intro'] = 'اطمینان حاصل کنید که حداقل یک فرهنگ که شامل حداقل یک اصطلاح باشد در این درس  داشته باشید. آنگاه می‌توانید تنظیمات زیر را تنظیم کنید';
$string['invisible'] = '(ادامه دارد)';
$string['lastmodified'] = 'آخرین اصطلاح تغییر کرده';
$string['nextone'] = 'اصطلاح بعدی';
$string['noentriesyet'] = 'هنوز اصطلاحی در فرهنگ انتخاب شده وجود ندارد.';
$string['notyetconfigured'] = 'لطفا با استفاده از آیکن ویرایش این بلوک را پیکربندی نمائید.';
$string['notyetglossary'] = 'باید حداقل یک فرهنگ داشته باشید تا آن را انتخاب کنید.';
$string['pluginname'] = 'واژهٔ تصادفی فرهنگ';
$string['random'] = 'اصطلاح تصادفی';
$string['refresh'] = 'تعداد روزها پیش از انتخاب یک اصطلاح جدید';
$string['select_glossary'] = 'انتخاب واژه‌ها از این فرهنگ';
$string['showconcept'] = 'نمایش عنوان اصطلاح‌ها';
$string['title'] = 'عنوان';
$string['type'] = 'چگونگی انتخاب اصطلاح‌های جدید';
$string['viewglossary'] = 'اصطلاح‌های بیشتر...';
$string['whichfooter'] = 'می‌توانید یک سری پیوند به اقدام‌های فرهنگ مرتبط با این بلوک  نمایش دهید. بلوک تنها پیوند به اقدام‌هایی را نمایش خواهد داد که در آن فرهنگ فعال هستند.';
