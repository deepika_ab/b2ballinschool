<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_collapse', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_collapse
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'نمایش/پنهان‌کردن دکمه‌های پیشرفته';
$string['privacy:metadata'] = 'پلاگین atto_collapse هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['settings'] = 'تنظیمات نوار ابزار جمع‌شده';
$string['showfewer'] = 'نمایش دکمه‌های کمتر';
$string['showgroups_desc'] = 'هنگامی که نوار ابزار جمع‌شده باشد (به‌طور پیش‌فرض جمع‌شده است) تنها این تعداد گروه در نوار ابزار نمایش داده می‌شوند.';
$string['showmore'] = 'نمایش دکمه‌های بیشتر';
