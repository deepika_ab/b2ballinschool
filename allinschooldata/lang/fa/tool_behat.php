<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_behat', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_behat
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aim'] = 'این ابزار مدیریتی به توسعه‌دهندگان و نویسندگان تست‌ها کمک می‌کند تا فایل‌های <span dir="ltr" style="direction: ltr; display: inline-block">.feature</span> ی که عملکرد مودل را تعریف می‌کنند بسازند و آنها را به‌طور خودکار اجرا کنند. تعریف مرحله (step definition)های موجود برای استفاده در فایل‌های <span dir="ltr" style="direction: ltr; display: inline-block">.feature</span> در زیر لیست شده‌اند.';
$string['allavailablesteps'] = 'تمام تعریف مرحله‌های موجود';
$string['errorcomposer'] = 'وابستگی‌های composer نصب نیستند.';
$string['infoheading'] = 'اطلاعات';
$string['installinfo'] = 'برای اطلاعات مربوط به نصب و اجرای تست‌ها {$a} را مطالعه کنید';
$string['newstepsinfo'] = 'برای اطلاعات دربارهٔ چگونه اضافه‌کردن تعریف مرحله (step definition)های جدید {$a} را مطالعه کنید';
$string['newtestsinfo'] = 'برای اطلاعات دربارهٔ چگونه نوشتن تست‌های جدید {$a} را مطالعه کنید';
$string['nostepsdefinitions'] = 'هیچ تعریف مرحله‌ای منطبق با این فیلترها نیست';
$string['stepsdefinitionsfilters'] = 'تعریف مرحله‌ها';
$string['viewsteps'] = 'فیلتر';
