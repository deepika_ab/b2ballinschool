<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshopform_accumulative', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   workshopform_accumulative
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['absent'] = 'غایب';
$string['addmoredimensions'] = 'اضافه کردن جای خالی برای {$a} جنبهٔ بیشتر';
$string['correct'] = 'درست';
$string['dimensiondescription'] = 'توصیف';
$string['dimensionmaxgrade'] = 'بهترین نمرهٔ ممکن / مقیاس مورد استفاده';
$string['dimensionnumber'] = 'جنبهٔ {$a}';
$string['dimensionweight'] = 'وزن';
$string['excellent'] = 'عالی';
$string['good'] = 'خوب';
$string['incorrect'] = 'نادرست';
$string['mustchoosegrade'] = 'باید یک نمره برای این جنبه انتخاب کنید';
$string['pluginname'] = 'نمره‌دهی جمع شونده';
$string['poor'] = 'ضعیف';
$string['present'] = 'حاضر';
$string['scalename0'] = 'بله/خیر (۲ امتیازی)';
$string['scalename1'] = 'حاضر/غایب (۲ امتیازی)';
$string['scalename2'] = 'درست/نادرست (۲ امتیازی)';
$string['scalename3'] = 'خوب/ضعیف (۳ امتیازی)';
$string['scalename4'] = 'عالی/خیلی ضعیف (۴ امتیازی)';
$string['scalename5'] = 'عالی/خیلی ضعیف (۵ امتیازی)';
$string['scalename6'] = 'عالی/خیلی ضعیف (۷ امتیازی)';
$string['verypoor'] = 'خیلی ضعیف';
