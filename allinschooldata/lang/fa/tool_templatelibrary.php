<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_templatelibrary', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_templatelibrary
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['all'] = 'تمام کامپوننت‌ها';
$string['component'] = 'کامپوننت';
$string['coresubsystem'] = 'زیرسیستم ({$a})';
$string['documentation'] = 'مستندات';
$string['example'] = 'مثال';
$string['noresults'] = 'این جستجو نتیجه‌ای نداشت';
$string['notemplateselected'] = 'الگویی انتخاب نشده است';
$string['pluginname'] = 'کتابخانهٔ الگوها';
$string['search'] = 'جستجو';
$string['searchtemplates'] = 'جستجوی الگوها';
$string['templatehasnoexample'] = 'این الگو مثالی ارائه نداده است بنابراین اینجا نمی‌توان خروجی نمونه الگو را نمایش داد. برای اضافه‌کردن مثال به این الگو، یک یادداشت (کامنت) Mustache به‌صورت «Example context (json):» درج کنید و در خط بعد مثال مربوطه را در قالب json بیاورید.';
$string['templates'] = 'الگوها';
$string['templateselected'] = 'الگو: {$a}';
