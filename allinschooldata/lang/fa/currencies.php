<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'currencies', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   currencies
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['AED'] = 'درهم امارات';
$string['AFN'] = 'افغانی افغانستان';
$string['ALL'] = 'لک آلبانی';
$string['AMD'] = 'درام ارمنستان';
$string['ANG'] = 'گیلدر آنتیل هلند';
$string['AOA'] = 'کوانزای آنگولا';
$string['ARS'] = 'پزوی آرژانتین';
$string['AUD'] = 'دلار استرالیا';
$string['AWG'] = 'فلورین آروبا';
$string['AZN'] = 'منات آذربایجان';
$string['BAM'] = 'مارک تبدیل‌پذیر بوسنی و هرزگوین';
$string['BBD'] = 'دلار باربادوس';
$string['BDT'] = 'تاکا بنگلادش';
$string['BGN'] = 'لو بلغارستان';
$string['BHD'] = 'دینار بحرین';
$string['BIF'] = 'فرانک بوروندی';
$string['BMD'] = 'دلار برمودا';
$string['BND'] = 'دلار برونئی';
$string['BOB'] = 'بولیویانو بولیوی';
$string['BRL'] = 'رئال برزیل';
$string['BSD'] = 'دلار باهاما';
$string['BTN'] = 'نگولتروم بوتان';
$string['BWP'] = 'پولای بوتسوانا';
$string['BYR'] = 'روبل بلاروس';
$string['BZD'] = 'دلار بلیز';
$string['CAD'] = 'دلار کانادا';
$string['CDF'] = 'فرانک کنگو';
$string['CHF'] = 'فرانک سوئسس';
$string['CLF'] = 'UF شیلی';
$string['CLP'] = 'پزو شیلی';
$string['CNY'] = 'یوان چین';
$string['COP'] = 'پزو کلمبیا';
$string['CRC'] = 'کولون کاستاریکا';
$string['CUC'] = '';
$string['CUP'] = 'پزو کوبا';
$string['CVE'] = 'اسکودوی کیپ ورد';
$string['CZK'] = 'کرونا چک';
$string['DJF'] = 'فرانک جیبوتی';
$string['DKK'] = 'کرون دانمارک';
$string['DOP'] = 'پزو دومینیکن';
$string['DZD'] = 'دینار الجزایر';
$string['EGP'] = 'پوند مصر';
$string['ERN'] = 'ناکفای اریتره';
$string['ETB'] = 'بیر اتیوپی';
$string['EUR'] = 'یورو';
$string['FJD'] = 'دلار فیجی';
$string['FKP'] = 'پوند جزایر فالکلند';
$string['GBP'] = 'پوند بریتانیا';
$string['GEL'] = 'لاری گرجستان';
$string['GHS'] = 'سدی غنا';
$string['GIP'] = 'پوند جبل‌الطارق';
$string['GMD'] = 'دالاسی گامبیا';
$string['GNF'] = 'فرانک گینه';
$string['GTQ'] = 'کوتزال گواتمالا';
$string['GYD'] = 'دلار گویان';
$string['HKD'] = 'دلار هنگ‌کنگ';
$string['HNL'] = 'لامپیرای هندوراس';
$string['HRK'] = 'کونا کرواسی';
$string['HTG'] = 'گورد هائیتی';
$string['HUF'] = 'فورینت مجارستان';
$string['IDR'] = 'روپیه اندونزی';
$string['ILS'] = '';
$string['INR'] = 'روپیه هند';
$string['IQD'] = 'دینار عراق';
$string['IRR'] = 'ریال ایران';
$string['ISK'] = 'کرونا ایسلند';
$string['JMD'] = 'دلار جامائیکا';
$string['JOD'] = 'دینار اردن';
$string['JPY'] = 'ین ژاپن';
$string['KES'] = 'شیلینگ کنیا';
$string['KGS'] = 'سوم قرقیزستان';
$string['KHR'] = 'ریل کامبوج';
$string['KMF'] = 'فرانک کومور';
$string['KPW'] = 'وون کره شمالی';
$string['KRW'] = 'وون کره جنوبی';
$string['KWD'] = 'دینار کویت';
$string['KYD'] = 'دلار جزایر کیمن';
$string['KZT'] = 'تنگه قزاقستان';
$string['LAK'] = 'کیپ لائوس';
$string['LBP'] = 'لیره لبنان';
$string['LKR'] = 'روپیه سری‌لانکا';
$string['LRD'] = 'دلار لیبریا';
$string['LSL'] = 'لوتی لسوتو';
$string['LTL'] = 'لیتاس لیتوانی';
$string['LVL'] = 'لاتس لتونی';
$string['LYD'] = 'دینار لیبی';
$string['MAD'] = 'درهم مراکش';
$string['MDL'] = 'لئوی مولداوی';
$string['MGA'] = 'آریاری ماداگاسکار';
$string['MKD'] = 'دینار مقدونیه';
$string['MMK'] = 'کیات میانمار(برمه)';
$string['MNT'] = 'توگروگ مغولستان';
$string['MOP'] = 'پاتاکای ماکانز';
$string['MRO'] = 'اوگویای موریتانی';
$string['MUR'] = 'روپیه موریس';
$string['MVR'] = 'روفیه مالدیو';
$string['MWK'] = 'کواچا مالاویا';
$string['MXN'] = 'پزوی مکزیک';
$string['MYR'] = 'رینگیت مالزی';
$string['MZN'] = 'متیکال موزامبیک';
$string['NAD'] = 'دلار نامیبیا';
$string['NGN'] = 'نایرا نیجریه';
$string['NIO'] = 'کوردوبا نیکاراگوئه';
$string['NOK'] = 'کرون نروژ';
$string['NPR'] = 'روپیه نپال';
$string['NZD'] = 'دلار نیوزیلند';
$string['OMR'] = 'ریال عمان';
$string['PAB'] = 'بالبوآ پاناما';
$string['PEN'] = 'سل جدید پرو';
$string['PGK'] = 'کینای پاپوآ گینه نو';
$string['PHP'] = 'پزوی فیلیپین';
$string['PKR'] = 'روپیه پاکستان';
$string['PLN'] = 'زلوتی لهستان';
$string['PYG'] = 'گوارانی پاراگوئه';
$string['QAR'] = 'ریال قطر';
$string['RON'] = 'لئوی رومانی';
$string['RSD'] = 'دینار صربستان';
$string['RUB'] = 'روبل روسیه';
$string['RWF'] = 'فرانک رواندا';
$string['SAR'] = 'ریال عربستان';
$string['SBD'] = 'دلار جزایر سلیمان';
$string['SCR'] = 'روپیه سیشل';
$string['SDG'] = 'پوند سودان';
$string['SEK'] = 'کرون سوئد';
$string['SGD'] = 'دلار سنگاپور';
$string['SHP'] = 'پوند سنت هلن';
$string['SLL'] = 'لئون سیرالئون';
$string['SOS'] = 'شیلینگ سومالی';
$string['SRD'] = 'دلار سورینام';
$string['SSP'] = 'پوند سودان جنوبی';
$string['STD'] = 'دبرای سائوتومه و پرینسیپ';
$string['SVC'] = 'کولون السالوادور';
$string['SYP'] = 'پوند سوریه';
$string['SZL'] = 'لیلانگنی سوازیلند';
$string['THB'] = 'بات تایلند';
$string['TJS'] = 'سامانی تاجیکستان';
$string['TMT'] = 'منات ترکمنستان';
$string['TND'] = 'دینار تونس';
$string['TOP'] = 'پاآنگای تونگا';
$string['TRY'] = 'لیر ترکیه';
$string['TTD'] = 'دلار ترینیداد و توباگو';
$string['TWD'] = 'دلار تایوان';
$string['TZS'] = 'شیلینگ تانزانیا';
$string['UAH'] = 'گریونای اوکراین';
$string['UGX'] = 'شیلینگ اوگاندا';
$string['USD'] = 'دلار آمریکا';
$string['UYU'] = 'پزوی اروگوئه';
$string['UZS'] = 'سم ازبکستان';
$string['VEF'] = 'بولیوار ونزوئلا';
$string['VND'] = 'دانگ ویتنام';
$string['VUV'] = 'واتوی وانواتو';
$string['WST'] = 'طلای ساموآ';
$string['XAF'] = 'فرانک سی‌اف‌ای بانک مرکزی ایالت‌های آفریقایی غربی';
$string['XCD'] = 'دلار کارائیب شرقی';
$string['XOF'] = 'فرانک سی‌اف‌ای بانک ایالت‌های آفریقایی مرکزی';
$string['XPF'] = 'فرانک سی‌اف‌ای';
$string['YER'] = 'ریال یمن';
$string['ZAR'] = 'رند آفریقای جنوبی';
$string['ZMK'] = 'کواچای زامبیا';
$string['ZWL'] = 'دلار زیمباوه';
