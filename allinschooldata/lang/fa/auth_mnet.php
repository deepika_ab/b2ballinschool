<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_mnet', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   auth_mnet
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_mnetdescription'] = 'شناسایی کاربران بر اساس شبکه‌ای مورد اطمینان که در تنظیمات شبکهٔ مودل شما تعریف شده است صورت می‌گیرد.';
$string['auth_mnet_roamin'] = 'کاربران این میزبان‌ها می‌توانند وارد سایت شما شوند';
$string['auth_mnet_roamout'] = 'کاربران شما می‌توانند وارد این میزبان‌ها شوند';
$string['auth_mnet_rpc_negotiation_timeout'] = 'مهلت صورت گرفتن شناسایی از طریق انتقال XMLRPC برحسب ثانیه.';
$string['pluginname'] = 'شناسایی شبکهٔ مودلی';
$string['privacy:metadata:mnet_external:lastaccess'] = 'زمانی که کاربر برای آخرین بار به سایت دسترسی پیدا کرد.';
$string['rpc_negotiation_timeout'] = 'مهلت مذاکرهٔ RPC';
$string['sso_idp_description'] = 'این سرویس را منتشر کنید تا کاربرانتان بتوانند بدون نیاز به وارد کردن مجدد نام کاربری و رمز ورود، در «{$a}» گشت و گذار کنند.<ul><li><em>وابستگی</em>: باید در سرویس SSO (فراهم کنندهٔ سرویس) در «{$a}» نیز <strong>مشترک</strong> شوید.</li></ul><br />مشترک این سرویس شوید تا به کاربران شناسایی شده در «{$a}» اجازه دهید بدون نیاز به وارد کردن مجدد نام کاربری و رمز ورودشان به سایت شما دسترسی داشته باشند.<ul><li><em>وابستگی</em>: باید سرویس SSO (فراهم کنندهٔ سرویس) را نیز برای «{$a}» <strong>منتشر</strong> کنید.</li></ul><br />';
$string['sso_idp_name'] = 'SSO (فراهم کنندهٔ هویت)';
$string['sso_sp_description'] = 'این سرویس را منتشر کنید تا کاربرانتان شناسایی شده در «{$a}» بتوانند بدون نیاز به وارد کردن مجدد نام کاربری و رمز ورودشان به سایت شما دسترسی داشته باشند.<ul><li><em>وابستگی</em>: باید در سرویس SSO (فراهم کنندهٔ هویت) در «{$a}» نیز <strong>مشترک</strong> شوید.</li></ul><br />در این سرویس مشترک شوید تا کاربرانتان بتوانند بدون نیاز به وارد کردن مجدد نام کاربری و رمز ورود، در «{$a}» گشت و گذار کنند.<ul><li><em>وابستگی</em>: باید سرویس SSO (فراهم کنندهٔ هویت) را نیز برای «{$a}» <strong>منتشر</strong> کنید.</li></ul><br />';
$string['sso_sp_name'] = 'SSO (فراهم کنندهٔ سرویس)';
