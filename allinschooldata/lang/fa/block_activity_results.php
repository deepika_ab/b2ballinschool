<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_activity_results', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_activity_results
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activity_results:addinstance'] = 'اضافه‌کردن یک بلوک جدید نتایج فعالیت';
$string['bestgrade'] = 'بیشترین نمره:';
$string['bestgrades'] = 'بیشترین {$a} نمره:';
$string['config_format_absolute'] = 'اعداد خام';
$string['config_format_fraction'] = 'کسری';
$string['config_format_percentage'] = 'درصد';
$string['config_grade_format'] = 'نمایش نمره‌ها به‌صورت:';
$string['config_name_format'] = 'حریم خصوصی نتایج';
$string['config_names_anon'] = 'نمایش نتایج به‌صورت ناشناس';
$string['config_names_full'] = 'نمایش کامل نام‌ها';
$string['config_names_id'] = 'فقط نمایش کد شناسایی';
$string['config_select_activity'] = 'این بلوک باید نتایج کدام آزمون را نمایش دهد؟';
$string['config_show_best'] = 'چند نمرهٔ بالا باید نمایش داده شود (برای غیرفعال شدن عدد صفر را وارد کنید)؟';
$string['config_show_worst'] = 'چند نمرهٔ پایین باید نمایش داده شود (برای غیرفعال شدن عدد صفر را وارد کنید)؟';
$string['configuredtoshownothing'] = 'پیکربندی این بلوک به گونه‌ای است که اجازهٔ نمایش هیچ نتیجه‌ای را نمی‌دهد.';
$string['config_use_groups'] = 'نمایش گروه‌ها به جای شاگردها (فقط در صورتی که فعالیت از گروه‌ها پشتیبانی کند)؟';
$string['pluginname'] = 'نتایج فعالیت';
$string['unknown'] = 'مقیاس ناشناخته';
$string['worstgrade'] = 'کمترین نمره:';
$string['worstgrades'] = 'کمترین {$a} نمره:';
