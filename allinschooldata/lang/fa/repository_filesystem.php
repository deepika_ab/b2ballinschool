<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'repository_filesystem', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   repository_filesystem
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configplugin'] = 'پیکربندی انبارهٔ سیستمِ فایل';
$string['donotusesysdir'] = 'از دایرکتوری سیستم به‌عنوان انباره استفاده نکنید، شامل';
$string['enablecourseinstances'] = 'اجازه دادن به کاربران برای اضافه کردن یک نمونهٔ جدید از انباره به درس (فقط توسط مدیران قابل پیکربندی است)';
$string['enableuserinstances'] = 'اجازه دادن به کاربران برای اضافه کردن یک نمونهٔ جدید از انباره به زمینهٔ کاربر (فقط توسط مدیران قابل پیکربندی است)';
$string['filesystem:view'] = 'نمایش فایل های سیستمی مخزن';
$string['information'] = 'این پوشه‌ها در دایرکتوری <b dir="ltr" style="display:inline-block;direction:ltr">{$a}</b> قرار دارند.';
$string['nosubdir'] = 'باید حداقل یک پوشه در داخل دایرکتوری <b dir="ltr" style="display:inline-block;direction:ltr">{$a}</b> بسازید تا بتوانید آن را اینجا انتخاب کنید.';
$string['pluginname'] = 'سیستم فایل';
$string['relativefiles'] = 'مجاز بودن فایل‌های نسبی';
$string['relativefiles_desc'] = 'این گزینه به تمام فایل‌های انباره اجازه می‌دهد که با استفاده از پیوندهای نسبی قابل دسترسی باشند.';
