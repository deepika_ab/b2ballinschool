<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'competency', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   competency
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allchildrenarecomplete'] = 'تمام زیرمجموعه‌ها کامل شدند';
$string['competencies'] = 'شایستگی‌ها';
$string['competenciesarenotenabled'] = 'شایستگی‌ها فعال نیستند.';
$string['competenciessettings'] = 'تنظیمات شایستگی‌ها';
$string['completeplanstask'] = 'کامل کردن برنامه‌های یادگیری که مهلتشان گذشته است';
$string['coursecompetencyoutcome_complete'] = 'شایستگی تکمیل شود';
$string['coursecompetencyoutcome_evidence'] = 'مدرک ضمیمه شود';
$string['coursecompetencyoutcome_none'] = 'هیچ اتفاقی نیافتد';
$string['coursecompetencyoutcome_recommend'] = 'برای بازبینی فرستاده شود';
$string['coursemodulecompetencyoutcome_complete'] = 'شایستگی تکمیل شود';
$string['coursemodulecompetencyoutcome_evidence'] = 'مدرک ضمیمه شود';
$string['coursemodulecompetencyoutcome_none'] = 'هیچ اتفاقی نیافتد';
$string['coursemodulecompetencyoutcome_recommend'] = 'برای بازبینی فرستاده شود';
$string['deletecompetencyratings'] = 'پاک کردن امتیازهای شایستگی';
$string['duplicateditemname'] = '{$a} (رونوشت)';
$string['enablecompetencies'] = 'فعال کردن شایستگی‌ها';
$string['enablecompetencies_desc'] = 'شایستگی‌ها این امکان را فراهم می‌کنند تا کاربران بر مبنای برنامه‌های یادگیری ارزشیابی شوند.';
$string['errorcannotchangeapastduedate'] = 'مهلت به پایان رسیده است؛ نمی‌تواند تغییر کند.';
$string['errorcannotsetduedateinthepast'] = 'مهلت را نمی‌توان در گذشته تعیین کرد.';
$string['errorcannotsetduedatetoosoon'] = 'مهلت خیلی نزدیک است.';
$string['errorcompetencyrule'] = 'شرط شایستگی درس «{$a}» ناشناخته است';
$string['errorcoursecompetencyrule'] = 'شرط شایستگی درس «{$a}» ناشناخته است';
$string['errorinvalidcourse'] = 'درس نامعتبر.';
$string['errornocompetency'] = 'شایستگی {$a} پیدا نشد';
$string['errorplanstatus'] = 'وضعیت برنامهٔ یادگیری «{$a}» نامعلوم است';
$string['errorscalealreadyused'] = 'با توجه به اینکه مقیاس در حال استفاده است، نمی‌تواند تغییر کند.';
$string['errorscaleconfiguration'] = 'مقادیر پیش‌فرض و مقادیری که مربوط به کسب مهارت هستند باید برای این مقیاس پیکربندی شوند.';
$string['errorusercomptencystatus'] = 'وضعیت شایستگی کاربر ناشناخته است «{$a}»';
$string['eventcompetencycreated'] = 'شایستگی اضافه شد.';
$string['eventcompetencydeleted'] = 'شایستگی حذف شد.';
$string['eventcompetencyframeworkcreated'] = 'چارچوب شایستگی ایجاد شد.';
$string['eventcompetencyframeworkdeleted'] = 'چارچوب شایستگی حذف شد.';
$string['eventcompetencyframeworkupdated'] = 'چارچوب شایستگی به‌روز شد.';
$string['eventcompetencyframeworkviewed'] = 'چارچوب شایستگی مشاهده شد.';
$string['eventcompetencyupdated'] = 'شایستگی به‌روز شد.';
$string['eventcompetencyviewed'] = 'شایستگی مشاهده شد.';
$string['eventevidencecreated'] = 'مدرک ایجاد شد';
$string['eventplanapproved'] = 'برنامهٔ یادگیری تایید شد.';
$string['eventplancompleted'] = 'برنامهٔ یادگیری تکمیل شد.';
$string['eventplancreated'] = 'برنامهٔ یادگیری ایجاد شد.';
$string['eventplandeleted'] = 'برنامهٔ یادگیری حذف شد.';
$string['eventplanreopened'] = 'برنامهٔ یادگیری دوباره باز شد.';
$string['eventplanreviewrequestcancelled'] = 'تقاضای بازبینی برنامهٔ یادگیری لغو شد.';
$string['eventplanreviewrequested'] = 'بازبینی برنامهٔ یادگیری تقاضا شد.';
$string['eventplanreviewstarted'] = 'بازبینی برنامهٔ یادگیری شروع شد.';
$string['eventplanreviewstopped'] = 'بازبینی برنامهٔ یادگیری متوقف شد.';
$string['eventplanunapproved'] = 'برنامهٔ یادگیری تایید نشد.';
$string['eventplanunlinked'] = 'برنامه یادگیری قطع اتصال شد.';
$string['eventplanupdated'] = 'برنامهٔ یادگیری به‌روز شد.';
$string['eventplanviewed'] = 'برنامهٔ یادگیری مشاهده شد.';
$string['eventtemplatecreated'] = 'الگوی برنامهٔ یادگیری ایجاد شد.';
$string['eventtemplatedeleted'] = 'الگوی برنامهٔ یادگیری حذف شد.';
$string['eventtemplateupdated'] = 'الگوی برنامهٔ یادگیری به‌روز شد.';
$string['eventtemplateviewed'] = 'الگوی برنامهٔ یادگیری مشاهده شد.';
$string['eventusercompetencyplanviewed'] = 'برنامهٔ شایستگی کاربر مشاهده شد.';
$string['eventusercompetencyrated'] = 'شایستگی کاربر امتیاز داده شد.';
$string['eventusercompetencyratedincourse'] = 'شایستگی کاربر امتیاز داده شد در درس.';
$string['eventusercompetencyratedinplan'] = 'شایستگی کاربر امتیاز داده شد در یک برنامهٔ یادگیری';
$string['eventusercompetencyreviewrequestcancelled'] = 'تقاضای بازبینی شایستگی کاربر لغو شد.';
$string['eventusercompetencyreviewrequested'] = 'بازبینی شایستگی کاربر تقاضا شد.';
$string['eventusercompetencyreviewstarted'] = 'بازبینی شایستگی کاربر شروع شد.';
$string['eventusercompetencyreviewstopped'] = 'بازبینی شایستگی کاربر متوقف شد.';
$string['eventusercompetencyviewed'] = 'شایستگی کاربر مشاهده شد.';
$string['eventusercompetencyviewedincourse'] = 'شایستگی کاربر مشاهده شد در یک درس';
$string['eventusercompetencyviewedinplan'] = 'شایستگی کاربر مشاهده شد در یک برنامه یادگیری';
$string['eventuserevidencecreated'] = 'مدرک یادگیری قبلی ساخته شد.';
$string['eventuserevidencedeleted'] = 'مدرک یادگیری قبلی پاک شد.';
$string['eventuserevidenceupdated'] = 'مدرک یادگیری قبلی به‌روز شد.';
$string['evidence_competencyrule'] = 'شرط شایستگی برقرار شد.';
$string['evidence_coursecompleted'] = 'درس «{$a}» کامل شد.';
$string['evidence_coursemodulecompleted'] = 'فعالیت «{$a}» کامل شد.';
$string['evidence_courserestored'] = 'امتیاز همراه با درس «{$a}» بازیابی شد.';
$string['evidence_evidenceofpriorlearninglinked'] = 'مدرک یادگیری قبلی «{$a}» متصل شد.';
$string['evidence_evidenceofpriorlearningunlinked'] = 'مدرک یادگیری قبلی «{$a}» قطع اتصال شد.';
$string['evidence_manualoverride'] = 'امتیاز شایستگی به‌طور دستی تعیین شد.';
$string['evidence_manualoverrideincourse'] = 'امتیاز شایستگی به‌طور دستی در درس «{$a}» تعیین شد.';
$string['evidence_manualoverrideinplan'] = 'امتیاز شایستگی به‌طور دستی در برنامهٔ یادگیری «{$a}» تعیین شد.';
$string['invalidevidencedesc'] = 'توصیف مدرک نامعتبر';
$string['invalidgrade'] = 'امتیاز نامعتبر';
$string['invalidpersistenterror'] = 'خطا: {$a}';
$string['invalidplan'] = 'برنامهٔ یادگیری نامعتبر';
$string['invalidtaxonomy'] = 'رده‌بندی نامعتبر: {$a}';
$string['invalidurl'] = 'آدرس اینترنتی معتبر نیست. مطمئن شوید که با «http://» یا «https://» شروع شود.';
$string['planstatusactive'] = 'فعال';
$string['planstatuscomplete'] = 'کامل';
$string['planstatusdraft'] = 'پیش‌نویس';
$string['planstatusinreview'] = 'درحال بازبینی';
$string['planstatuswaitingforreview'] = 'در انتظار بازبینی';
$string['pointsrequiredaremet'] = 'امتیاز مورد نیاز کسب شد';
$string['pushcourseratingstouserplans'] = 'ارسال امتیاز درس‌ها به برنامه‌های یادگیری فردی';
$string['pushcourseratingstouserplans_desc'] = 'مقدار پیش‌فرض برای تنظیم درس در مورد «به‌روزرسانی برنامه‌های یادگیری فردی در هنگامی که شایستگی‌های درس امتیاز دهی می‌شوند».';
$string['syncplanscohorts'] = 'همگام‌سازی برنامه‌ها از هم‌دوره‌ای‌های الگوی برنامهٔ یادگیری';
$string['taxonomy_behaviour'] = 'رفتار';
$string['taxonomy_competency'] = 'شایستگی';
$string['taxonomy_concept'] = 'مفهوم';
$string['taxonomy_domain'] = 'دامنه';
$string['taxonomy_indicator'] = 'شاخص';
$string['taxonomy_level'] = 'سطح';
$string['taxonomy_outcome'] = 'هدف';
$string['taxonomy_practice'] = 'تمرین';
$string['taxonomy_proficiency'] = 'تخصص';
$string['taxonomy_skill'] = 'مهارت';
$string['taxonomy_value'] = 'ارزش';
$string['usercommentedonacompetency'] = '{$a->fullname} روی شایستگی «{$a->competency}» نظر داده است.

{$a->comment}

مراجعه: {$a->url}';
$string['usercommentedonacompetencyhtml'] = '<p>{$a->fullname} روی شایستگی «{$a->competency}» نظر داده است.</p>
<div>{$a->comment}</div>
<p>مراجعه: <a href="{$a->url}">{$a->urlname}</a>.</p>';
$string['usercommentedonacompetencysmall'] = '{$a->fullname} روی شایستگی «{$a->competency}» نظر داده است.';
$string['usercommentedonacompetencysubject'] = '{$a} روی یک شایستگی نظر داده است.';
$string['usercommentedonaplan'] = '{$a->fullname} روی برنامهٔ یادگیری «{$a->plan}» نظر داده است:

{$a->comment}

مراجعه: {$a->url}';
$string['usercommentedonaplanhtml'] = '<p>{$a->fullname} روی برنامهٔ یادگیری «{$a->plan}» نظر داده است:</p>
<div>{$a->comment}</div>
<p>مراجعه: <a href="{$a->url}">{$a->urlname}</a>.</p>';
$string['usercommentedonaplansmall'] = '{$a->fullname} روی برنامهٔ یادگیری «{$a->plan}» نظر داده است.';
$string['usercommentedonaplansubject'] = '{$a} روی یک برنامهٔ یادگیری نظر داده است.';
$string['usercompetencystatus_idle'] = 'بی کار';
$string['usercompetencystatus_inreview'] = 'درحال بازبینی';
$string['usercompetencystatus_waitingforreview'] = 'در انتظار بازبینی';
$string['userplans'] = 'برنامه‌های یادگیری';
