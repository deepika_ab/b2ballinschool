<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_rss_client', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_rss_client
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addfeed'] = 'اضافه‌کردن یک آدرس اینترنتی مربوط به feed های اخبار:';
$string['addheadlineblock'] = 'اضافه‌کردن بلوک سرفصل‌های RSS';
$string['addnew'] = 'اضافه‌کردن جدید';
$string['cannotmakemodification'] = 'شما اجازه ندارید که در این لحظه تغییری در این فید RSS به‌وجود بیاورید.';
$string['clientchannellink'] = 'سایت مرجع...';
$string['configblock'] = 'پیکربندی این بلوک';
$string['couldnotfindfeed'] = 'feed با این شناسه پیدا نشد:';
$string['editfeeds'] = 'ویرایش، آبونمان یا لغو آبونمان در feed های خبری RSS/Atom';
$string['editnewsfeeds'] = 'ویرایش feed های اخبار';
$string['editrssblock'] = 'ویرایش بلوک سرفصل‌های RSS';
$string['feedadded'] = 'feed های اخبار اضافه شد';
$string['feedsconfigurenewinstance'] = 'برای پیکربندی این بلوک برای نمایش feed های RSS اینجا کلیک کنید.';
$string['feedupdated'] = 'feed های اخبار به‌روز شد';
$string['findmorefeeds'] = 'پیدا کردن feed های RSS بیشتر';
$string['pickfeed'] = 'انتخاب یک feed اخبار';
$string['rss_client:createprivatefeeds'] = 'ساختن feed های RSS اختصاصی';
$string['rss_client:createsharedfeeds'] = 'ساختن feed های RSS عمومی';
$string['seeallfeeds'] = 'مشاهدهٔ همهٔ feed ها';
$string['timeoutdesc'] = 'زمان نگهداری فید RSS در cache بر حسب دقیقه.';
$string['updatefeed'] = 'به‌روزکردن یک آدرس اینترنتی مربوط به feed های اخبار:';
