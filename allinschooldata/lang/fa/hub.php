<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'hub', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   hub
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['audienceadmins'] = 'مدیران مودل';
$string['audienceeducators'] = 'مربیان';
$string['audience_help'] = 'مخاطبان مورد نظر این درس را انتخاب کنید.';
$string['audiencestudents'] = 'شاگردان';
$string['badgesnumber'] = 'تعداد مدال‌ها ({$a})';
$string['coursesnumber'] = 'تعداد درس‌ها ({$a})';
$string['description'] = 'توصیف';
$string['errorbadimageheightwidth'] = 'حداکثر اندازهٔ تصویر باید {$a->width} X {$a->height} باشد';
$string['issuedbadgesnumber'] = 'تعداد مدال‌های صادر شده ({$a})';
$string['logourl'] = 'آدرس اینترنتی لوگو';
$string['modulenumberaverage'] = 'متوسط تعداد ماژول‌های درسی ({$a})';
$string['participantnumberaverage'] = 'متوسط تعداد شرکت کنندگان ({$a})';
$string['postaladdress'] = 'آدرس پستی';
$string['postaladdress_help'] = 'آدرس پستی این سایت یا نهادی که این سایت وابسته به آن است.';
$string['postsnumber'] = 'تعداد مطالب بیان شده در تالارها ({$a})';
$string['questionsnumber'] = 'تعداد سوال‌ها ({$a})';
$string['registeredcourses'] = 'درس‌های ثبت‌شده';
$string['registersite'] = 'ثبت در {$a}';
$string['registerwithmoodleorg'] = 'سایتتان را ثبت کنید';
$string['registrationinfo'] = 'اطلاعات ثبت';
$string['resourcesnumber'] = 'تعداد منابع ({$a})';
$string['roleassignmentsnumber'] = 'تعداد انتساب نقش‌های صورت گرفته ({$a})';
$string['search'] = 'جستجو';
$string['sendfollowinginfo'] = 'اطلاعات بیشتر';
$string['sendfollowinginfo_help'] = 'اطلاعات زیر تنها به منظور مشارکت در آمارهای کلی ارسال خواهند شد. این اطلاعات در هیچ جایی به صورت عمومی نمایش داده نخواهند شد.';
$string['siteadmin'] = 'مدیر سایت';
$string['siteadmin_help'] = 'نام و نام خانوادگی مدیر سایت.';
$string['sitecountry'] = 'کشور';
$string['sitecountry_help'] = 'کشوری که سازمان شما در آن قرار دارد.';
$string['sitedesc'] = 'توصیف سایت';
$string['sitedesc_help'] = 'این توصیف ممکن است در لیست سایت‌ها نمایش داده شود. لطفاً فقط از متن ساده استفاده کنید.';
$string['siteemail'] = 'آدرس پست الکترونیک';
$string['siteemail_help'] = 'باید یک آدرس پست الکترونیک وارد نمایید تا مدیر مرکز ثبت بتواند در صورت لزوم با شما تماس برقرار کند. از آدرس پست الکترونیک شما برای مقاصد دیگر استفاده نخواهد شد.';
$string['sitegeolocation'] = 'موقعیت جغرافیایی';
$string['sitegeolocation_help'] = 'ممکن است در آینده امکان جستجو بر اساس مکان را در مراکز ثبت فراهم کنیم. در صورتی که می‌خواهید مکان سایت خود را تعیین کنید آن را به صورت طول و عرض جغرافیایی (مانند <span dir="ltr" style="display:inline-block;direction:ltr">-31.947884,115.871285</span>) در این قسمت وارد نمایید. برای پیدا کردن طول و عرض جغرافیایی می‌توانید از Google Maps استفاده کنید.';
$string['sitelang'] = 'زبان';
$string['sitelang_help'] = 'زبان سایت شما در لیست سایت‌ها نمایش داده خواهد شد.';
$string['sitename'] = 'نام';
$string['sitename_help'] = 'نام سایت (در صورتی که مرکز ثبت اجازه دهد) در لیست سایت‌ها نشان داده خواهد شد.';
$string['sitephone'] = 'تلفن';
$string['sitephone_help'] = 'تلفن شما تنها توسط مدیر مرکز ثبت دیده خواهد شد.';
$string['siteprivacy'] = 'حریم خصوصی';
$string['siteprivacylinked'] = 'نام سایت همراه با یک پیوند منتشر شود';
$string['siteprivacynotpublished'] = 'لطفاً این سایت منتشر نشود';
$string['siteprivacypublished'] = 'فقط نام سایت منتظر شود';
$string['siteregistrationcontact'] = 'فرم تماس';
$string['siteregistrationcontact_help'] = 'در صورتی که اجازه دهید، سایر افراد می‌توانند از طریق یک فرم تماس در مرکز ثبت با شما تماس برقرار کنند. آنها به هیچ عنوان آدرس پست الکترونیک شما را نخواهند دید.';
$string['siteregistrationemail'] = 'اطلاع رسانی توسط پست الکترونیک';
$string['siteregistrationemail_help'] = 'اگر این تنظیم را فعال کنید، به مدیر مرکز ثبت اجازه می‌دهید که از طریق پست الکترونیک شما را از خبرهای مهم نظیر مسائل امنیتی مطلع کند.';
$string['siterelease'] = 'انتشار مودل';
$string['siterelease_help'] = 'شمارهٔ انتشار مودل این سایت.';
$string['siteurl'] = 'آدرس سایت';
$string['siteurl_help'] = 'اگر تنظیمات حریم خصوصی اجازه دهد که افراد آدرس سایت را ببینند، آنگاه آدرس مورد استفاده برابر با این گزینه خواهد بود.';
$string['siteversion'] = 'نسخهٔ مودل';
$string['siteversion_help'] = 'نسخهٔ مودل این سایت.';
$string['tags'] = 'برچسب ها';
$string['usersnumber'] = 'تعداد کاربران ({$a})';
