<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_email', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   auth_email
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_emaildescription'] = '<p>عضویت خود بر اساس پست الکترونیکی به کاربران این امکان را می‌دهد تا با استفاده از دکمهٔ «ایجاد حساب کاربری جدید» در صفحهٔ ورود به سایت، برای خودشان حساب کاربری بسازند. بعد از این کار، کاربر یک نامهٔ الکترونیکی دریافت می‌کند که شامل یک پیوند امن به صفحهٔ تایید نهایی حساب کاربری است. در ورودهای بعدی فقط نام کاربری و رمز بر اساس مقادیری که در پایگاه داده ذخیره شده است کنترل می‌شوند.</p><p>توجه: علاوه بر فعال کردن این پلاگین، عضویت خود بر اساس پست الکترونیکی هم باید از منوی بازشوندهٔ عضویت خود در صفحهٔ «مدیریت شناسایی‌ها» انتخاب شده باشد</p>';
$string['auth_emailnoemail'] = 'سعی شد که یک نامهٔ الکترونیکی برای شما ارسال شود ولی این تلاش موفقیت آمیز نبود!';
$string['auth_emailrecaptcha'] = 'یک المان تایید تصویری/صوتی به فرم عضویت کاربران (برای کاربرانی که با استفاده از خودعضوی پست الکترونیکی در سایت عضو می‌شوند) در سایت اضافه می‌کند. این کار از سایت شما در برابر فرستندگان هرز نوشته محافظت می‌کند. برای جزئیات بیشتر به http://www.google.com/recaptcha مراجعه کنید.';
$string['auth_emailrecaptcha_key'] = 'استفاده از تصویر reCAPTCHA';
$string['auth_emailsettings'] = 'تنظیمات';
$string['pluginname'] = 'عضویت بر اساس پست الکترونیک';
