<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   message
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['acceptandaddcontact'] = 'پذیرفتن و اضافه‌کردن به مخاطبین';
$string['addcontact'] = 'افزودن به مخاطبين';
$string['addcontactconfirm'] = 'آیا مطمئن هستید که می‌خواهید {$a} را به لیست مخاطبین خود اضافه کنید؟';
$string['addtofavourites'] = 'شروع مکالمه';
$string['addtoyourcontacts'] = 'اضافه‌شدن به لیست مخاطبین';
$string['addtoyourcontactsandmessage'] = 'اضافه‌کردن به مخاطبین و فرستادن پیام';
$string['ago'] = '{$a} قبل';
$string['allusers'] = 'همهٔ پیام‌ها از همهٔ کاربران';
$string['backto'] = 'برگشت به {$a}';
$string['backupmessageshelp'] = 'در صورت فعال بودن، پیام‌های رد و بدل شده بین کاربران در پشتیبان‌گیری‌های خودکار *سایت* شامل خواهند شد.';
$string['blockcontact'] = 'مسدود کردن مخاطب';
$string['blockedusers'] = 'کاربران مسدود شده';
$string['blocknoncontacts'] = 'افرادی که در لیست مخاطبین من نیستند نتوانند برای من پیام بفرستند';
$string['blockuser'] = 'مسدود کردن کاربر';
$string['blockuserconfirm'] = 'آیا مطمئن هستید که می‌خواهید {$a} را مسدود کنید؟';
$string['blockuserconfirmbutton'] = 'بلوک';
$string['canceledit'] = 'انصراف از ویرایش پیام‌ها';
$string['cancelselection'] = 'انصراف از';
$string['contactableprivacy'] = 'پذیرش پیام‌ها از:';
$string['contactableprivacy_coursemember'] = 'مخاطبین من و کسانی که در درس‌های من هستند';
$string['contactableprivacy_onlycontacts'] = 'فقط کسانی که در لیست مخاطبین من هستند';
$string['contactableprivacy_site'] = 'هر کس در سایت';
$string['contactblocked'] = 'مخاطب مسدود شده است';
$string['contactrequests'] = 'درخواست‌های اضافه کردن به مخاطبین';
$string['contactrequestsent'] = 'درخواست اضافه شدن به مخاطبین ارسال شد';
$string['contacts'] = 'مخاطبین';
$string['decline'] = 'نپذیرفتن';
$string['defaultmessageoutputs'] = 'خروجی‌های پیش‌فرض پیام';
$string['defaults'] = 'پیش‌فرض‌ها';
$string['deleteallconfirm'] = 'آیا مطمئن هستید که می‌خواهید کل این مکالمه را حذف کنید؟ این کار موجب پاک شدن مکالمه برای سایر شرکت‌کنندگان مکالمه نخواهد شد.';
$string['deleteallmessages'] = 'پاک‌کردن تمام پیام‌ها';
$string['deleteconversation'] = 'حذف مکالمه';
$string['deleteselectedmessages'] = 'پاک‌کردن پیام‌های انتخاب‌شده';
$string['deleteselectedmessagesconfirm'] = 'آیا مطمئنید که می‌خواهید پیام‌های انتخاب‌شده را پاک کنید؟ این کار موجب پاک شدن پیام‌ها برای سایر شرکت‌کنندگان مکالمه نخواهد شد.';
$string['disableall'] = 'غیرفعال‌کردن اطلاعیه‌ها';
$string['disabled'] = 'پیام رسانی در این سامانه غیر فعال شده است.';
$string['disallowed'] = 'غیر مجاز';
$string['editmessages'] = 'ویرایش پیام‌ها';
$string['emailtagline'] = 'کپی نسخه پیامی می باشد که در "{$a->sitename}" برای شما ارسال گردیده است. {$a->url} برای پاسخ به اینجا بروید.';
$string['enabled'] = 'فعال';
$string['errorcallingprocessor'] = 'خطا در فراخوانی خروجی تعریف‌شده';
$string['errorconversationdoesnotexist'] = 'مکالمه موجود نیست';
$string['errortranslatingdefault'] = 'خطا در ترجمهٔ تنظیمات پیش‌فرض فراهم‌شده توسط پلاگین؛ در عوض در حال استفاده از پیش‌فرض‌های سیستم.';
$string['eventgroupmessagesent'] = 'پیام گروهی  ارسال شد';
$string['eventmessagecontactadded'] = 'مخاطب پیام‌دهی اضافه شد';
$string['eventmessagecontactblocked'] = 'مخاطب پیام‌دهی مسدود شد';
$string['eventmessagecontactremoved'] = 'مخاطب پیام‌دهی حذف شد';
$string['eventmessagecontactunblocked'] = 'مخاطب پیام‌دهی از حالت مسدود خارج شد';
$string['eventmessagedeleted'] = 'پیام حذف شد';
$string['eventmessagesent'] = 'پیام ارسال شد';
$string['eventmessageuserblocked'] = 'کاربر مسدود شد';
$string['eventmessageuserunblocked'] = 'کاربر از مسدودی خارج شد';
$string['eventmessageviewed'] = 'پیام مشاهده شد';
$string['eventnotificationsent'] = 'اعلان ارسال شد';
$string['eventnotificationviewed'] = 'اعلان مشاهده شد';
$string['forced'] = 'قفل شده';
$string['groupconversations'] = 'گروه';
$string['groupinfo'] = 'اطلاعات گروه';
$string['guestnoeditmessage'] = 'کاربر مهمان قادر به ویرایش تنظیمات بخش پیام نمی باشد';
$string['guestnoeditmessageother'] = 'کاربر مهمان تنظیمات بخش پیام دیگر کاربران را نمی تواند ویرایش نماید.';
$string['hidemessagewindow'] = 'بستن پنجرهٔ پیام‌ها';
$string['hidenotificationwindow'] = 'پنهان‌کردن پنجرهٔ اطلاعیه‌ها';
$string['individualconversations'] = 'خصوصی';
$string['info'] = 'اطلاعات';
$string['isnotinyourcontacts'] = '{$a} در لیست مخاطبین شما نیست';
$string['loadmore'] = 'اطلاعات بیشتر';
$string['loggedin'] = 'هنگام بودن در سایت';
$string['loggedindescription'] = 'زمانی که در سایت هستید';
$string['loggedin_help'] = 'هنگامی که در سایت هستید به چه ترتیبی مایلید اطلاعیه‌ها را دریافت کنید';
$string['loggedoff'] = 'هنگام نبودن در سایت';
$string['loggedoffdescription'] = 'زمانی که در سایت نیستید';
$string['loggedoff_help'] = 'هنگامی که در خارج از سایت هستید به چه ترتیبی مایلید اطلاعیه‌ها را دریافت کنید';
$string['managemessageoutputs'] = 'مدیریت خروجی‌های پیام';
$string['message'] = 'پیام';
$string['messagecontactrequestsnotification'] = '{$a} می‌خواهد به عنوان یک مخاطب اضافه شود';
$string['messagecontactrequestsnotificationsubject'] = '{$a} می‌خواهد به عنوان یک مخاطب اضافه شود';
$string['messagedrawerviewcontact'] = 'جزییات کاربر برای {$a}';
$string['messagedrawerviewcontacts'] = 'پیام مخاطبین';
$string['messagedrawerviewconversation'] = 'مکالمه با {$a}';
$string['messagedrawerviewgroupinfo'] = 'جزییات گروه برای {$a}';
$string['messagedrawerviewoverview'] = 'کلیات پیام‌ها';
$string['messagedrawerviewsettings'] = 'تنظیمات پیام';
$string['messageoutputs'] = 'خروجی‌های پیام';
$string['messagepreferences'] = 'ترجیحات پیام‌دهی';
$string['messages'] = 'پیام‌ها';
$string['messagesselected:'] = 'پیام‌های انتخاب‌شده:';
$string['messagingdatahasnotbeenmigrated'] = 'بدلیل ارتقاء زیرساخت پیام‌رسانی، پیام‌های شما موقتا در دسترس نمی‌باشند. لطفا منتظر بمانید تا این مرحله به پایان برسد.';
$string['messagingdisabled'] = 'پیام رسانی در این سامانه غیر فعال شده است پیام شما در قالب پست الکترونیکی ارسال گردید.';
$string['newmessage'] = 'پیام جدید';
$string['newmessagesearch'] = 'برای فرستادن یک پیام جدید، مخاطبی را انتخاب یا جستجو کنید.';
$string['newonlymsg'] = 'فقط جدیدها نمایش داده شوند';
$string['nocontactrequests'] = 'درخواستی برای اضافه کردن به مخاطبین وجود ندارد';
$string['nocontacts'] = 'مخاطبی در لیست نیست';
$string['nocontactsgetstarted'] = 'مخاطبی در لیست نیست';
$string['noframesjs'] = 'استفاده از رابط کاربری ابتدایی‌تر';
$string['nogroupconversations'] = 'هیچ مکالمه گروهی ندارید';
$string['noindividualconversations'] = 'هیچ مکالمه خصوصی ندارید';
$string['nomessages'] = 'هیچ پیغامی منتظر جواب نیست';
$string['nomessagesfound'] = 'پیامی پیدا نشد';
$string['noncontacts'] = 'خارج از لیست مخاطبین';
$string['nonotifications'] = 'هیچ اطلاعیه‌ای ندارید';
$string['noparticipants'] = 'هیچ شرکت‌کننده‌ای نیست';
$string['noreply'] = 'به این پیام پاسخ ندهید.';
$string['notificationdatahasnotbeenmigrated'] = 'بدلیل ارتقاء زیرساخت ارسال اعلانات، اعلانات شما موقتا در دسترس نمی‌باشند. لطفا منتظر بمانید تا این مرحله به پایان برسد.';
$string['notificationimage'] = 'تصویر اطلاعیه';
$string['notificationpreferences'] = 'ترجیحات اطلاعیه‌ها';
$string['notifications'] = 'اطلاعیه‌ها';
$string['notificationwindow'] = 'پنجرهٔ اطلاعیه‌ها';
$string['notincontacts'] = 'شما باید {$a} به لیست مخاطبین خود اضافه کنید تا بتوانید برای ایشان پیام بفرستید';
$string['notincontactsheading'] = '{$a} در لیست مخاطبین شما نیست';
$string['numparticipants'] = '{$a} شرکت‌کننده';
$string['off'] = 'غیرفعال';
$string['offline'] = 'آفلاین';
$string['on'] = 'فعال';
$string['online'] = 'آنلاین';
$string['otherparticipants'] = 'دیگر شرکت‌کنندگان';
$string['outputdisabled'] = 'خروجی غیرفعال است';
$string['outputdoesnotexist'] = 'خروجی پیام وجود ندارد';
$string['outputenabled'] = 'خروجی فعال است';
$string['outputnotavailable'] = 'موجود نیست';
$string['outputnotconfigured'] = 'پیکربندی نشده است';
$string['participants'] = 'شرکت کنندگان';
$string['pendingcontactrequests'] = 'تعداد {$a} نفر در انتظار درخواست بودن در لیست مخاطبین شما هستند';
$string['permitted'] = 'مجاز';
$string['privacy'] = 'حریم خصوصی';
$string['privacy_desc'] = 'می‌توانید محدود کنید که چه کسانی بتوانند به شما پیام بفرستند';
$string['privacy:export:conversationprefix'] = 'مکالمه:';
$string['privacy:metadata:message_contact_requests'] = 'لیست درخواست‌های اضافه کردن به مخاطبین';
$string['privacy:metadata:message_contact_requests:requesteduserid'] = 'ID کاربری که درخواست اضافه شدن به مخاطبین را دریافت کرده است';
$string['privacy:metadata:message_contact_requests:timecreated'] = 'زمانی که درخواست اضافه‌شدن به مخاطبین ایجاد شد';
$string['privacy:metadata:message_contact_requests:userid'] = 'ID کاربری که درخواست اضافه شدن به مخاطبین را فرستاده است';
$string['privacy:metadata:message_contacts'] = 'فهرست مخاطبان';
$string['privacy:metadata:messages'] = 'پیام‌ها';
$string['privacy:metadata:messages:fullmessage'] = 'متن کامل پیام';
$string['privacy:metadata:messages:fullmessageformat'] = 'قالب متن کامل پیام';
$string['privacy:metadata:messages:fullmessagehtml'] = 'متن کامل پیام در قالب HTML';
$string['privacy:metadata:messages:smallmessage'] = 'نسخه خلاصه پیام';
$string['privacy:metadata:messages:subject'] = 'موضوع پیام';
$string['privacy:metadata:messages:timecreated'] = 'زمان ایجاد پیام';
$string['privacy:metadata:messages:useridfrom'] = 'ID کاربر ارسال‌کننده پیام';
$string['privacy:metadata:message_user_actions:action'] = 'اقدام انجام شده';
$string['privacy:metadata:message_user_actions:timecreated'] = 'زمان ایجاد اقدام';
$string['privacy:metadata:message_users_blocked'] = 'لیست کاربران مسدود شده';
$string['privacy:metadata:message_users_blocked:blockeduserid'] = 'ID کاربری که مسدود شده است';
$string['privacy:metadata:message_users_blocked:timecreated'] = 'زمانی که عمل مسدود کردن انجام شده است';
$string['privacy:metadata:message_users_blocked:userid'] = 'ID کاربری که مسدود کردن را انجام داده است';
$string['privacy:metadata:notifications'] = 'اعلانات';
$string['privacy:metadata:notifications:contexturl'] = 'URL مرتبط با این اعلان';
$string['privacy:metadata:notifications:eventtype'] = 'نوع رویداد';
$string['privacy:metadata:notifications:fullmessage'] = 'متن کامل اعلان';
$string['privacy:metadata:notifications:fullmessageformat'] = 'قالب اعلان';
$string['privacy:metadata:notifications:fullmessagehtml'] = 'متن اعلان در قالب HTML';
$string['privacy:metadata:notifications:smallmessage'] = 'خلاصه متن اعلان';
$string['privacy:metadata:notifications:subject'] = 'موضوع اعلان';
$string['privacy:metadata:notifications:timecreated'] = 'زمان ایجاد اعلان';
$string['privacy:metadata:notifications:timeread'] = 'زمان مشاهده اعلان';
$string['privacy:metadata:notifications:useridfrom'] = 'ID کاربر ارسال‌کننده اعلان';
$string['privacy:metadata:notifications:useridto'] = 'ID کاربر دریافت‌کننده اعلان';
$string['privacy:metadata:preference:core_message_settings'] = 'تنظیمات پیام‌رسانی';
$string['processorsettings'] = 'تنظیمات پردازشگر';
$string['removecontact'] = 'حذف کردن مخاطب';
$string['removecoursefilter'] = 'حذف فیلتر برای درس {$a}';
$string['removefromyourcontacts'] = 'حذف‌شدن از لیست مخاطبین';
$string['requiresconfiguration'] = 'نیاز به پیکربندی دارد';
$string['searchcombined'] = 'جستجو بین افراد و پیام‌ها';
$string['searchforuser'] = 'جستجو به‌دنبال یک کاربر';
$string['searchforuserorcourse'] = 'جستجو به‌دنبال یک کاربر یا درس';
$string['searchmessages'] = 'جستجوی پیام‌ها';
$string['seeall'] = 'مشاهدهٔ همه';
$string['selectmessagestodelete'] = 'پیام‌هایی که می‌خواهید پاک شوند را انتخاب کنید';
$string['selectnotificationtoview'] = 'هر یک از اطلاعیه‌های لیست سمت راست را انتخاب کنید تا جزئیات بیشتری ببینید';
$string['selfconversation'] = 'فضای شخصی';
$string['selfconversationdefaultmessage'] = 'ذخیره پیام‌های موقت، لینک‌ها، یادداشت‌ها و سایر موارد. برای دسترسی بعدتر.';
$string['send'] = 'ارسال';
$string['sendbulkmessage'] = 'ارسال پیام به تعداد {$a} نفر';
$string['sendbulkmessagesent'] = 'پیام به {$a} نفر ارسال شد';
$string['sendbulkmessagesentsingle'] = 'پیام به یک نفر ارسال شد';
$string['sendbulkmessagesingle'] = 'ارسال پیام به یک نفر';
$string['sendcontactrequest'] = 'ارسال درخواست اضافه‌شدن به مخاطبین';
$string['sender'] = '{$a}:';
$string['sendingvia'] = 'فرستادن «{$a->provider}» از طریق «{$a->processor}»';
$string['sendingviawhen'] = 'فرستادن «{$a->provider}» از طریق «{$a->processor}» هنگامی که {$a->state}';
$string['sendmessage'] = 'ارسال پیام';
$string['sendmessageto'] = 'ارسال پیام به {$a}';
$string['sendmessagetopopup'] = 'ارسال پیام به {$a} - پنجره جدید';
$string['settings'] = 'تنظیمات';
$string['showmessagewindownonew'] = 'نمایش پنجرهٔ پیام‌ها بدون پیام جدید';
$string['showmessagewindowwithcount'] = 'نمایش پنجرهٔ پیام‌ها با {$a} پیام جدید';
$string['shownotificationwindownonew'] = 'نمایش پنجرهٔ اطلاعیه‌ها بدون اطلاعیهٔ جدید';
$string['shownotificationwindowwithcount'] = 'نمایش پنجرهٔ اطلاعیه‌ها با {$a} اطلاعیهٔ جدید';
$string['togglemessagemenu'] = 'باز و بسته کردن منوی پیام‌دهی';
$string['togglenotificationmenu'] = 'باز و بسته کردن منوی اطلاعیه‌ها';
$string['touserdoesntexist'] = 'نمی‌توانید به کاربری با شناسهٔ ({$a}) که وجود ندارد پیامی بفرستید';
$string['unabletomessage'] = 'شما نمی‌توانید به این کاربر پیام بفرستید';
$string['unblockcontact'] = 'خارج کردن مخاطب از حالت مسدود';
$string['unblockuserconfirm'] = 'آیا مطمئن هستید که می‌خواهید {$a} را مسدود بودن خارج کنید؟';
$string['unknownuser'] = 'کاربر ناشناخته';
$string['unreadnewmessage'] = 'پیام جدید از {$a}';
$string['unreadnotification'] = 'اطلاعیهٔ خوانده‌نشده: {$a}';
$string['useentertosend'] = 'استفاده از کلید Enter برای ارسال';
$string['userisblockingyou'] = 'این کاربر شما را مسدود کرده است و امکان ارسال پیام برای او از طریق شما امکان پذیر نمی باشد';
$string['userisblockingyounoncontact'] = 'این کاربر تنها پیام افرادی که در لیست مخاطبانش باشند را می پذیرد ، شما در لیست مخاطبین او نمی باشید.';
$string['userwouldliketocontactyou'] = '{$a} تمایل دارد که شما را به لیست مخاطبینش اضافه کند';
$string['viewfullnotification'] = 'مشاهده متن کامل اطلاعیه';
$string['viewmessageswith'] = 'مشاهدهٔ پیام‌ها با {$a}';
$string['viewnotificationresource'] = 'برو به: {$a}';
$string['viewunreadmessageswith'] = 'مشاهدهٔ پیام‌های خوانده‌نشده با {$a}';
$string['wouldliketocontactyou'] = 'مایل است با شما تماس بگیرد';
$string['writeamessage'] = 'پیامی بنویسید...';
$string['you'] = 'شما:';
$string['youhaveblockeduser'] = 'شما این کاربر را مسدود کرده‌اید';
