<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'editor_atto', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   editor_atto
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['autosavefrequency'] = 'تناوب ذخیره‌شدن خودکار';
$string['autosavefrequency_desc'] = 'تعداد ثانیه‌های بین ذخیره‌شدن‌های خودکار. آتو با توجه به این تنظیم به‌صورت خودکار متن داخل ویرایشگر را ذخیره می‌کند، تا زمانی که کاربر یکسانی به یک فرم یکسان بازمی‌گردد، متن ویرایشگر بازیابی شود.';
$string['autosavesucceeded'] = 'پیش‌نویس ذخیره شد.';
$string['infostatus'] = 'اطلاعات';
$string['pluginname'] = 'ویرایشگر HTML آتو';
$string['privacy:metadata:database:atto_autosave:drafttext'] = 'متنی که ذخیره شد';
$string['settings'] = 'تنظیمات نوار ابزار آتو';
$string['toolbarconfig'] = 'پیکربندی نوار ابزار';
$string['toolbarconfig_desc'] = 'لیست پلاگین‌ها و ترتیب نمایش آنها را در این قسمت می‌توانید پیکربندی کنید. پیکربندی به این صورت است که در هر خط، ابتدا نام گروه می‌آید و سپس لیست ترتیبی پلاگین‌های آن گروه. نام گروه و پلاگین‌ها با علامت مساوی (=) و خود پلاگین‌ها هم با ویرگول انگلیسی (,) از یکدیگر جدا شده‌اند. نام هر گروه باید غیرتکراری نشان‌دهندهٔ وجه اشتراک دکمه‌های داخلی‌اش باشد. نام دکمه‌ها و گروه‌ها نمی‌تواند تکرار شود و فقط می‌تواند شامل اعداد و حروف انگلیسی باشد.';
