<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability_group', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability_group
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['anygroup'] = '(هر گروهی)';
$string['description'] = 'فقط شاگردانی که عضو یک گروه خاص هستند، یا تمام گروه‌ها.';
$string['error_selectgroup'] = 'باید یک گروه را انتخاب کنید.';
$string['missing'] = '(گروه حذف‌شده)';
$string['pluginname'] = 'محدودیت بر اساس گروه';
$string['privacy:metadata'] = 'پلاگین محدودیت بر اساس گروه فردی هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['requires_anygroup'] = 'عضو یک گروه باشید';
$string['requires_group'] = 'عضو <strong>{$a}</strong> باشید';
$string['requires_notanygroup'] = 'عضو هیچ گروهی نباشید';
$string['requires_notgroup'] = 'عضو <strong>{$a}</strong> نباشید';
$string['title'] = 'گروه';
