<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'mathslib', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   mathslib
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['anunexpectederroroccured'] = 'یک خطای غیرمنتظره رخ داد';
$string['cannotassigntoconstant'] = 'نمی‌توان مقدار ثابت «{$a}» را مقداردهی کرد';
$string['cannotredefinebuiltinfunction'] = 'تابع تعبیه‌شدهٔ «<span dir="ltr" style="direction: ltr; display: inline-block;">{$a}()</span>» را نمی‌توان دوباره تعریف کرد';
$string['divisionbyzero'] = 'تقسیم بر صفر';
$string['expectingaclosingbracket'] = 'یک براکت بسته مورد انتظار بود';
$string['illegalcharactergeneral'] = 'کاراکتر غیرمجاز «{$a}»';
$string['illegalcharacterunderscore'] = 'کاراکتر غیرمجاز «_»';
$string['implicitmultiplicationnotallowed'] = 'یک عملگر مورد انتظار است، ضرب به‌طور ضمنی مجاز نیست.';
$string['internalerror'] = 'خطای داخلی';
$string['operatorlacksoperand'] = 'برای عملگر «{$a}» عملوندی فراهم نشده است';
$string['undefinedvariable'] = 'متغیر تعریف نشده «{$a}»';
$string['undefinedvariableinfunctiondefinition'] = 'متغیر تعریف نشده «{$a}» در تعریف تابع';
$string['unexpectedclosingbracket'] = 'براکت بسته غیر منتظره';
$string['unexpectedcomma'] = 'کامای غیر منتظره';
$string['unexpectedoperator'] = 'عملگر «{$a}» غیر منتظره';
$string['wrongnumberofarguments'] = 'تعداد اشتباه برای آرگومان‌ها ({$a->given} آرگومان فراهم شد، {$a->expected} آرگومان مورد انتظار بود)';
