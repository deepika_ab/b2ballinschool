<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'logstore_database', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   logstore_database
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['buffersize'] = 'اندازهٔ بافر';
$string['buffersize_help'] = 'تعداد log هایی که به‌صورت دسته‌ای و با یک عمل پایگاه داده درج می‌شوند (که این کار باعث بهبود کارآیی می‌شود).';
$string['create'] = 'ایجاد';
$string['databasecollation'] = 'collation پایگاه داده';
$string['databasepersist'] = 'اتصال‌های مداوم (persistent connections) به پایگاه داده';
$string['databaseschema'] = 'schema پایگاه داده';
$string['databasesettings'] = 'تنظیمات پایگاه داده';
$string['databasesettings_help'] = 'جزئیات اتصال برای پایگاه دادهٔ log بیرونی: {$a}';
$string['databasetable'] = 'جدول پایگاه داده';
$string['databasetable_help'] = 'نام جدولی که log ها در آن ذخیره خواهند شد. این جدول باید با جدول مورد استفاده توسط logstore_standard (جدول mdl_logstore_standard_log) ساختار یکسانی داشته باشد.';
$string['filters'] = 'فیلتر کردن log ها';
$string['filters_help'] = 'فعال کردن فیلترهایی که باعث می‌شود بعضی از اقدام‌ها از log شدن مستثنی شوند.';
$string['includeactions'] = 'شامل عمل‌هایی که از این انواع هستند';
$string['includelevels'] = 'شامل عمل‌هایی که از این سطح‌های آموزشی هستند';
$string['logguests'] = 'log کردن عمل‌های کاربر مهمان';
$string['other'] = 'سایر';
$string['participating'] = 'مشارکت';
$string['pluginname'] = 'log در پایگاه دادهٔ خارجی';
$string['read'] = 'خواندن';
$string['teaching'] = 'تدریس';
$string['testingsettings'] = 'در حال آزمایش تنظیمات پایگاه داده...';
$string['testsettings'] = 'آزمایش اتصال';
$string['update'] = 'به‌روزرسانی';
