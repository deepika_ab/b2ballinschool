<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_installaddon', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_installaddon
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['featuredisabled'] = 'نصب‌کنندهٔ پلاگین روی این سایت غیرفعال است.';
$string['installaddon'] = 'نصب پلاگین!';
$string['installaddons'] = 'نصب پلاگین‌ها';
$string['installfromrepo'] = 'نصب پلاگین از فهرست پلاگین‌های مودل';
$string['installfromrepo_help'] = 'برای جستجو و نصب پلاگین جدید به صفحهٔ فهرست پلاگین‌ها در سایت مودل هدایت خواهید شد. توجه کنید که برای ساده‌تر کردن فرایند نصب، نام کامل و آدرس و نسخهٔ مودل سایت شما به سایت مودل فرستاده می‌شوند.';
$string['installfromzip'] = 'نصب پلاگین از فایل ZIP';
$string['installfromzipfile'] = 'بستهٔ ZIP';
$string['installfromzipfile_help'] = 'بستهٔ ZIP پلاگین باید فقط شامل یک دایرکتوری همنام با پلاگین باشد. فایل ZIP در محل مناسب (بسته به نوع پلاگین) از حالت فشرده خارج می‌شود. اگر بستهٔ مورد نظر از فهرست پلاگین‌های مودل دریافت شده باشد، دارای این ساختار خواهد بود.';
$string['installfromzip_help'] = 'به‌جایِ نصب مستقیم پلاگین از فهرست پلاگین‌های مودل، می‌توان بستهٔ ZIP پلاگین مورد نظر را آپلود کرد. بستهٔ ZIP باید بسته‌ای که فهرست پلاگین‌های مودل دانلود می‌شود ساختار یکسانی داشته باشد.';
$string['installfromzipinvalid'] = 'بستهٔ ZIP پلاگین باید فقط شامل یک دایرکتوری همنام با پلاگین باشد. فایل ارسال شده یک بستهٔ ZIP پلاگین معتبر نیست.';
$string['installfromziprootdir'] = 'تغییر نام دایرکتوری مرجع';
$string['installfromziprootdir_help'] = 'ممکن است نام دایرکتوری مرجع در برخی از بسته‌های ZIP (نظیر بسته‌هایی توسط Github تولید می‌شوند) نادرست باشند. در این صورت، می‌توان نام درست را به‌طور دستی وارد کرد.';
$string['installfromzipsubmit'] = 'نصب پلاگین با استفاده از فایل ZIP';
$string['installfromziptype'] = 'نوع پلاگین';
$string['installfromziptype_help'] = 'برای پلاگین‌هایی که نام کامپوننتشان را به‌درستی ابراز کرده باشند، نصب‌کننده می‌تواند نوع پلاگین را تشخیص دهد. در مواردی که تشخیص خودکار ممکن نیست، نوع درست پلاگین را به‌طور دستی انتخاب کنید. اخطار: در صورت انتخاب نوع اشتباه، ممکن است فرایند نصب پلاگین به‌طرز بدی با شکست مواجه شود.';
$string['permcheck'] = 'مطمئن شوید که سرویس‌دهندهٔ وب شما مجوز نوشتن در محل قرارگیری پلاگین (که بسته به نوع پلاگین متفاوت است) را داشته باشد.';
$string['permcheckerror'] = 'خطا در هنگام بررسی مجوز نوشتن';
$string['permcheckprogress'] = 'بررسی مجوز نوشتن ...';
$string['permcheckrepeat'] = 'بررسی مجدد';
$string['permcheckresultno'] = 'محل مربوط به این نوع پلاگین (<em>{$a->path}</em>) قابل نوشتن نیست';
$string['permcheckresultyes'] = 'محل مربوط به این نوع پلاگین (<em>{$a->path}</em>) قابل نوشتن است';
$string['pluginname'] = 'نصب‌کنندهٔ پلاگین';
