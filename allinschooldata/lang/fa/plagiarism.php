<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'plagiarism', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   plagiarism
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['availableplugins'] = 'پلاگین‌های موجود';
$string['configenableplagiarism'] = 'در صورت فعال بودن، مدیران را قادر می‌سازد تا پلاگین‌های پیشگیری (تشخیص) تقلب را پیکربندی کنند.';
$string['configplagiarismplugins'] = 'لطفا پلاگین تقلب‌یابی که می‌خواهید پیکربندی کنید را انتخاب کنید';
$string['enableplagiarism'] = 'فعال بودن پلاگین‌های پیشگیری از تقلب';
$string['manageplagiarism'] = 'مدیریت پلاگین‌های شناسایی تقلب';
$string['nopluginsinstalled'] = 'هیچ پلاگین تقلب‌یابی نصب نیست.';
$string['plagiarism'] = 'پیشگیری از تقلب';
