<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_section_links', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_section_links
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['incby1'] = 'مقدار اضافه شونده';
$string['incby1_help'] = 'پیوندهایی که برای قسمت‌های درس نمایش داده می‌شوند از ۱ شروع شده و با فاصلهٔ این عدد زیاد می‌شوند.';
$string['incby2'] = 'مقدار اضافه شوندهٔ دوم';
$string['incby2_help'] = 'پیوندهایی که برای قسمت‌های درس نمایش داده می‌شوند از ۱ شروع شده و با فاصلهٔ این عدد زیاد می‌شوند.';
$string['jumptocurrenttopic'] = 'پرش به موضوع جاری';
$string['jumptocurrentweek'] = 'پرش به هفتهٔ جاری';
$string['numsections1'] = 'تعداد قسمت‌ها';
$string['numsections1_help'] = 'هنگامی‌که تعداد قسمت‌های درس به این عدد برسد، آنگاه از مقدار اضافه‌شونده استفاده خواهد شد.';
$string['numsections2'] = 'تعداد قسمت‌های دوم';
$string['numsections2_help'] = 'هنگامی‌که تعداد قسمت‌های درس به این عدد برسد، آنگاه از مقدار اضافه‌شوندهٔ دوم استفاده خواهد شد.';
$string['pluginname'] = 'پیوندهای بخش‌ها';
$string['topics'] = 'موضوع‌ها';
$string['weeks'] = 'هفته‌ها';
