<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'bigbluebuttonbn', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   bigbluebuttonbn
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityoverview'] = 'شما یک جلسه بیگ‌بلوباتن در پیش دارید';
$string['bbbduetimeoverstartingtime'] = 'زمان تنظیم شده برای ایم فعالیت باید دیرتر از زمان شروع باشد';
$string['bbbdurationwarning'] = 'حداکثر زمان برای این جلسه %duration% دقیقه است';
$string['bbbrecordallfromstartwarning'] = 'این جلسه از زمان شروع ضبط می شود';
$string['bbbrecordwarning'] = 'این جلسه ضبط خواهد شد';
$string['bigbluebuttonbn'] = 'بیگ‌بلوباتن';
$string['bigbluebuttonbn:addinstance'] = 'اضافه کردن یک فعالیت/اتاق جدید بیگ‌بلوباتن';
$string['bigbluebuttonbn:join'] = 'پیوستن به جلسه بیگ‌بلوباتن';
$string['bigbluebuttonbn:managerecordings'] = 'مدیریت ضبط جلسات بیگ‌بلوباتن';
$string['bigbluebuttonbn:view'] = 'مشاهده یک اتاق/فعالیت';
$string['config_participant'] = 'تنظیمات شرکت کنندگان';
$string['config_participant_moderator_default'] = 'مجری بطور پیش‌فرض';
$string['config_recording_all_from_start_default'] = 'ضبط همه از ابتدا';
$string['config_recording_all_from_start_default_description'] = 'اگر این گزینه تیک بخورد جلسه از ابتدا ضبط می‌شود';
$string['config_recording_hide_button_default'] = 'پنهان کردن دکمه ضبط';
$string['config_recordings_general'] = 'نمایش تنظیمات ضبط';
$string['config_recordings_preview_default'] = 'پیش‌نمایش به صورت پیش‌فرض نمایش داده می‌شود';
$string['email_body_notification_meeting_description'] = 'توضیحات';
$string['index_heading_moderator'] = 'مجری‌ها';
$string['index_heading_viewer'] = 'مشاهده کنندگان';
$string['instance_type_default'] = 'اتاق/فعالیت با امکان ضبط شدن';
$string['instance_type_recording_only'] = 'فقط جلسات ضبط شده';
$string['instance_type_room_only'] = 'فقط اتاق/فعالیت';
$string['mod_form_block_general'] = 'تنظیمات عمومی';
$string['mod_form_block_participants'] = 'شرکت کنندگان';
$string['mod_form_block_recordings'] = 'مشاهده جلسات ضبط شده';
$string['mod_form_block_room'] = 'تنظیمات جلسه/فعالیت';
$string['mod_form_block_schedule'] = 'زمان‌بندی برنامه جلسه';
$string['mod_form_field_closingtime'] = 'زمان بسته شدن پیوستن به جلسه';
$string['mod_form_field_instanceprofiles'] = 'نوع جلسه';
$string['mod_form_field_intro'] = 'توضیحات';
$string['mod_form_field_name'] = 'نام اتاق جلسه مجازی';
$string['mod_form_field_nosettings'] = 'هیچ تنظیمی برای ویرایش نیست.';
$string['mod_form_field_notification'] = 'تغییرات را به کاربران ثبت نام شده اطلاع بده';
$string['mod_form_field_openingtime'] = 'زمان شروع پیوستن به جلسه';
$string['mod_form_field_participant_add'] = 'اضافه کردن شرکت کنندگان';
$string['mod_form_field_participant_bbb_role_moderator'] = 'مجری';
$string['mod_form_field_participant_bbb_role_viewer'] = 'مشاهده کننده';
$string['mod_form_field_participant_list'] = 'لیست شرکت کنندگان';
$string['mod_form_field_participant_list_type_all'] = 'همه کاربران ثبت‌نام شده';
$string['mod_form_field_record'] = 'جلسه می‌تواند ضبط شود';
$string['mod_form_field_wait'] = 'منتظر مجری جلسه بودن';
$string['mod_form_field_welcome'] = 'پیغام خوش آمد گویی';
$string['modulename'] = 'بیگ‌بلوباتن';
$string['modulenameplural'] = 'بیگ‌بلوباتن';
$string['pluginname'] = 'بیگ‌بلوباتن';
$string['search:activity'] = 'اطلاعات فعالیت بیگ‌بلوباتن';
$string['view_conference_action_join'] = 'پیوستن به جلسه';
$string['view_login_viewer'] = 'ورود به عنوان مشاهده کننده...';
$string['view_message_conference_room_ready'] = 'این اتاق جلسه  آماده است. شما می‌توانید همکنون وارد شوید.';
$string['view_message_moderator'] = 'مجری';
$string['view_message_moderators'] = 'مجری‌ها';
$string['view_message_norecordings'] = 'هیچ جلسه ضبط شده‌ای موجود نیست.';
$string['view_message_viewer'] = 'مشاهده کننده';
$string['view_message_viewers'] = 'مشاهده کنندگان';
$string['view_recording_description'] = 'توضیحات';
$string['view_recording_list_description'] = 'توضیحات';
$string['view_section_title_recordings'] = 'جلسات ضبط شده';
