<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_multichoice', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_multichoice
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['answerhowmany'] = 'تک یا چند جوابی؟';
$string['answernumbering'] = 'نام‌گذاری گزینه‌ها؟';
$string['answernumbering123'] = '۱.، ۲.، ۳.، ...';
$string['answernumberingiii'] = 'i.، ii.، iii.، ...';
$string['answernumberingIIII'] = 'I.، II.، III.، ...';
$string['answernumberingnone'] = 'بدون نام‌گذاری';
$string['answersingleno'] = 'انتخاب بیش از یک گزینه مجاز است';
$string['answersingleyes'] = 'فقط یک گزینه را می‌توان انتخاب کرد';
$string['choiceno'] = 'گزینهٔ {$a}';
$string['choices'] = 'گزینه‌های موجود';
$string['clozeaid'] = 'جای خالی را با کلمهٔ مناسب پر کنید';
$string['correctansweris'] = 'پاسخ درست «{$a}» است.';
$string['correctfeedback'] = 'برای هر پاسخ درست';
$string['errfractionsaddwrong'] = 'جمع نمره‌های مثبتی که تعیین کرده‌اید ٪۱۰۰ نمی‌شود<br />بلکه برابر با ٪{$a} می‌شود.';
$string['errfractionsnomax'] = 'نمرهٔ یکی از گزینه‌ها باید ٪۱۰۰ باشد تا<br />کسب نمرهٔ کامل از این سؤال ممکن باشد.';
$string['errgradesetanswerblank'] = 'نمره تعیین شده است ولی پاسخ خالی است';
$string['feedback'] = 'بازخورد';
$string['fillouttwochoices'] = 'باید حداقل دو گزینه تعریف کنید. گزینه‌های خالی استفاده نخواهند شد.';
$string['fractionsaddwrong'] = 'جمع نمره‌های مثبت تعیین شده به جای ٪۱۰۰ برابر با ٪{$a} می‌شود.<br />آیا می‌خواهید که بازگردید و مشکل را برطرف کنید؟';
$string['fractionsnomax'] = 'نمرهٔ یکی از گزینه‌ها باید ٪۱۰۰ باشد تا<br />به دست آوردن نمرهٔ کامل از سؤال ممکن باشد.<br />آیا مایلید که بازگردید و مشکل را برطرف کنید؟';
$string['incorrectfeedback'] = 'برای هر پاسخ نادرست';
$string['notenoughanswers'] = 'این نوع سؤال به حداقل {$a} گزینه نیاز دارد';
$string['overallcorrectfeedback'] = 'بازخورد برای هر پاسخ درست';
$string['overallfeedback'] = 'بازخورد کلی';
$string['overallincorrectfeedback'] = 'بازخورد برای هر پاسخ نادرست';
$string['overallpartiallycorrectfeedback'] = 'بازخورد برای هر پاسخ نیمه درست';
$string['partiallycorrectfeedback'] = 'برای هر پاسخ نیمه درست';
$string['pluginname'] = 'چند گزینه‌ای';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال چند گزینه‌ای';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال چند گزینه‌ای';
$string['pluginname_help'] = 'برای پاسخ به یک سؤال (که ممکن است شامل یک تصویر هم باشد)، پاسخ دهنده پاسخ را از میان گزینه‌های موجود انتخاب می‌کند. دو نوع سؤال چند گزینه‌ای وجود دارد: تک جوابی و چند جوابی.';
$string['pluginnamesummary'] = 'امکان انتخاب یک یا چند گزینه از بین گزینه‌های از قبل تعریف شده را فراهم می‌کند.';
$string['selectmulti'] = 'یک یا چند گزینه را انتخاب کنید:';
$string['selectone'] = 'یک گزینه را انتخاب کنید:';
$string['shuffleanswers'] = 'بهم ریختن ترتیب گزینه‌ها؟';
$string['shuffleanswers_help'] = 'در صورت انتخاب، ترتیب گزینه‌ها در هر بار نمایش به صورت تصادف بُر می‌خورد. البته مشروط به اینکه تنظیم «بهم ریختن ترتیب گزینه‌ها» در تنظیمات آزمون هم انتخاب شده باشد.';
$string['singleanswer'] = 'یکی از گزینه‌ها را انتخاب کنید.';
