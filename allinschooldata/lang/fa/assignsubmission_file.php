<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_file', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignsubmission_file
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['acceptedfiletypes'] = 'انواع فایل‌های مورد پذیرش';
$string['acceptedfiletypes_help'] = 'پذیریش انواع فایل‌ها می‌تواند با وارد کرد لیست پسوندها فایل‌ها محدود شود. اگر این قسمت خالی باشد، آنگاه همه انواع فایل‌ها مجاز است';
$string['configmaxbytes'] = 'حداکثر اندازه فایل';
$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['defaultacceptedfiletypes'] = 'انواع فایل‌های مورد پذیرش به صورت پیش‌فرض';
$string['default_help'] = 'اگر تعیین شده باشد، این شیوهٔ تحویل به طور پیش‌فرض برای تمام تکلیف‌های جدید فعال خواهد بود.';
$string['enabled'] = 'تحویل فایل';
$string['enabled_help'] = 'اگر فعال باشد، شاگردان می‌توانند یک یا چند فایل را به عنوان تکلیف تحویل دهند.';
$string['file'] = 'تحویل فایل';
$string['maxfilessubmission'] = 'حداکثر تعداد فایل‌های ارسالی';
$string['maxfilessubmission_help'] = 'اگر تحویل فایل فعال باشد، هر شاگرد می‌تواند تا این تعداد فایل را به‌عنوان تکلیف ارسال کند.';
$string['maximumsubmissionsize'] = 'حداکثر اندازه تحویل';
$string['maximumsubmissionsize_help'] = 'قایل‌هایی که توسط شاگردان ارسال می‌شود می‌توانند تا حداکثر این اندازه باشند.';
$string['pluginname'] = 'تحویل فایل';
$string['siteuploadlimit'] = 'محدودیت ارسال در سایت';
