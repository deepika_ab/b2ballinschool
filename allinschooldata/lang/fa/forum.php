<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'forum', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   forum
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityoverview'] = 'مطلب جدیدی در تالار گفتگو نیست';
$string['addanewdiscussion'] = 'شروع یک مباحثهٔ جدید';
$string['addanewquestion'] = 'طرح یک سؤال جدید';
$string['addanewtopic'] = 'طرح مباحثهٔ جدید';
$string['advancedsearch'] = 'جستجوی پیشرفته';
$string['allforums'] = 'همهٔ تالارها';
$string['allowdiscussions'] = 'آیا یک {$a} می‌تواند در این تالار مطلبی مطرح کند؟';
$string['allowsallsubscribe'] = 'در این تالار افراد مختارند که خودشان در مورد مشترک شدن یا نشدن تصمیم بگیرند';
$string['allowsdiscussions'] = 'در این تالار هر کسی فقط یک مبحث جدید می‌تواند مطرح کند';
$string['allsubscribe'] = 'مشترک شدن در همهٔ تالارها';
$string['allunsubscribe'] = 'لغو اشتراک همهٔ تالارها';
$string['alreadyfirstpost'] = 'این مطلب، خود اولین مطلب بیان شده در این مباحثه است';
$string['anyfile'] = 'هر فایلی';
$string['areaattachment'] = 'پیوست‌ها';
$string['attachment'] = 'فایل ضمیمه';
$string['attachment_help'] = 'در صورت تمایل می‌توانید یک و یا تعداد بیشتری فایل را ضمیمهٔ هر مطلب نمائید. اگر فایل پیوست شده یک تصویر باشد، در انتهای متن نمایش داده خواهد شد.';
$string['attachmentnopost'] = 'فایل‌های ضمیمه‌ای که شناسهٔ مطلب ندارند را نمی‌توانید صادر کنید';
$string['attachments'] = 'پیوست‌ها';
$string['attachmentswordcount'] = 'پیوست‌ها و شمارش حروف';
$string['availability'] = 'دسترسی';
$string['blockafter'] = 'آستانهٔ جلوگیری از بیان مطلب';
$string['blockafter_help'] = 'این گزینه بیشترین تعداد مطلبی که یک کاربر می‌تواند در یک محدودهٔ زمانی مشخص شده در تالار مطرح کند تعیین می‌کند. کاربرانی که قابلیت mod/forum:postwithoutthrottling را دارند از محدودیت ارسال مستثنی هستند.';
$string['blockperiod'] = 'بازهٔ زمانی جلوگیری از بیان مطلب';
$string['blockperioddisabled'] = 'نامحدود';
$string['blockperiod_help'] = 'می‌توان جلوی ارسال بیش از یک تعداد تعیین شده مطلب در یک مدت زمان تعیین شده توسط شاگردان را گرفت. کاربرانی که قابلیت mod/forum:postwithoutthrottling را دارند از محدودیت ارسال مستثنی هستند.';
$string['blogforum'] = 'تالار معمولی که به صورت وبلاگ مانند نمایش داده می‌شود';
$string['bynameondate'] = 'از {$a->name} در {$a->date}';
$string['cachedef_forum_is_tracked'] = 'وضعیت دنبال کردن تالارگفتگو برای کاربر';
$string['cannotadd'] = 'اضافه شدن مباحثه به این تالار انجام نشد';
$string['cannotadddiscussion'] = 'طرح مباحثه در این تالار نیازمند عضویت در گروه است.';
$string['cannotadddiscussionall'] = 'شما مجوز شروع کردن یک مباحثهٔ جدید برای همهٔ اعضا را ندارید.';
$string['cannotaddsubscriber'] = 'اشتراک کاربر با شناسهٔ {$a} ممکن نشد!';
$string['cannotaddteacherforumto'] = 'اضافه کردن تالار اساتید تبدیل شده به قسمت ۰ در درس ممکن نشد';
$string['cannotcreatediscussion'] = 'ایجاد مباحثهٔ‌جدید ممکن نشد';
$string['cannotcreateinstanceforteacher'] = 'ایجاد نمونهٔ ماژول درسی جدید برای تالار اساتید ممکن نشد';
$string['cannotdeletepost'] = 'نمی‌توانید این مطلب را پاک کنید!';
$string['cannoteditposts'] = 'نمی‌توانید مطالب سایر افراد را ویرایش کنید!';
$string['cannotfinddiscussion'] = 'مباحثهٔ مورد نظر در این تالار پیدا نشد';
$string['cannotfindfirstpost'] = 'پیدا کردن اولین مطلب مطرح شده در این تالار ممکن نشد';
$string['cannotfindorcreateforum'] = 'پیدا کردن یا ایجاد یک تالار اعلانات اصلی برای سایت ممکن نشد';
$string['cannotfindparentpost'] = 'پیدا کردن منشاء اولیهٔ مطلب {$a} ممکن نشد';
$string['cannotmovefromsingleforum'] = 'انتقال مباحثه از یک تالار تک مبحثی ممکن نیست';
$string['cannotmovenotvisible'] = 'این تالار توسط شما قابل مشاهده نیست';
$string['cannotmovetonotexist'] = 'تالار انتخاب شده وجود ندارد. نمی‌توانید مباحثه را به آن منتقل کنید!';
$string['cannotmovetonotfound'] = 'تالار مقصد در این درس پیدا نشد.';
$string['cannotmovetosingleforum'] = 'نمی‌توان مباحثه را به یک تالار تک مبحثی ساده منتقل کرد';
$string['cannotpurgecachedrss'] = 'پاک‌سازی RSS feed های ذخیره شده برای تالارهای مبدأ یا مقصد ممکن نشد. مجوزهای دسترسی فایل خود را بررسی کنید.';
$string['cannotremovesubscriber'] = 'لغو اشتراک کاربر با شناسهٔ {$a} در این تالار ممکن نشد!';
$string['cannotreply'] = 'شما نمی‌توانید به این مطلب پاسخ دهید';
$string['cannotsplit'] = 'مباحثه‌های این تالار را نمی‌شود تقسیم کرد';
$string['cannotsubscribe'] = 'ببخشید، ولی برای مشترک شدن باید عضو یکی از گروه‌ها باشید.';
$string['cannottrack'] = 'لغو پیگیری مطالب این تالار ممکن نبود';
$string['cannotunsubscribe'] = 'لغو اشتراک شما در این تالار ممکن نبود';
$string['cannotupdatepost'] = 'شما نمی‌توانید این مطلب را تغییر دهید';
$string['cannotviewpostyet'] = 'چون هنوز مطلبی نفرستاده‌اید نمی‌توانید  سؤالات دیگران را در این مباحثه ببینید';
$string['cannotviewusersposts'] = 'هیچ مطلب بیان شده‌ای از این کاربر که شما قادر به مشاهده باشید وجود ندارد.';
$string['cleanreadtime'] = 'ساعت علامت زدن مطالب قدیمی به صورت خوانده‌شده';
$string['clicktosubscribe'] = 'شما آبونهٔ این مباحثه نیستید. برای آبونه‌شدن کلیک کنید.';
$string['clicktounsubscribe'] = 'شما آبونهٔ این مباحثه هستید. برای لغو اشتراک کلیک کنید.';
$string['completiondiscussions'] = 'شاگرد باید این تعداد مباحثه را شروع کرده باشد:';
$string['completiondiscussionsgroup'] = 'نیازمند شروع مباحثه';
$string['completiondiscussionshelp'] = 'برای کامل در نظر گرفته شدن باید مباحثه‌هایی شروع شود';
$string['completionposts'] = 'شاگرد باید این تعداد مباحثهٔ جدید را شروع کند یا در مباحثات موجود مطلب بیان کند:';
$string['completionpostsgroup'] = 'نیازمند بیان مطلب';
$string['completionpostshelp'] = 'برای تکمیل شدن باید مباحثه‌های جدیدی شروه شود یا اینکه در مباحثه‌های موجود مطالبی بیان شود';
$string['completionreplies'] = 'شاگرد باید به این تعداد در پاسخ به مطالب موجود در مباحثه‌ها مطلب بیان کند:';
$string['completionrepliesgroup'] = 'نیازمند مشارکت در بحث';
$string['completionreplieshelp'] = 'برای کامل شدن نیازمند مشارکت در مباحثه‌های جاری است';
$string['configcleanreadtime'] = 'ساعتی از روز برای پاک کردن مطالب قدیمی از جدول «خوانده‌شده‌ها».';
$string['configdigestmailtime'] = 'کسانی که می‌خواهند نامه‌ها به صورت خلاصه برایشان فرستاده شود، خلاصهٔ مطالب را به صورت روزانه در یک نامه دریافت خواهند کرد. این قسمت تعیین می‌کند که در چه ساعتی از روز نامهٔ روزانه ارسال خواهد شد (اولین cronی که بعد از این ساعت اجرا می‌شود نامه را ارسال می‌کند).';
$string['configdisplaymode'] = 'نحوهٔ نمایش مباحثات به صورت پیش‌فرض';
$string['configenablerssfeeds'] = 'این گزینه امکان بهره‌مندی از RSS feed ها را برای همهٔ تالارها فراهم می‌کند. برای استفاده از این قابلیت، در تالارهای دلخواه نیز باید RSS feed را فعال کرد.';
$string['configenabletimedposts'] = 'با انتخاب این گزینه، می‌توان هنگام طرح مباحثات جدید محدوده‌ای برای نمایش تعیین کرد.';
$string['configlongpost'] = 'هر مطلبی که طول (تعداد حروف بدون در نظر گرفتن علائم HTML) کمتری از مقدار مشخص شده داشته باشد، کوتاه در نظر گرفته می‌شود. مطالبی که در صفحهٔ اول سایت، صفحه‌های درس‌های دارای قالب اجتماعی، یا صفحه مشخصات فردی کاربران نمایش داده می‌شود، از یک محل قابل قطع در جایی بین مقادیر forum_shortpost و forum_longpost کوتاه می‌شوند.';
$string['configmanydiscussions'] = 'حداکثر تعداد مباحثه‌های نمایش داده شده در هر صفحه از تالارها';
$string['configmaxattachments'] = 'حداکثر فایل‌های قابل پیوست به هر مطلب به صورت پیش‌فرض.';
$string['configmaxbytes'] = 'اندازهٔ پیش‌فرض حداکثر برای همهٔ فایل‌های ضمیمه در تالارهای سایت (محدود به محدودیت‌های هر درس و تنظیمات محلی)';
$string['configoldpostdays'] = 'تعداد روزهایی که پس از آن مطالب قدیمی خود به خود به صورت خوانده‌شده در نظر گرفته می‌شوند.';
$string['configreplytouser'] = 'آیا وقتی که یکی از مطالب تالار با پست الکترونیک فرستاده می‌شود، محتوی آدرس پست الکترونیکی کاربر باشد تا گیرندگان بتوانند به جای استفاده از تالار گفتگو، به صورت شخصی به مطلب مورد نظر پاسخ دهند؟ حتی اگر این گزینه انتخاب شده باشد، باز هم کاربران این اختیار را دارند تا در قسمت مشخصات فردی خود تعیین کنند که آدرس پست الکترونیکشان محرمانه بماند.';
$string['configshortpost'] = 'هر مطلبی که طول (تعداد حروف بدون در نظر گرفتن علائم HTML) کمتری از مقدار مشخص شده داشته باشد، کوتاه در نظر گرفته می‌شود (توضیح پائین را ببینید).';
$string['configtrackingtype'] = 'تنظیم پیش‌فرض برای دنبال‌کردن مطالب';
$string['configtrackreadposts'] = 'با فعال کردن این گزینه هر کاربر می‌تواند خوانده شده/نشده بودن مطالب را دنبال کند.';
$string['configusermarksread'] = 'اگر این گزینه انتخاب شده باشد، کاربران باید خودشان به‌صورت دستی مشخص کنند که مطلبی را خوانده‌اند. در غیر اینصورت، هنگامی که مطلبی مشاهده شود به‌عنوان خوانده شده تلقی می‌گردد.';
$string['confirmsubscribe'] = 'آیا واقعاً می‌خواهید مشترک تالار «{$a}» شوید؟';
$string['confirmsubscribediscussion'] = 'آیا واقعا می‌خواهید که مشترک مباحثهٔ «{$a->discussion}» در تالار گفتگوی «{$a->forum}» شوید؟';
$string['confirmunsubscribe'] = 'آیا واقعاً می‌خواهید اشتراک خود در تالار «{$a}» را لغو کنید؟';
$string['confirmunsubscribediscussion'] = 'آیا واقعا می‌خواهید که اشتراک خود در مباحثهٔ «{$a->discussion}» در تالار گفتگوی «{$a->forum}» را لغو کنید؟';
$string['couldnotadd'] = 'به دلیل بروز یک خطای ناشناخته ارائه مطلب شما امکان پذیر نبود';
$string['couldnotdeletereplies'] = 'متأسفانه این مطلب را نمی‌شود حذف کرد زیرا کسانی مطالبی در پاسخ به آن بیان کرده‌اند';
$string['couldnotupdate'] = 'به دلیل بروز یک خطای ناشناخته به‌روزرسانی مطلب شما امکان پذیر نبود';
$string['crontask'] = 'وظایف مربوط به فرستادن نامه‌ها و نگهداری تالار گفتگو';
$string['cutoffdate'] = 'تاریخ عدم پذیرش';
$string['cutoffdatevalidation'] = 'تاریخ عدم پذیرش نمی‌تواند قبل از مهلت تحویل باشد.';
$string['delete'] = 'حذف';
$string['deleteddiscussion'] = 'این مباحثه حذف شده است';
$string['deletedpost'] = 'این مطلب حذف شده است';
$string['deletedposts'] = 'آن مطالب حذف شده‌اند';
$string['deletesure'] = 'آیا مطمئنید که می‌خواهید این مطلب حذف شود؟';
$string['deletesureplural'] = 'آیا مطمئنید که می‌خواهید این مطلب و تمام مطالبی که در پاسخ به آن بیان شده‌اند حذف شوند؟ ({$a} مطلب)';
$string['digestmailheader'] = 'متن حاضر، خلاصهٔ روزانهٔ مطالب ارسالی جدید به تالارهای گفتگوی {$a->sitename} است. برای تغییر نحوهٔ دریافت پست الکترونیک تالارهای گفتگو، به {$a->userprefs} بروید.';
$string['digestmailprefs'] = 'صفحهٔ مشخصات فردی شما';
$string['digestmailsubject'] = '{$a}: خلاصهٔ مطالب ارسالی به تالار';
$string['digestmailtime'] = 'ساعت ارسال نامه‌های خلاصه';
$string['digestsentusers'] = 'متون خلاصه با موفقیت به {$a} ارسال شد.';
$string['disallowsubscribe'] = 'اشتراک مجاز نیست';
$string['disallowsubscribeteacher'] = 'اشتراک مجاز نیست (به جز برای اساتید)';
$string['disallowsubscription'] = 'اشتراک';
$string['disallowsubscription_help'] = 'پیکربندی این تالار به‌گونه‌ای است که نمی‌توانید در مباحثات آبونه شوید.';
$string['discussion'] = 'مباحثه';
$string['discussionlocked'] = 'این مباحثه قفل شده است و در نتیجه دیگر نمی‌توانید در آن شرکت کنید.';
$string['discussionlockingdisabled'] = 'مباحثات قفل نشوند';
$string['discussionlockingheader'] = 'قفل‌کردن مباحثات';
$string['discussionmoved'] = 'این مباحثه به «{$a}» منتقل شد.';
$string['discussionmovedpost'] = 'این مباحثه به <a href="{$a->discusshref}">این محل</a> واقع در تالار «<a href="{$a->forumhref}">{$a->forumname}</a>» منتقل شد';
$string['discussionname'] = 'نام مباحثه';
$string['discussionnownotsubscribed'] = '{$a->name} از مطالب جدیدی که در مباحثهٔ «{$a->discussion}» در «{$a->forum}» مطرح می‌شوند باخبر <strong>نخواهد</strong> شد';
$string['discussionnowsubscribed'] = '{$a->name} از مطالب جدیدی که به «{$a->discussion}» در «{$a->forum}» ارسال شوند باخبر خواهد شد.';
$string['discussionpin'] = 'سنجاق‌کردن';
$string['discussionpinned'] = 'سنجاق‌شده';
$string['discussionpinned_help'] = 'مباحثه‌های سنجاق‌شده در صدر تالار گفتگو نمایش داده خواهند شد.';
$string['discussions'] = 'مباحثات';
$string['discussionsstartedby'] = 'مباحثاتی که توسط {$a} آغاز شده‌اند';
$string['discussionsstartedbyrecent'] = 'مباحثاتی که اخیراً توسط {$a} آغاز شده‌اند';
$string['discussionsstartedbyuserincourse'] = 'مباحثات شروع شده توسط {$a->fullname} در {$a->coursename}';
$string['discussionsubscribestart'] = 'من را از مطالب جدید در این مباحثه باخبر کن';
$string['discussionsubscribestop'] = 'نمی‌خواهم از مطالب جدید این مباحثه باخبر شوم';
$string['discussionsubscription'] = 'آبونه‌شدن در مباحثه';
$string['discussionsubscription_help'] = 'با آبونه‌شدن در یک مباحثه از مطالب جدیدی که در مباحثه مطرح می‌شوند آگاه خواهید شد.';
$string['discussionunpin'] = 'برداشتن سنجاق';
$string['discussthistopic'] = 'بحث دربارهٔ این موضوع';
$string['displayend'] = 'خاتمهٔ نمایش';
$string['displayend_help'] = 'این گزینه تعیین می‌کند که مطالب تالار از تاریخ بخصوصی به بعد نمایش داده نشوند. توجه کنید که مدیران همیشه می‌توانند مطالب تالار را ببینند.';
$string['displaymode'] = 'نحوهٔ نمایش';
$string['displayperiod'] = 'محدودهٔ نمایش';
$string['displaystart'] = 'شروع نمایش';
$string['displaystart_help'] = 'این گزینه تعیین می‌کند که مطالب تالار از تاریخ بخصوصی به بعد نمایش داده شوند. توجه کنید که مدیران همیشه می‌توانند مطالب تالار را ببینند.';
$string['displaywordcount'] = 'نشان‌دادن تعداد حروف';
$string['displaywordcount_help'] = 'این تنظیم تعیین می‌کند که آیا تعداد حروف هر مطلب در تالار گفتگو باید نشان داده شود یا خیر.';
$string['duedate'] = 'مهلت تحویل';
$string['eachuserforum'] = 'هر نفر می‌تواند فقط یک مباحثه شروع کند';
$string['edit'] = 'ویرایش';
$string['editedby'] = 'توسط {$a->name} ویرایش شد - زمان اصلی طرح مطلب {$a->date} بوده است';
$string['editedpostupdated'] = 'مطلب بیان‌شده توسط {$a} به‌روز شد';
$string['editing'] = 'ویرایش';
$string['emaildigestdefault'] = 'پیش‌فرض ({$a})';
$string['emaildigestsubjectsshort'] = 'فقط موضوع‌ها';
$string['emptymessage'] = 'مطلب ارسالی شما دارای مشکلی است. ممکن است یک مطلب خالی فرستاده باشید، یا فایل ضمیمهٔ آن خیلی بزرگ بوده باشد. تغییرات شما ذخیره نشده است.';
$string['erroremptymessage'] = 'متن ارسالی نمی‌تواند خالی باشد';
$string['erroremptysubject'] = 'موضوع مطلب نمی‌تواند خالی باشد.';
$string['errorenrolmentrequired'] = 'باید در این درس ثبت نام باشید تا به این مطلب دسترسی داشته باشید';
$string['errorwhiledelete'] = 'خطایی هنگام حذف رکورد روی داد.';
$string['eventassessableuploaded'] = 'محتواهایی به سایت ارسال شد.';
$string['eventcoursesearched'] = 'درس جستجو شد';
$string['eventdiscussioncreated'] = 'مباحثه ساخته شد';
$string['eventdiscussiondeleted'] = 'مباحثه حذف شد';
$string['eventdiscussionmoved'] = 'مباحثه منتقل شد';
$string['eventdiscussionpinned'] = 'مباحثه سنجاق شد';
$string['eventdiscussionsubscriptioncreated'] = 'آمونمان در مباحثه ساخته شد';
$string['eventdiscussionsubscriptiondeleted'] = 'آبونمان در مباحثه لغو شد';
$string['eventdiscussionunpinned'] = 'سنجاق مباحثه برداشته شد';
$string['eventdiscussionupdated'] = 'مباحثه به‌روز شد';
$string['eventdiscussionviewed'] = 'مباحثه مشاهده شد';
$string['eventpostcreated'] = 'مطلب بیان شد';
$string['eventpostdeleted'] = 'مطلب حذف شد';
$string['eventpostupdated'] = 'مطلب به‌روز شد';
$string['eventreadtrackingdisabled'] = 'دنبال‌کردن خواندن مطالب غیرفعال شد';
$string['eventreadtrackingenabled'] = 'دنبال‌کردن خواندن مطالب فعال شد';
$string['eventsubscribersviewed'] = 'مشترکین مشاهده شدند';
$string['eventsubscriptioncreated'] = 'اشتراک ساخته شد';
$string['eventsubscriptiondeleted'] = 'اشتراک حذف شد';
$string['eventuserreportviewed'] = 'گزارش کاربر مشاهده شد';
$string['everyonecanchoose'] = 'هر کس که بخواهد می‌تواند مشترک مباحثات این تالار شود';
$string['everyonecannowchoose'] = 'هر کس که بخواهد می‌تواند مشترک مباحثات این تالار شود';
$string['everyoneisnowsubscribed'] = 'همه مشترک مباحثات این تالار شدند';
$string['everyoneissubscribed'] = 'همه مشترک این تالار هستند';
$string['existingsubscribers'] = 'نفر مشترک شده‌اند';
$string['exportdiscussion'] = 'صدور کل مباحثه';
$string['forcedreadtracking'] = 'مجاز بودن دنبال‌کردن اجباری مطالب';
$string['forcedreadtracking_desc'] = 'این امکان را به تالارهای گفتگو می‌دهد که دنبال‌کردن مطالب در تالار گفتگو، به‌خصوص در درس‌هایی که دارای چندین تالار و مطلب هستند، را اجباری کنند. اگر غیرفعال باشد، دنبال‌کردن مطالب در تالارهایی که قبلا دنبال‌کردن مطالب را تحمیل می‌کردند، اختیاری خواهد بود.';
$string['forcesubscribed'] = 'همه بالاجبار در این تالار مشترک هستند';
$string['forum'] = 'تالار گقتگو';
$string['forum:addinstance'] = 'اضافه‌کردن یک تالار گفتگوی جدید';
$string['forum:addnews'] = 'اضافه کردن اعلانات';
$string['forum:addquestion'] = 'اضافه کردن سؤال';
$string['forumauthorhidden'] = 'نویسنده (پنهان)';
$string['forumblockingalmosttoomanyposts'] = 'شما در حال نزدیک شدن به آستانهٔ تعیین شده برای بیان مطلب می‌باشید. محدودیت تعیین شده برابر با {$a->blockafter} مطلب در هر {$a->blockperiod} است و شما در طول {$a->blockperiod} گذشته {$a->numposts} مطلب بیان کرده‌اید.';
$string['forumbodyhidden'] = 'شما نمی‌توانید این مطلب را مشاهده نمائید. شاید دلیل این مسأله این باشد که هنوز در این مباحثه مطلبی بیان نکرده‌اید، حداکثر زمان ویرایش هنوز به‌پایان نرسیده است، مباحثه هنوز شروع نشده است یا اینکه مباحثه منقضی شده است.';
$string['forum:createattachment'] = 'ارسال فایل به پیوست';
$string['forum:deleteanypost'] = 'حذف هر مطلبی (هر زمانی)';
$string['forum:deleteownpost'] = 'حذف مطالب خود (در زمان محدود)';
$string['forum:editanypost'] = 'ویرایش همهٔ مطالب تالار';
$string['forum:exportdiscussion'] = 'صدور کل مباحثه';
$string['forum:exportownpost'] = 'صدور مطالب خود';
$string['forum:exportpost'] = 'صدور مطالب';
$string['forumintro'] = 'توصیف';
$string['forum:managesubscriptions'] = 'مدیریت اشتراک‌ها';
$string['forum:movediscussions'] = 'انتقال مباحثه‌ها';
$string['forumname'] = 'نام تالار';
$string['forum:pindiscussions'] = 'سنجاق‌کردن مباحثه';
$string['forumposts'] = 'مطالب بیان شده در تالارها';
$string['forum:postwithoutthrottling'] = 'مستثنی از محدودیت ارسال مطلب';
$string['forum:rate'] = 'ارزیابی مطالب';
$string['forum:replynews'] = 'پاسخ دادن به اعلانات';
$string['forum:replypost'] = 'پاسخ دادن به مطالب تالار';
$string['forums'] = 'تالارهای گفتگو';
$string['forum:splitdiscussions'] = 'انشقاق مباحثه‌ها';
$string['forum:startdiscussion'] = 'شروع مباحثهٔ جدید';
$string['forumsubjecthidden'] = 'موضوع (پنهان)';
$string['forumtracked'] = 'مطالب خوانده نشده دنبال می‌شوند';
$string['forumtrackednot'] = 'مطالب خوانده نشده دنبال نمی‌شوند';
$string['forumtype'] = 'نوع تالار';
$string['forumtype_help'] = '۵ نوع تالار گفتگو وجود دارد:

* تک مبحثی - یک موضوع برای بحث که هر کسی می‌تواند در مورد آن نظر دهد (با گروه‌های جداگانه نمی‌تواند استفاده شود)
* هر نفر می‌تواند فقط یک مباحثه شروع کند - هر شاگرد می‌تواند فقط یک مباحثهٔ جدید را شروع کند و همه می‌توانند در مورد موضوع مورد بحث نظر دهند ومطالبی ارائه کنند
* تالار پرسش و پاسخ - شاگردان ابتدا باید نظر خودشان را بیان کنند تا بعد بتوانند مطالب بیان شده توسط سایر شاگردان را ببینند
* تالار معمولی که به صورت وبلاگ مانند نمایش داده می‌شود - یک تالار گفتگوی آزاد که هر کسی در هر لحظه می‌تواند در آن بحث جدیدی را شروع کند. موضوعات مورد بحث به همراه پیوند‌هایی برای بحث در مورد هر کدام از موضوع‌ها در یک صفحه نمایش داده می‌شوند
* تالارمعمولی جهت کاربردهای عمومی - یک تالار گفتگوی آزاد که هر کسی در هر لحظه می‌تواند در آن بحث جدیدی را شروع کند';
$string['forum:viewallratings'] = 'مشاهدهٔ امتیازهای داده شده توسط تک تک افراد';
$string['forum:viewanyrating'] = 'مشاهدهٔ ارزیابی‌های نهایی که هر کسی کسب کرده است';
$string['forum:viewdiscussion'] = 'مشاهدهٔ مباحثه‌ها';
$string['forum:viewhiddentimedposts'] = 'مشاهدهٔ مطالب پنهان زمان‌دار';
$string['forum:viewqandawithoutposting'] = 'مشاهدهٔ همیشگی سؤال و جواب‌ها';
$string['forum:viewrating'] = 'مشاهدهٔ ارزیابی نهایی که کسب کرده‌اید';
$string['forum:viewsubscribers'] = 'مشاهدهٔ مشترک‌ها';
$string['generalforum'] = 'تالار معمولی جهت کاربردهای عمومی';
$string['generalforums'] = 'تالارهای عمومی';
$string['inforum'] = 'در {$a}';
$string['introblog'] = 'مطالب این تالار به صورت خودکار از بلاگ‌های کاربران این درس کپی شده‌اند زیرا آن بلاگ‌ها دیگر وجود ندارند';
$string['intronews'] = 'اعلانات و خبرهای عمومی';
$string['introsocial'] = 'یک تالار آزاد برای گفتگو در مورد هر چیزی که بخواهید';
$string['introteacher'] = 'تالاری برای طرح مباحثات و ملاحظات مختص اساتید';
$string['invalidaccess'] = 'این صفحه به طرز صحیح مورد دسترسی قرار نگرفت';
$string['invaliddiscussionid'] = 'شناسهٔ مباحثه اشتباه است یا اینکه این مباحثه دیگر وجود ندارد';
$string['invalidforcesubscribe'] = 'حالت اشتراک نامعتبر';
$string['invalidforumid'] = 'شناسهٔ تالار گفتگو غیرفعال بود.';
$string['invalidparentpostid'] = 'شناسهٔ مطلب منشأ نادرست بود';
$string['invalidpostid'] = 'شناسهٔ مطلب ({$a}) نامعتبر است';
$string['lastpost'] = 'آخرین مطلب';
$string['learningforums'] = 'تالارهای آموزشی';
$string['lockdiscussionafter'] = 'قفل‌کردن مباحثات بعد از مدتی غیرفعال بودن';
$string['lockdiscussionafter_help'] = 'مباحثات می‌توانند پس از گذشت زمان تعیین‌شده از زمان ارسال آخرین مطلب به‌طور خودکار قفل شوند.

کاربرانی که مجوز پاسخ‌دادن به مباحثات قفل‌شده را دارند می‌توانند با پاسخ‌دادن به مباحثه، آن را از حالت قفل‌شده خارج کنند.';
$string['longpost'] = 'مطلب طولانی';
$string['mailnow'] = 'بدون تاخیر برای پایان زمان مهلت ویرایش، همین الان اطلاعیه‌های مربوط به این پست را بفرست';
$string['manydiscussions'] = 'تعداد مباحثات در هر صفحه';
$string['markalldread'] = 'همهٔ مطالب این مباحثه به عنوان خوانده شده علامت‌گذاری شود.';
$string['markallread'] = 'همهٔ مطالب این تالار به عنوان خوانده شده علامت‌گذاری شود.';
$string['markasreadonnotification'] = 'وقتی اطلاعیه ارسال یک پست می‌آید';
$string['markread'] = 'به‌عنوان خوانده‌شده علامت‌گذاری شود';
$string['markreadbutton'] = 'علامت‌گذاری به‌عنوان<br />خوانده‌شده';
$string['markunread'] = 'به‌عنوان خوانده‌نشده علامت‌گذاری شود';
$string['markunreadbutton'] = 'علامت‌گذاری به‌عنوان<br />خوانده‌نشده';
$string['maxattachments'] = 'حداکثر تعداد فایل‌های پیوست';
$string['maxattachments_help'] = 'این گزینه بیشترین تعداد فایل‌هایی که می‌توان به یک مطلب در تالار ضمیمه کرد را تعیین می‌کند.';
$string['maxattachmentsize'] = 'حداکثر حجم فایل پیوست';
$string['maxattachmentsize_help'] = 'این گزینه بیشترین اندازه‌ای که فایل‌های پیوست شده به مطالب تالار می‌توانند داشته باشند را تعیین می‌کند.';
$string['maxtimehaspassed'] = 'متأسفانه مهلت ویرایش این مطلب ({$a}) به پایان رسیده است!';
$string['message'] = 'متن';
$string['messageinboundattachmentdisallowed'] = 'ارسال پاسخ شما ممکن نیست، زیرا شامل یک فایل ضمیمه است و در این تالار گفتگو ضمیمه کردن فایل مجاز نیست.';
$string['messageprovider:digests'] = 'خلاصهٔ مطالب تالارهای مشترک شده';
$string['messageprovider:posts'] = 'مطالب تالارهای مشترک شده';
$string['missingsearchterms'] = 'عبارات مورد جستجوی زیر فقط در نشانه‌گذاری HTML این پیغام ظاهر شده‌اند:';
$string['modeflatnewestfirst'] = 'نمایش مطالب به صورت مسطح (از جدید به قدیمی)';
$string['modeflatoldestfirst'] = 'نمایش مطالب به صورت مسطح (از قدیمی به جدید)';
$string['modenested'] = 'نمایش مطالب به صورت تو در تو';
$string['modethreaded'] = 'نمایش مطالب به صورت رشته‌ای';
$string['modulename'] = 'تالار گفتگو';
$string['modulename_help'] = 'فعالیت« تالار گفتگو» امکان بحث و گفتگوی غیر هم‌زمان (بحث‌هایی که در طی مدت زمانی طولانی صورت می‌گیرند) را برای شرکت‌کنندگان فراهم می‌کند.

انواع مختلفی تالار گفتگو وجود دارند که می‌توانید آنها را انتخاب کنید، مانند یک تالار معمولی که هر کسی می‌تواند در هر زمانی یک مباحثه جدید در آن شروع کند؛ تالاری که هر کدام از شاگردان می‌توانند فقط یک مباحثه در آن شروع کنند؛ یا یک تالار پرسش و پاسخ که شاگردان باید اول خودشان مطلبی را در تالار بیان کنند تا بتوانند مطالب بیان شده توسط سایرین را مشاهده کنند. استاد می‌تواند اجازه دهد که شاگردان به مطالبی که در تالار بیان می‌کنند فایل‌هایی را ضمیمه بکنند. اگر فایل‌های ضمیمه از نوع فایل‌های تصویری باشند، مستقیما در داخل مطلب بیان شده نمایش داده می‌شوند.

شرکت کنندگان می‌توانند در تالارهای گفتگو مشترک شوند تا از مطرح شدن مطالب جدید در تالار با خبر شوند. استاد می‌تواند مشترک شدن را اختیاری یا اجباری کند یا آن را بر روی حالت خودکار قرار دهد و یا اینکه کلا مشترک شدن در تالار را غیر فعال کند. اگر لازم باشد، می‌توان جلوی شاگردان را گرفت تا نتوانند بیشتر از تعداد معینی مطلب در یک مدت زمانی مشخص در تالار بیان کنند؛ این کار می‌تواند جلوی افراد را بگیرد تا نتوانند بحث را به سلطه خود درآورند.

مطالب طرح شده در تالار می‌توانند توسط استاد یا شاگردان (ارزشیابی همکلاسان از کار یکدیگر) امتیازدهی شوند. امتیازها می‌توانند جمع‌آوری شوند تا یک نمره نهایی را تشکیل دهند که در دفتر نمره ثبت شود.

تالارهای گفتگو کاربردهای زیادی دارند، مانند

* یک محل اجتماعی برای شاگردان تا با یکدیگر آشنا شوند
* برای اعلانات درس (با استفاده از یک تالار اعلانات که اشتراک در آن اجباری است)
* برای بحث در مورد محتوا یا منابع مطالعاتی درس
* برای ادامه دادن موضوعی که قبلا به صورت حضوری مطرح شده است
* برای بحث‌های مخصوص اساتید (با استفاده از یک تالار پنهان)
* یک مرکز مشاوره که در آن اساتید و شاگردان می‌توانند مشاوره بدهند
* یک محل پشتیبانی خصوصی برای ارتباط شاگرد-استادی خصوصی (با استفاده از یک تالار گفتگو با گروه‌های جداگانه و به طوری که یک شاگرد عضو هر گروه باشد)
* برای فعالیت‌های ترویجی، مثلا «بازی‌های فکری» برای شاگردان تا فکر کنند و راه حل‌هایی را پیشنهاد دهند';
$string['modulename_link'] = 'تالار گفتگو';
$string['modulenameplural'] = 'تالارهای گفتگو';
$string['more'] = 'بیشتر';
$string['movedmarker'] = '(منتقل شد)';
$string['movethisdiscussionto'] = 'انتقال کل مباحثه به ...';
$string['mustprovidediscussionorpost'] = 'برای انجام صدور، باید شناسهٔ مباحثه یا شناسهٔ مطلب فراهم شده باشد';
$string['myprofileotherdis'] = 'مباحثه‌های شروع‌شده در تالارها';
$string['namenews'] = 'تالار اعلانات';
$string['namenews_help'] = 'تالار اعلانات یک تالار ویژه برای اطلاع‌رسانی است که به صورت خودکار هنگام ایجاد درس‌ها ساخته می‌شود. هر درس می‌تواند فقط یک تالار اعلانات داشته باشد. فقط اساتید و مدیران می‌توانند در تالار اعلانات مطلب بگذارند. بلوک «تابلوی اعلانات»، اعلانات اخیر را نمایش می‌دهد.';
$string['namesocial'] = 'تالار گفتگوی اجتماعی';
$string['nameteacher'] = 'تالار اساتید';
$string['newforumposts'] = 'مطالب جدید تالار';
$string['noattachments'] = 'هیچ فایلی به این مطلب ضمیمه نشده است';
$string['nodiscussions'] = 'هیچ مباحثه‌ای در این تالار شروع نشده است';
$string['nodiscussionsstartedby'] = '{$a} هیچ مباحثه‌ای را شروع نکرده است';
$string['nodiscussionsstartedbyyou'] = 'هنوز هیچ مباحثه‌ای را شروع نکرده‌اید';
$string['noguestpost'] = 'متأسفانه مهمانان مجاز به بیان مطلب در تالار نیستند';
$string['noguesttracking'] = 'متأسفانه مهمانان اجازهٔ دنبال کردن مطالب را ندارند.';
$string['nomorepostscontaining'] = 'مطلب دیگری که شامل «{$a}» باشد پیدا نشد';
$string['nonews'] = 'هنوز اطلاعیه‌ای ارسال نشده است.';
$string['noonecansubscribenow'] = 'هم‌اکنون اشتراک مجاز نیست';
$string['nopermissiontosubscribe'] = 'شما مجوز لازم برای دیدن مشترکین این تالار گفتگو را ندارید';
$string['nopermissiontoview'] = 'شما مجوز لازم برای دیدن این مطلب را ندارید';
$string['nopostforum'] = 'متأسفانه شما اجازهٔ طرح مطلب در این تالار را ندارید';
$string['noposts'] = 'مطلبی نیست';
$string['nopostsmadebyuser'] = '{$a} هیچ ارسالی انجام نداده است';
$string['nopostsmadebyyou'] = 'شما هیچ ارسالی انجام نداده‌اید';
$string['noquestions'] = 'در این تالار هیچ سؤالی مطرح نشده است';
$string['nosubscribers'] = 'هنوز کسی مشترک این تالار نشده است';
$string['notexists'] = 'این مباحثه دیگر وجود ندارد';
$string['nothingnew'] = 'چیز جدیدی برای {$a} نیست';
$string['notingroup'] = 'برای دیدن این تالار باید عضو یکی از گروه‌ها باشید.';
$string['notinstalled'] = 'ماژول تالار گفتگو نصب نیست';
$string['notpartofdiscussion'] = 'این مطلب مربوط به هیچ مباحثه‌ای نیست!';
$string['notrackforum'] = 'دنبال نکردن مطالب خوانده نشده';
$string['notsubscribed'] = 'مشترک شدن';
$string['noviewdiscussionspermission'] = 'شما مجوز مشاهدهٔ مباحثه‌های این تالار را ندارید';
$string['nowallsubscribed'] = 'در همهٔ تالارهای گفتگوی «{$a}» مشترک شدید.';
$string['nowallunsubscribed'] = 'در هیچ یک از تالارهای «{$a}» مشترک نیستید.';
$string['nownotsubscribed'] = 'از این لحظه به بعد {$a->name} از مطالب ارسالی جدید به «{$a->forum}» باخبر نخواهد شد.';
$string['nownottracking'] = '{$a->name} دیگر مطالب «{$a->forum}» را دنبال نمی‌کند.';
$string['nowsubscribed'] = '{$a->name} از مطالب جدیدی که به «{$a->forum}» ارسال شوند باخبر خواهد شد.';
$string['nowtracking'] = '{$a->name} از این لحظه مطالب «{$a->forum}» را دنبال می‌کند.';
$string['numposts'] = '{$a} مطلب';
$string['olderdiscussions'] = 'مباحثه‌های قدیمی‌تر';
$string['oldertopics'] = 'موضوعات مورد بحث قدیمی‌تر';
$string['oldpostdays'] = 'خوانده‌شده پس از چند روز؟';
$string['overviewnumpostssince'] = '{$a} مطلب جدید از زمان آخرین ورود';
$string['overviewnumunread'] = '{$a} مطلب خوانده نشده';
$string['page-mod-forum-discuss'] = 'صفحهٔ مباحثه ماژول تالار گفتگو';
$string['page-mod-forum-view'] = 'صفحهٔ اصلی ماژول تالار گفتگو';
$string['page-mod-forum-x'] = 'هر صفحه‌ای از ماژول تالار گفتگو';
$string['parent'] = 'نمایش منشأ';
$string['parentofthispost'] = 'منشأ این مطلب';
$string['permalink'] = 'پیوند مستقیم';
$string['pluginadministration'] = 'مدیریت تالار گفتگو';
$string['pluginname'] = 'تالار گفتگو';
$string['postadded'] = '<p>مطلب شما با موفقیت ارسال شد.</p> <p>{$a} فرصت دارید تا در صورت تمایل تغییری در آن ایجاد نمائید.</p>';
$string['postaddedsuccess'] = 'مطلب شما با موفقیت ارسال شد.';
$string['postaddedtimeleft'] = '{$a} فرصت دارید تا در صورت تمایل تغییری در آن ایجاد نمائید.';
$string['postbyuser'] = '{$a->post} توسط {$a->user}';
$string['postincontext'] = 'دیدن مباحثهٔ مربوطه';
$string['postmailinfolink'] = 'این یک نسخه از مطلبی است که به {$a->coursename} ارسال شده است.

برای پاسخ دادن بر روی این پیوند کلیک کنید: {$a->replylink}';
$string['postmailnow'] = '<p>این مطلب بلافاصله به همهٔ مشترکین تالار پست خواهد شد.</p>';
$string['postmailsubject'] = '{$a->courseshortname}: {$a->subject}';
$string['postrating1'] = 'بیشتر جنبهٔ فردی دارد';
$string['postrating2'] = 'فردی و جمعی';
$string['postrating3'] = 'بیشتر جنبهٔ جمعی دارد';
$string['posts'] = 'مطالب';
$string['postsmadebyuser'] = 'مطالب مطرح شده توسط {$a}';
$string['postsmadebyuserincourse'] = 'مطالب مطرح شده توسط {$a->fullname} در {$a->coursename}';
$string['posttoforum'] = 'طرح در تالار';
$string['posttomygroups'] = 'ارسال یک نسخه به تمام گروه‌ها';
$string['posttomygroups_help'] = 'با یک بار ارسال، یک نسخه از پیام به تمام گروه‌هایی که به آنها دسترسی دارید ارسال می‌شود. شرکت‌کنندگانی که در گروه‌هایی هستند که شما به آنها دسترسی ندارید، این مطلب را نخواهند دید';
$string['postupdated'] = 'مطلب شما به‌روز شد';
$string['potentialsubscribers'] = 'اعضائی که مشترک نیستند';
$string['processingdigest'] = 'شروع ارسال پست الکترونیک خلاصه به {$a}';
$string['processingpost'] = 'پردازش مطلب {$a}';
$string['prune'] = 'انشقاق';
$string['prunedpost'] = 'یک مباحثهٔ جدید از آن مطلب ایجاد شده است';
$string['pruneheading'] = 'انشقاق مباحثه و انتقال این مطلب به یک مباحثهٔ جدید';
$string['qandaforum'] = 'تالار پرسش و پاسخ';
$string['qandanotify'] = 'این تالار از نوع پرسش و پاسخ است. برای دیدن پاسخ دیگران به این سؤال‌ها، باید ابتدا پاسخ خود را بیان کنید';
$string['re'] = 'در پاسخ به:';
$string['readtherest'] = 'خواندن ادامهٔ مطلب';
$string['removeallforumtags'] = 'حذف همه تگ‌های تالار‌ گفتگو';
$string['replies'] = 'تعداد مطالب';
$string['repliesmany'] = '{$a} پاسخ تا این لحظه';
$string['repliesone'] = '{$a} پاسخ تا این لحظه';
$string['reply'] = 'ارسال پاسخ';
$string['replyforum'] = 'پاسخ به تالار';
$string['reply_handler'] = 'پاسخ‌دادن به مطالب تالارهای گفتگو از طریق پست الکترونیکی';
$string['reply_handler_name'] = 'پاسخ‌دادن به مطالب تالارهای گفتگو';
$string['replytopostbyemail'] = 'با استفاده از پست الکترونیکی می‌توانید به این مطلب پاسخ دهید.';
$string['replytouser'] = 'استفاده از آدرس پست الکترونیک';
$string['resetdigests'] = 'حذف همه ترجیحات خلاصه ارسال تالارگفتگو به ازای کاربر';
$string['resetforums'] = 'حذف مطالب متعلق به';
$string['resetforumsall'] = 'حذف همهٔ مطالب';
$string['resetsubscriptions'] = 'لغو همهٔ اشتراک‌ها';
$string['resettrackprefs'] = 'لغو همهٔ سلایق مربوط به دنبال کردن مطالب تالارها';
$string['rssarticles'] = 'تعداد موارد جدید در RSS';
$string['rssarticles_help'] = 'این گزینه تعداد مواردی (تعداد مباحثات یا تعداد مطالب) که در RSS feed می‌آیند را تعیین می‌کند. معمولاً مقادیر بین ۵ و ۲۰ قابل قبول هستند.';
$string['rsssubscriberssdiscussions'] = 'RSS feed مباحثه‌ها';
$string['rsssubscriberssposts'] = 'RSS feed مطالب';
$string['rsstype'] = 'RSS feed برای این فعالیت';
$string['rsstype_help'] = 'برای فعال کردن RSS feed برای این فعالیت، یکی از گزینه‌های مباحثات یا مطالب را برای شامل شدن در feed انتخاب کنید.';
$string['search'] = 'جستجو';
$string['search:activity'] = 'تالار گفتگو - اطلاعات فعالیت';
$string['searchdatefrom'] = 'مطالب باید بعد از این تاریخ مطرح شده باشند';
$string['searchdateto'] = 'مطالب باید قبل از این تاریخ مطرح شده باشند';
$string['searchforumintro'] = 'لطفاً اصطلاحات مورد جستجو را در یکی یا چند تا از قسمت‌های زیر وارد نمائید:';
$string['searchforums'] = 'جستجو در تالارهای گفتگو';
$string['searchfullwords'] = 'این کلمات باید به صورت کلمهٔ کامل در متن باشند';
$string['searchnotwords'] = 'این کلمات نباید در متن باشند';
$string['searcholderposts'] = 'جستجو در مطالب قدیمی‌تر...';
$string['searchphrase'] = 'عین این عبارت باید در متن باشد';
$string['search:post'] = 'تالار گفتگو - مطالب';
$string['searchresults'] = 'نتایج جستجو';
$string['searchsubject'] = 'موضوع باید شامل این کلمات باشد';
$string['searchuser'] = 'این نام باید با نام نویسندهٔ مطلب یکی باشد';
$string['searchuserid'] = 'شناسهٔ مودل نویسندهٔ مطلب';
$string['searchwhichforums'] = 'تالار مورد جستجو';
$string['searchwords'] = 'این کلمات می‌توانند در هر قسمتی از متن باشند';
$string['seeallposts'] = 'مشاهدهٔ همهٔ مطالب بیان شده توسط این کاربر';
$string['sendstudentnotifications'] = 'باخبرکردن شاگردان';
$string['shortpost'] = 'مطلب کوتاه';
$string['showsubscribers'] = 'نمایش/ویرایش لیست مشترکین';
$string['singleforum'] = 'تک مبحثی';
$string['smallmessage'] = '{$a->user} مطلبی در {$a->forumname} بیان کرد';
$string['startedby'] = 'شروع‌کنندهٔ مباحثه';
$string['subject'] = 'موضوع';
$string['subscribe'] = 'مشترک شدن در این تالار';
$string['subscribeall'] = 'همه مشترک مطالب این تالار شوند';
$string['subscribed'] = 'مشترک‌شده';
$string['subscribeenrolledonly'] = 'متأسفیم، تنها کاربران ثبت نام شده مجاز به مشترک شدن برای دریافت مطالب ارسالی به تالار از طریق پست الکترونیکی هستند.';
$string['subscribenone'] = 'هیچ کس مشترک مطالب این تالار نباشد';
$string['subscribers'] = 'مشترکین';
$string['subscriberstowithcount'] = 'مشترکین «{$a->name}» ({$a->count})';
$string['subscribestart'] = 'من را از مطالب جدیدی که به این تالار فرستاده می‌شوند باخبر کن';
$string['subscribestop'] = 'من نمی‌خواهم از مطالب جدید در این تالار گفتگو باخبر شوم';
$string['subscription'] = 'اشتراک';
$string['subscriptionandtracking'] = 'اشتراک و دنبال‌کردن';
$string['subscriptionauto'] = 'اشتراک خودکار';
$string['subscriptiondisabled'] = 'غیر فعال بودن اشتراک';
$string['subscriptionforced'] = 'اشتراک اجباری';
$string['subscription_help'] = 'در صورتی که مشترک یک تالار گفتگو باشید اطلاعیه‌هایی دربارهٔ مطالب جدیدی که در تالار بیان می‌شود دریافت خواهید کرد. اگر چه معمولاً می‌توانید انتخاب کنید که می‌خواهید مشترک شوید یا خیر، ولی گاهی اشتراک به صورت اجباری تعیین شده است و همه اطلاعیه‌های تالار را دریافت می‌کنند.';
$string['subscriptionmode'] = 'وضعیت اشتراک';
$string['subscriptionmode_help'] = 'وقتی شرکت کننده‌ای در یک تالار گفتگو مشترک می‌شود یعنی اطلاعیه‌های مطالب تالار برایش فرستاده می‌شود.

۴ وضعیت برای اشتراک وجود دارد:

* اشتراک اختیاری - شرکت کنندگان می‌توانند انتخاب کنند که مشترک باشند یا خیر
* اشتراک اجباری - همه مشترک هستند و نمی‌توانند اشتراکشان را لغو کنند
* اشتراک خودکار - همه در ابتدا مشترک هستند ولی هر زمان که بخواهند می‌توانند اشتراکشان را لغو کنند
* غیر فعال بودن اشتراک - مشترک شدن مجاز نیست

توجه: هرگونه تغییر در وضعیت اشتراک تنها روی کاربرانی که بعدا در درس ثبت‌نام شوند تاثیر خواهد داشت؛ نه کاربران موجود.';
$string['subscriptionoptional'] = 'اشتراک اختیاری';
$string['subscriptions'] = 'اشتراک‌ها';
$string['thisforumhasduedate'] = 'مهلت ارسال برای پست کردن مطلب در این تالارگفتگو  {$a} است.';
$string['thisforumisdue'] = 'مهلت ارسال برای پست کردن مطلب در این تالارگفتگو  {$a} بود.';
$string['thisforumisthrottled'] = 'در این تالار، تعداد مطالبی که می‌توان در طول یک بازهٔ زمانی خاص ارائه کرد محدود است. این محدودیت در حال حاضر برابر با {$a->blockafter} مطلب در هر {$a->blockperiod} است.';
$string['timedhidden'] = 'وضعیت زمان‌بندی‌شده: پنهان شده از شاگردان';
$string['timedposts'] = 'مطالب زمان‌دار';
$string['timedvisible'] = 'وضعیت زمان‌بندی‌شده: قابل مشاهده توسط شاگردان';
$string['timestartenderror'] = 'تاریخ خاتمهٔ نمایش نمی‌تواند زودتر از تاریخ شروع نمایش باشد';
$string['trackforum'] = 'دنبال کردن مطالب خوانده نشده';
$string['tracking'] = 'دنبال کردن';
$string['trackingoff'] = 'ناممکن';
$string['trackingon'] = 'اجباری';
$string['trackingoptional'] = 'به اختیار کاربران';
$string['trackingtype'] = 'دنبال کردن مطالب';
$string['trackingtype_help'] = 'دنبال‌کردن مطالب، با هایلایت کردن مطالب جدید، شرکت‌کنندگان را قادر می‌سازد تا بتوانند به‌سادگی متوجه شوند که چه مطالبی را هنوز نخوانده‌اند.

اگر روی «به‌اختیار کاربران» قرار داده شود، شرکت‌کنندگان می‌توانند خودشان با استفاده از پیوندی در بلوک مدیریت انتخاب کنند که آیا می‌خواهند این این ویژگی را فعال یا غیرفعال کنند. (کاربران باید دنبال‌کردن تالار گفتگو را هم در ترجیهات تالار گفتگو برای خود فعال کنند.)

اگر «مجاز بودن دنبال‌کردن اجباری مطالب» در مدیریت سایت فعال شده باشد، آنگاه گزینهٔ دیگری هم در اختیار خواهد بود - اجباری. به این معنی که بدون توجه به ترجیحات کاربران برای تالار گفتگو، دنبال‌کردن مطالب همیشه فعال است.';
$string['trackreadposts_header'] = 'دنبال کردن تالارهای گفتگو';
$string['unread'] = 'خوانده نشده';
$string['unreadposts'] = 'مطالب خوانده نشده';
$string['unreadpostsnumber'] = '{$a} مطلب خوانده نشده';
$string['unreadpostsone'] = '۱ مطلب خوانده نشده';
$string['unsubscribe'] = 'لغو اشتراک در این تالار';
$string['unsubscribeall'] = 'لغو اشتراک در همهٔ تالارها';
$string['unsubscribeallconfirm'] = 'شما هم‌اکنون در {$a->forums} تالار گفتگو و {$a->discussions} مباحثه مشترک هستید. آیا واقعاً می‌خواهید که اشتراک خود را در همهٔ تالارها و مباحثه‌ها لغو کرده و اشتراک خودکار در مباحثه‌ها را غیرفعال نمائید؟';
$string['unsubscribealldone'] = 'همهٔ اشتراک‌های اختیاری شما در تالارها حذف شدند. شما همچنان ابلاغ‌هایی از تالارهایی که دارای اشتراک اجباری هستند دریافت خواهید کرد. برای مدیریت ابلاغیه‌های تالار گفتگو به ترجیحات اطلاعیه‌ها در صفحهٔ ترجیحات خود بروید.';
$string['unsubscribeallempty'] = 'شما در هیچ تالاری مشترک نیستید. برای غیرفعال کردن تمام ابلاغیه‌ها از طرف این کارگزار به ترجیحات اطلاعیه‌ها در صفحهٔ ترجیحات خود بروید.';
$string['unsubscribed'] = 'اشتراک لغو شد';
$string['unsubscribediscussion'] = 'لغو اشتراک در این مباحثه';
$string['unsubscribeshort'] = 'لغو اشتراک';
$string['usermarksread'] = 'علامت‌گذاری خواندن مطالب به صورت دستی';
$string['viewalldiscussions'] = 'مشاهدهٔ همهٔ مباحثه‌ها';
$string['viewthediscussion'] = 'مشاهدهٔ مباحثه';
$string['warnafter'] = 'آستانهٔ هشدار جلوگیری از بیان مطلب';
$string['warnafter_help'] = 'می‌توان به شاگردانی که در حال نزدیک شدن به محدودیت تعیین شده برای ارسال مطلب هستند هشدار داد. این گزینه تعیین می‌کند که پس از ارسال چند مطلب باید به آنها اخطار داد. کاربرانی که قابلیت mod/forum:postwithoutthrottling را دارند از محدودیت ارسال مستثنی هستند.';
$string['warnformorepost'] = 'هشدار! بیش از یک مباحثه در این تالار گفتگو وجود دارد - استفاده از جدیدترین';
$string['yournewquestion'] = 'سؤال جدید شما';
$string['yournewtopic'] = 'مباحثهٔ جدید شما';
$string['yourreply'] = 'پاسخ شما';
