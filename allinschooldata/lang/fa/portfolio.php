<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'portfolio', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   portfolio
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addtoportfolio'] = 'صدور به پورتفولیو';
$string['availableformats'] = 'قالب‌های صدور موجود';
$string['commonportfoliosettings'] = 'تنظیمات مشترک پورتفولیوها';
$string['commonsettingsdesc'] = '<p>اینکه یک انتقال در زمانی «متوسط» انجام خواهد شد یا «زیاد» روی تصمیم گیری کاربر برای اینکه آیا می‌تواند برای کامل شدن انتقال منتظر بماند یا خیر تاثیر می‌گذارد.</p><p>انتقال‌هایی که اندازه‌شان کمتر از آستانهٔ متوسط است، بلافاصله و بدون سوال از کاربر انجام می‌شوند. برای انتقال‌های «متوسط» و «زیاد» به کاربر هشدار داده می‌شوند که انتقال ممکن است کمی به طول بیانجامد و کاربر می‌تواند انتخاب کند که عملیات انجام شود یا خیر.</p><p>برخی از پلاگین‌های پورتفولیو ممکن است این تنظیمات را کاملاً نادیده بگیرند و تمام انتقال‌ها را اجباراً در صف قرار دهند.</p>';
$string['configexport'] = 'پیکربندی داده‌های صدور';
$string['configplugin'] = 'پیکربندی پلاگین پورتفولیو';
$string['configure'] = 'پیکربندی';
$string['confirmcancel'] = 'آیا مطمئن هستید که می‌خواهید این صدور را لغو کنید؟';
$string['confirmexport'] = 'لطفاً این صدور را تائید کنید';
$string['confirmsummary'] = 'خلاصهٔ صدور شما';
$string['disabled'] = 'متاسفیم، ولی صدور پورتفولیوها در این سایت فعال نشده است';
$string['disabledinstance'] = 'غیر فعال';
$string['enabled'] = 'فعال بودن پورتفولیوها';
$string['enableddesc'] = 'به مدیرها اجازه می‌دهد تا سیستم‌های راه دور را برای کاربران پیکربندی کنند تا محتوایشان را به آنها صادر کنند';
$string['exportedpreviously'] = 'صدورهای قبلی';
$string['exportexpireddesc'] = 'شما سعی کردید که صدور برخی از اطلاعات را تکرار کنید، یا اینکه یک صدور خالی را شروع کنید. برای
انجام درست آن کار باید به محل قبلی بازگردید و دوباره شروع کنید. این حالت گاهی به دلیل اینکه بعد از کامل‌شدن صدور از دکمهٔ بازگشت در مرورگر خود استفاده کرده‌اید، یا به دلیل نشانه‌گذاری یک آدرس اینترنتی اشتباه رخ می‌دهد.';
$string['exporting'] = 'صدور به پورتفولیو';
$string['exportingcontentfrom'] = 'صادر کردن محتوا از {$a}';
$string['exportingcontentto'] = 'صادر کردن محتوا به {$a}';
$string['format_leap2a'] = 'قالب پورتفولیوی Leap2A';
$string['highdbsizethreshold'] = 'اندازهٔ پایگاه دادهٔ انتقال زیاد';
$string['highdbsizethresholddesc'] = 'تعداد رکوردهای پایگاه داده که اگر پایگاه داده‌ای بیشتر از آن تعداد رکورد داشته باشد، به عنوان یک پایگاه دادهٔ بزرگ که انتقالش زمان زیادی به طول می‌انجامد در نظر گرفته می‌شود';
$string['highfilesizethreshold'] = 'اندازهٔ فایل انتقال زیاد';
$string['highfilesizethresholddesc'] = 'فایل‌هایی که اندازه‌شان بیشتر از این مقدار باشد به عنوان فایل‌های حجیم که انتقالشان زمان زیادی به طول می‌انجامد در نظر گرفته می‌شوند';
$string['instancesaved'] = 'پورتفولیو با موفقیت ذخیره شد';
$string['manageportfolios'] = 'مدیریت پورتفولیوها';
$string['manageyourportfolios'] = 'مدیریت پورتفولیوهای شما';
$string['moderatedbsizethreshold'] = 'اندازهٔ پایگاه دادهٔ انتقال متوسط';
$string['moderatedbsizethresholddesc'] = 'تعداد رکوردهای پایگاه داده که اگر پایگاه داده‌ای بیشتر از آن تعداد رکورد داشته باشد، به عنوان یک پایگاه دادهٔ متوسط که انتقالش زمان متوسطی به طول می‌انجامد در نظر گرفته می‌شود';
$string['moderatefilesizethreshold'] = 'اندازهٔ فایل انتقال متوسط';
$string['moderatefilesizethresholddesc'] = 'فایل‌هایی که اندازه‌شان بیشتر از این مقدار باشد، به عنوان فایل‌های متوسط که انتقالشان در زمان متوسطی انجام می‌شود در نظر گرفته می‌شوند';
$string['notyetselected'] = 'هنوز انتخاب نشده است';
$string['off'] = 'فعال ولی پنهان';
$string['on'] = 'فعال و قابل مشاهده';
$string['plugin'] = 'پلاگین پورتفولیو';
$string['portfolios'] = 'پورتفولیوها';
$string['returntowhereyouwere'] = 'بازگشت به جایی که بودید';
$string['save'] = 'ذخیره';
$string['selectedformat'] = 'قالب صدور انتخاب شده';
$string['selectplugin'] = 'مقصد را انتخاب کنید';
