<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradereport_history', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   gradereport_history
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allgradeitems'] = 'تمام موارد';
$string['allgraders'] = 'همهٔ نمره‌دهنده‌ها';
$string['datefrom'] = 'از تاریخ';
$string['datetime'] = 'روز و ساعت';
$string['dateto'] = 'تا تاریخ';
$string['excluded'] = 'از محاسبات کنار گذاشته شده';
$string['feedbacktext'] = 'متن بازخورد';
$string['finishselectingusers'] = 'اتمام انتخاب کاربران';
$string['foundnusers'] = '{$a} کاربر پیدا شد';
$string['gradenew'] = 'نمرهٔ تجدیدنظرشده';
$string['gradeold'] = 'نمرهٔ اصلی';
$string['grader'] = 'نمره‌دهنده';
$string['historyperpage'] = 'موارد تاریخچه‌ای در هر صفحه';
$string['historyperpage_help'] = 'این تنظیم تعداد موارد تاریخچه‌ای که در هر صفحه نمایش داده می‌شود را تعیین می‌کند.';
$string['pluginname'] = 'تاریخچهٔ نمره';
$string['revisedonly'] = 'فقط نمرات تجدیدنظرشده';
$string['revisedonly_help'] = 'فقط نمراتی که تجدیدنظر شده‌اند نمایش داده شوند. با انتخاب این گزینه فقط اطلاعاتی که موجب تغییر نمره می‌شوند لیست خواهند شد.';
$string['selectedusers'] = 'کاربران انتخاب‌شده';
$string['selectuser'] = 'انتخاب کاربر';
$string['selectusers'] = 'انتخاب کاربران';
$string['source'] = 'منشأ';
