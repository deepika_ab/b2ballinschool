<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_tag_youtube', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_tag_youtube
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['anycategory'] = 'هر طبقه‌ای';
$string['apierror'] = 'کلید API‌یوتیوب تنظیم نشده است. با مدیر سامانه‌تان تماس بگیرید.';
$string['apikey'] = 'کلید API';
$string['apikeyinfo'] = 'یک <a href="https://developers.google.com/youtube/v3/getting-started">کلید API گوگل</a> برای سایت مودل‌تان بگیرید.';
$string['autosvehicles'] = 'خودروها و وسائل نقلیه';
$string['category'] = 'طبقه';
$string['comedy'] = 'کمدی';
$string['configtitle'] = 'عنوان';
$string['education'] = 'آموزش';
$string['entertainment'] = 'سرگرمی';
$string['filmsanimation'] = 'فیلم و کارتون';
$string['gadgetsgames'] = 'بازی و اسباب‌بازی';
$string['howtodiy'] = 'راهنما و خودآموز';
$string['music'] = 'موسیقی';
$string['newspolitics'] = 'اخبار و سیاست';
$string['numberofvideos'] = 'تعداد فیلم‌ها';
$string['peopleblogs'] = 'مردم و بلاگ‌ها';
$string['petsanimals'] = 'حیوانات خانگی و جانوران';
$string['pluginname'] = 'یوتیوب';
$string['scienceandtech'] = 'دانش و فناوری';
$string['sports'] = 'ورزشی';
$string['tag_youtube:addinstance'] = 'اضافه‌کردن یک بلوک یوتیوب جدید';
$string['travel'] = 'مسافرت و اماکن';
