<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradereport_singleview', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   gradereport_singleview
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['all_grades'] = 'تمام نمره‌ها';
$string['assessmentname'] = 'مورد نمره';
$string['blanks'] = 'نمره‌های خالی';
$string['bulkappliesto'] = 'برای';
$string['bulkfor'] = 'نمرات برای {$a}';
$string['bulkinsertvalue'] = 'درج مقدار';
$string['bulklegend'] = 'درج انبوه';
$string['bulkperform'] = 'انجام درج انبوه';
$string['entrypage'] = 'نمره‌دادن به کاربر یا موردِ نمره';
$string['exclude'] = 'صرف‌نظر شدن';
$string['excludeall'] = 'صرف‌نظر کردن از تمام نمره‌ها';
$string['excludenone'] = 'هیچ‌کدام از نمره‌ها صرف‌نظر نشوند';
$string['filtergrades'] = 'نمایش نمره‌های مربوط به {$a}.';
$string['gradeitem'] = 'مورد نمره: {$a}';
$string['itemsperpage'] = 'مورد در صفحه';
$string['override'] = 'بازنویسی';
$string['overrideall'] = 'بازنویسی تمام نمره‌ها';
$string['overridenone'] = 'هیچ‌کدام از نمره‌ها بازنویسی نشوند';
$string['pluginname'] = 'نمای تکی';
$string['save'] = 'ذخیره';
$string['savegradessuccess'] = 'نمره‌های مربوط به {$a} مورد تعیین شدند';
$string['selectgrade'] = 'انتخاب مورد نمره...';
$string['selectuser'] = 'انتخاب کاربر...';
