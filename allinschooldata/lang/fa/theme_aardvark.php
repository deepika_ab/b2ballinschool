<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_aardvark', language 'fa', branch 'MOODLE_33_STABLE'
 *
 * @package   theme_aardvark
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['android'] = 'آدرس فروشگاه گوگل پلی';
$string['androiddesc'] = 'آدرس صفحهٔ فروشگاه گوگل پلی خود را وارد کنید. (مثال: https://play.google.com/store/apps/developer?id=mycollege)';
$string['apple'] = 'آدرس اپ‌استور اپل';
$string['appledesc'] = 'آدرس صفحهٔ اپ‌استور خود را وارد کنید. (مثال: https://itunes.apple.com/gb/artist/my-college/id123456789)';
$string['basicheading'] = 'تنظیمات ابتدایی';
$string['basicheadingdesc'] = 'بخش‌های ابتدایی پوسته را سفارشی کنید.';
$string['ceop'] = 'سازمان بهره‌برداری از کودکان و محافظت آنلاین';
$string['ceopaus'] = 'فعال‌کردن استرالیا';
$string['ceopdesc'] = 'فعال کردن گزارش سازمان بهره‌برداری از کودکان و محافظت آنلاین.';
$string['ceopnone'] = 'غیرفعال';
$string['ceopuk'] = 'فعال‌کردن پادشاهی متحده';
$string['choosereadme'] = 'آردوارک یک پوستهٔ سه ستونه برای مودل ۲٫۹+ است که در ابتدا توسط شان دابنی و بر مبنای bootstrap برای <a href="http://www.newbury-college.ac.uk">کالج نیوبری</a> ساخته شده بود. نسخه‌های قدیمی‌تر برای مودل ۲٫۸ و پائین‌تر موجود هستند.';
$string['configtitle'] = 'آردوارک';
$string['copyright'] = 'کپی‌رایت';
$string['copyrightdesc'] = 'نام سازمان شما';
$string['credit'] = 'بر اساس پوستهٔ اصلی ساخته شده توسط شان دابنی';
$string['customcss'] = 'CSS سفارشی';
$string['customcssdesc'] = 'هر دستور CSS ی که در این قسمت وارد کنید در تمام صفحه‌ها اعمال خواهد شد تا سفارشی کردن این پوسته به‌راحتی امکان‌پذیر باشد.';
$string['disclaimer'] = 'سلب مسئولیت';
$string['disclaimerdesc'] = 'محتوای این بلوک متنی در پایین تمام صفحه‌ها نمایش داده خواهد شد.';
$string['facebook'] = 'آدرس فیس‌بوک';
$string['facebookdesc'] = 'آدرس صفحهٔ فیس‌بوک خود را وارد کنید. (مثال: http://www.facebook.com/mycollege)';
$string['flickr'] = 'آدرس فلیکر';
$string['flickrdesc'] = 'آدرس صفحهٔ فلیکر خود را وارد کنید. (مثال: http://www.flickr.com/mycollege)';
$string['footeroptdesc'] = 'سفارشی کردن اجزای ابتدایی پایین صفحه در پوسته.';
$string['footeroptheading'] = 'پایین صفحه';
$string['frontpageheading'] = 'صفحهٔ اول';
$string['frontpageheadingdesc'] = 'سفارشی کردن اجزای مربوط به صفحهٔ اول در پوسته مانند اطلاعیه‌های کاربران و تاریخ.';
$string['generalalert'] = 'اطلاعیه عمومی';
$string['generalalertdesc'] = 'نمایش دادن اطلاعیه‌ای در صفحهٔ اول سایت برای مطلع‌کردن کاربران ازیک رویداد یا وضعیت خاص.';
$string['googleplus'] = 'آدرس گوگل‌پلاس';
$string['googleplusdesc'] = 'آدرس پروفایل گوگل‌پلاس خود را وارد کنید. (مثال: http://plus.google.com/107817105228930159735)';
$string['instagram'] = 'آدرس اینستاگرام';
$string['instagramdesc'] = 'آدرس صفحهٔ اینستاگرام خود را وارد کنید. (مثال: https://www.instagram.com/mycollege)';
$string['linkedin'] = 'آدرس لینکدین';
$string['linkedindesc'] = 'آدرس پروفایل لینکدین خود را وارد کنید. (مثال: http://www.linkedin.com/company/mycollege)';
$string['maincolor'] = 'رنگ اصلی';
$string['maincolordesc'] = 'رنگ اصلی پوسته را تغییر دهید. این رنگ شامل رنگ اشاره به منوها، اشاره به پیوندها و دکمه‌ها می‌شود.';
$string['pinterest'] = 'آدرس پینترست';
$string['pinterestdesc'] = 'آدرس صفحهٔ پینترست خود را وارد کنید. (مثال: http://pinterest.com/mycollege)';
$string['pluginname'] = 'آردوارک';
$string['region-side-post'] = 'چپ';
$string['region-side-pre'] = 'راست';
$string['snapchat'] = 'آدرس اسنپ‌چت';
$string['snapchatdesc'] = 'آدرس پروفایل اسنپ‌چت خود را وارد کنید. (مثال: https://www.snapchat.com/add/mycollege)';
$string['socialiconsheading'] = 'آیکن‌های شبکه‌های اجتماعی';
$string['socialiconsheadingdesc'] = 'آیکن‌های شبکه‌های اجتماعی را سفارشی کنید.';
$string['twitter'] = 'آدرس توییتر';
$string['twitterdesc'] = 'آدرس صفحهٔ توییتر خود را وارد کنید. (مثال: http://www.twitter.com/mycollege)';
$string['website'] = 'آدرس وب‌سایت';
$string['websitedesc'] = 'آدرس وب‌سایت اصلی خود را وارد کنید. (مثال: http://www.mycollege.ac.uk)';
$string['wikipedia'] = 'آدرس ویکی‌پدیا';
$string['wikipediadesc'] = 'آدرس صفحهٔ ویکی‌پدیای خود را وارد کنید. (مثال: http://en.wikipedia.org/wiki/mycollege)';
$string['youtube'] = 'آدرس یوتیوب';
$string['youtubedesc'] = 'آدرس کانال یوتیوب خود را وارد کنید. (مثال: http://www.youtube.com/mycollege)';
