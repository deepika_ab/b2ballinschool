<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'resource', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   resource
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['clicktodownload'] = 'برای دریافت فایل روی پیوند {$a} کلیک کنید.';
$string['clicktoopen2'] = 'برای مشاهدهٔ فایل روی پیوند {$a} کلیک کنید.';
$string['configdisplayoptions'] = 'تمام گزینه‌هایی که می‌خواهید موجود باشند را انتخاب کنید. تنظیمات فعلی منابع موجود تغییری نخواهد کرد. برای انتخاب چند گزینه کلید کنترل (Ctrl) را نگه دارید.';
$string['configframesize'] = 'وقتی که محتوای یک صفحهٔ وب یا یک فایل در داخل یک قاب نمایش داده می‌شود، این مقدار ارتفاع قاب بالایی (که شامل نوار راهبری است) را (بر حسب پیکسل) تعیین می‌کند.';
$string['configpopup'] = 'هنگام اضافه‌کردن یک منبع جدید که قابل نمایش داده شدن در یک پنجرهٔ popup است، آیا این گزینه باید به‌طور پیش‌فرض فعال باشد؟';
$string['configpopupdirectories'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید پیوندهای دایرکتوری را نمایش دهند؟';
$string['configpopupheight'] = 'ارتفاع پیش‌فرض برای پنجره‌های جدید popup باید چقدر باشد؟';
$string['configpopuplocation'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید نوار آدرس را نمایش دهند؟';
$string['configpopupmenubar'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید نوار منو را نمایش دهند؟';
$string['configpopupresizable'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید قابل تغییر اندازه باشند؟';
$string['configpopupscrollbars'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید قابل اسکرول باشند؟';
$string['configpopupstatus'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید نوار وضعیت را نمایش دهند؟';
$string['configpopuptoolbar'] = 'آیا پنجره‌های popup به‌طور پیش‌فرض باید نوار ابزار را نمایش دهند؟';
$string['configpopupwidth'] = 'عرض پیش‌فرض برای پنجره‌های جدید popup باید چقدر باشد؟';
$string['contentheader'] = 'محتوا';
$string['displayoptions'] = 'گزینه‌های موجود برای نحوهٔ نمایش';
$string['displayselect'] = 'نمایش';
$string['displayselectexplain'] = 'نحوهٔ نمایش فایل را انتخاب کنید. متاسفانه تمام انواع نمایش برای همهٔ فایل‌ها مناسب نیستند.';
$string['displayselect_help'] = 'این گزینه، همراه با نوع فایل و اینکه مرورگر اجازهٔ جاسازی شدن را بدهد یا خیر، نحوهٔ نمایش فایل را تعیین می‌کند. مقادیر قابل تعیین به این شرح هستند:

* خودکار - بهترین نحوهٔ نمایش به صورت خودکار بر اساس نوع فایل انتخاب می‌شود
* جاسازی - محتوای فایل به همراه توصیف آن و تمام بلوک‌ها در داخل صفحه و در زیر نوار راهبری نمایش داده می‌شود.
* اجبار به دریافت فایل - کاربر مجبور به دریافت و ذخیرهٔ فایل می‌شود
* باز شدن - تنها محتوای فایل در پنجرهٔ مرورگر نمایش داده می‌شود
* در پنجرهٔ pop-up - فایل در یک پنجرهٔ جدید که فاقد منوها و نوار آدرس است نمایش داده می‌شود
* در داخل یک قاب - محتوای فایل در داخل یک چارچوب زیر نوار راهبری و توصیف فایل قرار می‌گیرد
* پنجرهٔ جدید - محتوای فایل در یک پنجرهٔ جدید که دارای منوها و نوار آدرس است نمایش داده می‌شود';
$string['dnduploadresource'] = 'ساختن یک منبع از نوع فایل';
$string['filenotfound'] = 'متاسفانه فایل پیدا نشد.';
$string['filterfiles'] = 'استفاده از فیلترها روی محتوای فایل';
$string['filterfilesexplain'] = 'نوع اعمال فیلتر روی محتوای فایل را انتخاب کنید. لطفا در نظر داشته باشید که اعمال فیلتر ممکن است برخی از فایل‌های Flash و اپلت‌های جاوا را دچار مشکل کند. لطفا مطمئن شوید که تمام فایل‌های متنی با کدگذاری UTF-8 باشند.';
$string['framesize'] = 'ارتفاع قاب';
$string['legacyfilesactive'] = 'فعال';
$string['modulename'] = 'فایل';
$string['modulename_help'] = 'با استفاده از ماژول فایل می‌توان یک فایل را به‌عنوان یک منبع درسی به درس اضافه کرد. اگر امکان‌پذیر باشد، فایل در داخل درس نمایش داده خواهد شد؛ در غیر این صورت پنجرهٔ دریافت فایل به شاگردان نشان داده می‌شود و آنها می‌توانند فایل را دریافت کنند. «فایل» می‌تواند شامل فایل‌های کمکی باشد، مثلا یک صفحهٔ HTML می‌تواند عکس‌ها یا اشیاء فِلَش جاسازی‌شده‌ای داشته باشد.

توجه داشته باشید که شاگردان باید نرم‌افزارهای مناسب برای باز کردن فایل را بر روی کامپیوترشان داشته باشند.

از فایل می‌توان این استفاده‌ها را کرد

* برای به‌اشتراک‌گذاشتن یادداشت‌های سخنرانی یا درسی که در کلاس ارائه شده است
* برای شامل کردن یک وب‌سایت کوچک به‌عنوان یک منبع درسی
* برای فراهم کردن فایل‌های اولیهٔ نرم‌افزارهای خاص (مثلا فایل <span dir="ltr" style="direction:ltr;display:inline-block\'">.psd</span> فتوشاپ) تا شاگردان بتوانند آنها را ویرایش کنند و برای ارزیابی تحویل دهند.';
$string['modulenameplural'] = 'فایل‌ها';
$string['notmigrated'] = 'متاسفانه نوع قدیمی منبع ({$a}) هنوز منتقل نشده است.';
$string['optionsheader'] = 'اختیارات';
$string['page-mod-resource-x'] = 'هر صفحه‌ای از ماژول فایل';
$string['pluginadministration'] = 'مدیریت ماژول فایل';
$string['pluginname'] = 'فایل';
$string['popupheight'] = 'ارتفاع پنجرهٔ pop-up (به پیکسل)';
$string['popupheightexplain'] = 'ارتفاع پیش‌فرض تعیین‌شده برای پنجرهٔ popup.';
$string['popupresource'] = 'این منبع باید در یک پنجرهٔ popup ظاهر شود.';
$string['popupresourcelink'] = 'اگر نشد، اینجا کلیک کنید: {$a}';
$string['popupwidth'] = 'عرض پنجرهٔ pop-up (به پیکسل)';
$string['popupwidthexplain'] = 'عرض پیش‌فرض تعیین‌شده برای پنجرهٔ popup.';
$string['printintro'] = 'نمایش توصیف منبع';
$string['printintroexplain'] = 'نمایش دادن توصیف منبع در زیر محتوا؟ در صورت انتخاب برخی از انواع نمایش، حتی اگر این گزینه فعال شده باشد هم توصیف نمایش داده نمی‌شود.';
$string['resource:addinstance'] = 'اضافه‌کردن یک منبع جدید';
$string['resourcecontent'] = 'فایل‌ها و زیرپوشه‌ها';
$string['resourcedetails_sizedate'] = '{$a->size} {$a->date}';
$string['resourcedetails_sizetype'] = '{$a->size} {$a->type}';
$string['resourcedetails_sizetypedate'] = '{$a->size} {$a->type} {$a->date}';
$string['resourcedetails_typedate'] = '{$a->type} {$a->date}';
$string['resource:view'] = 'مشاهدهٔ منبع';
$string['search:activity'] = 'فایل';
$string['selectmainfile'] = 'لطفا با کلیک بر روی آیکن کنار نام فایل، فایل اصلی را انتخاب کنید.';
$string['showdate'] = 'نمایش تاریخ ارسال/تغییر';
$string['showdate_desc'] = 'نشان‌دادن تاریخ ارسال/تغییر در صفحهٔ درس؟';
$string['showdate_help'] = 'در کنار پیوند مربوط به فایل، تاریخ ارسال/تغییر نمایش داده می‌شود.

اگر این منبع شامل چند فایل باشد، تاریخ ارسال/تغییر «فایل شروع» نشان داده می‌شود.';
$string['showsize'] = 'نمایش اندازه';
$string['showsize_desc'] = 'نمایش حجم فایل در صفحهٔ درس؟';
$string['showsize_help'] = 'در کنار پیوند مربوط به فایل، اندازهٔ فایل (مثلا ۳٫۱ مگابایت) نمایش داده می‌شود.

اگر این منبع شامل چند فایل باشد، مجموع اندازه تمام فایل‌ها نمایش داده می‌شود.';
$string['showtype'] = 'نمایش نوع';
$string['showtype_desc'] = 'نمایش نوع فایل (به‌طور مثال «سند word») در صفحهٔ درس؟';
$string['showtype_help'] = 'در کنار پیوند مربوط به فایل، نوع فایل (مثلا سند word) نمایش داده می‌شود.

اگر این منبع شامل چند فایل باشد، نوع «فایل شروع» نشان داده می‌شود.

اگر نوع فایل برای سیستم شناخته‌شده نباشد، نمایش داده نخواهد شد.';
$string['uploadeddate'] = '(تاریخ بارگذاری: {$a})';
