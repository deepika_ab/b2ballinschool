<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_uploadcourse', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_uploadcourse
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allowdeletes'] = 'مجاز بودن حذف‌ها';
$string['allowdeletes_help'] = 'اینکه آیا فیلد delete پذیرفته می‌شود یا خیر.';
$string['allowrenames'] = 'مجاز بودن تغییر نام‌ها';
$string['allowrenames_help'] = 'اینکه آیا فیلد rename پذیرفته می‌شود یا خیر.';
$string['allowresets'] = 'مجاز بودن بازنشانی‌ها';
$string['allowresets_help'] = 'اینکه آیا فیلد reset پذیرفته می‌شود یا خیر.';
$string['coursecreated'] = 'درس ایجاد شد';
$string['coursedeleted'] = 'درس حذف شد';
$string['coursedeletionnotallowed'] = 'اجازهٔ حذف کردن درس داده نشده است';
$string['coursedoesnotexistandcreatenotallowed'] = 'درس وجود ندارد و اجازهٔ ایجاد درس داده نشده است';
$string['courseexistsanduploadnotallowed'] = 'درس وجود دارد و اجازهٔ به‌روزکردن داده نشده است';
$string['coursefile'] = 'فایل';
$string['coursefile_help'] = 'این فایل باید یک فایل CSV باشد.';
$string['courseprocess'] = 'پردازش درس';
$string['courserenamed'] = 'نام درس تغییر داده شد';
$string['courserestored'] = 'درس بازیابی شد';
$string['coursescreated'] = 'درس‌های ایجاد شده: {$a}';
$string['coursesdeleted'] = 'درس‌های حذف شده: {$a}';
$string['courseserrors'] = 'درس‌های با خطا مواجه شده: {$a}';
$string['coursestotal'] = 'تعداد درس‌ها در مجموع: {$a}';
$string['coursesupdated'] = 'درس‌های به‌روز شده: {$a}';
$string['coursetemplatename'] = 'پس از ارسال، از روی این درس بازیابی شود';
$string['coursetemplatename_help'] = 'نام کوتاه یک درس موجود را وارد کنید تا به‌عنوان الگو برای ساخت تمام درس‌ها استفاده شود.';
$string['coursetorestorefromdoesnotexist'] = 'درسی که قرار است از رویش بازیابی شود وجود ندارد';
$string['courseupdated'] = 'درس به‌روز شد';
$string['createall'] = 'ساختن همه، در صورت نیاز با اضافه‌کردن یک شماره به نام کوتاه';
$string['createnew'] = 'تنها ساختن درس‌های جدید و نادیده گرفتن درس‌های موجود';
$string['createorupdate'] = 'ساختن درس‌های جدید یا به‌روز کردن درس‌های موجود';
$string['csvdelimiter'] = 'حرفِ جداکننده';
$string['csvdelimiter_help'] = 'حرفِ جداکننده در فایل CSV.';
$string['csvfileerror'] = 'قالب فایل CSV مشکلی دارد. لطفا بررسی کنید که تعداد عنوان‌ها و ستون‌ها با هم یکی باشد و حرفِ جداکننده و کدگذاری فایل درست باشد: {$a}';
$string['csvline'] = 'خط';
$string['defaultvalues'] = 'مقادیر پیش‌فرض درس';
$string['encoding'] = 'کدگذاری';
$string['encoding_help'] = 'کدگذاری فایل CSV.';
$string['id'] = 'شناسه';
$string['idnumberalreadyinuse'] = 'کد شناسایی توسط درس دیگری در حال استفاده است';
$string['importoptions'] = 'گزینه‌های وارد کردن';
$string['invalidbackupfile'] = 'فایل پشتیبان نامعتبر';
$string['invalidcourseformat'] = 'قالب درس نامعتبر';
$string['invalidcsvfile'] = 'فایل ورودی CSV نامعتبر';
$string['invalidencoding'] = 'کدگذاری نامعتبر';
$string['mode'] = 'نوع ارسال';
$string['mode_help'] = 'با استفاده از این تنظیم می‌توانید تعیین کنید که آیا درس‌ها می‌توانند ساخته و/یا به‌روز شوند.';
$string['nochanges'] = 'بدون تغییر';
$string['preview'] = 'پیش‌نمایش';
$string['reset'] = 'بازنشانی بعد از ارسال';
$string['reset_help'] = 'اینکه پس از ساختن/به‌روزکردن درس، باید بازنشانی هم شود یا خیر.';
$string['result'] = 'نتیجه';
$string['rowpreviewnum'] = 'پیش‌نمایش سطرها';
$string['rowpreviewnum_help'] = 'تعداد سطرهای فایل CSV که در پیش‌نمایش در صفحهٔ بعد نشان داده خواهد شد. این گزینه برای محدودکردن اندازهٔ صفحهٔ بعد وجود دارد.';
$string['shortnametemplate'] = 'الگو برای تولید نام کوتاه';
$string['shortnametemplate_help'] = 'نام کوتاه درس در راهبری نمایش داده می‌شود. می‌توانید از دستور الگوی (<span dir="ltr" style="direction:ltr; display:inline-block;">%f</span> = نام کامل، <span dir="ltr" style="direction:ltr; display:inline-block;">%i</span> = کد شناسایی) استفاده کنید، یا یک مقدار اولیه اینجا وارد کنید تا به‌صورت افزایشی استفاده شود.';
$string['templatefile'] = 'پس از ارسال، از روی این فایل بازیابی شود';
$string['templatefile_help'] = 'یک فایل را انتخاب کنید تا به‌عنوان الگو برای ساخت تمام درس‌ها استفاده شود.';
$string['updatemissing'] = 'مقادیر خالی با اطلاعات فایل CSV و پیش‌فرض‌ها پر شوند';
$string['updatemode'] = 'مدل به‌روزرسانی';
$string['updatemodedoessettonothing'] = 'مدل به‌روزرسانی اجازهٔ به‌روزشدن هیچ چیزی را نمی‌دهد';
$string['updatemode_help'] = 'اگر به درس‌ها اجازهٔ به‌روز شدن بدهید، باید این را هم تعیین کنید که درس‌ها با استفاده از چه مقادیری به‌روز شوند.';
$string['updateonly'] = 'تنها به‌روز کردن درس‌های موجود';
$string['updatewithdataonly'] = 'تنها به‌روز شدن با اطلاعات فایل CSV';
$string['updatewithdataordefaults'] = 'به‌روز شدن با اطلاعات فایل CSV و پیش‌فرض‌ها';
$string['uploadcourses'] = 'ساختن درس‌ها از روی فایل';
$string['uploadcourses_help'] = 'درس‌ها را می‌توان از طریق فایل متنی وارد کرد. قالب فایل باید به‌صورت زیر باشد:

* هر خط فایل شامل یک رکورد است
* هر رکورد دنباله‌ای از داده است که با کاما (یا سایر حروف جداکننده) از یکدیگر جدا شده‌اند
* رکورد اول شامل دنباله‌ای از نام فیلدها است و تعیین‌کنندهٔ قالب ادامهٔ فایل است
* فیلدهای اجباری عبارتند از shortname و fullname و category';
$string['uploadcoursespreview'] = 'پیش‌نمایش درس‌های ارسال‌شده';
