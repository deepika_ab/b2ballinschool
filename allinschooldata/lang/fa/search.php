<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'search', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   search
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['author'] = 'نویسنده';
$string['createdon'] = 'تاریخ ایجاد';
$string['docmodifiedon'] = 'آخرین تغییر در {$a}';
$string['enginenotinstalled'] = 'موتور جستجوی {$a} نصب نیست.';
$string['engineserverstatus'] = 'موتور جستجو موجود نیست. لطفا با مدیر سایتتان تماس بگیرید.';
$string['globalsearchdisabled'] = 'جستجوی سراسری فعال نیست.';
$string['search'] = 'جستجو';
$string['searcharea'] = 'ناحیه جستجو';
$string['searching'] = 'در حال جستجو در ...';
$string['search:message_received'] = 'پیام‌ها - دریافت‌شده';
$string['search:message_sent'] = 'پیام‌ها - فرستاده‌شده';
$string['search:mycourse'] = 'درس‌های من';
$string['searchsetupdescription'] = 'مراحل زیر به شما کمک می‌کنند تا جستجوی سراسری مودل را راه‌اندازی کنید.';
$string['search:user'] = 'کاربران';
$string['step'] = 'مرحله';
$string['versiontoolow'] = 'متأسفیم، جستجوی سراسری به PHP نسخهٔ ۵٫۰٫۰ یا بالاتر احتیاج دارد';
