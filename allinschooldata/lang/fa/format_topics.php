<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_topics', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   format_topics
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addsections'] = 'اضافه کردن موضوعات';
$string['currentsection'] = 'موضوع جاری';
$string['deletesection'] = 'حذف موضوع';
$string['editsection'] = 'ویرایش موضوع';
$string['hidefromothers'] = 'پنهان کردن موضوع از دیگران';
$string['page-course-view-topics-x'] = 'تمام صفحه‌های درس در قالب موضوعی';
$string['pluginname'] = 'قالب موضوعی';
$string['section0name'] = 'عمومی';
$string['sectionname'] = 'موضوع';
$string['showfromothers'] = 'نشان دادن این موضوع به دیگران';
