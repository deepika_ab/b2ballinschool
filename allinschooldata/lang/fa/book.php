<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'book', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   book
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addafter'] = 'اضافه کردن یک فصل جدید';
$string['book:addinstance'] = 'اضافه کردن یک کتاب جدید';
$string['book:edit'] = 'ویرایش فصل‌های کتاب';
$string['book:read'] = 'مطالعه کتاب';
$string['book:viewhiddenchapters'] = 'مشاهده فصل‌های پنهان کتاب';
$string['chaptertitle'] = 'عنوان فصل';
$string['content'] = 'محتوا';
$string['customtitles'] = 'عنوان‌های دلخواه';
$string['customtitles_help'] = 'معمولا عنوان هر فصل هم در فهرست و هم به‌صورت تیتر در بالای صفحه نمایش داده می‌شود.

اگر مربع «عنوان دلخواه» علامت خورده باشد، عنوان فصل به‌عنوان تیتر در بالای صفحه نمایش داده *نخواهد شد*. به‌عنوان بخشی از محتوای صفحه می‌توان یک عنوان متفاوت (و احتمالا طولانی‌تر از عنوان فصل) برای صفحه وارد کرد.';
$string['deletechapter'] = 'حذف فصل «{$a}»';
$string['editchapter'] = 'ویرایش فصل «{$a}»';
$string['editingchapter'] = 'ویرایش کردن فصل';
$string['eventchapterviewed'] = 'فصل مشاهده شد';
$string['hidechapter'] = 'پنهان‌کردن فصل «{$a}»';
$string['modulename'] = 'کتاب';
$string['modulename_help'] = 'اساتید با استفاده از ماژول کتاب می‌توانند یک منبع چند صفحه‌ای شامل فصل‌ها و زیرفصل‌ها را در یک قالب کتاب-مانند در درس بسازند. کتاب‌ها که علاوه‌بر متن می‌توانند شامل فایل‌های چندرسانه‌ای هم باشند، برای نمایش متن‌های طولانی (با شکستن آنها به چند قسمت) مفید هستند.

از کتاب می‌توان در موارد زیر استفاده کرد:

* برای نمایش منابع مطالعاتی مربوط به هر قسمت از درس
* به‌عنوان کتاب راهنما برای کارکنان
* به‌عنوان ویترینی برای نمونه‌کارهای شاگردان';
$string['modulenameplural'] = 'کتاب‌ها';
$string['movechapterdown'] = 'انتقال فصل «{$a}» به پایین';
$string['movechapterup'] = 'انتقال فصل «{$a}» به بالا';
$string['navexit'] = 'خروج از کتاب';
$string['navimages'] = 'تصاویر';
$string['navnext'] = 'بعدی';
$string['navnexttitle'] = 'صفحهٔ بعد: {$a}';
$string['navprev'] = 'قبلی';
$string['navprevtitle'] = 'صفحهٔ قبل: {$a}';
$string['navstyle'] = 'سبک و شمایل راهبری';
$string['navstyle_help'] = 'تصاویر - از آیکن‌هایی برای راهبری استفاده می‌شود
متن - از عنوان فصل‌ها برای راهبری استفاده می‌شود';
$string['navtext'] = 'متن';
$string['navtoc'] = 'فقط فهرست';
$string['nocontent'] = 'هنوز محتوایی به این کتاب اضافه نشده است.';
$string['numbering'] = 'قالب‌بندی فصل‌ها';
$string['numbering0'] = 'هیچ';
$string['numbering1'] = 'عدد';
$string['numbering2'] = 'گلوله';
$string['numbering3'] = 'تورفته';
$string['numbering_help'] = '* هیچ - عنوان فصل‌ها و زیرفصل‌ها هیچ قالبی ندارد
* عدد - عنوان فصل‌ها و زیرفصل‌ها شماره‌گذاری می‌شود ۱، ۱.۱، ۱.۲، ۲، ...
* گلوله - زیرفصل‌ها به‌صورت تورفته و با گلوله در فهرست نمایش داده می‌شوند
* تورفته - زیرفصل‌ها به‌صورت تورفته در فهرست نشان داده می‌شوند';
$string['numberingoptions'] = 'گزینه‌های موجود برای قالب‌بندی فصل‌ها';
$string['page-mod-book-x'] = 'هر صفحه‌ای از ماژول کتاب';
$string['pluginadministration'] = 'مدیریت کتاب';
$string['pluginname'] = 'کتاب';
$string['search:activity'] = 'کتاب - اطلاعات منبع';
$string['search:chapter'] = 'کتاب - فصل‌ها';
$string['showchapter'] = 'نشان‌دادن فصل «{$a}»';
$string['subchapter'] = 'زیرفصل';
$string['subchapternotice'] = '(تنها پس از ساخته‌شدن اولین فصل موجود خواهد بود)';
$string['toc'] = 'فهرست';
