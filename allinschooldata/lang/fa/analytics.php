<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'analytics', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   analytics
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['analytics'] = 'تجزیه و تحلیل';
$string['analyticssettings'] = 'تنظیمات تجزیه و تحلیل';
$string['disabledmodel'] = 'مدل غیرفعال';
$string['errorpredictionnotfound'] = 'پیش‌بینی یافت نشد';
$string['nocourses'] = 'درسی برای تحلیل وجود ندارد';
$string['nodata'] = 'اطلاعاتی برای تحلیل وجود ندارد';
$string['notuseful'] = 'غیرمفید';
$string['novaliddata'] = 'اطلاعات معتبر دردسترس نیست';
$string['novalidsamples'] = 'نمونه‌ معتبر دردسترس نیست';
$string['privacy:metadata:analytics:indicatorcalc:endtime'] = 'پایان زمان محاسبه';
$string['privacy:metadata:analytics:indicatorcalc:starttime'] = 'شروع زمان محاسبه';
$string['privacy:metadata:analytics:indicatorcalc:value'] = 'مقدار محاسبه‌شده';
$string['privacy:metadata:analytics:predictions'] = 'پیش‌بینی‌ها';
$string['privacy:metadata:analytics:predictions:prediction'] = 'پیش‌بینی';
$string['privacy:metadata:analytics:predictions:predictionscore'] = 'امتیاز پیش‌بینی';
$string['privacy:metadata:analytics:predictions:timeend'] = 'زمان پایان محاسبات';
$string['privacy:metadata:analytics:predictions:timestart'] = 'زمان شروع محاسبات';
$string['successfullyanalysed'] = 'باموفقیت تحلیل شد';
