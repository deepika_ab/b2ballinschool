<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_recordrtc', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_recordrtc
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allowedtypes'] = 'انواع مجاز';
$string['allowedtypes_desc'] = 'کدام دکمه‌های ضبط در ویرایشگر آتو نمایش داده شوند';
$string['attachrecording'] = 'ضمیمه کردن فایل ضبط شده';
$string['audioandvideo'] = 'صدا و فیلم';
$string['audiobitrate'] = 'نرخ بیت (bit rate) صدا';
$string['audiobitrate_desc'] = 'کیفیت ضبط صدا (عدد بزرگتر یعنی کیفیت بالاتر)';
$string['audiortc'] = 'ضبط صدا';
$string['gumabort'] = 'یک اتفاق عجیب افتاد که جلوی استفاده شدن وب‌کم/میکروفن را گرفت';
$string['gumabort_title'] = 'اتفاقی افتاد';
$string['gumnotallowed'] = 'کاربر باید اجازهٔ استفاده از وب‌کم/میکروفن را به مرورگر بدهد';
$string['gumnotallowed_title'] = 'مجوزهای نادرست';
$string['gumnotfound'] = 'هیچ سخت‌افزار ورودی‌ای متصل یا فعال نیست';
$string['gumnotfound_title'] = 'سخت‌افزار موجود نیست';
$string['gumnotreadable'] = 'چیزی دارد جلوی استفادهٔ مرورگر از وب‌کم/میکروفن را می‌گیرد';
$string['gumnotreadable_title'] = 'خطای سخت‌افزاری';
$string['gumnotsupported'] = 'مرورگر شما از قابلیت ضبط کردن در بستر یک اتصال ناامن پشتیبانی نمی‌کند و باید پلاگین را ببندد.';
$string['gumnotsupported_title'] = 'از اتصال ناامن پشتیبانی نمی‌شود';
$string['gumsecurity'] = 'مرورگر شما از قابلیت ضبط کردن در بستر یک اتصال ناامن پشتیبانی نمی‌کند و باید پلاگین را ببندد.';
$string['gumsecurity_title'] = 'از اتصال ناامن پشتیبانی نمی‌شود';
$string['insecurealert'] = 'مرورگر شما ممکن است جلوی عملکرد این پلاگین را بگیرد مگر اینکه دسترسی به سایت شما یا در بستر HTTPS باشد و یا اینکه از طریق localhost باشد';
$string['insecurealert_title'] = 'اتصال ناامن!';
$string['nearingmaxsize'] = 'شما به حداکثر اندازهٔ مجاز برای ارسال فایل رسیدید';
$string['nearingmaxsize_title'] = 'ضبط متوقف شد';
$string['norecordingfound'] = 'به‌نظر می‌رسد چیزی با مشکل مواجه شده است، ظاهرا هیچ چیزی ضبط نشده است.';
$string['norecordingfound_title'] = 'فایل ضبط شده‌ای پیدا نشد';
$string['nowebrtc'] = 'مرورگر شما هنوز فناوری WebRTC را (به‌طور کامل و یا اصلا) پشتیبانی نمی‌کند. لطفا مرورگرتان را ارتقا دهید یا از مرورگر دیگری استفاده کنید';
$string['nowebrtc_title'] = 'WebRTC پشتیبانی نمی‌شود';
$string['onlyaudio'] = 'تنها صدا';
$string['onlyvideo'] = 'تنها فیلم';
$string['privacy:metadata'] = 'پلاگین RecordRTC هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['recordagain'] = 'ضبط مجدد';
$string['recordingfailed'] = 'ضبط ناموفق بود، دوباره امتحان کنید';
$string['settings'] = 'تنظیمات RecordRTC';
$string['startrecording'] = 'شروع ضبط';
$string['stoprecording'] = 'توقف ضبط';
$string['timelimit'] = 'محدودیت زمانی به ثانیه';
$string['timelimit_desc'] = 'حداکثر طول ضبط';
$string['uploadaborted'] = 'ارسال متوقف شد:';
$string['uploadfailed'] = 'ارسال ناموفق بود:';
$string['uploadfailed404'] = 'ارسال ناموفق بود: فایل بیش از حد بزرگ';
$string['videobitrate'] = 'نرخ بیت (bit rate) فیلم';
$string['videobitrate_desc'] = 'کیفیت ضبط فیلم (عدد بزرگتر یعنی کیفیت بالاتر)';
$string['videortc'] = 'ضبط فیلم';
