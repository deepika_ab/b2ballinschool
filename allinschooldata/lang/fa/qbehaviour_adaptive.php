<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_adaptive', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qbehaviour_adaptive
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['disregardedwithoutpenalty'] = 'ارائه نامعتبر بود و بدون اعمال جریمه نادیده گرفته شد.';
$string['gradingdetails'] = 'نمرهٔ این ارائه: {$a->raw} از {$a->max}.';
$string['gradingdetailsadjustment'] = 'با احتساب جریمه‌های قبلی، نمرهٔ <strong>{$a->cur} از {$a->max}</strong> را برای این سؤال کسب می‌کنید.';
$string['gradingdetailspenalty'] = 'این ارائه {$a} جریمه کسب کرده است.';
$string['gradingdetailspenaltytotal'] = 'مجموع جریمه‌ها تا به حال: {$a}.';
$string['notcomplete'] = 'ناکامل';
$string['pluginname'] = 'حالت سازگار';
