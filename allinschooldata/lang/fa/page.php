<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'page', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   page
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configdisplayoptions'] = 'تمام گزینه‌هایی که می‌خواهید موجود باشند را انتخاب کنید. تنظیمات فعلی صفحه‌های موجود تغییری نخواهد کرد. برای انتخاب چند گزینه کلید کنترل (Ctrl) را نگه دارید.';
$string['content'] = 'محتوای صفحه';
$string['contentheader'] = 'محتوا';
$string['createpage'] = 'ساختن یک منبع جدید از نوع صفحه';
$string['displayoptions'] = 'نحوه‌های ممکن نمایش';
$string['displayselect'] = 'نحوهٔ نمایش';
$string['displayselectexplain'] = 'نحوهٔ نمایش را انتخاب کنید.';
$string['legacyfiles'] = 'انتقال فایل‌های قدیمی درس';
$string['legacyfilesactive'] = 'فعال';
$string['legacyfilesdone'] = 'تمام شد';
$string['modulename'] = 'صفحه';
$string['modulename_help'] = 'ماژول صفحه به استاد این امکان را می‌دهد تا با استفاده از ویرایشگر مودل یک صفحهٔ تحت وب ایجاد کند. «صفحه» می‌تواند شامل متن، عکس، صدا، فیلم، پیوندهای وب و کدهای جاسازی‌شده (مانند نقشهٔ گوگل) باشد.

از جمله مزایای استفاده از ماژول صفحه نسبت به ماژول فایل برای قراردادن منابع این است که در صورت استفاده از «صفحه»، منابع در دسترس‌تر (مثلا برای کاربران موبایل) و به‌روزکردنشان ساده‌تر خواهد بود.

برای محتواهای طولانی، توصیه می‌شود که به‌جای «صفحه» از «کتاب» استفاده شود.

از کاربردهای «صفحه» می‌توان به موارد زیر اشاره کرد

* برای نمایش ضوابط و شرایط درس یا خلاصه‌ای از سیلابس درس
* برای جاسازی کردن چند فایل صوتی یا ویدئویی در کنار هم همراه با یک‌سری متن توصیفی';
$string['modulenameplural'] = 'صفحه‌ها';
$string['optionsheader'] = 'اختیارات نمایش';
$string['page:addinstance'] = 'اضافه‌کردن یک منبع جدید از نوع صفحه';
$string['page-mod-page-x'] = 'هر صفحه‌ای از ماژول صفحه';
$string['page:view'] = 'مشاهدهٔ محتوای صفحه';
$string['pluginadministration'] = 'مدیریت ماژول صفحه';
$string['pluginname'] = 'صفحه';
$string['popupheight'] = 'ارتفاع پنجرهٔ pop-up (بر حسب پیکسل)';
$string['popupheightexplain'] = 'ارتفاع پیش‌فرض پنجره‌های pop-up را تعیین می‌کند.';
$string['popupwidth'] = 'عرض پنجرهٔ pop-up (بر حسب پیکسل)';
$string['popupwidthexplain'] = 'عرض پیش‌فرض پنجره‌های pop-up را تعیین می‌کند.';
$string['printheading'] = 'نمایش نام صفحه';
$string['printheadingexplain'] = 'نمایش نام صفحه در بالای محتوا؟';
$string['printintro'] = 'نمایش توصیف صفحه';
$string['printintroexplain'] = 'نمایش توصیف صفحه در بالای محتویات آن؟';
$string['search:activity'] = 'صفحه';
