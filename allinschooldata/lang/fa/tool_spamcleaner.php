<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_spamcleaner', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_spamcleaner
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'پاک کنندهٔ هرز نوشته‌ها';
$string['spamauto'] = 'تشخیص خودکار الگوهای رایج هرز نوشته';
$string['spamcannotdelete'] = 'نمی‌توان این کاربر را پاک کرد';
$string['spamcannotfinduser'] = 'هیچ کاربری منطبق بر جستجوی شما نبود';
$string['spamcleanerintro'] = '<p>با استفاده از این اسکریپت می‌توانید پروفایل‌های تمام کاربران را برای پیدا کردن عبارت‌های کلیدی خاصی مورد جستجو قرار دهید و سپس حساب‌های کاربری‌ای که به طور واضح توسط ارسال کنندگان هرز نوشته ایجاد شده اند را حذف کنید. با استفاده از کاما می‌توانید چند عبارت کلیدی را جستجو کنید (مثال: casino, porn).</p>
<p>اطلاعات بیشتری در مورد <a href="http://docs.moodle.org/en/Reducing_spam_in_Moodle">کاهش هرز نامه در مودل</a> در مستندات مودل موجود است.</p>';
$string['spamdeleteall'] = 'پاک‌کردن تمام این حساب‌های کاربری';
$string['spamdeleteallconfirm'] = 'آیا مطمئن هستید که می‌خواهید این حساب‌های کاربری را پاک کنید؟ بعدا نمی‌توانید این عمل را خنثی کنید.';
$string['spamdeleteconfirm'] = 'آیا مطمئن هستید که می‌خواهید این مورد را پاک کنید؟ بعدا نمی‌توانید این عمل را خنثی کنید.';
$string['spamdesc'] = 'توصیف';
$string['spameg'] = 'مثال: casino, porn, xxx';
$string['spamfromblog'] = 'از مطلب بلاگ:';
$string['spamfromcomments'] = 'از نظر:';
$string['spamfromforumpost'] = 'از مطلب ارسالی در تالار گفتگو:';
$string['spamfrommessages'] = 'از پیام‌ها:';
$string['spamoperation'] = 'عملیات';
$string['spamresult'] = 'نتیجهٔ جستجوی پروفایل‌های کاربران به دنبال:';
$string['spamsearch'] = 'جستجوی این کلمه‌های کلیدی';
