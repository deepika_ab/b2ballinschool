<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_calculated', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_calculated
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['additem'] = 'اضافه کردن مورد';
$string['addmoreanswerblanks'] = 'اضافه کردن جا برای تعریف یک پاسخ دیگر';
$string['addsets'] = 'اضافه کردن مجموعه(ها)';
$string['answerhdr'] = 'پاسخ';
$string['answerstoleranceparam'] = 'پارامترهای مربوط به خطای قابل قبول در پاسخ';
$string['atleastoneanswer'] = 'باید حداقل یک پاسخ ارائه کنید.';
$string['atleastonerealdataset'] = 'در متن سؤال باید حداقل یک مجموعه‌دادهٔ حقیقی وجود داشته باشد';
$string['atleastonewildcard'] = 'حداقل یک متغیر باید در فرمول پاسخ یا متن سؤال وجود داشته باشد';
$string['calcdistribution'] = 'توزیع';
$string['calclength'] = 'تعداد اعشار';
$string['calcmax'] = 'حداکثر';
$string['calcmin'] = 'حداقل';
$string['choosedatasetproperties'] = 'خصوصیات مجموعه‌دادهٔ متغیرها را انتخاب کنید';
$string['choosedatasetproperties_help'] = 'یک مجموعه‌داده، مجموعه‌ای از مقادیر است که جایگزین یک متغیر می‌شوند. می‌توانید یک مجموعه‌دادهٔ اختصاصی برای یک سؤال بخصوص بسازید، یا اینکه یک مجموعه‌دادهٔ اشتراکی بسازید که امکان استفاده شدن در سایر سؤال‌های محاسباتی مربوط به همین طبقه را نیز داشته باشد.';
$string['correctanswerformula'] = 'فرمول پاسخ صحیح';
$string['correctanswershows'] = 'پاسخ صحیح نمایش می‌دهد:';
$string['correctanswershowsformat'] = 'قالب';
$string['correctfeedback'] = 'برای هر پاسخ صحیح';
$string['dataitemdefined'] = 'با {$a} مقدار عددی تعریف شده، موجود است';
$string['datasetrole'] = 'متغیرها <strong>{x..}</strong> با یک مقدار عددی از مجموعه‌دادهٔ خود جایگزین خواهند شد';
$string['decimals'] = 'با {$a}';
$string['deleteitem'] = 'حذف مورد';
$string['deletelastitem'] = 'پاک کردن آخرین مورد';
$string['editdatasets'] = 'ویرایش مجموعه‌داده‌های متغیرها';
$string['editdatasets_help'] = 'مقادیر متغیرها را می‌توان با وارد کردن یک عدد در هر یک از فیلدهای مربوط به متغیرها و سپس کلیک بر روی دکمهٔ اضافه کردن ایجاد کرد. برای تولید ۱۰ یا تعداد بیشتری مقدار به صورت خودکار، پیش از کلیک بر روی دکمهٔ اضافه کردن، تعداد مقادیر مورد نیاز را انتخاب کنید. توضیع یکنواخت یعنی اینکه شانس تولید هر یک از اعداد واقع در بازهٔ تعیین‌شده یکسان است. توضیح لگاریتم یکنواخت یعنی اینکه شانس تولید اعداد نزدیک به کران پائین بیشتر است.';
$string['existingcategory1'] = 'از یک مجموعه‌دادهٔ اشتراکی از قبل تعریف شده استفاده خواهد کرد';
$string['existingcategory2'] = 'یک فایل از یک مجموعه موجود از فایل‌هایی که توسط سایر سؤال‌های این طبقه هم استفاده شده‌اند';
$string['existingcategory3'] = 'یک پیوند از یک مجموعه موجود از پیوندهایی که توسط سایر سؤال‌های این طبقه هم استفاده شده‌اند';
$string['forceregeneration'] = 'اجبار به تولید دوباره';
$string['forceregenerationall'] = 'اجبار به تولید دوبارهٔ تمام متغیرها';
$string['forceregenerationshared'] = 'اجبار به تولید دوبارهٔ فقط متغیرهای غیر اشتراکی';
$string['getnextnow'] = 'دریافت «مورد برای اضافه کردن» جدید در این لحظه';
$string['hexanotallowed'] = 'مقدار {$a->value} که در قالب مبنای شانزده وارد شده است برای مجموعه‌داده‌های <strong>{$a->name}</strong> مجاز نیست';
$string['incorrectfeedback'] = 'برای هر پاسخ نادرست';
$string['itemno'] = 'مورد {$a}';
$string['itemscount'] = 'دفعات<br />استفاده';
$string['itemtoadd'] = 'مورد برای اضافه کردن';
$string['keptcategory1'] = 'مانند قبل همچنان از همان مجموعه‌دادهٔ اشتراکی موجود استفاده خواهد کرد';
$string['keptcategory2'] = 'همانند قبل، فایلی از طبقه مشابه شامل مجموعهٔ فایل‌های قابل استفادهٔ مجدد';
$string['keptcategory3'] = 'همانند قبل، پیوندی از طبقه مشابه شامل مجموعهٔ پیوندهای قابل استفادهٔ مجدد';
$string['keptlocal1'] = 'مانند قبل همچنان از همان مجموعه‌دادهٔ اختصاصی موجود استفاده خواهد کرد';
$string['keptlocal2'] = 'همانند قبل، فایلی از سؤال یکسان مجموعه اختصاصی فایل‌ها';
$string['keptlocal3'] = 'همانند قبل، پیوندی از سؤال یکسان مجموعه اختصاصی پیوندها';
$string['loguniform'] = 'لگاریتم یکنواخت';
$string['makecopynextpage'] = 'صفحهٔ بعد (سؤال جدید)';
$string['mandatoryhdr'] = 'متغیرهای اجباری موجود در پاسخ‌ها';
$string['minmax'] = 'بازهٔ مقادیر';
$string['newcategory1'] = 'از یک مجموعه‌دادهٔ اشتراکی جدید استفاده خواهد کرد';
$string['newcategory2'] = 'فایلی از یک مجموعهٔ جدید از فایل‌ها که ممکن است توسط سؤال‌های دیگر در این طبقه هم استفاده شوند';
$string['newcategory3'] = 'پیوندی از یک مجموعهٔ جدید از پیوندها که ممکن است توسط سؤال‌های دیگر در این طبقه هم استفاده شوند';
$string['newlocal1'] = 'از یک مجموعه‌دادهٔ اختصاصی جدید استفاده خواهد کرد';
$string['newlocal2'] = 'فایلی از یک مجموعهٔ جدید از فایل‌ها که فقط توسط این سؤال استفاده خواهند شد';
$string['newlocal3'] = 'پیوندی از یک مجموعهٔ جدید از پیوندها که فقط توسط این سؤال استفاده خواهند شد';
$string['newsetwildcardvalues'] = 'مجموعه(ها)ی جدیدی از مقادیر برای متغیرها';
$string['nextitemtoadd'] = '«مورد برای اضافه کردن» بعدی';
$string['nextpage'] = 'صفحهٔ بعد';
$string['nocoherencequestionsdatyasetcategory'] = 'برای سؤال با شناسهٔ {$a->qid}، شناسهٔ طبقه ({$a->qcat}) با شناسهٔ طبقهٔ ({$a->sharedcat}) مربوط به متغیر اشتراکی {$a->name} یکی نیست. سؤال را ویرایش کنید.';
$string['nocommaallowed'] = 'نمی‌توان از «,» استفاده کرد. از «.» استفاده کنید. مانند 0.013 یا 1.3e-2';
$string['nodataset'] = 'هیچ - این یک متغیر نیست';
$string['nosharedwildcard'] = 'متغیر اشتراکی‌ای در این طبقه وجود ندارد';
$string['notvalidnumber'] = 'مقدار متغیر یک عدد معتبر نیست';
$string['oneanswertrueansweroutsidelimits'] = 'حداقل یکی از پاسخ‌های صحیح بیرون محدودهٔ مقادیر واقعی قرار دارد.<br />تنظیمات مربوط به خطای قابل قبول واقع در قسمت مربوط به تنظیمات پیشرفته را تغییر دهید.';
$string['param'] = 'متغیر {<strong>{$a}</strong>}';
$string['partiallycorrectfeedback'] = 'برای هر پاسخ نیمه درست';
$string['pluginname'] = 'محاسباتی';
$string['pluginnameadding'] = 'تعریف یک سؤال محاسباتی';
$string['pluginnameediting'] = 'ویرایش یک سؤال محاسباتی';
$string['pluginname_help'] = 'سؤال‌های محاسباتی، امکان تعریف سؤال‌های عددی با عددهای متغیر را فراهم کرده‌اند. هنگام تعریف این سؤال‌ها می‌توان از نویسه‌های لاتین درون آکولاد استفاده کرد. این نویسه‌ها در هنگام شرکت در آزمون با مقادیر عددی جایگزین خواهند شد. به عنوان مثال، پاسخ صحیح سؤال «مساحت مستطیلی به طول {l} و عرض {w} را محاسبه نمائید» برابر با {l}*{w} می‌باشد (علامت * به معنی ضرب است).';
$string['pluginnamesummary'] = 'سؤال‌های محاسباتی مانند سؤال‌های عددی هستند. با این تفاوت که عددهای مورد استفاده در سؤال، هنگام برگزاری آزمون به صورت تصادفی از بین یک مجموعه انتخاب می‌شوند.';
$string['possiblehdr'] = 'متغیرهای احتمالی که فقط در متن سؤال آمده‌اند';
$string['questiondatasets'] = 'مجموعه‌داده‌های سؤال';
$string['questiondatasets_help'] = 'مجموعه‌داده‌های سؤال برای متغیرها که در تک تک سؤال‌ها استفاده خواهد شد';
$string['questionstoredname'] = 'نام ثبت شدهٔ سؤال';
$string['replacewithrandom'] = 'جایگزینی با یک مقدار تصادفی';
$string['reuseifpossible'] = 'در صورت وجود از همان مقدار قبلی استفاده شود';
$string['setno'] = 'مجموعهٔ {$a}';
$string['setwildcardvalues'] = 'مجموعه مقدار برای متغیرها';
$string['sharedwildcard'] = 'متغیر مشترک <strong>{$a}</strong>';
$string['sharedwildcardname'] = 'متغیر اشتراکی';
$string['sharedwildcards'] = 'متغیرهای اشتراکی';
$string['showitems'] = 'نمایش دادن';
$string['synchronize'] = 'همگام‌سازی داده‌های موجود در مجموعه‌داده‌های مشترک با سایر سؤال‌ها در یک آزمون';
$string['synchronizeno'] = 'همگام نشود';
$string['synchronizeyes'] = 'همگام شود';
$string['synchronizeyesdisplay'] = 'همگام شده و نام مجموعه‌داده‌های به اشتراک گذاشته شده به عنوان پیشوند نام سؤال نمایش داده شود';
$string['tolerance'] = 'خطای قابل قبول &plusmn;';
$string['trueanswerinsidelimits'] = 'پاسخ صحیح : {$a->correct} داخل محدوده مقدار واقعی {$a->true}';
$string['trueansweroutsidelimits'] = '<span class="error">خطای پاسخ صحیح : {$a->correct} بیرون محدوده مقدار واقعی {$a->true}</span>';
$string['uniform'] = 'یکنواخت';
$string['updatecategory'] = 'به‌روز کردن طبقه';
$string['updatedatasetparam'] = 'به‌روزرسانی پارامترهای مجموعه‌داده‌ها';
$string['updatetolerancesparam'] = 'به‌روزرسانی پارامترهای مربوط به خطای پاسخ‌ها';
$string['updatewildcardvalues'] = 'به‌روزرسانی مقادیر متغیرها';
$string['useadvance'] = 'برای دیدن خطاها از دکمهٔ پیشرفته استفاده کنید';
$string['usedinquestion'] = 'استفاده شده در سؤال';
$string['wildcard'] = 'متغیر {<strong>{$a}</strong>}';
$string['wildcardparam'] = 'پارامترهای مورد استفاده برای تولید مقادیر جایگزین متغیرها';
$string['wildcardrole'] = 'متغیرهای <strong>{x..}</strong> با یک مقدار عددی از بین مقادیر تولید شده جایگزین خواهند شد.';
$string['wildcards'] = 'متغیرها {a}...{z}';
$string['wildcardvalues'] = 'مقادیر متغیر(ها)';
$string['wildcardvaluesgenerated'] = 'مقادیر متغیر(ها) تولید شدند';
$string['youmustaddatleastoneitem'] = 'باید حداقل یک مورد مجموعه‌داده اضافه کنید تا بتوانید این سؤال را ذخیره کنید.';
$string['youmustaddatleastonevalue'] = 'برای اینکه بتوانید این سؤال را ذخیره کنید، باید حداقل یک مجموعه از مقادیر برای متغیرها اضافه کرده باشید.';
