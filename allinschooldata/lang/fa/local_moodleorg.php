<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_moodleorg', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   local_moodleorg
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['heroslide_getstarted_description'] = 'مودل محبوب‌ترین سیستم مدیریت یادگیری در جهان است. در چند دقیقه شروع به ایجاد سامانه یادگیری آنلاین خود کنید!';
$string['heroslide_getstarted_headline'] = 'شروع کار آسان است';
$string['heroslide_getstarted_slidecontrol'] = 'همین امروز شروع کنید';
