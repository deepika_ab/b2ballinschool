<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'portfolio_googledocs', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   portfolio_googledocs
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['clientid'] = 'شناسهٔ مشتری';
$string['oauthinfo'] = '<p>برای استفاده از این پلاگین باید سایتتان را به‌صورتی که در مستند <a href="{$a->docsurl}">راه‌اندازی Google OAuth 2.0</a> شرح داده شده است در گوگل ثبت کنید.</p><p>به‌عنوان بخشی از فرایند ثبت، نیاز دارید که آدرس زیر را به‌عنوان «Authorized Redirect URIs» وارد کنید:</p><p>{$a->callbackurl}</p>
<p>بعد از ثبت، یک شناسهٔ مشتری و رمز که می‌توانند برای پیکربندی تمام پلاگین‌های گوگل‌درایو و پیکاسا استفاده شوند به شما داده خواهد شد.</p>';
$string['pluginname'] = 'گوگل درایو';
