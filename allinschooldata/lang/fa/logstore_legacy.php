<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'logstore_legacy', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   logstore_legacy
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['loglegacy'] = 'log کردن داده‌های موروثی';
$string['loglegacy_help'] = 'این پلاگین داده‌های log را در جدول قدیمی log ها (mdl_log) ذخیره می‌کند. این روش با پلاگین‌های جدیدتر، غنی‌تر و بهینه‌تری جایگزین شده است، بنابراین تنها در صورتی باید از این پلاگین استفاده کنید که گزارش‌های قدیمی‌ای دارید که مستقیما روی جدول log کوئری اجرا می‌کنند. نوشتن روی log های موروثی موجب افزایش بار خواهد شد، بنابراین توصیه می‌شود که برای حفظ کارایی، این پلاگین را در حالتی که مورد نیاز نیست غیرفعال کنید.';
$string['pluginname'] = 'log های موروثی';
$string['taskcleanup'] = 'پاک‌سازی جدول log های موروثی';
