<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'antivirus', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   antivirus
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actantivirushdr'] = 'پلاگین‌های ویروس‌یابی موجود';
$string['antiviruses'] = 'پلاگین‌های ویروس‌یابی';
$string['antivirussettings'] = 'مدیریت پلاگین‌های ویروس‌یابی';
$string['configantivirusplugins'] = 'لطفا پلاگین‌های ویروس‌یابی‌ای که می‌خواهید استفاده کنید را انتخاب کرده و آنها را به ترتیبی که باید عمل کنند مرتب کنید.';
$string['emailsubject'] = '{$a} :: اطلاعیه‌های ویروس‌یاب';
$string['privacy:metadata'] = 'سیستم آنتی‌ویروس هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['virusfounduser'] = 'فایلی که ارسال کرده‌اید ({$a->filename}) توسط ویروس‌یاب بررسی و به عنوان آلوده تشخیص داده شد! ارسال فایل موفق نبود.';
