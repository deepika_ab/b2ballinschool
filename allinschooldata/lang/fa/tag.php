<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tag', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tag
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['added'] = 'برچسب(های) استاندارد اضافه شد(ند)';
$string['addotags'] = 'اضافه‌کردن برچسب استاندارد';
$string['addtagcoll'] = 'اضافه‌کردن مجموعه برچسب';
$string['addtagtomyinterests'] = 'اضافه شدن «{$a}» به علایق من';
$string['changename'] = 'تغییر نام بده';
$string['changetype'] = 'تغییر نوع بده';
$string['combineselected'] = 'موارد انتخاب‌شده ادغام شوند';
$string['component'] = 'قسمت';
$string['count'] = 'تعداد';
$string['coursetags'] = 'برچسب‌های درس';
$string['defautltagcoll'] = 'مجموعه پیش‌فرض';
$string['delete'] = 'حذف کن';
$string['deleted'] = 'حذف شد';
$string['deleteselected'] = 'موارد انتخاب‌شده حذف شوند';
$string['description'] = 'توصیف';
$string['editcollname'] = 'ویرایش نام مجموعه برچسب';
$string['editname'] = 'ویرایش نام برچسب';
$string['edittag'] = 'ویرایش این برچسب';
$string['edittagcoll'] = 'ویرایش مجموعه برچسب {$a}';
$string['edittagcollection'] = 'تغییر مجموعه برچسب';
$string['entertags'] = 'برچسب‌ها را وارد کنید...';
$string['eventtagadded'] = 'برچسب به یک مورد اضافه شد';
$string['eventtagcollcreated'] = 'مجموعه برچسب ساخته شد';
$string['eventtagcolldeleted'] = 'مجموعه برچسب حذف شد';
$string['eventtagcollupdated'] = 'مجموعه برچسب به‌روز شد';
$string['eventtagremoved'] = 'برچسب از یک موردی حذف شد';
$string['exclusivemode'] = 'تنها نمایش آن دسته از {$a->tagarea} که tag دارند';
$string['flag'] = 'علامت';
$string['flagasinappropriate'] = 'علامت‌گذاری به‌عنوان نامناسب';
$string['helprelatedtags'] = 'برچسب‌های مرتبط جداشده توسط کاما';
$string['id'] = 'شناسه';
$string['inputstandardtags'] = 'لیست برچسب‌های جدید را به‌صورت جداشده با کاما وارد کنید';
$string['managestandardtags'] = 'مدیریت برچسب‌های استاندارد';
$string['managetagcolls'] = 'مدیریت مجموعه‌های تگ';
$string['managetags'] = 'مدیریت برچسب‌ها';
$string['name'] = 'نام برچسب';
$string['namesalreadybeeingused'] = 'نام برچسب‌ها هم‌اکنون در حال استفاده است';
$string['newcollnamefor'] = 'نام جدید برای مجموعه برچسب {$a}';
$string['newnamefor'] = 'نام جدید برای برچسب {$a}';
$string['noresultsfor'] = 'نتیجه‌ای برای «{$a}» وجود نداشت';
$string['owner'] = 'مالک';
$string['ptags'] = 'برچسب‌های تعریف شده توسط کاربر (جدا شده توسط کاما)';
$string['relatedblogs'] = 'مطالب اخیر بلاگ که این برچسب را دارند';
$string['relatedtags'] = 'برچسب‌های مرتبط';
$string['removetagfrommyinterests'] = 'حذف «{$a}» از لیست علایق من';
$string['reset'] = 'حذف علامت';
$string['resetflag'] = 'بدون علامت کن';
$string['responsiblewillbenotified'] = 'شخص مسئول مطلع خواهد شد';
$string['search'] = 'جستجو';
$string['searchable'] = 'قابل جستجو';
$string['searchable_help'] = 'اگر علامت خورده باشد، برچسب‌هایی که در این مجموعه جستجو هستند از طریق صفحهٔ «جستجوی برچسب‌ها» قابل پیدا شدن هستند. اگر علامت نخورده باشد، برچسب‌ها همچنان از طریق سایر صفحه‌های جستجو قابل پیدا شدن هستند.';
$string['searchresultsfor'] = 'نتایج جستجو بدنبال «{$a}»';
$string['searchtags'] = 'جستجوی برچسب‌ها';
$string['seeallblogs'] = 'مشاهدهٔ همهٔ مطالب وبلاگ که برچسب «{$a}» را دارند';
$string['select'] = 'انتخاب';
$string['settypedefault'] = 'از لیست برچسب‌های استاندارد حذف شود';
$string['settypestandard'] = 'استاندارد شود';
$string['showstandard'] = 'استفاده از برچسب‌های استاندارد';
$string['standardforce'] = 'اجباری';
$string['standardhide'] = 'پیشنهاد نشود';
$string['standardsuggest'] = 'پیشنهاد شود';
$string['standardtag'] = 'استاندارد';
$string['suredeletecoll'] = 'آیا مطمئنید که می‌خواهید مجموعه برچسب «{$a}» را حذف کنید؟';
$string['tag'] = 'برچسب';
$string['tagarea_blog_external'] = 'پست‌های وبلاگ‌های بیرونی';
$string['tagarea_course'] = 'درس‌ها';
$string['tagarea_course_modules'] = 'فعالیت‌ها و منابع';
$string['tagareaenabled'] = 'فعال';
$string['tagareaname'] = 'نام';
$string['tagarea_post'] = 'مطلب‌های وبلاگ';
$string['tagareas'] = 'حیطه‌های برچسب‌ها';
$string['tagarea_user'] = 'علائق کاربران';
$string['tagcollection'] = 'مجموعه‌برچسب';
$string['tagcollections'] = 'مجموعه‌برچسب‌ها';
$string['tagdescription'] = 'توصیف برچسب';
$string['tags'] = 'برچسب‌ها';
$string['tagsaredisabled'] = 'برچسب‌ها غیرفعال شده‌اند';
$string['thingstaggedwith'] = '{$a->count} چیز برچسب «{$a->name}» را دارند';
$string['thingtaggedwith'] = '«{$a->name}» یک بار استفاده شده است';
$string['timemodified'] = 'تغییر';
$string['typechanged'] = 'نوع برچسب تغییر یافت';
$string['updatetag'] = 'به‌روزرسانی';
