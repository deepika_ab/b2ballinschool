<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_boost', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_boost
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['advancedsettings'] = 'تنظیمات پیشرفته';
$string['brandcolor'] = 'رنگ سازمانی';
$string['brandcolor_desc'] = 'رنگ مؤکد در سایت.';
$string['choosereadme'] = 'شتاب یک پوستهٔ مدرن و بسیار قابل تنظیم است. از این پوسته می‌توان به‌طور مستقیم یا به‌عنوان پوستهٔ والد برای ساختن پوسته‌های جدیدی که از Bootstrap 4 استفاده خواهند کرد استفاده کرد.';
$string['configtitle'] = 'شتاب';
$string['generalsettings'] = 'تنظیمات عمومی';
$string['pluginname'] = 'شتاب';
$string['rawscss'] = 'SCSS خام';
$string['rawscss_desc'] = 'از این فیلد برای فراهم کردن کد SCSS یا CSS که به انتهای style sheet مودل تزریق خواهد شد استفاده کنید.';
$string['rawscsspre'] = 'SCSS اولیه خام';
$string['region-side-pre'] = 'چپ';
