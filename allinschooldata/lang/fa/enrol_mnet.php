<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_mnet', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_mnet
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['mnet:config'] = 'پیکربندی نمونه‌های ثبت‌نام شبکهٔ مودلی';
$string['mnet_enrol_description'] = 'این سرویس را منتشر کنید تا به مدیرهای «{$a}» اجازهٔ ثبت نام کردن شاگردانشان در درس‌هایی که شما روی کارگزار خودتان ساخته‌اید بدهید.
<br/><ul><li>
<em>وابستگی</em>:
باید سرویس SSO (فراهم کنندهٔ سرویس) را نیز برای «{$a}» <strong>منتشر</strong> کنید.
</li><li>
<em>وابستگی</em>:
باید در سرویس SSO (فراهم کنندهٔ هویت) در «{$a}» نیز <strong>مشترک</strong> شوید.
</li></ul><br/>
مشترک این سرویس شوید تا بتوانید شاگردانتان را در درس‌های موجود در «{$a}» ثبت نام کنید.
<br/><ul><li>
<em>وابستگی</em>:
باید در سرویس SSO (فراهم کنندهٔ سرویس) در «{$a}» نیز <strong>مشترک</strong> شوید.
</li><li><em>وابستگی</em>:
باید سرویس SSO (فراهم کنندهٔ هویت) را نیز برای «{$a}» <strong>منتشر</strong> کنید.
</li></ul><br/>';
$string['mnet_enrol_name'] = 'سرویس ثبت نام از راه دور';
$string['pluginname'] = 'ثبت نام‌های دور دست شبکهٔ مودلی';
$string['pluginname_desc'] = 'به میزبان‌های دور دست شبکهٔ مودلی اجازه می‌دهد که کاربرانشان را در درس‌های ما ثبت نام کنند.';
$string['remotesubscribersall'] = 'تمام میزبان‌ها';
