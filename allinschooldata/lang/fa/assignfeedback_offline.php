<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignfeedback_offline', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignfeedback_offline
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['downloadgrades'] = 'دریافت برگه ثبت نمرات';
$string['gradesfile'] = 'برگه ثبت نمرات (قالب csv)';
$string['gradesfile_help'] = 'برگه ثبت نمرات با نمرات ثبت شده. این فایل باید یک فایل csv باشد که قبلا برای این تکلیف از مودل دریافت شده است و شامل ستون‌هایی برای نمره شاگردان و شناسه هویت آنها می‌باشد. کدگذاری این فایل باید «UTF-8» باشد.';
$string['ignoremodified'] = 'به‌روز شدن نمره‌هایی که به تازگی و پس از دریافت برگه ثبت نمرات در مودل تغییر کرده‌اند.';
$string['ignoremodified_help'] = 'هنگامی که برگه ثبت نمرات از مودل دریافت می‌شود، شامل جدیدترین تغییرات مربوط به هر یک از نمره‌ها است. اگر بعد از دریافت کردن این برگه، هر کدام از نمره‌های مربوط به آن در مودل تغییر کند، مودل به طور پیش‌فرض تفاوت‌های موجود در برگه ثبت نمرات را نادیده می‌گیرد. اگر این گزینه را انتخاب کنید، مودل این بررسی ایمنی را نادیده می‌گیرد و در نتیجه این امکان وجود خواهد داشت که نمره دهنده‌های مختلف نمره‌هایی که توسط همدیگر داده شده است را بازنویسی کنند.';
$string['uploadgrades'] = 'ارسال برگه ثبت نمرات';
