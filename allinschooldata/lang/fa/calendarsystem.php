<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'calendarsystem', language 'fa', branch 'MOODLE_26_STABLE'
 *
 * @package   calendarsystem
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['calendarsystem'] = 'سیستم‌های تقویم';
$string['calendarsystemdeleteconfirm'] = 'شما در آستانهٔ حذف کامل سیستم تقویم «{$a}» هستید. با این کار تمام چیزهای مربوط به این پلاگین به طور کامل از پایگاه داده پاک خواهد شد. آیا مطمئن هستید که می‌خواهید ادامه دهید؟';
$string['calendarsystems'] = 'سیستم‌های تقویم';
$string['calendarsystemsmanage'] = 'مدیریت سیستم‌های تقویم';
$string['changecalendar'] = 'تغییر';
$string['checkforupdates'] = 'بررسی موجود بودن نسخه‌های جدیدتر';
$string['configcalendarsystem'] = 'تقویم پیش‌فرض';
$string['forcecalendarsystem'] = 'اجبار تقویم';
$string['helpcalendarsystem'] = 'یک تقویم پیش‌فرض برای کل سایت انتخاب نمائید. کاربران می‌توانند بعداً این مقدار را بازنویسی کنند.';
$string['preferredcalendar'] = 'تقویم دلخواه';
$string['system'] = 'تقویم';
$string['updateavailable'] = 'نسخه جدیدتری از هسته مرکزی سیستم تقویم موجود است!';
$string['updateavailableforplugin'] = 'نسخه جدیدتری از پلاگین سیستم تقویم {$a} موجود است!';
$string['updateavailable_moreinfo'] = 'اطلاعات بیشتر';
$string['updateavailablenot'] = 'کد سیستم تقویم شما به‌روز است!';
$string['updateavailable_version'] = 'نسخه {$a}';
$string['updatecheck'] = 'بررسی به‌روز بودن سیستم تقویم';
