<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_equation', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_equation
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['librarygroup1_desc'] = 'دستورهای لیست‌شدهٔ TeX در زبانهٔ عملگرها';
$string['librarygroup2'] = 'فلش‌ها';
$string['librarygroup2_desc'] = 'دستورهای لیست‌شدهٔ TeX در زبانهٔ فلش‌ها';
$string['librarygroup3'] = 'علائم یونانی';
$string['librarygroup3_desc'] = 'دستورهای لیست‌شدهٔ TeX در زبانهٔ علائم یونانی';
$string['librarygroup4_desc'] = 'دستورهای لیست‌شدهٔ TeX در زبانهٔ پیشرفته';
$string['pluginname'] = 'ویرایشگر فرمول';
$string['privacy:metadata'] = 'پلاگین atto_equation هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['settings'] = 'تنظیمات ویرایشگر فرمول';
