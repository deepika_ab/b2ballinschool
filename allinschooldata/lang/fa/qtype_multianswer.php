<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_multianswer', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_multianswer
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['confirmquestionsaveasedited'] = 'تایید می‌کنم که می‌خواهم سؤال به صورت ویرایش شده ذخیره شود';
$string['confirmsave'] = 'تایید و سپس ذخیره {$a}';
$string['correctanswer'] = 'پاسخ صحیح';
$string['correctanswerandfeedback'] = 'پاسخ صحیح و بازخورد';
$string['decodeverifyquestiontext'] = 'رمزگشایی و بازبینی متن سؤال';
$string['layout'] = 'چیدمان';
$string['layouthorizontal'] = 'مجموعه‌ای از گزینه‌ها در یک سطر افقی';
$string['layoutselectinline'] = 'منوی بازشونده جایگذاری شده در متن';
$string['layoutundefined'] = 'آرایش تعریف‌نشده';
$string['layoutvertical'] = 'مجموعه‌ای از گزینه‌ها در یک ستون عمودی';
$string['nooptionsforsubquestion'] = 'دریافت گزینه‌ها برای سؤال قسمت {$a->sub} (question->id={$a->id}) مقدور نیست.';
$string['noquestions'] = 'سؤال Cloze (دارای جواب‌های جاسازی شده) «<strong>{$a}</strong>» شامل هیچ سؤالی نیست';
$string['pluginname'] = 'جواب‌های جاسازی شده (Cloze)';
$string['pluginnameadding'] = 'تعریف یک سؤال با جواب‌های جاسازی شده (Cloze)';
$string['pluginnameediting'] = 'ویرایش یک سؤال با جواب‌های جاسازی شده (Cloze)';
$string['pluginname_help'] = 'یک سؤال با جوا‌های جاسازی شده (یا cloze)، از یک قطعه متن شامل سؤال‌هایی مانند سؤال‌های چند گزینه‌ای و کوتاه جواب در داخل خود تشکیل شده است.';
$string['pluginnamesummary'] = 'این نوع سؤال‌ها بسیار انعطاف‌پذیر هستند، ولی ایجاد آن‌ها تنها از طریق وارد کردن متنی که شامل کد‌های خاصی باشد امکان پذیر است. آن کدها موجب درج شدن یک سری سؤال به صورت‌های چند گزینه‌ای، کوتاه جواب و عددی در لابه‌لای متن می‌شوند.';
$string['qtypenotrecognized'] = 'نوع {$a} تشخیص داده نشد';
$string['questiondefinition'] = 'تعریف سؤال';
$string['questiondeleted'] = 'سؤال حذف شد';
$string['questioninquiz'] = '<ul>
  <li>اضافه یا حذف کردن سؤال‌ها،</li>
  <li>تغییر ترتیب سؤال‌ها در متن،</li>
  <li>تغییر نوع آن‌ها (عددی، کوتاه جواب، چند گزینه‌ای). </li></ul>';
$string['questionnotfound'] = 'سؤالی برای قسمت شمارهٔ {$a} سؤال وجود ندارد';
$string['questionsaveasedited'] = 'سؤال به صورت ویرایش شده ذخیره خواهد شد';
$string['questionsless'] = '{$a} سؤال کمتر از سؤال‌های موجود در متن ذخیره شدند';
$string['questionsmissing'] = 'سؤال معتبری وجود ندارد، حداقل یک سؤال تعریف نمائید.';
$string['questionsmore'] = '{$a} سؤال بیشتر از سؤال‌های موجود در متن ذخیره شدند';
$string['questiontypechanged'] = 'نوع سؤال تغییر کرده است';
$string['questiontypechangedcomment'] = 'نوع حداقل یکی از سؤال‌ها تغییر کرده است.<br /> آیا سؤالی را اضافه، حذف یا منتقل کرده‌اید؟ بررسی کنید.';
$string['questionusedinquiz'] = 'این سؤال در {$a->nb_of_quiz} آزمون استفاده شده است. تعداد تلاش‌های صورت گرفته : {$a->nb_of_attempts}';
$string['subqresponse'] = 'بخش {$a->i}: {$a->response}';
$string['unknownquestiontypeofsubquestion'] = 'سؤال از نوع ناشناخته: {$a->type} مربوط به سؤال قسمت {$a->sub}';
$string['warningquestionmodified'] = '<b>هشدار</b>';
$string['youshouldnot'] = '<b>شما نباید</b>';
