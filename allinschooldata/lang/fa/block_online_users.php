<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_online_users', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_online_users
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configtimetosee'] = 'میزان زمانی که یک کاربر در صورت غیر فعال بودن بیش از آن دیگر حاضر محسوب نخواهد شد.';
$string['nouser'] = 'هیچ کاربری حاضر نیست';
$string['numuser'] = '{$a} کاربر حاضر در سایت';
$string['numusers'] = '{$a} کاربر حاضر در سایت';
$string['online_status:hide'] = 'وضعیت حضور من در سایت از سایر کاربران پنهان شود';
$string['online_status:show'] = 'وضعیت حضور من در سایت توسط سایر کاربران قابل مشاهده باشد';
$string['online_users:addinstance'] = 'اضافه‌کردن یک بلوک «کاربران حاضر» جدید';
$string['online_users:myaddinstance'] = 'اضافه کردن یک بلوک جدید کاربران حاضر به میز کار';
$string['online_users:viewlist'] = 'مشاهده فهرست کاربران حاضر';
$string['periodnminutes'] = 'از {$a} دقیقه قبل';
$string['pluginname'] = 'کاربران حاضر';
$string['privacy:metadata:preference:uservisibility'] = 'وضعیت حضور در سایت که توسط سایر کاربران در بلوک کاربران حاضر قابل مشاهده است.';
$string['timetosee'] = 'حذف پس از عدم فعالیت (دقیقه)';
