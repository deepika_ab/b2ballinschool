<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_cohort', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_cohort
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addgroup'] = 'اضافه‌کردن به گروه';
$string['assignrole'] = 'انتساب نقش';
$string['cohort:config'] = 'پیکربندی هم‌دوره‌ای‌ها در درس‌ها';
$string['cohort:unenrol'] = 'لغو ثبت نام کاربران تعلیق شده';
$string['creategroup'] = 'ساختن گروه جدید';
$string['defaultgroupnametext'] = 'گروه هم‌دوره‌ای‌های {$a->name} {$a->increment}';
$string['instanceexists'] = 'هم‌دوره‌ای قبلا با نقش انتخاب‌شده همگام شده است';
$string['pluginname'] = 'هم‌دوره‌ای‌های همگام';
$string['pluginname_desc'] = 'پلاگین ثبت نام هم‌دوره‌ای‌ها اعضای گروه هم‌دوره‌ای را با شرکت کنندگان در درس همگام می‌کند.';
$string['status'] = 'فعال';
