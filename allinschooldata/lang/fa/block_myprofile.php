<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_myprofile', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_myprofile
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['contentsettings'] = 'نمایش تنظیمات برای قسمت محتوا';
$string['display_address'] = 'نمایش آدرس';
$string['display_aim'] = 'نمایش شناسهٔ AIM';
$string['display_city'] = 'نمایش شهر';
$string['display_country'] = 'نمایش کشور';
$string['display_currentlogin'] = 'نمایش زمان ورود فعلی';
$string['display_email'] = 'نمایش پست الکترونیک';
$string['display_firstaccess'] = 'نمایش اولین دسترسی';
$string['display_icq'] = 'نمایش شمارهٔ ICQ';
$string['display_institution'] = 'نمایش مؤسسه';
$string['display_lastaccess'] = 'نمایش آخرین دسترسی';
$string['display_lastip'] = 'نمایش آخرین IP';
$string['display_msn'] = 'نمایش شناسهٔ MSN';
$string['display_phone1'] = 'نمایش شماره تلفن';
$string['display_phone2'] = 'نمایش تلفن همراه';
$string['display_picture'] = 'نمایش تصویر';
$string['display_skype'] = 'نمایش شناسهٔ اسکایپ';
$string['display_yahoo'] = 'نمایش شناسهٔ یاهو';
$string['myprofile:addinstance'] = 'اضافه‌کردن یک بلوک جدید کاربر وارد شده';
$string['myprofile:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید کاربر وارد شده به میز کار';
$string['myprofile_settings'] = 'مشخصات نمایش داده شدهٔ کاربران';
$string['pluginname'] = 'کاربرِ واردشده';
