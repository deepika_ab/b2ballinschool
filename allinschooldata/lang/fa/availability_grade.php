<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability_grade', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability_grade
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['description'] = 'شاگرد باید نمرهٔ خاصی را کسب کند.';
$string['error_backwardrange'] = 'در هنگام تعیین محدودهٔ نمره، حداقل نمره باید از حداکثر نمره کمتر باشد.';
$string['error_invalidnumber'] = 'محدودهٔ نمره باید با درصدهای معتبر تعیین شود.';
$string['error_selectgradeid'] = 'باید یک مورد نمره‌دار را برای شرط نمره انتخاب کنید.';
$string['label_max'] = 'درصد حداکثر نمره (شامل خود عدد نمی‌شود)';
$string['label_min'] = 'درصد حداقل نمره (شامل خود عدد هم می‌شود)';
$string['missing'] = '(فعالیت حذف‌شده)';
$string['option_max'] = 'باید کوچکتر باشد از';
$string['option_min'] = 'باید بزرگتر یا مساوی باشد از';
$string['pluginname'] = 'محدودیت بر اساس نمره';
$string['privacy:metadata'] = 'پلاگین محدودیت بر اساس نمرات هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['requires_any'] = 'نمره‌ای در <strong>{$a}</strong> داشته باشید';
$string['requires_max'] = 'امتیاز لازم در <strong>{$a}</strong> را کسب کنید';
$string['requires_min'] = 'امتیاز لازم را در <strong>{$a}</strong> کسب کنید';
$string['title'] = 'نمره';
