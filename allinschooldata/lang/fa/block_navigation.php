<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_navigation', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_navigation
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['courseactivities'] = 'طبقه‌ها، درس‌ها، و فعالیت‌های درسی';
$string['courses'] = 'طبقه‌ها و درس‌ها';
$string['coursestructures'] = 'طبقه‌ها، درس‌ها، و ساختار درس‌ها';
$string['enabledockdesc'] = 'اجازهٔ چسباندن این بلوک به نوار چسبان به کاربر داده شود';
$string['everything'] = 'همه چیز';
$string['expansionlimit'] = 'تولید راهبری برای';
$string['linkcategoriesdesc'] = 'نمایش طبقه‌ها به صورت پیوند';
$string['navigation:addinstance'] = 'اضافه‌کردن یک بلوک جدید راهبری';
$string['navigation:myaddinstance'] = 'اضافه‌کردن یک بلوک جدید راهبری به میز کار';
$string['pluginname'] = 'راهبری';
$string['trimlength'] = 'پیرایش به چند حرف صورت گیرد';
$string['trimmode'] = 'چگونگی پیرایش';
$string['trimmodecenter'] = 'قطع کردن حروف اضافی از وسط';
$string['trimmodeleft'] = 'قطع کردن حروف اضافی از سمت راست';
$string['trimmoderight'] = 'قطع کردن حروف اضافی از سمت چپ';
