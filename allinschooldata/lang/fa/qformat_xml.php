<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qformat_xml', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qformat_xml
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['invalidxml'] = 'فایل XML نامعتبر - رشته‌ای انتظار می‌رفت (استفاده از CDATA؟)';
$string['pluginname'] = 'قالب XML مودل';
$string['pluginname_help'] = 'این یک قالب مخصوص مودل برای وارد و صادر کردن سؤال‌ها است.';
$string['truefalseimporterror'] = '<b>هشدار</b>: سؤال صحیح/غلط «{$a->questiontext}» نمی‌تواند به درستی وارد شود. واضح نبود که پاسخ صحیح درست است یا نادرست. این سؤال با فرض اینکه پاسخ «{$a->answer}» است وارد شد. اگر این فرض درست نبوده است، باید سؤال را بعداً ویرایش کنید.';
$string['unsupportedexport'] = 'نوع سؤال {$a} توسط صدور XML پشتیبانی نمی‌شود';
$string['xmlimportnoname'] = 'نام سؤال در فایل xml نیامده است';
$string['xmlimportnoquestion'] = 'متن سؤال در فایل xml نیامده است';
$string['xmltypeunsupported'] = 'نوع سؤال {$a} در ورود از فایل xml پشتیبانی نمی‌شود';
