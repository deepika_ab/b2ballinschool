<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'blog', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   blog
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addnewentry'] = 'نوشتن مطلب جدید';
$string['addnewexternalblog'] = 'ثبت یک وبلاگ بیرونی';
$string['assocdescription'] = 'اگر مطلب شما دربارهٔ یک درس یا ماژول‌های فعالیت است، لطفا آنها را انتخاب کنید.';
$string['associated'] = '{$a} مربوطه';
$string['associatewithcourse'] = 'نوشتن در وبلاگ درباره درس {$a->coursename}';
$string['associatewithmodule'] = 'نوشتن در وبلاگ درباره {$a->modtype}: {$a->modname}';
$string['association'] = 'ارتباط';
$string['associations'] = 'ارتباط‌ها';
$string['associationunviewable'] = 'این نوشته نمی‌تواند توسط سایرین دیده شود مگر اینکه به یک درس مرتبط شود یا اینکه فیلد «انتشار برای» تغییر کند';
$string['autotags'] = 'افزودن این برچسب‌ها';
$string['autotags_help'] = 'یک یا چند برچسب محلی (به‌صورت جداشده با کاما) که می‌خواهید به‌طور خودکار به هر نوشته‌ای در وبلاگ که از یک وبلاگ بیرونی کپی می‌شود اضافه شود وارد کنید.';
$string['backupblogshelp'] = 'در صورت فعال بودن، بلاگ‌ها در پشتیبان‌گیری‌های خودکار سایت شامل خواهند شد.';
$string['blockexternalstitle'] = 'بلاگ‌های خارجی';
$string['blog'] = 'بلاگ';
$string['blogaboutthis'] = 'نوشتن در وبلاگ درباره این {$a->type}';
$string['blogaboutthiscourse'] = 'نوشتن یک مطلب درباره این درس';
$string['blogaboutthismodule'] = 'نوشتن یک مطلب دربارهٔ این {$a}';
$string['blogadministration'] = 'مدیریت بلاگ';
$string['blogdeleteconfirm'] = 'پاک‌کردن نوشتهٔ «{$a}» از بلاگ؟';
$string['blogdisable'] = 'بلاگ‌نویسی غیرفعال است!';
$string['blogentries'] = 'نوشته‌های بلاگ';
$string['blogentriesabout'] = 'نوشته‌های مربوط به {$a} در وبلاگ';
$string['blogentriesbygroupaboutcourse'] = 'نوشته‌های گروه «{$a->group}» درباره درس «{$a->course}» در وبلاگ';
$string['blogentriesbygroupaboutmodule'] = 'نوشته‌های گروه «{$a->group}» درباره «{$a->mod}» در وبلاگ';
$string['blogentriesbyuseraboutcourse'] = 'نوشته‌های {$a->user} در وبلاگ درباره درس «{$a->course}»';
$string['blogentriesbyuseraboutmodule'] = 'نوشته‌های {$a->user} درباره «{$a->mod}» در وبلاگ';
$string['blogentrybyuser'] = 'نوشته‌های بلاگ توسط {$a}';
$string['blogpreferences'] = 'ترجیحات بلاگ';
$string['blogs'] = 'بلاگ‌ها';
$string['blogscourse'] = 'وبلاگ‌های درس';
$string['blogssite'] = 'بلاگ‌های سایت';
$string['blogtags'] = 'برچسب‌های بلاگ';
$string['cannotviewcourseblog'] = 'شما مجوز کافی برای دیدن بلاگ‌های این درس ندارید';
$string['cannotviewcourseorgroupblog'] = 'شما مجوز کافی برای دیدن بلاگ‌های این درس/گروه ندارید';
$string['cannotviewsiteblog'] = 'شما مجوز کافی برای مشاهده بلاگ‌های سایت را ندارید';
$string['cannotviewuserblog'] = 'شما مجوز کافی برای خواندن بلاگ‌های کاربر ندارید';
$string['configexternalblogcrontime'] = 'مودل هر چند وقت یک بار بلاگ‌های خارجی را برای وجود نوشته‌های جدید بررسی کند.';
$string['configmaxexternalblogsperuser'] = 'تعداد بلاگ‌های خارجی‌ای که هر کاربر مجاز است به بلاگ مودل خود پیوند دهد.';
$string['configuseblogassociations'] = 'فعال کردن ارتباط نوشته‌های بلاگ با درس‌ها و ماژول‌های درسی.';
$string['configuseexternalblogs'] = 'به کاربران اجازه می‌دهد که فیدهایی برای بلاگ خارجی تعیین کنند. مودل مرتب این فیدهای بلاگ‌ها را چک می‌کند و نوشته‌های جدید را در بلاگ داخلی کاربر مربوطه کپی می‌کند.';
$string['courseblog'] = 'بلاگ درس: {$a}';
$string['courseblogdisable'] = 'بلاگ‌های درس فعال نشده‌اند';
$string['courseblogs'] = 'کاربران تنها وبلاگ‌های کسانی را می‌بینند که با آنها درس مشترکی داشته باشند';
$string['deleteblogassociations'] = 'پاک کردن ارتباط‌ها با وبلاگ‌ها';
$string['deleteblogassociations_help'] = 'اگر انتخاب شده باشد آنگاه نوشته‌های وبلاگ دیگر به این درس یا هر کدام از فعالیت‌ها یا منابع این درس مرتبط نخواهند بود و این ارتباط پاک خواهد شد. خود نوشته‌های وبلاگ پاک نخواهند شد.';
$string['deleteentry'] = 'حذف نوشته';
$string['deleteexternalblog'] = 'حذف این وبلاگ بیرونی';
$string['deleteotagswarn'] = 'آیا مطمئن هستید که می‌خواهید این تگ‌ها را از همهٔ مطالب وبلاگ‌ها حذف کرده و از سیستم پاک کنید؟';
$string['description'] = 'توصیف';
$string['description_help'] = 'محتویات وبلاگ بیرونی خود را به اختصار در یک یا دو جمله وارد کنید. (اگر توصیفی وارد نشود، توصیف ثبت شده در وبلاگ بیرونی شما استفاده خواهد شد).';
$string['donothaveblog'] = 'متاسفم، شما بلاگ خودتان را ندارید.';
$string['editentry'] = 'ویرایش یک نوشته بلاگ';
$string['editexternalblog'] = 'ویرایش این بلاگ خارجی';
$string['emptybody'] = 'بدنه نوشته بلاگ نمی‌تواند خالی باشد';
$string['emptyrssfeed'] = 'آدرسی که وارد کرده‌اید به یک RSS feed معتبر اشاره نمی‌کند';
$string['emptytitle'] = 'عنوان نوشته بلاگ نمی‌تواند خالی باشد';
$string['emptyurl'] = 'باید یک آدرس به یک RSS feed معتبر تعیین کنید';
$string['entrybody'] = 'بدنه نوشته بلاگ';
$string['entrybodyonlydesc'] = 'توصیف مطلب';
$string['entryerrornotyours'] = 'این نوشته متعلق به شما نیست';
$string['entrysaved'] = 'نوشتهٔ شما ذخیره شد';
$string['entrytitle'] = 'عنوان نوشته';
$string['eventblogassociationadded'] = 'ارتباط بلاگ ساخته شد';
$string['eventblogassociationdeleted'] = 'ارتباط بلاگ حذف شد';
$string['eventblogentriesviewed'] = 'مطالب بلاگ مشاهده شد';
$string['eventblogexternaladded'] = 'وبلاگ بیرونی ثبت شد';
$string['eventblogexternalremoved'] = 'وبلاگ بیرونی از ثبت در آمد';
$string['eventblogexternalupdated'] = 'وبلاگ بیرونی به‌روز شد';
$string['evententryadded'] = 'نوشته‌ای در بلاگ اضافه شد';
$string['evententrydeleted'] = 'نوشتهٔ بلاگ پاک شد';
$string['evententryupdated'] = 'نوشته بلاگ به‌روز شد';
$string['eventexternalblogsviewed'] = 'وبلاگ ثبت‌شدهٔ بیرونی مشاهده شد';
$string['externalblogcrontime'] = 'زمانبندی cron بلاگ خارجی';
$string['externalblogdeleteconfirm'] = 'این وبلاگ بیرونی از ثبت در بیاید؟';
$string['externalblogdeleted'] = 'وبلاگ بیرونی از ثبت در آمد';
$string['externalblogs'] = 'وبلاگ‌های بیرونی';
$string['feedisinvalid'] = 'این feed نامعتبر است';
$string['feedisvalid'] = 'این feed معتبر است';
$string['filterblogsby'] = 'فیلتر کردن مطالب بر اساس...';
$string['filtertags'] = 'فیلتر کردن برچسب‌ها';
$string['filtertags_help'] = 'با استفاد از این قابلیت می‌توانید مطالبی که می‌خواهید استفاده کنید را انتخاب کنید. اگر در این قسمت برچسب‌هایی را تعیین کرده باشید (به‌صورت جداشده با کاما) آنگاه تنها مطالبی که دارای آن برچسب‌ها هستند از وبلاگ بیرونی کپی خواهند شد.';
$string['groupblog'] = 'بلاگ گروهی: {$a}';
$string['groupblogdisable'] = 'بلاگ گروهی فعال نشده است';
$string['groupblogentries'] = 'مطالب مربوط به {$a->coursename} توسط گروه {$a->groupname} در وبلاگ';
$string['groupblogs'] = 'کاربران تنها می‌توان وبلاگ افرادی را ببینند که با آنها گروه مشترکی داشته باشند';
$string['incorrectblogfilter'] = 'نوع فیلتر اشتباهی برای وبلاگ تعیین شده است';
$string['intro'] = 'این فید RSS به‌طور خودکار از روی یک یا چند وبلاگ تولید شده است.';
$string['invalidgroupid'] = 'شناسهٔ گروه نامعتبر';
$string['invalidurl'] = 'این آدرس قابل دسترسی نیست';
$string['linktooriginalentry'] = 'پیوند به نوشتهٔ وبلاگ اصلی';
$string['maxexternalblogsperuser'] = 'حداکثر تعداد بلاگ‌های خارجی به ازای هر کاربر';
$string['myprofileuserblogs'] = 'مشاهدهٔ تمام نوشته‌های وبلاگ';
$string['name'] = 'نام';
$string['name_help'] = 'یک نام گویا برای وبلاگ بیرونی خود وارد کنید. (اگر نامی وارد نشود، عنوان وبلاگ بیرونی شما استفاده خواهد شد).';
$string['noentriesyet'] = 'هیچ نوشتهٔ قابل مشاهده‌ای وجود ندارد';
$string['noguestpost'] = 'مهمان نمی‌تواند بلاگی را ارسال نماید!';
$string['nopermissionstodeleteentry'] = 'شما مجوز لازم برای پاک کردن این نوشتهٔ وبلاگ را ندارید';
$string['norighttodeletetag'] = 'شما مجوزی برای پاک کردن این تگ ندارید - {$a}';
$string['nosuchentry'] = 'چنین مطلبی در وبلاگ وجود ندارد';
$string['notallowedtoedit'] = 'شما اجازهٔ ویرایش این نوشته را ندارید';
$string['numberofentries'] = 'مطالب: {$a}';
$string['numberoftags'] = 'تعداد برچسب برای نمایش دادن';
$string['page-blog-edit'] = 'صفحه‌های ویرایشی وبلاگ';
$string['page-blog-index'] = 'صفحه‌های نمایشی وبلاگ';
$string['page-blog-x'] = 'تمام صفحه‌های وبلاگ';
$string['pagesize'] = 'تعداد مطلب در هر صفحه';
$string['permalink'] = 'پیوند ثابت';
$string['personalblogs'] = 'کاربران فقط بتوانند بلاگ خودشان را ببینند';
$string['preferences'] = 'ترجیحات وبلاگ';
$string['publishto'] = 'انتشار برای';
$string['publishtocourse'] = 'کاربرانی که درس مشترکی با شما دارند';
$string['publishtocourseassoc'] = 'اعضای درس مربوطه';
$string['publishtocourseassocparam'] = 'اعضای {$a}';
$string['publishtogroup'] = 'کاربرانی که گروه مشترکی با شما دارند';
$string['publishtogroupassoc'] = 'اعضای گروه شما در درس مربوطه';
$string['publishtogroupassocparam'] = 'اعضای گروه شما در {$a}';
$string['publishto_help'] = '۳ گزینه وجود دارد:

* خودتان (پیش‌نویس) - فقط شما و مدیران سایت می‌توانند این نوشته را ببینند

* هر کس در این سایت - هر کس که در این سایت ثبت‌نام کرده‌باشد می‌تواند این نوشته را بخواند

* هر کس در دنیا - هر کسی، شامل مهمان‌ها نیز، می‌تواند این نوشته را بخواند';
$string['publishtonoone'] = 'خودتان (پیش‌نویس)';
$string['publishtosite'] = 'هر کس در این سایت';
$string['publishtoworld'] = 'هر کس در دنیا';
$string['readfirst'] = 'ابتدا این را بخوانید';
$string['relatedblogentries'] = 'نوشته‌های مرتبط در بلاگ';
$string['retrievedfrom'] = 'برگرفته از ';
$string['rssfeed'] = 'فید RSS وبلاگ';
$string['searchterm'] = 'جستجو: {$a}';
$string['settingsupdatederror'] = 'خطایی رخ داد، تنظیمات ترجیحات وبلاگ نتوانست به‌روز شود';
$string['siteblogdisable'] = 'بلاگ سایت فعال نیست';
$string['siteblogheading'] = 'وبلاگ سایت';
$string['siteblogs'] = 'همهٔ کاربران سایت می‌توانند همهٔ نوشته‌های بلاگ‌ها را ببینند';
$string['tagdatelastused'] = 'تاریخی که برچسب برای آخرین بار استفاده شده است';
$string['tagparam'] = 'برچسب: {$a}';
$string['tags'] = 'برچسب‌ها';
$string['tagsort'] = 'مرتب کردن نمایش برچسب‌ها بر اساس';
$string['tagtext'] = 'متن برچسب';
$string['timefetched'] = 'زمان آخرین همگام‌سازی';
$string['timewithin'] = 'نمایش برچسب‌هایی که در بازهٔ این تعداد روز استفاده شده‌اند';
$string['updateentrywithid'] = 'به‌روز کردن نوشته';
$string['url'] = 'آدرس';
$string['url_help'] = 'آدرس RSS feed وبلاگ بیرونی خود را وارد کنید.';
$string['useblogassociations'] = 'فعال بودن ارتباط‌های بلاگ‌ها';
$string['useexternalblogs'] = 'فعال بودن بلاگ‌های خارجی';
$string['userblog'] = 'بلاگ کاربر: {$a}';
$string['userblogentries'] = 'نوشته‌های بلاگ توسط {$a}';
$string['valid'] = 'معتبر';
$string['viewallblogentries'] = 'تمام مطالب مربوط به این {$a}';
$string['viewallmodentries'] = 'دیدن تمام مطالب مربوط به این {$a->type}';
$string['viewallmyentries'] = 'مشاهدهٔ تمام مطالب من';
$string['viewblogentries'] = 'مطالب مربوط به این {$a->type}';
$string['viewblogsfor'] = 'مشاهدهٔ تمام مطالب برای...';
$string['viewcourseblogs'] = 'مشاهده تمام نوشته‌های مربوط به این درس';
$string['viewentriesbyuseraboutcourse'] = 'مشاهدهٔ مطالب مربوط به این درس که توسط {$a} نوشته شده‌اند';
$string['viewgroupblogs'] = 'مشاهدهٔ مطالب برای گروه...';
$string['viewgroupentries'] = 'مطالب گروه';
$string['viewmodblogs'] = 'مشاهدهٔ مطالب برای ماژول...';
$string['viewmodentries'] = 'مطالب ماژول';
$string['viewmyentries'] = 'مطالب من';
$string['viewmyentriesaboutcourse'] = 'مشاهده نوشته‌های من درباره این درس';
$string['viewmyentriesaboutmodule'] = 'دیدن مطالب من دربارهٔ این {$a}';
$string['viewsiteentries'] = 'مشاهدهٔ تمام مطالب';
$string['viewuserentries'] = 'مشاهدهٔ تمام مطالب نوشته شده توسط {$a}';
$string['worldblogs'] = 'تمام بازدیدکننده‌های سایت بتوانند نوشته‌هایی که به صورت در دسترس جهان تعیین شده‌اند بخوانند';
$string['wrongexternalid'] = 'شناسهٔ اشتباه برای وبلاگ بیرونی';
$string['wrongpostid'] = 'شناسهٔ مطلب وبلاگ اشتباه';
