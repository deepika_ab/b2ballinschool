<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'data', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   data
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'اقدامات';
$string['add'] = 'درج داده';
$string['addcomment'] = 'اضافه کردن نظر';
$string['addentries'] = 'داده‌هایی را اضافه کنید';
$string['addtemplate'] = 'قالب درج';
$string['advancedsearch'] = 'جستجوی پیشرفته';
$string['allowcomments'] = 'مجاز بودن اظهار نظر در مورد اصطلاحات';
$string['alttext'] = 'متن جایگزین';
$string['approve'] = 'تایید';
$string['approved'] = 'وضعیت تایید';
$string['ascending'] = 'صعودی';
$string['asearchtemplate'] = 'قالب جستجوی پیشرفته';
$string['atmaxentry'] = 'شما به اندازهٔ حداکثر مجاز تعداد ورودی‌ها داده وارد کرده‌اید!';
$string['authorfirstname'] = 'نام وارد کننده';
$string['authorlastname'] = 'نام خانوادگی وارد کننده';
$string['autogenallforms'] = 'تولید تمام قالب‌های پیش‌فرض';
$string['autolinkurl'] = 'پیوند شدن خودکار آدرس';
$string['availablefromdate'] = 'قابل دسترسی از';
$string['availabletags'] = 'علامت‌های موجود';
$string['availabletags_help'] = 'علامت‌ها، متن‌های خاصی هستند که در قالب‌ها استفاده می‌شوند و هنگام ویرایش یا مشاهدهٔ ورودی‌ها، با داده‌ها یا موارد دیگر (مانند آیکن ویرایش) جایگزین می‌شوند.

علامت‌های مربوط به فیلدها به صورت [[نام فیلد]] می‌باشند. تمام علامت‌های دیگر به شکل ##علامت## هستند.

فقط علامت‌هایی که در لیست «علامت‌های موجود» قرار دارند می‌توانند در قالبی که در حال ویرایش آن هستید استفاده شوند.';
$string['availabletodate'] = 'قابل دسترسی تا';
$string['blank'] = 'خالی';
$string['buttons'] = 'عملیات';
$string['bynameondate'] = 'توسط {$a->name} در {$a->date}';
$string['cancel'] = 'انصراف';
$string['cannotaccesspresentsother'] = 'شما اجازهٔ دسترسی به مجموعه‌های آمادهٔ سایر کاربران را ندارید';
$string['cannotadd'] = 'نمی‌توانید داده‌ای وارد کنید!';
$string['cannotdeletepreset'] = 'خطا در حذف مجموعهٔ آماده!';
$string['cannotunziptopreset'] = 'نمی‌توان فایل مجموعهٔ آماده را از حالت فشرده خارج کرد';
$string['checkbox'] = 'گزینه‌های مربعی';
$string['chooseexportfields'] = 'فیلدهایی که می‌خواهید صادر شوند را انتخاب کنید:';
$string['chooseexportformat'] = 'قالب صدور را انتخاب کنید:';
$string['chooseorupload'] = 'انتخاب فایل';
$string['columns'] = 'ستون';
$string['comment'] = 'نظر';
$string['commentdeleted'] = 'نظر حذف شد';
$string['commentempty'] = 'نظر خالی بود';
$string['comments'] = 'نظرات';
$string['commentsaved'] = 'نظر ذخیره شد';
$string['commentsn'] = '{$a} نظر';
$string['commentsoff'] = 'قابلیت نظر دادن فعال نیست';
$string['configenablerssfeeds'] = 'این گزینه امکان RSS feed ها را برای تمام بانک‌های اطلاعاتی فعال می‌کند. البته پس از فعال کردن این قابلیت، باید در قسمت تنظیمات هر بانک اطلاعاتی، feed ها را فعال کنید.';
$string['confirmdeletefield'] = 'شما در آستانهٔ حذف این فیلد هستید. آیا مطمئنید؟';
$string['confirmdeleterecord'] = 'آیا مطمئن هستید که می‌خواهید این مورد را حذف کنید؟';
$string['csstemplate'] = 'قالب CSS';
$string['csvfailed'] = 'خواندن داده‌های خام از فایل CSV امکان‌پذیر نبود';
$string['csvfile'] = 'فایل CSV';
$string['csvimport'] = 'ورود از فایل CSV';
$string['csvimport_help'] = 'داده‌ها را می‌توان از طریق یک فایل متنی ساده که در خط اول شامل لیستی از نام فیلدها و در خط‌های بعدی داده‌ها به صورت هر رکورد داده در یک خط باشد وارد کرد.';
$string['csvwithselecteddelimiter'] = 'فایل متنی <acronym title="Comma Separated Values (مقادیر جدا شده از هم توسط کاما)">CSV</acronym> با حرفِ جداکنندهٔ انتخاب شده:';
$string['data:addinstance'] = 'اضافه‌کردن یک بانک اطلاعاتی جدید';
$string['data:approve'] = 'تایید ورودی‌های تایید نشده';
$string['data:comment'] = 'نوشتن نظر';
$string['data:exportallentries'] = 'صادر کردن تمام داده‌های بانک اطلاعاتی';
$string['data:exportentry'] = 'صادر کردن یک داده از بانک اطلاعاتی';
$string['data:exportownentry'] = 'صادر کردن داده‌های وارد شده توسط خود';
$string['data:managecomments'] = 'مدیریت نظرات';
$string['data:manageentries'] = 'مدیریت ورودی‌ها';
$string['data:managetemplates'] = 'مدیریت قالب‌ها';
$string['data:manageuserpresets'] = 'مدیریت همهٔ مجموعه قالب‌های از پیش آماده';
$string['data:rate'] = 'ارزیابی ورودی‌ها';
$string['data:readentry'] = 'خواندن ورودی‌ها';
$string['data:viewallratings'] = 'دیدن تمام ارزیابی‌های خام صورت گرفته توسط افراد';
$string['data:viewalluserpresets'] = 'مشاهدهٔ مجموعه‌های آمادهٔ همهٔ کاربران';
$string['data:viewanyrating'] = 'دیدن ارزیابی‌های نهایی‌ای که هر کس دریافت کرده است';
$string['data:viewentry'] = 'دیدن ورودی‌ها';
$string['data:viewrating'] = 'دیدن ارزیابی نهایی‌ای که دریافت کرده‌اید';
$string['data:writeentry'] = 'نوشتن ورودی‌ها';
$string['date'] = 'تاریخ';
$string['dateentered'] = 'تاریخ وارد شده';
$string['defaultfielddelimiter'] = '(به طور پیش‌فرض حرف ویرگول انگلیسی است)';
$string['defaultfieldenclosure'] = '(به طور پیش‌فرض هیچ حرفی نیست)';
$string['defaultsortfield'] = 'فیلد پیش‌فرض مرتب‌سازی';
$string['delete'] = 'حذف';
$string['deleteallentries'] = 'حذف تمام داده‌ها';
$string['deletecomment'] = 'آیا مطمئن هستید که می‌خواهید این نظر را حذف کنید؟';
$string['deleted'] = 'حذف شد';
$string['deletefield'] = 'حذف یک فیلد موجود';
$string['deletenotenrolled'] = 'حذف داده‌هایی که توسط کاربرانی که ثبت نام نیستند وارد شده‌اند';
$string['deletewarning'] = 'آیا مطمئن هستید که می‌خواهید این مجموعهٔ آماده را حذف کنید؟';
$string['descending'] = 'نزولی';
$string['directorynotapreset'] = '{$a->directory} یک مجموعهٔ آماده نیست: فایل‌های گمشده: {$a->missing_files}';
$string['download'] = 'دریافت فایل';
$string['edit'] = 'ویرایش';
$string['editcomment'] = 'ویرایش نظر';
$string['editentry'] = 'ویرایش داده';
$string['editordisable'] = 'غیرفعال کردن ویرایشگر';
$string['editorenable'] = 'فعال کردن ویرایشگر';
$string['emptyadd'] = 'قالب درج خالی است. در حال تولید یک فرم پیش‌فرض...';
$string['emptyaddform'] = 'شما هیچ فیلدی را پر نکردید!';
$string['entries'] = 'داده‌های ورودی';
$string['entrieslefttoadd'] = 'برای کامل کردن این فعالیت باید {$a->entriesleft} دادهٔ دیگر وارد کنید';
$string['entrieslefttoaddtoview'] = 'باید {$a->entrieslefttoview} دادهٔ دیگر وارد کنید تا بتوانید داده‌های وارد شده توسط سایرین را ببینید.';
$string['entry'] = 'دادهٔ ورودی';
$string['entrysaved'] = 'دادهٔ ورودی شما ذخیره شده است';
$string['errormustbeteacher'] = 'برای استفاده از این صفحه باید یک استاد باشید!';
$string['errorpresetexists'] = 'یک مجموعهٔ آماده با همین نام از قبل وجود دارد';
$string['example'] = 'نمونه ماژول بانک اطلاعاتی';
$string['excel'] = 'فایل Excel';
$string['expired'] = 'متأسفیم، این فعالیت در {$a} بسته شد و دیگر در دسترس نیست';
$string['export'] = 'صدور';
$string['exportaszip'] = 'صادر کردن به صورت فایل فشردهٔ zip';
$string['exportaszip_help'] = 'قابلیت صادر کردن به صورت فایل zip، به شما امکان ذخیرهٔ قالب‌ها و فیلدها را به صورت یک مجموعهٔ آمادهٔ از پیش تعریف شده در قالب یک فایل فشردهٔ zip که قابل دریافت از روی سایت است را می‌دهد. این فایل zip را می‌تواند بعداً در درس دیگری وارد کرد.';
$string['exportedtozip'] = 'به فایل zip موقتی صادر شد...';
$string['exportentries'] = 'صدور محتویات به فایل';
$string['exportownentries'] = 'فقط صادر شدن داده‌های وارد شدهٔ خودتان؟ ({$a->mine} از {$a->all})';
$string['failedpresetdelete'] = 'خطا در حذف یک مجموعه داده';
$string['fieldadded'] = 'فیلد اضافه شد';
$string['fieldallowautolink'] = 'اجازهٔ پیونددهی خودکار';
$string['fielddeleted'] = 'فیلد حذف شد';
$string['fielddelimiter'] = 'جدا کنندهٔ فیلدها';
$string['fielddescription'] = 'توصیف فیلد';
$string['fieldenclosure'] = 'حصار فیلدها';
$string['fieldheight'] = 'ارتفاع';
$string['fieldheightlistview'] = 'ارتفاع تصویر به هنگام نمایش در لیست';
$string['fieldheightsingleview'] = 'ارتفاع تصویر به هنگام نمایش تکی';
$string['fieldids'] = 'شناسه‌های فیلدها';
$string['fieldmappings'] = 'نگاشت فیلدها';
$string['fieldmappings_help'] = 'این منو امکان نگهداری داده‌های بانک اطلاعاتی موجود را فراهم می‌کند. برای حفظ دادهٔ یک فیلد، باید آن فیلد را به یک فیلد جدید (که داده در آن ظاهر خواهد شد) نگاشت کنید. همچنین می‌توان هر فیلدی را خالی باقی گذاشت، که در این صورت هیچ اطلاعاتی در آن کپی نمی‌شود. هر فیلد قدیمی‌ای که به یک فیلد جدید نگاشته نشود از دست خواهد رفت و تمام داده‌هایش حذف خواهد شد.
فقط فیلدهایی که از یک نوع باشند را می‌توانید روی یکدیگر بنگارید، بنابراین هر کدام از منوها شامل فیلدهای متفاوتی می‌باشند. همچنین، باید مراقب باشید که یک فیلد قدیمی را روی بیشتر از یک فیلد جدید ننگارید.';
$string['fieldname'] = 'نام فیلد';
$string['fieldnotmatched'] = 'این فیلدها که در فایل شما وجود دارند، در این بانک اطلاعاتی شناخته شده نیستند: {$a}';
$string['fieldoptions'] = 'گزینه‌ها (هر کدام در یک خط)';
$string['fields'] = 'فیلدها';
$string['fieldupdated'] = 'فیلد به‌روز شد';
$string['fieldwidth'] = 'عرض';
$string['fieldwidthlistview'] = 'عرض تصویر به هنگام نمایش در لیست';
$string['fieldwidthsingleview'] = 'عرض تصویر به هنگام نمایش تکی';
$string['file'] = 'فایل';
$string['fileencoding'] = 'کدگذاری';
$string['filesnotgenerated'] = 'همهٔ فایل‌ها تولید نشدند: {$a}';
$string['filtername'] = 'پیوند دهی خودکار بانک اطلاعاتی';
$string['footer'] = 'پا صفحه';
$string['forcelinkname'] = 'نام اجباری برای پیوند';
$string['foundnorecords'] = 'هیچ رکوردی پیدا نشد (<a href="{$a->reseturl}">بازنشانی فیلترها</a>)';
$string['foundrecords'] = 'رکوردهای پیدا شده: {$a->num} از {$a->max} (<a href="{$a->reseturl}">بازنشانی فیلترها</a>)';
$string['fromfile'] = 'وارد کردن از فایل فشردهٔ zip';
$string['fromfile_help'] = 'قابلیت وارد کردن از فایل فشردهٔ zip، امکان انتخاب و ارسال یک فایل فشردهٔ zip شامل قالب‌ها و فیلدها را فراهم می‌کند.';
$string['generateerror'] = 'تمام فایل‌ها ساخته نشدند!';
$string['header'] = 'سر صفحه';
$string['headeraddtemplate'] = 'طراحی ظاهر صفحهٔ ویرایش ورودی‌ها';
$string['headerasearchtemplate'] = 'طراحی ظاهر فرم جستجوی پیشرفته';
$string['headercsstemplate'] = 'تعریف CSS های داخلی برای سایر قالب‌ها';
$string['headerjstemplate'] = 'تعریف جاوا اسکریپت‌های سفارشی برای سایر قالب‌ها';
$string['headerlisttemplate'] = 'طراحی ظاهر صفحهٔ مرور داده‌ها به صورت چندتایی';
$string['headerrsstemplate'] = 'طراحی نحوهٔ نمایش داده‌ها در RSS feed ها';
$string['headersingletemplate'] = 'طراحی ظاهر صفحهٔ مرور داده‌ها به صورت تکی';
$string['importentries'] = 'ورود محتویات از فایل';
$string['importsuccess'] = 'مجموعهٔ آماده با موفقیت استفاده شد.';
$string['insufficiententries'] = 'برای دیدن این بانک اطلاعاتی، داده‌های ورودی بیشتری نیاز است';
$string['intro'] = 'متن معرفی';
$string['invalidaccess'] = 'این صفحه به طور مناسب مورد دسترسی قرار نگرفت';
$string['invalidfieldid'] = 'شناسهٔ فیلد نادرست است';
$string['invalidfieldname'] = 'لطفاً نام دیگری را برای این فیلد انتخاب کنید.';
$string['invalidfieldtype'] = 'نوع فیلد نادرست است';
$string['invalidid'] = 'شناسهٔ دادهٔ نادرست';
$string['invalidpreset'] = '{$a} یک مجموعهٔ آماده نیست.';
$string['invalidrecord'] = 'رکورد نادرست';
$string['invalidurl'] = 'آدرسی که وارد کرده‌اید معتبر نیست';
$string['jstemplate'] = 'قالب جاوا اسکریپت';
$string['latitude'] = 'عرض جغرافیایی';
$string['latlong'] = 'طول و عرض جغرافیایی';
$string['latlongdownloadallhint'] = 'پیوند دریافت تمام داده‌ها به صورت فایل KML';
$string['latlongkmllabelling'] = 'چگونگی عنوان‌گذاری موردها در فایل‌های KML (مربوط به Google Earth)';
$string['latlonglinkservicesdisplayed'] = 'پیوند به سرویس‌های خارجی برای نمایش';
$string['latlongotherfields'] = 'سایر فیلدها';
$string['list'] = 'مشاهدهٔ لیست';
$string['listtemplate'] = 'قالب لیست';
$string['longitude'] = 'طول جغرافیایی';
$string['manageapproved'] = 'مجاز بودن ویرایش ورودی‌های تایید شده';
$string['manageapproved_help'] = 'اگر غیرفعال باشد، ورودی‌های تاییدشده دیگر توسط کاربری که آنها را اضافه کرده است قابل ویرایش یا قابل حذف شدن نیستند. این تنظیم هیچ تاثیری ندارد مگر اینکه تنظیم «نیازمند تایید» فعال باشد.';
$string['mapexistingfield'] = 'نگاشت به {$a}';
$string['mapnewfield'] = 'ایجاد یک فیلد جدید';
$string['mappingwarning'] = 'تمام فیلدهای قدیمی‌ای که به یک فیلد جدید نگاشته نشوند از دست خواهند رفت و داده‌هایشان حذف خواهد شد.';
$string['maxentries'] = 'حداکثر داده‌ها';
$string['maxentries_help'] = 'بیشترین تعداد داده‌هایی که یک شاگرد اجازه دارد در این فعالیت وارد کند.';
$string['maxsize'] = 'حداکثر حجم';
$string['menu'] = 'منوی باز شونده';
$string['menuchoose'] = 'انتخاب کنید...';
$string['missingdata'] = 'شناسهٔ داده یا شئ باید برای کلاس فیلد فراهم شده باشد';
$string['missingfield'] = 'اشتباه برنامه‌نویس: هنگام تعریف کلاس field باید فیلد و یا داده را مشخص کنید.';
$string['modulename'] = 'بانک اطلاعاتی';
$string['modulename_help'] = 'با استفاده از ماژول فعالیت پایگاه داده، افراد می‌توانند بانکی از اطلاعات ثبت شده (رکوردهای اطلاعاتی) را ایجاد و نگهداری و در میان داده‌های آن جستجو کنند. قالب و ساختار این اطلاعات توسط استاد به‌عنوان مجموعه‌ای از فیلدها تعریف می‌شود. این فیلدها می‌توانند از نوع مربع‌های قابل انتخاب، دکمه‌های رادیویی، منوهای باز شونده، کادرهای متنی، آدرس‌های اینترنتی، عکس و فایل‌های ارسال شده باشند.

چیدمان ظاهری اطلاعات در زمانی که لیست شده‌اند، مشاهده می‌شوند یا در حال ویرایش هستند، همه توسط قالب‌های بانک اطلاعاتی قابل تنظیم هستند. بانک‌های اطلاعاتی می‌توانند به صورت مجموعه‌های آماده بین درس‌ها به اشتراک گذاشته شوند. همچنین اساتید می‌توانند داده‌های بانک اطلاعاتی را به فایل صادر و از فایل وارد کنند.

اگر فیلتر پیونددهی خودکار بانک اطلاعاتی فعال باشد، هرگاه که کلمات یا عبارت‌های موجود در بانک اطلاعاتی در جایی از درس به کار رفته باشند، آن کلمات یا عبارت‌ها به طور خودکار به رکورد مربوطه در بانک اطلاعاتی پیوند خواهند شد.

استاد می‌تواند نظر دادن در مورد اطلاعات موجود در بانک را فعال کند. همچنین اطلاعات موجود در بانک می‌توانند توسط استاد یا شاگردان (ارزشیابی همکلاسان از کار یکدیگر) امتیاز دهی شوند. امتیازها می‌توانند جمع‌آوری شوند تا یک نمره نهایی را تشکیل دهند که در دفتر نمره ثبت شود.

بانک‌های اطلاعاتی کاربردهای بسیاری دارند، مانند

* یک مجموعه حاصل از کار مشارکتی شامل پیوندهای وب، کتاب‌ها، نقدهای صورت گرفته در مورد کتاب‌ها، مجلات علمی و غیره
* برای نمایش دادن عکس‌های گرفته شده، پوسترهای طراحی شده، وب‌گاه‌های ساخته شده یا شعرهای نوشته شده توسط شاگردان به منظور دریافت نظر و نقد سایر همکلاسی‌ها';
$string['modulenameplural'] = 'بانک‌های اطلاعاتی';
$string['more'] = 'نمایش جزئیات';
$string['moreurl'] = 'آدرس صفحهٔ جزئیات';
$string['movezipfailed'] = 'فایل فشردهٔ zip را نمی‌توان منتقل کرد';
$string['multientry'] = 'قسمت تکرار شونده';
$string['multimenu'] = 'منو (چند انتخابی)';
$string['multipletags'] = 'بعضی از علامت‌ها چند بار تکرار شده‌اند! قالب ذخیره نشد';
$string['newentry'] = 'ورودی جدید';
$string['newfield'] = 'تعریف یک فیلد جدید';
$string['newfield_help'] = 'از طریق فیلدها می‌توان داده‌ها را وارد کرد. هر دادهٔ ورودی در یک فعالیت بانک اطلاعاتی می‌تواند شامل چندین فیلد از انواع مختلف باشد. مانند فیلدهای تاریخی که به افراد امکان انتخاب یک روز، ماه و سال را از یک لیست بازشونده می‌دهند؛ فیلدهای تصویری که به افراد اجازهٔ ارسال یک فایل تصویری را می‌دهند؛ یا گزینه‌های مربعی که به افراد اجازهٔ انتخاب یک یا چند گزینه را می‌دهند.

هر فیلد باید یک نام منحصر به فرد داشته باشد. نوشتن توصیف برای فیلدها اختیاری است.';
$string['noaccess'] = 'شما اجازهٔ دسترسی به این صفحه را ندارید.';
$string['nodefinedfields'] = 'مجموعهٔ آمادهٔ جدید شامل هیچ فیلد تعریف شده‌ای نیست!';
$string['nofieldcontent'] = 'محتوای فیلد پیدا نشد';
$string['nofieldindatabase'] = 'فیلدهای این بانک اطلاعاتی تعریف نشده‌اند.';
$string['nolisttemplate'] = 'قالب لیست هنوز تعریف نشده است';
$string['nomatch'] = 'دادهٔ مطابقی پیدا نشد!';
$string['nomaximum'] = 'حداکثر ندارد';
$string['norecords'] = 'بانک اطلاعاتی خالی است';
$string['nosingletemplate'] = 'قالب تکی هنوز تعریف نشده است';
$string['notapproved'] = 'دادهٔ ورودی هنوز تایید نشده است.';
$string['notinjectivemap'] = 'نگاشت یک‌به‌یک نیست';
$string['notopenyet'] = 'با عرض پوزش، این فعالیت تا قبل از {$a} در دسترس نیست';
$string['number'] = 'عدد';
$string['numberrssarticles'] = 'متن‌های RSS';
$string['numnotapproved'] = 'در حال بررسی';
$string['numrecords'] = '{$a} دادهٔ ورودی';
$string['ods'] = 'فایل <acronym title="OpenDocument Spreadsheet (برنامهٔ صفحه گسترده در مجموعهٔ اپن آفیس)">ODS</acronym> (مجموعهٔ OpenOffice)';
$string['optionaldescription'] = 'شرح کوتاه (اختیاری)';
$string['optionalfilename'] = 'نام فایل (اختیاری)';
$string['other'] = 'غیره';
$string['overrwritedesc'] = 'بازنویسی مجموعهٔ آماده اگر از قبل وجود داشته باشد';
$string['overwrite'] = 'بازنویسی';
$string['overwritesettings'] = 'بازنویسی تنظیمات فعلی';
$string['page-mod-data-x'] = 'هر صفحه‌ای از ماژول فعالیت بانک اطلاعاتی';
$string['pagesize'] = 'تعداد داده‌ها در هر صفحه';
$string['participants'] = 'شرکت کنندگان';
$string['picture'] = 'تصویر';
$string['pleaseaddsome'] = 'لطفاً فیلدهایی را در پائین تعریف نمائید یا برای شروع <a href="{$a}">یک مجموعهٔ از پیش تعریف شده را انتخاب کنید</a>.';
$string['pluginadministration'] = 'مدیریت بانک اطلاعاتی';
$string['pluginname'] = 'بانک اطلاعاتی';
$string['portfolionotfile'] = 'صادر کردن به یک محفظه الکترونیکی اسناد به جای یک فایل (فقط csv و leap2a)';
$string['presetinfo'] = 'ذخیره به عنوان یک مجموعهٔ آماده موجب انتشار این قالب خواهد شد. سایر کاربران قادر خواهند بود که از این قالب در بانک‌های اطلاعاتی خود استفاده کنند.';
$string['presets'] = 'مجموعه‌های آماده';
$string['radiobutton'] = 'دکمه‌های رادیویی';
$string['recordapproved'] = 'دادهٔ ورودی تایید شد';
$string['recorddeleted'] = 'حذف شد';
$string['recordsnotsaved'] = 'هیچ داده‌ای ذخیره نشد. لطفاً قالب فایل ارسال شده را بررسی کنید.';
$string['recordssaved'] = 'داده ذخیره شد';
$string['requireapproval'] = 'نیازمند تایید؟';
$string['requireapproval_help'] = 'در صورت فعال بودن، برای اینکه داده‌های وارد شده بتوانند توسط همگان قابل مشاهده باشند، باید توسط یک استاد تایید شوند.';
$string['required'] = 'اجباری';
$string['requiredentries'] = 'تعداد داده‌های لازم';
$string['requiredentries_help'] = 'تعداد داده‌هایی که یک شاگرد باید در این بانک اطلاعاتی ثبت کند تا این فعالیت به صورت کامل شده برایش در نظر گرفته شود.';
$string['requiredentriestoview'] = 'تعداد داده‌های لازم برای دیدن';
$string['requiredentriestoview_help'] = 'تعداد داده‌هایی که یک شاگرد باید در بانک اطلاعاتی ثبت کند تا بتواند داده‌هایی که توسط سایر شاگردان وارد شده است ببیند.

توجه: اگر ورود داده‌ها برای دیدن لازم است، فیلتر پیونددهی خودکار بانک اطلاعاتی باید غیر فعال باشد. زیرا فیلتر پیونددهی خودکار بانک اطلاعاتی نمی‌تواند تشخیص دهد که آیا شاگردی به تعداد لازم داده وارد کرده است یا خیر.';
$string['requiredfield'] = 'فیلد اجباری';
$string['resetsettings'] = 'بازنشانی فیلترها';
$string['resettemplate'] = 'بازنشانی قالب';
$string['resizingimages'] = 'تغییر اندازهٔ عکس‌های کوچک...';
$string['rows'] = 'سطر';
$string['rssglobaldisabled'] = 'غیر فعال است. پیکربندی سایت را ببینید.';
$string['rsstemplate'] = 'قالب RSS';
$string['rsstitletemplate'] = 'قالب عنوان RSS';
$string['save'] = 'ذخیره';
$string['saveandadd'] = 'ذخیره و درج دادهٔ جدید';
$string['saveandview'] = 'ذخیره و نمایش';
$string['saveaspreset'] = 'ذخیره به عنوان یک مجموعهٔ آماده';
$string['saveaspreset_help'] = 'قابلیت ذخیره به عنوان مجموعهٔ آماده، قالب‌ها و فیلدها را به صورت یک محموعهٔ آماده که توسط سایرین قابل استفاده است، منتشر می‌کند. (هر زمان که بخواهید می‌توانید آن را از لیست مجموعه‌های آماده پاک کنید.)';
$string['savesettings'] = 'ذخیرهٔ تنظیمات';
$string['savesuccess'] = 'با موفقیت ذخیره شد. مجموعهٔ آمادهٔ شما اکنون در سراسر سایت در دسترس خواهد بود.';
$string['savetemplate'] = 'ذخیرهٔ قالب';
$string['search'] = 'جستجو';
$string['search:activity'] = 'بانک اطلاعاتی - اطلاعات فعالیت';
$string['search:entry'] = 'بانک اطلاعاتی - مدخل‌ها';
$string['selectedrequired'] = 'تمامی گزینه‌های انتخاب شده لازم هستند';
$string['showall'] = 'نمایش همهٔ داده‌ها';
$string['single'] = 'مشاهدهٔ تکی';
$string['singletemplate'] = 'قالب تکی';
$string['subplugintype_datafield'] = 'نوع فیلد بانک اطلاعاتی';
$string['subplugintype_datafield_plural'] = 'انواع فیلد بانک اطلاعاتی';
$string['subplugintype_datapreset'] = 'مجموعهٔ آماده';
$string['subplugintype_datapreset_plural'] = 'مجموعه‌های آماده';
$string['tags'] = 'tag ها';
$string['teachersandstudents'] = '{$a->teachers} و {$a->students}';
$string['templates'] = 'قالب‌ها';
$string['templatesaved'] = 'قالب ذخیره شد';
$string['text'] = 'متن';
$string['textarea'] = 'متن چند خطی';
$string['timeadded'] = 'زمان اضافه شدن';
$string['timemodified'] = 'زمان آخرین تغییر';
$string['todatabase'] = 'به این بانک اطلاعاتی.';
$string['type'] = 'نوع فیلد';
$string['undefinedprocessactionmethod'] = 'هیچ متد عملی در مجموعه داده‌های آماده برای رسیدگی به عمل «{$a}» تعریف نشده است.';
$string['unsupportedexport'] = '({$a->fieldtype}) نمی‌تواند صادر شود.';
$string['updatefield'] = 'به‌روزرسانی یک فیلد موجود';
$string['uploadfile'] = 'ارسال فایل';
$string['uploadrecords'] = 'دریافت اطلاعات از فایل';
$string['uploadrecords_help'] = 'محتویات را می‌توان با استفاده از فایل متنی به بانک فرستاد. ساختار فایل باید به این صورت باشد:

* هر خط از فایل شامل یک رکورد است
* هر رکورد، سلسله‌ای از داده‌ها است که با کاما (یا سایر جدا کننده‌ها) از هم جدا شده‌اند
* رکورد اول شامل لیستی از نام فیلدها است و تعیین کنندهٔ ساختار بقیهٔ فایل می‌باشد

«حصار فیلد» حرفی است که تک تک فیلدها در تک تک رکوردها را محصور می‌کند. معمولاً می‌شود حصاری را تعیین نکرد.';
$string['url'] = 'آدرس اینترنتی';
$string['usestandard'] = 'استفاده از یک مجموعهٔ آماده';
$string['usestandard_help'] = 'برای استفاده از یک مجموعهٔ از پیش آمادهٔ موجود در کل سایت، آن را از لیست انتخاب کنید. (اگر خودتان با استفاده از قابلیت «ذخیره به عنوان یک مجموعهٔ آماده» مجموعه‌ای را به لیست اضافه کرده باشید، آنگاه امکان حذف آن را خواهید داشت.)';
$string['viewfromdate'] = 'فقط خواندنی باشد از';
$string['viewtodate'] = 'فقط خواندنی باشد تا';
$string['wrongdataid'] = 'شناسهٔ دادهٔ فراهم شده اشتباه است';
