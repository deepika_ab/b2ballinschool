<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_bulkenrol', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   local_bulkenrol
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['bulkenrol:enrolusers'] = 'استفاده از ثبت‌نام دسته‌جمعی';
$string['enrolplugin'] = 'پلاگین ثبت‌نام';
$string['enrol_users'] = 'ثبت‌نام کاربران';
$string['enrol_users_successful'] = 'ثبت‌نام دسته‌جمعی کاربران موفق بود';
$string['error_enrol_user'] = 'مشکلی در ثبت‌نام کاربر با ایمیل <em>{$a->email}</em> در این درس وجود داشت.';
$string['error_enrol_users'] = 'هنگام ثبت‌نام کاربران در درس مشکلی پیش آمد.';
