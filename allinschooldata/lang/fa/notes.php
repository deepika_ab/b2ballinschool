<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'notes', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   notes
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addnewnote'] = 'اضافه کردن یک یادداشت جدید';
$string['addnewnoteselect'] = 'کاربرانی را برای نوشتن یادداشت در موردشان انتخاب کنید';
$string['bynameondate'] = 'از {$a->name} در {$a->date}';
$string['configenablenotes'] = 'فعال کردن قابلیت نوشتن یادداشت‌هایی در مورد هر یک از کاربران.';
$string['content'] = 'متن';
$string['course'] = 'درس';
$string['coursenotes'] = 'یادداشت‌های درس';
$string['created'] = 'ایجاد';
$string['deleteconfirm'] = 'حذف این یادداشت؟';
$string['deletenotes'] = 'حذف همهٔ یادداشت‌ها';
$string['editnote'] = 'ویرایش یادداشت';
$string['enablenotes'] = 'فعال بودن یادداشت‌ها';
$string['eventnotecreated'] = 'یادداشت ایجاد شد';
$string['eventnotedeleted'] = 'یادداشت حذف شد';
$string['eventnotesviewed'] = 'یادداشت‌ها مشاهده شد';
$string['eventnoteupdated'] = 'یادداشت به‌روز شد';
$string['invalidid'] = 'شناسهٔ یادداشت ارائه شده نامعتبر است.';
$string['invaliduserid'] = 'کد کاربری نامعتبر: {$a}';
$string['myprofileownnotes'] = 'یادداشت‌های من';
$string['nocontent'] = 'متن یادداشت نمی‌تواند خالی باشد';
$string['nonotes'] = 'هنوز یادداشتی از این نوع نوشته نشده است';
$string['nopermissiontodelete'] = 'نمی‌توانید این یادداشت را پاک کنید';
$string['note'] = 'یادداشت';
$string['notes'] = 'یادداشت‌ها';
$string['notesdisabled'] = 'یادداشت‌ها غیرفعال هستند.';
$string['notesnotvisible'] = 'شما مجاز به مشاهدهٔ یادداشت نیستید.';
$string['nouser'] = 'باید کاربری را انتخاب کنید';
$string['page-notes-index'] = 'صفحهٔ اصلی یادداشت‌ها';
$string['page-notes-x'] = 'هر صفحه‌ای از یادداشت‌ها';
$string['personal'] = 'خصوصی';
$string['personalnotes'] = 'یادداشت‌های خصوصی';
$string['publishstate'] = 'دامنه';
$string['publishstate_help'] = 'دامنهٔ یک یادداشت تعیین می‌کند که چه کسانی می‌توانند آن را ببینند.

* خصوصی - یادداشت  فقط توسط شما قابل دیدن خواهد بود
* درس - یادداشت توسط اساتید این درس قابل دیدن خواهد بود
* سایت - یادداشت توسط اساتید همهٔ درس‌ها قابل دیدن خواهد بود';
$string['site'] = 'سایت';
$string['sitenotes'] = 'یادداشت‌های سایت';
$string['unknown'] = 'ناشناخته';
