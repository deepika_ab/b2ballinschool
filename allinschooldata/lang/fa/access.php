<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'access', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   access
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['access'] = 'دسترسی';
$string['accesshelp'] = 'راهنمای دسترسی';
$string['accesskey'] = 'کلید دستیابی، {$a}';
$string['accessstatement'] = 'شرح میزان دسترسی';
$string['activitynext'] = 'فعالیت بعدی';
$string['activityprev'] = 'فعالیت قبلی';
$string['breadcrumb'] = 'نوار راهبری';
$string['eventcontextlocked'] = '';
$string['eventcontextunlocked'] = '';
$string['hideblocka'] = 'نهفتن بلوک {$a}';
$string['showblocka'] = 'نمایش بلوک {$a}';
$string['sitemap'] = 'معماری سایت';
$string['skipa'] = 'گذشتن از {$a}';
$string['skipblock'] = 'گذشتن از بلوک';
$string['skipnavigation'] = 'گذشتن از راهبری';
$string['skipto'] = 'عبور به {$a}';
$string['tocontent'] = 'پرش به محتوای اصلی';
$string['tonavigation'] = 'رفتن به راهبری';
$string['youarehere'] = 'شما اینجا هستید';
