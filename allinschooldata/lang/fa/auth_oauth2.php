<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_oauth2', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   auth_oauth2
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accountexists'] = 'یک حساب کاربری با همین نام کاربری در این سایت وجود دارد. اگر این حساب کاربری متعلق به شما است، به‌صورت دستی وارد شوید و این پیوند را از صفحهٔ ترجیحات خود برقرار کنید.';
$string['auth_oauth2description'] = 'شناسایی مبتنی بر استانداردهای OAuth 2';
$string['auth_oauth2settings'] = 'تنظیمات شناسایی OAuth 2';
$string['confirmaccountemail'] = 'سلام {$a->fullname}،

با استفاده از نشانی الکترونیکی شما، یک حساب کاربری جدید در «{$a->sitename}» درخواست شده است.

برای تایید حساب کاربری جدیدتان، لطفا به آدرس زیر بروید:

{$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

در صورت نیاز به کمک، لطفاً با مدیر سایت تماس بگیرید،
{$a->admin}';
$string['confirmlinkedloginemail'] = 'سلام {$a->fullname}،

با استفاده از نشانی الکترونیکی شما یک درخواست برای پیوند دادن حساب {$a->linkedemail} در {$a->issuername} به حساب کاربری شما در «{$a->sitename}» ثبت شده است.

برای تایید این تقاضا و پیوند دادن این حساب‌ها، لطفا به آدرس زیر بروید:

{$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

در صورت نیاز به کمک، لطفاً با مدیر سایت تماس بگیرید،
{$a->admin}';
$string['createaccountswarning'] = 'این پلاگین شناسایی به کاربران اجازه می‌دهد که در سایت شما حساب کاربری ایجاد کنند. در صورت استفاده از این پلاگین، شاید بخواهید که تنظیم «authpreventaccountcreation» را فعال کنید.';
$string['emailpasswordchangeinfosubject'] = '{$a}: اطلاعات مربوط به تغییر رمز ورود';
$string['issuer'] = 'سرویس OAuth 2';
$string['loginerror_cannotcreateaccounts'] = 'حساب کاربری وجود ندارد و این سایت اجازهٔ اینکه افراد خودشان عضو سایت شوند را نمی‌دهد.';
$string['notloggedindebug'] = 'تلاش برای وارد شدن ناموفق بود. دلیل: {$a}';
$string['oauth2:managelinkedlogins'] = 'مدیریت حساب‌های کاربری مرتبط با خود';
$string['pluginname'] = 'OAuth 2';
