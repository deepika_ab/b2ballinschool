<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_manual', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   auth_manual
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auth_manualdescription'] = 'این روش تمام راه‌های موجود برای ایجاد حساب کاربری توسط خود کاربران را از بین می‌برد. تمام حساب‌ها باید به صورت دستی توسط مدیر ایجاد شوند.';
$string['expiration'] = 'فعال بودن انقضای رمز ورود';
$string['expiration_desc'] = 'رمزهای ورود کاربران بعد از زمان مشخصی منقضی خواهند شد.';
$string['expiration_warning'] = 'آستانهٔ اطلاع‌رسانی';
$string['expiration_warning_desc'] = 'تعداد روزهای مانده به انقضای رمز ورود که باید اطلاعیه صادر شود.';
$string['passwdexpire_settings'] = 'تنظیمات انقضای رمز ورود';
$string['passwdexpiretime'] = 'مدت رمز ورود';
$string['passwdexpiretime_desc'] = 'طول زمانی که در آن مدت رمز ورود معتبر است.';
$string['pluginname'] = 'حساب‌های دستی';
