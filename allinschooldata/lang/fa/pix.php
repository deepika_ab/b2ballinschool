<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'pix', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   pix
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['angry'] = 'عصبانی';
$string['approve'] = 'راضی';
$string['biggrin'] = 'لبخند پهن';
$string['blackeye'] = 'چشم کبود';
$string['blush'] = 'خجالت‌زده';
$string['clown'] = 'دلقک';
$string['cool'] = 'باحال';
$string['dead'] = 'خسته';
$string['egg'] = 'تخم‌مرغ';
$string['evil'] = 'بدجنس';
$string['heart'] = 'قلب';
$string['kiss'] = 'بوس';
$string['martin'] = 'مارتین';
$string['mixed'] = 'گیج';
$string['sad'] = 'ناراحت';
$string['shy'] = 'خجالتی';
$string['sleepy'] = 'خواب‌آلود';
$string['smiley'] = 'لبخند';
$string['surprise'] = 'متعجب';
$string['thoughtful'] = 'اندیشناک';
$string['tongueout'] = 'بیرون آمدن زبان';
$string['wideeyes'] = 'چشمان گشاد';
$string['wink'] = 'چشمک';
