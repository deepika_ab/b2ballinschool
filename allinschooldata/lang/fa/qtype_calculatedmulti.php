<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_calculatedmulti', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_calculatedmulti
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'محاسباتی چندگزینه‌ای';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال محاسباتی چندگزینه‌ای';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال محاسباتی چندگزینه‌ای';
$string['pluginname_help'] = 'سؤال‌های محاسباتی چندگزینه‌ای، مانند سؤال‌های چندگزینه‌ای هستند که گزینه‌هایشان می‌تواند شامل فرمول‌هایی باشد که این فرمول‌ها شامل متغیرهایی هستند که بین یک جفت آکولاد باز و بسته قرار گرفته‌اند. این متغیرها در هنگام برگزاری آزمون با مقادیر عددی جایگزین می‌شوند. به عنوان مثال، اگر سؤال «مساحت مستطیلی به طول {l} و عرض {w} چقدر است؟» را داشته باشیم، یکی از گزینه‌ها <span dir="ltr" style="display:inline-block;direction:ltr">{={l}*{w}}</span> است (که در آن * نشان دهندهٔ عملگر ضرب است).';
$string['pluginnamesummary'] = 'سؤال‌های محاسباتی چندگزینه‌ای، مانند سؤال‌های چندگزینه‌ای هستند که گزینه‌های این سؤال‌ها در هنگام برگزاری آزمون و با جایگذاری مقادیر عددی‌ای که به صورت تصادفی از میان مجموعه‌ای از اعداد انتخاب شده‌اند، در یک سری فرمول تعیین می‌شوند.';
