<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_recyclebin', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_recyclebin
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alertdeleted'] = '«{$a->name}» پاک شد.';
$string['alertemptied'] = 'سطل بازیافت خالی شد.';
$string['alertrestored'] = '«{$a->name}» بازیابی شد.';
$string['autohide_desc'] = 'هنگامی که سطل بازیافت خالی است، لینک آن به‌طور خودکار پنهان شود.';
$string['categorybinenable'] = 'فعال‌کردن سطل بازیافت طبقه';
$string['categorybinexpiry_desc'] = 'درس‌هایی که پاک می‌شوند تا چه مدت در سطل بازیافت نگهداری شوند؟';
$string['coursebinenable'] = 'فعال‌کردن سطل بازیافت درس';
$string['coursebinexpiry_desc'] = 'مواردی که پاک می‌شوند تا چه مدت در سطل بازیافت نگهداری شوند؟';
$string['datedeleted'] = 'تاریخ پاک شدن';
$string['deleteall'] = 'حذف کردن همه';
$string['deleteallconfirm'] = 'آیا مطمئن هستید که می‌خواهید تمام محتویات سطل بازیافت را خالی کنید؟';
$string['deleteconfirm'] = 'آیا مطمئن هستید که می‌خواهید مورد انتخاب‌شده را از سطل بازیافت حذف کنید؟';
$string['deleteexpirywarning'] = 'محتویات بعد از {$a} روز به‌طور دائمی حذف خواهند شد.';
$string['eventitemcreated'] = 'مورد ایجاد شد';
$string['eventitemcreated_desc'] = 'مورد با شناسهٔ {$a->objectid} ایجاد شد.';
$string['eventitemdeleted'] = 'مورد حذف شد';
$string['eventitemdeleted_desc'] = 'مورد با شناسهٔ {$a->objectid} حذف شد.';
$string['eventitemrestored'] = 'مورد بازیابی شد';
$string['eventitemrestored_desc'] = 'مورد با شناسهٔ {$a->objectid} بازیابی شد.';
$string['noitemsinbin'] = 'هیچ موردی در سطل بازیافت نیست.';
$string['notenabled'] = 'متاسفیم، ولی سطل بازیافت توسط مدیر سیستم غیرفعال شده است.';
$string['pluginname'] = 'سطل بازیافت';
$string['recyclebin:deleteitems'] = 'پاک‌کردن موارد موجود در سطل بازیافت';
$string['recyclebin:restoreitems'] = 'بازیابی موارد موجود در سطل بازیافت';
$string['recyclebin:viewitems'] = 'مشاهدهٔ موارد موجود در سطل بازیافت';
$string['taskcleanupcategorybin'] = 'پاک‌سازی سطل بازیافت طبقه';
$string['taskcleanupcoursebin'] = 'پاک‌سازی سطل بازیافت درس';
