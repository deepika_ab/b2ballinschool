<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qformat_webct', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qformat_webct
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['errorsdetected'] = '{$a} خطا پیدا شد';
$string['missinganswer'] = '<span dir="ltr" style="display:inline-block;direction:ltr">:ANSWER</span> و <span dir="ltr" style="display:inline-block;direction:ltr">:Lx</span> و <span dir="ltr" style="display:inline-block;direction:ltr">:Rx</span> خیلی کم برای سؤال خط {$a}. حداقل باید ۲ پاسخ ممکن را تعریف کنید.';
$string['missingquestion'] = 'متن سؤال پس از خط {$a} موجود نیست';
$string['pluginname'] = 'قالب برنامهٔ WebCT';
$string['pluginname_help'] = 'قالب WebCT می‌تواند سؤال‌های چند گزینه‌ای و کوتاه جواب ذخیره شده در قالب متنی WebCT را وارد کند.';
$string['questionnametoolong'] = 'نام سؤال خیلی طولانی در خط {$a} (حداکثر ۲۵۵ حرف). این نام کوتاه شده است.';
$string['unknowntype'] = 'نوع سؤال بعد از خط {$a} ناشناخته است';
$string['warningsdetected'] = '{$a} اخطار شناسائی شد';
$string['wronggrade'] = 'نمرهٔ اشتباه (بعد از خط {$a}) :';
