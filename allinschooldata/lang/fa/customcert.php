<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'customcert', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   customcert
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcertpage'] = 'اضافه کردن یک صفحهٔ جدید به گواهینامه';
$string['addelement'] = 'اضافه کردن المان';
$string['code'] = 'کد';
$string['copy'] = 'کپی';
$string['coursetimereq'] = 'دقایق لازم در درس';
$string['coursetimereq_help'] = 'حداقل زمان لازم  (بر حسب دقیقه) که یک شاگرد باید در درس صرف کند تا واجد دریافت گواهینامه شود را وارد کنید.';
$string['createtemplate'] = 'ایجاد قالب جدید';
$string['customcert:addinstance'] = 'اضافه کردن یک گواهینامهٔ سفارشی جدید';
$string['customcert:manage'] = 'مدیریت یک گواهینامهٔ سفارشی';
$string['customcert:view'] = 'مشاهدهٔ یک گواهینامهٔ سفارشی';
$string['deletecertpage'] = 'حذف صفحهٔ گواهینامه';
$string['deleteconfirm'] = 'تایید حذف';
$string['deleteelement'] = 'حذف المان';
$string['deleteelementconfirm'] = 'آیا مطمئن هستید که می‌خواهید این المان را حذف کنید؟';
$string['deletepageconfirm'] = 'آیا مطمئن هستید که می‌خواهید این صفحهٔ گواهینامه را حذف کنید؟';
$string['deletetemplateconfirm'] = 'آیا مطمئن هستید که می‌خواهید این قالب گواهینامه را حذف کنید؟';
$string['description'] = 'توصیف';
$string['editcustomcert'] = 'ویرایش گواهینامهٔ سفارشی';
$string['editelement'] = 'ویرایش المان';
$string['edittemplate'] = 'ویرایش قالب';
$string['elementname'] = 'نام المان';
$string['elementname_help'] = 'این نامی خواهد بود که از آن برای شناسایی این المان در هنگام ویرایش گواهینامهٔ سفارشی استفاده خواهد شد. به‌طور مثال، ممکن است از عکس‌های مختلفی در گواهینامه استفاده کرده باشید و می‌خواهید که در هنگام طراحی گواهینامه بتوانید به‌سرعت تفاوت آنها را تشخیص دهید و بدانید که هر کدام چه هستند. توجه: این نام در PDF نمایش داده نخواهد شد.';
$string['elements'] = 'المان‌ها';
$string['elements_help'] = 'این لیست المان‌هایی است که درگواهینامه نمایش داده خواهند شد.

لطفا توجه داشته باشید که المان‌ها به‌ترتیب از بالا به پایین ترسیم می‌شوند. با استفاده از پیکان‌های کنار هر المان، می‌توان ترتیب المان‌ها را تغییر داد.';
$string['elementwidth'] = 'عرض';
$string['elementwidth_help'] = 'عرض المان را تعیین کنید - مقدار صفر به معنی این است که محدودیتی برای عرض وجود ندارد.';
$string['font'] = 'قلم';
$string['fontcolour'] = 'رنگ';
$string['fontcolour_help'] = 'رنگ قلم مورد استفاده.';
$string['font_help'] = 'قلم مورد استفاده در هنگام تولید این المان.';
$string['fontsize'] = 'اندازه';
$string['fontsize_help'] = 'اندازهٔ قلم بر حسب point.';
$string['getcustomcert'] = 'گواهینامهٔ سفارشی خود را دریافت کنید';
$string['height'] = 'ارتفاع';
$string['height_help'] = 'این مقدار ارتفاع PDF گواهینامه بر حسب میلی‌متر است. به‌عنوان مرجع، ارتفاع یک صفحهٔ A4 برابر با ۲۹۷ میلی‌متر و ارتفاع یک صفحهٔ letter برابر با ۲۷۹ میلی‌متر است.';
$string['invalidcolour'] = 'رنگ نامعتبری تعیین شده است. لطفا یک نام رنگ معتبر HTML، یا یک رنگ هگزادیسمال ۶ رقمی یا ۳ رقمی وارد کنید.';
$string['invalidelementwidth'] = 'لطفا یک عدد مثبت وارد کنید.';
$string['invalidheight'] = 'مقدار ارتفاع باید یک عدد معتبر بزرگتر از صفر باشد.';
$string['invalidmargin'] = 'مقدار حاشیه باید یک عدد معتبر بزرگتر از صفر باشد.';
$string['invalidposition'] = 'لطفا یک عدد مثبت را برای مکان انتخاب کنید {$a}.';
$string['invalidwidth'] = 'مقدار عرض باید یک عدد معتبر بزرگتر از صفر باشد.';
$string['leftmargin'] = 'حاشیه چپ';
$string['leftmargin_help'] = 'مقدار حاشیه چپ در PDF گواهینامه بر حسب میلی‌متر';
$string['load'] = 'بارگیری';
$string['loadtemplate'] = 'بارگیری قالب';
$string['loadtemplatemsg'] = 'آیا مطمئن هستید که می‌خواهید این قالب را بارگیری کنید؟ این کار موجب حذف‌شدن تمام صفحه‌های موجود و المان‌ها از این گواهینامه خواهد شد.';
$string['managetemplates'] = 'مدیریت قالب‌ها';
$string['managetemplatesdesc'] = 'این پیوند شما را به صفحهٔ جدیدی هدایت خواهد کرد که در آن صفحه قادر به مدیریت قالب‌های مورد استفاده توسط نسخه‌های گواهینامهٔ سفارشی در درس‌ها خواهید بود.';
$string['modify'] = 'تغییر';
$string['modulename'] = 'گواهینامه سفارشی';
$string['modulename_help'] = 'با استفاده از این ماژول می‌توان گواهینامه‌های PDF را به طور پویا صادر کرد.';
$string['modulenameplural'] = 'گواهینامه‌های سفارشی';
$string['name'] = 'نام';
$string['noimage'] = 'عکسی موجود نیست';
$string['notemplates'] = 'قالبی موجود نیست';
$string['options'] = 'گزینه‌ها';
$string['page'] = 'صفحهٔ {$a}';
$string['pluginadministration'] = 'مدیریت گواهینامهٔ سفارشی';
$string['pluginname'] = 'گواهینامهٔ سفارشی';
$string['print'] = 'چاپ';
$string['rearrangeelements'] = 'تغییر مکان المان‌ها';
$string['rearrangeelementsheading'] = 'برای تغییر محل قرار گرفتن المان‌ها در گواهینامه، آنها را بگیرید و جابه‌چا کنید.';
$string['receiveddate'] = 'تاریخ دریافت';
$string['refpoint'] = 'محل نقطهٔ مرجع';
$string['refpoint_help'] = 'نقطهٔ مرجع، محلی از المان است که محتصات x و y آن تعیین شده است. این محل با علامت + که در مرکز یا یکی از گوشه‌های المان نمایش داده می‌شود مشخص می‌شود.';
$string['rightmargin'] = 'حاشیه راست';
$string['rightmargin_help'] = 'مقدار حاشیه راست در PDF گواهینامه بر حسب میلی‌متر';
$string['save'] = 'ذخیره';
$string['saveandclose'] = 'ذخیره و بستن';
$string['saveandcontinue'] = 'ذخیره و ادامه';
$string['savechangespreview'] = 'ذخیرهٔ تعییرات و پیش‌نمایش';
$string['savetemplate'] = 'ذخیرهٔ قالب';
$string['search:activity'] = 'گواهینامهٔ سفارشی - اطلاعات فعالیت';
$string['setprotection'] = 'تعیین محدودیت‌ها';
$string['setprotection_help'] = 'کارهایی که می‌خواهید کاربران نتوانند روی این گواهینامه انجام دهند را انتخاب کنید.';
$string['templatename'] = 'نام قالب';
$string['templatenameexists'] = 'این نام قالب قبلا استفاده شده است. لطفا یک نام دیگر وارد کنید.';
$string['topcenter'] = 'مرکز';
$string['topleft'] = 'بالا سمت چپ';
$string['topright'] = 'بالا سمت راست';
$string['type'] = 'نوع';
$string['uploadimage'] = 'بارگذاری تصویر';
$string['uploadimagedesc'] = 'این پیوند شما را به صفحه‌ای هدایت خواهد کرد که در آن صفحه می‌توانید تصاویر دلخواه خود را بارگذاری کنید. تصاویری که از این طریق بارگذاری شده باشند  در کل سایت در دسترس کاربرانی که مجوز ساختن یک گواهینامه سفارشی را دارند قرار خواهد گرفت.';
$string['width'] = 'عرض';
$string['width_help'] = 'این مقدار عرض PDF گواهینامه بر حسب میلی‌متر است. به‌عنوان مرجع، عرض یک صفحهٔ A4 برابر با ۲۱۰ میلی‌متر و عرض یک صفحهٔ letter برابر با ۲۱۶ میلی‌متر است.';
