<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradingform_guide', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   gradingform_guide
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcomment'] = 'اضافه کردن نظر پر کاربرد';
$string['addcriterion'] = 'اضافه کردن معیار';
$string['clicktocopy'] = 'کلیک کنید تا این متن در پنجره بازخورد کپی شود';
$string['clicktoedit'] = 'برای ویرایش کلیک کنید';
$string['clicktoeditname'] = 'برای ویرایش نام معیار کلیک کنید';
$string['comments'] = 'نظرهای پر کاربرد';
$string['commentsempty'] = 'برای ویرایش نظر کلیک کنید';
$string['criteriondelete'] = 'پاک کردن معیار';
$string['criterionempty'] = 'برای ویرایش معیار کلیک کنید';
$string['criterionmovedown'] = 'انتقال به پایین';
$string['criterionmoveup'] = 'انتقال به بالا';
$string['definemarkingguide'] = 'تعریف راهنمای نمره دهی';
$string['description'] = 'توصیف';
$string['descriptionmarkers'] = 'شرح برای نمره دهندگان';
$string['descriptionstudents'] = 'شرح برای شاگردان';
$string['err_maxscorenotnumeric'] = 'مقدار تعیین شده برای حداکثر نمرهٔ معیار باید عددی باشد';
$string['err_nomaxscore'] = 'حداکثر نمرهٔ معیار نمی‌تواند خالی باشد';
$string['guideoptions'] = 'گزینه‌های راهنمای نمره دهی';
$string['guidestatus'] = 'وضعیت فعلی راهنمای نمره دهی';
$string['hidemarkerdesc'] = 'پنهان کردن شرح معیارهای مربوط به نمره دهنده';
$string['hidestudentdesc'] = 'پنهان کردن شرح معیارهای مربوط به شاگرد';
$string['maxscore'] = 'حداکثر نمره';
$string['name'] = 'نام';
$string['pluginname'] = 'راهنمای نمره دهی';
$string['saveguide'] = 'راهنمای نمره دهی ذخیره و آماده استفاده شود';
$string['saveguidedraft'] = 'ذخیره به صورت پیش‌نویس';
$string['score'] = 'نمره';
$string['showmarkerdesc'] = 'نمایش شرح معیارهای مربوط به نمره دهنده';
$string['showmarkspercriterionstudents'] = 'نمایش نمره مربوط به هر معیار به شاگردان';
$string['showstudentdesc'] = 'نمایش شرح معیارهای مربوط به شاگرد';
