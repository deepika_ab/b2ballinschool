<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'survey', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   survey
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actual'] = 'وضعیت فعلی';
$string['actualclass'] = 'وضعیت فعلی کلاس';
$string['actualstudent'] = 'وضعیت فعلی {$a}';
$string['allquestionrequireanswer'] = 'تمام سؤال‌های مطرح شده مورد نیاز هستند و باید پاسخ داده شوند';
$string['allquestions'] = 'همهٔ سؤال‌ها به ترتیب، همهٔ شاگردان';
$string['allscales'] = 'تمام مقیاس‌ها، تمام شاگردان';
$string['alreadysubmitted'] = 'قبلا این ارزیابی را انجام داده‌اید';
$string['analysisof'] = 'تحلیل {$a}';
$string['answers'] = 'پاسخ‌ها';
$string['attls1'] = 'در ارزیابی نظر افراد، من به کیفیت استدلال آن‌ها توجه می‌کنم، نه اینکه چه کسی آن نظر را ارائه می‌کند.';
$string['attls10'] = 'برایم مهم است که وقتی چیزی را تحلیل می‌کنم، تا حد امکان بی‌طرف باشم.';
$string['attls10short'] = 'بی‌طرف ماندن';
$string['attls11'] = 'من سعی می‌کنم که در کنار افراد فکر کنم به جای اینکه در برابر آن‌ها فکر کنم.';
$string['attls11short'] = 'فکر کردن در کنار افراد';
$string['attls12'] = 'من معیارهای خاصی دارم که برای ارزیابی استدلال‌ها از آن‌ها استفاده می‌کنم.';
$string['attls12short'] = 'استفاده از معیار برای ارزیابی';
$string['attls13'] = 'من بیشتر سعی می‌کنم که به جای ارزیابی نظر دیگران، آن را متوجه شوم.';
$string['attls13short'] = 'تلاش برای متوجه شدن';
$string['attls14'] = 'من سعی می‌کنم که به ضعف‌های تفکر دیگران اشاره کنم تا به آن‌ها کمک کنم که استدلالشان را روشن کنند.';
$string['attls14short'] = 'اشاره به نقاط ضعف';
$string['attls15'] = 'من مایلم که در هنگام بحث در مورد مسائل بحث برانگیز خود را جای دیگران بگذارم تا ببینم که چرا آن‌ها اینگونه فکر می‌کنند.';
$string['attls15short'] = 'خود را جای دیگران گذاشتن';
$string['attls16'] = 'ممکن است افراد نحوهٔ تجزیه و تحلیل کردن چیز‌ها توسط من را «دادگاهی کردن آن‌ها» بنامند، زیرا من تمام شواهد و مدارک را با دقت مورد رسیدگی قرار می‌دهم.';
$string['attls16short'] = 'دادگاهی کردن';
$string['attls17'] = 'هنگام حل مسائل، به دلیل و منطق اهمیت بیشتری می‌دهم تا نگرانی‌های شخصی.';
$string['attls17short'] = 'به منطق بیشترین اهمیت را می‌دهم';
$string['attls18'] = 'من از طریق همدلی می‌توانم به درک دیدگاه‌هایی که با دیدگاه من متفاوت هستند برسم.';
$string['attls18short'] = 'بصیرت از طریق همدلی';
$string['attls19'] = 'هنگامی که با افرادی مواجه می‌شوم که نظراتشان برایم غریب است، تلاش بسیاری به خرج می‌دهم که خود را به آن‌ها نزدیک کنم و سعی کنم متوجه شوم که چطور می‌توانند آن نظرات را داشته باشند.';
$string['attls19short'] = 'تلاش برای نزدیک شدن';
$string['attls1short'] = 'توجه به کیفیت استدلال';
$string['attls2'] = 'من دوست دارم رویکرد انتقادی داشته باشم - در مخالفت با نظر افراد استدلال کنم.';
$string['attls20'] = 'من وقت می‌گذارم تا ایراد چیزها را پیدا کنم. مثلا، در یک نقد ادبی دنبال چیزی می‌گردم که به اندازهٔ کافی در موردش بحث نشده باشد.';
$string['attls20short'] = 'چه چیزی اشتباه است؟';
$string['attls2short'] = 'داشتن رویکرد انتقادی';
$string['attls3'] = 'من دوست دارم دلیل نظر افراد را درک کنم. بدانم «از کجا می‌آیند» و چه تجربیاتی باعث شده که طرز فکر فعلی‌شان را داشته باشند.';
$string['attls3short'] = 'افراد از کجا آمده‌اند';
$string['attls4'] = 'مهمترین بخش تحصیل من یادگیری درک افرادی که تفاوت‌های زیادی با من دارند بوده است.';
$string['attls4short'] = 'درک افراد متفاوت';
$string['attls5'] = 'معتقدم که بهترین راه برای من که بتوانم هویت خود را شکل دهم، تعامل داشتن با افراد گوناگون است.';
$string['attls5short'] = 'تعامل داشتن با افراد مختلف';
$string['attls6'] = 'از شندین نظرات افرادی که پیش‌زمینه‌های متفاوتی نسبت به من دارند لذت می‌برم - این نظرات به من کمک می‌کنند که بفهمم چطور چیزهای یکسان می‌توانند به صورت‌های مختلفی دیده شوند.';
$string['attls6short'] = 'لذت بردن از شنیدن نظرات';
$string['attls7'] = 'متوجه شده‌ام که از طریق بحث با کسی که با من مخالف است، می‌توانم موقعیت خودم را مستحکم کنم.';
$string['attls7short'] = 'مستحکم کردن موقعیت با بحث';
$string['attls8'] = 'همیشه علاقه‌مندم که دلیل افراد برای چیزهایی که می‌گویند و به آن‌ها معتقدند را بدانم.';
$string['attls8short'] = 'دانستن دلیل افراد';
$string['attls9'] = 'من اغلب با نویسندهٔ کتاب‌هایی که می‌خوانم بحث دارم. سعی می‌کنم که به لحاظ منطقی کشف کنم که چرا آن‌ها اشتباه می‌کنند.';
$string['attls9short'] = 'بحث با نویسندگان';
$string['attlsintro'] = 'هدف از این پرسشنامه، کمک به ما در سنجش نگرش شما نسبت به تفکر و یادگیری است.

برای این سؤال‌ها پاسخ «درست» یا «اشتباه» وجود ندارد؛ ما فقط علاقه‌مندیم نظر شما را بدانیم. مطمئن باشید که پاسخ‌های شما بسیار محرمانه تلقی خواهند شد و روی ارزیابی شما تاثیری نخواهند داشت.';
$string['attlsm1'] = 'نگرش نسبت به تفکر و یادگیری';
$string['attlsm2'] = 'یادگیری جمعی';
$string['attlsm3'] = 'یادگیری فردی';
$string['attlsmintro'] = 'در بحث و گفتگو ...';
$string['attlsname'] = 'نگرش نسبت به تفکر و یادگیری (نسخهٔ ۲۰ سؤالی)';
$string['cannotfindanswer'] = 'هنوز هیچ پاسخی برای این ارزیابی ارسال نشده است.';
$string['cannotfindquestion'] = 'سؤال وجود ندارد';
$string['cannotfindsurveytmpt'] = 'هیچ نوع ارزیابی‌ای پیدا نشد!';
$string['ciq1'] = 'در چه مقطعی از کلاس بیشترین میزان مشارکت را به عنوان یک یادگیرنده داشتید؟';
$string['ciq1short'] = 'بیشترین میزان مشارکت';
$string['ciq2'] = 'در چه مقطعی از کلاس بیشترین دور افتادگی را به عنوان یک یادگیرنده داشتید؟';
$string['ciq2short'] = 'بیشترین دور افتادگی';
$string['ciq3'] = 'کدام عمل صورت گرفته (از جانب هر کسی) در تالارهای گفتگو را متعهدانه‌تر و مفیدتر یافتید؟';
$string['ciq3short'] = 'لحظهٔ مفید';
$string['ciq4'] = 'کدام عمل صورت گرفته (از جانب هر کسی) در تالارهای گفتگو را گیج‌کننده‌تر و مبهم‌تر یافتید؟';
$string['ciq4short'] = 'لحظهٔ گیج‌کننده';
$string['ciq5'] = 'چه رویدادی شما را بیشتر از همه شگفت‌زده کرد؟';
$string['ciq5short'] = 'لحظهٔ شگفت‌انگیز';
$string['ciqintro'] = 'در حالی که به وقایع اخیر در این کلاس فکر می‌کنید، به سؤال‌های زیر پاسخ دهید.';
$string['ciqname'] = 'حوادث مهم و بحرانی';
$string['clicktocontinue'] = 'برای ادامه کلیک کنید';
$string['clicktocontinuecheck'] = 'برای بررسی و ادامه، اینجا کلیک کنید';
$string['colles1'] = 'یادگیری من روی مسائل مورد علاقه من متمرکز است.';
$string['colles10'] = 'من از سایر شاگردان می‌خواهم که ایده‌هایشان توضیح دهند.';
$string['colles10short'] = 'من درخواست توضیح می‌کنم';
$string['colles11'] = 'سایر شاگردان از من می‌خواهند که ایده‌هایم را توضیح دهم.';
$string['colles11short'] = 'از من خواسته می‌شود که توضیح دهم';
$string['colles12'] = 'سایر شاگردان به ایده‌های من پاسخ می‌دهند.';
$string['colles12short'] = 'شاگردان به من پاسخ می‌دهند';
$string['colles13'] = 'مربی موجب ترغیب من به تفکر می‌شود.';
$string['colles13short'] = 'مربی به تفکر ترغیب می‌کند';
$string['colles14'] = 'مربی من را به مشارکت تشویق می‌کند.';
$string['colles14short'] = 'مربی من را تشویق می‌کند';
$string['colles15'] = 'مربی بحث و گفتگوهای خوبی را طراحی می‌کند.';
$string['colles15short'] = 'مربی بحث و گفتگو را طراحی می‌کند';
$string['colles16'] = 'مربی، خوداندیشی‌های منتقدانه‌ای را طراحی می‌کند.';
$string['colles16short'] = 'مربی خوداندیشی را طراحی می‌کند';
$string['colles17'] = 'سایر شاگردان من را به مشارکت تشویق می‌کنند.';
$string['colles17short'] = 'شاگردان من را تشویق می‌کنند';
$string['colles18'] = 'سایر شاگردان از کمک و همکاری من تعریف می‌کنند.';
$string['colles18short'] = 'شاگردان از من تعریف می‌کنند';
$string['colles19'] = 'سایر شاگردان برای کمک و همکاری من ارزش قائل هستند.';
$string['colles19short'] = 'شاگردان برای من ارزش قائل هستند';
$string['colles1short'] = 'تمرکز روی مسائل جذاب';
$string['colles2'] = 'آنچه که می‌آموزم برای فعالیت حرفه‌ای من مهم است.';
$string['colles20'] = 'سایر شاگردان با تلاش و کشمکش من برای یادگیری همدردی می‌کنند.';
$string['colles20short'] = 'شاگردان همدردی می‌کنند';
$string['colles21'] = 'من پیام‌های سایر شاگردان را به خوبی درک می‌کنم.';
$string['colles21short'] = 'من سایر شاگردان را درک می‌کنم';
$string['colles22'] = 'سایر شاگردان پیام‌های من را به خوبی درک می‌کنند.';
$string['colles22short'] = 'شاگردان من را درک می‌کنند';
$string['colles23'] = 'من پیام‌های مربی را به خوبی درک می‌کنم.';
$string['colles23short'] = 'من مربی را درک می‌کنم';
$string['colles24'] = 'مربی پیام‌های من را به خوبی درک می‌کند.';
$string['colles24short'] = 'مربی من را درک می‌کند';
$string['colles2short'] = 'اهمیت در حرفه';
$string['colles3'] = 'من یاد می‌گیرم که چگونه فعالیت حرفه‌ای خود را بهبود بخشم.';
$string['colles3short'] = 'بهبود فعالیت حرفه‌ای';
$string['colles4'] = 'آنچه که می‌آموزم به خوبی به فعالیت حرفه‌ای من مرتبط می‌شود.';
$string['colles4short'] = 'به فعالیت من مرتبط می‌شود.';
$string['colles5'] = 'من به صورت منتقدانه در مورد نحوهٔ یادگیری‌ام فکر می‌کنم.';
$string['colles5short'] = 'من منتقد یادگیری‌ام هستم';
$string['colles6'] = 'من به صورت منتقدانه در مورد ایده‌هایم فکر می‌کنم.';
$string['colles6short'] = 'من منتقد ایده‌هایم هستم';
$string['colles7'] = 'من به صورت منتقدانه در مورد ایده‌های سایر شاگردان فکر می‌کنم.';
$string['colles7short'] = 'من منتقد سایر شاگردان هستم';
$string['colles8'] = 'من به صورت منتقدانه در مورد ایده‌های موجود در متن‌ها فکر می‌کنم.';
$string['colles8short'] = 'من منتقد متن‌ها هستم';
$string['colles9'] = 'من ایده‌هایم را برای سایر شاگردان توضیح می‌دهم.';
$string['colles9short'] = 'من ایده‌هایم را توضیح می‌دهم';
$string['collesaintro'] = 'هدف این پرسشنامه، کمک به ما در درک این است که ارائه آنلاین این واحد درسی چقدر به یادگیری شما کمک کرده است.

در ۲۴ سؤال زیر، از شما در مورد تجربه‌تان در این واحد درسی سؤال شده است.

برای این سؤال‌ها پاسخ «درست» یا «اشتباه» وجود ندارد؛ ما فقط علاقه‌مندیم نظر شما را بدانیم. مطمئن باشید که پاسخ‌های شما بسیار محرمانه تلقی خواهند شد و روی ارزیابی شما تاثیری نخواهند داشت.

پاسخ‌های دقیق شما در ارتقای سطح ارائه این واحد درسی در آینده به ما کمک خواهند کرد.

سپاسگزاریم.';
$string['collesaname'] = 'ارزیابی محیط یادگیری آنلاین سازاگرا (واقعی)';
$string['collesapintro'] = 'هدف از این پرسشنامه کمک به ما در درک این است که ارائه آنلاین این واحد درسی چقدر به یادگیری شما کمک کرده است.

هر کدام از ۲۴ عبارت زیر از شما می‌خواهند که <b>ترجیح</b> (ایده‌آل) و تجربهٔ <b>واقعی</b> خود در این واحد درسی را با هم مقایسه کنید.

برای این سؤال‌ها پاسخ «صحیح» یا «غلط» وجود ندارد؛ ما فقط علاقه‌مندیم نظر شما را بدانیم. مطمئن باشید که پاسخ‌های شما بسیار محرمانه تلقی خواهند شد و روی ارزیابی شما تاثیری نخواهند داشت.

پاسخ‌های دقیق شما در ارتقای سطح ارائه این واحد درسی در آینده به ما کمک خواهند کرد.

سپاسگزاریم.';
$string['collesapname'] = 'ارزیابی محیط یادگیری آنلاین سازاگرا (ایده‌آل و واقعی)';
$string['collesm1'] = 'ارتباط';
$string['collesm1short'] = 'ارتباط';
$string['collesm2'] = 'نقد چگونگی تفکر';
$string['collesm2short'] = 'نقد چگونگی تفکر';
$string['collesm3'] = 'تعامل';
$string['collesm3short'] = 'تعامل';
$string['collesm4'] = 'تشویق و حمایت مربی';
$string['collesm4short'] = 'تشویق و حمایت مربی';
$string['collesm5'] = 'تشویق و حمایت دوستان';
$string['collesm5short'] = 'تشویق و حمایت دوستان';
$string['collesm6'] = 'تعبیر';
$string['collesm6short'] = 'تعبیر';
$string['collesmintro'] = 'در این واحد درسی آنلاین...';
$string['collespintro'] = 'هدف از این ارزیابی این است که به ما کمک کند تا بدانیم چه چیزهایی در یک تجربهٔ یادگیری آنلاین برای شما ارزش دارد.

در ۲۴ سؤال زیر، از شما در مورد تجربه <b>مرجح</b> (ایده‌آل) تان در این واحد درسی سؤال شده است.

برای این سؤال‌ها پاسخ «درست» یا «اشتباه» وجود ندارد؛ ما فقط علاقه‌مندیم نظر شما را بدانیم. مطمئن باشید که پاسخ‌های شما بسیار محرمانه تلقی خواهند شد و روی ارزیابی شما تاثیری نخواهند داشت.

پاسخ‌های دقیق شما در ارتقای سطح ارائه این واحد درسی در آینده به ما کمک خواهند کرد.

سپاسگزاریم.';
$string['collespname'] = 'ارزیابی محیط یادگیری آنلاین سازاگرا (ایده‌آل)';
$string['customintro'] = 'متن معرفی';
$string['deleteallanswers'] = 'پاک کردن تمام ارزیابی‌های صورت گرفته';
$string['deleteanalysis'] = 'پاک کردن تحلیل پاسخ‌ها';
$string['done'] = 'پر کرده است';
$string['download'] = 'دریافت';
$string['downloadexcel'] = 'دریافت داده‌ها به صورت فایل Excel';
$string['downloadinfo'] = 'می‌توانید داده‌های خام کامل این ارزیابی را به شکلی که برای تجزیه و تحلیل در Excel یا SPSS یا مجموعه‌های دیگر مناسب باشد دریافت کنید.';
$string['downloadresults'] = 'دریافت فایل نتایج';
$string['downloadtext'] = 'دریافت داده‌ها به صورت یک فایل سادهٔ متنی';
$string['editingasurvey'] = 'در حال ویرایش یک ارزیابی';
$string['guestsnotallowed'] = 'مهمان‌ها اجازهٔ پر کردن فرم ارزیابی را ندارند';
$string['howlong'] = 'کامل کردن این فرم چقدر از شما زمان برد؟';
$string['howlongoptions'] = 'کمتر از ۱ دقیقه,۱-۲ دقیقه,۲-۳ دقیقه,۳-۴ دقیقه,۴-۵ دقیقه,۵-۱۰ دقیقه,بیشتر از ۱۰ دقیقه';
$string['ifoundthat'] = 'متوجه شدم که:';
$string['introtext'] = 'متن معرفی';
$string['invalidsurveyid'] = 'شناسهٔ ارزیابی نادرست بود';
$string['invalidtmptid'] = 'شناسهٔ نوع ارزیابی اشتباه است';
$string['ipreferthat'] = 'ترجیح می‌دهم که:';
$string['modulename'] = 'فرم ارزیابی درس';
$string['modulename_help'] = 'ماژول ارزیابی، چند نوع ابزار مطالعاتی برای ارزیابی یادگیری و ایجاد انگیزه برای آن در محیط‌های مجازی فراهم می‌کند. اساتید می‌توانند از این ابزارها برای جمع‌آوری اطلاعاتی که به آن‌ها در آشنا شدن با کلاسشان کمک می‌کند استفاده کنند و نتایج را روی تدریسشان منعکس کنند.

توجه داشته باشید که سؤالات ابزار ارزیابی از پیش تعین‌شده هستند. اساتیدی که می‌خواهند نظرسنجی مورد نظر خودشان را بسازند، می‌توانند از ماژول فعالیت «بازخورد» استفاده کنند.';
$string['modulenameplural'] = 'ارزیابی‌ها';
$string['name'] = 'نام';
$string['newsurveyresponses'] = 'ارزیابی‌های جدیداً صورت گرفته';
$string['nobodyyet'] = 'هنوز کسی این فرم ارزیابی را پر نکرده است';
$string['notdone'] = 'هنوز پر نکرده است';
$string['notes'] = 'تحلیل و یادداشت‌های خصوصی شما';
$string['notyetanswered'] = 'هنوز پاسخ داده نشده است';
$string['othercomments'] = 'هر گونه نظرات دیگری که دارید در این قسمت بنویسید.';
$string['page-mod-survey-x'] = 'هر صفحه‌ای از ماژول ارزیابی درس';
$string['peoplecompleted'] = 'تا این لحظه {$a} نفر این ارزیابی را انجام داده‌اند';
$string['pluginadministration'] = 'مدیریت ارزیابی';
$string['pluginname'] = 'ارزیابی';
$string['preferred'] = 'ایده‌آل';
$string['preferredclass'] = 'ترجیح کلاس';
$string['preferredstudent'] = 'ترجیح {$a}';
$string['question'] = 'سؤال';
$string['questions'] = 'سؤال‌ها';
$string['questionsnotanswered'] = 'برخی از سؤالات چندگزینه‌ای پاسخ داده نشده‌اند.';
$string['report'] = 'گزارش ارزیابی';
$string['responsereports'] = 'گزارشات پاسخ‌ها';
$string['responses'] = 'پاسخ‌ها';
$string['savednotes'] = 'یادداشت‌های شماذخیره شدند';
$string['scaleagree5'] = 'شدیدا مخالف,نسبتا مخالف,بی نظر,نسبتا موافق,شدیدا موافق';
$string['scales'] = 'مقیاس‌ها';
$string['scaletimes5'] = 'تقریبا هیچ‌وقت,به‌ندرت,گاهی اوقات,غالبا,تقریبا همیشه';
$string['search:activity'] = 'نظرسنجی - اطلاعات فعالیت';
$string['seemoredetail'] = 'برای دیدن جزئیات بیشتر اینجا را کلیک کنید';
$string['selectedquestions'] = 'سؤال‌های انتخاب شده از یک مقیاس، همه شاگردان';
$string['summary'] = 'خلاصه';
$string['survey:addinstance'] = 'اضافه‌کردن یک نظرسنجی جدید';
$string['surveycompleted'] = 'شما این ارزیابی را انجام داده‌اید. نمودار زیر خلاصه نتایج شما را در مقایسه با متوسط کلاس نشان می‌دهد.';
$string['survey:download'] = 'دریافت فایل پاسخ‌ها';
$string['surveygraph'] = 'نمودار ارزیابی';
$string['surveyname'] = 'نام ارزیابی';
$string['survey:participate'] = 'پاسخ به فرم ارزیابی';
$string['survey:readresponses'] = 'دیدن پاسخ‌ها';
$string['surveysaved'] = 'ارزیابی ذخیره شد';
$string['surveytype'] = 'نوع ارزیابی';
$string['surveytype_help'] = '۳ نوع فرم ارزیابی برای استفاده وجود دارد:

* بررسی نگرش‌ها نسبت به تفکر و یادگیری (ATTL) - برای سنجش میزان «فراگیر اجتماعی» (کسی که یادگیری را لذت‌بخش‌تر می‌داند و معمولاً خودمانی‌تر و بیشتر اهل همکاری است و بیشتر مایل است که به اندیشه‌های دیگران چیزی اضافه کند) یا «فراگیر انفرادی» (کسی که تمایل بیشتری که به گرفتن موضع‌های انتقادی و جدلی دارد) بودن یک فرد
* ارزیابی حوادث مهم و بحرانی (Critical Incidents)
* ارزیابی محیط یادگیری آنلاین سازاگرا (COLLES) - برای نظارت بر اینکه قابلیت‌های تعاملی شبکهٔ جهانی وب تا چه حد می‌توانند برای درگیر کردن شاگردان در شیوه‌های پویای یادگیری مورد بهره‌برداری قرار گیرند.';
$string['thanksforanswers'] = '{$a}؛ با تشکر از پاسخ دادن به این فرم ارزیابی';
$string['time'] = 'زمان';
$string['viewsurveyresponses'] = 'مشاهدهٔ {$a} ارزیابی صورت گرفته';
