<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshopform_numerrors', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   workshopform_numerrors
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addmoredimensions'] = 'اضافه کردن جا برای {$a} ادعای دیگر';
$string['configgrade0'] = 'کلمه پیش‌فرضی برای توصیف ارزشیابی منفی یک ادعا.';
$string['configgrade1'] = 'کلمه پیش‌فرضی برای توصیف ارزشیابی مثبت یک ادعا.';
$string['dimensiondescription'] = 'توصیف';
$string['dimensiongrade'] = 'نمره';
$string['dimensionnumber'] = 'ادعای {$a}';
$string['dimensionweight'] = 'وزن';
$string['grade0'] = 'کلمه استفاده شده برای اشتباه';
$string['grade0default'] = 'خیر';
$string['grade1'] = 'کلمه استفاده شده برای موفقیت';
$string['grade1default'] = 'بله';
$string['grademapping'] = 'جدول نگاشت نمره';
$string['maperror'] = 'تعداد وزن‌دار اشتباه‌ها کمتر است از یا برابر است با';
$string['mapgrade'] = 'نمره برای تحویل کار';
$string['pluginname'] = 'تعداد اشتباهات';
