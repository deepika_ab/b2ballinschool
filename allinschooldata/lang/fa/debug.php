<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'debug', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   debug
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['authpluginnotfound'] = 'پلاگین شناسایی {$a} پیدا نشد.';
$string['blocknotexist'] = 'بلوک {$a} وجود ندارد';
$string['cannotbenull'] = '{$a} نمی‌تواند تعیین‌نشده باشد!';
$string['cannotdowngrade'] = 'نسخهٔ {$a->plugin} را نمی‌توان از {$a->oldversion} به {$a->newversion} تنزل داد.';
$string['cannotfindadmin'] = 'یک کاربر admin پیدا نشد!';
$string['cannotinitpage'] = 'نمی‌توان صفحه را به‌طور کامل مقداردهی اولیه کرد: شناسهٔ (id: {a->id}) نامعتبر برای {a->name}';
$string['cannotsetuptable'] = 'برپاسازی جدول‌های {$a} موفقیت‌آمیز نبود!';
$string['codingerror'] = 'یک خطای برنامه‌نویسی شناسایی شد. این خطا باید توسط یک برنامه‌نویس برطرف شود: {$a}';
$string['configmoodle'] = 'مودل هنوز پیکربندی نشده است. ابتدا باید config.php را ویرایش کنید.';
$string['erroroccur'] = 'خطایی در حین این فرآیند رخ داد';
$string['invalidarraysize'] = 'اندازهٔ آرایه‌ها در پارامترهای {$a} نادرست است';
$string['invalideventdata'] = 'مقدار فرستاده شده برای eventdata نادرست است: {$a}';
$string['invalidparameter'] = 'مقدار نامعتبر در پارامتر';
$string['invalidresponse'] = 'مقدار نامعتبر در پاسخ';
$string['missingconfigversion'] = 'جدول config شامل نسخه نیست. متاسفانه نمی‌توان ادامه داد.';
$string['modulenotexist'] = 'ماژول {$a} موجود نیست';
$string['morethanonerecordinfetch'] = 'بیش از یک رکورد در <span dir="ltr" style="directory:ltr;display:inline-block">fetch()</span> پیدا شد!';
$string['mustbeoveride'] = 'متد انتزاعی {$a} باید override شود.';
$string['noadminrole'] = 'نقش admin پیدا نشد';
$string['noblocks'] = 'هیچ بلوکی نصب نیست!';
$string['nocate'] = 'هیچ طبقه‌ای وجود ندارد!';
$string['nomodules'] = 'هیچ ماژولی پیدا نشد!!';
$string['nopageclass'] = '{$a} با موفقیت import شد ولی کلاس صفحه در آن تعیین نشده است';
$string['noreports'] = 'هیچ گزارشی قابل دسترسی نیست';
$string['notables'] = 'هیچ جدولی وجود ندارد!';
$string['phpvaroff'] = 'مقدار متغیر «{$a->name}» در کارگزار PHP باید Off باشد - {$a->link}';
$string['phpvaron'] = 'مقدار متغیر «{$a->name}» در کارگزار PHP برابر با On نیست - {$a->link}';
$string['sessionmissing'] = 'آبجکت {$a} در session وجود ندارد';
$string['sqlrelyonobsoletetable'] = 'این SQL مبتنی بر جدول یا جدول‌های منسوخ شده است: {$a}! کد شما باید توسط یک برنامه‌نویس تصحیح شود.';
$string['withoutversion'] = 'فایل version.php اصلی وجود ندارد، قابل خواندن نیست، یا اینکه خراب است.';
$string['xmlizeunavailable'] = 'توابع xmlize موجود نیستند';
