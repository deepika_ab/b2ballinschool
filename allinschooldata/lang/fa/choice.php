<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'choice', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   choice
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addmorechoices'] = 'اضافه کردن انتخاب‌های بیشتر';
$string['allowmultiple'] = 'بشود بیشتر از یک گزینه را انتخاب کرد';
$string['allowupdate'] = 'اجازهٔ به‌روز کردن گزینهٔ انتخابی';
$string['answered'] = 'پاسخ داده';
$string['choice'] = 'انتخاب';
$string['choice:addinstance'] = 'اضافه‌کردن یک انتخاب جدید';
$string['choice:choose'] = 'ثبت یک انتخاب';
$string['choiceclose'] = 'تا';
$string['choice:deleteresponses'] = 'پاک کردن پاسخ‌ها';
$string['choice:downloadresponses'] = 'دریافت پاسخ‌ها به صورت فایل';
$string['choicefull'] = 'همهٔ گزینه‌ها پر شده‌اند و هیچ جای خالی‌ای موجود نیست.';
$string['choicename'] = 'نام انتخاب';
$string['choiceopen'] = 'از';
$string['choiceoptions'] = 'گزینه‌های انتخابی';
$string['choiceoptions_help'] = 'در این قسمت گزینه‌هایی که کاربران باید از بین آن‌ها انتخاب کنند را مشخص می‌کنید.

می‌توانید هر تعداد گزینه که بخواهید داشته باشید. اگر برخی از گزینه‌ها را خالی بگذارید، نمایش داده نخواهند شد. اگر به بیش ار ۸ گزینه نیاز دارید، بر روی دکمهٔ »اضافه کردن ۳ گزینه به فرم» کلیک کنید.';
$string['choice:readresponses'] = 'دیدن پاسخ‌ها';
$string['choicesaved'] = 'انتخاب شما ذخیره شد';
$string['choicetext'] = 'متن انتخاب';
$string['chooseaction'] = 'یک عمل را انتخاب کنید ...';
$string['completionsubmit'] = 'هنگامی که کاربر انتخاب را انجام داد به عنوان کامل نمایش داده شود';
$string['description'] = 'توصیف';
$string['displayhorizontal'] = 'نمایش به صورت افقی';
$string['displaymode'] = 'نحوهٔ نمایش';
$string['displayvertical'] = 'نمایش به صورت عمودی';
$string['expired'] = 'با عرض پوزش، این فعالیت در {$a} بسته شد و دیگر در دسترس نیست';
$string['full'] = '(پر)';
$string['havetologin'] = 'پیش از اینکه بتوانید انتخابتان را ارسال کنید باید وارد سایت شوید';
$string['includeinactive'] = 'شامل پاسخ‌های کاربران غیرفعال/تعلیق‌شده';
$string['limit'] = 'محدودیت';
$string['limitanswers'] = 'محدود کردن تعداد پاسخ‌های مجاز';
$string['limitanswers_help'] = 'این گزینه برای شما امکان محدود کردن تعداد کاربرانی که می‌توانند هر یک از گزینه‌ها را انتخاب کنند فراهم می‌کند. هنگامی که گزینه‌ای به این محدودیت برسد، دیگر کسی امکان انتخاب آن گزینه را نخواهد داشت.

اگر این تنظیم غیر فعال باشد، آنگاه هر تعداد کاربر می‌تواند هر یک از گزینه‌ها را انتخاب کند.';
$string['limitno'] = 'محدودیت {no}';
$string['modulename'] = 'انتخاب';
$string['modulename_help'] = 'ماژول فعالیت انتخاب، به اساتید این امکان را می‌دهد که یک سوال بپرسند و چند پاسخ ممکن برای آن فراهم کنند.

نتایج انتخاب را می‌توان پس از اینکه شاگردان به سوال پاسخ دادند یا پس از یک تاریخ معین منتشر کرد یا اینکه تصمیم به عدم انتشار آنها گرفت. نتایج منتشر شده می‌تواند شامل نام شاگردان باشد یا اینکه به نحوی منتشر شود که هویت شاگردان ناشناس بماند.

یک فعالیت انتخاب می‌تواند در موارد زیر استفاده شود

* یک نظرسنجی سریع برای برانگیختن تفکر در مورد یک موضوع
* برای سنجش سریع میزان متوجه شدن شاگردان
* برای دخیل کردن شاگردان در تصمیم گیری‌ها (مثلا شاگردان بتوانند نظر دهند که درس به کدام مسیر برود)';
$string['modulenameplural'] = 'انتخاب‌ها';
$string['moveselectedusersto'] = 'انتقال کاربران انتخاب شده به...';
$string['mustchooseone'] = 'قبل از اقدام به ذخیره باید پاسخ خود را انتخاب کنید. چیزی ذخیره نشد.';
$string['noguestchoose'] = 'با عرض پوزش، مهمان‌ها مجاز به انتخاب نیستند.';
$string['noresultsviewable'] = 'نتایج در این لحظه قابل دیدن نیستند.';
$string['notanswered'] = 'هنوز پاسخ نداده‌اند';
$string['notenrolledchoose'] = 'با عرض پوزش، تنها کاربران ثبت نام شده مجاز به انتخاب گزینه‌ها هستند.';
$string['notopenyet'] = 'با عرض معذرت، این فعالیت تا قبل از {$a} در دسترس نیست';
$string['numberofuser'] = 'تعداد کاربران';
$string['numberofuserinpercentage'] = 'درصد پاسخ‌ها';
$string['option'] = 'گزینه';
$string['optionno'] = 'گزینه {no}';
$string['options'] = 'گزینه‌ها';
$string['page-mod-choice-x'] = 'هر صفحه‌ای از ماژول انتخاب';
$string['pluginadministration'] = 'مدیریت انتخاب';
$string['pluginname'] = 'انتخاب';
$string['privacy'] = 'پوشیدگی نتایج';
$string['publish'] = 'انتشار نتایج';
$string['publishafteranswer'] = 'نمایش نتایج به شاگردان پس از اینکه پاسخ خود را انتخاب کردند';
$string['publishafterclose'] = 'نمایش نتایج به شاگردان تنها پس از اینکه مهلت انتخاب به پایان رسید';
$string['publishalways'] = 'همیشه نتایج به شاگردان نشان داده شود';
$string['publishanonymous'] = 'نتایج به صورت ناشناس (بدون نمایش نام شاگردان) منتشر شود';
$string['publishnames'] = 'انتشار نتایج به صورت کامل همراه با نمایش نام‌ها و گزینهٔ انتخابی کاربران';
$string['publishnot'] = 'نتایج برای شاگردان منتشر نشود';
$string['removemychoice'] = 'حذف انتخاب من';
$string['removeresponses'] = 'حذف همهٔ پاسخ‌ها';
$string['responses'] = 'پاسخ‌ها';
$string['responsesresultgraphheader'] = 'نمایش نمودار';
$string['responsesto'] = 'پاسخ‌های داده شده به {$a}';
$string['results'] = 'نتایج';
$string['savemychoice'] = 'ذخیرهٔ انتخاب من';
$string['search:activity'] = 'انتخاب - اطلاعات فعالیت';
$string['showpreview'] = 'نشان‌دادن پیش‌نمایش';
$string['showpreview_help'] = 'شاگردان بتوانند گزینه‌های موجود را از پیش از اینکه فعالیت «انتخاب» برای دریافت پاسخ باز شود ببینند.';
$string['showunanswered'] = 'نمایش ستون برای کاربرانی که پاسخ نداده‌اند';
$string['spaceleft'] = 'فضای در دسترس';
$string['spacesleft'] = 'فضاهای در دسترس';
$string['taken'] = 'دریافت شده';
$string['userchoosethisoption'] = 'کاربرانی که این گزینه را انتخاب کرده‌اند';
$string['viewallresponses'] = 'مشاهدهٔ {$a} پاسخ';
$string['withselected'] = 'با موارد انتخاب شده';
$string['yourselection'] = 'انتخاب شما';
