<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshopallocation_random', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   workshopallocation_random
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addselfassessment'] = 'اضافه کردن خود-ارزشیابی';
$string['allocationaddeddetail'] = 'ارزشیابی جدیدی که باید انجام شود: <strong>{$a->reviewername}</strong> بازبین <strong>{$a->authorname}</strong> است';
$string['allocationdeallocategraded'] = 'لغو تخصیص ارزشیابی‌ای که نمره داده شده است ممکن نیست: بازبین: <strong>{$a->reviewername}</strong>، تحویل دهندهٔ کار مورد بازبینی: <strong>{$a->authorname}</strong>';
$string['allocationsettings'] = 'تنظیمات اختصاص';
$string['assessmentdeleteddetail'] = 'وظیفهٔ ارزشیابی برداشته شد: <strong>{$a->reviewername}</strong> دیگر بازبین <strong>{$a->authorname}</strong> نیست';
$string['assesswosubmission'] = 'شرکت کنندگان بدون اینکه چیزی تحویل داده باشند نیز می‌توانند به ارزشیابی بپردازند';
$string['confignumofreviews'] = 'تعداد پیش‌فرض کارهایی که به صورت تصادفی برای بازبینی اختصاص داده می‌شوند';
$string['noallocationtoadd'] = 'تخصیصی برای صورت گرفتن وجود ندارد';
$string['numofdeallocatedassessment'] = 'لغو تخصیص {$a} بازبینی';
$string['numofrandomlyallocatedsubmissions'] = 'تخصیص تصادفی {$a} کار تحویل داده شده';
$string['numofreviews'] = 'تعداد دفعات بازبینی';
$string['numofselfallocatedsubmissions'] = 'تخصیص {$a} مورد کار ارائه شده توسط افراد به خودشان';
$string['numperauthor'] = 'بار به ازای هر کار تحویل شده';
$string['numperreviewer'] = 'بار به ازای هر بازبین';
$string['pluginname'] = 'تخصیص تصادفی';
$string['randomallocationdone'] = 'تخصیص تصادفی انجام شد';
$string['removecurrentallocations'] = 'حذف تخصیص‌های فعلی';
$string['stats'] = 'آمار تخصیص فعلی';
