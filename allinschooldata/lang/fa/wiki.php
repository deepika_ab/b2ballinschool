<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'wiki', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   wiki
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcomment'] = 'اضافه‌کردن نظر';
$string['addedbegins'] = 'شروع قسمت اضافه شده';
$string['addedends'] = 'پایان قسمت اضافه شده';
$string['admin'] = 'مدیریت';
$string['adminmenu'] = 'منوی مدیر';
$string['attachmentattach'] = 'اضافه کردن به صورت ضمیمه';
$string['attachmentimage'] = 'اضافه کردن به صورت تصویر';
$string['attachmentlink'] = 'اضافه کردن به صورت پیوند';
$string['attachments'] = 'ضمیمه‌ها';
$string['backcomments'] = 'بازگشت به نظرات';
$string['backhistory'] = 'بازگشت به سوابق';
$string['backoldversion'] = 'بازگشت به نسخهٔ قدیمی';
$string['backpage'] = 'بازگشت به صفحه';
$string['backtomapmenu'] = 'بازگشت به منوی معماری';
$string['cannotmanagefiles'] = 'شما مجوز مدیریت فایل‌های ویکی را ندارید.';
$string['cannotviewfiles'] = 'شما مجوز مشاهدهٔ فایل‌های ویکی را ندارید.';
$string['changerate'] = 'آیا مایلید که تغییرش دهید؟';
$string['comments'] = 'نظرات';
$string['commentscount'] = 'نظرات ({$a})';
$string['comparesel'] = 'مقایسهٔ نسخه‌های انتخاب شده';
$string['comparewith'] = 'مقایسهٔ نسخهٔ {$a->old} با نسخهٔ {$a->new}';
$string['contributions'] = 'مشارکت‌ها';
$string['contributions_help'] = 'لیست صفحه‌هایی که ویرایش کرده‌اید.';
$string['createcomment'] = 'ثبت نظر';
$string['createddate'] = 'ایجاد: {$a->date} توسط {$a->username}';
$string['createpage'] = 'ایجاد صفحه';
$string['creating'] = 'ایجاد صفحهٔ ویکی';
$string['defaultformat'] = 'قالب پیش‌فرض';
$string['defaultformat_help'] = 'این تنظیم، قالب پیش‌فرضی که هنگام ویرایش صفحه‌های ویکی استفاده می‌شود را تعیین می‌کند.

* HTML - ویرایشگر HTML موجود است
* Creole - یک زبان رایج نشانه‌گذاری ویکی که یک نوار ابزار ویرایش کوچک برایش موجود است
* NWiki - یک زبان نشانه‌گذاری مدیاویکی مانند که در ماژول کمکی Nwiki استفاده شده است';
$string['deleteallpages'] = 'حذف تمام صفحه‌های ویکی';
$string['deletecomment'] = 'پاک کردن نظر';
$string['deletedbegins'] = 'شروع قسمت حذف شده';
$string['deletedends'] = 'پایان قسمت حذف شده';
$string['deleteupload'] = 'حذف';
$string['deleteversions'] = 'پاک کردن نسخه‌های صفحه';
$string['diff'] = 'تفاوت';
$string['diff_help'] = 'نسخه‌های انتخاب شده از صفحه، به منظور پیدا کردن تفاوت‌ها، می‌توانند با هم مقایسه شوند.';
$string['edit'] = 'ویرایش';
$string['editblocks'] = 'شروع ویرایش بلوک‌ها';
$string['editcomment'] = 'ویرایش نظر';
$string['editfiles'] = 'ویرایش فایل‌های ویکی';
$string['editing'] = 'ویرایش صفحهٔ ویکی';
$string['editingcomment'] = 'ویرایش نظر';
$string['editingpage'] = 'ویرایش این صفحه: «{$a}»';
$string['editsection'] = 'ویرایش';
$string['eventhistoryviewed'] = 'تاریخچهٔ ویکی مشاهده شد';
$string['eventpagecreated'] = 'صفحهٔ ویکی ایجاد شد';
$string['eventpagedeleted'] = 'صفحهٔ ویکی حذف شد';
$string['eventpageupdated'] = 'صفحهٔ ویکی به‌روز شد';
$string['eventpageviewed'] = 'صفحهٔ ویکی مشاهده شد';
$string['filenotuploadederror'] = 'فایل «{$a}» نتوانست با موفقیت ارسال شود.';
$string['files'] = 'فایل‌ها';
$string['filtername'] = 'پیونددهی خودکار ویکی';
$string['firstpagetitle'] = 'نام صفحهٔ اول';
$string['firstpagetitle_help'] = 'عنوان اولین صفحهٔ ویکی.';
$string['forceformat'] = 'تحمیل قالب';
$string['forceformat_help'] = 'اگر قالب تحمیل شده باشد (این گزینه انتخاب شده باشد)، در هنگام ویرایش صفحه‌های ویکی، امکان انتخاب قالب وجود نخواهد داشت.';
$string['format'] = 'قالب';
$string['formatcreole'] = 'قالب Creole';
$string['formatcreole_help'] = 'Creole یک زبان نشانه‌گذاری ویکی رایج است که یک نوار ابزار برای درج نشانه‌های مناسب است.

برای ساختن یک صفحهٔ جدید، نام صفحهٔ جدید را در میان دو جفت کروشه (مانند [[صفحهٔ ۲]]) قرار دهید.';
$string['format_help'] = 'قالب مورد استفاده در هنگام ویرایش صفحه‌های ویکی

* HTML - ویرایشگر HTML موجود است
* Creole - یک زبان رایج نشانه‌گذاری ویکی که یک نوار ابزار ویرایش کوچک برایش موجود است
* NWiki - یک زبان نشانه‌گذاری مدیاویکی مانند که در ماژول کمکی Nwiki استفاده شده است';
$string['formathtml'] = 'قالب HTML';
$string['formathtml_help'] = 'می‌توان از ویرایشگر HTML برای قالب دهی محتوا استفاده کرد.

برای ساختن یک صفحهٔ جدید، نام صفحهٔ جدید را در میان دو جفت کروشه (مانند [[صفحهٔ ۲]]) قرار دهید.';
$string['formatnwiki'] = 'قالب NWiki';
$string['formatnwiki_help'] = 'Nwiki یک زبان نشانه‌گذاری ویکی مشابه Mediawiki است که در ماژول Nwiki استفاده می‌شود.

برای ساختن یک صفحهٔ جدید، نام صفحهٔ جدید را در میان دو جفت کروشه (مانند [[صفحهٔ ۲]]) قرار دهید.';
$string['history'] = 'سوابق';
$string['history_help'] = 'صفحهٔ سوابق یک لیست از پیوندهایی به نسخه‌های قبلی صفحه را نمایش می‌دهد.';
$string['html'] = 'HTML';
$string['incorrectdeleteversions'] = 'نسخه‌های صفحه‌ای که برای پاک شدن فراهم شده‌اند نادرست است.';
$string['individualpagedoesnotexist'] = 'صفحهٔ ویکی شخصی وجود ندارد';
$string['insertcomment'] = 'درج نظر';
$string['insertimage'] = 'درج یک تصویر...';
$string['insertimage_help'] = 'این لیست باز شونده یک تصویر در ویرایشگر ویکی درج خواهد کرد. اگر نیاز به درج تصاویر بیشتری در ویکی دارید، لطفاً از زبانهٔ «فایل‌ها» استفاده کنید.';
$string['invalidlock'] = 'این صفحه توسط کاربر دیگری قفل شده اشت.';
$string['invalidparameters'] = 'پارامترهای داده شده نامعتبر هستند.';
$string['invalidsection'] = 'Invalid section.';
$string['invalidsesskey'] = 'Sesskey داده شده معتبر نیست. لطفاً داده‌ها را دوباره ارسال کنید.';
$string['javascriptdisabledlocks'] = 'جاوا اسکریپت در مرورگر شما غیر فعال است و در نتیجه قفل‌ها کار نمی‌کنند. تغییراتی که انجام می‌دهید ممکن است به درستی ذخیره نشود.';
$string['links'] = 'پیوندها';
$string['listall'] = 'نمایش همه';
$string['listorphan'] = 'فقط نمایش صفحه‌های دور افتاده';
$string['lockingajaxtimeout'] = 'ویرایش زمان تازه‌سازی قفل کردن صفحه';
$string['lockingtimeout'] = 'مدت زمان فعال بودن قفل';
$string['map'] = 'معماری صفحه‌ها';
$string['mapmenu'] = 'منوی معماری';
$string['migrationfinished'] = 'مهاجرت با موفقیت به پایان رسید';
$string['migrationfinishednowikis'] = 'مهاجرت به پایان رسید. هیچ ویکی‌ای منتقل نشد.';
$string['missingpages'] = 'صفحه‌های بدون محتوا';
$string['modified'] = 'آخرین تغییر';
$string['modulename'] = 'ویکی';
$string['modulename_help'] = 'با استفاده از ماژول فعالیت ویکی، شرکت‌کنندگان می‌توانند با مشارکت یکدیگر مجموعه‌ای از صفحه‌های وب را بسازند و ویرایش کنند. یک ویکی می‌تواند مشارکتی باشد و همه بتوانند در آن مشارکت کنند و آن را ویرایش کنند، یا اینکه فردی باشد و هر کس ویکی مخصوص به خود که تنها توسط خودش قابل ویرایش است را داشته باشد.

تاریخچه‌ای از نسخه‌های قدیمی هر یک از صفحه‌های ویکی (همراه با ویرایش‌هایی که هر کدام از شرکت‌کنندگان انجام داده‌اند) نگهداری می‌شود.

ویکی‌ها کاربردهای متعددی دارند، مانند:

* برای جزوه‌های درسی گروهی یا راهنماهای مطالعه
* برای اعضای هیئت علمی تا با مشارکت هم طرحی از یک کار یا دستورکار جلسه‌ای را برنامه‌ریزی کنند
* برای شاگردان تا با مشارکت هم یک کتاب آنلاین بنویسند، محتوای موضوعی که توسط مربی تعیین‌شده‌است را بسازند
* برای اینکه با مشارکت هم قصه‌ای بنویسند یا شعری بسرایند به این صورت که هر شرکت‌کننده یک خط یا یک بیت را بنویسد
* به‌عنوان یک ژورنال شخصی برای یادداشت‌های امتحان (با استفاده از ویکی شخصی)';
$string['modulenameplural'] = 'ویکی‌ها';
$string['navigation'] = 'راهبری';
$string['navigationfrom'] = 'از صفحه‌های زیر می‌توان به این صفحه رسید';
$string['navigationfrom_help'] = 'صفحه‌هایی از ویکی که به این صفحه پیوند داده‌اند';
$string['navigationto'] = 'از این صفحه می‌توان به صفحه‌های زیر رفت';
$string['navigationto_help'] = 'پیوند به سایر صفحه‌ها';
$string['newpage'] = 'جدید';
$string['newpagetitle'] = 'عنوان صفحهٔ جدید';
$string['noattachments'] = '<strong>فایلی ضمیمه نشده است</strong>';
$string['nocomments'] = 'نظری ارائه نشده است';
$string['nocontent'] = 'محتوایی برای این صفحه وجود ندارد';
$string['nocontribs'] = 'شما هیچ مشارکتی در این ویکی انجام نداده‌اید';
$string['nocreatepermission'] = 'مجوز ایجاد صفحه لازم است';
$string['noeditcommentpermission'] = 'مجوز ویرایش نظر لازم است';
$string['noeditpermission'] = 'مجوز ویرایش صفحه لازم است';
$string['nofrompages'] = 'هیچ پیوندی به این صفحه وجود ندارد';
$string['nohistory'] = 'این صفحه هیچ سابقه‌ای ندارد';
$string['nomanagecommentpermission'] = 'مجوز مدیریت نظرات مورد نیاز است';
$string['nomanagewikipermission'] = 'مجوز مدیریت ویکی لازم است';
$string['noorphanedpages'] = 'هیج صفحهٔ دور افتاده‌ای وجود ندارد';
$string['nooverridelockpermission'] = 'مجوز باطل کردن قفل لازم است';
$string['norated'] = 'این صفحه هنوز ارزیابی نشده است. اولین نفر باشید!';
$string['norating'] = 'بدون امتیاز';
$string['nosearchresults'] = 'بدون نتیجه';
$string['noteditblocks'] = 'ویرایش بلوک‌ها را غیر فعال کنید';
$string['notingroup'] = 'بدون گروه';
$string['notmigrated'] = 'این ویکی هنوز منتقل نشده است. لطفاً با مدیر سایت تماس بگیرید.';
$string['notopages'] = 'این صفحه به صفحهٔ دیگری پیوند نمی‌دهد';
$string['noupdatedpages'] = 'صفحهٔ به‌روز شده‌ای وجود ندارد';
$string['noviewcommentpermission'] = 'مجوز دیدن نظرات لازم است';
$string['noviewpagepermission'] = 'مجوز دیدن صفحه لازم است';
$string['oldversion'] = 'نسخهٔ قدیمی';
$string['orphaned'] = 'صفحه‌های دور افتاده';
$string['orphaned_help'] = 'لیست صفحه‌هایی که از هیچ صفحهٔ دیگری به آن‌ها پیوند داده نشده است.';
$string['overridelocks'] = 'ابطال قفل‌ها';
$string['overridinglocks'] = 'باطل کردن قفل‌ها...';
$string['pageexists'] = 'این صفحه در حال حاضر وجود دارد. در حال تغییر مسیر به صفحهٔ موجود.';
$string['pageindex'] = 'فهرست صفحه‌ها';
$string['pageindex_help'] = 'درخت صفحه‌های این ویکی';
$string['pageislocked'] = 'شخصی در همین لحظه در حال ویرایش این صفحه است. ویرایش این صفحه را چند دقیقهٔ دیگر امتحان کنید.';
$string['pagelist'] = 'لیست صفحه‌ها';
$string['pagelist_help'] = 'لیست صفحه‌ها – دسته‌بندی شده به ترتیب حروف الفبا';
$string['page-mod-wiki-comments'] = 'صفحهٔ نظرات ماژول ویکی';
$string['page-mod-wiki-history'] = 'صفحهٔ تاریخچهٔ ویکی';
$string['page-mod-wiki-map'] = 'صفحهٔ معماری صفحه‌های ویکی';
$string['page-mod-wiki-view'] = 'صفحهٔ اصلی ماژول ویکی';
$string['page-mod-wiki-x'] = 'هر صفحه‌ای از ماژول ویکی';
$string['peerreview'] = 'بازنگری همشاگردی‌ها';
$string['pluginadministration'] = 'مدیریت ویکی';
$string['pluginname'] = 'ویکی';
$string['prettyprint'] = 'نسخهٔ مناسب چاپ';
$string['previewwarning'] = 'این یک پیش‌نمایش است. تغییرات هنوز ذخیره نشده‌اند.';
$string['rated'] = 'شما به این صفحه امتیاز {$a} را داده‌اید';
$string['rating'] = 'امتیاز';
$string['ratingmode'] = 'شیوهٔ ارزیابی';
$string['removeallwikitags'] = 'حذف همهٔ برچسب‌ها در ویکی‌ها';
$string['removepages'] = 'حذف صفحه‌ها';
$string['reparsetimeout'] = 'موعد پیش‌فرض تجزیه دوباره';
$string['repeatedsection'] = 'خطا در ویکی: نام قسمت نمی‌تواند تکرار شود «{$a}»';
$string['restore'] = 'بازیابی';
$string['restoreconfirm'] = 'آیا مطمئنید که می‌خواهید نسخهٔ شمارهٔ {$a} را بازیابی کنید؟';
$string['restoreerror'] = 'بازیابی نسخهٔ شمارهٔ {$a} امکان‌پذیر نبود';
$string['restorethis'] = 'بازیابی این نسخه';
$string['restoreversion'] = 'بازیابی نسخهٔ قدیمی';
$string['restoring'] = 'در حال بازیابی نسخهٔ شمارهٔ {$a}';
$string['return'] = 'بازگشت';
$string['save'] = 'ذخیره';
$string['saving'] = 'ذخیره‌سازی صفحهٔ ویکی';
$string['savingerror'] = 'خطا به هنگام ذخیره‌سازی';
$string['search:activity'] = 'ویکی - اطلاعات فعالیت';
$string['search:collaborative_page'] = 'ویکی - صفحه‌های مشارکتی';
$string['searchcontent'] = 'جستجو در محتوای صفحه';
$string['searchresult'] = 'نتایج جستجو:';
$string['searchwikis'] = 'جستجو در ویکی‌ها';
$string['special'] = 'ویژه';
$string['tableofcontents'] = 'فهرست';
$string['tagarea_wiki_pages'] = 'صفحه‌های ویکی';
$string['tagsdeleted'] = 'برچسب‌های ویکی حذف شدند';
$string['teacherrating'] = 'ارزیابی استاد';
$string['timesrating'] = 'این صفحه {$a->c} بار مورد ارزیابی قرار گرفت که میانگین ارزیابیهای صورت گرفته برابر است با: {$a->s}';
$string['updatedpages'] = 'صفحه‌های تغییر یافته';
$string['updatedpages_help'] = 'صفحه‌های ویکی اخیراً به‌روز شده';
$string['updatedwikipages'] = 'صفحه‌های تغییر یافتهٔ ویکی';
$string['upload'] = 'ارسال یا حذف فایل';
$string['uploadactions'] = 'عملیات';
$string['uploadfiletitle'] = 'ضمیمه‌ها';
$string['uploadname'] = 'نام فایل';
$string['uploadtitle'] = 'فایل‌های ضمیمه';
$string['version'] = 'نسخه';
$string['versionerror'] = 'شناسهٔ نسخه وجود ندارد';
$string['versionnum'] = 'نسخهٔ شمارهٔ {$a}';
$string['view'] = 'مشاهده';
$string['viewallhistory'] = 'مشاهدهٔ تمام سوابق';
$string['viewcurrent'] = 'نسخهٔ فعلی';
$string['viewperpage'] = 'نمایش {$a} نسخه در هر صفحه';
$string['viewversion'] = 'در حال مشاهدهٔ نسخهٔ شمارهٔ {$a}';
$string['wiki'] = 'ویکی';
$string['wiki:addinstance'] = 'اضافه‌کردن یک ویکی جدید';
$string['wikiattachments'] = 'ضمیمه‌های ویکی';
$string['wikiboldtext'] = 'متن ذخیم';
$string['wiki:createpage'] = 'ساختن صفحه‌های جدید در ویکی';
$string['wiki:editcomment'] = 'اضافه کردن نظر به صفحه‌ها';
$string['wiki:editpage'] = 'ذخیرهٔ صفحه‌ها در ویکی';
$string['wikiexternalurl'] = 'آدرس اینترنتی خارجی';
$string['wikifiles'] = 'فایل‌های ویکی';
$string['wikifiletable'] = 'لیست فایل‌های ارسال شده';
$string['wikiheader'] = 'عنوان سطح {$a}';
$string['wikihr'] = 'خط صاف افقی';
$string['wikiimage'] = 'تصویر';
$string['wikiinternalurl'] = 'پیوند داخلی';
$string['wikiintro'] = 'توصیف ویکی';
$string['wikiitalictext'] = 'متن مورب';
$string['wikilockingsettings'] = 'موعدهای تجزیهٔ دوباره و آزاد شدن قفل';
$string['wiki:managecomment'] = 'مدیریت نظرات در ویکی';
$string['wiki:managefiles'] = 'مدیریت فایل‌های ویکی';
$string['wiki:managewiki'] = 'مدیریت تنظیمات ویکی';
$string['wikimode'] = 'نوع ویکی';
$string['wikimodecollaborative'] = 'ویکی مشارکتی';
$string['wikimode_help'] = 'نوع ویکی تعیین می‌کند که آیا همه می‌توانند ویکی را ویرایش کنند (یک ویکی گروهی) یا اینکه هر کس ویکی خودش که تنها قابل ویرایش توسط خودش باشد (یک ویکی شخصی) را داشته باشد.';
$string['wikimodeindividual'] = 'ویکی فردی';
$string['wikiname'] = 'نام ویکی';
$string['wikinowikitext'] = 'متن غیر ویکی';
$string['wikiorderedlist'] = 'لیست ترتیبی';
$string['wiki:overridelock'] = 'ابطال قفل‌های ویکی';
$string['wikipages'] = 'صفحه‌های ویکی';
$string['wikisettings'] = 'تنظیمات ویکی';
$string['wikiunorderedlist'] = 'لیست گلوله‌ای';
$string['wiki:viewcomment'] = 'مشاهدهٔ نظرات مربوط به صفحه';
$string['wiki:viewpage'] = 'مشاهدهٔ صفحه‌های ویکی';
$string['wrongversionlock'] = 'هنگامی که شما در حال ویرایش این صفحه بودید، کاربر دیگری آن را ویرایش کرد و محتوایش را تغییر داد.';
$string['wrongversionsave'] = 'هنگامی که شما در حال ویرایش این صفحه بودید، کاربر دیگری یک نسخهٔ جدید از این صفحه ایجاد کرد. در نتیجه شما تغییرات آن کاربر را بازنویسی کردید. سوابق صفحه را بررسی کنید.';
