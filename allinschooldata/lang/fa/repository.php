<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'repository', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   repository
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activaterep'] = 'انباره‌های فعال';
$string['activerepository'] = 'پلاگین‌های انباره موجود';
$string['activitybackup'] = 'پشتیبان فعالیت';
$string['add'] = 'اضافه کردن';
$string['addfile'] = 'اضافه کردن...';
$string['addfiletext'] = 'اضافه‌کردن فایل';
$string['addplugin'] = 'اضافه کردن یک پلاگین انباره';
$string['allowexternallinks'] = 'مجاز بودن پیوندهای خارجی';
$string['areacategoryintro'] = 'توصیف طبقه';
$string['areacourseintro'] = 'توصیف درس';
$string['areacourseoverviewfiles'] = 'فایل‌های توصیف درس';
$string['areamainfile'] = 'فایل اصلی';
$string['arearoot'] = 'سیستم';
$string['areauserpersonal'] = 'فایل‌های خصوصی';
$string['attachedfiles'] = 'فایل‌های پیوست';
$string['attachment'] = 'فایل پیوست';
$string['author'] = 'مؤلف';
$string['automatedbackup'] = 'پشتیبان‌های خودکار';
$string['back'] = '&raquo; بازگشت';
$string['cachecleared'] = 'فایل‌های cache شده حذف شدند';
$string['cacheexpire'] = 'انقضای cache';
$string['cannotdelete'] = 'نمی‌توان این فایل را پاک کرد.';
$string['chooselicense'] = 'انتخاب اجازه‌نامه';
$string['commonrepositorysettings'] = 'تنظیمات عمومی انباره‌ها';
$string['configallowexternallinks'] = 'این گزینه تمام کاربران را قادر می‌سازد تا انتخاب کنند که فایل‌های رسانه‌ای خارجی در مودل کپی شوند یا خیر. اگر این گزینه غیر فعال باشد، فایل رسانه‌ای همیشه در مودل کپی خواهد شد (این کار معمولا به لحاظ یکپارچگی کلی داده‌ها و امنیت بهتر است). اگر فعال باشد، آنگاه کاربران می‌توانند هر بار که یک فایل رسانه‌ای را به متنی اضافه می‌کنند این را انتخاب کنند.';
$string['configcacheexpire'] = 'مدت زمان نگهداری لیست‌های فایل‌ها (بر حسب ثانیه) به صورت محلی در cache به هنگام مرور انباره‌های خارجی.';
$string['configgetfiletimeout'] = 'مهلت دریافت‌کردن یک فایل از بیرون برحسب ثانیه.';
$string['configsyncfiletimeout'] = 'مهلت همگام‌سازی اندازهٔ فایل بیرونی برحسب ثانیه.';
$string['configsyncimagetimeout'] = 'مهلت دریافت‌کردن یک فایل تصویری از یک انبارهٔ بیرونی در حین همگام‌سازی بر حسب ثانیه.';
$string['confirmdelete'] = 'آیا واقعا می‌خواهید این انباره را پاک کنید: «{$a}»؟';
$string['confirmdeletefile'] = 'آیا مطمئنید که می‌خواهید این فایل را حذف کنید؟';
$string['confirmdeletefilewithhref'] = 'آیا مطمئن هستید که می‌خواهید این فایل را پاک کنید؟ {$a} فایل مستعار/میان‌بُر از این فایل به‌عنوان مرجع خود استفاده می‌کنند. اگر ادامه دهید، آنگاه آن فایل‌های مستعار به نسخه‌های واقعی تبدیل خواهند شد.';
$string['confirmdeletefolder'] = 'آیا مطمئنید که می‌خواهید این پوشه را پاک کنید؟ تمام فایل‌ها و پوشه‌های داخل این پوشه پاک خواهد شد.';
$string['confirmrenamefile'] = 'آیا مطمئن هستید که می‌خواهید این فایل را منتقل کنید یا تغییر نام دهید؟ {$a} فایل مستعار/میان‌بُر از این فایل به‌عنوان مرجع خود استفاده می‌کنند. اگر ادامه دهید، آنگاه آن فایل‌های مستعار به نسخه‌های واقعی تبدیل خواهند شد.';
$string['confirmrenamefolder'] = 'آیا مطمئن هستید که می‌خواهید این پوشه را منتقل کنید یا تغییر نام دهید؟ هر فایل مستعار یا میان‌بُری که از فایل‌های داخل این پوشه به‌عنوان مرجع خود استفاده کند به یک نسخهٔ واقعی از مرجع خود تبدیل خواهد شد.';
$string['createinstance'] = 'ساختن یک نمونهٔ جدید انباره';
$string['createrepository'] = 'ساختن یک نسخه از انباره';
$string['createxxinstance'] = 'ایجاد یک نسخه جدید از «{$a}»';
$string['datecreated'] = 'ایجاد';
$string['deleted'] = 'مخزن حذف شد';
$string['deleterepository'] = 'حذف این مخزن';
$string['disabled'] = 'غیر فعال';
$string['displaydetails'] = 'نمایش پوشه با جزئیات فایل‌ها';
$string['displayicons'] = 'نمایش پوشه با آیکن فایل‌ها';
$string['displaytree'] = 'نمایش پوشه به‌صورت درختی';
$string['downloadfolder'] = 'دریافت همه';
$string['editrepositoryinstance'] = 'ویرایش مخزن';
$string['enablecourseinstances'] = 'اجازه دادن به کاربران برای اضافه کردن یک نمونهٔ جدید از انباره به درس';
$string['enableuserinstances'] = 'اجازه دادن به کاربران برای اضافه کردن یک نمونهٔ جدید از انباره به زمینهٔ کاربر';
$string['entername'] = 'لطفاً نام پوشه را وارد کنید';
$string['enternewname'] = 'لطفا نام فایل جدید را وارد کنید';
$string['error'] = 'یک خطای نامشخص رخ داد!';
$string['errordoublereference'] = 'نمی‌توان فایل را به فایل مستعار/میان‌بُر تبدیل کرد زیرا فایل‌های میان‌بُر دیگری وجود دارند که از این فایل به‌عنوان مرجع خود استفاده می‌کنند.';
$string['filepicker'] = 'انتخاب فایل';
$string['getfile'] = 'انتخاب این فایل';
$string['getfiletimeout'] = 'مهلت دریافت فایل';
$string['iconview'] = 'نمایش به صورت تصویری';
$string['instancedeleted'] = 'نمونه حذف شد';
$string['instancesforcourses'] = '{$a} نمونهٔ عمومی سطح درس';
$string['instancesforsite'] = '{$a} نمونهٔ عمومی سطح سایت';
$string['instancesforusers'] = '{$a} نمونهٔ خصوصی کاربر';
$string['invalidfiletype'] = 'فایل از نوع {$a} نمی‌تواند پذیرفته شود.';
$string['invalidrepositoryid'] = 'شناسه نخزن نامعتبر است';
$string['isactive'] = 'فعال؟';
$string['lastmodified'] = 'آخرین تغییر';
$string['license'] = 'اجازه‌نامه';
$string['listview'] = 'نمایش به صورت لیست';
$string['loading'] = 'دریافت اطلاعات...';
$string['makefileinternal'] = 'تهیهٔ یک رونوشت از فایل';
$string['makefilereference'] = 'ایجاد یک فایل مستعار یا میان‌بُر به این فایل';
$string['manage'] = 'مدیریت انباره‌ها';
$string['manageurl'] = 'مدیریت';
$string['name'] = 'نام';
$string['newfoldername'] = 'نام پوشه جدید:';
$string['nofilesattached'] = 'فایلی ضمیمه نشده است';
$string['nofilesavailable'] = 'فایلی موجود نیست';
$string['off'] = 'فعال ولی پنهان';
$string['on'] = 'فعال و قابل مشاهده';
$string['openpicker'] = 'انتخاب یک فایل...';
$string['path'] = 'مسیر';
$string['plugin'] = 'پلاگین‌های انباره';
$string['pluginname'] = 'نام پلاگین انباره';
$string['pluginnamehelp'] = 'اگر این قسمت را خالی بگذارید نام پیش‌فرض استفاده خواهد شد.';
$string['popupblockeddownload'] = 'پنجرهٔ دریافت فایل مسدود شده است، لطفاً باز شدن پنجره‌های باز شونده را در مرورگرتان مجاز کنید و مجدداً تلاش کنید.';
$string['preview'] = 'پیش‌نمایش';
$string['referencesexist'] = '{$a} فایل مستعار/میان‌بُر از این فایل به‌عنوان مرجع خود استفاده می‌کنند.';
$string['referenceslist'] = 'فایل‌های مستعار/میان‌بُر';
$string['refresh'] = 'تازه‌سازی';
$string['renameto'] = 'تغییر نام به «{$a}»';
$string['repositories'] = 'انباره‌ها';
$string['repository'] = 'مخزن';
$string['repositorycourse'] = 'انباره‌های درسی';
$string['repositoryicon'] = 'آیکن انباره';
$string['save'] = 'ذخیره';
$string['saveas'] = 'ذخیره با نام';
$string['search'] = 'جستجو';
$string['select'] = 'انتخاب';
$string['setmainfile'] = 'تعیین فایل اصلی';
$string['setmainfile_help'] = 'اگر چند فایل در پوشه وجود داشته باشد، فایل اصلی فایلی است که در صفحهٔ نمایش ظاهر می‌شود. سایر فایل‌ها مانند تصاویر یا فیلم‌ها می‌توانند در آن جاسازی شوند. در دیالوگ مدیریت فایل، عنوان فایل اصلی به‌صورت ذخیم نمایش داده می‌شود.';
$string['settings'] = 'تنظیمات';
$string['setupdefaultplugins'] = 'برپا کردن پلاگین‌های پیش‌فرض انباره';
$string['siteinstances'] = 'نمونه‌های انباره در سایت';
$string['size'] = 'اندازه';
$string['submit'] = 'ثبت';
$string['syncfiletimeout'] = 'مهلت همگام‌سازی فایل‌ها';
$string['thumbview'] = 'نمایش به‌صورت آیکن‌ها';
$string['type'] = 'نوع';
$string['upload'] = 'قرار دادن این فایل روی سایت';
$string['usenonjsfilemanager'] = 'باز کردن دیالوگ مدیریت فایل در یک پنجرهٔ جدید';
$string['usenonjsfilepicker'] = 'باز کردن انتخاب‌گر رنگ در پنجرهٔ جدید';
