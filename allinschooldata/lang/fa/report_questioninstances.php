<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_questioninstances', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   report_questioninstances
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['eventreportviewed'] = 'گزارش مشاهده شد';
$string['getreport'] = 'مشاهدهٔ گزارش';
$string['hiddenquestions'] = 'پنهان';
$string['intro'] = 'این گزارش تمام زمینه‌هایی در سیستم که سؤالی از نوع مشخص شده در آنها وجود دارد لیست می‌کند.';
$string['pluginname'] = 'سؤال‌های ساخته شده';
$string['questioninstances:view'] = 'مشاهدهٔ گزارش سؤال‌های ساخته شده';
$string['reportforallqtypes'] = 'گزارش همهٔ انواع سؤال';
$string['reportformissingqtypes'] = 'گزارش سؤال‌های دارای نوع ناشناخته';
$string['reportforqtype'] = 'گزارش نوع سؤال «{$a}»';
$string['reportsettings'] = 'تنظیمات گزارش';
$string['totalquestions'] = 'جمع کل';
$string['visiblequestions'] = 'قابل مشاهده';
