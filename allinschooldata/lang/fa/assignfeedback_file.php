<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignfeedback_file', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignfeedback_file
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['batchoperationconfirmuploadfiles'] = 'ارسال یک یا چند فایل بازخورد برای تمام کاربران انتخاب شده؟';
$string['batchuploadfilesforusers'] = 'ارسال فایل‌های بازخورد به {$a} کاربر انتخاب شده.';
$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['enabled'] = 'بازخورد فایلی';
$string['enabled_help'] = 'اگر فعال باشد، استاد در هنگام تصحیح و نمره دادن به تکالیف می‌تواند نظرات بازخوردی خود را در قالب فایل‌هایی برای هر تکلیف ارسال کند. این فایل‌ها می‌توانند شامل تکلیف تصحیح شدهٔ شاگرد، فایل صوتی شامل نظر استاد در مورد تکلیف شاگرد، یا هر چیز دیگری باشند.';
$string['file'] = 'بازخورد فایلی';
$string['pluginname'] = 'بازخورد فایلی';
$string['selectedusers'] = 'کاربران انتخاب شده';
$string['uploadfiles'] = 'ارسال فایل‌های بازخورد';
