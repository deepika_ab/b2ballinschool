<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_photo', language 'fa', branch 'MOODLE_32_STABLE'
 *
 * @package   theme_photo
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['advancedsettings'] = 'تنظیمات پیشرفته';
$string['backgrounds'] = 'پس‌زمینه‌ها';
$string['brandcolor'] = 'رنگ سازمانی';
$string['brandcolor_desc'] = 'رنگ مؤکد در سایت.';
$string['choosereadme'] = 'پوستهٔ عکس، یک پوستهٔ فرزند از پوستهٔ Boost است. این پوسته امکان بارگذاری تصاویر پس‌زمینه را فراهم می‌کند.';
$string['configtitle'] = 'تنظیمات عکس';
$string['dashboardbackgroundimage'] = 'تصویر پس‌زمینهٔ صفحهٔ میزِ کار';
$string['dashboardbackgroundimage_desc'] = 'تصویری که کشیده خواهد شد تا پس‌زمینهٔ صفحهٔ میزِکار را بپوشاند.';
$string['defaultbackgroundimage'] = 'تصویر پس‌زمینهٔ پیش‌فرض صفحه‌ها';
$string['defaultbackgroundimage_desc'] = 'تصویری که کشیده خواهد شد تا پس‌زمینهٔ تمام صفحه‌هایی که پس‌زمینهٔ اختصاصی‌تری ندارند را بپوشاند.';
$string['frontpagebackgroundimage'] = 'تصویر پس‌زمینهٔ صفحهٔ اول';
$string['frontpagebackgroundimage_desc'] = 'تصویری که کشیده خواهد شد تا پس‌زمینهٔ صفحهٔ اول را بپوشاند.';
$string['generalsettings'] = 'تنظیمات عمومی';
$string['incoursebackgroundimage'] = 'تصویر پس‌زمینهٔ صفحهٔ درس';
$string['incoursebackgroundimage_desc'] = 'تصویری که کشیده خواهد شد تا پس‌زمینهٔ صفحه‌های درس را بپوشاند.';
$string['loginbackgroundimage'] = 'تصویر پس‌زمینهٔ صفحهٔ ورود به سایت';
$string['loginbackgroundimage_desc'] = 'تصویری که کشیده خواهد شد تا پس‌زمینهٔ صفحهٔ ورود به سایت را بپوشاند.';
$string['pluginname'] = 'عکس';
$string['rawscss'] = 'SCSS خام';
$string['rawscss_desc'] = 'از این فیلد برای فراهم کردن کد SCSS یا CSS که به انتهای style sheet مودل تزریق خواهد شد استفاده کنید.';
$string['rawscsspre'] = 'SCSS اولیه خام';
$string['region-side-pre'] = 'چپ';
