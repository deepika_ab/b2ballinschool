<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'bulkusers', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   bulkusers
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addall'] = 'اضافه کردن همه';
$string['addsel'] = 'اضافه کردن به لیست';
$string['allfilteredusers'] = 'کاربران رد شده از فیلترها ({$a->count} از {$a->total})';
$string['allselectedusers'] = 'کاربران انتخاب شده ({$a->count} از {$a->total})';
$string['allusers'] = 'همه کاربران ({$a})';
$string['available'] = 'موجود';
$string['confirmmessage'] = 'آیا می‌خواهید که متن بالا را به همه این کاربران بفرستید؟<br />{$a}';
$string['nofilteredusers'] = 'کاربری پیدا نشد (0 از {$a})';
$string['noselectedusers'] = 'کاربری انتخاب نشده است';
$string['removeall'] = 'حذف همه';
$string['removesel'] = 'حذف از لیست';
$string['selected'] = 'انتخاب شده';
$string['selectedlist'] = 'لیست کاربران انتخاب شده...';
$string['selectedlist_help'] = 'با کلیک روی نام کاربران و سپس کلیک بر روی دکمه مناسب می‌توان آن‌ها را به لیست کاربران انتخاب شده اضافه کرد یا از آن حذف کرد. با نگاه داشتن کلید Apple یا Ctrl در هنگام کلیک بر روی نام کاربران می‌توان چند کاربر را انتخاب کرد.';
$string['users'] = 'کاربران';
$string['usersfound'] = '{$a} کاربر پیدا شد.';
$string['users_help'] = 'تمام کاربرانی که از فیلترهای تعیین شده رد شوند در لیست کاربران موجود نشان داده می‌شوند. اگر فیلتری تعیین نشده باشد، تمام کاربران سایت لیست می‌شوند.';
$string['usersinlist'] = 'کاربران در لیست';
$string['usersselected'] = '{$a} کاربر انتخاب شد.';
