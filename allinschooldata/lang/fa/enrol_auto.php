<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_auto', language 'fa', branch 'MOODLE_36_STABLE'
 *
 * @package   enrol_auto
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['auto:manage'] = 'مدیریت کاربران ثبت‌نام شده';
$string['courseview'] = 'مشاهده درس';
$string['customwelcomemessage'] = 'پیام خوش‌آمد سفارشی';
$string['defaultrole'] = 'نقش پیش‌فرض جهت تخصیص';
$string['enrolon'] = 'ثبت‌نام در لحظه‌ای که';
$string['enrolon_desc'] = 'اتفاقی که باید رخ دهد تا ثبت‌نام انجام شود';
$string['modview'] = 'مشاهده فعالیت یا منبع از یک درس';
$string['pluginname'] = 'ثبت‌نام خودکار';
$string['role'] = 'نقش تخصیص داده شونده پیش‌فرض';
$string['sendcoursewelcomemessage'] = 'ارسال پیام خوش‌آمد به درس';
$string['sendcoursewelcomemessage_help'] = 'اگر فعال شود، کاربر یک پیام خوش‌آمد به درس را از طریق ایمیل دریافت می‌کند.';
$string['userlogin'] = 'ورود به سایت توسط کاربر';
