<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_log', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   report_log
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allsources'] = 'تمام منشأها';
$string['eventcomponent'] = 'کامپوننت';
$string['eventcontext'] = 'زمینهٔ رویداد';
$string['eventloggedas'] = '{$a->realusername} در قالب {$a->asusername}';
$string['eventorigin'] = 'منشأ';
$string['eventrelatedfullnameuser'] = 'کاربر تأثیر گرفته';
$string['eventreportviewed'] = 'گزارش log ها مشاهده شد';
$string['eventuserreportviewed'] = 'گزارش log های کاربر مشاهده شد';
$string['log:view'] = 'مشاهده log های درس';
$string['log:viewtoday'] = 'مشاهده log های امروز';
$string['page'] = 'صفحهٔ {$a}';
$string['page-report-log-index'] = 'گزارش لاگ‌های درس';
$string['page-report-log-user'] = 'گزارش لاگ‌های درس کاربر';
$string['pluginname'] = 'log ها';
$string['restore'] = 'بازیابی';
$string['web'] = 'وب';
$string['ws'] = 'وب‌سرویس';
