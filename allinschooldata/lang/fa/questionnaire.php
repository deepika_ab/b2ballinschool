<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'questionnaire', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   questionnaire
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'اقدام';
$string['additionalinfo'] = 'اطلاعات اضافی';
$string['additionalinfo_help'] = 'متنی که در بالای صفحهٔ اول این پرسش‌نامه نمایش داده می‌شود (به‌عنوان مثال دستورالعمل‌ها، اطلاعات پیش‌زمینه، ...)';
$string['addnewquestion'] = 'اضافه‌کردن {$a} سؤال';
$string['addquestions'] = 'اضافه‌کردن سؤال‌ها';
$string['addselqtype'] = 'اضافه‌کردن نوع سؤال انتخاب‌شده';
$string['alignment'] = 'تنظیم دکمه‌های رادیویی';
$string['alignment_help'] = 'انتخاب نحوه نمایش دکمه‌ها: عمودی (پیش فرض) یا افقی';
$string['all'] = 'تمام موارد';
$string['alreadyfilled'] = 'شما قبلا {$a} این پرسش‌نامه را برای ما تکمیل کرده‌اید. با تشکر.';
$string['andaveragevalues'] = 'میانگین مقادیر';
$string['anonymous'] = 'بی نام';
$string['answer'] = 'پاسخ';
$string['answergiven'] = 'پاسخ این سوال داده شده است.';
$string['answernotgiven'] = 'پاسخ این سوال داده نشده است.';
$string['answerquestions'] = 'پاسخگویی به سوالات';
$string['answers'] = 'پاسخ‌ها';
$string['attempted'] = 'این پرسش‌نامه ارسال شده است.';
$string['attemptstillinprogress'] = 'در حال اجرا. ذخیره شده در:';
$string['autonumbering'] = 'شماره‌گذاری خودکار';
$string['autonumbering_help'] = 'شماره‌گذاری خودکار سؤالات و صفحات. شما ممکن است بخواهید شماره‌گذاری خودکار را برای پرسش‌نامه با شاخه شرطی غیرفعال کنید.';
$string['autonumberno'] = 'عدم شماره‌گذاری سوالات و صفحات';
$string['autonumberpages'] = 'شماره‌گذاری خودکار صفحات';
$string['autonumberpagesandquestions'] = 'شماره‌گذاری خودکار صفحات و سوالات';
$string['autonumberquestions'] = 'شماره‌گذاری خودکار سوالات';
$string['average'] = 'میانگین';
$string['averageposition'] = 'میانگین شماره سوالات';
$string['averagerank'] = 'میانگین هر ردیف';
$string['bodytext'] = 'متن';
$string['boxesnbexact'] = 'دقیقا {$a}  کادر';
$string['boxesnbmax'] = 'حداکثر {$a}  کادر';
$string['boxesnbmin'] = 'حداقل {$a}  کادر';
$string['boxesnbreq'] = 'حتما باید به این سوال پاسخ دهید';
$string['by'] = 'بر اساس';
$string['cannotviewpublicresponses'] = 'شما نمی‌توانید پاسخ‌هایی که به این پرسش‌نامه عمومی داده شده است مشاهده کنید.';
$string['chart:bipolar'] = 'نمودار میله‌ای دو ستونی';
$string['chart:hbar'] = 'نمودار میله‌ای افقی';
$string['chart:radar'] = 'نمودار راداری (عنکبوتی)';
$string['chart:rose'] = 'نمودار گل سرخی';
$string['chart:type'] = 'نوع نمودار';
$string['chart:type_help'] = 'نوع نمودار مورد نظر برای بازخورد را انتخاب کنید.';
$string['chart:vprogress'] = 'نمودار پیشرفت میله‌ای عمودی';
$string['checkboxes'] = 'کادرهای انتخاب';
$string['checkboxes_help'] = 'در هر خط یک گزینه برای کاربر قرار دهید تا یک یا چند پاسخ را انتخاب کند.';
$string['checkbreaksadded'] = 'اضافه‌کردن صفحه(های) جدید در محل(های):';
$string['checkbreaksok'] = 'تمام صفحات ایجاد شده  مورد نیاز وجود دارد.';
$string['checkbreaksremoved'] = 'حذف {$a} صفحه اضافی';
$string['checknotstarted'] = '"انجام نشده" را انتخاب کنید.';
$string['checkstarted'] = '"انجام شده" را انتخاب کنید.';
$string['clicktoswitch'] = 'برای تغییر گزینه کلیک کنید.';
$string['closed'] = 'مهلت تکمیل این پرسش‌نامه در{$a} به اتمام رسیده است. با تشکر';
$string['closedate_help'] = 'شما می‌توانید برای غیرفعال شدن پرسش‌نامه تاریخ تعیین کنید. کادر را علامت بزنید و تاریخ و زمان مورد نظر را انتخاب کنید. در این صورت کاربران نمی‌توانند پس از تاریخ معین‌شده پرسش‌نامه را تکمیل کنند. اگر این گزینه انتخاب نشود، می‌توان پرسش‌نامه را در هر زمانی تکمیل نمود.';
$string['closeson'] = 'مهلت تکمیل پرسش‌نامه در {$a} به اتمام می‌رسد.';
$string['completionsubmit'] = 'کاربر برای کامل‌کردن این پرسش‌نامه باید آن را ثبت کند.';
$string['condition'] = 'شرط';
$string['confalts'] = '- یا - <br />صفحهٔ تأییدیه';
$string['configmaxsections'] = 'بیشترین بخشهای دارای بازخورد';
$string['configusergraph'] = 'نمایش نمودار برای بازخوردهای "تست شخصیت"';
$string['configusergraphlong'] = 'به کارگیری کتابخانه <a href="http://www.rgraph.net/">Rgraph</a>  به منظور نمایش نمودار بازخورد "تست شخصیت"';
$string['confirmdelallresp'] = 'آیا مطمئن هستید که می‌خواهید تمام پاسخ‌های این پرسش‌نامه را حذف نمایید؟';
$string['confirmdelchildren'] = 'چنانچه این سوال را حذف نمایید، سوالات وابسته به آن نیز حذف می‌گردد.';
$string['confirmdelgroupresp'] = 'آیا مطمئن هستید که می‌خواهید تمام پاسخ‌های {$a} را حذف نمایید؟';
$string['confirmdelquestion'] = 'آیا مطمئن هستید که می‌خواهید سوال شماره {$a} را حذف نمایید.';
$string['confirmdelquestionresps'] = 'انتخاب این گزینه، پاسخ {$a}  که قبلا داده شده بود را حذف می‌نماید.';
$string['confpage'] = 'عنوان';
$string['confpagedesc'] = 'عنوان (به صورت پررنگ) و متن مربوط به صفحه «تأییدیه» که پس از تکمیل پرسش‌نامه توسط کاربر نمایش داده خواهد شد. (در صورت وجود URL به متن ارجحیت دارد.)';
$string['confpage_help'] = 'عنوان (به‌صورت پررنگ) و متن مربوط به صفحه «تأییدیه» که پس از تکمیل پرسش‌نامه توسط کاربر نمایش داده خواهد شد. (در صورت وجود، URL به متن ارجحیت دارد)  اگر این فیلد را خالی بگذارید، پس از اتمام پرسش‌نامه، پیام پیش‌فرض (با تشکر از شما برای تکمیل این پرسش‌نامه) نمایش داده خواهد شد.';
$string['contentoptions'] = 'گزینه‌های محتوا';
$string['couldnotcreatenewsurvey'] = 'امکان ایجاد نظرسنجی جدید وجود ندارد!';
$string['couldnotdelresp'] = 'امکان حذف پاسخ وجود ندارد.';
$string['createcontent'] = 'تعریف محتوای جدید';
$string['createcontent_help'] = 'یکی از گزینه‌های دکمه‌های رادیویی را انتحاب نمایید. گزینه "ایجاد محتوای جدید" به صورت پیش فرض انتخاب شده است.';
$string['createnew'] = 'ایجاد محتوای جدید';
$string['date'] = 'تاریخ';
$string['dateformatting'] = 'از فرمت روز / ماه / سال استفاده کنید، برای مثال 14 مارس سال 1945';
$string['date_help'] = 'چنانچه انتظار دارید پاسخ دارای فرمت درست تاریخ باشد، از این نوع سوال استفاده نمایید.';
$string['deleteallresponses'] = 'حذف تمام پاسخها';
$string['deletecurrentquestion'] = 'حذف سوال {$a}';
$string['deletedallgroupresp'] = 'حذف تمام پاسخهای مربوط به گروه  {$a}';
$string['deletedallresp'] = 'پاسخ‌های پرسش‌نامه حذف شد.';
$string['deletedisabled'] = 'امکان حذف این مورد وجود ندارد.';
$string['deletedresp'] = 'پاسخ حذف شده';
$string['deleteresp'] = 'حذف این پاسخ';
$string['dependencies'] = 'وابستگی‌ها';
$string['dependquestion'] = 'سوال اصلی';
$string['dependquestion_help'] = 'شما می‌توانید سوال اصلی و گزینه مربوط به این سوال را انتخاب کنید. سوال زیر شاخه این سوال فقط در صورتی برای کاربران نمایش داده می‌شود که  سوال  اصلی و گزینه آن قبلا انتخاب شده باشد.';
$string['directwarnings'] = 'وابستگی‌های مستقیم به این سوال حذف می‌شود. این امر باعث می‌شود:';
$string['displaymethod'] = 'روش نمایش برای سوال تعریف نشده است.';
$string['download'] = 'دانلود';
$string['downloadtextformat'] = 'دانلود در قالب متن';
$string['downloadtextformat_help'] = 'این ویژگی شما را قادر می‌سازد تا تمام پاسخ‌های پرسش‌نامه را در قالب فایل متنی (CSV) ذخیره کنید. در این صورت می‌توانید فایل را به صفحه گسترده (به عنوان مثال MS Excel یا Open Office Calc) یا یک بسته آماری به منظور پردازش بیشتر داده‌ها وارد نمایید.';
$string['dropdown'] = 'منوی کرکره‌ای';
$string['dropdown_help'] = 'پیشنهاد می‌شود به جای استفاده از منوی کرکره‌ای از دکمه‌های رادیویی استفاده نمایید. مگر در مواردی که گزینه ها بسیار زیاد است.';
$string['edit'] = 'ویرایش';
$string['firstrespondent'] = 'اولین پاسخ‌دهنده';
$string['lastrespondent'] = 'آخرین پاسخ‌دهنده';
$string['managequestions'] = 'مدیریت سوالات';
$string['non_respondents'] = 'کسانی‌که تاکنون پاسخ این پرسش‌نامه را ارسال ننموده‌اند';
$string['noresponsedata'] = 'به این سوال پاسخ داده نشده است.';
$string['noresponses'] = 'بدون پاسخ';
$string['notrequired'] = 'پاسخ الزامی نیست.';
$string['order_ascending'] = 'نمایش با ترتیب صعودی';
$string['order_default'] = 'نمایش با ترتیب پیش‌فرض';
$string['order_descending'] = 'نمایش با ترتیب نزولی';
$string['orderresponses'] = 'مرتب نمودن پاسخ‌ها';
$string['previewing'] = 'مشاهده پیش‌نمایش پرسش‌نامه';
$string['preview_label'] = 'پیش‌نمایش';
$string['preview_questionnaire'] = 'پیش‌نمایش پرسش‌نامه';
$string['print'] = 'چاپ این پاسخ';
$string['printblank'] = 'چاپ فرم خالی';
$string['questionnaire:message'] = 'ارسال پیام به پاسخ نداده‌ها';
$string['questionnaire:printblank'] = 'چاپ فرم خالی پرسش‌نامه';
$string['questions'] = 'سوالات';
$string['required'] = 'پاسخ الزامی است';
$string['respondent'] = 'پاسخ‌داده';
$string['respondents'] = 'پاسخ‌داده‌ها';
$string['response'] = 'پاسخ';
$string['responses'] = 'پاسخ‌ها';
$string['save'] = 'ذخیره';
$string['savedbutnotsubmitted'] = 'این پرسش‌نامه ذخیره شده، اما هنوز ارسال نشده است.';
$string['savedprogress'] = 'پاسخ‌های شما ذخیره شد. شما می‌توانید در هر زمان برای تکمیل این پرسش‌نامه اقدام نمایید.';
$string['send'] = 'ارسال';
$string['sendemail'] = 'ارسال ایمیل';
$string['send_message'] = 'ارسال پیام به کاربران انتخاب شده';
$string['send_message_to'] = 'ارسال پیام به :';
$string['show_nonrespondents'] = 'پاسخ‌ نداده‌ها';
$string['subject'] = 'موضوع';
$string['submitpreview'] = 'ارسال پیش‌نمایش';
$string['submitsurvey'] = 'ارسال فرم پرسش‌نامه';
$string['summary'] = 'خلاصه';
$string['thisresponse'] = 'این پاسخ';
$string['viewallresponses'] = 'مشاهده همه پاسخ‌ها';
$string['viewbyresponse'] = 'لیست پاسخ‌ها';
$string['viewindividualresponse'] = 'پاسخ‌های انفرادی';
$string['viewresponses'] = 'تمام پاسخ‌های ({$a})';
$string['yourresponse'] = 'پاسخ شما';
$string['yourresponses'] = 'پاسخ‌های شما';
