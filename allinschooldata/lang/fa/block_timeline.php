<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_timeline', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_timeline
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['all'] = 'همه';
$string['ariadayfilter'] = 'فیلتر کردن موارد گاه‌شمار فعالیت‌ها';
$string['ariadayfilteroption'] = 'گزینهٔ فیلتر {$a}';
$string['ariaeventlistpagelimit'] = 'نمایش {$a} فعالیت در صفحه';
$string['ariaeventlistpaginationnavdates'] = 'صفحه‌بندی گاه‌شمار فعالیت‌ها';
$string['ariaviewselector'] = 'مرتب‌سازی موارد گاه‌شمار فعالیت‌ها';
$string['ariaviewselectoroption'] = 'گزینهٔ مرتب‌سازی {$a}';
$string['duedate'] = 'مهلت';
$string['morecourses'] = 'درس‌های بیشتر';
$string['next30days'] = '۳۰ روز آینده';
$string['next3months'] = '۳ ماه آینده';
$string['next6months'] = '۶ ماه آینده';
$string['next7days'] = '۷ روز آینده';
$string['noevents'] = 'مهلت هیچ فعالیتی نزدیک نیست';
$string['overdue'] = 'تاخیر دار';
$string['pluginname'] = 'گاه‌شمار فعالیت‌ها';
$string['sortbycourses'] = 'مرتب‌سازی بر اساس درس‌ها';
$string['sortbydates'] = 'مرتب‌سازی بر اساس تاریخ‌ها';
$string['timeline'] = 'گاه‌شمار فعالیت‌ها';
$string['timeline:myaddinstance'] = 'اضافه کردن یک بلوک گاه‌شمار فعالیت‌ها به میزکار';
$string['viewcourse'] = 'مشاهده درس';
