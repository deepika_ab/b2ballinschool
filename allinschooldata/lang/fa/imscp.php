<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'imscp', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   imscp
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['contentheader'] = 'محتوا';
$string['keepold'] = 'بایگانی کردن بسته‌ها';
$string['keepoldexplain'] = 'چند بسته باید بایگانی شود؟';
$string['modulename'] = 'بستهٔ محتوای IMS';
$string['modulename_help'] = 'یک بستهٔ محتوای IMS، شامل یک سری فایل است که مطابق با یک استاندارد مورد توافق بسته‌بندی شده‌اند تا بتوانند در سیستم‌های مختلف مورد استفادهٔ مجدد قرار گیرند. با استفاده از ماژول فعالیت IMS می‌توان بسته‌های IMS را به‌صورت یک فایل zip بارگذاری کرد و به‌عنوان یک منبع آموزشی به درس اضافه کرد.

محتوا معمولا بر روی چند صفحه (با امکان حرکت بین صفحات) نمایش داده می‌شود. گزینه‌های متعددی برای نمایش محتوا در یک پنجرهٔ بازشونده، با منو یا دکمه‌های حرکت بین صفحات و ... وجود دارد.

از یک بستهٔ محتوای IMS می‌توان برای نمایش محتواهای چندرسانه‌ای و انیمیشن استفاده کرد.';
$string['navigation'] = 'راهبری';
$string['packagefile'] = 'فایل بسته';
$string['page-mod-imscp-x'] = 'هر صفحه‌ای از ماژول بستهٔ محتوای IMS';
$string['pluginname'] = 'بستهٔ محتوای IMS';
$string['search:activity'] = 'بسته محتوای IMS - اطلاعات منبع';
$string['toc'] = 'فهرست';
