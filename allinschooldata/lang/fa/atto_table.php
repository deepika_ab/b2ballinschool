<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_table', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_table
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcolumnafter'] = 'درج ستون بعد از';
$string['addrowafter'] = 'درج سطر بعد از';
$string['all'] = 'اطراف هر کدام از خانه‌ها';
$string['allowbackgroundcolour'] = 'مجاز بودن رنگ پس‌زمینه';
$string['allowborder'] = 'مجاز بودن سفارشی‌کردن مرزها';
$string['allowborder_desc'] = 'اگر فعال باشد، مرزهای جدول و خانه‌های جدول را می‌توان سفارشی کرد.';
$string['allowwidth'] = 'مجاز بودن عرض';
$string['appearance'] = 'ظاهر';
$string['backgroundcolour'] = 'رنگ پس‌زمینه';
$string['bordercolour'] = 'رنگ لبه';
$string['borders'] = 'لبه‌ها';
$string['bordersize'] = 'اندازهٔ لبه';
$string['borderstyles'] = 'سبک لبه‌ها';
$string['both'] = 'هر دو';
$string['caption'] = 'شرح';
$string['captionposition'] = 'محل شرح';
$string['columns'] = 'ستون‌ها';
$string['createtable'] = 'ساخت جدول';
$string['dashed'] = 'خط‌چین';
$string['deletecolumn'] = 'حذف ستون';
$string['deleterow'] = 'حذف سطر';
$string['dotted'] = 'نقطه‌چین';
$string['edittable'] = 'ویرایش جدول';
$string['headers'] = 'تعریف عنوان در';
$string['movecolumnleft'] = 'انتقال ستون به چپ';
$string['movecolumnright'] = 'انتقال ستون به راست';
$string['moverowdown'] = 'انتقال سطر به پایین';
$string['moverowup'] = 'انتقال سطر به بالا';
$string['noborder'] = 'بدون لبه';
$string['none'] = 'هیچ';
$string['numberofcolumns'] = 'تعداد ستون‌ها';
$string['numberofrows'] = 'تعداد سطرها';
$string['outer'] = 'اطراف جدول';
$string['pluginname'] = 'جدول';
$string['privacy:metadata'] = 'پلاگین atto_table هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['rows'] = 'سطرها';
$string['settings'] = 'تنظیمات جدول';
$string['solid'] = 'یک‌پارچه';
$string['themedefault'] = 'پیش‌فرض پوسته';
$string['updatetable'] = 'به‌روزکردن جدول';
$string['width'] = 'عرض جدول (بر حسب درصد)';
