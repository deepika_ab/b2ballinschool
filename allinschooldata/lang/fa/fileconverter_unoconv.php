<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'fileconverter_unoconv', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   fileconverter_unoconv
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pathtounoconv'] = 'مسیر تبدیل‌کنندهٔ اسناد unoconv';
$string['pathtounoconv_help'] = 'مسیر تبدیل‌کنندهٔ اسناد unoconv. این یک برنامهٔ اجرایی است که می‌تواند قالب‌هایی که توسط LibreOffice پشتیبانی می‌شود را به یکدیگر تبدیل کند. این کار اختیاری است، ولی اگر تعیین شده باشد، مودل از آن برای تبدیل خودکار قالب اسناد استفاده خواهد کرد. از این برنامه برای پشتیبانی از بازهٔ وسیع‌تری از فایل‌های ورودی برای قابلیت یادداشت‌نویسی روی PDF در تکلیف استفاده می‌شود.';
$string['pluginname'] = 'Unoconv';
$string['test_unoconv'] = 'آزمایش مسیر unoconv';
$string['test_unoconvdoesnotexist'] = 'مسیر unoconv به برنامهٔ unoconv اشاره نمی‌کند. لطفا تنظیمات مسیرها را مرور کنید.';
$string['test_unoconvdownload'] = 'دریافت فایل pdf تستیِ تبدیل‌شده.';
$string['test_unoconvempty'] = 'مسیر unoconv تعیین نشده است. لطفا تنظیمات مسیرها را مرور کنید.';
$string['test_unoconvisdir'] = 'مسیر unoconv به یک پوشه اشاره می‌کند. لطفا نام برنامهٔ unoconv را در مسیری که تعیین می‌کنید بیاورید';
$string['test_unoconvnotexecutable'] = 'مسیر unoconv به فایلی اشاره می‌کند که اجرایی نیست';
$string['test_unoconvok'] = 'پیکربندی مسیر unoconv به نظر درست می‌رسد.';
$string['test_unoconvversionnotsupported'] = 'نسخهٔ unoconv ی که نصب کرده‌اید پشتیبانی نمی‌شود.';
