<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_tag_flickr', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   block_tag_flickr
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configtitle'] = 'عنوان';
$string['date-posted-asc'] = 'تاریخ ارسال (صعودی)';
$string['date-posted-desc'] = 'تاریخ ارسال (نزولی)';
$string['date-taken-asc'] = 'تاریخ گرفته شدن (صعودی)';
$string['date-taken-desc'] = 'تاریخ گرفته شدن (نزولی)';
$string['defaulttile'] = 'فلیکر';
$string['interestingness-asc'] = 'میزان جذابیت (صعودی)';
$string['interestingness-desc'] = 'میزان جذابیت (نزولی)';
$string['numberofphotos'] = 'تعداد تصاویر';
$string['pluginname'] = 'فلیکر';
$string['relevance'] = 'میزان مربوط بودن';
$string['sortby'] = 'ترتیب';
$string['tag_flickr:addinstance'] = 'اضافه‌کردن یک بلوک جدید فلیکر';
