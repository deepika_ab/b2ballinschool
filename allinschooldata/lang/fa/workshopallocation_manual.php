<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshopallocation_manual', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   workshopallocation_manual
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addreviewee'] = 'اضافه کردن افراد مورد بازبینی';
$string['addreviewer'] = 'اضافه کردن بازبین';
$string['allocationadded'] = 'مسئولیت بازبینی با موفقیت اختصاص داده شد';
$string['allocationexists'] = 'این تخصیص هم‌اکنون وجود دارد';
$string['areyousuretodeallocate'] = 'آیا مطمئن هستید که می‌خواهید ارزشیاب انتخاب شده را حذف کنید؟';
$string['areyousuretodeallocategraded'] = 'شما در آستانهٔ حذف ارزشیابی‌ای هستید که نمره داده شده است. آیا واقعا مطمئنید که می‌خواهید این کار را بکنید؟';
$string['pluginname'] = 'تخصیص دستی';
$string['showallparticipants'] = 'مشاهدهٔ همهٔ شرکت کنندگان';
