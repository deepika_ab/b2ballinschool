<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filter_urltolink', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   filter_urltolink
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['embedimages'] = 'جایگذاری تصاویر';
$string['embedimages_desc'] = 'URL های متنی مربوط به تصاویر در قالب‌های متنی انتخاب شده را با خود تصویر جاگذاری می‌کند.';
$string['filtername'] = 'تبدیل URL ها به عکس و پیوند';
$string['settingformats'] = 'اعمال روی قالب‌ها';
$string['settingformats_desc'] = 'فیلتر تنها در صورتی اعمال خواهد شد که متن اصلی در یکی از قالب‌های انتخاب شده وارد شده باشد.';
