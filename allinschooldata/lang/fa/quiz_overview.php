<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_overview', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   quiz_overview
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allattempts'] = 'نمایش همهٔ دفعات';
$string['allattemptscontributetograde'] = 'همهٔ دفعه‌های شرکت در آزمون روی نمرهٔ نهایی کاربر تأثیرگذار هستند.';
$string['allstudents'] = 'نمایش همهٔ {$a}';
$string['attemptsonly'] = 'فقط نمایش {$a}ی که در آزمون شرکت کرده‌اند';
$string['attemptsprepage'] = 'دفعه‌های نمایش داده شده در هر صفحه';
$string['deleteselected'] = 'پاک کردن دفعه‌های انتخاب شده';
$string['done'] = 'انجام شد';
$string['err_failedtodeleteregrades'] = 'پاک کردن نمره‌های محاسبه شدهٔ شرکت در آزمون موفقیت‌آمیز نبود';
$string['err_failedtorecalculateattemptgrades'] = 'محاسبهٔ مجدد نمره‌های شرکت در آزمون موفقیت‌آمیز نبود';
$string['highlightinggraded'] = 'دفعه‌های شرکت در آزمون که در نمرهٔ نهایی تأثیرگذار بوده‌اند به صورت مشخص نمایش داده شده‌اند.';
$string['needed'] = 'لازم است';
$string['noattemptsonly'] = 'فقط نمایش / دریافت آن دسته از {$a} که در آزمون شرکت نکرده‌اند';
$string['noattemptstoregrade'] = 'هیچ‌یک از دفعه‌های شرکت در آزمون نیاز به نمره‌دهی مجدد ندارند';
$string['nogradepermission'] = 'شما اجازهٔ نمره‌دهی این آزمون را ندارید.';
$string['onlyoneattemptallowed'] = 'هر کاربر فقط یک بار مجاز به شرکت در این آزمون است.';
$string['optallattempts'] = 'همهٔ دفعه‌های شرکت در آزمون';
$string['optallstudents'] = 'همهٔ {$a}ی که در آزمون شرکت کرده یا نکرده‌اند.';
$string['optattemptsonly'] = '{$a}ی که در آزمون شرکت کرده‌اند';
$string['optnoattemptsonly'] = '{$a}ی که در آزمون شرکت نکرده‌اند';
$string['optonlyregradedattempts'] = 'که دوباره نمره داده شده است / علامت خورده است که باید دوباره نمره داده شود';
$string['overview'] = 'نمرات';
$string['overviewdownload'] = 'دریافت فایل مرور اجمالی';
$string['overviewreport'] = 'گزارش نمرات';
$string['overviewreportgraph'] = 'نمودار میله‌ای تعداد شاگردانی که نمرهٔ آن‌ها در هر یک از محدوده‌ها قرار دارد';
$string['overviewreportgraphgroup'] = 'نمودار میله‌ای تعداد شاگردان گروه «{$a}» که نمرهٔ آن‌ها در هر یک از محدوده‌ها قرار دارد';
$string['pagesize'] = 'اندازهٔ صفحه';
$string['preferencespage'] = 'ترجیحات مربوط به فقط این صفحه';
$string['preferencessave'] = 'نمایش گزارش';
$string['preferencesuser'] = 'ترجیحات شما برای این گزارش';
$string['regrade'] = 'نمره‌دهی مجدد';
$string['regradeall'] = 'نمره‌دهی مجدد همه';
$string['regradealldry'] = 'اجرای آزمایشی یک نمره‌دهی مجدد کامل';
$string['regradealldrydo'] = 'نمره‌دهی مجدد دفعه‌هایی که به عنوان نیازمند به نمره‌دهی مجدد علامت خورده‌اند ({$a})';
$string['regradealldrydogroup'] = 'نمره‌دهی مجدد دفعه‌هایی ({$a->countregradeneeded}) که به عنوان نیازمند به نمره‌دهی مجدد در گروه «{$a->groupname}» علامت خورده‌اند';
$string['regradealldrygroup'] = 'اجرای آزمایشی یک نمره‌دهی مجدد کامل برای گروه «{$a->groupname}»';
$string['regradeallgroup'] = 'نمره‌دهی مجدد کامل برای گروه «{$a->groupname}»';
$string['regradecomplete'] = 'نمره‌دهی مجدد با موفقیت انجام شد';
$string['regradeheader'] = 'نمره‌دهی مجدد';
$string['regradeselected'] = 'نمره‌دهی مجدد دفعه‌های انتخاب شده';
$string['regradingattemptxofy'] = 'نمره‌دهی مجددِ تلاش ({$a->done} از {$a->count})';
$string['show'] = 'نمایش / دریافت';
$string['showattempts'] = 'تنها دفعاتی نمایش داده / دریافت شوند';
$string['showdetailedmarks'] = 'نمره‌های مربوط به هر سؤال';
$string['showinggraded'] = 'در حال نمایش دفعه‌های شرکت هر کاربر در آزمون که نمره داده شده است.';
$string['showinggradedandungraded'] = 'در حال نمایش دفعه‌های شرکت هر کاربر در آزمون (چه نمره داده شده و چه نشده). دفعه‌ای از شرکت هر کاربر که نمره داده شده است به صورت مشخص نشان داده شده است. نمره‌دهی این آزمون بر اساس {$a} است.';
$string['studentingroup'] = '«{$a->coursestudent}» در گروه «{$a->groupname}»';
$string['studentingrouplong'] = '«{$a->coursestudent}» در این گروه';
