<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'form', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   form
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addfields'] = 'اضافه کردن {$a} گزینه به فرم';
$string['advancedelement'] = 'مورد پیشرفته';
$string['close'] = 'بستن';
$string['custom'] = 'سفارشی';
$string['day'] = 'روز';
$string['default'] = 'پیش‌فرض';
$string['display'] = 'نمایش';
$string['err_alphanumeric'] = 'شما اینجا فقط عدد و حروف باید وارد کنید.';
$string['err_email'] = 'شما باید یک آدرس پست‌الکترونیکی معتبر اینجا وارد کنید.';
$string['err_lettersonly'] = 'شما باید فقط عدد اینجا وارد کنید.';
$string['err_maxfiles'] = 'شما نباید بیشتر از {$a} فایل اینجا پیوست کنید.';
$string['err_maxlength'] = 'شما اینجا نباید بیشتر از {$a->format} حرف وارد کنید.';
$string['err_minlength'] = 'شما اینجا باید حداقل {$a->format} حرف وارد کنید.';
$string['err_nonzero'] = 'شما اینجا باید یک عدد که با 0 شروع نمی شود وارد کنید.';
$string['err_nopunctuation'] = 'شما اینجا نباید از هیچ حرف نشانگذاری استفاده کنید.';
$string['err_numeric'] = 'شما اینجا باید یک عدد وارد کنید.';
$string['err_rangelength'] = 'شما اینجا باید بین {$a->format[0]} تا {$a->format[1]} حرف وارد کنید.';
$string['err_required'] = 'باید این قسمت را پر کنید.';
$string['filesofthesetypes'] = 'انواع فایل‌های مورد پذیرش:';
$string['filetypesany'] = 'همه انواع فایل‌ها';
$string['filetypesothers'] = 'دیگر فایل‌ها';
$string['filetypesunknown'] = 'فایل از نوع ناشناخته: {$a}';
$string['general'] = 'عمومی';
$string['hideadvanced'] = 'پنهان کردن قسمت‌های پیشرفته';
$string['hour'] = 'ساعت';
$string['minute'] = 'دقیقه';
$string['miscellaneoussettings'] = 'تنظیمات متفرقه';
$string['modstandardels'] = 'تنظیمات عمومی ماژول';
$string['month'] = 'ماه';
$string['mustbeoverriden'] = 'متد انتزاعی form_definition باید در کلاس {$a} تعریف (override) شود؛ لطفا کد را اصلاح کنید.';
$string['newvaluefor'] = 'مقدار جدید برای {$a}';
$string['nomethodforaddinghelpbutton'] = 'هیچ متدی برای اضافه کردن یک دکمهٔ راهنما به المان {$a->name} در فرم وجود ندارد (کلاس{$a->classname})';
$string['nonexistentformelements'] = 'تلاش برای اضاف کردن دکمهٔ راهنمایی به المان(های) غیر موجود در فرم: {$a}';
$string['noselection'] = 'چیزی انتخاب نشده';
$string['nosuggestions'] = 'پیشنهادی نیست';
$string['novalue'] = 'چیزی وارد نشد';
$string['novalueclicktoset'] = 'برای نوشتن متن کلیک کنید';
$string['optional'] = 'اختیاری';
$string['othersettings'] = 'سایر تنظیمات';
$string['passwordunmaskedithint'] = 'ویرایش رمز';
$string['passwordunmaskinstructions'] = 'برای ذخیرهٔ تغییرات کلید enter را فشار دهید';
$string['passwordunmaskrevealhint'] = 'نمایش دادن';
$string['requiredelement'] = 'لازم';
$string['security'] = 'امنیت';
$string['selectallornone'] = 'انتخاب همه/هیچکدام';
$string['selected'] = 'انتخاب شده';
$string['selecteditems'] = 'موارد انتخاب‌شده:';
$string['showadvanced'] = 'نمایش قسمت‌های پیشرفتهٔ فرم';
$string['showless'] = 'نمایش کمتر...';
$string['showmore'] = 'نمایش بیشتر...';
$string['somefieldsrequired'] = 'پر کردن قسمت‌هایی که با {$a} مشخص شده‌اند الزامی است.';
$string['time'] = 'زمان';
$string['timeunit'] = 'واحد زمان';
$string['timing'] = 'زمان‌بندی';
$string['unmaskpassword'] = 'نمایش رمز';
$string['year'] = 'سال';
