<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'adobeconnect', language 'fa', branch 'MOODLE_30_STABLE'
 *
 * @package   adobeconnect
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['assignroles'] = 'اختصاص نقش';
$string['duplicatemeetingname'] = 'يك نام مشابه بر روي سرور يافت شده است';
$string['duplicateurl'] = 'يك نام مشابه بر روي سرور يافت شده است';
$string['error1'] = 'شما بايد براي دسترسي به اين بخش مجوز لازم را داشته باشيد';
$string['errormeeting'] = 'اشكال در بازيابي فايل ركورد شده';
$string['errorrecording'] = 'جلسه ركورد شده يافت نمي شود';
$string['joinmeeting'] = 'پيوستن به كلاس';
$string['meetinfotxt'] = 'ديدن جزئيات سرور كلاس';
$string['meetingend'] = 'زمان پايان كلاس';
$string['meetinginfo'] = 'اطلاعات كلاس';
$string['meetingintro'] = 'خلاصه كلاس';
$string['meetingname'] = 'نام كلاس';
$string['meetingstart'] = 'زمان شروع كلاس';
$string['meeturl'] = 'آدرس كلاس';
