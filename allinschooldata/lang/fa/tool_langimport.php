<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_langimport', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_langimport
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['install'] = 'نصب بسته(های) زبانی انتخاب شده';
$string['installedlangs'] = 'بسته‌های زبانی نصب شده';
$string['langimport'] = 'ابزار دریافت زبان';
$string['langimportdisabled'] = 'قابلیت وارد کردن زبان غیرفعال شده است. باید بسته‌های زبانی‌تان را به‌طور دستی در سطح سیستم به‌روز کنید. فراموش نکنید که پس از این کار، cache های ترجمه را پاک‌سازی کنید.';
$string['langpackinstalled'] = 'بستهٔ زبانی «{$a}» با موفقیت نصب شد';
$string['langpackinstalledevent'] = 'بستهٔ زبانی نصب شد';
$string['langpackremoved'] = 'بستهٔ زبانی «{$a}» حذف شد';
$string['langpackremovedevent'] = 'بستهٔ زبانی حذف شد';
$string['langpackupdatedevent'] = 'بسته‌ زبانی به‌روز شد';
$string['langpackupdateskipped'] = 'به‌روزرسانی بستهٔ زبانی «{$a}» لغو شد';
$string['langupdatecomplete'] = 'به‌روزرسانی بستهٔ زبانی کامل شد';
$string['missinglangparent'] = 'Missing parent language <em>{$a->parent}</em> of <em>{$a->lang}</em>.';
$string['nolangupdateneeded'] = 'All your language packs are up to date, no update is needed';
$string['pluginname'] = 'بسته‌های زبانی';
$string['purgestringcaches'] = 'پاک‌سازی cache های ترجمه';
$string['uninstall'] = 'حذف بسته(های) زبانی انتخاب شده';
$string['uninstallconfirm'] = 'شما در آستانهٔ حذف کامل این بسته‌های زبانی هستید: <strong>{$a}</strong>. آیا مطمئنید؟';
$string['updatelangs'] = 'به‌روزرسانی همهٔ بسته‌های زبانی نصب شده';
