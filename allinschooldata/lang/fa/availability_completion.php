<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability_completion', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability_completion
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['description'] = 'نیاز به این است که شاگردان فعالیت دیگری را کامل کرده (یا نکرده) باشند.';
$string['error_selectcmid'] = 'برای شرط تکمیل باید یک فعالیت را انتخاب کنید.';
$string['label_cm'] = 'فعالیت یا منبع';
$string['missing'] = '(فعالیت حذف‌شده)';
$string['option_complete'] = 'باید به‌عنوان کامل‌شده علامت خورده باشد';
$string['option_fail'] = 'باید با کسب نمرهٔ مردود کامل شده باشد';
$string['option_incomplete'] = 'نباید به‌عنوان کامل‌شده علامت خورده باشد';
$string['option_pass'] = 'باید با کسب نمرهٔ قبولی کامل شده باشد';
$string['pluginname'] = 'محدودیت بر اساس تکمیل فعالیت';
$string['privacy:metadata'] = 'پلاگین محدودیت بر اساس تکمیل فعالیت هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['requires_complete'] = 'فعالیت <strong>{$a}</strong> به‌عنوان کامل‌شده علامت خورده باشد';
$string['requires_complete_fail'] = 'فعالیت <strong>{$a}</strong> کامل شده و نمرهٔ مردود در آن کسب شده باشد';
$string['requires_complete_pass'] = 'فعالیت <strong>{$a}</strong> کامل شده و نمرهٔ قبولی در آن کسب شده باشد';
$string['requires_incomplete'] = 'فعالیت <strong>{$a}</strong> کامل نشده باشد';
$string['requires_not_complete_fail'] = 'فعالیت <strong>{$a}</strong> کامل‌شده با نمرهٔ مردود نباشد';
$string['requires_not_complete_pass'] = 'فعالیت <strong>{$a}</strong> کامل‌شده با نمرهٔ قبولی نباشد';
$string['title'] = 'تکمیل فعالیت';
