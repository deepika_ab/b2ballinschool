<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_description', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_description
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'توضیح';
$string['pluginnameadding'] = 'درج یک توضیح';
$string['pluginnameediting'] = 'ویرایش یک توضیح';
$string['pluginname_help'] = '«توضیح» در واقع یک نوع سؤال نیست. بلکه امکان نمایش یک متن بدون نیاز به جواب دادن به آن (مانند برچسب در صفحهٔ درس) را فراهم می‌کند.

متن سؤال هم در حین شرکت در آزمون و هم در صفحهٔ مرور نمایش داده می‌شود. بازخوردهای عمومی فقط در صفحهٔ مرور نشان داده می‌شوند.';
$string['pluginnamesummary'] = '«توضیح» در واقع یک سؤال نیست. بلکه راهی است برای اضافه کردن یک سری دستورالعمل، راهنمایی، عنوان یا سایر مطالب به صفحهٔ آزمون. در واقع مشابه با استفاده از برچسب‌ها برای درج متن در صفحهٔ درس.';
