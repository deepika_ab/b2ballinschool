<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'media', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   media
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['audioextensions'] = 'صوتی: {$a}';
$string['defaultheight'] = 'ارتفاع پیش‌فرض';
$string['defaultheightdesc'] = 'ارتفاع پخش‌کنندهٔ صوتی-تصویری در صورتی که ارتفاع تعیین نشده باشد و ارتفاع واقعی فایل صوتی-تصویری توسط پخش‌کننده قابل تشخصی نباشد.';
$string['defaultwidth'] = 'عرض پیش‌فرض';
$string['defaultwidthdesc'] = 'عرض پخش‌کنندهٔ صوتی-تصویری در صورتی که عرض تعیین نشده باشد و عرض واقعی فایل صوتی-تصویری توسط پخش‌کننده قابل تشخصی نباشد.';
$string['extensions'] = 'پسوندها: {$a}';
$string['managemediaplayers'] = 'مدیریت پخش‌کننده‌های صوتی-تصویری';
$string['mediaformats'] = 'پخش‌کننده‌های موجود';
$string['mediaformats_desc'] = 'اگر پخش‌کننده‌هایی فعال شده باشند، آنگاه می‌توان فایل‌های چندرسانه‌ای را به مدد پلاگین فیلتر چندرسانه‌ای (اگر فعال باشد) یا با استفاده از منبع از نوع «فایل» یا «آدرس اینترنتی» جاسازی کرد. اگر فعال نباشد، این فرمت‌ها جاسازی نمی‌شوند و کاربران می‌توانند فایل را دانلود کنند یا اینکه پیوند مربوط به این منابع را دنبال کنند.

اگر فرمتی توسط بیش از یک پخش‌کننده پشتیبانی شود،‌ فعال‌کردن هر دو پخش‌کننده باعث افزایش سازگاری با دستگاه‌های مختلف (مانند تلفن‌های همراه) خواهد شد. با فراهم‌کردن یک فایل صوتی یا ویدیویی یکسان به‌صورت چند فایل با فرمت‌های مختلف می‌توان سازگاری را بیشتر هم کرد.';
$string['supports'] = 'پشتیبانی می‌کند';
$string['videoextensions'] = 'تصویری: {$a}';
