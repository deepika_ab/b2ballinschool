<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'dbtransfer', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   dbtransfer
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['checkingsourcetables'] = 'بررسی ساختار جدول مبدأ';
$string['copyingtable'] = 'کپی کردن جدول {$a}';
$string['copyingtables'] = 'کپی کردن محتویات جدول';
$string['creatingtargettables'] = 'ایجاد جدول‌ها در پایگاه دادهٔ مقصد';
$string['dbexport'] = 'صدور پایگاه داده';
$string['dbtransfer'] = 'انتقال پایگاه داده';
$string['differenttableexception'] = 'ساختار جدول {$a} مطابقت ندارد.';
$string['done'] = 'انجام شد';
$string['exportschemaexception'] = 'ساختار فعلی پایگاه داده با تمام فایل‌های install.xml منطبق نیست. <br /> {$a}';
$string['importschemaexception'] = 'ساختار فعلی پایگاه داده با تمام فایل‌های install.xml منطبق نیست. <br /> {$a}';
$string['importversionmismatchexception'] = 'نسخهٔ فعلی ({$a->currentver}) با نسخهٔ فایل صادره ({$a->schemaver}) منطبق نیست.';
$string['malformedxmlexception'] = 'قالب این XML دارای اشکال است و نمی‌توان ادامه داد.';
$string['tablex'] = 'جدول {$a}:';
$string['unknowntableexception'] = 'جدول ناشناس {$a} در فایل صدور پیدا شد.';
