<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'label', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   label
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['label:addinstance'] = 'اضافه‌کردن یک برچسب جدید';
$string['labeltext'] = 'متن برچسب';
$string['modulename'] = 'برچسب';
$string['modulename_help'] = 'با استفاده از برچسب می‌توان متون و عکس‌هایی را در کنار پیوندهای فعالیت‌ها در صفحهٔ درس درج کرد. برچسب‌ها همه‌کاره هستند و اگر به‌دقت استفاده شوند می‌توانند نمای ظاهری درس را بهتر کنند.

برای کاربرد برچسب‌ها می‌توان به موارد زیر اشاره کرد:

* برای جدا کردن یک لیست طولانی از فعالیت‌ها با یک عنوان یا یک عکس
* برای نمایش مستقیم یک فایل صوتی یا ویدئویی به‌صورت جاسازی‌شده در درس
* برای اضافه‌کردن یک توصیف مختصر به یک قسمت در صفحهٔ اصلی درس';
$string['modulenameplural'] = 'برچسب‌ها';
$string['pluginadministration'] = 'مدیریت برچسب';
$string['pluginname'] = 'برچسب';
$string['search:activity'] = 'برچسب';
