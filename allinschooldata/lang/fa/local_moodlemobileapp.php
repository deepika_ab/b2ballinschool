<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_moodlemobileapp', language 'fa', branch 'MOODLE_26_STABLE'
 *
 * @package   local_moodlemobileapp
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addon.calendar.calendarevents'] = 'رویدادهای تقویم';
$string['addon.calendar.defaultnotificationtime'] = 'زمان پیش‌فرض اطلاع‌رسانی';
$string['addon.calendar.errorloadevent'] = 'خطا در بارگیری رویداد.';
$string['addon.calendar.errorloadevents'] = 'خطا در بارگیری رویدادها.';
$string['addon.calendar.noevents'] = 'هیچ رویدادی نیست';
$string['addon.competency.errornocompetenciesfound'] = 'هیچ شایستگی‌ای پیدا نشد';
$string['addon.competency.nocompetencies'] = 'بدون شایستگی';
$string['addon.coursecompletion.complete'] = 'کامل شد';
$string['addon.coursecompletion.couldnotloadreport'] = 'امکان بارگذاری گزارش تکمیل فعالیت درس نیست. لطفا دوباره بعداً تلاش کنید';
$string['addon.files.couldnotloadfiles'] = 'لیست فایل‌ها نمی‌تواند باگذاری شود.';
$string['addon.files.emptyfilelist'] = 'هیچ فایلی برای نمایش وجود ندارد.';
$string['addon.messageoutput_airnotifier.processorsettingsdesc'] = 'پیکربندی دستگاه‌ها';
$string['addon.messages.contactlistempty'] = 'لیست مخاطبین خالی است';
$string['addon.messages.contactname'] = 'نام مخاطب';
$string['addon.messages.deletemessage'] = 'حذف پیغام';
$string['addon.messages.errorwhileretrievingdiscussions'] = 'خطا در دریافت مباحثه‌ها از کارگزار.';
$string['addon.messages.messagenotsent'] = 'پیغام ارسال نشد. لطفا دوباره بعداً تلاش کنید';
$string['addon.messages.newmessages'] = 'پیغام جدید';
$string['addon.messages.nousersfound'] = 'هیچ کاربری پیدا نشد';
$string['addon.messages.showdeletemessages'] = 'نمایش گزینهٔ حذف پیغام‌ها';
$string['addon.messages.type_blocked'] = 'مسدود شده';
$string['addon.messages.type_offline'] = 'آفلاین';
$string['addon.messages.type_online'] = 'آنلاین';
$string['addon.messages.type_search'] = 'نتایج جستجو';
$string['addon.messages.type_strangers'] = 'دیگران';
$string['addon.messages.warningmessagenotsent'] = 'امکان ارسال پیغام به کاربر {{user}} وجود ندارد. {{error}}';
$string['addon.mod_assign.acceptsubmissionstatement'] = 'لطفا پذیریش بیانیه‌ی تحویل تکلیف را تایید کنید.';
$string['addon.mod_assign.gradenotsynced'] = 'نمره هم‌گام‌ نشد';
$string['addon.mod_chat.errorwhileconnecting'] = 'خطا در هنگام اتصال به چت';
$string['addon.mod_chat.errorwhilegettingchatdata'] = 'خطا در هنگام دریافت داده‌های چت';
$string['addon.mod_chat.errorwhilegettingchatusers'] = 'خطا در هنگام دریافت کاربران چت';
$string['addon.mod_chat.errorwhileretrievingmessages'] = 'خطا در هنگام بازیابی پیغام‌ها از سرور';
$string['addon.mod_chat.errorwhilesendingmessage'] = 'خطا در هنگام ارسال پیغام';
$string['addon.mod_chat.mustbeonlinetosendmessages'] = 'شما برای ارسال پیغام، باید آنلاین باشید';
$string['addon.mod_choice.errorgetchoice'] = 'خطا در دریافت داده‌های ماژول انتخاب';
$string['addon.mod_choice.responsesresultgraphdescription'] = '{{number}}% از کاربران گزینه  : {{text}} انتخاب کرده‌اند';
$string['addon.mod_choice.resultsnotsynced'] = 'پاسخ آخر شما قبل از اینکه در نتایج لحاظ گردد باید هم‌گام‌سازی شود.';
$string['addon.mod_data.errordeleting'] = 'خطا در حذف کردن داده‌های ورودی';
$string['addon.mod_feedback.feedback_submitted_offline'] = 'این بازخورد ذخیره شد تا بعداً ثبت شود';
$string['addon.mod_folder.emptyfilelist'] = 'هیچ فایلی برای نمایش وجود ندارد';
$string['addon.mod_forum.errorgetforum'] = 'خطا در دریافت داده‌های تالارگفتگو';
$string['addon.mod_forum.errorgetgroups'] = 'خطا در دریافت تنظیمات گروه';
$string['addon.mod_forum.forumnodiscussionsyet'] = 'هنوز هیچ مباحثه ای در این تالار شروع نشده است.';
$string['addon.mod_forum.group'] = 'گروه';
$string['addon.mod_forum.numdiscussions'] = '{{numdiscussions}} مباحثه';
$string['addon.mod_forum.numreplies'] = '{{numreplies}} پاسخ';
$string['addon.mod_forum.refreshdiscussions'] = 'تازه‌سازی مباحثه‌ها';
$string['addon.mod_forum.refreshposts'] = 'تازه‌سازی مطالب مباحثه';
$string['addon.mod_glossary.byalphabet'] = 'الفبایی';
$string['addon.mod_glossary.byauthor'] = 'گروه بر اساس نویسنده';
$string['addon.mod_glossary.bycategory'] = 'گروه بر اساس دسته';
$string['addon.mod_glossary.bynewestfirst'] = 'جدیدترین اول';
$string['addon.mod_glossary.byrecentlyupdated'] = 'اخیرا به‌روز شد';
$string['addon.mod_glossary.bysearch'] = 'جستجو';
$string['addon.mod_glossary.cannoteditentry'] = 'ویرایش داده ورودی امکان ندارد';
$string['addon.mod_imscp.showmoduledescription'] = 'نمایش توضیحات';
$string['addon.mod_quiz.confirmleavequizonerror'] = 'در حین ذخیرهٔ پاسخ‌ها خطایی رخ داد. آیا مطمئن هستید که می‌خواهید از آزمون خارج شوید؟';
$string['addon.mod_resource.openthefile'] = 'باز کردن فایل';
$string['addon.mod_scorm.cannotcalculategrade'] = 'نمرات نمی‌توانند محاسبه شوند';
$string['addon.mod_scorm.errorinvalidversion'] = 'متاسفانه برنامه تنها از اسکورم ۱٫۲ پشتیبانی می‌کند.';
$string['addon.mod_scorm.errorpackagefile'] = 'متاسفانه برنامه تنها از بسته‌های ZIP پشتیبانی می‌کند.';
$string['addon.mod_survey.cannotsubmitsurvey'] = 'متاسفیم. مشکلی در ثبت نظرسنجی شما وجود داشت. لطفا دوباره تلاش کنید.';
$string['addon.mod_survey.results'] = 'نتایج';
$string['addon.mod_wiki.viewpage'] = 'مشاهدهٔ صفحه';
$string['addon.notes.userwithid'] = 'کاربر با  ID {{id}}';
$string['addon.notifications.notifications'] = 'هشدارها';
$string['addon.notifications.playsound'] = 'پخش صدا';
$string['addon.notifications.therearentnotificationsyet'] = 'هیچ هشداری وجود ندارد';
$string['appstoredescription'] = '*توجه:* این نسخهٔ رسمی برنامهٔ موبایل مودل *تنها* با سایت‌های مودلی که استفاده از برنامه را مجاز کرده باشند کار خواهد کرد. لطفا چنانچه در اتصال مشکل دارید، با مدیر سایت مودل خود تماس بگیرید.

اگر سایت مودل شما به‌درستی پیکربندی شده باشد، از این برنامه می‌توانید برای:

- مرور محتواهای درس‌هایتان حتی در هنگامی که به اینترنت متصل نیستید
- دریافت فوری اطلاعیه‌های مربوط به پیام‌ها و سایر رویدادها
- پیدا کردن و تماس سریع با سایر افراد در درس‌هایتان
- بارگذاری تصویر، صدا، فیلم و سایر فایل‌ها از روی دستگاه موبایلتان
- مشاهدهٔ نمرات درس‌هایتان
- و موارد بیشتر!
استفاده کنید.

برای مشاهدهٔ اطلاعات به‌روز، لطفا http://docs.moodle.org/fa/Mobile_app را ببینید.

ما حقیقتا از بازخوردهای مثبتی دربارهٔ عملکرد برنامه تا به اینجا دریافت کرده‌ایم، و همچنین پیشنهادهای شما دربارهٔ اینکه می‌خواهید این برنامه چه کارهای دیگری انجام دهد  استقبال می‌کنیم.

این برنامه به مجوزهای زیر نیاز دارد:
ضبط صدا - برای ضبط‌کردن صدا برای بارگذاری در مودل
خواندن و تغییر دادن محتوای کارت حافظه - محتواها بر روی کارت حافظه ذخیره می‌شود تا بتوانید به‌صورت آفلاین به آنها دسترسی داشته باشید
دسترسی به شبکه - برای اینکه بتوان به سایت مودل شما متصل شد و برای بررسی اینکه آیا شما متصل هستید یا خیر برای سوییچ‌کردن به حالت آفلاین
اجرا در شروع - تا بتوانید اطلاعیه‌های محلی را حتی در هنگامی که برنامه در پس‌زمینه در حال اجرا است دریافت کنید
جلوگیری از به خواب رفتن گوشی - تا بتوانید اطلاعیه‌های ارسالی به گوشی را در هر زمانی دریافت کنید';
$string['core.android'] = 'اندروید';
$string['core.cannotconnect'] = 'اتصال به سایت ممکن نبود. بررسی کنید که نشانی سایت را درست وارد کرده باشید و اینکه سایت شما از مودل ۲٫۴ یا جدیدتر استفاده کند.';
$string['core.captureaudio'] = 'ضبط صدا';
$string['core.capturedimage'] = 'عکس گرفته‌شده.';
$string['core.captureimage'] = 'گرفتن عکس';
$string['core.capturevideo'] = 'ضبط ویديو';
$string['core.clearsearch'] = 'پاک کردن جستجو';
$string['core.confirmcanceledit'] = 'آیا مطمئنید که می‌خواهید این صفحه را ترک کنید؟ تمام تغییرات از بین خواهند رفت.';
$string['core.confirmloss'] = 'آیا مطمئن هستید؟ تمام تغییرات از بین خواهند رفت.';
$string['core.course.allsections'] = 'تمام قسمت‌ها';
$string['core.course.confirmdeletemodulefiles'] = 'آیا مطمئنید که می‌خواهید فایل‌های این ماژول را پاک کنید؟';
$string['core.course.confirmdownload'] = 'شما در آستانهٔ دریافت {{size}} هستید. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['core.course.confirmdownloadunknownsize'] = 'ما نتوانستیم حجم دریافت را محاسبه کنیم. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['core.course.confirmpartialdownloadsize'] = 'شما در آستانهٔ دریافت <strong>حداقل</strong> {{size}} هستید. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['core.course.contents'] = 'محتواها';
$string['core.course.couldnotloadsectioncontent'] = 'محتوای قسمت نتوانست بارگیری شود. لطفا بعدا دوباره تلاش کنید.';
$string['core.course.couldnotloadsections'] = 'قسمت‌ها نتوانستند بارگیری شوند. لطفا بعدا دوباره تلاش کنید.';
$string['core.courses.confirmselfenrol'] = 'آیا مطمئنید که می‌خواهید خود را در این درس ثبت‌نام کنید؟';
$string['core.courses.errorloadcourses'] = 'در هنگام بارگیری درس‌ها خطایی رخ داد.';
$string['core.courses.filtermycourses'] = 'پالایش درس‌های من';
$string['core.course.useactivityonbrowser'] = 'البته همچنان می‌توانید با استفاده از مرورگر دستگاه خود، از آن استفاده کنید.';
$string['core.currentdevice'] = 'دستگاه فعلی';
$string['core.datastoredoffline'] = 'ارسال اطلاعات ناموفق بود. اطلاعات روی دستگاه ذخیره شد و بعدا به‌طور خودکار فرستاده خواهد شد.';
$string['core.errordownloading'] = 'خطا در دانلود فایل';
$string['core.fileuploader.confirmuploadfile'] = 'شما در آستانهٔ ارسال {{size}} هستید. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['core.fileuploader.confirmuploadunknownsize'] = 'ما نتوانستیم حجم ارسال را محاسبه کنیم. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['core.fileuploader.errorcapturingaudio'] = 'خطا در ضبط صدا';
$string['core.fileuploader.errorcapturingvideo'] = 'خطا در ضبط ویدئو';
$string['core.fileuploader.fileuploaded'] = 'فایل آپلود شده';
$string['core.fileuploader.uploading'] = 'درحال بارگذاری';
$string['core.fileuploader.video'] = 'ویدئو';
$string['core.imageviewer'] = 'نمایشگر تصویر';
$string['core.login.authenticating'] = 'در حال شناسایی';
$string['core.login.confirmdeletesite'] = 'آیا مطمئن هستید که می‌خواهید سایت {{sitename}} را پاک کنید؟';
$string['core.login.connecttomoodle'] = 'اتصال به مودل';
$string['core.login.invalidaccount'] = 'لطفا اطلاعات ورودخود را بررسی نمایید';
$string['core.login.invalidmoodleversion'] = 'این نسخه از برنامه سایت قابلیت اتصال به موبایل ندارد';
$string['core.login.invalidsite'] = 'نشانی سایت معتبر نیست';
$string['core.login.logininsiterequired'] = 'لازم است به سایت از طریق مرورگر متصل گردید';
$string['core.login.mobileservicesnotenabled'] = 'سرویس دسترسی موبایل در سایت شما فعال نیست';
$string['core.login.passwordrequired'] = 'گذرواژه مورد نیاز است';
$string['core.login.siteaddress'] = 'آدرس سایت';
$string['core.login.siteinmaintenance'] = 'سایت شما در حالت نگهداشت می باشد';
$string['core.login.siteurl'] = 'نشانی سایت';
$string['core.login.siteurlrequired'] = 'نشانی سایت لازم است. مثال: <i>http://www.yourmoodlesite.abc یا https://www.yourmoodlesite.efg</i>';
$string['core.login.usernamerequired'] = 'نام کاربر لازم است';
$string['core.login.webservicesnotenabled'] = 'وب سرویس سایت اشکال دارد.با مدیر سایت هماهنگ نمایید';
$string['core.lostconnection'] = 'اطلاعات توکن شناسایی شما معتبر نیست یا منقضی شده است. باید دوباره به سایت متصل شوید.';
$string['core.mainmenu.appsettings'] = 'تنظیمات برنامه';
$string['core.mainmenu.website'] = 'پایگاه اینترنتی';
$string['core.networkerrormsg'] = 'شبکه قابل دسترسی نیست';
$string['core.openinbrowser'] = 'باز کردن در مرورگر';
$string['core.pulltorefresh'] = 'برای تازه‌سازی بکشید';
$string['core.settings.about'] = 'درباره';
$string['core.settings.cordovadevicemodel'] = 'مدل دستگاه کُردوا';
$string['core.settings.deletesitefiles'] = 'آیا از حذف کلیه فایل‌های این سایت مطمئنید؟';
$string['core.settings.deletesitefilestitle'] = 'پاک‌کردن فایل‌های سایت';
$string['core.settings.deviceinfo'] = 'اطلاعات دستگاه';
$string['core.settings.deviceos'] = 'سیستم‌عامل دستگاه';
$string['core.settings.enabledownloadsection'] = 'فعال‌کردن دریافت قسمت‌ها';
$string['core.settings.enablerichtexteditor'] = 'فعال‌کردن ویرایشگر متنی پیشرفته';
$string['core.settings.enablerichtexteditordescription'] = 'اگر فعال باشد، در جاهایی که ممکن باشد یک ویرایشگر متنی پیشرفته نمایش داده خواهد شد.';
$string['core.settings.estimatedfreespace'] = 'فضای آزاد تخمینی';
$string['core.settings.privacypolicy'] = 'خط مشی حفظ حریم خصوصی';
$string['core.settings.reportinbackground'] = 'گزارش خطاها به‌صورت خودکار';
$string['core.settings.spaceusage'] = 'فضای مورد استفاده';
$string['core.settings.synchronization'] = 'هماهنگسازی';
$string['core.sorry'] = 'متاسفیم...';
$string['core.unexpectederror'] = 'خطای غیرمنتظره.لطفا برنامه را ببندید و دوباره اجرا نمایید';
$string['core.user.contact'] = 'مخاطب';
$string['pluginname'] = 'عبارت‌های ترجمه زبان در نسخه موبایل مودل';
