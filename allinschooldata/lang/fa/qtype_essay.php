<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_essay', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_essay
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['acceptedfiletypes'] = 'انواع فایل‌های مورد پذیرش';
$string['acceptedfiletypes_help'] = 'پذیریش انواع فایل‌ها می‌تواند با وارد کرد لیست پسوندها فایل‌ها محدود شود. اگر این قسمت خالی باشد، آنگاه همه انواع فایل‌ها مجاز است';
$string['allowattachments'] = 'مجاز بودن ضمیمه کردن فایل';
$string['formateditor'] = 'ویرایشگر HTML';
$string['formateditorfilepicker'] = 'ویرایشگر HTML با انتخاب کننده فایل';
$string['formatmonospaced'] = 'متن ساده، با فونت فاصله ثابت';
$string['formatplain'] = 'متن ساده';
$string['graderinfo'] = 'توضیحات مربوط به مصححان';
$string['nlines'] = '{$a} خط';
$string['pluginname'] = 'تشریحی';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال تشریحی';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال تشریحی';
$string['pluginname_help'] = 'در پاسخ به این سؤال (که می‌تواند شامل یک عکس باشد)، پاسخ دهنده پاسخی شامل یک یا دو پاراگراف را وارد می‌کند. نمرهٔ سؤال تشریحی به صورت خودکار محاسبه نمی‌شود، بلکه پاسخ آن باید توسط یک استاد مرور شود و سپس به صورت دستی به آن نمره داده شود.';
$string['pluginnamesummary'] = 'اجازهٔ ورود پاسخی شامل چند جمله یا پاراگراف را می‌دهد. این پاسخ باید بعدا به صورت دستی نمره داده شود.';
$string['responsefieldlines'] = 'اندازه جعبه ورود متن';
$string['responseformat'] = 'قالب پاسخ';
