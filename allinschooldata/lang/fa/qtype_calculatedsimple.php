<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_calculatedsimple', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_calculatedsimple
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['atleastonewildcard'] = 'در فرمول پاسخ‌های صحیح باید حداقل از یک متغیر <strong>{x..}</strong> استفاده شود.';
$string['converttocalculated'] = 'ذخیره به عنوان یک سؤال محاسباتی ساده جدید';
$string['findwildcards'] = 'پیدا کردن متغیرهای {x..} موجود در فرمول پاسخ‌های صحیح';
$string['generatenewitemsset'] = 'تولید';
$string['newsetwildcardvalues'] = 'مجموعه جدید از مقادیر برای متغیرها';
$string['pluginname'] = 'محاسباتی ساده';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال محاسباتی ساده';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال محاسباتی ساده';
$string['pluginname_help'] = 'با استفاده از سؤال‌های محاسباتی ساده، می‌توان سؤال‌های عددی‌ای ساخت که دارای متغیرهایی باشند که در هنگام برگزاری آزمون با مقادیر عددی جایگزین می‌شوند. سؤال‌های محاسباتی ساده، قابلیت‌های پر استفاده‌تر سؤال‌های محاسباتی را به صورت ساده‌تری با استفاده از رابط کاربری ساده‌تری برای ساختن سؤال در اختیار قرار می‌دهند.';
$string['pluginnamesummary'] = 'نسخهٔ ساده شدهٔ سؤال‌های محاسباتی که شبیه سؤال‌های عددی است با این تفاوت که عددهای استفاده شده در متن سؤال، در هنگام نمایش سؤال، به صورت تصادفی از بین یک مجموعه انتخاب می‌شوند.';
$string['setno'] = 'مجموعهٔ {$a}';
$string['setwildcardvalues'] = 'مجموعه مقدار برای متغیرها';
$string['showitems'] = 'نمایش';
$string['updatewildcardvalues'] = 'به‌روزرسانی مقادیر متغیرها';
$string['useadvance'] = 'برای دیدن خطاها از دکمهٔ پیشرفته استفاده کنید';
$string['wildcard'] = 'متغیر {<strong>{$a}</strong>}';
$string['wildcardparam'] = 'پارامترهای مورد استفاده برای تولید مقادیر جایگزین متغیرها';
$string['wildcardrole'] = 'متغیرهای <strong>{x..}</strong> با یک مقدار عددی از بین مقادیر تولید شده جایگزین خواهند شد.';
$string['wildcardvalues'] = 'مقادیر متغیر(ها)';
$string['wildcardvaluesgenerated'] = 'مقادیر متغیر(ها) تولید شدند';
$string['willconverttocalculated'] = 'در صورت انتخاب، با فشردن دکمهٔ <strong>ذخیره به عنوان یک سؤال جدید</strong>، این سؤال به صورت یک سؤال جدید محاسباتی ذخیره خواهد شد';
$string['youmustaddatleastonevalue'] = 'برای اینکه بتوانید این سؤال را ذخیره کنید، باید حداقل یک مجموعه از مقادیر برای متغیرها اضافه کنید.';
