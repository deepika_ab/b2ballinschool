<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability_profile', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability_profile
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['conditiontitle'] = 'مشخصهٔ فردی کاربر';
$string['description'] = 'کنترل دسترسی براساس مشخصه‌های فردی شاگرد.';
$string['error_selectfield'] = 'باید یک مشخصهٔ فردی را انتخاب کنید.';
$string['error_setvalue'] = 'باید مقداری را وارد کنید.';
$string['label_operator'] = 'شیوهٔ مقایسه';
$string['label_value'] = 'مقداری که با آن مقایسه شود';
$string['missing'] = '(مشخصهٔ اضافی حذف‌شده: {$a})';
$string['op_contains'] = 'شامل';
$string['op_doesnotcontain'] = 'شامل نشود';
$string['op_endswith'] = 'خاتمه یابد به';
$string['op_isempty'] = 'خالی باشد';
$string['op_isequalto'] = 'برابر باشد با';
$string['op_isnotempty'] = 'خالی نباشد';
$string['op_startswith'] = 'شروع شود با';
$string['pluginname'] = 'محدودیت بر اساس مشخصه فردی';
$string['privacy:metadata'] = 'پلاگین محدودیت بر اساس مشخصات فردی هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['requires_contains'] = '<strong>{$a->field}</strong> شما شامل <strong>{$a->value}</strong> باشد';
$string['requires_doesnotcontain'] = '<strong>{$a->field}</strong> شما شامل <strong>{$a->value}</strong> نباشد';
$string['requires_endswith'] = '<strong>{$a->field}</strong> شما به <strong>{$a->value}</strong> ختم شود';
$string['requires_isempty'] = '<strong>{$a->field}</strong> شما خالی باشد';
$string['requires_isequalto'] = '<strong>{$a->field}</strong> شما برابر با <strong>{$a->value}</strong> باشد';
$string['requires_isnotempty'] = '<strong>{$a->field}</strong> شما خالی نباشد';
$string['requires_notendswith'] = '<strong>{$a->field}</strong> شما به <strong>{$a->value}</strong> ختم نشود';
$string['requires_notisequalto'] = '<strong>{$a->field}</strong> شما برابر با <strong>{$a->value}</strong> نباشد';
$string['requires_notstartswith'] = '<strong>{$a->field}</strong> شما با <strong>{$a->value}</strong> شروع نشود';
$string['requires_startswith'] = '<strong>{$a->field}</strong> شما با <strong>{$a->value}</strong> شروع شود';
$string['title'] = 'مشخصهٔ فردی کاربر';
