<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_grading', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   quiz_grading
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alreadygraded'] = 'نمره داده شده';
$string['alsoshowautomaticallygraded'] = 'سؤال‌هایی که به‌صورت خودکار نمره داده شده‌اند هم نمایش داده شوند';
$string['attemptstograde'] = 'تلاش‌هایی که باید نمره‌دهی شوند';
$string['automaticallygraded'] = 'به‌طور خودکار نمره داده شده';
$string['backtothelistofquestions'] = 'بازگشت به لیست سؤال‌ها';
$string['bydate'] = 'بر اساس تاریخ';
$string['bystudentfirstname'] = 'بر اساس نام شاگرد';
$string['bystudentidnumber'] = 'بر اساس کد شناسایی شاگرد';
$string['bystudentlastname'] = 'بر اساس نام خانوادگی شاگرد';
$string['changeoptions'] = 'تغییر اختیارات';
$string['essayonly'] = 'سؤال‌های زیر باید به صورت دستی نمره‌دهی شوند';
$string['grade'] = 'نتیجه';
$string['gradeall'] = 'نمره‌دهی تمام تلاش‌ها';
$string['gradeattemptsautograded'] = 'آنهایی که به‌طور خودکار نمره داده شده‌اند ({$a})';
$string['gradeattemptsmanuallygraded'] = 'آنهایی که قبلا به‌طور دستی نمره داده شده‌اند ({$a})';
$string['gradeattemptsneedsgrading'] = 'آنهایی که نیاز به نمره‌دهی دارند ({$a})';
$string['graded'] = '(نمره‌دهی شده است)';
$string['gradenextungraded'] = 'نمره‌دهی {$a} دفعه تلاش بعدی بدون نمره';
$string['gradeungraded'] = 'نمره‌دهی همهٔ {$a} دفعهٔ بدون نمره';
$string['grading'] = 'نمره‌دهی دستی';
$string['gradingall'] = 'همهٔ {$a} دفعه تلاش صورت گرفته روی این سؤال.';
$string['gradingattempt'] = 'دفعهٔ {$a->attempt} ام از تلاش‌های {$a->fullname}.';
$string['gradingattemptsxtoyofz'] = 'نمره دادن به تلاش‌های {$a->from} تا {$a->to} از {$a->of}';
$string['gradingnextungraded'] = '{$a} تلاش بدون نمرهٔ بعدی';
$string['gradingnotallowed'] = 'شما مجوز نمره‌دهی دستی پاسخ‌ها در این آزمون را ندارید';
$string['gradingquestionx'] = 'نمره‌دادن به سؤال {$a->number}: {$a->questionname}';
$string['gradingreport'] = 'گزارش نمره‌دهی دستی';
$string['gradingungraded'] = '{$a} دفعه شرکت بدون نمره';
$string['gradinguser'] = 'دفعات شرکت {$a}';
$string['grading:viewidnumber'] = 'دیدن کد شناسایی کاربران در هنگام نمره دادن';
$string['grading:viewstudentnames'] = 'دیدن نام کاربران در هنگام نمره دادن';
$string['hideautomaticallygraded'] = 'سؤال‌هایی که به‌صورت خودکار نمره داده شده‌اند پنهان شوند';
$string['nothingfound'] = 'چیزی برای نمایش وجود ندارد';
$string['options'] = 'اختیارات';
$string['orderattempts'] = 'ترتیب تلاش‌ها';
$string['pluginname'] = 'نمره‌دهی دستی';
$string['questionname'] = 'نام سؤال';
$string['questionsperpage'] = 'تعداد سؤال در هر صفحه';
$string['questionsthatneedgrading'] = 'سؤال‌هایی که نیاز به نمره‌دهی دارند';
$string['questiontitle'] = 'سؤال {$a->number} : «{$a->name}» ({$a->openspan}{$a->gradedattempts}{$a->closespan} دفعه از کل {$a->totalattempts} دفعه تلاش {$a->openspan}نمره‌دهی شده است{$a->closespan}).';
$string['randomly'] = 'تصادفی';
$string['saveandnext'] = 'ذخیره و رفتن به صفحهٔ بعد';
$string['tograde'] = 'باید نمره داده شود';
$string['total'] = 'مجموع';
$string['updategrade'] = 'به‌روز کردن نمرات';
