<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshop', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   workshop
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aggregategrades'] = 'محاسبهٔ مجدد نمره‌ها';
$string['aggregation'] = 'اجتماع نمره‌ها';
$string['allocate'] = 'اختصاص برای بازبینی';
$string['allocatedetails'] = 'مورد انتظار: {$a->expected}<br />ارائه شده: {$a->submitted}<br />در انتظار اختصاص: {$a->allocate}';
$string['allocation'] = 'تخصیص برای بازبینی';
$string['allocationdone'] = 'تخصیص انجام شد';
$string['allocationerror'] = 'تخصیص با خطا مواجه شد';
$string['allowedfiletypesforsubmission'] = 'انواع فایل‌های مجاز برای ضمیمه‌شدن به تحویل‌ها';
$string['allowedfiletypesforsubmission_help'] = 'انواع فایل‌هایی که می‌توان به تحویل‌ها ضمیمه کرد را می‌توان با وارد کردن لیستی از پسوندهای مجاز (که با کاما از یکدیگر جدا شده‌اند؛ به‌طور مثال: «mp4, mp3, png, jpg») محدود کرد. اگر این لیست خالی باشد، آنگاه تمام انواع فایل‌ها مجاز هستند.';
$string['allsubmissions'] = 'تمام کارهای ارائه شده';
$string['alreadygraded'] = 'نمره داده شده است';
$string['areainstructauthors'] = 'دستورالعمل چگونگی تحویل دادن';
$string['areainstructreviewers'] = 'دستورالعمل چگونگی ارزشیابی';
$string['areasubmissionattachment'] = 'پیوست‌های کار';
$string['areasubmissioncontent'] = 'متن کار';
$string['assess'] = 'ارزشیابی';
$string['assessedexample'] = 'نمونه کار مورد ارزشیابی';
$string['assessedsubmission'] = 'کار مورد ارزشیابی';
$string['assessingexample'] = 'ارزشیابی نمونه کار';
$string['assessingsubmission'] = 'ارزشیابی کار ارائه شده';
$string['assessment'] = 'ارزشیابی';
$string['assessmentby'] = 'توسط <a href="{$a->url}">{$a->name}</a>';
$string['assessmentbyfullname'] = 'ارزشیابی توسط {$a}';
$string['assessmentbyyourself'] = 'ارزشیابی شما';
$string['assessmentdeleted'] = 'مسئولیت ارزشیابی برداشته شد';
$string['assessmentend'] = 'مهلت ارزشیابی';
$string['assessmentenddatetime'] = 'مهلت ارزشیابی: {$a->daydatetime} ({$a->distanceday})';
$string['assessmentform'] = 'فرم ارزشیابی';
$string['assessmentofsubmission'] = '<a href="{$a->assessmenturl}">ارزشیابی</a> کردن <a href="{$a->submissionurl}">{$a->submissiontitle}</a>';
$string['assessmentreference'] = 'ارزشیابی مرجع';
$string['assessmentreferenceneeded'] = 'به منظور فراهم کردن یک ارزشیابی مرجع، باید این نمونه کار را ارزشیابی کنید. برای ارزشیابی، روی دکمهٔ «ادامه» کلیک کنید.';
$string['assessmentsettings'] = 'تنظیمات ارزشیابی';
$string['assessmentstart'] = 'آزاد بودن برای ارزشیابی از';
$string['assessmentstartdatetime'] = 'از {$a->daydatetime} ({$a->distanceday}) برای ارزشیابی باز است';
$string['assessmentweight'] = 'وزن ارزشیابی';
$string['assignedassessments'] = 'کارهای محول شده برای ارزشیابی';
$string['assignedassessmentsnone'] = 'کاری برای ارزشیابی به شما محول نشده است';
$string['backtoeditform'] = 'بازگشت به فرم ویرایش';
$string['byfullname'] = 'توسط <a href="{$a->url}">{$a->name}</a>';
$string['calculategradinggrades'] = 'محاسبهٔ نمرهٔ ارزشیابی‌ها';
$string['calculategradinggradesdetails'] = 'مورد انتظار: {$a->expected}<br />محاسبه شده: {$a->calculated}';
$string['calculatesubmissiongrades'] = 'محاسبهٔ نمرهٔ کارهای ارائه شده';
$string['calculatesubmissiongradesdetails'] = 'مورد انتظار: {$a->expected}<br />محاسبه شده: {$a->calculated}';
$string['chooseuser'] = 'انتخاب کاربر...';
$string['clearaggregatedgrades'] = 'پاک کردن همهٔ نمره‌های جمع شده';
$string['clearaggregatedgradesconfirm'] = 'آیا مطمئنید که می‌خواهید نمره‌های محاسبه شده برای تحویل دادن کارها و نمره‌های ارزشیابی‌ها را پاک کنید؟';
$string['clearaggregatedgrades_help'] = 'نمره‌های جمع شده برای کارهای تحویل داده شده و نمره‌های ارزشیابی‌ها پاک خواهند شد. این نمره‌ها را مجددا در فاز بررسی نمره‌دهی می‌توانید از نو محاسبه کنید.';
$string['clearassessments'] = 'پاک کردن ارزشیابی‌ها';
$string['clearassessmentsconfirm'] = 'آیا مطمئن هستید که می‌خواهید تمام نمرات ارزشیابی‌ها را پاک کنید؟ اطلاعات پاک شده را نمی‌توانید به تنهایی دوباره بدست آورید. بازبین‌ها باید کارهایی که به آن‌ها محول شده بود را دوباره ارزشیابی کنند.';
$string['clearassessments_help'] = 'نمره‌های محاسبه شده برای کارهای تحویل داده شده و نمره‌های ارزشیابی‌ها پاک خواهند شد. اطلاعات مربوط به اینکه فرم‌های ارزشیابی چگونه پر می‌شوند همچنان نگاه داشته می‌شود، ولی تمام بازبین‌ها باید فرم‌های ارزشیابی را دوباره باز کنند و مجددا آن‌ها را ذخیره کنند تا نمره‌های داده شده دوباره محاسبه شوند.';
$string['configexamplesmode'] = 'حالت پیش‌فرض ارزشیابی مثال‌های ارائه شده در کارگاه‌ها';
$string['configgrade'] = 'مقدار پیش‌فرض بیشترین نمره برای تحویل در کارگاه‌ها';
$string['configgradedecimals'] = 'تعداد پیش‌فرض رقم‌هایی که در هنگام نمایش نمرات باید بعد از ممیز اعشار نشان داده شود.';
$string['configgradinggrade'] = 'مقدار پیش‌فرض بیشترین نمره برای ارزشیابی کردن در کارگاه‌ها';
$string['configmaxbytes'] = 'حداکثر اندازه فایل ارسالی به صورت پیش‌فرض برای تمام کارگاه‌های داخل سایت (تحت تاثیر محدودیت‌های درس و سایر تنظیمات داخلی)';
$string['configstrategy'] = 'استراتژی پیش‌فرض نمره‌دهی برای کارگاه‌ها';
$string['createsubmission'] = 'ارائه';
$string['daysago'] = '{$a} روز قبل';
$string['daysleft'] = '{$a} روز باقی مانده';
$string['daystoday'] = 'امروز';
$string['daystomorrow'] = 'فردا';
$string['daysyesterday'] = 'دیروز';
$string['editassessmentform'] = 'ویرایش فرم ارزشیابی';
$string['editassessmentformstrategy'] = 'ویرایش فرم ارزشیابی ({$a})';
$string['editingassessmentform'] = 'در حال ویرایش فرم ارزشیابی';
$string['editingsubmission'] = 'در حال ویرایش';
$string['editsubmission'] = 'ویرایش کار ارائه شده';
$string['err_multiplesubmissions'] = 'در حین ویرایش این فرم، نسخهٔ دیگری از کار تحویل داده شد. تحویل بیشتر از یک کار مجاز نیست.';
$string['err_removegrademappings'] = 'حذف نگاشت نمره‌های استفاده نشده مقدور نیست';
$string['evaluategradeswait'] = 'لطفا تا اتمام بررسی ارزشیابی‌ها و محاسبهٔ نمرات صبر کنید';
$string['evaluation'] = 'بررسی نمره‌دهی';
$string['evaluationmethod'] = 'روش بررسی نمره‌دهی';
$string['evaluationmethod_help'] = 'روش بررسی نمره‌دهی، چگونگی محاسبهٔ نمرهٔ ارزشیابی را تعیین می‌کند. در حال حاضر فقط یک روش برای بررسی وجود دارد: مقایسه با بهترین ارزشیابی';
$string['example'] = 'نمونه کار';
$string['exampleadd'] = 'اضافه کردن نمونه کار';
$string['exampleassess'] = 'ارزشیابی نمونه کار';
$string['exampleassessments'] = 'نمونه کارها برای ارزشیابی';
$string['exampleassesstask'] = 'ارزشیابی نمونه کارها';
$string['exampleassesstaskdetails'] = 'مورد انتظار: {$a->expected}<br />ارزشیابی کرده: {$a->assessed}';
$string['examplecomparing'] = 'مقایسه ارزشیابی نمونه کار';
$string['exampledelete'] = 'حذف نمونه کار';
$string['exampledeleteconfirm'] = 'آیا مطمئنید که می‌خواهید نمونه کار زیر را حذف کنید؟ برای پاک کردن نمونه کار روی دکمهٔ «ادامه» کلیک کنید.';
$string['exampleedit'] = 'ویرایش نمونه کار';
$string['exampleediting'] = 'در حال ویرایش نمونه کار';
$string['exampleneedassessed'] = 'ابتدا باید تمام نمونه کارها را ارزشیابی کنید';
$string['exampleneedsubmission'] = 'ابتدا باید کار خود را تحویل دهید و تمام نمونه کارها را ارزشیابی کنید';
$string['examplesbeforeassessment'] = 'مثال‌ها پس از تحویل دادن کار در دسترس خواهند بود و پیش از ارزشیابی کارها توسط شاگردان، باید ارزشیابی شوند';
$string['examplesbeforesubmission'] = 'قبل از اینکه کسی کار خود را تحویل دهد باید مثال‌ها را ارزشیابی کرده باشد';
$string['examplesmode'] = 'حالت ارزشیابی مثال‌ها';
$string['examplesubmissions'] = 'نمونه کارها';
$string['examplesvoluntary'] = 'ارزشیابی مثال‌های ارائه شده اختیاری است';
$string['feedbackauthor'] = 'بازخورد برای ارائه دهنده';
$string['feedbackby'] = 'بازخورد توسط {$a}';
$string['feedbackreviewer'] = 'بازخورد برای بازبین';
$string['feedbacksettings'] = 'اظهار نظر';
$string['formatpeergradeoverweighted'] = '<span class="grade">{$a->grade}</span> <span class="gradinggrade">(<del>{$a->gradinggrade}</del> /ش <ins>{$a->gradinggradeover}</ins>)</span> @ <span class="weight">{$a->weight}</span>';
$string['givengrades'] = 'نمره‌هایی که داده است';
$string['gradecalculated'] = 'نمرهٔ محاسبه شده برای ارائه';
$string['gradedecimals'] = 'تعداد ارقام اعشار در نمره‌ها';
$string['gradeinfo'] = 'نمره: {$a->received} از {$a->max}';
$string['gradeitemassessment'] = '{$a->workshopname} (ارزشیابی)';
$string['gradeitemsubmission'] = '{$a->workshopname} (تحویل کار)';
$string['gradeover'] = 'ابطال نمرهٔ محاسبه شده برای ارائه و استفاده از این نمره';
$string['gradesreport'] = 'گزارش نمره‌های کارگاه';
$string['gradetopassgrading'] = 'نمره قبولی برای ارزشیابی کردن';
$string['gradetopasssubmission'] = 'نمره قبولی برای تحویل دادن';
$string['gradinggrade'] = 'نمره برای ارزشیابی کردن';
$string['gradinggradecalculated'] = 'نمرهٔ محاسبه شده برای ارزشیابی';
$string['gradinggrade_help'] = 'این تنظیم، حداکثر نمره‌ای که می‌توان برای ارزشیابی کارها کسب کرد را تعیین می‌کند.';
$string['gradinggradeof'] = 'نمره برای ارزشیابی (از {$a})';
$string['gradinggradeover'] = 'ابطال نمرهٔ محاسبه شده و استفاده از این نمره برای ارزشیابی';
$string['gradingsettings'] = 'تنظیمات نمره‌دهی';
$string['iamsure'] = 'بله، مطمئنم';
$string['info'] = 'اطلاعات';
$string['instructauthors'] = 'دستورالعمل چگونگی تحویل دادن';
$string['instructreviewers'] = 'دستورالعمل چگونگی ارزشیابی';
$string['introduction'] = 'معرفی';
$string['latesubmissions'] = 'تحویل با تاخیر';
$string['latesubmissionsallowed'] = 'تحویل با تاخیر مجاز است';
$string['latesubmissions_desc'] = 'اجازهٔ تحویل دادن کار پس از مهلت تعیین شده';
$string['latesubmissions_help'] = 'در صورتی که فعال باشد، کاربران می‌توانند پس از مهلت تحویل یا در طول فاز ارزشیابی، کارشان را تحویل دهند. البته، کارهایی که دیر تحویل داده شده باشند قابل ویرایش نیستند.';
$string['maxbytes'] = 'حداکثر اندازه فایل';
$string['modulename'] = 'کارگاه';
$string['modulename_help'] = 'با استفاده از ماژول فعالیت کارگاه می‌توان کار شاگردان را جمع‌آوری و مرور کرد و از آنها خواست که کار همدیگر را ارزیابی کنند.

شاگردان می‌توانند هر محتوای دیجیتالی (فایل) مانند فایل‌های متنی وُرد یا فایل صفحه‌گستردهٔ اکسل را تحویل دهند و حتی می‌توانند با استفاده از ویرایشگر متنی مودل به‌طور مستقیم متن خود را وارد کنند.

کارهای تحویل داده شده با استفاده از یک فرم ارزیابی چندمعیاره که توسط استاد تعریف شده است ارزیابی می‌شوند. فرآیند ارزیابی هم‌شاگردی‌ها و شناخت فرم ارزیابی را با استفاده از نمونه‌کارهای تحویل داده شده همراه با نمونه‌ارزیابی مرجع که توسط استاد فراهم شده اند می‌توان از پیش تمرین کرد. از شاگردان خواسته می‌شود که کار یک یا چند نفر از هم‌کلاسی‌های خود را ارزیابی کنند. در صورت لزوم کارهای تحویل‌داده‌شده و ارزیابی‌های صورت‌گرفته می‌توانند ناشناس باشند.

شاگردان در یک فعالیت کارگاه دو نمرهٔ مجزا کسب می‌کنند: یک نمره برای کاری که تحویل داده‌اند و یک نمره برای ارزیابی‌هایی که روی کار هم‌شاگردی‌هایشان انجام داده‌اند. هر دو نمره در دفتر نمره ثبت می‌شود.';
$string['modulenameplural'] = 'کارگاه‌ها';
$string['mysubmission'] = 'کار ارائه شده';
$string['nattachments'] = 'حداکثر تعداد فایل‌های قابل پیوست به هر ارائه';
$string['noexamples'] = 'هنوز مثالی در این کارگاه وجود ندارد';
$string['noexamplesformready'] = 'قبل از تهیه نمونه کارها باید فرم ارزشیابی را تعریف کنید';
$string['nogradeyet'] = 'هنوز نمره ندارد';
$string['nosubmissionfound'] = 'این کاربر چیزی تحویل نداده است';
$string['nosubmissions'] = 'هنوز چیزی در این کارگاه تحویل داده نشده است';
$string['notassessed'] = 'هنوز ارزشیابی نشده است';
$string['nothingfound'] = 'چیزی برای نمایش وجود ندارد';
$string['nothingtoreview'] = 'چیزی برای مرور وجود ندارد';
$string['notoverridden'] = 'باطل نشود';
$string['noworkshops'] = 'هیچ کارگاهی در این درس وجود ندارد';
$string['noyoursubmission'] = 'هنوز کارتان را تحویل نداده‌اید';
$string['nullgrade'] = '–';
$string['page-mod-workshop-x'] = 'هر صفحه‌ای از ماژول کارگاه';
$string['participant'] = 'شرکت کننده';
$string['participantrevierof'] = 'شرکت کننده کار این افراد را بازبینی می‌کند';
$string['participantreviewedby'] = 'کار شرکت کننده توسط این افراد بازبینی می‌شود';
$string['phaseassessment'] = 'فاز ارزشیابی';
$string['phaseclosed'] = 'بسته';
$string['phaseevaluation'] = 'فاز بررسی نمره‌دهی';
$string['phasesetup'] = 'فاز راه‌اندازی';
$string['phasesubmission'] = 'فاز تحویل';
$string['pluginadministration'] = 'مدیریت کارگاه';
$string['pluginname'] = 'کارگاه';
$string['prepareexamples'] = 'آماده کردن نمونه کارها برای تمرین ارزشیابی';
$string['previewassessmentform'] = 'پیش‌نمایش';
$string['publishedsubmissions'] = 'کارهای منتشر شده';
$string['publishsubmission'] = 'انتشار کار ارائه شده';
$string['publishsubmission_help'] = 'ارائه‌های منتشر شده پس از بسته شدن کارگاه در دسترس سایرین خواهند بود.';
$string['reassess'] = 'ارزشیابی مجدد';
$string['receivedgrades'] = 'نمره‌هایی که دریافت کرده است';
$string['recentassessments'] = 'ارزشیابی‌های کارگاه:';
$string['recentsubmissions'] = 'ارائه‌های کارگاه:';
$string['resetsubmissions'] = 'پاک کردن تمام تحویل‌ها';
$string['saveandclose'] = 'ذخیره و اتمام';
$string['saveandcontinue'] = 'ذخیره و ادامه ویرایش';
$string['saveandpreview'] = 'ذخیره و پیش‌نمایش';
$string['search:activity'] = 'کارگاه - اطلاعات فعالیت';
$string['selfassessmentdisabled'] = 'خود-ارزشیابی غیر فعال است';
$string['showingperpage'] = 'نمایش {$a} مورد در صفحه';
$string['someuserswosubmission'] = 'حداقل یک نفر هنوز کارش را تحویل نداده است.';
$string['sortasc'] = 'مرتب‌سازی صعودی';
$string['sortdesc'] = 'مرتب‌سازی نزولی';
$string['strategy'] = 'استراتژی نمره‌دهی';
$string['strategyhaschanged'] = 'از زمانی که فرم برای ویرایش باز شد، استراتژی نمره‌دهی کارگاه تغییر کرده است.';
$string['strategy_help'] = 'استراتژی نمره‌دهی، فرم ارزشیابی مورد استفاده و همچنین روش نمره‌دهی کارهای تحویل داده شده را تعیین می‌کند. ۴ انتخاب برای این تنظیم وجود دارد:

* نمره‌دهی جمع شونده - یک نمره و نظر در ارتباط با هر یک از جنبه‌های مشخص شده داده می‌شود
* نظرها - در ارتباط با هر یک از جنبه‌های مشخص شده نظری ارائه می‌شود ولی نمی‌توان نمره‌ای برای آن‌ها تعیین کرد
* تعداد اشتباهات - یک ارزشیابی بله/خیر همراه با ارائه نظر در مورد ادعاهای مشخص شده صورت می‌گیرد
روبریک - سطح هر یک از معیارهای مشخص شده مورد ارزشیابی قرار می‌گیرد';
$string['submission'] = 'ارائه';
$string['submissionattachment'] = 'پیوست';
$string['submissionby'] = 'تحویل توسط {$a}';
$string['submissioncontent'] = 'محتوای ارائه';
$string['submissionend'] = 'مهلت تحویل کار';
$string['submissionenddatetime'] = 'مهلت تحویل: {$a->daydatetime} ({$a->distanceday})';
$string['submissiongrade'] = 'نمره برای تحویل دادن';
$string['submissiongrade_help'] = 'این تنظیم، حداکثر نمره‌ای که می‌توان برای کار تحویل داده شده کسب کرد را تعیین می‌کند.';
$string['submissiongradeof'] = 'نمره برای کار ارائه شده (از {$a})';
$string['submissionlastmodified'] = 'آخرین تغییر';
$string['submissionsettings'] = 'تنظیمات مربوط به تحویل کار';
$string['submissionstart'] = 'آزاد بودن برای تحویل کار از';
$string['submissionstartdatetime'] = 'تحویل دادن کار از {$a->daydatetime} ({$a->distanceday}) آزاد است';
$string['submissiontitle'] = 'عنوان';
$string['switchingphase'] = 'تغییر فاز';
$string['switchphase'] = 'تغییر فاز';
$string['switchphase10info'] = 'شما در آستانهٔ بردن کارگاه به <strong>فاز راه‌اندازی</strong> هستید. در این فاز، کاربران قادر به تغییر کارها یا ارزشیابی‌هایی که صورت داده‌اند نیستند. اساتید می‌توانند از این فاز برای تغییر تنظیمات کارگاه یا تغییر استراتژی نمره‌دهی در فرم‌های تغییر ارزشیابی استفاده کنند.';
$string['switchphase20info'] = 'شما در آستانهٔ بردن کارگاه به <strong>فاز تحویل</strong> هستید. در این فاز، شاگردان می‌توانند کارهایشان را (در صورت تعیین، در محدودهٔ تاریخ‌های تحویل) تحویل دهند. اساتید می‌توانند کارهای تحویل داده شده را برای بازبینی مجدد توسط فرد ثالث اختصاص دهند.';
$string['switchphase30info'] = 'شما در آستانهٔ بردن کارگاه به <strong>فاز ارزشیابی</strong> هستید. در این فاز، بازبین‌ها می‌توانند به کارهای ارائه شده‌ای که به آن‌ها تفویض شده است را (در صورت تعیین، در محدودهٔ تاریخ‌های ارزشیابی) مورد ارزشیابی قرار دهند.';
$string['switchphase40info'] = 'شما در آستانهٔ بردن کارگاه به <strong>فاز بررسی نمره‌دهی</strong> هستید. در این فاز، کاربران نمی‌توانند کارها یا ارزشیابی‌هایشان را تغییر دهند. اساتید می‌توانند از ابزار بررسی نمره‌دهی برای محاسبهٔ نمره نهایی و آماده کردن بازخورد برای بازبین‌ها استفاده کنند.';
$string['switchphase50info'] = 'شما در آستانهٔ بستن کارگاه هستید. این کار موجب می‌شود که نمره‌های محاسبه شده در دفتر نمره قرار بگیرند. شاگردان امکان دیدن کارهایی که تحویل داده‌اند و ارزشیابی آن‌ها را خواهند داشت.';
$string['taskassesspeers'] = 'ارزشیابی همشاگردی‌ها';
$string['taskassesspeersdetails'] = 'کل: {$a->total}<br />معوق: {$a->todo}';
$string['taskassessself'] = 'ارزشیابی کار خود';
$string['taskinstructauthors'] = 'فراهم کردن دستورالعمل چگونگی تحویل دادن';
$string['taskinstructreviewers'] = 'فراهم کردن دستورالعمل برای ارزشیابی';
$string['taskintro'] = 'تعیین متن معرفی کارگاه';
$string['tasksubmit'] = 'کار خود را تحویل دهید';
$string['toolbox'] = 'جعبه ابزار کارگاه';
$string['undersetup'] = 'کارگاه در حال راه‌اندازی است. لطفا تا زمانی که به فاز بعدی برود صبر کنید.';
$string['useexamples'] = 'استفاده از مثال‌ها';
$string['useexamples_desc'] = 'نمونه ارائه‌هایی برای تمرین ارزشیابی فراهم می‌شود';
$string['useexamples_help'] = 'در صورت فعال بودن، کاربران می‌توانند یک یا چند مثال ارائه شده را ارزشیابی کنند و ارزشیابی‌شان را با یک ارزشیابی مرجع مقایسه کنند. این نمره‌ها، در محاسبات استفاده نمی‌شوند.';
$string['usepeerassessment'] = 'استفاده از ارزشیابی همشاگردی‌ها';
$string['usepeerassessment_desc'] = 'شاگردان می‌توانند کار دیگران را ارزشیابی کنند';
$string['usepeerassessment_help'] = 'در صورتی که فعال باشد، به کاربران می‌توان کارهای تحویل داده شده توسط سایر کاربران را برای بازبینی و ارزشیابی اختصاص داد. در این صورت کاربران علاوه بر نمره‌ای که برای کارهایی که تحویل داده‌اند می‌گیرند، نمره‌ای هم برای ارزشیابی کار کاربرانی که به آن‌ها محول شده است دریافت می‌کنند.';
$string['userdatecreated'] = 'در <span>{$a}</span> تحویل داده شده است';
$string['userdatemodified'] = 'در <span>{$a}</span> تغییر کرده است';
$string['userplan'] = 'طراح کارگاه';
$string['userplan_help'] = 'طراح کارگاه، تمام فازهای مربوط به این فعالیت را نشان می‌دهد و کارهایی که در هر فاز باید صورت بگیرد را لیست می‌کند. فاز فعلی به صورت مشخص نمایش داده می‌شود و تکمیل کارها با یک علامت تیک مشخص می‌شود.';
$string['useselfassessment'] = 'استفاده از خود-ارزشیابی';
$string['useselfassessment_desc'] = 'شاگردان می‌توانند کار خودشان را ارزشیابی کنند';
$string['useselfassessment_help'] = 'در صورتی که فعال باشد، می‌توان کاربرانی را موظف به ارزشیابی کار خودشان کرد. این کاربران علاوه بر نمره‌ای که برای کاری که تحویل داده‌اند دریافت می‌کنند، برای ارزشیابی کردن هم نمره‌ای می‌گیرند.';
$string['weightinfo'] = 'وزن: {$a}';
$string['withoutsubmission'] = 'این بازبین خودش کاری تحویل نداده است';
$string['workshop:addinstance'] = 'اضافه‌کردن یک کارگاه جدید';
$string['workshop:allocate'] = 'اختصاص کارهای تحویل داده شده برای بازبینی';
$string['workshop:editdimensions'] = 'ویرایش فرم‌های ارزشیابی';
$string['workshop:manageexamples'] = 'مدیریت نمونه کارهای ارائه شده';
$string['workshopname'] = 'نام کارگاه';
$string['workshop:overridegrades'] = 'نادیده گرفتن و بازنویسی نمرات محاسبه شده';
$string['workshop:peerassess'] = 'ارزشیابی همشاگردی‌ها';
$string['workshop:publishsubmissions'] = 'انتشار کارها';
$string['workshop:submit'] = 'تحویل کار';
$string['workshop:switchphase'] = 'تغییر فاز';
$string['workshop:view'] = 'دیدن کارگاه';
$string['workshop:viewallassessments'] = 'دیدن همهٔ ارزشیابی‌ها';
$string['workshop:viewallsubmissions'] = 'دیدن همهٔ کارهای ارائه شده';
$string['workshop:viewauthornames'] = 'دیدن نام کسی که کار را تحویل داده است';
$string['workshop:viewauthorpublished'] = 'مشاهدهٔ کسانی که کارهای منتشر شده را تحویل داده‌اند';
$string['workshop:viewpublishedsubmissions'] = 'دیدن کارهای منتشر شده';
$string['workshop:viewreviewernames'] = 'دیدن نام بازبین‌ها';
$string['yoursubmission'] = 'کار شما';
