<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_match', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_match
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['availablechoices'] = 'گزینه‌های موجود';
$string['blanksforxmorequestions'] = '{no} جفت مورد دیگر اضافه شود';
$string['correctansweris'] = 'پاسخ درست: {$a}';
$string['filloutthreeqsandtwoas'] = 'باید حداقل دو سؤال و سه پاسخ وارد کنید. برای وارد کردن پاسخ‌های اشتباه بیشتر، می‌توانید این پاسخ‌ها را همراه با سؤال‌های خالی وارد کنید. اگر هم سؤال و هم جوابی خالی باشد، این جفت سؤال و جواب نادیده گرفته خواهد شد.';
$string['nomatchinganswerforq'] = 'برای این سؤال باید یک جواب مشخص کنید.';
$string['notenoughqsandas'] = 'باید حداقل {$a->q} سؤال و {$a->a} جواب وارد کنید.';
$string['notenoughquestions'] = 'باید حداقل {$a} جفت سؤال و جواب ارائه کنید';
$string['pleaseananswerallparts'] = 'لطفاً به تمام قسمت‌های سؤال پاسخ دهید.';
$string['pluginname'] = 'جور کردنی';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال جور کردنی';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال جور کردنی';
$string['pluginname_help'] = 'در سؤال‌های جور کردنی، پاسخ دهنده باید لیستی از نام‌ها یا عبارت‌ها (سؤال‌ها) را به لیستی دیگری از نام‌ها یا عبارت‌ها (پاسخ‌ها) مرتبط کند.';
$string['pluginnamesummary'] = 'پاسخ هر کدام از زیر-سؤال‌ها را باید از میان لیستی از گزینه‌ها انتخاب کرد.';
$string['shuffle'] = 'بهم ریختن ترتیب‌ها';
$string['shuffle_help'] = 'در صورت انتخاب، ترتیب عبارت‌ها (پاسخ‌ها) در هر بار نمایش به صورت تصادف بُر می‌خورد. البته مشروط به اینکه گزینهٔ «بهم ریختن ترتیب گزینه‌ها» در تنظیمات آزمون هم انتخاب شده باشد.';
