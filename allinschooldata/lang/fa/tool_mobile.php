<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_mobile', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_mobile
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configmobilecssurl'] = 'یک فایل CSS برای سفارشی‌کردن ظاهر در برنامه موبایل';
$string['enablesmartappbanners'] = 'فعال‌کردن اعلان هوشمند نصب برنامه';
$string['enablesmartappbanners_desc'] = 'در صورت فعال بودن، هنگامی که سایت با استفاده از سافاری تحت موبایل بازدید شود، اعلانی به‌منظور معرفی برنامه موبایل مودل نمایش داده می‌شود.';
$string['forcedurlscheme'] = 'اگر می‌خواهید که فقط برنامه سفارشی‌شدهٔ شما بتواند از طریق یک پنجرهٔ مرورگر باز شود، الگوی URL برنامه را وارد کنید؛ در غیر اینصورت این تنظیم را خالی بگذارید.';
$string['forcedurlscheme_key'] = 'الگوی URL';
$string['getmoodleonyourmobile'] = 'دریافت نرم‌افزار تلفن همراه';
$string['iosappid'] = 'شناسهٔ یکتای برنامه';
$string['iosappid_desc'] = 'این تنظیم را می‌توان خالی گذاشت مگر اینکه یک برنامهٔ سفارشی‌شدهٔ iOS داشته باشید.';
$string['loginintheapp'] = 'از طریق برنامه';
$string['logininthebrowser'] = 'از طریق پنجرهٔ یک مرورگر (برای پلاگین‌های SSO)';
$string['loginintheembeddedbrowser'] = 'از طریق یک مرورگر داخلی (برای پلاگین‌های SSO)';
$string['mobileapp'] = 'برنامهٔ موبایل';
$string['mobileappconnected'] = 'ارتباط از طریق تلفن همراه برقرار است';
$string['mobileappearance'] = 'نمای موبایل';
$string['mobileappenabled'] = 'دسترسی با اپ موبایل به این سایت امکان‌پذیر است.<br /><a href="{$a}">اپ موبایل را دانلود کنید</a>.';
$string['mobileauthentication'] = 'شناسایی موبایل';
$string['mobilecssurl'] = 'CSS';
$string['mobilesettings'] = 'تنظیمات موبایل';
$string['pluginname'] = 'ابزار موبایل مودل';
$string['pluginnotenabledorconfigured'] = 'پلاگین فعال نیست یا پیکربندی نشده است.';
$string['smartappbanners'] = 'اعلان هوشمند نصب برنامه (فقط iOS)';
$string['typeoflogin'] = 'نوع شناسایی';
$string['typeoflogin_desc'] = 'چگونگی شناسایی را انتخاب کنید.';
