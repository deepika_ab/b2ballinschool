<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'files', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   files
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['privacy:metadata:files:author'] = 'نویسنده محتوای فایل';
$string['privacy:metadata:files:filesize'] = 'اندازه فایل';
$string['privacy:metadata:files:timecreated'] = 'زمان ایجاد فایل';
$string['privacy:metadata:files:timemodified'] = 'زمانی که فایل برای آخرین بار تغییر کرده است';
$string['privacy:metadata:files:userid'] = 'کاربر ایجاد‌کننده فایل';
