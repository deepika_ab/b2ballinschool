<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'repository_flickr', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   repository_flickr
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['apikey'] = 'کلید API';
$string['callbackurl'] = 'آدرس بازگشت';
$string['callbackurltext'] = '<ol>
<li>دوباره به <a href="http://www.flickr.com/services/api/keys/">کلیدهای API فلیکر</a> مراجعه کنید.</li>
<li>مطمئن شوید که آدرس بازگشت این کلید فلیکر را به صورت <strong>{$a}</strong> تعیین کنید.</li></ol>';
$string['callbackwarning'] = '<ol>
<li>یک <a href="http://www.flickr.com/services/api/keys/">کلید API فلیکر و رمز</a> از فلیکر برای این سایت مودل دریافت کنید.</li>
<li>آنها را اینجا وارد کنید، سپس روی دکمهٔ ذخیره کلیک کنید و سپس روی تنظیمات تا دوباره به این صفحه برگردید. خواهید دید که مودل برایتان یک آدرس بازگشت تولید کرده است.</li>
<li>جزئیات کلید <a href="http://www.flickr.com/services/api/keys/">فلیکر</a>تان را دوباره ویرایش کنید و آدرس بازگشت را وارد نمائید.</li></ol>';
$string['configplugin'] = 'پیکربندی فلیکر';
$string['emailaddress'] = 'آدرس پست الکترونیک';
$string['flickr:view'] = 'دیدن انباره فلیکر';
$string['pluginname'] = 'فلیکر';
$string['pluginname_help'] = 'انباره در flickr.com';
$string['secret'] = 'رمز';
