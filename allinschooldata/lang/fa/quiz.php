<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   quiz
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accessnoticesheader'] = 'می‌توانید پیش‌نمایش این آزمون را ببینید، ولی اگر واقعا در حال شرکت در آزمون بودید، هم اکنون دسترسی شما مسدود شده بود. زیرا:';
$string['action'] = 'عمل';
$string['activityoverview'] = 'آزمون‌هایی دارید که از مهلتشان گذشته‌است';
$string['adaptive'] = 'حالت سازگار';
$string['adaptive_help'] = 'در صورت فعال بودن، چند بار پاسخ دادن به یک سؤال در طی یک بار شرکت در آزمون مجاز خواهد بود. به عنوان مثال، اگر پاسخی به عنوان نادرست شناخته شود، شاگرد اجازه خواهد داشت که بلافاصله دوباره پاسخ سؤال را بدهد. البته، بسته به تنظیم «اعمال جریمه»، به ازای هر بار پاسخ اشتباه معمولا مقداری از نمره به عنوان جریمه کسر خواهد شد.';
$string['add'] = 'اضافه کردن';
$string['addaquestion'] = 'یک سؤال جدید';
$string['addarandomquestion'] = 'یک سؤال تصادفی';
$string['addarandomquestion_help'] = 'با اضافه شدن یک سؤال تصادفی، یک سؤال که به طور تصادفی از میان سؤال‌های طبقه انتخاب شده است در آزمون قرار می‌گیرد. به این معنی که شاگردان مختلف به احتمال زیاد سؤال‌های متفاوتی را خواهند دید. و در حالتی که شرکت کردن مجدد در آزمون مجاز باشد، به احتمال زیاد، هر بار سؤال‌ها تغییر خواهند کرد.';
$string['addarandomselectedquestion'] = 'اضافه‌کردن یک سؤال انتخاب‌شده به‌طور تصادفی ...';
$string['addasection'] = 'یک بخش جدید';
$string['adddescriptionlabel'] = 'اضافه کردن یک توضیح';
$string['addingquestion'] = 'اضافه کردن یک سؤال';
$string['addingquestions'] = 'این سمت از صفحه جائی است که شما بانک سؤال‌های خود را مدیریت می‌نمائید. سؤال‌ها، به منظور کمک به شما در سازمان‌دهی آن‌ها، طبقه‌بندی شده‌اند، و می‌توانند توسط هر آزمونی در درس شما یا حتی درس‌های دیگر (اگر آن‌ها را «منتشر» نمائید) استفاده شوند.<br /><br /> پس از اینکه یک طبقهٔ سؤال‌ها را انتخاب یا ایجاد نمودید قادر به تعریف یا ویرایش سؤال‌ها خواهید بود. هر کدام از این سؤال‌ها را می‌توانید برای اضافه کردن به آزمون خود در سمت دیگر صفحه انتخاب نمائید.';
$string['addmoreoverallfeedbacks'] = 'اضافه کردن {no} مورد بازخورد دیگر';
$string['addnewgroupoverride'] = 'بازنویسی تنظیمات برای گروه‌ها';
$string['addnewpagesafterselected'] = 'درج صفحه‌های جدید بعد از سؤال‌های انتخاب شده';
$string['addnewquestionsqbank'] = 'اضافه کردن سؤال به طبقهٔ {$a->catname}: {$a->link}';
$string['addnewuseroverride'] = 'بازنویسی تنظیمات برای کاربران';
$string['addpagebreak'] = 'اضافه‌کردن صفحهٔ جدید';
$string['addpagehere'] = 'درج صفحه در این محل';
$string['addquestion'] = 'اضافه‌کردن سؤال';
$string['addquestionfrombankatend'] = 'اضافه‌کردن از بانک سؤال به انتهای آزمون';
$string['addquestionfrombanktopage'] = 'اضافه‌کردن از بانک سؤال به صفحهٔ {$a}';
$string['addquestions'] = 'اضافه کردن سؤال‌ها';
$string['addquestionstoquiz'] = 'اضافه کردن سؤال‌ها به آزمون جاری';
$string['addrandom'] = 'اضافه کردن {$a} سؤال تصادفی';
$string['addrandom1'] = '<< اضافه شدن';
$string['addrandom2'] = 'سؤال‌های شانسی';
$string['addrandomfromcategory'] = 'اضافه کردن سؤال‌های تصادفی از طبقه:';
$string['addrandomquestion'] = 'اضافه‌کردن سؤال تصادفی';
$string['addrandomquestionatend'] = 'اضافه‌کردن یک سؤال تصادفی به انتهای آزمون';
$string['addrandomquestiontopage'] = 'اضافه‌کردن یک سؤال تصادفی به صفحهٔ {$a}';
$string['addrandomquestiontoquiz'] = 'اضافه کردن یک سؤال تصادفی به آزمون {$a}';
$string['addselectedquestionstoquiz'] = 'اضافه‌کردن سؤال‌های انتخاب‌شده به آزمون';
$string['addselectedtoquiz'] = 'اضافه شدن موارد انتخاب شده به آزمون';
$string['addtoquiz'] = 'اضافه شدن به آزمون';
$string['affectedstudents'] = '{$a} متأثر شده';
$string['aftereachquestion'] = 'پس از اضافه کردن هر سؤال';
$string['afternquestions'] = 'پس از اضافه کردن هر {$a} سؤال';
$string['age'] = 'عمر';
$string['allattempts'] = 'همهٔ دفعات شرکت';
$string['allinone'] = 'نامحدود';
$string['allowreview'] = 'اجازهٔ مرور';
$string['alreadysubmitted'] = 'به احتمال زیاد قبلاً این تلاش را ارائه داده اید';
$string['alternativeunits'] = 'واحدهای قابل جایگزینی';
$string['alwaysavailable'] = 'همیشه حاضر';
$string['analysisoptions'] = 'گزینه‌های تحلیل';
$string['analysistitle'] = 'جدول تحلیل موردی';
$string['answer'] = 'جواب';
$string['answered'] = 'پاسخ داده شد';
$string['answerhowmany'] = 'یک یا چند جواب؟';
$string['answers'] = 'جواب‌ها';
$string['answersingleno'] = 'انتخاب بیش از یک گزینه مجاز است';
$string['answersingleyes'] = 'فقط یک گزینه قابل انتخاب است';
$string['answertoolong'] = 'پاسخ بسیار طولانی پس از خط {$a} (حداکثر ۲۵۵ حرف)';
$string['anytags'] = 'هر برچسبی';
$string['aon'] = 'قالب AON';
$string['areyousureremoveselected'] = 'آیا مطمئنید که می‌خواهید تمام سؤال‌های انتخاب شده را حذف کنید؟';
$string['asshownoneditscreen'] = 'به ترتیبی که در صفحهٔ ویرایش نشان داده می‌شوند';
$string['attempt'] = 'دفعهٔ {$a}';
$string['attemptalreadyclosed'] = 'این شرکت قبلا به پایان رسیده است.';
$string['attemptclosed'] = 'شرکت هنوز به پایان نرسیده است.';
$string['attemptduration'] = 'زمان صرف شده';
$string['attemptedon'] = 'شرکت در آزمون در';
$string['attempterror'] = 'اجازهٔ شرکت در این آزمون را هم‌اکنون ندارید زیرا: {$a}';
$string['attempterrorcontentchange'] = 'این پیش‌نمایش آزمون دیگر وجود ندارد. (هنگامی که یک آزمون ویرایش می‌شود، تمام پیش‌نمایش‌های درجریان به‌طور خودکار پاک می‌شوند.)';
$string['attempterrorcontentchangeforuser'] = 'این تلاش صورت‌گرفته در آزمون دیگر وجود ندارد.';
$string['attempterrorinvalid'] = 'شناسهٔ تلاش در آزمون نامعتبر';
$string['attemptfirst'] = 'اولین تلاش';
$string['attemptincomplete'] = 'این دفعه شرکت در آزمون (توسط {$a}) هنوز کامل نشده است.';
$string['attemptlast'] = 'آخرین تلاش';
$string['attemptnumber'] = 'تلاش';
$string['attemptquiznow'] = 'شرکت در آزمون';
$string['attempts'] = 'دفعات شرکت';
$string['attemptsallowed'] = 'تعداد دفعه‌های مجاز شرکت در آزمون';
$string['attemptsdeleted'] = 'شرکت‌های صورت گرفته در آزمون حذف شدند';
$string['attemptselection'] = 'تلاش‌هایی که به ازای هر کاربر می‌خواهید تحلیل شوند را انتخاب کنید:';
$string['attemptsexist'] = 'دیگر نمی‌توانید سؤالی را اضافه یا کم کنید.';
$string['attempts_help'] = 'تعداد کل تلاش‌های مجاز در آزمون (بدون تلاش‌های اضافی).';
$string['attemptsnum'] = 'دفعات شرکت در آزمون: {$a}';
$string['attemptsnumthisgroup'] = 'دفعات شرکت در آزمون: {$a->total} ({$a->group} بار از این گروه)';
$string['attemptsnumyourgroups'] = 'دفعات شرکت در آزمون: {$a->total} ({$a->group} بار از گروه‌های شما)';
$string['attemptsonly'] = 'فقط نمایش شاگردانی که در آزمون شرکت کرده‌اند';
$string['attemptstate'] = 'وضعیت';
$string['attemptsunlimited'] = 'به تعداد نامحدود';
$string['autosaveperiod'] = 'تأخیر ذخیرهٔ خودکار';
$string['back'] = 'بازگشت به پیش‌نمایش سؤال';
$string['backtocourse'] = 'بازگشت به درس';
$string['backtoquestionlist'] = 'بازگشت به لیست سؤال‌ها';
$string['backtoquiz'] = 'بازگشت به ویرایش سؤال';
$string['basicideasofquiz'] = 'ایده‌های اساسی ایجاد آزمون';
$string['bestgrade'] = 'بهترین نمره';
$string['bothattempts'] = 'نمایش شاگردانی که در آزمون شرکت کرده‌اند یا نکرده‌اند';
$string['browsersecurity'] = 'امنیت مرورگر';
$string['browsersecurity_help'] = 'اگر «پنجرهٔ پاپ-آپ تمام‌صفحهٔ امن شده با استفاده از جاوا اسکریپت» انتخاب شده باشد،

* آزمون تنها در صورتی که شاگرد از یک مرورگر وب که جاوا اسکریپت رویش فعال باشد استفاده کند شروع خواهد شد
* آزمون در یک پنجرهٔ پاپ-آپ تمام‌صفحه که تمام پنجره‌های دیگر را پوشانده است و دارای دکمه‌های کنترل راهبری نیست نمایش داده می‌شود
* تا جایی که ممکن است، جلوی استفادهٔ شاگردها از امکاناتی مانند کپی کردن و درج متن گرفته می‌شود';
$string['calculated'] = 'محاسباتی';
$string['calculatedquestion'] = 'سؤال محاسباتی خط {$a} پشتیبانی نمی‌شود. این سؤال نادیده گرفته خواهد شد.';
$string['cannotcreatepath'] = 'مسیر ({$a}) قابل ایجاد نیست';
$string['cannoteditafterattempts'] = 'نمی‌توانید سؤال‌ها را کم یا زیاد نمائید زیرا کسانی در این آزمون شرکت کرده‌اند. ({$a})';
$string['cannotfindprevattempt'] = 'پیدا کردن دفعهٔ قبلی شرکت در آزمون به منظور ایجاد تلاش جدید بر مبنای آن مقدور نیست.';
$string['cannotfindquestionregard'] = 'دریافت سؤال‌ها برای نمره‌دهی مجدد با موفقیت انجام نشد!';
$string['cannotinsert'] = 'درج سؤال مقدور نبود';
$string['cannotinsertrandomquestion'] = 'درج سؤال تصادفی جدید مقدور نبود!';
$string['cannotloadquestion'] = 'دریافت تنظیمات سؤال مقدور نبود';
$string['cannotloadtypeinfo'] = 'بارگذاری اطلاعات مربوط به نوع سؤال مقدور نبود';
$string['cannotopen'] = 'فایل صدور ({$a}) قابل باز شدن نیست';
$string['cannotrestore'] = 'بازیابی جلسه‌های آزمون مقدور نبود';
$string['cannotreviewopen'] = 'نمی‌توانید این شرکت را مرور کنید؛ هنوز جاری است.';
$string['cannotsavelayout'] = 'ذحیرهٔ چیدمان مقدور نبود';
$string['cannotsavenumberofquestion'] = 'ذخیرهٔ تعداد سؤال در هر صفحه مقدور نبود';
$string['cannotsavequestion'] = 'ذخیرهٔ لیست سؤال‌ها مقدور نبود';
$string['cannotsetgrade'] = 'تعیین یک حداکثر جدید برای نمرهٔ آزمون مقدور نبود';
$string['cannotsetsumgrades'] = 'مقدار دهی جمع نمرات با شکست روبرو شد';
$string['cannotwrite'] = 'نوشتن روی فایل صدور ({$a}) مقدور نیست';
$string['caseno'] = 'خیر، حالت حروف مهم نیست';
$string['casesensitive'] = 'حساس بودن به بزرگ و کوچکی حروف';
$string['caseyes'] = 'بله، حالت حروف باید تطابق داشته باشد';
$string['categoryadded'] = 'طبقهٔ «{$a}» اضافه شد';
$string['categorydeleted'] = 'طبقهٔ «{$a}» حذف شد';
$string['categorynoedit'] = 'شما مجوزهای ویرایش در طبقهٔ «{$a}» را ندارید.';
$string['categoryupdated'] = 'طبقه با موفقیت به‌روز شد';
$string['close'] = 'بستن پنجره';
$string['closebeforeopen'] = 'آزمون قابل به‌روز شدن نبود. تاریخی که برای بسته شدن آزمون تعیین کرده‌اید پیش از تاریخ باز شدن آزمون است.';
$string['closepreview'] = 'بستن پیش‌نمایش';
$string['closereview'] = 'اتمام مرور';
$string['comment'] = 'توضیح';
$string['commentorgrade'] = 'نوشتن توضیح یا تغییر نمره';
$string['comments'] = 'توضیحات';
$string['completedon'] = 'پایان';
$string['completionattemptsexhausted'] = 'یا اینکه تمام تلاش‌های باقیمانده کامل شده باشد';
$string['completionpass'] = 'نیازمند نمرهٔ قبولی';
$string['configadaptive'] = 'اگر این قابلیت را فعال کنید، آنگاه شاگردان مجاز خواهند بود به هر سؤال (حتی طی یک بار شرکت در آزمون) چند بار پاسخ دهند.';
$string['configattemptsallowed'] = 'محدودیت تعداد دفعه‌های مجاز شرکت هر شاگرد در آزمون.';
$string['configdecimaldigits'] = 'تعداد رقم‌هایی که هنگام نمایش نمره‌ها باید پس از علامت اعشار وجود داشته باشند.';
$string['configdecimalplaces'] = 'تعداد رقم‌هایی که هنگام نمایش نمره‌های آزمون باید پس از علامت اعشار وجود داشته باشند.';
$string['configdecimalplacesquestion'] = 'تعداد رقم‌هایی که هنگام نمایش نمرهٔ تک تک سؤال‌ها، باید پس از علامت اعشار وجود داشته باشند.';
$string['configdelay1'] = 'اگر تاخیری را در این قسمت تعیین کنید، آنگاه برای اینکه شاگردان بتوانند برای بار دوم در آزمون شرکت کنند، حتما باید حداقل به اندازهٔ زمان مشخص شده از دفعهٔ اولی که در آزمون شرکت کرده‌اند سپری شده باشد.';
$string['configdelay1st2nd'] = 'اگر تاخیری را در این قسمت تعیین کنید، شاگردان تا قبل از سپری شدن زمان مشخص شده در این قسمت از لحظهٔ اتمام شرکتشان در آزمون برای بار اول، نمی‌توانند برای بار دوم در آزمون شرکت کنند.';
$string['configdelay2'] = 'اگر تاخیری را در این قسمت تعیین کنید، آنگاه برای اینکه شاگردان بتوانند برای بار سوم و دفعات بعد از آن در آزمون شرکت کنند، حتما باید حداقل به اندازهٔ زمان مشخص شده از دفعهٔ آخری که در آزمون شرکت کرده‌اند سپری شده باشد.';
$string['configdelaylater'] = 'اگر تاخیری را در این قسمت تعیین کنید، شاگردان تا قبل از سپری شدن زمان مشخص شده در این قسمت از لحظهٔ اتمام شرکتشان در آزمون برای آخرین بار، نمی‌توانند برای بار سوم، چهارم، ... در آزمون شرکت کنند.';
$string['configeachattemptbuildsonthelast'] = 'اگر چند بار شرکت در آزمون مجاز باشد، آنگاه هر بار شرکت جدید در آزمون شامل نتایج دفعهٔ قبل خواهد بود.';
$string['configgrademethod'] = 'در صورتی که چند بار شرکت در آزمون مجاز باشد، از چه روشی باید برای محاسبهٔ نمرهٔ نهایی شاگردان در آزمون استفاده کرد.';
$string['configintro'] = 'مقادیری که اینجا تعیین می‌کنید به عنوان مقادیر پیش‌فرض در فرم تنظیمات فعالیت که هنگام ایجاد یک آزمون جدید نمایش داده می‌شود مورد استفاده قرار خواهد گرفت. به علاوه می‌توانید تعیین کنید که کدام موردها در فرم تنظیمات آزمون به عنوان مورد پیشرفته در نظر گرفته شوند.';
$string['configmaximumgrade'] = 'نمره‌ای که به طور پیش‌فرض به عنوان حداکثر نمره در نظر گرفته می‌شود و نمره‌های آزمون با در نظر گرفتن آن به عنوان سقف نمره متناسب خواهند شد.';
$string['confignavmethod'] = 'در حرکت آزاد، سوال‌ها می‌توانند به هر ترتیبی پاسخ داده شوند. در حرکت ترتیبی، سوال‌ها فقط می‌توانند به‌ترتیب نمایش پاسخ داده شوند.';
$string['confignewpageevery'] = 'هنگام اضافه کردن سؤال‌ها به آزمون، بر اساس تنظیمی که در ایت قسمت تعیین کرده‌اید، به صورت خودکار صفحه‌های جدیدی آغاز خواهند شد.';
$string['configpenaltyscheme'] = 'به ازای هر بار پاسخ اشتباه در حالت سازگار، نمره‌ای به عنوان جریمه کسر خواهد شد.';
$string['configpopup'] = 'اجبار به شرکت کردن در آزمون در یک پنجرهٔ پاپ‌آپ، و استفاده از جاوا اسکریپت برای جلوگیری از کپی کردن و درج کردن و غیره در حین شرکت در آزمون.';
$string['configrequirepassword'] = 'شاگردها برای شرکت در آزمون باید این رمز را وارد کنند.';
$string['configrequiresubnet'] = 'شاگردها فقط از این رایانه‌ها مجاز به شرکت در آزمون هستند.';
$string['configreviewoptions'] = 'این گزینه‌ها تعیین می‌کنند که چه اطلاعاتی توسط شاگردان، وقتی که در حال مرور شرکت در آزمون یا دیدن گزارش‌های آزمون هستند، قابل دیدن باشد.';
$string['configshowblocks'] = 'نمایش بلوک‌ها در طول شرکت در آزمون.';
$string['configshowuserpicture'] = 'نمایش عکس کاربر بر روی صفحهٔ نمایش در هنگام شرکت او در آزمون.';
$string['configshufflewithin'] = 'در صورت فعال بودن این گزینه، اجزای تشکیل دهندهٔ سؤال‌ها در هر باری که به شاگردی نمایش داده می‌شوند، به صورت تصادفی جابه‌جا می‌شوند؛ به شرطی که در تنظیمات آن سؤال‌ها هم این تنظیم فعال باشد.';
$string['configtimelimit'] = 'محدودیت زمانی پیش‌فرض به دقیقه برای آزمون‌ها. تعیین مقدار صفر به معنی نداشتن محدودیت است.';
$string['configtimelimitsec'] = 'محدودیت زمانی پیش‌فرض به ثانیه برای آزمون‌ها. تعیین مقدار صفر به معنی نداشتن محدودیت است.';
$string['configurerandomquestion'] = 'پیکربندی سؤال';
$string['confirmclose'] = 'شما در آستانهٔ اتمام شرکت خود در آزمون هستید. پس ا ز اتمام شرکت دیگر نمی‌توانید پاسخ‌های خود را تغییر دهید.';
$string['confirmserverdelete'] = 'آیا مطمئنید که می‌خواهید کارگزار <b>{$a}</b> را از لیست حذف کنید؟';
$string['connectionerror'] = 'اتصال شبکه قطع شد. (ذخیرهٔ خودکار با شکست مواجه شد).

پاسخ‌هایی که در چند دقیقهٔ اخیر در این صفحه وارد کرده‌اید را یادداشت کنید، سپس سعی کنید دوباره متصل شوید.

پس از اتصال مجدد ارتباط، پاسخ‌های شما باید ذخیره شده باشند و این پیغام ناپدید خواهد شد.';
$string['connectionok'] = 'ارتباط شبکه دوباره برقرار شد. می‌توانید ادامه دهید.';
$string['containercategorycreated'] = 'این طبقه برای نگهداری تمام طبقه‌های اصلی‌ای که به دلائل مطرح شده در زیر به سطح سایت منتقل شده‌اند، ایجاد شده است.';
$string['continueattemptquiz'] = 'ادامهٔ آخرین دفعهٔ شرکت در آزمون';
$string['continuepreview'] = 'ادامهٔ آخرین پیش‌نمایش';
$string['copyingfrom'] = 'ایجاد یک کپی از سؤال «{$a}»';
$string['copyingquestion'] = 'کپی کردن یک سؤال';
$string['correct'] = 'صحیح';
$string['correctanswer'] = 'پاسخ صحیح';
$string['correctanswerformula'] = 'فرمول محاسبهٔ پاسخ';
$string['correctansweris'] = 'پاسخ صحیح: {$a}';
$string['correctanswerlength'] = 'ارقام بامعنی';
$string['correctanswers'] = 'پاسخ‌های صحیح';
$string['correctanswershows'] = 'پاسخ صحیح نمایش می‌دهد:';
$string['corrresp'] = 'پاسخ صحیح';
$string['countdown'] = 'شمارش معکوس';
$string['countdownfinished'] = 'آزمون در حال بسته شدن است. باید پاسخ‌هایتان را هم‌اکنون ارائه کنید.';
$string['countdowntenminutes'] = 'آزمون تا ۱۰ دقیقه بسته خواهد شد.';
$string['coursetestmanager'] = 'قالب برنامهٔ Course Test Manager';
$string['createcategoryandaddrandomquestion'] = 'ایجاد طبقه و اضافه کردن سوال تصادفی';
$string['createfirst'] = 'ابتدا باید چند سؤال کوتاه-جواب ایجاد کنید.';
$string['createmultiple'] = 'اضافه کردن چند سؤال تصادفی به آزمون';
$string['createnewquestion'] = 'تعریف یک سؤال جدید';
$string['createquestionandadd'] = 'ایجاد یک سؤال جدید و اضافه کردن آن به آزمون.';
$string['custom'] = 'قالب سفارشی';
$string['dataitemneed'] = 'باید حداقل یک مجموعه از اقلام داده‌ای اضافه کنید تا یک سؤال معتبر داشته باشید';
$string['datasetdefinitions'] = 'تعاریف مجموعه داده قابل استفاده مجدد برای طبقه {$a}';
$string['datasetnumber'] = 'عدد';
$string['daysavailable'] = 'روز در دسترس است';
$string['decimaldigits'] = 'تعداد رقم‌های اعشار در نمره‌ها';
$string['decimalplaces'] = 'تعداد ارقام اعشار در نمره‌ها';
$string['decimalplaces_help'] = 'این تنظیم، تعداد ارقامی که هنگام نمایش نمره‌ها بعد از علامت اعشار نمایش داده می‌شوند را تعیین می‌کند. این تنظیم فقط روی نحوهٔ نمایش تاثیر می‌گذارد، نه خود نمره‌ها و یا محاسبات داخلی که با حداکثر دقت انجام می‌شوند.';
$string['decimalplacesquestion'] = 'تعداد ارقام اعشار در نمرهٔ سؤال‌ها';
$string['decimalplacesquestion_help'] = 'این تنظیم تعداد رقم‌های اعشاری که هنگام نمایش نمرهٔ تک تک سؤال‌ها نمایش داده می‌شوند را تعیین می‌کند.';
$string['decimalpoints'] = 'ممیزهای اعشار';
$string['default'] = 'پیش‌فرض';
$string['defaultgrade'] = 'نمرهٔ پیش‌فرض سؤال';
$string['defaultinfo'] = 'طبقهٔ پیش‌فرض برای سؤال‌ها.';
$string['delay1'] = 'فاصله بین دفعه‌های اول و دوم شرکت در آزمون';
$string['delay1st2nd'] = 'فاصلهٔ اجباری بین دفعهٔ اول و دوم شرکت در آزمون';
$string['delay1st2nd_help'] = 'در صورت فعال بودن، برای اینکه شاگردی بتواند برای بار دوم در آزمون شرکت کند، حتما باید حداقل به اندازهٔ زمان مشخص شده از دفعهٔ اولی که در آزمون شرکت کرده است سپری شده باشد.';
$string['delay2'] = 'فاصله بین دفعه‌های بعدی';
$string['delaylater'] = 'فاصلهٔ اجباری بین دفعه‌های بعدی';
$string['delaylater_help'] = 'در صورت فعال بودن، برای اینکه شاگردی بتواند برای بار سوم و دفعات بعد از آن در آزمون شرکت کند، باید صبر کند تا به اندازهٔ زمان تعیین شده، از آخرین دفعه‌ای که در آزمون شرکت کرده است سپری شود.';
$string['deleteattemptcheck'] = 'آیا به طور قطع مطمئن هستید که می‌خواهید دفعات انتخاب شدهٔ شرکت در آزمون را کاملاً حذف نمائید؟';
$string['deleteselected'] = 'حذف موارد انتخاب شده';
$string['deletingquestionattempts'] = 'پاک کردن تلاش‌های صورت گرفته در سؤال';
$string['description'] = 'توضیح';
$string['disabled'] = 'غیر فعال';
$string['displayoptions'] = 'تنظیمات نمایش';
$string['download'] = 'برای دریافت فایل صادر شدهٔ طبقه کلیک کنید';
$string['downloadextra'] = '(فایل هم در محل فایل‌های درس در پوشهٔ <span dir="ltr" style="display:inline-block;direction:ltr">/backupdata/quiz</span> ذخیره می‌شود)';
$string['dragtoafter'] = 'پس از {$a}';
$string['duplicateresponse'] = 'این ارائه نادیده گرفته شده است، زیرا شما همین پاسخ را قبلاً داده‌اید.';
$string['eachattemptbuildsonthelast'] = 'هر بار شرکت در آزمون از ادامهٔ دفعهٔ قبل باشد';
$string['eachattemptbuildsonthelast_help'] = 'در صورتی که چند بار شرکت در آزمون مجاز باشد و این تنظیم هم فعال باشد، هر بار شرکت جدید در آزمون شامل نتایج دفعهٔ قبل خواهد بود. در این صورت، یک آزمون می‌تواند طی چند بار شرکت کامل شود.';
$string['editcategories'] = 'ویرایش طبقه‌ها';
$string['editcategory'] = 'ویرایش طبقه';
$string['editcatquestions'] = 'ویرایش سؤال‌های طبقه';
$string['editingquestion'] = 'در حال ویرایش یک سؤال';
$string['editingquiz'] = 'ویرایش آزمون';
$string['editingquiz_help'] = 'هنگام ایجاد یک آزمون، مفاهیم کلیدی عبارتند از:

* آزمون، محتوی سؤال‌هایی در یک یا چند صفحه
* بانک سؤال، که کل سؤال‌ها به صورت طبقه‌بندی شده نگهداری می‌کند
* سؤال‌های تصادفی - یک شاگرد هر بار که در آزمون شرکت می‌کند سؤال متفاوتی می‌بیند؛ همچنین شاگردان مختلف ممکن است سؤال‌های متفاوتی دریافت کنند';
$string['editingquizx'] = 'ویرایش آزمون: {$a}';
$string['editmaxmark'] = 'ویرایش حداکثر نمره';
$string['editoverride'] = 'ویرایش بازنویسی';
$string['editqcats'] = 'ویرایش طبقه‌های سؤال‌ها';
$string['editquestion'] = 'ویرایش سؤال';
$string['editquestions'] = 'ویرایش سؤالات';
$string['editquiz'] = 'ویرایش محتوای آزمون';
$string['editquizquestions'] = 'ویرایش سؤال‌های آزمون';
$string['emailconfirmbody'] = '{$a->username} گرامی،

از ارائه پاسخ‌هایتان در ارتباط با
«{$a->quizname}»
در درس «{$a->coursename}»
در {$a->submissiontime} سپاسگزاریم.

این نامه مؤید آن است که پاسخ‌های شما ذخیره شده‌اند.

در آدرس {$a->quizurl} می‌توانید به این آزمون دسترسی داشته باشید.';
$string['emailnotifybody'] = '{$a->username} گرامی،

{$a->studentname} آزمون «{$a->quizname}» ({$a->quizurl}) در درس «{$a->coursename}» را کامل کرده است.

در آدرس {$a->quizreviewurl} می‌توانید این تلاش را مرور کنید.';
$string['emailnotifysubject'] = '{$a->studentname} آزمون {$a->quizname} را کامل کرده است';
$string['emailoverduebody'] = '{$a->studentname} عزیز،

شما در «{$a->quizname}» در درس «{$a->coursename}» شرکت کردید ولی هنوز پاسخ‌های خود را تحویل نداده‌اید.

اگر هنوز مایلید که پاسخ‌هایتان را تحویل بدهید، لطفا به {$a->attemptsummaryurl} بروید که روی دکمهٔ تحویل کلیک کنید. این کار را باید تا پیش از {$a->attemptgraceend} انجام دهید در غیر اینصورت تلاش شما محسوب نخواهد شد.';
$string['empty'] = 'خالی';
$string['enabled'] = 'فعال';
$string['endtest'] = 'اتمام آزمون ...';
$string['erroraccessingreport'] = 'شما به این گزارش دسترسی ندارید';
$string['errorinquestion'] = 'خطا در سؤال';
$string['errormissingquestion'] = 'خطا: سیستم، سؤال با شناسهٔ {$a} را از دست داده است';
$string['errornotnumbers'] = 'خطا - پاسخ‌ها باید عددی باشند';
$string['errorunexpectedevent'] = 'کد رویداد پیش‌بینی نشده‌ای ({$a->event}) برای سؤال {$a->questionid} در تلاش {$a->attemptid} پیدا شد.';
$string['essay'] = 'تشریحی';
$string['essayquestions'] = 'سؤال‌ها';
$string['eventattemptdeleted'] = 'تلاش در آزمون حذف شد';
$string['eventattemptpreviewstarted'] = 'پیش‌نمایش تلاش در آزمون شروع شد';
$string['eventattemptreviewed'] = 'تلاش در آزمون مرور شد';
$string['eventattemptsummaryviewed'] = 'خلاصه وضعیت شرکت در آزمون مشاهده شد';
$string['eventattemptviewed'] = 'تلاش در آزمون مشاهده شد';
$string['eventquestionmanuallygraded'] = 'سؤال به‌طور دستی نمره داده شد';
$string['eventquizattemptstarted'] = 'تلاش در آزمون آغاز شد';
$string['eventquizattemptsubmitted'] = 'تلاش در آزمون تحویل داده شد';
$string['eventreportviewed'] = 'گزارش آزمون مشاهده شد';
$string['everynquestions'] = 'هر {$a} سؤال';
$string['everyquestion'] = 'هر سؤال';
$string['everythingon'] = 'انتخاب همهٔ موارد';
$string['existingcategory'] = 'طبقهٔ موجود';
$string['exportcategory'] = 'طبقهٔ مورد صدور';
$string['exporterror'] = 'خطایی در حین فرآیند صدور رخ داد';
$string['exportingquestions'] = 'سؤال‌ها در حال صادر شدن به فایل هستند';
$string['exportname'] = 'نام فایل';
$string['exportquestions'] = 'صدور سؤال‌ها به فایل';
$string['extraattemptrestrictions'] = 'محدودیت‌های اضافی برای شرکت در آزمون';
$string['false'] = 'نادرست';
$string['feedback'] = 'بازخورد';
$string['feedbackerrorboundaryformat'] = 'کران‌های نمره برای بازخورد باید به صورت درصد یا یک عدد باشند. مقداری که شما برای کران {$a} وارد کرده‌اید نامعتبر است.';
$string['feedbackerrorboundaryoutofrange'] = 'کران‌های نمره برای بازخورد باید بین ٪۰ تا ٪۱۰۰ باشند. مقداری که شما برای کران {$a} وارد کرده‌اید خارج از این محدوده است.';
$string['feedbackerrorjunkinboundary'] = 'کران‌های نمره مربوط به بازخورد را باید به ترتیب از بالا به پائین و بدون خالی گذاشتن کران‌های میانی (کران‌های قبلی) پر کنید.';
$string['feedbackerrorjunkinfeedback'] = 'متن‌های بازخوردها را باید به ترتیب از بالا به پائین و بدون باقی گذاشتن فاصله بین آن‌ها پر کنید.';
$string['feedbackerrororder'] = 'کران‌های نمره مربوط به بازخوردها باید به ترتیب و به صورت نزولی باشند. مقداری که برای کران {$a} وارد کرده‌اید خارج از ترتیب است.';
$string['file'] = 'فایل';
$string['fileformat'] = 'قالب فایل';
$string['fillcorrect'] = 'نمایش پاسخ صحیح';
$string['filloutnumericalanswer'] = 'باید حداقل یک جواب ممکن و خطای قابل قبول را مشخص نمائید. از اولین پاسخ منطبق برای تعیین نمره و بازخورد استفاده خواهد شد. اگر در انتها بازخوردی را بدون وارد کردن پاسخ مرتبط برایش وارد کنید، به شاگردانی که پاسخشان با هیچ یک از سایر پاسخ‌ها منطبق نیست نمایش داده خواهد شد.';
$string['filloutoneanswer'] = 'باید حداقل یک جواب ممکن را مشخص نمائید. جواب‌هایی که خالی بمانند استفاده نخواهند شد. از «*» می‌توان به نمایندگی از طرف هر حرفی استفاده کرد. از اولین پاسخ منطبق برای تعیین نمره و بازخورد استفاده خواهد شد.';
$string['filloutthreequestions'] = 'باید حداقل ۳ سؤال همراه با پاسخ‌های مرتبط را مشخص نمائید. می‌توانید پاسخ‌های اشتباه اضافه‌تری را با فراهم کردن پاسخ برای سؤال‌های خالی مشخص کنید. قسمت‌هایی که هم سؤال و هم جواب خالی باشد نادیده گرفته خواهد شد.';
$string['fillouttwochoices'] = 'باید حداقل ۲ گزینه را پر کنید. گزینه‌هایی که خالی مانده باشند استفاده نخواهند شد.';
$string['finishattemptdots'] = 'اتمام آزمون...';
$string['finishreview'] = 'اتمام مرور';
$string['forceregeneration'] = 'تولید مجدد اجباری';
$string['formatnotfound'] = 'قالب ورود/صدور {$a} پیدا نشد';
$string['formulaerror'] = 'خطاهای فرمول!';
$string['fractionsaddwrong'] = 'جمع نمره‌های مثبتی که وارد کرده‌اید برابر با ٪۱۰۰ نمیشود.<br />بلکه حاصل جمع آن‌ها ٪{$a} می‌شود.<br />آیا مایلید که بازگردید و این سؤال را درست کنید؟';
$string['fractionsnomax'] = 'نمرهٔ یکی از گزینه‌ها باید ٪۱۰۰ باشد تا گرفتن نمرهٔ کامل این سؤال ممکن باشد.<br />آیا مایلید که بازگردید و این سؤال را درست کنید؟';
$string['fromfile'] = 'فایل مبدا:';
$string['functiondisabledbysecuremode'] = 'قابلیت مورد نظر هم‌اکنون غیر فعال است';
$string['generalfeedback'] = 'بازخورد عمومی';
$string['generalfeedback_help'] = 'بازخورد عمومی، متنی است که پس از اینکه به یک سؤال پاسخ داده شد نمایش داده می‌شود. بر خلاف بازخورد متعلق به یک سؤال خاص که به پاسخ ارائه شده بستگی دارد، همواره بازخورد عمومی یکسانی نمایش داده می‌شود.';
$string['graceperiod'] = 'مهلت تحویل پاسخ‌ها';
$string['graceperiod_desc'] = 'اگر تنظیم «وقتی زمان به‌پایان می‌رسد» روی «مهلتی برای تحویل در نظر گرفته شود، ولی پاسخ‌ها قابل تغییر نباشند» تعیین شده باشد، این مقدار زمان اضافهٔ پیش‌فرضی است که برای تحویل داده خواهد شد.';
$string['graceperiod_help'] = 'اگر تنظیم «وقتی زمان به‌پایان می‌رسد» روی «مهلتی برای تحویل در نظر گرفته شود، ولی پاسخ‌ها قابل تغییر نباشند» تعیین شده باشد، این مقدار زمان اضافه‌ای است که برای تحویل داده خواهد شد.';
$string['graceperiodmin'] = 'آخرین مهلت تحویل پاسخ‌ها';
$string['grade'] = 'نمره';
$string['gradeall'] = 'نمره‌دهی همه';
$string['gradeaverage'] = 'نمرهٔ میانگین';
$string['gradeboundary'] = 'کران نمره';
$string['gradeessays'] = 'نمره دادن به سؤال‌های تشریحی';
$string['gradehighest'] = 'بیشترین نمره';
$string['grademethod'] = 'روش نمره دادن';
$string['grademethod_help'] = 'در صورتی که چند بار شرکت در آزمون مجاز باشد، روش‌های زیر برای محاسبهٔ نمرهٔ نهایی آزمون موجود هستند:

* بیشترین نمره در کل دفعات
* نمرهٔ میانگین کل دفعات
* نمرهٔ اولین دفعه (نمرهٔ دفعات بعدی نادیده گرفته می‌شود)
* نمرهٔ آخرین دفعه (نمرهٔ دفعات قبلی نادیده گرفته می‌شود)';
$string['gradesdeleted'] = 'نمره‌های آزمون پاک شدند';
$string['gradesofar'] = '{$a->method}: {$a->mygrade} از {$a->quizgrade}.';
$string['gradingdetails'] = 'نمرهٔ این دفعه: {$a->raw} از {$a->max}.';
$string['gradingdetailsadjustment'] = 'با احتساب جریمه‌های قبلی، نمرهٔ <strong>{$a->cur} از {$a->max}</strong> را برای این سؤال کسب می‌کنید.';
$string['gradingdetailspenalty'] = 'این ارائه {$a} جریمه کسب کرده است.';
$string['gradingdetailszeropenalty'] = 'برای این ارائه جریمه نشدید.';
$string['gradingmethod'] = 'نحوهٔ محاسبهٔ نهایی نمره: {$a}';
$string['groupoverrides'] = 'بازنویسی‌های مربوط به گروه‌ها';
$string['groupsnone'] = 'هیچ گروهی در این درس وجود ندارد';
$string['guestsno'] = 'با عرض پوزش، مهمان‌ها قادر به دیدن یا شرکت کردن در آزمون‌ها نیستند';
$string['hidebreaks'] = 'عدم نمایش محل شروع صفحه‌های جدید';
$string['hidereordertool'] = 'پنهان کردن ابزار مرتب‌سازی';
$string['history'] = 'تاریخچهٔ پاسخ‌ها:';
$string['imagedisplay'] = 'عکس مربوط به سوال جهت نمایش';
$string['importcategory'] = 'طبقهٔ ورودی';
$string['importerror'] = 'خطایی در حین فرآیند ورود رخ داد';
$string['importfilearea'] = 'ورود از یکی از فایل‌های موجود در درس...';
$string['importfileupload'] = 'ورود از طریق ارسال فایل...';
$string['importfromthisfile'] = 'ورود سؤال‌ها از این فایل';
$string['import_help'] = 'با استفاده از این قابلیت می‌توانید سؤال‌هایی با قالب‌های مختلف را از طریق فایل‌های متنی وارد کنید.

اگر فایل‌هایتان محتوی حروف غیر اسکی است، باید از کدگذاری <span dir="ltr" style="display:inline-block;direction:ltr">UTF-8</span> استفاده کند. به طور خاص در مورد فایل‌هایی که توسط مجموعهٔ آفیس مایکروسافت تولید شده‌اند مراقب باشید. زیرا این فایل‌ها معمولا از کدگذاری‌های بخصوصی استفاده می‌کنند که به نحو مناسبی مدیریت نخواهند شد.

قالب‌های ورود و صدور، منابع قابل اتصالی هستند. قالب‌های دلخواه دیگری را می‌توان از پایگاه ماژول‌ها و پلاگین‌ها دریافت کرد.';
$string['importingquestions'] = 'در حال وارد کردن {$a} سؤال از فایل';
$string['importmax10error'] = 'خطایی در سؤال وجود دارد. نمی‌توانید بیش از ۱۰ پاسخ داشته باشید';
$string['importmaxerror'] = 'خطایی در سؤال وجود دارد. تعداد پاسخ‌ها خیلی زیاد است.';
$string['importquestions'] = 'ورود سؤال‌ها از فایل';
$string['inactiveoverridehelp'] = '* شاگرد عضو گروه مناسب یا دارای نقش مناسب برای شرکت در آزمون نیست';
$string['incorrect'] = 'نادرست';
$string['indivresp'] = 'پاسخ‌های ارائه شده توسط هر یک از افراد به هر یک از موردها';
$string['info'] = 'اطلاعات';
$string['infoshort'] = 'ت';
$string['initialnumfeedbacks'] = 'تعداد اولیهٔ فیلدهای بازخورد کلی';
$string['initialnumfeedbacks_desc'] = 'هنگام ساختن یک آزمون جدید، این تعداد جای خالی برای وارد کردن بازخوردهای کلی در فرم آزمون نشان داده می‌شود. پس از اینکه آزمون ساخته شد، فرم ویرایش آزمون دقیقا به تعداد بازخوردهای کلی از پیش تعیین شده، فیلد نمایش خواهد داد. این تنظیم باید حداقل ۱ باشد.';
$string['inprogress'] = 'در جریان';
$string['introduction'] = 'توصیف';
$string['invalidattemptid'] = 'این شناسهٔ تلاش وجود ندارد';
$string['invalidcategory'] = 'شناسهٔ طبقه نامعتبر است';
$string['invalidoverrideid'] = 'شناسهٔ بازنویسی نامعتبر';
$string['invalidquestionid'] = 'شناسهٔ سؤال نامعتبر';
$string['invalidquizid'] = 'شناسهٔ آزمون نامعتبر';
$string['invalidsource'] = 'این منبع به عنوان معتبر پذیرفته نیست.';
$string['invalidsourcetype'] = 'نوع منبع نامعتبر.';
$string['invalidstateid'] = 'شناسهٔ وضعیت نامعتبر';
$string['lastanswer'] = 'آخرین پاسخ شما:';
$string['layout'] = 'چیدمان';
$string['layoutasshown'] = 'چیدمان صفحه، آنگونه که نمایش داده می‌شود.';
$string['layoutasshownwithpages'] = 'چیدمان صفحه، آنگونه که نمایش داده می‌شود. <small>(شروع خودکار در یک صفحهٔ جدید به ازای هر {$a} سؤال)</small>';
$string['layoutshuffledandpaged'] = '{$a} سؤال در هر صفحه و با ترتیب تصادفی.';
$string['layoutshuffledsinglepage'] = 'همهٔ سؤال‌هادر یک صفحه و با ترتیب تصادفی.';
$string['link'] = 'پیوند';
$string['listitems'] = 'فهرست مندرجات آزمون';
$string['literal'] = 'لفظی';
$string['loadingquestionsfailed'] = 'بارگذاری سؤال‌ها با مشکل مواجه شد: {$a}';
$string['makecopy'] = 'ذخیره به‌عنوان یک سؤال جدید';
$string['managetypes'] = 'مدیریت انواع سؤال و کارگزارها';
$string['manualgrading'] = 'نمره دادن';
$string['mark'] = 'ثبت';
$string['markall'] = 'ارائه صفحه';
$string['marks'] = 'جمع نمره';
$string['marks_help'] = 'نمره‌های عددی برای هر سؤال، و نمرهٔ نهایی شرکت در آزمون.';
$string['match'] = 'جور کردنی';
$string['matchanswer'] = 'پاسخ جور کردنی';
$string['matchanswerno'] = 'پاسخ جور کردنی {$a}';
$string['max'] = 'حداکثر';
$string['maxmark'] = 'حداکثر نمره';
$string['messageprovider:attempt_overdue'] = 'اخطار زمانی که مهلت تلاش شما در آزمون به‌پایان می‌رسد';
$string['messageprovider:confirmation'] = 'تاییدیه شرکت‌کردن خودتان در آزمون (ثبت پاسخ‌ها)';
$string['messageprovider:submission'] = 'خبر شرکت کردن‌ها در آزمون (ثبت پاسخ‌ها)';
$string['min'] = 'حداقل';
$string['minutes'] = 'دقیقه';
$string['missingcorrectanswer'] = 'پاسخ صحیح باید مشخص شود';
$string['missingitemtypename'] = 'نام وارد نشده است';
$string['missingquestion'] = 'به‌نظر می‌رسد این سؤال دیگر وجود ندارد';
$string['modulename'] = 'آزمون';
$string['modulename_help'] = 'ماژول آزمون اساتید را قادر به طراحی و تعیین آزمون‌هایی متشکل از سؤال‌هایی از انواع چند گزینه‌ای، جورکردنی، کوتاه‌جواب، عددی و غیره می‌کند.

استاد می‌تواند اجازه دهد که بتوان چند بار در آزمون شرکت کرد و ترتیب سوال‌های آزمون به‌هم بریزد یا اینکه سوال‌ها به‌طور تصادفی از یک بانک سوال انتخاب شوند. می‌توان برای شرکت در آزمون محدودیت زمانی هم تعیین کرد.

هر بار شرکت در آزمون به طور خودکار نمره داده می‌شود (به‌استثنای سوال‌های تشریحی) و این نمره در دفتر نمره ثبت می‌شود.

استاد می‌تواند انتخاب کند که چه‌زمانی راهنمایی‌ها، بازخوردها و پاسخ‌های صحیح به شاگردان نمایش داده شوند یا اینکه هر کدام از این موارد نمایش داده شوند یا خیر.

از آزمون‌ها می‌توان در موارد زیر استفاده کرد

* به‌عنوان امتحان درسی
* به‌عنوان یک آزمون کوچک در انتهای یک موضوع درسی
* به‌عنوان امتحان آزمایشی با استفاده از سوال‌های امتحان‌های گذشته
* برای ارائه بازخورد سریع دربارهٔ عملکرد
* برای خودآزمایی';
$string['modulenameplural'] = 'آزمون‌ها';
$string['moveselectedonpage'] = 'انتقال سؤال‌های انتخاب شده به صفحهٔ: {$a}';
$string['multichoice'] = 'چند گزینه‌ای';
$string['multipleanswers'] = 'حداقل یکی از گزینه‌ها را انتخاب کنید.';
$string['mustbesubmittedby'] = 'تا پیش از {$a} باید پاسخ‌های خود را ثبت کنید.';
$string['name'] = 'نام';
$string['navigatenext'] = 'صفحهٔ بعد';
$string['navigateprevious'] = 'صفحهٔ قبل';
$string['navmethod'] = 'نحوهٔ حرکت در آزمون';
$string['navmethod_free'] = 'آزاد';
$string['navmethod_help'] = 'اگر حرکت ترتیبی فعال باشد، شاگرد باید به‌ترتیب نمایش سوال‌ها در آزمون پیشروی کند و نمی‌تواند به صفحه‌های قبلی بازگردد یا از روی صفحه‌ای بپرد.';
$string['navmethod_seq'] = 'به‌ترتیب';
$string['navnojswarning'] = 'هشدار: این پیوندها پاسخ‌های شما را ذخیره نخواهند کرد. از دکمهٔ «بعدی» در پائین صفحه استفاده کنید.';
$string['neverallononepage'] = 'هیچ - تمام سؤال‌ها در یک صفحه';
$string['newattemptfail'] = 'خطا: آغاز یک تلاش جدید در آزمون مقدور نبود';
$string['newcategory'] = 'طبقهٔ جدید';
$string['newpage'] = 'صفحهٔ جدید به ازای ...';
$string['newpageevery'] = 'شروع خودکار یک صفحهٔ جدید';
$string['newpage_help'] = 'در آزمون‌های طولانی‌تر، منطقی است که با محدود کردن تعداد سؤال‌ها در هر صفحه، آزمون را در چند صفحه بسط داد. هنگام اضافه کردن سؤال به آزمون، صفحه‌های جدید به صورت خودکار (بر اساس این تنظیم) ایجاد خواهند شد. البته، محل قطع صفحه و شروع صفحه‌های جدید به صورت دستی در صفحهٔ ویرایش قابل تغییر است.';
$string['noanswers'] = 'هیچ پاسخی انتخاب نشده بود!';
$string['noattempts'] = 'کسی در این آزمون شرکت نکرده است';
$string['noattemptstoshow'] = 'هیچ تلاش برای نمایش وجود ندارد';
$string['nocategory'] = 'طبقه اشتباه است یا اینکه اصلا مشخص نشده است';
$string['noclose'] = 'بدون تاریخ بسته شدن';
$string['nocommentsyet'] = 'هنوز نظری داده نشده است.';
$string['noconnection'] = 'هم اکنون اتصال به وب سرویسی که بتواند این سؤال را پردازش کند برقرار نیست. لطفا با مدیر سیستم تماس بگیرید.';
$string['nodataset'] = 'هیچ - این یک wild card نیست';
$string['nodatasubmitted'] = 'هیچ اطلاعاتی ارسال نشده است.';
$string['noessayquestionsfound'] = 'هیچ سؤالی که نیاز به نمره دادن به صورت دستی داشته باشد پیدا نشد';
$string['nogradewarning'] = 'این آزمون نمره ندارد. در نتیجه نمی‌توانید بر اساس نمره بازخوردهای متفاوتی تعیین کنید.';
$string['nomoreattempts'] = 'شرکت در این آزمون دیگر مجاز نیست';
$string['none'] = 'هیچ';
$string['noopen'] = 'بدون تاریخ باز شدن';
$string['nooverridedata'] = 'حداقل یکی از تنظیمات آزمون را باید بازنویسی کنید.';
$string['nopossibledatasets'] = 'بدون مجموعه دادهٔ ممکن';
$string['noquestionintext'] = 'متن سؤال شامل هیچ سؤال جای خالی‌ای نیست';
$string['noquestions'] = 'هنوز سؤالی اضافه نشده است';
$string['noquestionsfound'] = 'سؤالی پیدا نشد';
$string['noquestionsinquiz'] = 'هیچ سؤالی در این آزمون وجود ندارد.';
$string['noquestionsnotinuse'] = 'این سؤال تصادفی مورد استفاده قرار نگرفته است. زیرا طبقه‌اش خالی است.';
$string['noquestionsonpage'] = 'صفحه خالی';
$string['noresponse'] = 'بدون پاسخ';
$string['noreview'] = 'شما اجازهٔ مرور این آزمون را ندارید';
$string['noreviewshort'] = 'مجاز نیست';
$string['noreviewuntil'] = 'اجازهٔ مرور این آزمون را تا قبل از {$a} ندارید';
$string['noreviewuntilshort'] = '{$a} در دسترس';
$string['noscript'] = 'جاوا اسکریپت باید فعال شده باشد تا بتوانید ادامه دهید!';
$string['notavailabletostudents'] = 'توجه: این آزمون در حال حاضر توسط شاگردان شما قابل دسترسی نیست';
$string['notenoughsubquestions'] = 'تعداد زیر-سؤال‌های تعریف شده کافی نیست!<br />آیا مایلید که بازگردید و مشکل این سؤال را برطرف کنید؟';
$string['notimedependentitems'] = 'موارد وابسته به زمان در حال حاضر توسط ماژول آزمون پشتیبانی نمی‌شوند. به عنوان یک راه حل، یک محدودیت زمانی برای کل آزمون تعیین کنید. آیا مایلید که از مورد دیگری استفاده کنید (یا اینکه هنوز هم می‌خواهید از همین مورد استفاده کنید)؟';
$string['notyourattempt'] = 'این تلاش متعلق به شما نیست!';
$string['noview'] = 'کاربر وارد شده مجاز به دیدن این آزمون نیست';
$string['numattempts'] = '{$a->attemptnum} بار شرکت در این آزمون توسط {$a->studentnum} {$a->studentstring} صورت گرفته است';
$string['numattemptsmade'] = '{$a} بار شرکت در این آزمون صورت گرفته است';
$string['numerical'] = 'عددی';
$string['numquestionsx'] = 'تعداد سؤال‌ها: {$a}';
$string['oneminute'] = '۱ دقیقه';
$string['onlyteachersexport'] = 'فقط اساتید می‌توانند سؤال‌ها را صادر کنند';
$string['onlyteachersimport'] = 'فقط اساتید دارای مجوز ویرایش می‌توانند سؤال‌ها را وارد کنند';
$string['open'] = 'پاسخ داده نشد';
$string['openclosedatesupdated'] = 'تاریخ‌های باز شدن و بسته شدن آزمون به‌روز شد';
$string['optional'] = 'اختیاری';
$string['orderandpaging'] = 'ترتیب و صفحه‌بندی';
$string['orderandpaging_help'] = 'اعداد ۱۰، ۲۰، ۳۰، ... در مقابل سوال‌ها نشان‌دهنده ترتیب سوال‌ها هستند. اعداد ۱۰ تا ۱۰ تا اضافه می‌شوند تا فضای کافی برای قرار دادن سوالی بین دو سوال دیگر وجود داشته باشد. برای مرتب کردن سوال‌ها، این عددها را تغییر دهید و سپس بر روی دکمه «مرتب کردن سؤال‌ها» کلیک کنید.

برای درج کردن صفحه‌های جدید پس از سؤال‌های مورد نظر، مربع کنار سؤال‌ها را علامت بزنید و بر روی دکمهٔ «درج صفحه‌های جدید بعد از سؤال‌های انتخاب شده» کلیک کنید.

برای تقسیم سؤال‌ها بین چند صفحه، روی دکمهٔ صفحه‌بندی مجدد کلیک کنید و تعداد سؤال‌های دلخواه در هر صفحه را انتخاب نمائید.';
$string['orderingquiz'] = 'ترتیب و صفحه‌بندی';
$string['outof'] = '{$a->grade} از {$a->maxgrade}';
$string['outofpercent'] = '{$a->grade} از {$a->maxgrade} (٪{$a->percent})';
$string['outofshort'] = '{$a->grade} از {$a->maxgrade}';
$string['overallfeedback'] = 'بازخورد کلی';
$string['overallfeedback_help'] = 'بازخورد کلی، متنی است که پس از شرکت در آزمون نشان داده می‌شود. با مشخص کردن کران‌های بیشتر (به صورت درصد یا عدد)، می‌توان کاری کرد که متن نشان داده شده به نمرهٔ کسب شده بستگی داشته باشد.';
$string['overdue'] = 'تأخیر تحویل';
$string['overduehandling'] = 'وقتی زمان به‌پایان می‌رسد';
$string['overduehandlingautoabandon'] = 'آزمون باید پیش از اینکه زمان به پایان برسد تحویل داده شود، در غیر اینصورت شرکت در آزمون محسوب نخواهد شد';
$string['overduehandlingautosubmit'] = 'تلاش کاربر در آزمون به‌طور خودکار تحویل داده شود';
$string['overduehandlinggraceperiod'] = 'مهلتی در نظر گرفته شود تا پاسخ‌های ثبت‌شده تحویل داده شوند، ولی سوال اضافه‌تری قابل پاسخ نیست';
$string['overduehandling_help'] = 'این تنظیم تعیین می‌کند که اگر کاربر تا پیش از اتمام وقت آزمون را تحویل نداده باشد چه اتفاقی بیافتد. اگر در هنگام تمام شدن وقت، کاربر به‌طور فعال در حال شرکت در آزمون باشد آنگاه زمان‌سنج شمارش معکوسی که در هنگام آزمون نمایش داده می‌شود به‌طور خودکار تلاش کاربر را ثبت و تحویل می‌دهد. ولی اگر کاربر از سایت خارج شده باشد، آنگاه این تنظیم تعیین می‌کند که چه اتفاقی بیافتد.';
$string['override'] = 'بازنویسی';
$string['overridedeletegroupsure'] = 'آیا مطمئنید که می‌خواهید بازنویسی صورت گرفته برای گروه {$a} را حذف کنید؟';
$string['overridedeleteusersure'] = 'آیا مطمئنید که می‌خواهید بازنویسی صورت گرفته برای کاربر {$a} را حذف کنید؟';
$string['overridegroup'] = 'گروه مورد بازنویسی';
$string['overridegroupeventname'] = '{$a->quiz} - {$a->group}';
$string['overrides'] = 'بازنویسی‌ها';
$string['overrideuser'] = 'کاربر مورد بازنویسی';
$string['overrideusereventname'] = '{$a->quiz} - بازنویسی';
$string['page-mod-quiz-attempt'] = 'صفحهٔ شرکت در آزمون';
$string['page-mod-quiz-edit'] = 'صفحهٔ ویرایش آزمون';
$string['page-mod-quiz-report'] = 'تمام صفحه‌های گزارش آزمون';
$string['page-mod-quiz-review'] = 'صفحهٔ مرور شرکت در آزمون';
$string['page-mod-quiz-summary'] = 'صفحهٔ خلاصه وضعیت شرکت در آزمون';
$string['page-mod-quiz-view'] = 'صفحهٔ اطلاعات آزمون';
$string['page-mod-quiz-x'] = 'هر صفحه‌ای از ماژول آزمون';
$string['pageshort'] = 'ص';
$string['pagesize'] = 'تعداد تلاش‌های نمایش داده شده در هر صفحه:';
$string['parent'] = 'ایجاد در';
$string['parentcategory'] = 'طبقهٔ مادر';
$string['parsingquestions'] = 'تجزیهٔ سؤال‌ها از فایل وارد شده.';
$string['partiallycorrect'] = 'نیمه درست';
$string['penalty'] = 'جریمه';
$string['penaltyscheme'] = 'اعمال جریمه';
$string['penaltyscheme_help'] = 'در صورت فعال بودن، بازای هر پاسخ نادرست، جریمه‌ای از نمرهٔ نهایی کسر می‌شود. میران جریمه در تنظیمات مربوط به هر سؤال تعیین می‌شود. جریمه‌ها فقط در صورت فعال بودن حالت سازگار اعمال می‌شوند.';
$string['percentcorrect'] = 'درصد صحیح';
$string['pleaseclose'] = 'درخواست شما پردازش شده است. اکنون می‌توانید این پنجره را ببندید';
$string['pluginadministration'] = 'مدیریت آزمون';
$string['pluginname'] = 'آزمون';
$string['popup'] = 'نمایش آزمون در یک پنجرهٔ «امن»';
$string['popupblockerwarning'] = 'این بخش از آزمون در حالت امن می‌باشد. یعنی شما باید در یک پنجرهٔ امن در آزمون شرکت نمائید. لطفاً برنامه‌های مسدود کردن پاپ-آپ را غیرفعال کنید. با تشکر.';
$string['popupnotice'] = 'شاگردها این آزمون را در یک پنجرهٔ امن خواهند دید';
$string['preprocesserror'] = 'هنگام پیش‌پردازش خطایی رخ داد!';
$string['preview'] = 'پیش‌نمایش';
$string['previewquestion'] = 'پیش‌نمایش سؤال';
$string['previewquiz'] = 'پیش نمایش {$a}';
$string['previewquiznow'] = 'پیش‌نمایش آزمون';
$string['previous'] = 'حالت قبلی';
$string['privacy:metadata:core_question'] = 'فعالیت آزمون اطلاعات استفاده از سؤال‌ها را در زیرسیستم core_question نگهداری می‌کند.';
$string['privacy:metadata:quiz'] = 'فعالیت آزمون از گزارش‌های آزمون استفاده می‌کند.';
$string['privacy:metadata:quizaccess'] = 'فعالیت آزمون از شرایط دسترسی به آزمون استفاده می‌کند.';
$string['privacy:metadata:quiz_attempts'] = 'جزئیات مربوط به هر دفعه تلاش در آزمون.';
$string['privacy:metadata:quiz_attempts:attempt'] = 'دفعهٔ تلاش.';
$string['privacy:metadata:quiz_attempts:currentpage'] = 'صفحهٔ فعلی که کاربر در آن است.';
$string['privacy:metadata:quiz_attempts:preview'] = 'اینکه آیا این یک پیش‌نمایش از آزمون است یا خیر.';
$string['privacy:metadata:quiz_attempts:state'] = 'وضعیت فعلی تلاش.';
$string['privacy:metadata:quiz_attempts:sumgrades'] = 'جمع نمره‌ها در تلاش.';
$string['privacy:metadata:quiz_attempts:timecheckstate'] = 'زمانی که وضعیت بررسی شد.';
$string['privacy:metadata:quiz_attempts:timefinish'] = 'زمانی که تلاش کامل شد.';
$string['privacy:metadata:quiz_attempts:timemodified'] = 'زمانی که تلاش به‌روز شد.';
$string['privacy:metadata:quiz_attempts:timestart'] = 'زمانی که تلاش شروع شد.';
$string['privacy:metadata:quiz_grades'] = 'جزئیات مربوط به نمرهٔ کل برای این آزمون.';
$string['privacy:metadata:quiz_grades:grade'] = 'نمرهٔ کل برای این آزمون.';
$string['publish'] = 'انتشار';
$string['publishedit'] = 'برای اضافه کردن یا ویرایش سؤال‌های این طبقه، باید مجوز لازم در درس مورد انتشار را داشته باشید';
$string['qname'] = 'نام';
$string['qti'] = 'قالب IMS QTI';
$string['qtypename'] = 'نوع، نام';
$string['question'] = 'سؤال';
$string['questionbank'] = 'از بانک سؤال';
$string['questionbankmanagement'] = 'مدیریت بانک سؤال';
$string['questionbehaviour'] = 'رفتار سؤال';
$string['questioncats'] = 'طبقه‌های سؤال‌ها';
$string['questiondeleted'] = 'این سؤال پاک شده است. لطفا با استادتان تماس بگیرید';
$string['questiondependsonprevious'] = 'برای دیدن این سوال باید ابتدا سوال قبل را پاسخ دهید.';
$string['questioninuse'] = 'سؤال «{$a->questionname}» هم‌اکنون در آزمون(های) زیر در حال استفاده شدن است: <br />{$a->quiznames}<br />این سؤال فقط از لیست سؤال‌های طبقه حذف می‌شود، ولی از آن آزمون‌ها حذف نخواهد شد.';
$string['questionmissing'] = 'سؤال مربوط به این جلسه پیدا نشد';
$string['questionname'] = 'نام سؤال';
$string['questionnotloaded'] = 'سؤال {$a} از پایگاه داده بارگذاری نشده است';
$string['questionorder'] = 'ترتیب سؤال‌ها';
$string['questions'] = 'سؤال‌ها';
$string['questionsinclhidden'] = 'سؤال‌ها (شامل سؤال‌های پنهان)';
$string['questionsinthisquiz'] = 'سؤال‌های این آزمون';
$string['questionsmatchingfilter'] = 'سؤال‌هایی که با این فیلتر منطبق هستند: {$a}';
$string['questionsperpage'] = 'تعداد سؤال‌ها در هر صفحه';
$string['questionsperpageselected'] = 'تعداد سؤال‌ها در هر صفحه تعیین شده است و صفحه‌بندی در حال حاضر ثابت است. در نتیجه، کنترل‌های کرب.ط به صفحه‌بندی غیر فعال شده‌اند. این تنظیم را در {$a} می‌توانید تغییر دهید.';
$string['questionsperpagex'] = 'تعداد سؤال‌ها در هر صفحه: {$a}';
$string['questiontext'] = 'متن سؤال';
$string['questiontextisempty'] = '[متن سؤال خالی]';
$string['questiontype'] = 'نوع سؤال {$a}';
$string['questiontypesetupoptions'] = 'گزینه‌های راه‌اندازی برای هر نوع سؤال:';
$string['quiz:addinstance'] = 'اضافه‌کردن یک آزمون جدید';
$string['quiz:attempt'] = 'شرکت در آزمون‌ها';
$string['quizavailable'] = 'آزمون تا {$a} در دسترس است';
$string['quizclose'] = 'بسته شدن آزمون';
$string['quizclosed'] = 'این آزمون در {$a} بسته شد';
$string['quizcloses'] = 'بسته شدن آزمون';
$string['quizcloseson'] = 'این آزمون {$a} بسته خواهد شد';
$string['quiz:deleteattempts'] = 'حذف دفعات شرکت در آزمون';
$string['quiz:emailconfirmsubmission'] = 'دریافت نامهٔ تأیید هنگام ارائه کردن';
$string['quiz:emailnotifysubmission'] = 'دریافت نامهٔ اطلاع‌رسانی برای ارائه‌ها';
$string['quiz:emailwarnoverdue'] = 'دریافت پیام اطلاع‌رسانی هنگامی که از موعد یک تلاش در آزمون می‌گذرد و نیازمند تحویل است.';
$string['quiz:grade'] = 'نمره‌دهی آزمون‌ها به صورت دستی';
$string['quiz:ignoretimelimits'] = 'نادیده گرفتن محدودیت زمانی برای آزمون‌ها';
$string['quizisclosed'] = 'این آزمون تمام شده است';
$string['quizisopen'] = 'این آزمون باز است';
$string['quizisopenwillclose'] = 'آزمون باز است ({$a} بسته می‌شود)';
$string['quiz:manage'] = 'مدیریت آزمون‌ها';
$string['quiz:manageoverrides'] = 'مدیریت بازنویسی‌ها در آزمون';
$string['quiznavigation'] = 'راهبری آزمون';
$string['quizopen'] = 'باز شدن آزمون';
$string['quizopenclose'] = 'زمان‌های باز و بسته شدن';
$string['quizopenclose_help'] = 'شاگردان تنها پس از زمان تعیین شده به عنوان زمان باز شدن آزمون می‌توانند در آزمون شرکت کنند و شرکت خود را تا پیش از زمان بسته شدن باید کامل کنند.';
$string['quizopened'] = 'آزمون باز است.';
$string['quizopenedon'] = 'این آزمون در {$a} باز شد';
$string['quizopens'] = 'باز شدن آزمون';
$string['quizopenwillclose'] = 'این آزمون باز است. در {$a} بسته خواهد شد در';
$string['quizordernotrandom'] = 'ترتیب سؤال‌های آزمون بهم ریخته نشده است';
$string['quizorderrandom'] = '* ترتیب سوال‌های آزمون به‌هم ریخته شده است';
$string['quiz:preview'] = 'پیش‌نمایش آزمون‌ها';
$string['quiz:regrade'] = 'نمره‌دهی مجدد تلاش‌ها';
$string['quiz:reviewmyattempts'] = 'مرور دفعات قبلی شرکت خود در آزمون';
$string['quizsettings'] = 'تنظیمات آزمون';
$string['quiz:view'] = 'مشاهدهٔ اطلاعات آزمون';
$string['quiz:viewreports'] = 'مشاهدهٔ گزارش‌های آزمون';
$string['quizwillopen'] = 'این آزمون در {$a} باز خواهد شد';
$string['random'] = 'سؤال تصادفی';
$string['randomcreate'] = 'ساختن سؤال‌های تصادفی';
$string['randomediting'] = 'در حال ویرایش یک سؤال تصادفی';
$string['randomfromcategory'] = 'سؤال تصادفی از طبقهٔ:';
$string['randomfromexistingcategory'] = 'سوال تصادفی از یک طبقهٔ موجود';
$string['randomnosubcat'] = 'فقط از سؤال‌های این طبقه، نه زیرطبقه‌هایش.';
$string['randomnumber'] = 'تعداد سؤال‌های تصادفی';
$string['randomquestion'] = 'سؤال تصادفی';
$string['randomquestion_help'] = 'سؤال تصادفی، راهی برای درج یک سؤال است که در زمان آزمون به‌طور تصادفی از بین تمام سؤال‌های موجود در یک طبقه یا سؤال‌هایی که دارای یک سری برچسب خاص در یک طبقه هستند انتخاب خواهد شد.';
$string['randomquestiontags'] = 'برچسب‌ها';
$string['randomquestiontags_help'] = 'ضوابط انتخاب را با مشخص کردن یک سری برچسب در اینجا می‌توانید محدودتر کنید.

سؤال‌های «تصادفی» از بین سؤال‌هایی انتخاب می‌شوند که دارای تمام این برچسب‌ها باشند.';
$string['randomquestionusinganewcategory'] = 'سوال تصادفی با استفاده از یک طبقهٔ جدید';
$string['randomwithsubcat'] = 'از سؤال‌های این طبقه و زیرطبقه‌هایش.';
$string['readytosend'] = 'شما در آستانهٔ ارائه کل پاسخ‌هایتان برای تصحیح هستید. آیا مطمئنید که می‌خواهید ادامه دهید؟';
$string['reattemptquiz'] = 'شرکت مجدد در آزمون';
$string['recentlyaddedquestion'] = 'سؤال اخیراً اضافه شده!';
$string['recurse'] = 'سؤال‌های تعریف‌شده در زیرطبقه‌ها هم نمایش داده شوند';
$string['regrade'] = 'نمره‌دهی مجدد همهٔ تلاش‌ها';
$string['regradecomplete'] = 'همهٔ تلاش‌ها مجددا نمره‌دهی شده‌اند';
$string['regradecount'] = '{$a->changed} نمره از {$a->attempt} نمره تغییر کرد';
$string['regradedisplayexplanation'] = 'دفعاتی که طی نمره‌دهی مجدد تغییر کرده‌اند به صورت پیوند به پنجرهٔ مرور سؤال نمایش داده شده‌اند';
$string['regradenotallowed'] = 'شما مجوز نمره‌دهی مجدد این آزمون را ندارید';
$string['regradingquestion'] = 'نمره‌دهی مجدد «{$a}».';
$string['regradingquiz'] = 'نمره‌دهی مجدد آزمون «{$a}»';
$string['remove'] = 'حذف';
$string['removeallgroupoverrides'] = 'پاک کردن تمام بازنویسی‌های صورت گرفته برای گروه‌ها';
$string['removeallquizattempts'] = 'پاک کردن تمام تلاش‌های صورت گرفته در آزمون';
$string['removealluseroverrides'] = 'پاک کردن تمام بازنویسی‌های صورت گرفته برای کاربران';
$string['removeemptypage'] = 'حذف صفحهٔ خالی';
$string['removepagebreak'] = 'ادغام صفحه‌ها';
$string['removeselected'] = 'حذف موارد انتخاب شده';
$string['rename'] = 'تغییر نام';
$string['renderingserverconnectfailed'] = 'کارگزار {$a} قادر به پردازش درخواست RQP نبود. بررسی کنید که آدرس کارگزار درست باشد.';
$string['reorderquestions'] = 'مرتب کردن سؤال‌ها';
$string['reordertool'] = 'نمایش ابزار مرتب‌سازی';
$string['repaginate'] = 'صفحه‌بندی مجدد به صورت {$a} سؤال در هر صفحه';
$string['repaginatecommand'] = 'صفحه‌بندی مجدد';
$string['repaginatenow'] = 'هم‌اکنون صفحه‌بندی مجدد شود';
$string['replace'] = 'جایگزینی';
$string['replacementoptions'] = 'اختیارات جایگزینی';
$string['report'] = 'گزارش‌ها';
$string['reportanalysis'] = 'تحلیل مورد';
$string['reportattemptsfrom'] = 'شامل تلاش‌های';
$string['reportattemptsthatare'] = 'شامل تلاش‌های';
$string['reportdisplayoptions'] = 'گزینه‌های نمایش';
$string['reportfullstat'] = 'آمارهای تفصیلی';
$string['reportmulti_percent'] = 'چند درصدی';
$string['reportmulti_q_x_student'] = 'گزینه‌های چند شاگردی';
$string['reportmulti_resp'] = 'پاسخ‌های فردی';
$string['reportnotfound'] = 'گزارش شناخته شده نیست ({$a})';
$string['reportoverview'] = 'مرور کلی';
$string['reportregrade'] = 'نمره‌دهی مجدد تلاش‌ها';
$string['reportresponses'] = 'پاسخ‌های تفصیلی';
$string['reports'] = 'گزارش‌ها';
$string['reportshowonly'] = 'تنها نمایش تلاش‌هایی';
$string['reportshowonlyfinished'] = 'حداکثر یک تلاش پایان‌یافته بازای هر کاربر نشان داده شود ({$a})';
$string['reportsimplestat'] = 'آمارهای ساده';
$string['reportusersall'] = 'تمام کاربرانی که در آزمون شرکت کرده‌اند';
$string['reportuserswith'] = 'کاربران ثبت‌نام‌شده که در آزمون شرکت کرده‌اند';
$string['reportuserswithorwithout'] = 'کاربران ثبت‌نام‌شده که در آزمون شرکت کرده‌اند یا نکرده‌اند';
$string['reportuserswithout'] = 'کاربران ثبت‌نام‌شده که در آزمون شرکت نکرده‌اند';
$string['reportwhattoinclude'] = 'چه مواردی در گزارش شامل شوند';
$string['requirepassword'] = 'نیاز به رمز ورود';
$string['requirepassword_help'] = 'اگر در این قسمت رمزی مشخص شده باشد، شاگردان برای شرکت در آزمون باید آن را وارد کنند.';
$string['requiresubnet'] = 'آدرس شبکه مورد نیاز';
$string['requiresubnet_help'] = 'دسترسی به آزمون را می‌توان محدود به زیرشبکه‌های خاصی از شبکهٔ محلی یا اینترنت نمود. برای این کار باید لیستی از آدرس (یا بخشی از آدرس) های IP را به صورت جدا شده با کاما در این قسمت مشخص کرد. انجام این کار در یک آزمون تحت مراقبت و نظارت، برای اطمینان از اینکه فقط افراد واقع در یک مکان خاص می‌توانند به آزمون دسترسی داشته باشند، می‌تواند مفید باشد.';
$string['response'] = 'پاسخ';
$string['responses'] = 'پاسخ‌های ارائه شده';
$string['results'] = 'نتایج';
$string['returnattempt'] = 'بازگشت و ادامهٔ آزمون';
$string['reuseifpossible'] = 'از آنچه قبلا حذف شده استفاده شود';
$string['reverttodefaults'] = 'بازگشت به پیش‌فرض‌های آزمون';
$string['review'] = 'مرور';
$string['reviewafter'] = 'اجازهٔ مرور پس از بسته شدن آزمون';
$string['reviewalways'] = 'اجازهٔ مرور در هر لحظه‌ای';
$string['reviewattempt'] = 'مرور آزمون';
$string['reviewbefore'] = 'اجازهٔ مرور تا وقتی که آزمون باز است';
$string['reviewclosed'] = 'پس از بسته شدن آزمون';
$string['reviewduring'] = 'در هنگام آزمون';
$string['reviewimmediately'] = 'بلافاصله بعد از شرکت در آزمون';
$string['reviewnever'] = 'بدون اجازهٔ مرور';
$string['reviewofattempt'] = 'مرور دفعهٔ {$a} ام شرکت در آزمون';
$string['reviewofpreview'] = 'مرور پیش‌نمایش';
$string['reviewofquestion'] = 'مرور سؤال {$a->question} در {$a->quiz} توسط {$a->user}';
$string['reviewopen'] = 'پس از شرکت، وقتی که آزمون هنوز باز است';
$string['reviewoptions'] = 'مواردی که شاگردها می‌توانند ببینند';
$string['reviewoptionsheading'] = 'گزینه‌های مرور';
$string['reviewoptionsheading_help'] = 'این گزینه‌ها تعیین می‌کنند که چه اطلاعاتی توسط شاگردان، وقتی که در حال مرور شرکت در آزمون یا دیدن گزارش‌های آزمون هستند، قابل دیدن باشد.

تنظیمات قسمت **در هنگام آزمون** تنها برای برخی از رفتارها (مانند تعاملی با چند بار تلاش) که ممکن است بازخوردی را در حین شرکت در آزمون نمایش دهند مربوط هستند.

تنظیمات **بلافاصله بعد از شرکت در آزمون** بعد از کلیک روی دکمهٔ «ثبت همهٔ پاسخ‌ها و اتمام آزمون» و به‌مدت ۲ دقیقه برقرار هستند.

تنظیمات **پس از شرکت، وقتی که آزمون هنوز باز است** پس از حالت قبل و تا قبل از تاریخ بسته شدن آزمون برقرار هستند.

تنظیمات **پس از بسته شدن آزمون** بعد از سپری شدن تاریخ بسته شدن آزمون اعمال خواهند شد. اگر آزمون دارای تاریخ بسته شدن نباشد، این حالت هرگز پیش نخواهد آمد.';
$string['reviewoverallfeedback'] = 'بازخورد کلی';
$string['reviewoverallfeedback_help'] = 'بازخوردی که در پایان شرکت در آزمون و براساس نمرهٔ کلی شاگرد به او نمایش داده می‌شود.';
$string['reviewresponse'] = 'مرور پاسخ ارائه شده';
$string['reviewresponsetoq'] = 'مرور پاسخ‌های وارد شده (سؤال {$a})';
$string['reviewthisattempt'] = 'مرور پاسخ‌هایتان در این دفعه';
$string['rqp'] = 'سؤال خارج از سایت';
$string['rqps'] = 'سؤال‌های خارج از سایت';
$string['sameasoverall'] = 'مشابه تنظیم صورت گرفته برای نمرهٔ کل';
$string['save'] = 'ذخیره';
$string['saveandedit'] = 'ذخیرهٔ تغییرات و ویرایش سؤال‌ها';
$string['saveattemptfailed'] = 'ذخیرهٔ این تلاش در آزمون با شکست مواجه شد.';
$string['savedfromdeletedcourse'] = 'از درس حذف شدهٔ «{$a}» ذخیره شد';
$string['savegrades'] = 'ذخیرهٔ نمره‌ها';
$string['savemyanswers'] = 'ذخیرهٔ پاسخ‌های من';
$string['savenosubmit'] = 'ذخیره بدون ارائه دادن';
$string['saveoverrideandstay'] = 'ذخیره و تعریف یک بازنویسی دیگر';
$string['savequiz'] = 'ذخیرهٔ کل این آزمون';
$string['score'] = 'امتیاز خام';
$string['scores'] = 'نمره‌ها';
$string['search:activity'] = 'آزمون - اطلاعات فعالیت';
$string['seequestions'] = '(دیدن سؤال‌ها)';
$string['select'] = 'انتخاب';
$string['selectall'] = 'انتخاب همه';
$string['selectcategory'] = 'انتخاب طبقه';
$string['selectedattempts'] = 'تلاش‌های انتخاب شده...';
$string['selectmultipleitems'] = 'انتخاب کردن چند مورد با هم';
$string['selectnone'] = 'انتخاب هیچ‌کدام';
$string['selectquestiontype'] = '-- نوع سؤال را انتخاب کنید --';
$string['serveradded'] = 'کارگزار اضافه شد';
$string['serveridentifier'] = 'شناسه';
$string['serverinfo'] = 'اطلاعات کارگزار';
$string['servers'] = 'کارگزارها';
$string['serverurl'] = 'آدرس کارگزار';
$string['settingsoverrides'] = 'تنظیمات بازنویسی شده';
$string['shortanswer'] = 'کوتاه‌جواب';
$string['show'] = 'نمایش';
$string['showall'] = 'نمایش همهٔ سؤال‌ها در یک صفحه';
$string['showblocks'] = 'نمایش بلوک‌ها در طول شرکت در آزمون';
$string['showblocks_help'] = 'اگر روی بله تنظیم شده باشد، آنگاه بلوک‌های عادی در طول شرکت در آزمون نمایش داده خواهند شد';
$string['showbreaks'] = 'نمایش محل تفکیک صفحه‌ها';
$string['showcategorycontents'] = 'نمایش محتویات طبقه {$a->arrow}';
$string['showcorrectanswer'] = 'پاسخ صحیح در بازخورد نمایش داده شود؟';
$string['showdetailedmarks'] = 'نمایش جزئیات نمره';
$string['showeachpage'] = 'نمایش صفحه‌ها به صورت جداگانه';
$string['showfeedback'] = 'پس از پاسخ دادن، بازخورد نمایش داده شود؟';
$string['showinsecurepopup'] = 'استفاده از یک پنجرهٔ باز شوندهٔ «امن» برای شرکت در آزمون';
$string['showlargeimage'] = 'عکس بزرگ';
$string['shownoattempts'] = 'نمایش شاگردانی که اصلا در آزمون شرکت نکرده‌اند';
$string['shownoattemptsonly'] = 'فقط نمایش شاگردانی که اصلا در آزمون شرکت نکرده‌اند';
$string['shownoimage'] = 'بدون عکس';
$string['showreport'] = 'نمایش گزارش';
$string['showsmallimage'] = 'عکس کوچک';
$string['showteacherattempts'] = 'نمایش تلاش‌های استاد';
$string['showuserpicture'] = 'نمایش عکس کاربر';
$string['showuserpicture_help'] = 'در صورت فعال بودن، نام و عکس کاربر در هنگام شرکت در آزمون و همچنین هنگام مرور آزمون، روی صفحه نشان داده خواهند شد. این کار تشخیص اینکه آیا شاگردان به جای خودشان در حال شرکت در آزمون هستند را در آزمون‌هایی که تحت مراقبت و نظارت برگزار می‌شوند را ساده‌تر می‌کند.';
$string['shuffle'] = 'بهم ریختن ترتیب';
$string['shuffleanswers'] = 'بهم ریختن ترتیب گزینه‌ها';
$string['shuffledrandomly'] = 'به صورت تصادفی و درهم';
$string['shufflequestions'] = 'بهم ریختن ترتیب سؤال‌ها';
$string['shufflewithin'] = 'بهم ریختن ترتیب گزینه‌ها';
$string['shufflewithin_help'] = 'در صورت فعال بودن، اجزای تشکیل دهندهٔ سؤال‌ها می‌توانند در هر باری که به شاگردی نمایش داده می‌شوند، به صورت تصادفی جابه‌جا شوند؛ به شرطی که در تنظیمات آن سؤال‌ها هم این تنظیم فعال باشد. این تنظیم فقط روی سؤال‌هایی اعمال می‌شود که از چند جزء تشکیل شده باشند (مثل سؤال‌های چند گزینه‌ای یا جور کردنی).';
$string['singleanswer'] = 'یک گزینه را انتخاب کنید.';
$string['sortage'] = 'مرتب‌سازی بر اساس زمان تعریف';
$string['sortalpha'] = 'مرتب‌سازی بر اساس نام';
$string['sortquestionsbyx'] = 'مرتب‌سازی سؤال‌ها بر اساس: {$a}';
$string['sortsubmit'] = 'مرتب‌سازی سؤالات';
$string['sorttypealpha'] = 'مرتب‌سازی بر اساس نوع، نام';
$string['specificapathnotonquestion'] = 'مسیر فایل تعیین شده در سؤال مشخص شده نیست';
$string['specificquestionnotonquiz'] = 'سؤال مورد نظر در آزمون مورد نظر نیست';
$string['startagain'] = 'شروع مجدد';
$string['startattempt'] = 'شرکت در آزمون';
$string['startedon'] = 'شروع';
$string['startnewpreview'] = 'شروع یک پیش‌نمایش جدید';
$string['stateabandoned'] = 'هرگز تحویل داده نشده';
$string['statefinished'] = 'پایان‌یافته';
$string['statefinisheddetails'] = 'ثبت شده در: {$a}';
$string['stateinprogress'] = 'در جریان';
$string['statenotloaded'] = 'وضعیت سؤال {$a} از پایگاه داده بارگذاری نشده است.';
$string['stateoverdue'] = 'از موعد گذشته';
$string['status'] = 'وضعیت';
$string['stoponerror'] = 'توقف در صورت بروز خطا';
$string['submitallandfinish'] = 'ثبت همهٔ پاسخ‌ها و اتمام آزمون';
$string['subneterror'] = 'با عرض پوزش، این آزمون طوری قفل شده است تا فقط از محل‌های معینی قابل دسترسی باشد. هم‌اکنون رایانهٔ شما جزو آن‌هایی که مجاز به استفاده از آزمون هستند نیست.';
$string['subnetnotice'] = 'این آزمون طوری قفل شده است تا فقط از محل‌های معینی قابل دسترسی باشد. رایانهٔ شما در یک زیرشبکهٔ مجاز قرار ندارد. در هر صورت، به عنوان استاد، شما امکان پیش‌نمایش آزمون را دارید.';
$string['subplugintype_quizaccess_plural'] = 'قوانین دسترسی';
$string['subplugintype_quiz_plural'] = 'گزارش‌ها';
$string['substitutedby'] = 'جایگزین خواهد شد با';
$string['summaryofattempt'] = 'وضعیت شرکت در آزمون';
$string['summaryofattempts'] = 'نتایج خلاصهٔ دفعه‌های قبلی شرکت شما در آزمون';
$string['temporaryblocked'] = 'شما موقتاً اجازهٔ شرکت مجدد در آزمون را ندارید. می‌توانید مجدداً در آزمون شرکت کنید در:';
$string['theattempt'] = 'پاسخ ارائه شده';
$string['theattempt_help'] = 'اینکه آیا شاگرد می‌تواند تلاش خود را مرور کند یا خیر.';
$string['time'] = 'زمان';
$string['timecompleted'] = 'اتمام';
$string['timedelay'] = 'اجازهٔ شرکت در آزمون را ندارید، زیرا هنوز به اندازهٔ تاخیر تعیین شده از آخرین دفعهٔ شرکت در آزمون نگذشته است.';
$string['timeleft'] = 'زمان باقیمانده';
$string['timelimit'] = 'محدودیت زمانی';
$string['timelimitexeeded'] = 'متاسفیم! محدودیت زمانی آزمون به سر رسید!';
$string['timelimit_help'] = 'در صورت فعال بودن، یک پنجرهٔ شمارندهٔ شناور (جاوا اسکریپت لازم است) در حال شمارش معکوس نمایش داده می‌شود. وقتی که مهلت تعیین شده تمام شود، آزمون به صورت خودکار با جواب‌هایی که تا آن لحظه وارد شده‌اند تحویل داده می‌شود.';
$string['timestr'] = '%H:%M:%S در %d/%m/%y';
$string['timesup'] = 'وقت تمام شد!';
$string['timetaken'] = 'زمان صرف شده';
$string['timing'] = 'زمان‌بندی';
$string['tofile'] = 'به فایل';
$string['tolerance'] = 'خطای قابل قبول';
$string['toomanyrandom'] = 'تعداد سؤال‌های تصادفی مورد نیاز بیشتر از تعداد سؤال‌هایی است که هنوز در طبقه موجود هستند!';
$string['top'] = 'ریشه';
$string['totalmarksx'] = 'جمع نمرات: {$a}';
$string['totalquestionsinrandomqcategory'] = 'از بین کلاً {$a} سؤال موجود در طبقه.';
$string['true'] = 'درست';
$string['truefalse'] = 'صحیح/غلط';
$string['type'] = 'نوع';
$string['unfinished'] = 'باز';
$string['ungraded'] = 'بدون نمره';
$string['unit'] = 'واحد';
$string['unknowntype'] = 'سؤال تعریف شده در خط {$a} پشتیبانی نمی‌شود. این سؤال نادیده گرفته خواهد شد';
$string['updatesettings'] = 'به‌روزرسانی تنظیمات آزمون';
$string['upgradesure'] = '<div>به طور خاص در ماژول آزمون، جدول‌های این ماژول به طور گسترده‌ای تغییر خواهد کرد و انجام این ارتقا هنوز به اندازهٔ کافی آزموده نشده است. شدیداً اصرار می‌شود که پیش از ادامه دادن، از جدول‌های پایگاه دادهٔ خود پشتیبان تهیه نمائید.</div>';
$string['url'] = 'آدرس';
$string['usedcategorymoved'] = 'این طبقه نگه داشته شده است و به سطح سایت منتقل شده است زیرا یک طبقه منتشر شده است که هنوز توسط سایر درس‌ها استفاده می‌شود.';
$string['useroverrides'] = 'بازنویسی‌های مربوط به کاربران';
$string['usersnone'] = 'هیچ شاگردی به آزمون دسترسی ندارد';
$string['validate'] = 'اعتبار سنجی';
$string['viewallanswers'] = 'مشاهدهٔ {$a} تلاش صورت گرفته در آزمون';
$string['viewallreports'] = 'مشاهدهٔ گزارش {$a} تلاش صورت گرفته در آزمون';
$string['viewed'] = 'مشاهده شده';
$string['warningmissingtype'] = '<b>این سؤال از نوعی است که هنوز روی مودل شما نصب نشده است.<br />لطفا مدیر مودل خود را مطلع سازید.</b>';
$string['wheregrade'] = 'نمرهٔ من کجاست؟';
$string['wildcard'] = 'متغیر';
$string['windowclosing'] = 'این پنجره به زودی بسته خواهد شد.';
$string['withsummary'] = 'با آمارهای خلاصه';
$string['wronguse'] = 'نمی‌توانید اینگونه از این صفحه استفاده کنید';
$string['xhtml'] = 'قالب XHTML';
$string['youneedtoenrol'] = 'پیش از اینکه بتوانید در آزمون شرکت کنید باید در این درس ثبت نام شوید';
$string['yourfinalgradeis'] = 'نمرهٔ نهایی شما در این آزمون {$a} است.';
