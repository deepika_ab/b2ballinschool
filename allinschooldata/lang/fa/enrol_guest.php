<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_guest', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_guest
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allowguests'] = 'ورود کاربران مهمان به این درس مجاز است';
$string['guestaccess_withoutpassword'] = 'دسترسی مهمان';
$string['guestaccess_withpassword'] = 'دسترسی مهمان';
$string['guest:config'] = 'پیکربندی دسترسی مهمان در درس‌ها';
$string['password'] = 'رمز دسترسی';
$string['password_help'] = 'با تعیین رمز دسترسی می‌توان دسترسی به درس به عنوان مهمان را محدود به کسانی کرد که این رمز دسترسی را می‌دانند. هنگام ورود به درس از مهمان‌ها خواسته می‌شود که رمز دسترسی را وارد کنند.';
$string['passwordinvalid'] = 'رمز دسترسی اشتباه است، لطفاً دوباره سعی کنید';
$string['passwordinvalidhint'] = 'رمز دسترسی اشتباه بود، لطفاً دوباره سعی کنید<br />
(راهنمایی: با «{$a}» شروع می‌شود)';
$string['pluginname'] = 'دسترسی مهمان';
$string['pluginname_desc'] = 'پلاگین دسترسی مهمان فقط امکان دسترسی موقتی به درس‌ها را فراهم می‌کند. این پلاگین کاربران را واقعاً ثبت نام نمی‌کند.';
$string['requirepassword'] = 'الزام رمز دسترسی مهمان';
$string['requirepassword_desc'] = 'تعیین رمز دسترسی مهمان برای درس‌های جدید الزامی خواهد بود و اجازهٔ حذف رمز دسترسی در درس‌های موجود داده نمی‌شود.';
$string['showhint'] = 'نشان دادن راهنمایی';
$string['showhint_desc'] = 'نشان دادن حرف اول رمز دسترسی مهمان.';
$string['status'] = 'اجازهٔ ورود به عنوان مهمان';
$string['status_desc'] = 'به طور پیش‌فرض اجازهٔ دسترسی موقتی به عنوان مهمان داده شود.';
$string['status_help'] = 'این گزینه تعیین می‌کند که آیا کاربران اجازهٔ دسترسی به درس به عنوان مهمان، و بدون نیاز به ثبت نام، را داشته باشند یا خیر.';
$string['usepasswordpolicy'] = 'اعمال قوانین مربوط به رمز ورود';
$string['usepasswordpolicy_desc'] = 'رمز دستیابی باید منطبق با شرایط تعیین شده استاندارد برای رمز ورود باشد.';
