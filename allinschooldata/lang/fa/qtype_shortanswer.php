<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_shortanswer', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_shortanswer
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addmoreanswerblanks'] = 'جای خالی برای {no} جواب دیگر';
$string['answer'] = 'پاسخ: {$a}';
$string['answermustbegiven'] = 'اگر نمره یا بازخوردی وجود داشته باشد، باید پاسخی را هم وارد کنید.';
$string['answerno'] = 'پاسخ {$a}';
$string['caseno'] = 'خیر، حالت حروف مهم نیست';
$string['casesensitive'] = 'حساس بودن به بزرگ و کوچک بودن حروف';
$string['caseyes'] = 'بله، حالت حروف باید یکسان باشد';
$string['correctansweris'] = 'پاسخ درست: {$a}';
$string['correctanswers'] = 'پاسخ درست';
$string['filloutoneanswer'] = 'باید حداقل یک جواب ممکن را فراهم کنید. جواب‌های خالی استفاده نخواهند شد. از «*» می‌توان به عنوان یک wildcard به عنوان جایگزینی برای تعدادی از حروف استفاده کرد. از اولین پاسخ منطبق برای تعیین نمره و بازخورد استفاده می‌شود.';
$string['notenoughanswers'] = 'این نوع سؤال به حداقل {$a} پاسخ نیاز دارد';
$string['pluginname'] = 'کوتاه جواب';
$string['pluginnameadding'] = 'اضافه کردن یک سؤال کوتاه جواب';
$string['pluginnameediting'] = 'در حال ویرایش یک سؤال کوتاه جواب';
$string['pluginname_help'] = 'در پاسخ به یک سؤال (که می‌تواند شامل یک عکس باشد) پاسخ دهنده یک کلمه یا عبارت کوتاه را وارد می‌کند. پاسخ‌های صحیح متعددی ممکن است وجود داشته باشند و هر کدام می‌توانند نمرهٔ متفاوتی داشته باشند. اگر گزینهٔ «حساس بودن به بزرگی و کوچکی حروف» انتخاب شده باشد، آنگاه می‌توانید نمره‌های متفاوتی برای مثلا «Word» و «word» داشته باشید.';
$string['pluginnamesummary'] = 'شاگرد می‌تواند یک کلمه یا عبارت کوتاه را به عنوان پاسخ سؤال وارد کند. پاسخ شاگرد از طریق مقایسه با مجموعهٔ پاسخ‌های صحیح (که ممکن است شامل wildcard ها هم باشند) به صورت خودکار نمره داده می‌شود.';
