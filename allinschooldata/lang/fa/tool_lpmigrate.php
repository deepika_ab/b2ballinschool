<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_lpmigrate', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_lpmigrate
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allowedcourses_help'] = 'درس‌هایی که می‌خواهید به چارچوب جدید مهاجرت کنند را انخاب کنید. اگر هیچ درسی را انتخاب نکنید آنگاه تمام درس‌ها مهاجرت خواهند یافت.';
$string['coursestartdate_help'] = 'اگر فعال باشد، درس‌هایی که تاریخ شروعشان پیش از تاریخ وارد شده است مهاجرت نخواهند یافت.';
$string['disallowedcourses_help'] = 'درس‌هایی که *نباید* به چارچوب جدید مهاجرت کنند را انتخاب کنید.';
$string['errorcannotmigratetosameframework'] = 'نمی‌توان از یک چارچوب به همان چارچوب مهاجرت کرد.';
$string['excludethese'] = 'مستثنی شدن اینها';
$string['explanation'] = 'از این ابزار می‌توان برای به‌روز کردن یک چارچوب شایستگی به نسخهٔ جدیدتر استفاده کرد. این ابزار به جستجوی شایستگی‌ها در درس‌ها و فعالیت‌هایی که از چارچوب قدیمی‌تر استفاده می‌کنند می‌پردازد و پیوندها را به‌روز می‌کند تا به چارچوب جدید اشاره کنند.

توصیه نمی‌شود که مجموعه شایستگی‌های قدیمی را به‌طور مستقیم ویرایش کنید زیرا این کار باعث تغییر تمام شایستگی‌هایی که تا به‌حال در برنامه یادگیری کاربران اعطاء شده‌اند می‌شود.

معمولا شما نسخهٔ جدید یک چارچوب را وارد (import) می‌کنید، چارچوب قدیمی را پنهان می‌کنید، سپس از این ابزار برای مهاجرت درس‌های جدید به چارچوب جدید استفاده می‌کنید.';
$string['frameworks'] = 'چارچوب‌ها';
$string['limittothese'] = 'محدود به این‌ها';
$string['lpmigrate:frameworksmigrate'] = 'مهاجرت چارچوب‌ها';
$string['migrateframeworks'] = 'مهاجرت چارچوب‌ها';
$string['migratefrom'] = 'مهاجرت از';
$string['migratefrom_help'] = 'چارچوب قدیمی‌تری که هم‌اکنون در حال استفاده است را انتخاب کنید.';
$string['migrateto'] = 'مهاجرت به';
$string['migrateto_help'] = 'نسخهٔ جدیدتر چارچوب را انتخاب کنید. تنها چارچوبی را می‌توان انتخاب کرد که پنهان نباشد.';
$string['performmigration'] = 'انجام مهاجرت';
$string['pluginname'] = 'ابزار مهاجرت چارچوب‌ها';
$string['results'] = 'نتایج';
$string['startdatefrom'] = 'تاریخ شروع از';
