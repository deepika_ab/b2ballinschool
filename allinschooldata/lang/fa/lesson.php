<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'lesson', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   lesson
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accesscontrol'] = 'کنترل دستیابی';
$string['actionaftercorrectanswer'] = 'رفتار پس از پاسخ صحیح';
$string['actionaftercorrectanswer_help'] = 'پس از اینکه پاسخ یک سوال به طور صحیح داده شد، ۳ حالت برای صفحهٔ بعد وجود دارد:

* طبیعی - مسیر مبحث دنبال شود
* نمایش یک صفحهٔ دیده نشده - صفحه‌ها با ترتیب تصادفی به صورت غیر تکراری نمایش داده می‌شوند
* نمایش یک صفحهٔ پاسخ داده نشده - صفحه‌ها به ترتیب تصادفی نمایش داده می‌شوند. صفحه‌هایی که شامل سوال‌های پاسخ داده نشده باشند مجددا نمایش داده خواهند شد';
$string['actions'] = 'اقدامات';
$string['activitylink'] = 'پیوند به فعالیت بعدی';
$string['activitylink_help'] = 'می‌توان در انتهای مبحث، پیوندی به یکی دیگر از فعالیت‌های درس را قرار داد. فعالیتی که می‌خواهید به آن پیوند دهید را از میان لیست انتخاب کنید.';
$string['activitylinkname'] = 'مراجعه به {$a}';
$string['addabranchtable'] = 'اضافه کردن یک صفحه مندرجات';
$string['addanendofbranch'] = 'اضافه کردن یک انتهای شاخه';
$string['addanewpage'] = 'اضافه کردن یک صفحهٔ جدید';
$string['addaquestionpage'] = 'اضافه کردن یک صفحهٔ سؤال';
$string['addaquestionpagehere'] = 'اضافه کردن یک صفحهٔ سؤال در این محل';
$string['addbranchtable'] = 'اضافه کردن یک صفحهٔ مندرجات';
$string['addcluster'] = 'اضافه کردن یک خوشه';
$string['addedabranchtable'] = 'یک صفحهٔ مندرجات اضافه شد';
$string['addedanendofbranch'] = 'یک انتهای شاخه اضافه شد';
$string['addedaquestionpage'] = 'یک صفحه سؤال اضافه شد';
$string['addedcluster'] = 'یک خوشه اضافه شد';
$string['addedendofcluster'] = 'یک انتهای خوشه اضافه شد';
$string['addendofcluster'] = 'اضافه کردن یک انتهای خوشه';
$string['addpage'] = 'اضافه کردن یک صفحهٔ';
$string['anchortitle'] = 'شروع محتوای اصلی';
$string['and'] = 'و همچنین';
$string['answer'] = 'جواب';
$string['answeredcorrectly'] = 'درست پاسخ داده‌اند.';
$string['answersfornumerical'] = 'پاسخ‌های سؤالات عددی باید با مقادیر حداقل و حداکثر مطابقت داشته باشد';
$string['arrangebuttonshorizontally'] = 'قرارگیری دکمه‌های مندرجات به صورت افقی؟';
$string['attempt'] = 'تلاش: {$a}';
$string['attempts'] = 'تلاش‌ها';
$string['attemptsdeleted'] = 'تلاش‌ها حذف شدند';
$string['attemptsremaining'] = 'می‌توانید {$a} بار دیگر تلاش کنید';
$string['available'] = 'قابل دسترسی از';
$string['averagescore'] = 'نمرهٔ میانگین';
$string['averagetime'] = 'زمان میانگین';
$string['branch'] = 'مندرجات';
$string['branchtable'] = 'مندرجات';
$string['cancel'] = 'انصراف';
$string['cannotfindanswer'] = 'خطا: پیدا کردن پاسخ مقدور نبود';
$string['cannotfindattempt'] = 'خطا: پیدا کردن تلاش مقدور نبود';
$string['cannotfindessay'] = 'خطا: پیدا کردن سؤال تشریحی مقدور نبود';
$string['cannotfindfirstgrade'] = 'خطا: پیدا کردن نمرات مقدورنبود';
$string['cannotfindfirstpage'] = 'پیدا کردن صفحهٔ اول مقدور نبود';
$string['cannotfindgrade'] = 'خطا: پیدا کردن نمرات مقدورنبود';
$string['cannotfindnewestgrade'] = 'خطا: پیدا کردن جدیدترین نمره مقدورنبود';
$string['cannotfindnextpage'] = 'انتقال صفحه: صفحهٔ بعدی پیدا نشد!';
$string['cannotfindpagerecord'] = 'اضافه کردن انتهای شاخه: رکورد صفحه پیدا نشد';
$string['cannotfindpages'] = 'پدا کردن صفحه‌های مبحث مقدور نبود';
$string['cannotfindpagetitle'] = 'تایید حذف: عنوان صفحه پیدا نشد';
$string['cannotfindpreattempt'] = 'پیدا کردن رکورد مربوط به تلاش قبلی مقدور نبود!';
$string['cannotfindrecords'] = 'خطا: پیدا کردن رکوردهای مبحث مقدور نبود';
$string['cannotfindtimer'] = 'خطا: پیدا کردن رکوردهای lesson_timer مقدور نبود';
$string['cannotfinduser'] = 'خطا: پیدا کردن کاربران مقدور نبود';
$string['canretake'] = '{$a} بتواند مجدداً شرکت کند';
$string['casesensitive'] = 'استفاده از عبارت‌های منظم';
$string['casesensitive_help'] = 'در صورتی که این گزینه انتخاب شده باشد، از عبارت‌های منظم برای تحلیل پاسخ‌ها استفاده می‌شود.';
$string['checkbranchtable'] = 'بررسی صفحهٔ مندرجات';
$string['checkedthisone'] = 'این گزینه را انتخاب کرده‌اند.';
$string['checknavigation'] = 'بررسی راهبری';
$string['checkquestion'] = 'بررسی سؤال';
$string['classstats'] = 'آمار کلاس';
$string['clicktodownload'] = 'برای دریافت فایل روی پیوند زیر کلیک کنید.';
$string['cluster'] = 'خوشه';
$string['clusterjump'] = 'سؤال دیده نشده از یک خوشه';
$string['clustertitle'] = 'خوشه';
$string['collapsed'] = 'خلاصه';
$string['comments'] = 'توضیحات شما';
$string['completed'] = 'تکمیل';
$string['completederror'] = 'مبحث درس را به پایان برسانید';
$string['completethefollowingconditions'] = 'قبل از شروع این مبحث درسی، باید شرایط زیر را در مبحث درسی <b>{$a}</b> احراز نمائید.';
$string['completionendreached'] = 'نیازمند رسیدن به انتها';
$string['completionendreached_desc'] = 'شاگردان باید به صفحهٔ انتهای مبحث درسی برسند تا این فعالیت را کامل کنند';
$string['completiontimespent'] = 'حداقل  مدت زمانی که شاگردان باید در این فعالیت صرف کنند';
$string['completiontimespentgroup'] = 'نیازمند صرف زمان';
$string['conditionsfordependency'] = 'شرایط وابستگی';
$string['configmaxanswers'] = 'حداکثر تعداد پاسخ‌ها/شاخه‌ها در هر صفحه به طور پیش‌فرض';
$string['configmediaclose'] = 'در صفحهٔ جدیدی که برای یک فایل چندرسانه‌ای که به آن پیوند داده شده تولید می‌شود، دکمه‌ای برای بستن صفحه وجود داشته باشد';
$string['configmediaheight'] = 'ارتفاع پنجرهٔ بازشونده برای یک فایل چندرسانه‌ای پیوند داده شده را تعیین می‌کند';
$string['configmediawidth'] = 'عرض پنجرهٔ بازشونده برای یک فایل چندرسانه‌ای پیوند داده شده را تعیین می‌کند';
$string['configslideshowbgcolor'] = 'رنگ پس‌زمینهٔ نمایش اسلاید در صورتی که فعال باشد';
$string['configslideshowheight'] = 'ارتفاع نمایش اسلاید را (در صورت فعال بودن) را تعیین می‌کند';
$string['configslideshowwidth'] = 'عرض نمایش اسلاید را (در صورت فعال بودن) را تعیین می‌کند';
$string['confirmdelete'] = 'پاک کردن صفحه';
$string['confirmdeletionofthispage'] = 'تایید حذف این صفحه';
$string['congratulations'] = 'تبریک - به انتهای مبحث درسی رسیدید';
$string['continue'] = 'ادامه';
$string['continuetoanswer'] = 'ادامهٔ تغییر دادن پاسخ‌ها.';
$string['correctanswerjump'] = 'پرش در صورت پاسخ صحیح';
$string['correctanswerscore'] = 'نمرهٔ پاسخ صحیح';
$string['correctresponse'] = 'واکنش به پاسخ صحیح';
$string['credit'] = 'معتبر';
$string['customscoring'] = 'نمره‌دهی سفارشی';
$string['customscoring_help'] = 'اگر فعال باشد، برای هر کدام از پاسخ‌ها می‌توان یک نمره (مثبت یا منفی) معین کرد.';
$string['deadline'] = 'مهلت';
$string['defaultessayresponse'] = 'پاسخ تشریحی شما توسط استادتان تصحیح خواهد شد.';
$string['deleteallattempts'] = 'حذف همهٔ تلاش‌های صورت گرفته در مباحث درسی';
$string['deletedefaults'] = 'حذف {$a} مقدار پیش‌فرض برای مباحث درسی';
$string['deletedpage'] = 'صفحهٔ حذف شده';
$string['deleting'] = 'در حال حذف';
$string['deletingpage'] = 'حذف صفحهٔ {$a}';
$string['dependencyon'] = 'وابستگی';
$string['dependencyon_help'] = 'با استفاده از این تنظیم می‌توان دستیابی شاگردان به این مبحث درسی را منوط به عملکرد آنها در یک مبحث درسی دیگر از این درس کرد. هر ترکیب ممکنی از «زمان صرف شده»، «تکمیل» و «نمرهٔ بهتر از» قابل استفاده است.';
$string['description'] = 'توضیح';
$string['detailedstats'] = 'آمار تفصیلی';
$string['didnotanswerquestion'] = 'به این سؤال پاسخ نداده است.';
$string['didnotreceivecredit'] = 'اعتبار دریافت نکرد';
$string['displaydefaultfeedback'] = 'نمایش بازخورد پیش‌فرض';
$string['displaydefaultfeedback_help'] = 'در صورت فعال بودن، اگر واکنشی برای یک سؤال خاص پیدا نشود، واکنش‌های پیش‌فرض «پاسخ صحیح» یا «این پاسخ اشتباه است» نمایش داده خواهند شد.';
$string['displayinleftmenu'] = 'نمایش در منو';
$string['displayleftif'] = 'حداقل نمره برای نمایش منو';
$string['displayleftif_help'] = 'این تنظیم تعیین می‌کند که آیا یک شاگرد برای اینکه بتواند منوی سمت راست را ببیند باید حتما نمرهٔ خاصی را کسب کند یا خیر. با این کار، شاگردان مجبور به مطالعهٔ کامل مبحث درسی در اولین دفعهٔ ورود به آن خواهند شد. سپس، پس از کسب نمرهٔ لازم، شاگردان می‌توانند از منوی سمت راست برای مرور استفاده کنند.';
$string['displayleftmenu'] = 'نمایش منو';
$string['displayleftmenu_help'] = 'در صورت فعال بودن، لیستی از صفحه‌ها نمایش داده می‌شود.';
$string['displayofgrade'] = 'نمایش نمره (مختص شاگردان)';
$string['displayreview'] = 'فراهم کردن امکان پاسخ دادن دوباره به یک سؤال';
$string['displayreview_help'] = 'در صورت فعال بودن، هنگامی که شاگردی به سؤالی پاسخ نادرست می‌دهد، به او اجازه داده می‌شود که باز هم (بدون دریافت نمره) پاسخ را وارد کند و یا اینکه مبحث درسی را ادامه دهد.';
$string['displayscorewithessays'] = '<p>شما از قسمت سؤالاتی که به صورت خودکار تصحیح می‌شوند نمرهٔ {$a->score} از {$a->tempmaxgrade} را بدست آوردید.</p>
<p>{$a->essayquestions} سؤال تشریحی باقیمانده بعداً تصحیح و نمرهٔ بدست آمده به نمرهٔ نهایی شما اضافه خواهد شد.</p>
<p>نمرهٔ فعلی شما بدون در نظر گرفتن نمرهٔ سؤالات تشریحی {$a->score} از {$a->grade} است.</p>';
$string['displayscorewithoutessays'] = 'نمره شما {$a->score} است (از {$a->grade}).';
$string['edit'] = 'ویرایش';
$string['editingquestionpage'] = 'در حال ویرایش صفحهٔ سؤال {$a}';
$string['editlessonsettings'] = 'ویرایش تنظیمات مبحث درسی';
$string['editoverride'] = 'ویرایش بازنویسی';
$string['editpage'] = 'ویرایش محتوای صفحه';
$string['editpagecontent'] = 'ویرایش محتویات صفحه';
$string['email'] = 'پست الکترونیک';
$string['emailallgradedessays'] = 'ارسال همهٔ پاسخ‌های تصحیح شده با پست الکترونیک';
$string['emailgradedessays'] = 'ارسال پاسخ‌های تصحیح شده با پست الکترونیک';
$string['emailsuccess'] = 'نامه‌های الکترونیکی با موفقیت ارسال شدند';
$string['emptypassword'] = 'کلمهٔ رمز نمی‌تواند خالی باشد';
$string['endofbranch'] = 'انتهای شاخه';
$string['endofcluster'] = 'انتهای خوشه';
$string['endofclustertitle'] = 'انتهای خوشه';
$string['endoflesson'] = 'انتهای مبحث درسی';
$string['enteredthis'] = 'این عبارت را وارد کرده‌اند.';
$string['enterpassword'] = 'لطفاً کلمهٔ رمز را وارد نمائید:';
$string['eolstudentoutoftime'] = 'توجه: وقت شما در این مبحث تمام شده است. آخرین پاسخ شما در صورتی که پس از اتمام وقت ارائه شده باشد ممکن است در نظر گرفته نشود.';
$string['eolstudentoutoftimenoanswers'] = 'به هیچ سؤالی پاسخ ندادید. نمرهٔ شما در این مبحث درسی صفر می‌باشد.';
$string['essay'] = 'تشریحی';
$string['essayemailmessage2'] = '<p>عنوان سؤال:<blockquote>{$a->question}</blockquote></p><p>پاسخ شما:<blockquote><em>{$a->response}</em></blockquote></p><p>نظر نمره‌دهنده:<blockquote><em>{$a->comment}</em></blockquote></p><p>شما از این سؤال نمرهٔ {$a->earned} از {$a->outof} را کسب کرده‌اید.</p><p>نمرهٔ شما در این مبحث درسی به {$a->newgrade}٪ تغییر یافت.</p>';
$string['essayemailsubject'] = 'نمرهٔ شما از سؤال «{$a}»';
$string['essays'] = 'پاسخ‌ها';
$string['essayscore'] = 'نمرهٔ پاسخ';
$string['eventlessonended'] = 'مبحث درسی به‌پایان رسید';
$string['eventlessonrestarted'] = 'مبحث درسی دوباره از اول شروع شد';
$string['eventlessonresumed'] = 'مبحث درسی ادامه داده شد';
$string['eventlessonstarted'] = 'مبحث درسی شروع شد';
$string['eventpagecreated'] = 'صفحه ایجاد شد';
$string['eventpagedeleted'] = 'صفحه حذف شد';
$string['eventpagemoved'] = 'صفحه منتقل شد';
$string['eventpageupdated'] = 'صفحه به‌روز شد';
$string['eventquestionanswered'] = 'سؤال پاسخ داده شد';
$string['eventquestionviewed'] = 'سؤال مشاهده شد';
$string['fileformat'] = 'قالب فایل';
$string['firstanswershould'] = 'اولین پاسخ باید به صفحهٔ «صحیح» برود';
$string['firstwrong'] = 'متأسفانه پاسخ شما اشتباه بود و نمرهٔ این سؤال را بدست نیاوردید. آیا مایلید که تنها برای لذت یادگیری (و بدون تغییر در نمره) به حدس زدن ادامه دهید؟';
$string['flowcontrol'] = 'کنترل روند';
$string['full'] = 'تفصیلی';
$string['general'] = 'عمومی';
$string['grade'] = 'نمره';
$string['gradebetterthan'] = 'نمرهٔ بهتر از (٪)';
$string['gradebetterthanerror'] = 'نمره‌ای بهتر از {$a} درصد بدست بیاورید';
$string['graded'] = 'نمره داده شده است';
$string['gradeessay'] = 'تصحیح سؤالات تشریحی ({$a->notgradedcount} پاسخ تصحیح نشده است و {$a->notsentcount} پاسخ ارسال نشده است)';
$string['gradeis'] = 'نمره: {$a}';
$string['gradeoptions'] = 'اختیارات نمره‌ای';
$string['groupsnone'] = 'هیچ گروهی در این درس وجود ندارد';
$string['handlingofretakes'] = 'نحوهٔ رفتار با شرکت‌های مجدد';
$string['handlingofretakes_help'] = 'اگر شرکت مجدد مجاز باشد، مقدار این تنظیم تعیین می‌کند که نمرهٔ نهایی مبحث درسی میانگین نمره‌های کسب شده در هر بار تلاش باشد یا اینکه بیشتری نمرهٔ کسب شده.';
$string['havenotgradedyet'] = 'هنوز تصحیح نشده است.';
$string['here'] = 'اینجا';
$string['highscore'] = 'بیشترین نمره';
$string['hightime'] = 'بیشترین زمان';
$string['importcount'] = 'وارد کردن {$a} سؤال';
$string['importquestions'] = 'ورود سؤالات';
$string['importquestions_help'] = 'این قابلیت، امکان وارد کردن سؤال‌ها با قالب‌های مختلف را از طریق فایل متنی فراهم می‌کند.';
$string['insertedpage'] = 'صفحهٔ درج شده';
$string['invalidfile'] = 'فایل نامعتبر است';
$string['invalidid'] = 'نه شناسهٔ ماژول درسی و نه شناسهٔ مبحث درسی ارائه نشده‌اند';
$string['invalidlessonid'] = 'شناسهٔ مبحث درسی نادرست بود';
$string['invalidpageid'] = 'شناسهٔ صفحه نادرست است';
$string['jump'] = 'پرش';
$string['jumps'] = 'پرش‌ها';
$string['jumps_help'] = 'هر یک از پاسخ‌ها (در مورد سؤال‌ها) یا توضیح‌ها (در مورد صفحه‌های مندرجات) به یک پرش متناظر می‌شوند. این پرش‌ها می‌توانند به صورت نسبی (مانند «همین صفحه» یا «صفحهٔ بعد») و یا مطلق (با مشخص کردن یکی از صفحه‌های مبحث به طور خاص) باشند.';
$string['jumpsto'] = 'به <em>{$a}</em> می‌پرد';
$string['leftduringtimed'] = 'شما مطالعهٔ یک مبحث درسی زمان‌دار را به صورت نیمه‌کاره رها کردید.<br />برای شروع مجدد لطفاً بر روی ادامه کلیک کنید.';
$string['leftduringtimednoretake'] = 'شما مطالعهٔ یک مبحث درسی زمان‌دار را به صورت نیمه‌کاره رها کردید و<br />اجازهٔ شروع مجدد یا ادامهٔ آن را ندارید.';
$string['lessonclosed'] = 'این مبحث درسی در {$a} بسته شد.';
$string['lessoncloses'] = 'مبحث درسی بسته می‌شود';
$string['lessoncloseson'] = 'این مبحث درسی در {$a} بسته می‌شود';
$string['lesson:edit'] = 'ویرایش مبحث درسی';
$string['lessonformating'] = 'قالب مبحث درسی';
$string['lesson:manage'] = 'مدیریت مبحث درسی';
$string['lessonmenu'] = 'منوی مبحث درسی';
$string['lessonnotready'] = 'این مبحث درسی برای ارائه شدن آماده نیست. لطفاً با {$a} خود تماس بگیرید.';
$string['lessonnotready2'] = 'این مبحث درسی برای ارائه شدن آماده نیست.';
$string['lessonopen'] = 'این مبحث درسی از {$a} باز خواهد شد.';
$string['lessonopens'] = 'مبحث درسی باز می‌شود';
$string['lessonpagelinkingbroken'] = 'صفحهٔ اول پیدا نشد. پیوندهای بین صفحه‌های مبحث درسی باید شکسته شده باشد. لطفاً با یک مدیر تماس بگیرید.';
$string['lessonstats'] = 'آمار مبحث درسی';
$string['linkedmedia'] = 'پیوند به فایل';
$string['loginfail'] = 'رمز وارد شده اشتباه بود. لطفاً مجدداً سعی کنید...';
$string['lowscore'] = 'کمترین نمره';
$string['lowtime'] = 'کمترین زمان';
$string['manualgrading'] = 'تصحیح سؤالات تشریحی';
$string['matchesanswer'] = 'گزینهٔ مطابق با جواب';
$string['matching'] = 'جور کردنی';
$string['matchingpair'] = 'جفت مرتبط {$a}';
$string['maxgrade'] = 'حداکثر نمره';
$string['maxgrade_help'] = 'این تنظیم، بیشترین نمرهٔ قابل کسب در مبحث درسی را تعیین می‌کند. چنانچه بر روی صفر تنظیم شود، مبحث درسی در صفحهٔ نمره‌ها نمایش داده نخواهد شد.';
$string['maximumnumberofanswersbranches'] = 'حداکثر تعداد پاسخ‌ها';
$string['maximumnumberofanswersbranches_help'] = 'این تنظیم، حداکثر تعداد پاسخ (گزینه) های قابل استفاده در مبحث درسی را تعیین می‌کند. اگر فقط از سؤال‌های صحیح/غلط استفاده می‌شود، می‌توان این گزینه را روی ۲ تنظیم کرد. با توجه به اینکه این تنظیم فقط روی چیزی که اساتید می‌بینند (و نه بر روی اطلاعات) تاثیر می‌گذارد، بنابر این در هر لحظه‌ای قابل تغییر دادن می‌باشد.';
$string['maximumnumberofattempts'] = 'حداکثر تعداد تلاش‌ها';
$string['maximumnumberofattempts_help'] = 'این تنظیم، تعیین کنندهٔ حداکثر دفعات مجاز برای پاسخ دادن به هر سؤال می‌باشد. در صورتی که به صورت پی در پی به سؤالی پاسخ اشتباه داده شود، هنگامی که تعداد دفعات به این مقدار برسد، صفحهٔ بعدی مبحث نمایش داده خواهد شد.';
$string['maximumnumberofattemptsreached'] = 'حداکثر دفعات تلاش مجاز را انجام داده‌اید - حرکت به صفحهٔ بعد';
$string['mediaclose'] = 'نمایش دکمهٔ «بستن»';
$string['mediafile'] = 'پنجرهٔ بازشونده به فایل یا صفحهٔ وب';
$string['mediafile_help'] = 'اگر مایلید که در ابتدای مبحث درسی، یک پنجره پاپ‌آپ باز شود، فایلی که قرار است در آن پنجره نمایش داده شود را انتخاب کنید. تمام صفحه‌های مبحث، شامل پیوندی خواهند بود تا در صورت نیاز با استفاده از آن بتوان پنجرهٔ پاپ‌آپ را دوباره باز کرد.';
$string['mediafilepopup'] = 'برای مشاهده کلیک کنید';
$string['mediaheight'] = 'ارتفاع پنجرهٔ پاپ‌آپ:';
$string['mediawidth'] = 'عرض پنجرهٔ پاپ‌آپ:';
$string['messageprovider:graded_essay'] = 'خبر نمره داده شدن سؤال تشریحی';
$string['minimumnumberofquestions'] = 'حداقل تعداد سؤالات';
$string['minimumnumberofquestions_help'] = 'این تنظیم، حداقل تعداد سؤال‌هایی که برای محاسبهٔ یک نمره در این فعالیت مورد استفاده قرار خواهند گرفت را تعیین می‌کند. اگر مبحث درسی شامل یک یا چند صفحهٔ مندرجات باشد، این گزینه باید روی صفر تنظیم شود.

اگر مثلا روی ۲۰ تنظیم شده باشد، پیشنهاد می‌شود که متنی که در ادامه می‌آید را به صفحهٔ اول مبحث اضافه کنید: «در این مبحث باید حداقل به ۲۰ سؤال پاسخ دهید. در صورت تمایل می‌توانید به سؤال‌های بیشتری نیز پاسخ دهید. در هر صورت، اگر به کمتر از ۲۰ سؤال پاسخ داده باشید، نمرهٔ شما به نحوی محاسبه خواهد شد که گویا به ۲۰ سؤال پاسخ داده‌اید.»';
$string['missingname'] = 'لطفاً یک نام مستعار وارد کنید';
$string['modattempts'] = 'اجازهٔ مرور داشتن شاگرد';
$string['modattempts_help'] = 'در صورتی که فعال باشد، شاگردان می‌توانند مجددا با شروع از صفحهٔ اول درون مبحث حرکت کنند.';
$string['modattemptsnoteacher'] = 'قابلیت مرور فقط برای شاگردان کار می‌کند.';
$string['modulename'] = 'مبحث درسی';
$string['modulename_help'] = 'ماژول مبحث درسی به اساتید امکان طراحی یک تجربهٔ یادگیری انطباقی با استفاده از یک سری صفحه‌های سؤال را می‌دهد.

ماژول فعالیت مبحث درسی امکان ارائه محتوا و/یا تمرین فعالیت‌ها به شیوه‌هایی قابل انعطاف و جذاب را برای اساتید به ارمغان می‌آورد. اساتید می‌توانند از مبحث درسی استفاده کنند تا یک مجموعه خطی از صفحه‌های شامل محتوا بسازند و یا اینکه فعالیت‌های آموزشی‌ای بسازند که مسیرها و انتخاب‌های متعددی در اختیار فراگیران قرار می‌دهد. در هر دو حالت، اساتید می‌توانند با استفاده از انواع سوال‌ها (چند گزینه‌ای، جور کردنی، کوتاه جواب، ...) مشارکت شاگردان را افزایش دهند و از درک مفاهیم اطمینان حاصل کنند. بسته به انتخاب شاگرد و اینکه استاد چگونه مبحث درسی را تهیه کرده باشد، شاگردان ممکن است به صفحه بعد پیش روند، به یکی از صفحه‌های قبلی برده شوند یا اینکه کاملا به مسیر متفاوتی هدایت شوند.

یک مبحث درسی می‌تواند نمره داده شود و نمره‌هایش در دفتر نمره ثبت شوند.

مبحث‌های درسی به عنوان مثال می‌توانند در موارد زیر به کار روند

* به عنوان خودآموزی برای یک مبحث جدید
* سناریوهایی برای تمرین شبیه‌سازی/تصمیم‌گیری
* برای داشتن مرورهای متفاوت، با داشتن سؤال‌هایی متفاوت بسته به پاسخی که به سؤال اول داده شده است';
$string['modulenameplural'] = 'مباحث درسی';
$string['move'] = 'انتقال صفحه';
$string['movedpage'] = 'صفحه منتقل شد';
$string['movepagehere'] = 'انتقال صفحه به اینجا';
$string['moving'] = 'در حال انتقال صفحهٔ: {$a}';
$string['multianswer'] = 'چند پاسخی';
$string['multianswer_help'] = 'اگر بیش از یک پاسخ درست است، این گزینه را انتخاب کنید.';
$string['multichoice'] = 'چندگزینه‌ای';
$string['multipleanswer'] = 'چند پاسخی';
$string['nameapproved'] = 'نام تایید شد';
$string['namereject'] = 'متأسفانه نام شما توسط فیلتر پذیرفته نشد.<br />لطفاً نام دیگری انتخاب نمائید.';
$string['new'] = 'جدید';
$string['nextpage'] = 'صفحهٔ بعد';
$string['noanswer'] = 'به یک یا چند سؤال پاسخی داده نشده است. لطفاً بازگردید و پاسخی را ارائه نمائید.';
$string['noattemptrecordsfound'] = 'هیچ تلاشی صورت نگرفته است: نمره ای داده نشده است';
$string['nobranchtablefound'] = 'هیچ صفحهٔ مندرجاتی پیدا نشد';
$string['noclose'] = 'بدون تاریخ بسته شدن';
$string['nocommentyet'] = 'هنوز نظری داده نشده است.';
$string['nocoursemods'] = 'هیچ فعالیتی پیدا نشد';
$string['nocredit'] = 'نامعتبر';
$string['nodeadline'] = '-';
$string['noessayquestionsfound'] = 'هیچ سؤال تشریحی در این مبحث درسی وجود ندارد';
$string['nohighscores'] = 'هیچ نمرهٔ خوبی وجود ندارد';
$string['nolessonattempts'] = 'هیچ تلاشی در این مبحث درسی صورت نگرفته است.';
$string['nooneansweredcorrectly'] = 'کسی پاسخ صحیح نداد.';
$string['nooneansweredthisquestion'] = 'کسی به این سؤال پاسخ نداده است.';
$string['noonecheckedthis'] = 'هیچ کس این گزینه را انتخاب نکرده است.';
$string['nooneenteredthis'] = 'کسی وارد این قسمت نشده است.';
$string['noonehasanswered'] = 'هنوز کسی به یک سؤال تشریحی پاسخ نداده است.';
$string['noopen'] = 'بدون تاریخ باز شدن';
$string['noretake'] = 'شما اجازهٔ شرکت مجدد در این مبحث درسی را ندارید.';
$string['normal'] = 'طبیعی - مسیر مبحث درسی دنبال شود';
$string['notcompleted'] = 'تمام نشده';
$string['notdefined'] = 'تعریف نشده';
$string['notgraded'] = 'نمره داده نشده است';
$string['notitle'] = 'بدون عنوان';
$string['numberofcorrectanswers'] = 'تعداد پاسخ‌های صحیح: {$a}';
$string['numberofcorrectmatches'] = 'تعداد جفت‌های صحیح: {$a}';
$string['numberofpagestoshow'] = 'تعداد صفحه‌ها جهت نمایش';
$string['numberofpagestoshow_help'] = 'این گزینه، تعداد صفحه‌هایی که در مبحث نمایش داده می‌شوند را مشخص می‌کند. این تنظیم تنها روی مبحث‌هایی که صفحه‌هایشان به ترتیب تصادفی نمایش داده می‌شوند (هنگامی که گزینهٔ «رفتار پس از پاسخ صحیح» بر روی «نمایش یک صفحهٔ دیده نشده» یا «نمایش یک صفحهٔ پاسخ داده نشده» تنظیم شده باشد) قابل اعمال است.';
$string['numberofpagesviewed'] = 'تعداد سؤال‌هایی که پاسخ داده‌اید: {$a}';
$string['numberofpagesviewednotice'] = 'تعداد سؤال‌هایی که پاسخ داده‌اید: {$a->nquestions} (حداقل باید به {$a->minquestions} سؤال پاسخ دهید)';
$string['numerical'] = 'عددی';
$string['ongoing'] = 'نمایش نمره درحال شرکت';
$string['ongoingcustom'] = 'تا الان {$a->score} نمره از {$a->currenthigh} بدست آورده‌اید.';
$string['ongoing_help'] = 'در صورت فعال بودن، نمره‌ای که شاگرد تا هر لحظه کسب کرده است در تمام صفحه‌های مبحث نمایش داده خواهد شد.';
$string['ongoingnormal'] = 'به {$a->correct} سؤال از {$a->viewed} سؤال پاسخ صحیح داده‌اید.';
$string['onpostperpage'] = 'هر نمره فقط یک بار می‌تواند ارسال شود';
$string['options'] = 'انتخاب‌ها';
$string['or'] = 'یا';
$string['ordered'] = 'مرتب شده';
$string['other'] = 'متفرقه';
$string['outof'] = 'از {$a}';
$string['overridegroup'] = 'گروه مورد بازنویسی';
$string['overridegroupeventname'] = '{$a->lesson} - {$a->group}';
$string['overview'] = 'مرور کلی';
$string['overview_help'] = 'یک مبحث درسی از تعدادی صفحه و صفحهٔ مندرجات (به صورت اختیاری) تشکیل شده است. یک صفحه شامل یک سری مطلب است و معمولا با یک سؤال به پایان می‌رسد. برای هر کدام از پاسخ‌هایی که می‌توان به سؤال داد، یک پرش تعریف شده است. این پرش‌ها می‌توانند به صورت نسبی (مانند «همین صفحه» یا «صفحهٔ بعد») و یا مطلق (با مشخص کردن یکی از صفحه‌های مبحث به طور خاص) باشند. یک صفحهٔ مندرجات، صفحه‌ای است که شامل تعدادی پیوند به صفحه‌های دیگر مبحث است (مثلا مانند یک فهرست).';
$string['page'] = 'صفحهٔ {$a}';
$string['pagecontents'] = 'محتوای صفحه';
$string['page-mod-lesson-edit'] = 'صفحهٔ ویرایش مبحث درسی';
$string['page-mod-lesson-view'] = 'صفحهٔ مشاهده یا پیش‌نمایش مبحث درسی';
$string['page-mod-lesson-x'] = 'هر صفحه‌ای از ماژول مبحث درسی';
$string['pages'] = 'صفحه';
$string['pagetitle'] = 'عنوان صفحه';
$string['password'] = 'کلمهٔ رمز';
$string['passwordprotectedlesson'] = 'برای شروع مبحث درسی {$a} نیاز به ورود کلمهٔ رمز دارید.';
$string['pleasecheckoneanswer'] = 'لطفاً یک گزینه را انتخاب نمائید';
$string['pleasecheckoneormoreanswers'] = 'لطفاً یک یا چند گزینه را انتخاب نمائید';
$string['pleaseenteryouranswerinthebox'] = 'لطفا پاسخ خود را در چارچوب وارد نمائید';
$string['pleasematchtheabovepairs'] = 'لطفاً جفت‌های مرتبط را معین کنید';
$string['pluginadministration'] = 'مدیریت مبحث درسی';
$string['pluginname'] = 'مبحث درسی';
$string['pointsearned'] = 'نمرهٔ کسب شده';
$string['postprocesserror'] = 'در حین انجام پس‌پردازش خطایی رخ داد!';
$string['postsuccess'] = 'با موفقیت ارسال شد';
$string['practice'] = 'مبحث درسی تمرینی';
$string['practice_help'] = 'یک مبحث درس تمرینی در دفتر نمره نمایش داده نمی‌شود.';
$string['preprocesserror'] = 'در حین انجام پیش‌پردازش خطایی رخ داد!';
$string['preview'] = 'پیش‌نمایش';
$string['previewlesson'] = 'پیش‌نمایش {$a}';
$string['previouspage'] = 'صفحهٔ قبل';
$string['processerror'] = 'در حین پردازش کردن خطایی رخ داد!';
$string['progressbar'] = 'نوار پیشرفت';
$string['progressbar_help'] = 'در صورت فعال بودن، نواری که درصد تقریبی تکمیل مبحث درسی را نشان می‌دهد، در انتهای صفحه‌های مبحث نمایش داده خواهد شد.';
$string['progressbarteacherwarning'] = 'نوار پیشرفت برای {$a} نمایش داده نمی‌شود';
$string['progressbarteacherwarning2'] = 'شما نوار پیشرفت را نخواهید دید زیرا می‌توانید این مبحث درسی را ویرایش کنید';
$string['progresscompleted'] = '{$a}٪ از این مبحث درسی را کامل کرده‌اید';
$string['qtype'] = 'نوع صفحه';
$string['question'] = 'سؤال';
$string['questionoption'] = 'سؤال';
$string['questiontype'] = 'نوع سؤال';
$string['randombranch'] = 'صفحهٔ مندرجات شانسی';
$string['randompageinbranch'] = 'سؤال شانسی از یک صفحه مندرجات';
$string['rank'] = 'رتبه';
$string['rawgrade'] = 'نمرهٔ خام';
$string['receivedcredit'] = 'کسب امتیاز معتبر';
$string['redisplaypage'] = 'نمایش مجدد صفحه';
$string['removeallgroupoverrides'] = 'پاک کردن تمام بازنویسی‌های صورت گرفته برای گروه‌ها';
$string['removealluseroverrides'] = 'پاک کردن تمام بازنویسی‌های صورت گرفته برای کاربران';
$string['report'] = 'گزارش';
$string['reports'] = 'گزارش‌ها';
$string['response'] = 'واکنش به جواب';
$string['retakesallowed'] = 'شرکت مجدد مجاز است';
$string['retakesallowed_help'] = 'در صورت فعال بودن، شاگردها می‌توانند بیش از یک بار در مبحث درسی شرکت کنند';
$string['returnto'] = 'باز گشت به {$a}';
$string['returntocourse'] = 'بازگشت به درس';
$string['review'] = 'مرور';
$string['reviewlesson'] = 'مرور مبحث درسی';
$string['reviewquestionback'] = 'بله، می‌خواهم مجدداً تلاش کنم';
$string['reviewquestioncontinue'] = 'خیر، می‌خواهم به سؤال بعدی بروم';
$string['sanitycheckfailed'] = 'اعتبار سنجی ناموفق بود: این تلاش پاک شده است';
$string['savechanges'] = 'ذخیرهٔ تغییرات';
$string['savechangesandeol'] = 'ذخیرهٔ همهٔ تغییرات و رفتن به انتهای مبحث درسی.';
$string['savepage'] = 'ذخیرهٔ صفحه';
$string['score'] = 'نمره';
$string['scores'] = 'نمرات';
$string['search:activity'] = 'مبحث درسی - اطلاعات فعالیت';
$string['secondpluswrong'] = 'صحیح نیست. آیا مایلید مجدداً تلاش کنید؟';
$string['selectaqtype'] = 'نوع سؤال را مشخص کنید';
$string['shortanswer'] = 'کوتاه جواب';
$string['showanunansweredpage'] = 'نمایش یک صفحهٔ پاسخ داده نشده';
$string['showanunseenpage'] = 'نمایش یک صفحهٔ دیده نشده';
$string['singleanswer'] = 'تک پاسخی';
$string['skip'] = 'رد کردن راهبری';
$string['slideshow'] = 'نمایش اسلاید';
$string['slideshowbgcolor'] = 'رنگ پس‌زمینهٔ اسلاید';
$string['slideshowheight'] = 'ارتفاع اسلاید';
$string['slideshow_help'] = 'در صورت فعال بودن، مبحث درسی به صورت یک مجموعه اسلاید با عرض و ارتفاعی ثابت نمایش داده می‌شود.';
$string['slideshowwidth'] = 'عرض اسلاید';
$string['startlesson'] = 'شروع مبحث درسی';
$string['studentattemptlesson'] = 'تلاش شمارهٔ {$a->attempt} {$a->firstname} {$a->lastname}';
$string['studentname'] = 'نام {$a}';
$string['studentoneminwarning'] = 'توجه: کمتر از ۱ دقیقه برای اتمام مبحث درسی فرصت دارید.';
$string['studentresponse'] = 'پاسخ {$a}';
$string['submit'] = 'ارائه';
$string['submitname'] = 'ارائه دادن نام';
$string['teacherjumpwarning'] = 'در این مبحث درسی از یک پرش به {$a->cluster} یا پرش به {$a->unseen} استفاده شده است. به جای آن‌ها از پرش به «صفحهٔ بعد» استفاده خواهد شد. برای آزمایش این پرش‌ها بعنوان شاگرد وارد شوید.';
$string['teacherongoingwarning'] = 'نمره در حال شرکت فقط به شاگردان نمایش داده می‌شود. برای آزمایش نمره در حال شرکت، در قالب یک شاگر وارد شوید.';
$string['teachertimerwarning'] = 'تایمر فقط برای شاگردان کار می‌کند. با ورود در قالب یک شاگرد، تایمر را آزمایش کنید.';
$string['thatsthecorrectanswer'] = 'پاسخ شما صحیح است';
$string['thatsthewronganswer'] = 'پاسخ شما اشتباه است';
$string['thefollowingpagesjumptothispage'] = 'صفحه‌های زیر به این صفحه پیوند دارند';
$string['thispage'] = 'این صفحه';
$string['timelimit'] = 'محدودیت زمانی';
$string['timelimit_help'] = 'در صورتی‌که فعال باشد، در ابتدای مبحث درسی اخطاری مبنی بر محدودیت زمانی همراه با یک شمارندهٔ معکوس نمایش داده می‌شود. پاسخ‌هایی که پس از اتمام وقت داده شوند تصحیح (نمره داده) نمی‌شوند.';
$string['timeremaining'] = 'زمان باقیمانده';
$string['timespenterror'] = 'حداقل {$a} دقیقه برای مطالعه و پاسخگوئی به سؤالات مبحث درسی صرف کنید';
$string['timespentminutes'] = 'زمان صرف شده (دقیقه)';
$string['timetaken'] = 'زمان صرف شده';
$string['truefalse'] = 'صحیح/غلط';
$string['unabledtosavefile'] = 'فایلی که ارسال کردید قابل ذخیره شدن نبود';
$string['unknownqtypesnotimported'] = '{$a} سؤال که از انواع سؤال‌های پشتیبانی نشده بودند وارد نشدند';
$string['unseenpageinbranch'] = 'سؤال دیده نشده از صفحه مندرجات';
$string['unsupportedqtype'] = 'نوع پشتیبانی نشده ({$a})!';
$string['updatedpage'] = 'صفحهٔ به‌روز شده';
$string['updatefailed'] = 'به‌روزرسانی با شکست مواجه شد';
$string['usemaximum'] = 'انتخاب بهترین نمره';
$string['usemean'] = 'در نظر گرفتن میانگین';
$string['usepassword'] = 'مبحث درسی محافظت شده با کلمهٔ رمز';
$string['usepassword_help'] = 'در صورت فعال بودن، دسترسی به مبحث درسی نیازمند به وارد کردن رمز است.';
$string['viewgrades'] = 'مشاهدهٔ نمره‌ها';
$string['viewreports'] = 'مشاهدهٔ {$a->attempts} تلاش کامل شده توسط {$a->student}‌ها';
$string['viewreports2'] = 'مشاهدهٔ {$a} تلاش کامل شده';
$string['welldone'] = 'آفرین!';
$string['whatdofirst'] = 'ابتدا مایلید چه کاری انجام دهید؟';
$string['wronganswerjump'] = 'پرش مربوط به پاسخ اشتباه';
$string['wronganswerscore'] = 'نمرهٔ پاسخ اشتباه';
$string['wrongresponse'] = 'واکنش به پاسخ اشتباه';
$string['xattempts'] = '{$a} تلاش';
$string['youhaveseen'] = 'شما قبلاً بیش از یک صفحه از این مبحث درسی را دیده اید.<br />آیا می‌خواهید از آخرین صفحه‌ای که دیده‌اید شروع کنید؟';
$string['youranswer'] = 'پاسخ شما';
$string['yourcurrentgradeis'] = 'نمرهٔ فعلی شما {$a} است';
$string['yourcurrentgradeisoutof'] = 'نمرهٔ فعلی شما {$a->grade} از {$a->total} است';
$string['youshouldview'] = 'باید حداقل به {$a} سؤال پاسخ دهید';
