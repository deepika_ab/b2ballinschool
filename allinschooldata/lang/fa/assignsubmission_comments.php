<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_comments', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   assignsubmission_comments
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['default'] = 'فعال بودن به صورت پیش‌فرض';
$string['default_help'] = 'اگر تعیین شده باشد، این شیوهٔ تحویل به طور پیش‌فرض برای تمام تکلیف‌های جدید فعال خواهد بود.';
$string['enabled'] = 'توضیحات برای تحویل';
$string['enabled_help'] = 'اگر فعال باشد، شاگردان می‌توانند برای تکلیفی که تحویل می‌دهند توضیحاتی هم بنویسند. با استفاده از این قابلیت، در حالت‌هایی که مثلا شاگردان چند فایل مرتبط با هم تحویل می‌دهند، می‌توانند به تصحیح کننده بگویند که کدام فایل فایل اصلی است.';
$string['pluginname'] = 'توضیحات برای تحویل';
