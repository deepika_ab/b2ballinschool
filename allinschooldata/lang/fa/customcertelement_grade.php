<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'customcertelement_grade', language 'fa', branch 'MOODLE_37_STABLE'
 *
 * @package   customcertelement_grade
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['coursegrade'] = 'نمرهٔ درس';
$string['gradeformat'] = 'قالب نمایش نمره';
$string['gradeformat_help'] = 'قالبی که می‌خواهید هنگام نمایش نمره استفاده شود.';
$string['gradeitem'] = 'مرجع نمره';
$string['gradeitem_help'] = 'موردی که می‌خواهید نمره‌اش را نمایش دهید.';
$string['gradeletter'] = 'نمرهٔ حرفی';
$string['gradepercent'] = 'نمرهٔ درصدی';
$string['gradepoints'] = 'نمرهٔ عددی';
$string['pluginname'] = 'نمره';
