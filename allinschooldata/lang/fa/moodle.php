<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'moodle', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   moodle
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['abouttobeinstalled'] = 'در آستانهٔ نصب';
$string['accept'] = 'پذیرش';
$string['action'] = 'اقدام';
$string['actionchoice'] = 'چه اقدامی در ارتباط با فایل\'{$a}\'صورت پذیرد؟';
$string['actions'] = 'اقدامات';
$string['actionsmenu'] = 'منوی اقدامات';
$string['active'] = 'فعال';
$string['activeusers'] = 'کاربران فعال';
$string['activities'] = 'فعالیت‌ها';
$string['activities_help'] = 'فعالیت‌ها (مانند تالارهای گفتگو، آزمون‌ها، ویکی‌ها) امکان اضافه کردن محتواهای تعاملی به درس را فراهم می‌کنند.';
$string['activity'] = 'فعالیت';
$string['activityclipboard'] = 'انتقال این فعالیت: <b>«{$a}»</b>';
$string['activityiscurrentlyhidden'] = 'با عرض معذرت، در حال حاضر این فعالیت پنهان است';
$string['activitymodule'] = 'ماژول فعالیت';
$string['activitymodules'] = 'ماژول‌های فعالیت';
$string['activityreport'] = 'گزارش فعالیت';
$string['activityreports'] = 'گزارش‌های فعالیت';
$string['activityselect'] = 'انتخاب این فعالیت برای انتقال به محلی دیگر';
$string['activitysince'] = 'فعالیت‌های صورت گرفته از {$a}';
$string['activitytypetitle'] = '{$a->activity} - {$a->type}';
$string['activityweighted'] = 'فعالیت بازای هر کاربر';
$string['add'] = 'اضافه کردن';
$string['addactivity'] = 'اضافه کردن یک فعالیت...';
$string['addactivitytosection'] = 'اضافه کردن یک فعالیت به قسمت «{$a}»';
$string['addadmin'] = 'اضافه کردن مدیر';
$string['addblock'] = 'اضافه کردن یک بلوک';
$string['addcomment'] = 'اضافه کردن نظر...';
$string['addcountertousername'] = 'ایجاد کاربر با اضافه کردن عدد به نام کاربری';
$string['addcreator'] = 'اضافه کردن درس‌ساز';
$string['adddots'] = 'اضافه کردن...';
$string['added'] = '{$a} اضافه شد';
$string['addedrecip'] = '{$a} نفر به لیست دریافت‌کنندگان اضافه شد';
$string['addedrecips'] = '{$a} نفر به لیست دریافت‌کنندگان اضافه شد';
$string['addedtogroup'] = 'به گروه «{$a}» اضافه شد';
$string['addedtogroupnot'] = 'به گروه «{$a}» اضافه نشد';
$string['addedtogroupnotenrolled'] = 'به گروه «{$a}» اضافه نشد، زیرا در درس ثبت‌نام نشده است';
$string['addfilehere'] = 'قرار دادن فایل (فایل‌ها) در این قسمت';
$string['addinganew'] = 'اضافه کردن یک {$a} جدید';
$string['addinganewto'] = 'در حال اضافه کردن یک {$a->what} به {$a->to}';
$string['addingdatatoexisting'] = 'اضافه کردن داده به آنچه از قبل موجود است';
$string['additionalnames'] = 'سایر نام‌ها';
$string['addlinkhere'] = 'قرار دادن پیوند در این قسمت';
$string['addnewcategory'] = 'ایجاد طبقهٔ درسی جدید';
$string['addnewcourse'] = 'ایجاد یک درس جدید';
$string['addnewuser'] = 'تعریف یک کاربر جدید';
$string['addnousersrecip'] = 'اضافه کردن کاربرانی که به این {$a} دسترسی نداشته‌اند به لیست دریافت کنندگان';
$string['addpagehere'] = 'قرار دادن متن در این قسمت';
$string['addresource'] = 'اضافه کردن یک منبع...';
$string['addresourceoractivity'] = 'اضافه کردن یک فعالیت یا منبع';
$string['addresourcetosection'] = 'افزودن منبع به بخش';
$string['address'] = 'آدرس';
$string['addsections'] = 'اضافه‌کردن قسمت‌ها';
$string['addstudent'] = 'اضافه‌کردن شاگرد';
$string['addsubcategory'] = 'اضافه کردن یک زیرطبقه';
$string['addteacher'] = 'اضافه‌کردن استاد';
$string['admin'] = 'مدیر';
$string['adminhelpaddnewuser'] = 'جهت ایجاد یک حساب کاربری جدید به صورت دستی';
$string['adminhelpassignadmins'] = 'مدیرها می‌توانند در سایت هر کاری انجام دهند و به هر جائی بروند';
$string['adminhelpassigncreators'] = 'درس‌ساز‌ها می‌توانند درس‌های جدید ایجاد کنند';
$string['adminhelpassignsiteroles'] = 'انتساب نقش‌های تعریف شدهٔ سایت به کاربران خاص';
$string['adminhelpassignstudents'] = 'وارد یکی از درس‌ها شوید و از طریق منوی مدیر شاگردان را اضافه نمائید';
$string['adminhelpauthentication'] = 'می‌توانید از حساب‌های کاربری داخلی و یا پایگاه‌های دادهٔ خارجی استفاده کنید';
$string['adminhelpbackup'] = 'پیکربندی پشتیبان‌گیری خودکار و زمانبندی آن';
$string['adminhelpconfiguration'] = 'پیکربندی شکل ظاهری و نحوهٔ کار سایت';
$string['adminhelpconfigvariables'] = 'پیکربندی متغیرهایی که روی عملکرد کلی سایت تأثیر می‌گذارند';
$string['adminhelpcourses'] = 'تعریف درس‌ها و طبقه‌ها و انتساب افراد به آنها، ویرایش درس‌های معلق';
$string['adminhelpeditorsettings'] = 'تعریف تنظیمات اولیهٔ ویرایشگر HTML';
$string['adminhelpedituser'] = 'مرور لیست حساب‌های کاربری و ویرایش هرکدام از آنها';
$string['adminhelpenvironment'] = 'بررسی اینکه کارگزار شما تا چه حد نیازمندی‌های نصب فعلی و آینده را برآورده می‌کند';
$string['adminhelpfailurelogs'] = 'مرور log های مربوط به تلاش‌های ناموفق برای ورود به سایت';
$string['adminhelplanguage'] = 'جهت بررسی و ویرایش بستهٔ زبانی جاری';
$string['adminhelplogs'] = 'مرور log های مربوط به همهٔ فعالیت‌های سایت';
$string['adminhelpmanageblocks'] = 'مدیریت بلوک‌های نصب شده و تنظیمات آنها';
$string['adminhelpmanagedatabase'] = 'دستیابی مستقیم به پایگاه داده (مراقب باشید!)';
$string['adminhelpmanagefilters'] = 'انتخاب فیلترهای متنی و تنظیمات مربوطه';
$string['adminhelpmanagemodules'] = 'مدیریت ماژول‌های نصب شده و تنظیمات آنها';
$string['adminhelpmanageroles'] = 'ایجاد و تعریف نقش‌های قابل انتساب به کاربران';
$string['adminhelpmymoodle'] = 'پیکربندی بلوک‌های صفحهٔ «مودل من» برای کاربران';
$string['adminhelpreports'] = 'گزارش‌ها در سطح سایت';
$string['adminhelpsitefiles'] = 'برای منتشر کردن فایل‌های عمومی یا ارسال پشتیبان‌های خارجی';
$string['adminhelpsitesettings'] = 'تعیین شکل ظاهری صفحهٔ اول سایت';
$string['adminhelpstickyblocks'] = 'پیکربندی بلوک‌های چسبناک برای کل مودل';
$string['adminhelpthemes'] = 'انتخاب شکل ظاهری سایت (رنگ‌ها، قلم‌ها و غیره)';
$string['adminhelpuploadusers'] = 'وارد کردن حساب‌های کاربری جدید از یک فایل متنی';
$string['adminhelpusers'] = 'کاربران خود را تعریف کرده و نحوهٔ شناسائی را تعیین نمائید';
$string['adminhelpxmldbeditor'] = 'واسطی برای ویرایش فایل‌های XMLDB. مخصوص توسعه‌دهندگان.';
$string['administration'] = 'مدیریت';
$string['administrationsite'] = 'مدیریت سایت';
$string['administrator'] = 'مدیر';
$string['administratordescription'] = 'مدیرها به طور معمول قادر به انجام هر کاری در سایت و در همهٔ درس‌ها هستند.';
$string['administrators'] = 'مدیران';
$string['administratorsall'] = 'همهٔ مدیران';
$string['administratorsandteachers'] = 'مدیران و اساتید';
$string['advanced'] = 'پیشرفته';
$string['advancedfilter'] = 'جستجوی پیشرفته';
$string['advancedsettings'] = 'مورد پیشرفته';
$string['afterresource'] = 'بعد از منبع «{$a}»';
$string['aftersection'] = 'بعد از قسمت «{$a}»';
$string['again'] = 'دوباره';
$string['aimid'] = 'شناسهٔ AIM';
$string['ajaxuse'] = 'AJAX و جاوااسکریپت';
$string['all'] = 'همه';
$string['allactions'] = 'همهٔ اقدامات';
$string['allactivities'] = 'همهٔ فعالیت‌ها';
$string['allcategories'] = 'همه طبقه‌ها';
$string['allchanges'] = 'همهٔ تغییرات';
$string['alldays'] = 'همهٔ روزها';
$string['allfieldsrequired'] = 'وار کردن تمام موارد اجباری است';
$string['allfiles'] = 'همهٔ فایل‌ها';
$string['allgroups'] = 'همهٔ گروه‌ها';
$string['alllogs'] = 'همهٔ log ها';
$string['allmods'] = 'همهٔ {$a}';
$string['allow'] = 'مجاز';
$string['allowinternal'] = 'روش‌های داخلی هم مجاز باشند';
$string['allownone'] = 'هیچ‌کدام';
$string['allownot'] = 'اجازه نده';
$string['allowstealthmodules'] = 'فعالیت‌ها بتوانند قابل دسترسی باشند ولی در قسمت‌های قابل مشاهده در صفحهٔ درس نمایش داده نشوند';
$string['allowstealthmodules_help'] = 'در صورت فعال بودن، تنظیم «در دسترس بودن» در قسمت تنظیمات عمومی ماژول، به جای دو گزینه سه گزینه خواهد داشت: «نمایش در صفحهٔ درس»، «پنهان بودن از شاگردان» و «قابل دسترسی باشد ولی در صفحهٔ درس نمایش داده نشود». اگر فعالیت یا منبعی قابل مشاهده باشد ولی در صفحهٔ درس نمایش داده نشود، باید در جای دیگری (مثلا از منبع دیگری از نوع صفحه) یک پیوند به آن قرار داده شود. البته این فعالیت همچنان در دفتر نمره و در سایر گزارش‌ها نمایش داده خواهد شد.';
$string['allparticipants'] = 'همهٔ اعضاء';
$string['allteachers'] = 'همهٔ اساتید';
$string['alphanumerical'] = 'تنها می‌تواند شامل حروف الفبا یا اعداد، خط تیره (-) یا نقطه (.) باشد';
$string['alreadyconfirmed'] = 'عضویت قبلاً تایید شده است';
$string['alternatename'] = 'نام جایگزین';
$string['always'] = 'همیشه';
$string['and'] = '{$a->one} و {$a->two}';
$string['answer'] = 'پاسخ';
$string['any'] = 'هر چه';
$string['appearance'] = 'ظاهر';
$string['approve'] = 'تصویب';
$string['areyousure'] = 'آیا مطمئن هستید؟';
$string['areyousuretorestorethis'] = 'آیا می‌خواهید ادامه دهید؟';
$string['areyousuretorestorethisinfo'] = 'در قسمت‌های بعدی این فرآیند، می‌توانید انتخاب کنید که این پشتیبان به یک درس موجود اضافه شود یا اینکه کاملاً یک درس جدید ایجاد شود.';
$string['asc'] = 'صعودی';
$string['assessment'] = 'ارزیابی';
$string['assignadmins'] = 'انتساب مدیرها';
$string['assigncreators'] = 'انتساب درس‌سازها';
$string['assignedrolecount'] = '{$a->role}: {$a->count}';
$string['assignsiteroles'] = 'انتساب نقش‌ها در کل سایت';
$string['authenticateduser'] = 'کاربر شناسایی شده';
$string['authenticateduserdescription'] = 'همهٔ کاربرانی که با استفاده از نام کاربری خود وارد سایت شده‌اند.';
$string['authentication'] = 'شناسایی';
$string['authenticationplugins'] = 'پلاگین‌های شناسایی';
$string['autosubscribe'] = 'آبونه شدن خودکار در تالارهای گفتگو';
$string['autosubscribeno'] = 'خیر: من را به صورت خودکار آبونهٔ مباحثات تالارهای گفتگو نکن';
$string['autosubscribeyes'] = 'بله: هرگاه مطلبی نوشتم، من را آبونهٔ آن مباحثه در تالار گفتگو کن';
$string['availability'] = 'در دسترس بودن';
$string['availablecourses'] = 'درس‌های موجود';
$string['back'] = 'بازگشت';
$string['backto'] = 'بازگشت به {$a}';
$string['backtocourselisting'] = 'بازگشت به لیست درس‌ها';
$string['backtohome'] = 'بازگشت به صفحه اصلی سایت';
$string['backtopageyouwereon'] = 'برگشت به صفحه‌ای که در آن بودید';
$string['backtoparticipants'] = 'بازگشت به لیست شرکت کنندگان';
$string['backup'] = 'پشتیبان‌گیری';
$string['backupactivehelp'] = 'انتخاب کنید که آیا پشتیبان‌گیری خودکار انجام شود یا خیر.';
$string['backupcancelled'] = 'پشتیبان‌گیری لغو شد';
$string['backupcoursefileshelp'] = 'در صورت فعال بودن این گزینه فایل‌های درس‌ها در پشتیبان‌های خودکار شامل می‌شوند';
$string['backupdate'] = 'تاریخ پشتیبان';
$string['backupdatenew'] = '&nbsp; {$a->TAG} برابر با {$a->weekday}، {$a->mday} {$a->month} {$a->year} شد<br />';
$string['backupdateold'] = '{$a->TAG} برابر با {$a->weekday}، {$a->mday} {$a->month} {$a->year} بود';
$string['backupdaterecordtype'] = '<br />{$a->recordtype} – {$a->recordname}<br />';
$string['backupdetails'] = 'جزئیات پشتیبان';
$string['backuperrorinvaliddestination'] = 'پوشهٔ‌مقصد پشتیبان‌گیری وجود ندارد یا اینکه قابل نوشتن نیست.';
$string['backupexecuteathelp'] = 'انتخاب کنید که پشتیبان‌گیری خودکار باید در چه ساعتی شروع شود.';
$string['backupfailed'] = 'بعضی از درس‌های شما ذخیره نشدند!!';
$string['backupfilename'] = 'poshtiban';
$string['backupfinished'] = 'پشتیبان‌گیری با موفقیت انجام شد';
$string['backupfromthissite'] = 'پشتیبان‌گیری روی این سایت انجام شده است؟';
$string['backupgradebookhistoryhelp'] = 'در صورت فعال بودن، تاریخچهٔ دفتر نمرات در پشتیان‌های خودکار شامل خواهد شد. توجه نمائید که برای کار کردن این قسمت، تاریخچهٔ نمره‌ها در تنظیمات کارگزار (disablegradehistory) نباید غیرفعال باشد.';
$string['backupincludemoduleshelp'] = 'انتخاب کنید که آیا می‌خواهید پشتیبان‌هایی که به صورت خودکار ساخته می‌شوند شامل ماژول‌های درس، با یا بدون داده‌های کاربران، باشند یا خیر.';
$string['backupincludemoduleuserdatahelp'] = 'انتخاب کنید که آیا می‌خواهید پشتیبان‌هایی که به صورت خودکار ساخته می‌شوند شامل داده‌های کاربران در ماژول‌ها باشند یا خیر.';
$string['backuplogdetailed'] = 'Log اجرا به صورت مفصل';
$string['backuploglaststatus'] = 'log آخرین اجرا';
$string['backupmissinguserinfoperms'] = 'توجه: این پشتیبان شامل اطلاعات کاربران نمی‌باشد. این پشتیبان شامل فعالیت‌های «تمرین» و «کارگاه» نمی‌شود، زیرا این ماژول‌ها با این نوع پشتیبان‌گیری سازگار نیستند.';
$string['backupnext'] = 'پشتیبان بعدی';
$string['backupnonisowarning'] = 'هشدار: این پشتیبان متعلق به یک نسخهٔ غیر unicode مودل است (قدیمی‌تر از نسخهٔ ۱.۶). اگر این پشتیبان حاوی هرگونه متن غیر ISO-8859-1 باشد و بخواهید آن را در این نسخهٔ unicodeی مودل بازیابی نمائید، آن متن‌ها ممکن است بهم بریزند. جهت کسب اطلاعات بیشتر در بارهٔ چگونگی ترمیم این پشتیبان به طور صحیح، <a href="http://docs.moodle.org/en/Backup_FAQ">سؤالات متداول در مورد پشتیبان‌گیری</a> را ببینید.';
$string['backupnotyetrun'] = 'در انتظار پشتیبان‌گیری خودکار';
$string['backuporiginalname'] = 'نام پشتیبان';
$string['backuproleassignments'] = 'پشتیبان‌گیری از انتساب نقش‌ها برای این نقش‌ها';
$string['backupsavetohelp'] = 'مسیر کامل پوشه‌ای که می‌خواهید فایل‌های پشتیبان را در آنجا ذخیره کنید';
$string['backupsitefileshelp'] = 'در صورت فعال بودن فایل‌هایی از سایت که در درس‌ها استفاده شده‌اند در پشتیبان‌های خودکار شامل خواهند شد';
$string['backuptakealook'] = 'لطفاً نگاهی به log های پشتیبان‌گیری خود در این آدرس بیاندازید:
  {$a}';
$string['backupuserfileshelp'] = 'انتخاب کنید که آیا می‌خواهید پشتیبان‌های خودکار شامل فایل‌های کاربران (مثلاً عکس صفحه مشخصات فردی) نیز بشود یا خیر';
$string['backupversion'] = 'نسخهٔ پشتیبان';
$string['badges'] = 'مدال‌ها';
$string['block'] = 'بلوک';
$string['blockconfiga'] = 'پیکربندی یک بلوک {$a}';
$string['blockconfigbad'] = 'این بلوک به طور صحیح پیاده‌سازی نشده است و بنابراین نمی‌تواند واسطی برای پیکربندی فراهم کند.';
$string['blocks'] = 'بلوک‌ها';
$string['blocksaddedit'] = 'اضافه کردن/ویرایش بلوک‌ها';
$string['blockseditoff'] = 'اتمام ویرایش بلوک‌ها';
$string['blocksediton'] = 'ویرایش بلوک‌ها';
$string['blocksetup'] = 'برپائی جدول‌های بلوک';
$string['blocksuccess'] = 'جدول‌های {$a} به درستی برپا شدند';
$string['brief'] = 'به صورت خلاصه';
$string['bulkactions'] = 'عملیات گروهی';
$string['bulkactionselect'] = '{$a} انتخاب عمل گروهی';
$string['bulkmovecoursessuccess'] = '{$a->courses} درس با موفقیت به {$a->category} منتقل شد';
$string['bycourseorder'] = 'ترتیب قرار داشتن در درس';
$string['byname'] = 'از {$a}';
$string['bypassed'] = 'صرف‌نظر شده';
$string['cachecontrols'] = 'Cache کردن کنترل‌ها';
$string['cancel'] = 'انصراف';
$string['cancelled'] = 'لغو شد';
$string['categories'] = 'طبقه‌های درسی';
$string['categoriesandcourses'] = 'طبقه‌های درسی و درس‌ها';
$string['category'] = 'طبقه';
$string['categoryadded'] = 'طبقهٔ «{$a}» اضافه شد';
$string['categorybulkaction'] = 'عملیات گروهی برای طبقه‌های انتخاب شده';
$string['categorycontents'] = 'زیرطبقه‌ها و دروس';
$string['categorycurrentcontents'] = 'محتویات {$a}';
$string['categorydeleted'] = 'طبقهٔ «{$a}» حذف شد';
$string['categoryduplicate'] = 'طبقه‌ای با نام «{$a}» وجود دارد!';
$string['categorymodifiedcancel'] = 'طبقه تغییر کرده است! لطفاً فرایند را لغو کرده و مجدداً تلاش نمائید.';
$string['categoryname'] = 'نام طبقه';
$string['categorysubcategoryof'] = '{$a->category} - زیر گروه {$a->parentcategory}';
$string['categoryupdated'] = 'طبقهٔ «{$a}» به‌روز شد';
$string['changedpassword'] = 'رمز عبور تغییر یافته';
$string['changepassword'] = 'تغییر رمز ورود';
$string['changesmadereallygoaway'] = 'شما تغییراتی در این صفحه داده‌اید. آیا مطمئن هستید که می‌خواهید با خارج شدن از صفحه تغییراتتان از دست برود؟';
$string['changessaved'] = 'تغیرات ذخیره شدند';
$string['check'] = 'بررسی';
$string['checkall'] = 'بررسی همه';
$string['checkingbackup'] = 'در حال بررسی پشتیبان';
$string['checkingcourse'] = 'بررسی شدن درس';
$string['checkingforbbexport'] = 'در حال بررسی صدور از BlackBoard';
$string['checkinginstances'] = 'بررسی نمونه‌های ساخته‌شده';
$string['checkingsections'] = 'بررسی قسمت‌ها';
$string['checklanguage'] = 'بررسی زبان';
$string['checknone'] = 'بررسی هیچ‌کدام';
$string['childcoursenotfound'] = 'درس زیرمجموعه پیدا نشد!';
$string['childcourses'] = 'درس‌های زیرمجموعه';
$string['choose'] = 'انتخاب کنید';
$string['choosecourse'] = 'درسی را انتخاب کنید';
$string['choosedots'] = 'انتخاب کنید...';
$string['chooselivelogs'] = 'یا فعالیت‌های جاری را نگاه کنید';
$string['chooselogs'] = 'log هایی که می‌خواهید ببینید را انتخاب کنید';
$string['choosereportfilter'] = 'فیلتری برای گزارش انتخاب نمائید';
$string['choosetheme'] = 'انتخاب پوسته';
$string['chooseuser'] = 'انتخاب یک کاربر';
$string['city'] = 'شهر/شهرک';
$string['cleaningtempdata'] = 'در حال پاک کردن داده‌های موقت';
$string['clear'] = 'پاک کردن';
$string['clickhelpiconformoreinfo'] = '... ادامه دارد ... برای خواندن مقالهٔ کامل روی آیکن راهنمایی کلیک کنید';
$string['clickhere'] = 'کلیک نمائید ...';
$string['clicktochangeinbrackets'] = '{$a} (برای تغییر حالت کلیک کنید)';
$string['clicktohideshow'] = 'برای باز یا بسته شدن کلیک کنید';
$string['closebuttontitle'] = 'بستن';
$string['closewindow'] = 'بستن این پنجره';
$string['collapse'] = 'جمع‌کردن';
$string['collapseall'] = 'جمع شدن همه ';
$string['collapsecategory'] = 'جمع‌کردن {$a}';
$string['commentincontext'] = 'مشاهدهٔ این نظر در جایی که وارد شده است';
$string['comments'] = 'نظرات';
$string['commentscount'] = 'نظرات ({$a})';
$string['commentsnotenabled'] = 'قابلیت نظر دادن فعال نیست';
$string['commentsrequirelogin'] = 'برای دیدن نظرها باید وارد سیستم شوید.';
$string['comparelanguage'] = 'مقایسه و ویرایش زبان جاری';
$string['complete'] = 'کامل';
$string['completereport'] = 'گزارش کامل';
$string['configuration'] = 'پیکربندی';
$string['confirm'] = 'تایید';
$string['confirmcheckfull'] = 'آیا کاملاً مطمئنید که می‌خواهید {$a} را تایید نمائید؟';
$string['confirmcoursemove'] = 'آیا مطمئنید که می‌خواهید این درس ({$a->course}) را به این طبقه ({$a->category}) منتقل کنید؟';
$string['confirmdeletesection'] = 'آیا واقعاً مطمئن هستید که می‌خواهید بخش «{$a}» و تمام فعالیت‌های داخل آن را به‌طور کامل پاک کنید؟';
$string['confirmed'] = 'عضویت شما تایید شد';
$string['confirmednot'] = 'عضویت شما هنوز تایید نشده است!';
$string['considereddigitalminor'] = 'سن شما برای ایجاد حساب کاربری در این سایت کم ایست.';
$string['content'] = 'محتوا';
$string['continue'] = 'ادامه';
$string['continuetocourse'] = 'برای ورود به درس خود اینجا کلیک نمائید';
$string['convertingwikitomarkdown'] = 'در حال تبدیل ویکی به Markdown';
$string['cookiesenabled'] = 'کوکی‌ها باید در مرورگر شما فعال باشند';
$string['cookiesenabled_help'] = 'دو کوکی توسط این سایت استفاده می‌شوند:

کوکی اصلی کوکی نشست است که معمولاً MoodleSession نامیده می‌شود. مرورگر شما باید به این کوکی اجازه دهد تا تداوم و حفظ ورود بین صفحه‌های مختلف را برای شما فراهم کند. هنگامی که از سایت خارج می‌شوید یا مرورگر را می‌بندید این کوکی از بین می‌رود (در مرورگر شما و در کارگزار).

کوکی دیگر که معمولاً MOODLEID نامیده می‌شود، صرفاً برای راحتی است. از این کوکی فقط برای به خاطر سپردن نام کاربری شما در مرورگر استفاده می‌شود. در نتیجه هرگاه بعداً به این سایت می‌آیید، فیلد نام کاربری در صفحهٔ ورود به سایت از قبل برای شما پر شده خواهد بود. نپذیرفتن این کوکی امن‌تر است - تنها باید هر بار که وارد سایت می‌شوید نام کاربری خود را تایپ کنید.';
$string['cookiesenabledonlysession'] = 'کوکی‌ها باید در مرورگر شما فعال باشند';
$string['cookiesnotenabled'] = 'متآسفانه، در حال حاضر کوکی‌ها در مرورگر شما فعال نیستند';
$string['copy'] = 'کپی';
$string['copyasnoun'] = 'رونوشت';
$string['copyingcoursefiles'] = 'در حال کپی کردن فایل‌های درس';
$string['copyingsitefiles'] = 'در حال کپی کردن فایل‌های سایت که در درس استفاده شده‌اند.';
$string['copyinguserfiles'] = 'در حال کپی کردن فایل‌های کاربران';
$string['copyingzipfile'] = 'در حال کپی کردن فایل فشرده';
$string['copyrightnotice'] = 'اعلان حق نشر';
$string['coresystem'] = 'سیستم مرکزی';
$string['cost'] = 'شهریه';
$string['costdefault'] = 'شهریهٔ پیش‌فرض';
$string['counteditems'] = '{$a->count} {$a->items}';
$string['country'] = 'کشور';
$string['course'] = 'درس';
$string['courseadministration'] = 'مدیریت درس';
$string['courseapprovedemail'] = 'درسی که درخواست کرده بودید ({$a->name}) مورد موافقت قرار گرفت و شما {$a->teacher} شدید. برای دسترسی به درس جدید خود، به آدرس <span dir="ltr" style="display:inline-block;direction:ltr">{$a->url}</span> بروید';
$string['courseapprovedemail2'] = 'درسی که درخواست کرده بودید ({$a->name}) مورد موافقت قرار گرفت. برای دسترسی به درس جدید خود، به آدرس <span dir="ltr" style="display:inline-block;direction:ltr">{$a->url}</span> بروید';
$string['courseapprovedfailed'] = 'ذخیرهٔ درس به‌عنوان موافقت‌شده با شکست مواجه شد!';
$string['courseapprovedsubject'] = 'درس شما مورد موافقت قرار گرفت!';
$string['courseavailable'] = 'این درس توسط شاگردان قابل دسترسی است';
$string['courseavailablenot'] = 'این درس توسط شاگردان قابل دسترسی نیست';
$string['coursebackup'] = 'پشتیبان‌گیری از درس';
$string['coursebulkaction'] = 'عملیات گروهی برای درس‌های انتخاب شده';
$string['coursecategories'] = 'طبقه‌های درسی';
$string['coursecategory'] = 'طبقهٔ درسی';
$string['coursecategorydeleted'] = 'طبقهٔ درسی {$a} حذف شد';
$string['coursecategory_help'] = 'مقدار این گزینه طبقه‌ای که درس در لیست درس‌ها در آن طبقه قرار می‌گیرد تعیین می‌کند.';
$string['coursecatmanagement'] = 'مدیریت درس و طبقه';
$string['coursecompletion'] = 'تکمیل درس';
$string['coursecompletions'] = 'تکمیل درس‌ها';
$string['coursecreators'] = 'درس‌ساز';
$string['coursecreatorsdescription'] = 'درس‌سازها می‌توانند درس‌های جدید بسازند.';
$string['coursedeleted'] = 'درس {$a} حذف شد';
$string['coursedetails'] = 'جزئیات درس';
$string['coursedisplay'] = 'چیدمان درس';
$string['coursedisplay_help'] = 'مقدار این گزینه تعیین می‌کند که آیا تمام قسمت‌های درس در یک صفحه نمایش داده شود یا اینکه در چند صفحه تقسیم شود.';
$string['coursedisplay_multi'] = 'نمایش دادن یک قسمت در هر صفحه';
$string['coursedisplay_single'] = 'نمایش دادن تمام قسمت‌ها در یک صفحه';
$string['courseduration'] = 'مدت درس';
$string['courseduration_desc'] = 'از مدت درس برای محاسبهٔ زمان پیش‌فرض پایان درس استفاده می‌شود. تاریخ پایان درس تنها در گزارش‌ها مورد استفاده قرار می‌گیرد. کاربران بعد از این تاریخ هم همچنان می‌توانند وارد درس شوند.';
$string['courseenddateenabled'] = 'به‌صورت پیش‌فرض، تاریخ پایان درس فعال است';
$string['courseenddateenabled_desc'] = 'این تنظیم تعیین می‌کند که آیا تاریخ اتمام درس برای درس‌های جدید به‌صورت پیش‌فرض فعال بوده و بر روی تاریخی که از محاسبهٔ مدت درس به‌دست می‌آید تنظیم خواهد شد یا خیر.';
$string['courseextendednamedisplay'] = '{$a->shortname} {$a->fullname}';
$string['coursefiles'] = 'فایل‌های موروثی درس';
$string['coursefilesedit'] = 'ویرایش فایل‌های موروثی درس';
$string['coursefileswarning'] = 'فایل‌های درس از رده خارج هستند';
$string['coursefileswarning_help'] = 'از مودل ۲٫۰ به بعد دیگر استفاده از فایل‌های درس توصیه نمی‌شود. در عوض لطفاً تا حد امکان از انباره‌های خارجی استفاده کنید.';
$string['courseformatdata'] = 'اطلاعات قالب درس';
$string['courseformatoptions'] = 'گزینه‌های قالب‌بندی برای {$a}';
$string['courseformats'] = 'قالب‌های درسی';
$string['courseformatudpate'] = 'به‌روز کردن قالب';
$string['coursegrades'] = 'نمره‌های درس';
$string['coursehelpcategory'] = 'درس را در لیست درس‌ها قرار داده و پیدا کردن آن را برای شاگردان را راحت‌تر می‌کند.';
$string['coursehelpforce'] = 'تحمیل حالت گروه‌بندی به تمام فعالیت‌های داخل درس.';
$string['coursehelpformat'] = 'صفحهٔ اصلی درس با این قالب نمایش داده خواهد شد.';
$string['coursehelphiddensections'] = 'قسمت‌های پنهان درس به چه صورتی به شاگردها نمایش داده شوند.';
$string['coursehelpmaximumupload'] = 'بزرگ‌ترین اندازهٔ فایل (محدود شده با تنظیمات سراسری سایت) قابل ارسال در این درس را تعیین می‌کند.';
$string['coursehelpnewsitemsnumber'] = 'تعداد موارد جدیدی از تالار اعلانات که در بلوک تابلوی اعلانات در صفحهٔ درس نشان داده می‌شوند. در صورت انتخاب عدد صفر، تالار اعلانات ساخته نخواهد شد.';
$string['coursehelpnumberweeks'] = 'تعداد قسمت‌هایی که (به‌طور مثال هفته‌ها/موضوع‌هایی که) در صفحهٔ اصلی درس نمایش داده می‌شود (در برخی از قالب‌های درسی کاربرد دارد).';
$string['coursehelpshowgrades'] = 'فعال کردن نمایش دفتر نمرات. این گزینه از نمایش داده شدن نمره‌ها در تک تک فعالیت‌ها جلوگیری نمی‌کند.';
$string['coursehidden'] = 'این درس در حال حاضر در دسترس شاگردان نیست';
$string['courseinfo'] = 'اطلاعات درس';
$string['courselegacyfiles'] = 'فایل‌های موروثی درس';
$string['courselegacyfiles_help'] = 'بخش فایل‌های درس به منظور سازگاری با مودل ۱٫۹ یا نسخه‌های قدیمی‌تر طراحی شده است. تمام فایل‌هایی که در این بخش قرار دارند (فارغ از اینکه به آن‌ها پیوند دهید یا خیر) همواره توسط تمام شرکت کنندگان داخل درس قابل دسترسی هستند و راهی هم برای دانستن اینکه هر کدام از این فایل‌ها در کجای مودل مورد استفاده قرار گرفته است وجود ندارد.

اگر از این قسمت برای نگهداری فایل‌های درس استفاده کنید، خود را در معرض بسیاری از مشکلات امنیتی یا مشکلات مربوط به افشای اطلاعات خصوصی، و همچنین از دست دادن فایل‌ها در نسخه‌های پشتیبان، در درس‌های ساخته شده از روی درس‌های دیگر، و در هر زمان که محتوا به اشتراک گذاشته می‌شود یا مورد استفادهٔ مجدد قرار می‌گیرد قرار می‌دهید. از این رو توصیه می‌شود که از این بخش مگر در مواقع خاص استفاده نشود.

پیوند زیر اطلاعات بیشتری در مورد این مسائل فراهم می‌کند و چند روش بهتر برای مدیریت  فایل‌ها در مودل ۲ را به شما نشان خواهد داد.';
$string['courselegacyfiles_link'] = 'فایل‌های‌درس۲';
$string['courselegacyfilesofcourse'] = 'فایل‌های موروثی درس: {$a}';
$string['coursemessage'] = 'ارسال پیغام به کاربران درس';
$string['coursenotaccessible'] = 'اجازهٔ دسترسی عمومی به این درس وجود ندارد';
$string['courseoverview'] = 'مرور اجمالی درس';
$string['courseoverviewfiles'] = 'فایل‌های توصیف درس';
$string['courseoverviewfilesext'] = 'پسوندهای قابل قبول برای فایل‌های توصیف درس';
$string['courseoverviewfiles_help'] = 'فایل‌ تصویر درس در نمای کلی درس‌ها در میزکار نمایش داده می‌شود.
انواع فایل‌های اضافی مورد پذیریش و تعداد بیش از یک فایل توسط مدیر سایت قابل تنظیم است. اگر چنین شود، این فایل‌ها قابل نمایش در کنار خلاصه درس در صفحه لیست درس‌ها نمایش داده خواهد شد';
$string['courseoverviewfileslimit'] = 'محدودیت تعداد فایل‌های توصیف درس';
$string['courseoverviewgraph'] = 'نمودار مرور اجمالی درس';
$string['coursepreferences'] = 'ترجیحات درس';
$string['courseprofiles'] = 'درس‌ها';
$string['coursereasonforrejecting'] = 'دلائل شما برای رد کردن این درخواست';
$string['coursereasonforrejectingemail'] = 'این متن توسط پست الکترونیک به متقاضی ارسال خواهد شد';
$string['coursereject'] = 'رد درخواست یک درس';
$string['courserejected'] = 'درس پذیرفته نشد و به متقاضی اطلاع داده شد.';
$string['courserejectemail'] = 'متأسفانه درسی که درخواست نموده بودید پذیرفته نشد. دلیلی که برای پذیرفته نشدن درخواست عنوان شده به این شرح است:

{$a}';
$string['courserejectreason'] = 'دلایل خود برای نپذیرفتن این درس را به صورت کلی مطرح نمائید<br />(این متن با پست الکترونیک به متقاضی ارسال می‌شود)';
$string['courserejectsubject'] = 'درس شما پذیرفته نشده است';
$string['coursereport'] = 'گزارش درس';
$string['coursereports'] = 'گزارش‌های درس';
$string['courserequest'] = 'درخواست درس';
$string['courserequestdetails'] = 'جزئیات درس درخواستی';
$string['courserequestfailed'] = 'به دلایلی، درخواست درس شما ذخیره نشد';
$string['courserequestintro'] = 'از این فرم برای درخواست ایجاد یک درس برای خود استفاده نمائید.<br />هرگونه اطلاعاتی که می‌توانید را شرح دهید تا مدیران<br />بتوانند دلایل شما برای خواستن این درس را متوجه شوند.';
$string['courserequestreason'] = 'دلائل درخواست این درس';
$string['courserequestsuccess'] = 'درخواست درس شما با موفقیت ذخیره شد. نتیجهٔ درخواستتان توسط یک نامهٔ الکترونیکی به شما اطلاع داده خواهد شد.';
$string['courserequestsupport'] = 'اطلاعات کمکی برای کمک به مدیر در ارزیابی این درخواست';
$string['courserequestwarning'] = 'کاربری که این درس را درخواست کرده است به‌طور خودکار ثبت‌نام خواهد شد و نقش {$a} را خواهد گرفت.';
$string['courserestore'] = 'بازیابی درس';
$string['courses'] = 'درس‌ها';
$string['coursesearch'] = 'جستجوی درس‌ها';
$string['coursesearch_help'] = '<p>می‌توانید در آن واحد چند کلمه را جستجو کنید. روش جستجو به این صورت است:</p>
<ul>
<li>کلمه: هر مطابقتی از این کلمه در متن را پیدا می‌کند.</li>
<li>+کلمه: فقط کلمات دقیقاً مطابق پیدا در متن می‌شوند.</li>
<li>-کلمه: شامل نتایج دربردارندهٔ این کلمه نباشد.</li>
</ul>';
$string['coursesectionsummaries'] = 'خلاصه‌های قسمت‌های درس';
$string['coursesectiontitle'] = 'درس: {$a->course} ،{$a->sectionname}: {$a->sectiontitle}';
$string['coursesettings'] = 'تنظیمات پیش‌فرض درس';
$string['coursesmovedout'] = 'درس‌ها از طبقهٔ {$a} بیرون آمدند';
$string['coursespending'] = 'تصویب درس‌های معلق';
$string['coursestart'] = 'تاریخ شروع درس';
$string['coursesummary'] = 'توصیف درس';
$string['coursesummary_help'] = 'توصیف هر درس در لیست درس‌ها نمایش داده می‌شود. جستجوی درس‌ها علاوه بر نام درس‌ها، متن توصیف درس‌ها را نیز مورد جستجو قرار می‌دهد.';
$string['coursetitle'] = 'درس: {$a->course}';
$string['courseupdates'] = 'تغییرات درس';
$string['coursevisibility'] = 'قابل مشاهده بودن درس';
$string['coursevisibility_help'] = 'این تنظیم تعیین می‌کند که آیا درس در لیست درس‌ها نمایش داده شود و آیا شاگردان می‌توانند به آن دسترسی داشته باشند یا خیر. اگر روی پنهان شدن تنظیم شود، دسترسی محدود به کاربرانی خواهد بود که توانایی مشاهده کردن درس‌های پنهان را داشته باشند (مانند اساتید).';
$string['create'] = 'ایجاد';
$string['createaccount'] = 'ایجاد حساب کاربری من';
$string['createcategory'] = 'ایجاد طبقه';
$string['createfolder'] = 'ساخت یک پوشه در <span dir="ltr" style="display:inline-block;direction:ltr">{$a}</span>';
$string['createnew'] = 'ایجاد جدید';
$string['createnewcategory'] = 'ایجاد طبقه جدید';
$string['createnewcourse'] = 'ایجاد درس جدید';
$string['createnewsubcategory'] = 'ایجاد زیرطبقه جدید';
$string['createsubcategoryof'] = 'ایجاد زیرطبقه برای {$a}';
$string['createuser'] = 'ساختن کاربر';
$string['createuserandpass'] = 'نام کاربری و رمز عبور خود را انتخاب کنید';
$string['createziparchive'] = 'به صورت یک فایل فشرده آرشیو کن';
$string['creatingblocks'] = 'ساخته شدن بلوک‌ها';
$string['creatingblocksroles'] = 'تعریف انتساب نقش‌ها و بازنویسی مجوزها در سطح بلوک';
$string['creatingblogsinfo'] = 'در حال ایجاد اطلاعات بلاگ‌ها';
$string['creatingcategoriesandquestions'] = 'ایجاد دسته‌ها و سؤالات';
$string['creatingcoursemodules'] = 'ایجاد ماژول‌های درسی';
$string['creatingcourseroles'] = 'تعریف انتساب نقش‌ها و بازنویسی مجوزها در سطح درس';
$string['creatingevents'] = 'تعریف رویدادها';
$string['creatinggradebook'] = 'ایجاد دفتر نمره';
$string['creatinggroupings'] = 'ساخته شدن ابرگروه‌ها';
$string['creatinggroupingsgroups'] = 'اضافه شدن گروه‌ها به ابرگروه‌ها';
$string['creatinggroups'] = 'ساخته شدن گروه‌ها';
$string['creatinglogentries'] = 'در حال ایجاد جزئیات log';
$string['creatingmessagesinfo'] = 'در حال ایجاد اطلاعات پیغام‌ها';
$string['creatingmodroles'] = 'تعریف انتساب نقش‌ها و بازنویسی مجوزها در سطح ماژول';
$string['creatingnewcourse'] = 'ایجاد درس جدید';
$string['creatingrolesdefinitions'] = 'تعریف نقش‌های جدید';
$string['creatingscales'] = 'تعریف مقیاس‌ها';
$string['creatingsections'] = 'ساخته شدن قسمت‌ها';
$string['creatingtemporarystructures'] = 'در حال ایجاد ساختارهای موقتی';
$string['creatinguserroles'] = 'تعریف انتساب نقش‌ها و بازنویسی مجوزها در سطح کاربر';
$string['creatingusers'] = 'تعریف کاربران';
$string['creatingxmlfile'] = 'در حال ایجاد فایل XML';
$string['currency'] = 'پول رایج';
$string['currentcourse'] = 'درس جاری';
$string['currentcourseadding'] = 'درس جاری، با اضافه کردن داده‌ها به آن';
$string['currentcoursedeleting'] = 'درس جاری، با حذف درس و ایجاد مجدد آن';
$string['currentlanguage'] = 'زبان فعلی';
$string['currentlocaltime'] = 'زمان محلی شما';
$string['currentlyselectedusers'] = 'کاربران انتخاب شده';
$string['currentpicture'] = 'عکس فعلی';
$string['currentrelease'] = 'اطلاعات انتشار فعلی';
$string['currentversion'] = 'نسخهٔ مورد استفاده';
$string['databasechecking'] = 'ارتقای پایگاه دادهٔ مودل از نسخهٔ <span dir="ltr" style="display:inline-block;direction:ltr">{$a->oldversion}</span> به <span dir="ltr" style="display:inline-block;direction:ltr">{$a->newversion}</span>...';
$string['databaseperformance'] = 'عملکرد پایگاه داده';
$string['databasesetup'] = 'برپایی پایگاه داده';
$string['databasesuccess'] = 'پایگاه داده با موفقیت ارتقا یافت';
$string['databaseupgradebackups'] = 'نسخهٔ پشتیبان هم‌اکنون {$a} است';
$string['databaseupgradeblocks'] = 'نسخهٔ بلوک‌ها هم‌اکنون {$a} است';
$string['databaseupgradegroups'] = 'نسخهٔ گروه‌ها هم‌اکنون {$a} است';
$string['databaseupgradelocal'] = 'نسخهٔ سفارشی سازی های پایگاه دادهٔ محلی هم‌اکنون {$a} است';
$string['databaseupgrades'] = 'در حال ارتقای پایگاه داده';
$string['dataformats'] = 'قالب‌های داده';
$string['date'] = 'تاریخ';
$string['datechanged'] = 'تاریخ تغییر کرد';
$string['datemostrecentfirst'] = 'تاریخ - جدیدترین در ابتدا';
$string['datemostrecentlast'] = 'تاریخ - جدیدترین در انتها';
$string['day'] = 'روز';
$string['days'] = 'روز';
$string['decodinginternallinks'] = 'رمزگشائی پیوندهای داخلی';
$string['default'] = 'پیش‌فرض';
$string['defaultcompetencescale'] = 'مقیاس پیش‌فرض شایستگی';
$string['defaultcompetencescaledesc'] = 'یک مقیاس دومقداری که به‌جزاینکه کسی مهارت و تسلط خود را نشان داده است یا خیر، اطلاعات بیشتری نمی‌دهد.';
$string['defaultcompetencescalenotproficient'] = 'هنوز شایسته نیست';
$string['defaultcompetencescaleproficient'] = 'شایسته';
$string['defaultcoursestudent'] = 'شاگرد';
$string['defaultcoursestudentdescription'] = 'شاگردان معمولاً حقوق محدودتری در درس دارند.';
$string['defaultcoursestudents'] = 'شاگردان';
$string['defaultcoursesummary'] = 'در این قسمت یک پاراگراف مختصر و جالب توجه بنویسید که توضیح دهد این درس در مورد چیست';
$string['defaultcourseteacher'] = 'استاد';
$string['defaultcourseteacherdescription'] = 'اساتید می‌توانند در درس خود هر کاری از جمله تغییر فعالیت‌ها و نمره‌دادن به شاگردان را انجام دهند.';
$string['defaultcourseteachers'] = 'اساتید';
$string['defaulteditor'] = 'ویرایشگر پیش‌فرض';
$string['delete'] = 'حذف';
$string['deleteablock'] = 'حذف یک بلوک';
$string['deleteall'] = 'حذف همه';
$string['deleteallcannotundo'] = 'حذف همه - قابل بازگشت نیست';
$string['deleteallcomments'] = 'حذف همهٔ نظرها';
$string['deleteallratings'] = 'حذف همهٔ امتیازها';
$string['deletecategory'] = 'حذف طبقه: {$a}';
$string['deletecategorycheck'] = 'آیا واقعاً مطمئن هستید که می‌خواهید این طبقه (<b>{$a}</b>) را کاملاً حذف نمائید؟<br />این کار همهٔ درس‌ها را به طبقهٔ بیرونی (در صورت وجود) یا متفرقه منتقل خواهد کرد.';
$string['deletecategorycheck2'] = 'اگر می‌خواهید این طبقه را حذف کنید، باید مشخص کنید که با درس‌ها و زیرطبقه‌های داخلی چه رفتاری شود.';
$string['deletecategoryempty'] = 'این طبقه خالی است.';
$string['deletecheck'] = 'حذف {$a} ؟';
$string['deletecheckfiles'] = 'آیا کاملاً مطمئن هستید که می‌خواهید این فایل‌ها را حذف نمائید؟';
$string['deletecheckfull'] = 'آیا کاملاً مطمئنید که می‌خواهید حساب کاربری {$a} را به طور کامل (همراه با ثبت‌نام‌ها، داده‌های ثبت‌شده در فعالیت‌ها و سایر داده‌های مربوط به این کاربر) حذف نمائید؟';
$string['deletechecktype'] = 'آیا مطمئنید که می‌خواهید این {$a->type} را پاک کنید؟';
$string['deletechecktypename'] = 'آیا مطمئنید که می‌خواهید {$a->type} «{$a->name}» را پاک کنید؟';
$string['deletecheckwarning'] = 'شما در آستانهٔ حذف این فایل‌ها هستید';
$string['deletecommentbyon'] = 'پاک‌کردن نظر ارسال‌شده توسط {$a->user} در {$a->time}';
$string['deletecompletely'] = 'به طور کامل حذف کن';
$string['deletecourse'] = 'حذف یک درس';
$string['deletecoursecheck'] = 'آیا کاملاً مطمئن هستید که می‌خواهید این درس و همهٔ اطلاعاتی که شامل شده است را به طور کامل حذف نمائید؟';
$string['deleted'] = 'حذف شد';
$string['deletedactivity'] = '{$a} حذف شد';
$string['deletedcourse'] = '{$a} به طور کامل حذف شد';
$string['deletednot'] = '{$a} قابل حذف نبود !';
$string['deletepicture'] = 'حذف';
$string['deletesection'] = 'حذف قسمت';
$string['deleteselected'] = 'حذف موارد انتخاب شده';
$string['deleteselectedkey'] = 'حذف کلید انتخابی';
$string['deletingcourse'] = 'در حال حذف {$a}';
$string['deletingexistingcoursedata'] = 'حذف داده‌های درس موجود';
$string['deletingolddata'] = 'در حال حذف داده‌های قدیمی';
$string['department'] = 'دپارتمان';
$string['deprecatedeventname'] = '{$a} (دیگر مورد استفاده نیست)';
$string['desc'] = 'نزولی';
$string['description'] = 'توصیف';
$string['deselectall'] = 'انتخاب هیچ';
$string['detailedless'] = 'با جزئیات کمتر';
$string['detailedmore'] = 'با جزئیات بیشتر';
$string['digitalminor_desc'] = 'لطفا از سرپرست یا والدین خود بخواهید تا با این شخص تماس بگیرد:';
$string['directory'] = 'دایرکتوری';
$string['disable'] = 'غیر فعال';
$string['disabledcomments'] = 'ثبت نظر غیر فعال است';
$string['displayingfirst'] = 'فقط {$a->count} {$a->things} اول نمایش داده شده‌اند';
$string['displayingrecords'] = 'نمایش {$a} رکورد';
$string['displayingusers'] = 'نمایش کاربران از {$a->start} تا {$a->end}';
$string['displayonpage'] = 'نمایش در صفحه';
$string['dndcourse'] = 'شما می‌توانید با گرفتن و رها کردن ترتیب این درس را تغییر دهید یا آنرا به یک طبقه دیگر منتقل کنید';
$string['dndenabled_inbox'] = 'برای اضافه کردن فایل، می‌توانید فایل‌های مورد نظر را بکشید و در این قسمت رها کنید.';
$string['dndnotsupported'] = 'ارسال فایل از طریق کشیدن و رها کردن پشتیبانی نمی‌شود';
$string['dndnotsupported_help'] = 'مرورگر شما قابلیت ارسال فایل از طریق کشیدن و رها کردن را پشتیبانی نمی‌کند.<br />این قابلیت در تمام نسخه‌های اخیر کروم، فایرفاکس، سافاری، و همچنین اینترنت اکسپلورر نسخه ۱۰ به بالا موجود است.';
$string['dndnotsupported_insentence'] = 'قابلیت کشیدن و رها کردن پشتیبانی نمی‌شود';
$string['dnduploadwithoutcontent'] = 'این بارگذاری شامل هیچ محتوایی نیست';
$string['dndworkingfile'] = 'برای ارسال فایل‌ها می‌توانید آنها را از روی رایانهٔ خود بکشید و در قسمت‌های مختلف درس رها کنید';
$string['dndworkingfilelink'] = 'برای ارسال فایل یا پیوند می‌توانید آنها را از روی رایانهٔ خود بکشید و در قسمت‌های مختلف درس رها کنید';
$string['dndworkingfiletext'] = 'برای ارسال فایل یا متن می‌توانید آنها را از روی رایانهٔ خود بکشید و در قسمت‌های مختلف درس رها کنید';
$string['dndworkingfiletextlink'] = 'برای ارسال فایل، متن یا پیوند می‌توانید آنها را از روی رایانهٔ خود بکشید و در قسمت‌های مختلف درس رها کنید';
$string['dndworkinglink'] = 'برای آپلود پیوندها در بخش های درس آنها را بگیرید و رها کنید';
$string['dndworkingtext'] = 'برای آپلود متن در بخش‌های درس آنها را بگیرید و در جای مورد نظر رها کنید';
$string['dndworkingtextlink'] = 'برای آپلود لینک یا متن در بخش های درس آنها را بگیرید و در جای مورد نظر رها کنید';
$string['documentation'] = 'مستندات مودل';
$string['dontsortcategories'] = 'طبقه‌ها را مرتب نکن';
$string['dontsortcourses'] = 'درس‌ها را مرتب نکن';
$string['down'] = 'پائین';
$string['download'] = 'دریافت';
$string['downloadall'] = 'دریافت همه';
$string['downloadexcel'] = 'دریافت در قالب فایل اکسل';
$string['downloadfile'] = 'دریافت فایل';
$string['downloadods'] = 'دریافت در قالب فایل ODS';
$string['downloadtext'] = 'دریافت در قالب فایل متنی';
$string['doyouagree'] = 'آیا شرایط را خواندید و متوجه شدید؟';
$string['droptoupload'] = 'فایل‌ها را در این قسمت رها کنید تا اضافه شوند';
$string['duplicate'] = 'تکثیر';
$string['duplicatedmodule'] = '{$a} (کپی)';
$string['edhelpaspellpath'] = 'برای استفاده از بررسی کنندهٔ املا در ویرایشگر، <u>باید</u> <strong dir="ltr" style="display:inline-block;direction:ltr">aspell 0.50</strong> یا نسخهٔ جدیدتر نصب باشد، و شما باید مسیر صحیح برای دسترسی به فایل باینری aspell را مشخص کنید. در سیستم‌های یونیکسی/لینوکسی، این مسیر معمولاً <span dir="ltr" style="display:inline-block;direction:ltr"><strong>/usr/bin/aspell</strong></span> است، ولی امکان دارد که متفاوت باشد.';
$string['edhelpbgcolor'] = 'رنگ پس‌زمینهٔ ناحیهٔ ویرایش را تعیین کنید.<br />مقادیر معتبر به‌عنوان مثال عبارتند از: <span dir="ltr" style="display:inline-block;direction:ltr">#FFFFFF</span> یا white';
$string['edhelpcleanword'] = 'این گزینه پاک‌سازی قالب‌های Word را فعال یا غیرفعال می‌کند.';
$string['edhelpenablespelling'] = 'فعال یا غیرفعال کردن بررسی املاء. در صورت فعال بودن، باید <strong>aspell</strong> روی کارگزار نصب شده باشد. مقدار دوم <strong>لغت‌نامهٔ پیش‌فرض</strong> است. این مقدار در صورتی که aspell برای زبان کاربر دارای لغت‌نامه نباشد استفاده خواهد شد.';
$string['edhelpfontfamily'] = 'ویژگی font-family، لیستی از نام‌های خانوادهٔ قلم و/یا نام‌های خانوادهٔ عمومی است. نام‌های خانواده باید توسط کاما جدا شده باشند.';
$string['edhelpfontlist'] = 'قلم‌هایی که در منوی ویرایشگر استفاده می‌شوند را تعیین نمائید.';
$string['edhelpfontsize'] = 'اندازهٔ پیش‌فرض قلم.<br />مقادیر معتبر به‌عنوان مثال عبارتند از:medium، large، smaller، larger، 10pt، 11px.';
$string['edit'] = 'ویرایش ';
$string['edita'] = 'ویرایش {$a}';
$string['editcategorysettings'] = 'ویرایش تنظیمات طبقه';
$string['editcategorythis'] = 'ویرایش این طبقه';
$string['editcoursesettings'] = 'ویرایش تنظیمات درس';
$string['editfiles'] = 'ویرایش فایل‌ها';
$string['editgroupprofile'] = 'ویرایش مشخصات گروه';
$string['editinga'] = 'در حال ویرایش {$a}';
$string['editingteachershort'] = 'ویراستار';
$string['editlock'] = 'این مقدار قابل ویرایش نیست!';
$string['editmyprofile'] = 'ویرایش مشخصات فردی';
$string['editorbgcolor'] = 'رنگ پس‌زمینه';
$string['editorcleanonpaste'] = 'پاک سازی HTMLهای Word هنگام درج از حافظه';
$string['editorcommonsettings'] = 'تنظیمات عمومی';
$string['editordefaultfont'] = 'قلم پیش‌فرض';
$string['editorenablespelling'] = 'فعال کردن بررسی املا';
$string['editorfontlist'] = 'لیست قلم‌ها';
$string['editorfontsize'] = 'اندازهٔ پیش‌فرض قلم';
$string['editorpreferences'] = 'ترجیحات ویرایشگر';
$string['editorresettodefaults'] = 'بازگشت به مقادیر پیش‌فرض';
$string['editorsettings'] = 'تنظیمات ویرایشگر';
$string['editorshortcutkeys'] = 'کلیدهای میانبر ویرایشگر';
$string['editsection'] = 'ویرایش قسمت';
$string['editsectionname'] = 'ویرایش نام قسمت';
$string['editsettings'] = 'پیکربندی';
$string['editsummary'] = 'ویرایش خلاصه';
$string['editthisactivity'] = 'ویرایش این فعالیت';
$string['editthiscategory'] = 'ویرایش این طبقه';
$string['edittitle'] = 'ویرایش عنوان';
$string['edittitleinstructions'] = 'برای تایید تغییر نام کلید Enter و برای لغو تغییرات کلید Escape را بر روی صفحه کلید خود فشار دهید';
$string['edituser'] = 'ویرایش حساب‌های کاربری';
$string['edulevel'] = 'تمام رویدادها';
$string['edulevel_help'] = '* تدریس - کارهایی که توسط یک استاد انجام می‌شود، مانند به‌روز کردن یک منبع درسی
* مشارکت - کارهایی که توسط یک شاگرد انجام می‌شود، مانند طرح مطلب در یک تالار گفتگو
* سایر - کارهایی که توسط کاربری با نقشی به‌جز استاد یا شاگرد انجام می‌شود';
$string['edulevelother'] = 'دیگر';
$string['edulevelparticipating'] = 'شرکت کردن';
$string['edulevelteacher'] = 'تدریس';
$string['email'] = 'آدرس پست الکترونیک';
$string['emailactive'] = 'فعال بودن پست الکترونیک';
$string['emailagain'] = 'پست الکترونیک (دوباره)';
$string['emailalreadysent'] = 'یک ایمیل تغییر رمز ورود ارسال شده است. لطفا ایمیل خود را چک کنید.';
$string['emailcharset'] = 'charset نامه‌های الکترونیکی';
$string['emailconfirm'] = 'حساب کاربری خود را تایید کنید';
$string['emailconfirmation'] = 'سلام {$a->firstname}،

یک حساب کاربری جدید در «{$a->sitename}» با استفاده از
آدرس پست الکترونیکی شما تقاضا شده است.

جهت تایید حساب کاربری جدید، لطفاً به این آدرس اینترنتی بروید:

{$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

در صورت نیاز به کمک، لطفاً با مدیر سایت تماس بگیرید،
{$a->admin}';
$string['emailconfirmationresend'] = 'ارسال مجدد ایمیل تاییدیه';
$string['emailconfirmationsubject'] = '{$a}: تایید حساب کاربری';
$string['emailconfirmsent'] = '<p>باید نامه‌ای به آدرس شما در <b>{$a}</b> فرستاده شده باشد</p>
   <p>این نامه شامل دستورالعمل‌های ساده‌ای برای تکمیل عضویت شما است.</p>
   <p>در صورت تداوم مواجهه با مشکل، با مدیر سایت تماس بگیرید.</p>';
$string['emailconfirmsentfailure'] = 'ارسال ایمیل تاییدیه با شکست مواجه شد';
$string['emailconfirmsentsuccess'] = 'ارسال ایمیل تاییدیه موفقیت آمیز بود';
$string['emaildigest'] = 'نوع پست الکترونیک خلاصه';
$string['emaildigestcomplete'] = 'کامل (یک نامهٔ روزانه شامل همهٔ مطالب)';
$string['emaildigest_help'] = 'این تنظیمات خلاصه روزانه است که تالارها به صورت پیش فرض استفاده می‌کنند:

* بدون خلاصه - شما برای هر پست تالار یک ایمیل دریافت می‌کنید؛
* خلاصه - پست‌های کامل - شما یک ایمیل خلاصه در روز شامل محتوای کامل هر پست تالار دریافت می‌کنید؛
* خلاصه - فقط عناوین - شما یک ایمیل خلاصه در روز شامل فقط عنوان هر پست تالار دریافت می‌کنید.

شما همچنین می توانید برای هر تالار تنظیم متفاوتی انتخاب کنید.';
$string['emaildigestoff'] = 'غیر فعال (یک نامهٔ مجزا بازای هر ارسال به تالار گفتگو)';
$string['emaildigestsubjects'] = 'موضوع‌ها (یک نامهٔ روزانه فقط شامل موضوع‌ها)';
$string['emaildisable'] = 'این آدرس پست الکترونیکی غیر فعال است';
$string['emaildisableclick'] = 'جهت جلوگیری از ارسال همهٔ نامه‌های الکترونیکی به این آدرس، اینجا را کلیک کنید';
$string['emaildisplay'] = 'نمایش آدرس پست الکترونیک';
$string['emaildisplaycourse'] = 'فقط اعضای کلاس اجازه دارند آدرس پست الکترونیک من را ببینند';
$string['emaildisplay_help'] = 'افراد مجاز (همانند معلمان و مدیران) همواره قادر به مشاهده آدرس پست الکترونیک شما خواهند بود.';
$string['emaildisplayhidden'] = 'آدرس پست الکترونیک پنهان شده است';
$string['emaildisplayno'] = 'آدرس پست الکترونیک من را به کسی نشان نده';
$string['emaildisplayyes'] = 'همه بتوانند آدرس پست الکترونیک من را ببینند';
$string['emailenable'] = 'این آدرس پست الکترونیکی فعال است';
$string['emailenableclick'] = 'جهت راه‌اندازی مجدد ارسال نامه‌های الکترونیکی به این آدرس، اینجا را کلیک کنید';
$string['emailexists'] = 'این آدرس پست الکترونیکی قبلاً عضو شده است.';
$string['emailexistshintlink'] = 'بازیابی نام‌کاربری یا رمز ورود';
$string['emailexistssignuphint'] = 'شاید قبلاً یک حساب کاربری ایجاد کرده‌اید؟ {$a}';
$string['emailformat'] = 'قالب پست الکترونیکی';
$string['emailmustbereal'] = 'توجه: آدرس پست الکترونیکی شما باید واقعی باشد';
$string['emailnotallowed'] = 'آدرس‌های پست الکترونیکی متعلق به این دامنه‌ها مجاز نیستند ({$a})';
$string['emailnotfound'] = 'این آدرس پست الکترونیکی در پایگاه داده وجود ندارد';
$string['emailonlyallowed'] = 'این آدرس پست الکترونیک جزء آدرس‌های مجاز ({$a}) نیست';
$string['emailpasswordchangeinfo'] = 'سلام {$a->firstname}،

شخصی (احتمالاً خودتان) تقاضای دریافت یک رمز ورود جدید برای حساب کاربری شما
در «{$a->sitename}» را نموده است.

برای تغییر رمز ورود خود، لطفاً با آدرس اینترنتی زیر بروید:

{$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

در صورت نیاز به کمک، لطفاً با مدیر سایت تماس بگیرید،
{$a->admin}';
$string['emailpasswordchangeinfodisabled'] = 'سلام،

شخصی (احتمالاً خودتان) تقاضای دریافت یک رمز ورود جدید برای حساب کاربری شما
در «{$a->sitename}» را نموده است.

متأسفانه حساب کاربری شما در این سایت غیرفعال است، بنابراین رمز شما نمی‌تواند بازنشانی شود.
لطفاً با مدیر سایت {$a->admin} تماس بگیرید.';
$string['emailpasswordchangeinfofail'] = 'سلام،

شخصی (احتمالاً خودتان) تقاضای دریافت یک رمز ورود جدید برای حساب کاربری شما
در «{$a->sitename}» را نموده است.

متأسفانه در این سایت امکان بازنشانی رمزهای ورود وجود ندارد. لطفاً با مدیر سایت {$a->admin} تماس بگیرید.';
$string['emailpasswordchangeinfosubject'] = '{$a}: اطلاعات مربوط به تغییر رمز ورود';
$string['emailpasswordconfirmation'] = 'سلام {$a->firstname}،

شخصی (احتمالاً خودتان) تقاضای دریافت یک رمز ورود جدید برای حساب کاربری شما
در «{$a->sitename}» را نموده است.

برای تایید کردن این تقاضا و دریافت یک رمز ورود جدید از طریق پست الکترونیک،
به آدرس اینترنتی زیر بروید:

{$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

در صورت نیاز به کمک، لطفاً با مدیر سایت تماس بگیرید،
{$a->admin}';
$string['emailpasswordconfirmationsubject'] = '{$a}: تایید تغییر رمز ورود';
$string['emailpasswordconfirmmaybesent'] = '<p>اگر یک نام کاربری یا آدرس پست الکترونیک صحیح را وراد کرده باشید، یک نامهٔ الکترونیکی باید به شما ارسال شده باشد.</p>
   <p>این نامه محتوی دستورالعمل‌های ساده‌ای برای تایید و تکمیل تعویض رمز ورود است.
اگر مشکل شما همچنان برقرار بود، لطفاً با مدیر سایت تماس بگیرید.</p>';
$string['emailpasswordconfirmnoemail'] = '<p>حساب کاربری که مشخص کرده اید یک آدرس ایمیل ثبت شده ندارد.</p>
<p>لطفا با مدیر سایت تماس بگیرید.</p>';
$string['emailpasswordconfirmnotsent'] = '<p>جزییات کاربری که شما وارد کرده اید در حساب های کاربری موجود یافت نشد.</p>
<p>لطفا اطلاعات وارد شده را بررسی کنید و دوباره تلاش کنید.
اگر هنوز مشکل ادامه داشت، با مدیر سایت تماس بگیرید.</p>';
$string['emailpasswordconfirmsent'] = 'باید نامه‌ای به آدرس شما در <b>{$a}</b> فرستاده شده باشد.
<br />این نامه شامل دستورالعمل‌های ساده‌ای برای تایید و تکمیل عوض شدن رمز ورود است.
اگر همچنان با مشکل مواجه می‌شوید، با مدیر سایت تماس بگیرید.';
$string['emailpasswordsent'] = 'از اینکه تغییر رمز ورود را تایید کرده‌اید متشکریم.
یک نامهٔ الکترونیکی که شامل رمز ورود جدید شما است به آدرس شما در <b>{$a->email}</b> فرستاده شده است.<br />
رمز ورود جدید به صورت خودکار تولید شده است - در صورت تمایل می‌توانید این رمز را
به عبارت ساده‌تری که راحت‌تر بخاطر سپرده می‌شود <a href="{$a->link}">تغییر دهید</a>.';
$string['emailresetconfirmation'] = 'سلام {$a->firstname} {$a->lastname}،

برای حساب کاربری شما (با نام کاربری {$a->username})  در سایت {$a->sitename} تقاضای بازنشانی رمز ورود شده است.

برای تایید این درخواست، و تعیین یک رمز ورود جدید برای حساب کاربری‌تان، لطفا به آدرس اینترنتی زیر مراجعه فرمایید:

{$a->link}
(این پیوند به‌مدت {$a->resetminutes} دقیقه از اولین زمانی که بازنشانی درخواست شده است معتبر خواهد بود.)

اگر درخواست بازنشانی رمز ورود از جانب شما صورت نگرفته است، نیازی نیست کاری انجام دهید.

در صورت نیاز به راهنمایی، لطفا با مدیر سایت ({$a->admin}) تماس بگیرید.';
$string['emailresetconfirmationsubject'] = '{$a}: درخواست بازنشانی رمزعبور';
$string['emailresetconfirmsent'] = 'یک ایمیل به آدرس شما در <b>{$a}</b> ارسال شده است. <br/> این ایمیل حاوی دستورات ساده‌ای برای تایید و کامل کردن تغییر رمز عبور است. اگر باز هم مشکل داشتید با مدیر سایت تماس بگیرید.';
$string['emailtoprivatefiles'] = 'می‌توانید فایل‌‌ها را با ضمیمه‌کردن به پست الکترونیکی هم مستقیما به محل فایل‌های خصوصی خود بفرستید. کافیست فایل‌ها را ضمیمهٔ یک نامهٔ الکترونیکی کنید و نامه را به آدرس {$a} ارسال کنید.';
$string['emailtoprivatefilesdenied'] = 'مدیر سایت شما امکان بارگذاری فایل در ناحیه فایل‌های خصوصی‌تان را غیرفعال کرده است.';
$string['emailuserhasnone'] = 'هیچ آدرس ایمیلی برای این کاربر نیست';
$string['emailvia'] = '{$a->name} (به‌واسطهٔ {$a->url})';
$string['emptydragdropregion'] = 'منطقه تهی';
$string['enable'] = 'فعال‌سازی';
$string['encryptedcode'] = 'کد رمزگذاری شده';
$string['enddate'] = 'تاریخ پایان درس';
$string['enddate_help'] = 'تاریخ پایان درس تنها برای گزارش‌گیری استفاده می‌شود. کاربران بعد از این تاریخ هم همچنان می‌توانند وارد درس شوند.';
$string['english'] = 'انگلیسی';
$string['enrolmentmethods'] = 'روش‌های ثبت نام';
$string['entercourse'] = 'کلیک کنید تا وارد این درس شوید';
$string['enteremail'] = 'آدرس پست الکترونیک خود را وارد کنید';
$string['enteremailaddress'] = 'آدرس پست الکترونیک خود را وارد نمائید تا رمز ورود شما بازنشانی شده و
و رمز ورود جدید از طریق پست الکترونیک برایتان ارسال شود.';
$string['enterusername'] = 'نام کاربری خود را وارد کنید';
$string['entries'] = 'ورودی‌ها';
$string['error'] = 'خطا';
$string['errorcreatingactivity'] = 'امکان ایجاد یک نمونه از فعالیت \'{$a}\' وجود ندارد';
$string['errorfiletoobig'] = 'فایل بزرگ‌تر از محدودهٔ حجم {$a} بایت است';
$string['errornouploadrepo'] = 'هیچ انباره‌ای از نوع ارسال فایل برای این سایت فعال نشده است';
$string['errorwhenconfirming'] = 'به دلیل وقوع یک خطا، هنوز تایید نشده‌اید. اگر با کلیک بر روی پیوندی در یک نامهٔ الکترونیک به اینجا آمده‌اید، اطمینان یابد که خط مربوط به پیوند در نامه شکسته نشده باشد. می‌توانید از کپی کردن و درج آدرس در مرورگر جهت احیای پیوند استفاده نمائید.';
$string['eventcommentcreated'] = 'توضیح ایجاد شد';
$string['eventcommentdeleted'] = 'توضیح حذف شد';
$string['eventcommentsviewed'] = 'توضیح‌ها مشاهده شد';
$string['eventconfiglogcreated'] = 'فایل log برای تنظیمات ایجاد شد';
$string['eventcontentviewed'] = 'محتوا مشاهده شد';
$string['eventcoursebackupcreated'] = 'پشتیبان درس ایجاد شد';
$string['eventcoursecategorycreated'] = 'طبقه ایجاد شد';
$string['eventcoursecategorydeleted'] = 'طبقه حذف شد';
$string['eventcoursecategoryupdated'] = 'طبقه به روز شد';
$string['eventcoursecategoryviewed'] = 'طبقه مشاهده شد';
$string['eventcoursecontentdeleted'] = 'محتوای درس حذف شد';
$string['eventcoursecreated'] = 'درس ایجاد شد';
$string['eventcoursedeleted'] = 'درس حذف شد';
$string['eventcourseinformationviewed'] = 'توصیف درس مشاهده شد';
$string['eventcoursemodulecreated'] = 'ماژول درس ایجاد شد';
$string['eventcoursemoduledeleted'] = 'ماژول درس حذف شد';
$string['eventcoursemoduleinstancelistviewed'] = 'لیست نمونه ماژول درس مشاهده شد';
$string['eventcoursemoduleupdated'] = 'ماژول درس به روز شد';
$string['eventcoursemoduleviewed'] = 'ماژول درس مشاهده شد';
$string['eventcourseresetended'] = 'بازنشانی درس به پایان رسید';
$string['eventcourseresetstarted'] = 'بازنشانی درس شروع شد';
$string['eventcourserestored'] = 'درس بازیابی شد';
$string['eventcoursesectioncreated'] = 'قسمت درس ساخته شد';
$string['eventcoursesectiondeleted'] = 'قسمت درس حذف شد';
$string['eventcoursesectionupdated'] = 'قسمت درس به‌روز شد';
$string['eventcoursessearched'] = 'درس‌ها جستجو شدند';
$string['eventcourseupdated'] = 'درس به‌روز شد';
$string['eventcourseuserreportviewed'] = 'گزارش کاربری درس مشاهده شد';
$string['eventcourseviewed'] = 'درس مشاهده شد';
$string['eventdashboardreset'] = 'میزِ کار بازنشانی شد';
$string['eventdashboardsreset'] = 'میزهای کار بازنشانی شدند';
$string['eventdashboardviewed'] = 'میزِ کار مشاهده شد';
$string['eventemailfailed'] = 'خطایی در ارسال ایمیل به وجود آمد';
$string['eventname'] = 'نام رویداد';
$string['eventrecentactivityviewed'] = 'فعالیت اخیر مشاهده شد';
$string['eventsearchindexed'] = 'داده‌های جستجو index شدند';
$string['eventsearchresultsviewed'] = 'نتایج جستجو مشاهده شد';
$string['eventunknownlogged'] = 'رویداد نامشخص';
$string['eventusercreated'] = 'کاربر ایجاد شد';
$string['eventuserdeleted'] = 'کاربر حذف شد';
$string['eventuserinfocategorycreated'] = 'دسته مشخصه کاربری ایجاد شد';
$string['eventuserinfocategorydeleted'] = 'دسته مشخصه کاربری حذف شد';
$string['eventuserinfocategoryupdated'] = 'دسته مشخصه کاربری به‌روز شد';
$string['eventuserinfofieldcreated'] = 'مشخصه کاربری ایجاد شد';
$string['eventuserinfofielddeleted'] = 'مشخصه کاربری حذف شد';
$string['eventuserinfofieldupdated'] = 'مشخصه کاربری به‌روز شد';
$string['eventuserlistviewed'] = 'لیست کاربران مشاهده شد';
$string['eventuserloggedout'] = 'کاربر خارج شد';
$string['eventuserpasswordupdated'] = 'رمز عبور کاربر به روز شد';
$string['eventuserprofileviewed'] = 'پروفایل کاربر مشاهده شد';
$string['eventuserupdated'] = 'کاربر به روز شد';
$string['everybody'] = 'همه';
$string['executeat'] = 'اجرا در ساعت';
$string['existing'] = 'موجود';
$string['existingadmins'] = 'مدیران موجود';
$string['existingcourse'] = 'درس موجود';
$string['existingcourseadding'] = 'یکی از درس‌های موجود، با اضافه کردن اطلاعات به آن';
$string['existingcoursedeleting'] = 'یکی از درس‌های موجود، پس از حذف اولیهٔ آن';
$string['existingcreators'] = 'درس‌سازهای موجود';
$string['existingstudents'] = 'شاگردان ثبت‌نام شده';
$string['existingteachers'] = 'اساتید موجود';
$string['expand'] = 'باز کردن';
$string['expandall'] = 'باز شدن همه';
$string['expandcategory'] = 'بازکردن {$a}';
$string['explanation'] = 'توضیح';
$string['extendperiod'] = 'میزان تمدید';
$string['failedloginattempts'] = '{$a->attempts} ورود ناموفق از زمان آخرین ورود شما';
$string['favourites'] = 'ستاره‌دار';
$string['feedback'] = 'بازخورد';
$string['file'] = 'فایل';
$string['fileexists'] = 'فایل دیگری با نام «{$a}» موجود است';
$string['filemissing'] = '{$a} وجود ندارد';
$string['filereaderror'] = 'امکان خواندن فایل \'{$a}\' وجود ندارد. لطفا بررسی نمایید که این یک فایل است و نه یک پوشه.';
$string['files'] = 'فایل‌ها';
$string['filesanduploads'] = 'فایل‌ها و ارسال‌ها';
$string['filesfolders'] = 'فایل‌ها/فولدرها';
$string['fileuploadwithcontent'] = 'آپلود فایلها نباید شامل پارامتر محتوا باشد';
$string['filloutallfields'] = 'لطفأ همهٔ گزینه‌های این فرم را پر نمائید';
$string['filter'] = 'فیلتر';
$string['filteroption'] = '{$a->criteria}: {$a->value}';
$string['filters'] = 'فیلتر‌ها';
$string['findmorecourses'] = 'پیدا کردن درس‌های بیشتر...';
$string['first'] = 'اولین';
$string['firstaccess'] = 'اولین دسترسی';
$string['firstname'] = 'نام';
$string['firstnamephonetic'] = 'نام آوایی';
$string['firstsiteaccess'] = 'اولین دسترسی به سایت';
$string['firsttime'] = 'برای اولین بار به این صفحه آمده‌اید؟';
$string['folder'] = 'پوشه';
$string['folderclosed'] = 'پوشهٔ بسته';
$string['folderopened'] = 'پوشهٔ باز';
$string['followingoptional'] = 'موارد زیر اختیاری هستند';
$string['followingrequired'] = 'موارد زیر اجباری هستند';
$string['for'] = 'برای';
$string['force'] = 'تحمیل';
$string['forcelanguage'] = 'اجبار زبان';
$string['forceno'] = 'اجباری نباشد';
$string['forcepasswordchange'] = 'اجبار به تغییر رمز ورود';
$string['forcepasswordchangecheckfull'] = 'آیا کاملا مطمئنید که می‌خواهید {$a} را مجبور به تغییر دادن رمز ورود کنید؟';
$string['forcepasswordchange_help'] = 'اگر این مربع علامت خورده باشد، به هنگام ورود کاربر به سایت در دفعهٔ بعد از او خواسته می‌شود که رمز ورود خود را تغییر دهد';
$string['forcepasswordchangenot'] = 'نمی‌توان {$a} را به تغییر دادن رمز ورود مجبور کرد';
$string['forcepasswordchangenotice'] = 'برای پیش‌روی باید رمز ورود خود را تغییر دهید.';
$string['forcetheme'] = 'تحمیل پوسته';
$string['forgotaccount'] = 'رمز ورود را فراموش کرده‌اید؟';
$string['forgotten'] = 'نام کاربری و یا رمز ورود خود را فراموش کرده‌اید؟';
$string['forgottenduplicate'] = 'آدرس پست الکترونیک برای چند حساب کاربری مشترک است، در عوض لطفاً نام کاربری را وارد نمائید';
$string['forgotteninvalidurl'] = 'آدرس اینترنتی بازنشانی رمز ورود نامعتبر است';
$string['format'] = 'قالب';
$string['format_help'] = 'قالبِ درس نحوهٔ چیدمان صفحهٔ درس را مشخص می‌کند.

* قالبِ تک فعالیتی - برای نمایش دادن یک فعالیت یا منبع تکی (مانند یک آزمون یا بستهٔ SCORM) در صفحهٔ درس
* قالب اجتماعی - صفحهٔ درس به صورت یک تالار گفتگو می‌شود
* قالب موضوعی - صفحهٔ درس متشکل از قسمت‌های موضوع دار است
* قالب هفتگی - صفحهٔ درس به صورت هفتگی با شروع از تاریخ شروع درس قسمت‌بندی شده است';
$string['formathtml'] = 'قالب HTML';
$string['formatmarkdown'] = 'قالب Markdown';
$string['formatplain'] = 'قالب متنی ساده';
$string['formattext'] = 'قالب‌بندی خودکار مودل';
$string['formattexttype'] = 'قالب‌بندی';
$string['forumpreferences'] = 'ترجیحات تالار گفتگو';
$string['framesetinfo'] = 'محتویات این قاب:';
$string['from'] = 'از';
$string['frontpagecategorycombo'] = 'لیست ترکیبی طبقه‌ها و درس‌ها';
$string['frontpagecategorynames'] = 'لیست طبقه‌ها';
$string['frontpagecourselist'] = 'لیست درس‌ها';
$string['frontpagecoursesearch'] = 'پنجرهٔ جستجوی درس';
$string['frontpagedescription'] = 'توصیف صفحهٔ اول';
$string['frontpagedescriptionhelp'] = 'این توصیف می‌تواند با استفاده از بلوک «توصیف درس/سایت» در صفحهٔ اول سایت نمایش داده شود.';
$string['frontpageenrolledcourselist'] = 'درس‌های ثبت نام شده';
$string['frontpageformat'] = 'شکل صفحه اول';
$string['frontpageformatloggedin'] = 'قالب صفحهٔ اول هنگامی که کاربر وارد شده است';
$string['frontpagenews'] = 'اعلانات';
$string['frontpagesettings'] = 'تنظیمات صفحهٔ اول';
$string['fulllistofcourses'] = 'همهٔ درس‌ها';
$string['fullname'] = 'نام کامل';
$string['fullnamecourse'] = 'نام کامل درس';
$string['fullnamecourse_help'] = 'نام کامل درس در بالای تمام صفحه‌های درس و همچنین در لیست درس‌ها نمایش داده می‌شود.';
$string['fullnamedisplay'] = '{$a->firstname} {$a->lastname}';
$string['fullnameuser'] = 'نام کامل';
$string['fullprofile'] = 'مشخصات فردی کامل';
$string['fullsitename'] = 'نام کامل سایت';
$string['functiondisabled'] = 'در حال حاضر این کاربرد غیرفعال است';
$string['general'] = 'عمومی';
$string['geolocation'] = 'عرض جغرافیایی - طول جغرافیایی';
$string['gettheselogs'] = 'تهیهٔ این log ها';
$string['go'] = 'شروع';
$string['gpl'] = 'حق کپی ۱۹۹۹ به بعد  مارتین دوگیاماس  (http://moodle.com)

این برنامه یک نرم‌افزار آزاد است؛ می‌توانید آن را بر اساس شرایط
اجازه‌نامهٔ عمومی همگانی گنو که توسط بنیاد نرم‌افزار آزاد منتشر
شده‌است، مجدداً توزیع کنید و/یا تغییر دهید؛ هر کدام از نسخه‌های ۲،
یا (به دلخواه شما) نسخه‌های جدیدتر.

این برنامه با امید به مفید بودن، امّا بدون هیچ‌گونه ضمانتی،
حتی بدون تعهد ضمنی مبنی بر قابل داد و ستد بودن یا مناسب بودن
برای یک منظور خاص، توزیع شده است.

برای مشاهدهٔ جزئیات کامل صفحهٔ اطلاعات اجازه‌نامهٔ مودل را ببینید:
http://docs.moodle.org/dev/License';
$string['gpl3'] = 'حق کپی ۱۹۹۹ به بعد  مارتین دوگیاماس  (http://moodle.com)

این برنامه یک نرم‌افزار آزاد است؛ می‌توانید آن را بر اساس شرایط
اجازه‌نامهٔ عمومی همگانی گنو که توسط بنیاد نرم‌افزار آزاد منتشر
شده‌است، مجدداً توزیع کنید و/یا تغییر دهید؛ هر کدام از نسخه‌های ۳،
یا (به دلخواه شما) نسخه‌های جدیدتر.

این برنامه با امید به مفید بودن، امّا بدون هیچ‌گونه ضمانتی،
حتی بدون تعهد ضمنی مبنی بر قابل داد و ستد بودن یا مناسب بودن
برای یک منظور خاص، توزیع شده است.

برای مشاهدهٔ جزئیات کامل صفحهٔ اطلاعات اجازه‌نامهٔ مودل را ببینید:
http://docs.moodle.org/dev/License';
$string['gpllicense'] = 'اجازه‌نامهٔ GPL';
$string['grade'] = 'نمره';
$string['grades'] = 'نمره‌ها';
$string['gravatarenabled'] = '<a href="http://www.gravatar.com/">گرآواتار</a> برای این سایت فعال شده است. اگر شما در صفحهٔ مشخصات فردی برای خود عکسی ارسال نکنید، مودل سعی خواهد کرد که از طریق گرآواتار شما تصویرتان را نمایش دهد.';
$string['group'] = 'گروه';
$string['groupadd'] = 'ایجاد گروه جدید';
$string['groupaddusers'] = 'اضافه شدن انتخاب‌شدگان به گروه';
$string['groupfor'] = 'برای گروه';
$string['groupinfo'] = 'اطلاعات در بارهٔ گروه انتخاب شده';
$string['groupinfoedit'] = 'ویرایش تنظیمات گروه';
$string['groupinfomembers'] = 'اطلاعات در بارهٔ اعضای انتخاب شده';
$string['groupinfopeople'] = 'اطلاعات در بارهٔ افراد انتخاب شده';
$string['groupmembers'] = 'اعضای گروه';
$string['groupmemberssee'] = 'مشاهدهٔ اعضای گروه';
$string['groupmembersselected'] = 'اعضای گروه انتخاب شده';
$string['groupmode'] = 'نحوهٔ گروه‌بندی';
$string['groupmodeforce'] = 'تحمیل نحوهٔ گروه‌بندی';
$string['groupmy'] = 'گروه من';
$string['groupnonmembers'] = 'کسانی که در گروهی نیستند';
$string['groupnotamember'] = 'ببخشید، شما عضوی از آن گروه نیستید';
$string['grouprandomassign'] = 'گروه‌بندی همه به صورت شانسی';
$string['groupremove'] = 'حذف گروه انتخاب شده';
$string['groupremovemembers'] = 'حذف اعضای انتخاب شده';
$string['groups'] = 'گروه‌ها';
$string['groupsnone'] = 'بدون گروه‌بندی';
$string['groupsseparate'] = 'گروه‌های جداگانه';
$string['groupsvisible'] = 'گروه‌های مرئی';
$string['guest'] = 'مهمان';
$string['guestdescription'] = 'مهمان‌ها دارای کمترین امتیازات هستند و معمولاً نمی‌توانند در هیچ قسمتی متنی ارسال کنند.';
$string['guestskey'] = 'به مهمان‌هایی که کلید را دارند اجازهٔ ورود داده شود';
$string['guestsno'] = 'به مهمان‌ها اجازهٔ ورود داده نشود';
$string['guestsnotallowed'] = 'متأسفانه «{$a}» به مهمانان اجازهٔ ورود نمی‌دهد.';
$string['guestsyes'] = 'به مهمان‌ها بدون سؤال در مورد کلید اجازهٔ ورود داده شود';
$string['guestuser'] = 'کاربر مهمان';
$string['guestuserinfo'] = 'این کاربر یک کاربر ویژه است که اجازهٔ دستیابی فقط خواندنی به بعضی از درس‌ها را فراهم می‌کند.';
$string['help'] = 'راهنمایی';
$string['helpprefix2'] = 'راهنمایی دربارهٔ «{$a}»';
$string['helpwiththis'] = 'راهنمایی در این مورد';
$string['hiddenassign'] = 'انتصاب پنهانی';
$string['hiddenfromstudents'] = 'پنهان از شاگردان';
$string['hiddenoncoursepage'] = 'قابل دسترسی است ولی در صفحهٔ درس نمایش داده نمی‌شود';
$string['hiddensections'] = 'قسمت‌های پنهان';
$string['hiddensectionscollapsed'] = 'قسمت‌های پنهان به صورت سربسته نمایش داده شوند';
$string['hiddensections_help'] = 'این گزینه تعیین می‌کند که قسمت‌های پنهان شده به صورت سربسته به شاگردان نمایش داده شوند (احتمالا برای نشان دادن تعطیلات در درسی با قالب هفتگی) یا اینکه اصلاً نشان داده نشوند.';
$string['hiddensectionsinvisible'] = 'قسمت‌های پنهان کاملاً نامرئی باشند';
$string['hiddenwithbrackets'] = '(مخفی)';
$string['hide'] = 'پنهان کردن';
$string['hideadvancedsettings'] = 'پنهان کردن تنظیمات پیشرفته';
$string['hidechartdata'] = 'پنهان کردن داده‌های نمودار';
$string['hidefromstudents'] = 'پنهان بودن از شاگردان';
$string['hideoncoursepage'] = 'قابل دسترسی باشد ولی در صفحهٔ درس نمایش داده نشود';
$string['hidepicture'] = 'پنهان کردن تصویر';
$string['hidepopoverwindow'] = 'پنهان کردن پنجرهٔ ظاهرشونده';
$string['hidesection'] = 'پنهان کردن قسمت {$a}';
$string['hidesettings'] = 'پنهان کردن تنظیمات';
$string['hideshowblocks'] = 'نمایش‌دادن یا مخفی‌کردن بلوک‌ها';
$string['highlight'] = 'هایلایت';
$string['highlightoff'] = 'حذف هایلایت';
$string['hits'] = 'مراجعه‌ها';
$string['hitsoncourse'] = 'دفعات مراجعهٔ {$a->username} به {$a->coursename}';
$string['hitsoncoursetoday'] = 'مراجعه‌های امروز {$a->username} به {$a->coursename}';
$string['home'] = 'خانه';
$string['hour'] = 'ساعت';
$string['hours'] = 'ساعت';
$string['howtomakethemes'] = 'نحوهٔ ایجاد پوسته‌های جدید';
$string['htmleditor'] = 'از ویرایشگر HTML استفاده شود';
$string['htmleditoravailable'] = 'ویرایشگر HTML فعال است';
$string['htmleditordisabled'] = 'شما ویرایشگر HTML را در مشخصات فردی خود غیرفعال کرده‌اید';
$string['htmleditordisabledadmin'] = 'مدیر ویرتیشگر HTML را در این سایت غیرفعال کرده است';
$string['htmleditordisabledbrowser'] = 'ویرایشگر HTML غیرفعال شده است زیرا مرورگر شما با آن سازگار نیست';
$string['htmlfilesonly'] = 'فقط فایل‌های HTML';
$string['htmlformat'] = 'قالب آراستهٔ HTML';
$string['icon'] = 'تصویر';
$string['icqnumber'] = 'شمارهٔ ICQ';
$string['idnumber'] = 'کد شناسائی';
$string['idnumbercourse'] = 'کد درس';
$string['idnumbercoursecategory'] = 'شمارهٔ شناسهٔ طبقه';
$string['idnumbercoursecategory_help'] = 'شمارهٔ شناسهٔ یک طبقهٔ درسی در هیچ قسمتی از سایت نمایش داده نمی‌شود و تنها در هنگام تطابق طبقهٔ درسی با سیستم‌های دیگر مورد استفاده قرار می‌گیرد. اگر طبقهٔ درسی یک کد رسمی دارد می‌توان آن را در این قسمت وارد کرد. در غیر این صورت می‌توان این قسمت را خالی گذاشت.';
$string['idnumbercourse_help'] = 'کد یک درس در هیچ قسمتی از سایت نمایش داده نمی‌شود و تنها در هنگام تطابق درس با سیستم‌های دیگر مورد استفاده قرار می‌گیرد. اگر درس یک کد رسمی دارد می‌توان آن را وارد کرد. در غیر این صورت می‌توان این قسمت را خالی گذاشت.';
$string['idnumbergroup'] = 'شمارهٔ شناسهٔ گروه';
$string['idnumbergroup_help'] = 'شمارهٔ شناسهٔ یک گروه در هیچ قسمتی از سایت نمایش داده نمی‌شود و تنها در هنگام تطابق گروه با سیستم‌های دیگر مورد استفاده قرار می‌گیرد. اگر گروه یک کد رسمی دارد می‌توان آن را در این قسمت وارد کرد. در غیر این صورت می‌توان این قسمت را خالی گذاشت.';
$string['idnumbergrouping'] = 'شمارهٔ شناسهٔ ابرگروه';
$string['idnumbergrouping_help'] = 'شمارهٔ شناسهٔ یک ابرگروه در هیچ قسمتی از سایت نمایش داده نمی‌شود و تنها در هنگام تطابق ابرگروه با سیستم‌های دیگر مورد استفاده قرار می‌گیرد. اگر ابرگروه یک کد رسمی دارد می‌توان آن را در این قسمت وارد کرد. در غیر این صورت می‌توان این قسمت را خالی گذاشت.';
$string['idnumbermod'] = 'شمارهٔ شناسائی';
$string['idnumbermod_help'] = 'اختصاص یک شمارهٔ شناسائی، راهی برای تشخیص فعالیت به منظور محاسبهٔ نمره است. در صورتی که فعالیت در محاسبهٔ نمره‌ها دخیل نباشد، می‌توان این گزینه را خالی گذاشت.

شمارهٔ شناسائی را در دفتر نمره هم می‌توان تعیین کرد. هر چند که فقط در صفحهٔ تنظیمات فعالیت قابل ویرایش است.';
$string['idnumbertaken'] = 'این کد شناسائی قبلاً استفاده شده است';
$string['imagealt'] = 'توصیف عکس';
$string['import'] = 'وارد کردن';
$string['importdata'] = 'ورود داده‌های درس';
$string['importdataexported'] = 'صدور اطلاعات از درس «مبدأ» با موفقیت انجام شد.<br />در ادامه، اطلاعات در درس «مقصد» وارد می‌شود.';
$string['importdatafinished'] = 'ورود اطلاعات تمام شد! با ورود به درس خود ادامه دهید';
$string['importdatafrom'] = 'درسی برای وارد کردن اطلاعات از آن انتخاب کنید:';
$string['inactive'] = 'غیرفعال';
$string['include'] = 'شامل';
$string['includeallusers'] = 'شامل همهٔ کاربران';
$string['includecoursefiles'] = 'شامل فایل‌های درس';
$string['includecourseusers'] = 'شامل کاربران درس';
$string['included'] = 'شامل شده است';
$string['includelogentries'] = 'شامل اقلام log';
$string['includemodules'] = 'شامل ماژول‌ها شوند';
$string['includemoduleuserdata'] = 'شامل داده‌های کاربران در ماژول‌ها شوند';
$string['includeneededusers'] = 'شامل کاربران لازم';
$string['includenoneusers'] = 'شامل هیچ کاربری نشود';
$string['includeroleassignments'] = 'شامل نقش‌های افراد';
$string['includesitefiles'] = 'شامل فایل‌های سایت که در این درس استفاده شده‌اند';
$string['includeuserfiles'] = 'شامل فایل‌های کاربران';
$string['increasesections'] = 'زیاد کردن تعداد قسمت‌ها';
$string['indicator:completeduserprofile'] = 'مشخصه کاربری کامل شد';
$string['indicator:nostudent'] = 'هیچ شاگردی وجود ندارد';
$string['indicator:noteacher'] = 'هیچ استادی وجود ندارد';
$string['indicator:readactions'] = 'تعداد دفعات مطالعه';
$string['indicator:userforumstracking'] = 'کاربر تالارهای گفتگو را دنبال می‌کند';
$string['info'] = 'توضیحات';
$string['institution'] = 'مؤسسه';
$string['instudentview'] = 'از دید شاگرد';
$string['interests'] = 'علایق';
$string['interestslist'] = 'لیست علایق';
$string['interestslist_help'] = 'علایق خود را یکی یکی وارد کنید و کلید Enter را بزنید. علایق شما در صفحهٔ مشخصات فردی‌تان نمایش داده خواهد شد.';
$string['invalidemail'] = 'آدرس پست الکترونیک نامعتبر';
$string['invalidlogin'] = 'ورود نامعتبر، لطفاً دوباره سعی کنید.';
$string['invalidusername'] = 'نام کاربری تنها می‌تواند شامل حروف کوچک الفبای انگلیسی یا اعداد، خط تیرهٔ زیرین (_)، خط تیره (-)، نقطه (.) یا علامت @ باشد.';
$string['invalidusernameupload'] = 'نام کاربری غیر مجاز';
$string['ip_address'] = 'آدرس IP';
$string['jump'] = 'پرش';
$string['jumpto'] = 'رفتن به...';
$string['keep'] = 'نگهداری';
$string['keepsearching'] = 'به جستجو ادامه دهید';
$string['langltr'] = 'جهت زبان از چپ به راست';
$string['langrtl'] = 'جهت زبان از راست به چپ';
$string['language'] = 'زبان';
$string['languagegood'] = 'بستهٔ زبانی به‌روز است! :-)';
$string['last'] = 'آخرین';
$string['lastaccess'] = 'زمان آخرین دسترسی';
$string['lastcourseaccess'] = 'آخرین دسترسی به درس';
$string['lastedited'] = 'آخرین ویرایش';
$string['lastip'] = 'آخرین آدرس آی پی';
$string['lastlogin'] = 'زمان آخرین ورود';
$string['lastmodified'] = 'آخرین تغییر';
$string['lastname'] = 'نام خانوادگی';
$string['lastnamephonetic'] = 'نام خانوادگی آوایی';
$string['lastsiteaccess'] = 'آخرین دسترسی به سایت';
$string['lastyear'] = 'سال گذشته';
$string['latestlanguagepack'] = 'بررسی جدیدترین بستهٔ زبانی در moodle.org';
$string['layouttable'] = 'جدول صفحه آرائی';
$string['leavetokeep'] = 'برای حفظ رمز ورود فعلی، این قسمت را خالی بگذارید';
$string['legacylogginginuse'] = 'در حال حاضر تنظیم ذخیرهٔ لاگ‌ها روی محل ذخیرهٔ موروثی در این سایت فعال است. نگهداری لاگ‌ها در این محل منسوخ شده است. لطفا لاگ‌ها را روی یک محل ذخیرهٔ پشتیبانی‌شده مانند «استاندارد» یا «خارجی» نگهداری کنید.';
$string['legacythemeinuse'] = 'این سایت در «حالت سازگار» به شما نمایش داده می‌شود زیرا مرورگر شما خیلی قدیمی است.';
$string['license'] = 'اجازه‌نامه';
$string['licenses'] = 'اجازه‌نامه‌ها';
$string['liketologin'] = 'آیا مایلید که هم‌اکنون با یک جساب کاربری کامل وارد شوید؟';
$string['list'] = 'لیست محتوا';
$string['listfiles'] = 'لیست فایل‌های داخل {$a}';
$string['listofallpeople'] = 'لیست همهٔ افراد';
$string['listofcourses'] = 'لیست درس‌ها';
$string['loading'] = 'در حال بارگیری';
$string['loadinghelp'] = 'در حال بارگیری';
$string['local'] = 'محلی';
$string['localplugins'] = 'پلاگین‌های محلی';
$string['localpluginsmanage'] = 'مدیریت پلاگین‌های محلی';
$string['location'] = 'مکان';
$string['locktimeout'] = 'زمان اجرای عملیات مادام انتظار برای یک قفل به پایان رسید';
$string['log_excel_date_format'] = 'yyyy mmmm d h:mm';
$string['loggedinas'] = 'شما در قالب {$a} وارد سایت شده‌اید';
$string['loggedinasguest'] = 'در حال حاضر از دسترسی مهمان استفاده می‌کنید';
$string['loggedinnot'] = 'هنوز وارد سایت نشده‌اید.';
$string['login'] = 'ورود به سایت';
$string['loginactivity'] = 'فعالیت ورود';
$string['loginalready'] = 'در حال حاضر شما وارد سایت هستید';
$string['loginas'] = 'ورود در قالب این کاربر';
$string['loginaspasswordexplain'] = '<p>برای استفاده از این ویژگی باید «رمز ورود به‌عنوان» ویژه را وارد نمائید.<br />اگر آن را نمی‌دانید، از مدیر کارگزار خود بپرسید.</p>';
$string['login_failure_logs'] = 'Log های مربوط به عدم موفقیت در ورود';
$string['loginguest'] = 'ورود به‌عنوان مهمان';
$string['loginsite'] = 'ورود به سایت';
$string['loginsteps'] = 'برای داشتن دسترسی کامل به این سایت، پیش از هر چیز باید یک حساب کاربری بسازید.';
$string['loginstepsnone'] = '<p>سلام!</p>
<p>جهت دستیابی کامل به درس‌ها باید یک حساب کاربری برای خود ایجاد نمائید.</p>
<p>تنها کاری که باید انجام دهید در نظر گرفتن یک نام کاربری و رمز ورود و استفاده از آن‌ها در فرم این صفحه است!</p>
<p>اگر شخص دیگری قبلاً نام کاربری دلخواه شما را انتخاب کرده باشد، مجبورید که نام کاربری دیگری را امتحان کنید.</p>';
$string['loginto'] = 'ورود به {$a}';
$string['logout'] = 'خروج از سایت';
$string['logoutconfirm'] = 'آیا واقعاً می‌خواهید خارج شوید؟';
$string['logs'] = 'Log ها';
$string['logtoomanycourses'] = '[ <a href="{$a->url}">بیشتر</a> ]';
$string['logtoomanyusers'] = '[ <a href="{$a->url}">بیشتر</a> ]';
$string['lookback'] = 'نگاه به گذشته';
$string['mailadmins'] = 'آگاه سازی مدیرها';
$string['mailstudents'] = 'آگاه سازی شاگردها';
$string['mailteachers'] = 'آگاه سازی اساتید';
$string['maincoursepage'] = 'صفحه اصلی درس';
$string['makeafolder'] = 'ساخت پوشه';
$string['makeavailable'] = 'قابل دسترسی شود';
$string['makeeditable'] = 'در صورتی که «{$a}» را قابل ویرایش توسط برنامهٔ کارگزار وب (مثل Apache) نمائید، مستقیماً از طریق این صفحه قادر به ویرایش این فایل خواهید بود';
$string['makethismyhome'] = 'در نظر گرفتن این صفحه به عنوان صفحهٔ خانگی من';
$string['makeunavailable'] = 'غیر قابل دسترسی شود';
$string['manageblocks'] = 'بلوک‌ها';
$string['managecategorythis'] = 'مدیریت این طبقه';
$string['managecourses'] = 'مدیریت درس‌ها';
$string['managedatabase'] = 'پایگاه داده';
$string['managedataformats'] = 'مدیریت قالب‌های داده';
$string['manageeditorfiles'] = 'مدیریت فایل‌های استفاده شده توسط ویرایشگر';
$string['managefilters'] = 'فیلترها';
$string['managemodules'] = 'ماژول‌ها';
$string['manageroles'] = 'نقش‌ها و مجوزها';
$string['markallread'] = 'علامت‌زدن همه به‌عنوان خوانده‌شده';
$string['markedthistopic'] = 'این موضوع به‌عنوان موضوع فعلی به‌صورت برجسته نمایش داده شده است';
$string['markthistopic'] = 'برجسته کردن این موضوع به‌عنوان موضوع فعلی';
$string['matchingsearchandrole'] = 'تطبیق «{$a->search}» و {$a->role}';
$string['maxareabytesreached'] = 'فایل (یا جمع حجم چندین فایل) بزرگتز از فضای باقیمانده در این قسمت است';
$string['maxfilesize'] = 'حداکثر اندازه فایل‌های جدید: {$a}';
$string['maxfilesreached'] = 'شما مجاز به افزودن حداکثر {$a} فایل به این آیتم هستید';
$string['maximumchars'] = 'حداکثر {$a} حرف';
$string['maximumgrade'] = 'سقف نمره';
$string['maximumgradex'] = 'نمرهٔ حداکثر: {$a}';
$string['maximumshort'] = 'حداکثر';
$string['maximumupload'] = 'حداکثر اندازهٔ فایل ارسالی';
$string['maximumupload_help'] = 'این گزینه بیشترین حجم مجاز برای فایل‌هایی که قرار است در درس قرار گیرند را تعیین می‌کند. مقادیر مجاز برای انتخاب توسط تنظیمات کلی سایت که توسط یک مدیر قابل انجام است محدود شده‌اند. هر کدام از فعالیت‌های درون درس نیز در تنظیمات خود گزینه‌ای برای تعیین دقیق‌تر حداکثر حجم مجاز یک فایل دارند.';
$string['maxnumberweeks'] = 'حداکثر تعداد قسمت‌ها';
$string['maxnumberweeks_desc'] = 'این مقدار تعیین می‌کند که بیشترین مقدار موجود در گزینه‌های مربوط به تنظیم «تعداد قسمت‌ها» (به‌طور مثال تعداد هفته‌ها/موضوع‌ها) برای درس‌ها چقدر باشد (در برخی از قالب‌های درسی کاربرد دارد).';
$string['maxnumcoursesincombo'] = 'مشاهدهٔ لیست <a href="{$a->link}">{$a->numberofcourses} درس';
$string['maxsize'] = 'حداکثر اندازه: {$a}';
$string['maxsizeandareasize'] = 'حداکثر اندزه برای فایل‌های جدید: {$a->size}، محدودیت کلی: {$a->areasize}';
$string['maxsizeandattachments'] = 'حداکثر اندازه برای فایل‌های جدید: {$a->size}، حداکثر تعداد فایل‌های پیوست: {$a->attachments}';
$string['maxsizeandattachmentsandareasize'] = 'حداکثر اندازه برای فایل‌های جدید: {$a->size}، حداکثر تعداد فایل‌های پیوست: {$a->attachments}، محدودیت کلی: {$a->areasize}';
$string['memberincourse'] = 'افراد موجود در درس';
$string['messagebody'] = 'متن پیغام';
$string['messagedselectedcountusersfailed'] = 'مشکلی رخ داد و هنوز {$a} پیام ارسال نشدند.';
$string['messagedselecteduserfailed'] = 'پیام به کاربر {$a->fullname} ارسال نشد.';
$string['messagedselectedusers'] = 'به کاربران انتخاب شده پیغام ارسال شده و لیست دریافت کنندگان بازنشانی شده است.';
$string['messagedselectedusersfailed'] = 'هنگام ارسال پیغام به کاربران انتخاب شده اشکالی رخ داد. برخی ممکن است نامهٔ الکترونیکی را دریافت کرده باشند.';
$string['messageprovider:availableupdate'] = 'اطلاعیه‌های مربوط به به‌روزرسانی‌های موجود';
$string['messageprovider:backup'] = 'خبرهای مربوط به پشتیبان‌گیری';
$string['messageprovider:badgecreatornotice'] = 'اطلاعیه‌های سازندهٔ مدال';
$string['messageprovider:badgerecipientnotice'] = 'اطلاعیه‌های دریافت‌کنندهٔ مدال';
$string['messageprovider:competencyplancomment'] = 'نظر ارسال‌شده روی یک برنامهٔ یادگیری';
$string['messageprovider:competencyusercompcomment'] = 'نظر ارسال‌شده روی یک شایستگی';
$string['messageprovider:courserequestapproved'] = 'خبر تایید درخواست ایجاد درس';
$string['messageprovider:courserequested'] = 'خبر درخواست ایجاد درس';
$string['messageprovider:courserequestrejected'] = 'خبر رد درخواست ایجاد درس';
$string['messageprovider:errors'] = 'خطاهای مهم در سایت';
$string['messageprovider:errors_help'] = 'این‌ها خطاهای مهمی هستند که یک مدیر باید در مورد آن‌ها بداند.';
$string['messageprovider:instantmessage'] = 'پیغام‌های شخصی بین کاربران';
$string['messageprovider:instantmessage_help'] = 'این قسمت تعیین می‌کند که با پیغام‌هایی که از طرف کاربران این سایت به طور مستقیم به شما فرستاده شده‌اند باید چه شود.';
$string['messageprovider:notices'] = 'اخطارهای مربوط به مشکلات جزئی';
$string['messageprovider:notices_help'] = 'این‌ها اخطارهایی هستند که یک مدیر ممکن است بخواهد آن‌ها را ببیند.';
$string['messageselect'] = 'انتخاب این کاربر به‌عنوان یکی از دریافت‌کنندگان نامهٔ الکترونیکی';
$string['messageselectadd'] = 'ارسال پیغام';
$string['middlename'] = 'نام میانی';
$string['migratinggrades'] = 'انتقال نمره‌ها';
$string['min'] = 'دقیقه';
$string['mins'] = 'دقیقه';
$string['minute'] = 'دقیقه';
$string['minutes'] = 'دقیقه';
$string['miscellaneous'] = 'متفرقه';
$string['missingcategory'] = 'باید یک طبقه انتخاب کنید';
$string['missingdescription'] = 'قسمت توصیف خالی است';
$string['missingemail'] = 'آدرس پست الکترونیک را وارد کنید';
$string['missingfirstname'] = 'نام را وارد کنید';
$string['missingfromdisk'] = 'از روی دیسک پاک شده است';
$string['missingfullname'] = 'نام کامل وارد نشده است';
$string['missinglastname'] = 'نام خانوادگی را وارد کنید';
$string['missingname'] = 'نام وارد نشده است';
$string['missingnewpassword'] = 'رمز ورود جدید وارد نشده است';
$string['missingpassword'] = 'رمز ورود را وارد کنید';
$string['missingrecaptchachallengefield'] = 'مقدار reCAPTCHA وارد نشده است';
$string['missingreqreason'] = 'دلیلی ارائه نشده است';
$string['missingshortname'] = 'نام کوتاه وارد نشده است';
$string['missingshortsitename'] = 'نام کوتاه سایت وارد نشده است';
$string['missingsitedescription'] = 'توصیف سایت وارد نشده است';
$string['missingsitename'] = 'نام سایت وارد نشده است';
$string['missingstrings'] = 'بررسی بمنظور کلمات و عبارات ترجمه نشده';
$string['missingstudent'] = 'باید چیزی را انتخاب کنید';
$string['missingsummary'] = 'خلاصه وارد نشده است';
$string['missingteacher'] = 'باید چیزی را انتخاب کنید';
$string['missingurl'] = 'آدرس اینترنتی را وارد نکرده‌اید';
$string['missingusername'] = 'نام کاربری را وارد کنید';
$string['moddoesnotsupporttype'] = 'ماژول {$a->modname} از بارگذاری فایل از نوع {$a->type} پشتیبانی نمی‌کند';
$string['modhide'] = 'پنهان کردن';
$string['modified'] = 'آخرین تغییر';
$string['modshow'] = 'نمایش';
$string['moduleintro'] = 'توصیف';
$string['modulesetup'] = 'برپاسازی جدول‌های ماژول';
$string['modulesuccess'] = 'جدول‌های {$a} به درستی برپا شدند';
$string['modulesused'] = 'ماژول های استفاده شده';
$string['modvisible'] = 'در دسترس بودن';
$string['modvisible_help'] = 'اگر «در دسترس بودن» روی «نمایش در صفحهٔ درس» قرار داده شود، فعالیت یا منبع در دسترس شاگردان خواهد بود (البته با نظر گرفتن محدودیت‌هایی دسترسی‌ای که ممکن است تعیین شده باشد).<br><br>
اگر «در دسترس بودن» روی «پنهان بودن از شاگردان» قرار داده شود، فعالیت یا منبع تنها توسط کاربرانی که مجوز مشاهدهٔ فعالیت‌های پنهان را دارند (به‌طور پیش‌فرض کاربرانی که نقش استاد یا استاد بدون حق ویرایش را دارند) قابل مشاهده خواهد بود.';
$string['modvisiblehiddensection'] = 'در دسترس بودن';
$string['modvisiblehiddensection_help'] = 'اگر «در دسترس بودن» روی «پنهان بودن از شاگردان» قرار داده شود، فعالیت یا منبع تنها توسط کاربرانی که مجوز مشاهدهٔ فعالیت‌های پنهان را دارند (به‌طور پیش‌فرض کاربرانی که نقش استاد یا استاد بدون حق ویرایش را دارند) قابل مشاهده خواهد بود.<br><br>
اگر درس شامل فعالیت‌ها یا منابع بسیاری باشد، می‌توان صفحهٔ درس را با قرار دادن تنظیم «در دسترس بودن» روی «قابل دسترسی باشد ولی در صفحهٔ درس نمایش داده نشود» ساده‌تر کرد. در این حالت، باید از جای دیگری (مثلا از یک منبع از نوع صفحه) یک پیوند به فعالیت یا منبع فراهم شود. این فعالیت همچنان در دفتر نمره و سایر گزارش‌ها شامل خواهد شد.';
$string['modvisiblewithstealth'] = 'در دسترس بودن';
$string['modvisiblewithstealth_help'] = 'اگر «در دسترس بودن» روی «نمایش در صفحهٔ درس» قرار داده شود، فعالیت یا منبع در دسترس شاگردان خواهد بود (البته با نظر گرفتن محدودیت‌هایی دسترسی‌ای که ممکن است تعیین شده باشد).<br><br>
اگر «در دسترس بودن» روی «پنهان بودن از شاگردان» قرار داده شود، فعالیت یا منبع تنها توسط کاربرانی که مجوز مشاهدهٔ فعالیت‌های پنهان را دارند (به‌طور پیش‌فرض کاربرانی که نقش استاد یا استاد بدون حق ویرایش را دارند) قابل مشاهده خواهد بود.<br><br>
اگر درس شامل فعالیت‌ها یا منابع بسیاری باشد، می‌توان صفحهٔ درس را با قرار دادن تنظیم «در دسترس بودن» روی «قابل دسترسی باشد ولی در صفحهٔ درس نمایش داده نشود» ساده‌تر کرد. در این حالت، باید از جای دیگری (مثلا از یک منبع از نوع صفحه) یک پیوند به فعالیت یا منبع فراهم شود. این فعالیت همچنان در دفتر نمره و سایر گزارش‌ها شامل خواهد شد.';
$string['month'] = 'ماه';
$string['months'] = 'ماه‌ها';
$string['moodledocs'] = 'اسناد مودل';
$string['moodledocslink'] = 'اسناد مودل برای این صفحه';
$string['moodlelogo'] = 'لوگوی مودل';
$string['moodlerelease'] = 'نسخهٔ انتشار مودل';
$string['moodleversion'] = 'نسخهٔ مودل';
$string['more'] = 'بیشتر';
$string['morehelp'] = 'راهنمایی بیشتر';
$string['moreinfo'] = 'اطلاعات بیشتر';
$string['moreinformation'] = 'اطلاعات بیشتر در بارهٔ این خطا';
$string['morenavigationlinks'] = 'بیشتر...';
$string['moreprofileinfoneeded'] = 'لطفاً بیشتر در مورد خودتان برای ما بگویید';
$string['mostrecently'] = 'آخرین بار در';
$string['move'] = 'انتقال';
$string['movecategoriessuccess'] = '{$a->count} طبقه با موفقیت به طبقهٔ «{$a->to}» انتقال داده شدند';
$string['movecategoriestotopsuccess'] = '{$a->count} طبقه با موفقیت به بالاترین رده انتقال داده شدند';
$string['movecategorycontentto'] = 'انتقال به';
$string['movecategorysuccess'] = 'طبقهٔ «{$a->moved}» با موفقیت به داخل طبقهٔ «{$a->to}» منتقل شد';
$string['movecategoryto'] = 'انتقال طبقه به:';
$string['movecategorytotopsuccess'] = 'طبقهٔ «{$a->moved}» با موفقیت به بالاترین رده منتقل شد';
$string['movecontent'] = '{$a} انتقال';
$string['movecontentafter'] = 'بعد از "{$a}"';
$string['movecontentstoanothercategory'] = 'انتقال محتویات به یک طبقهٔ دیگر';
$string['movecoursemodule'] = 'انتقال منبع';
$string['movecoursesection'] = 'انتقال قسمت';
$string['movecourseto'] = 'انتقال درس به:';
$string['movedown'] = 'انتقال به پائین';
$string['movefilestohere'] = 'انتقال فایل‌ها به این محل';
$string['movefull'] = 'انتقال {$a} به این محل';
$string['movehere'] = 'انتقال به این محل';
$string['moveleft'] = 'انتقال به راست';
$string['moveright'] = 'انتقال به چپ';
$string['movesection'] = 'انتقال قسمت {$a}';
$string['moveselectedcategoriesto'] = 'جابجایی دسته بندی های انتخاب شده به';
$string['moveselectedcoursesto'] = 'انتقال درس‌های انتخاب شده به...';
$string['movetoanotherfolder'] = 'به پوشهٔ دیگری منتقل کن';
$string['moveup'] = 'انتقال به بالا';
$string['msnid'] = 'شناسهٔ MSN';
$string['mustchangepassword'] = 'رمز ورود جدید باید نسبت به رمز فعلی متفاوت باشد';
$string['mustconfirm'] = 'شما نیاز به تایید حساب کاربری خود دارید';
$string['mycourses'] = 'درس‌های من';
$string['myfiles'] = 'فایل‌های خصوصی من';
$string['myfilesmanage'] = 'مدیریت فایل‌های خصوصی من';
$string['myhome'] = 'میز کار';
$string['mymoodledashboard'] = 'میز کار مودل من';
$string['myprofile'] = 'مشخصات فردی من';
$string['name'] = 'نام';
$string['namedfiletoolarge'] = 'فایل «{$a->filename}» بیش از حد مجاز بزرگ است و نمی‌تواند بارگذاری شود';
$string['nameforlink'] = 'نام این پیوند را چه می‌خواهید بگذارید؟';
$string['nameforpage'] = 'نام';
$string['navigation'] = 'راهبری';
$string['needed'] = 'لازم';
$string['networkdropped'] = 'ارتباط اینترنت شما قابل اتکا نیست یا اینکه قطع شده است.<br />
لطفا مطلع باشید، تا زمان بهبود ارتباط اینترنتی‌تان، تغییراتی که می‌دهید ممکن است به‌درستی ذخیره نشود.';
$string['never'] = 'هیچ‌وقت';
$string['neverdeletelogs'] = 'Logها هیچ‌وقت حذف نشوند';
$string['new'] = 'جدید';
$string['newaccount'] = 'حساب کاربری جدید';
$string['newactivityname'] = 'نام جدید برای فعالیت {$a}';
$string['newcourse'] = 'یک درس جدید';
$string['newpassword'] = 'رمز ورود جدید';
$string['newpasswordfromlost'] = '<strong>توجه:</strong> <strong>رمز ورود فعلی</strong> شما در <strong>دومین</strong> پست الکترونیک از دو پست الکترونیک که به‌عنوان بخشی از فرآیند بازیابی رمز عبور فراموش‌شده ارسال می‌شوند قرار دارد. قبل از ادامهٔ مراحل، مطمئن شوید که رمز ورود جایگزین را دریافت کرده باشید.';
$string['newpassword_help'] = 'یک رمز جدید وارد کنید و یا اینکه این گزینه را خالی بگذارید تا رمز ورود فعلی تغییر نکند.';
$string['newpasswordtext'] = 'سلام {$a->firstname}،

رمز ورود حساب کاربری شما در «{$a->sitename}» بازنشانی شده
و یک مقدار جدید موقت برای آن درنظر گرفته شده است.

اطلاعات فعلی مربوط به ورود شما:
   نام کاربری: {$a->username}
   رمز ورود: {$a->newpassword}

لطفاً به این صفحه بروید تا رمز ورود خود را تغییر دهید:
   {$a->link}

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

با آرزوی سلامتی و موفقیت از طرف مدیر «{$a->sitename}»،
{$a->signoff}';
$string['newpicture'] = 'عکس جدید';
$string['newpicture_help'] = 'برای اضافه کردن یک عکس جدید، یک فایل تصویری (به قالب JPG یا PNG) را انتخاب کنید و سپس دکمهٔ «به‌روزرسانی مشخصات فردی» را کلیک نمائید. تصویر انتخاب شده به صورت مربع بریده شده و به اندازهٔ ۱۰۰×۱۰۰ پیکسل در تبدیل خواهد شد.';
$string['newpictureusernotsetup'] = 'عکس کاربر را تنها پس از اینکه تمام اطلاعات ضروری ذخیره شدند می‌توان اضافه کرد.';
$string['newsectionname'] = 'نام جدید برای قسمت {$a}';
$string['newsitem'] = 'خبر';
$string['newsitems'] = 'اعلانات';
$string['newsitemsnumber'] = 'تعداد اعلانات';
$string['newsitemsnumber_help'] = 'تالار اعلانات، یک تالار گفتگوی ویژه است که به‌طور خودکار در درس ساخته می‌شود، اشتراک به‌طور پیش‌فرض در آن اجباری است و تنها کاربرانی که دارای مجوز لازم هستند (به‌طور پیش‌فرض اساتید) می‌توانند مطالبی در آن قرار دهند.

این تنظیم تعداد اطلاعیه‌هایی که در بلوک تابلوی اعلانات در صفحهٔ درس نمایش داده می‌شود را تعیین می‌کند.

چنانچه نیازی به تالار اعلانات در درس نیست، این تنظیم باید روی صفر قرار داده شود.';
$string['newuser'] = 'کاربر جدید';
$string['newusernewpasswordsubj'] = 'حساب کاربری جدید';
$string['newusernewpasswordtext'] = 'سلام {$a->firstname}،

یک حساب کاربری جدید در «{$a->sitename}» برای شما ایجاد
و یک رمز ورود موقت برای آن در نظر گرفته شده است.

اطلاعات مربوط به ورود شما:
   نام کاربری: {$a->username}
   رمز ورود: {$a->newpassword}
   (وقتی که برای اولین بار وارد سایت شدید،
   باید رمز ورود خود را تغییر دهید)

برای شروع استفاده از «{$a->sitename}»، از طریق
{$a->link} وارد سایت شوید.

در اکثر برنامه‌های پست الکترونیک، آدرس فوق باید به صورت یک پیوند آبی رنگ
نمایش داده شده باشد که می‌توانید بر روی آن کلیک کنید. اگر به این صورت نبود،
کافیست که آدرس مورد نظر را در نوار آدرس واقع در قسمت بالای پنجرهٔ
مرورگر خود کپی نمائید.

با آرزوی سلامتی و موفقیت از طرف مدیر «{$a->sitename}»،
{$a->signoff}';
$string['newusers'] = 'کاربران جدید';
$string['newwindow'] = 'پنجرهٔ جدید';
$string['next'] = 'ادامه';
$string['nextsection'] = 'قسمت بعد';
$string['no'] = 'خیر';
$string['noblockstoaddhere'] = 'نمی‌توانید بلوکی را به این صفحه اضافه کنید.';
$string['nobody'] = 'هیچ‌کس';
$string['nochange'] = 'بدون تغییر';
$string['nocomments'] = 'بدون دیدگاه';
$string['nocourses'] = 'هیچ درسی';
$string['nocoursesfound'] = 'هیچ درسی که شامل «{$a}» باشد پیدا نشد';
$string['nocoursestarttime'] = 'درس فاقد زمان شروع می‌باشد';
$string['nocoursesyet'] = 'درسی در این طبقه وجود ندارد';
$string['nodstpresets'] = 'مدیر سیستم پشتیبانی از تغییر ساعت تابستانی را فعال نکرده است.';
$string['nofilesselected'] = 'فایلی بمنظور بازیابی انتخاب نشده است';
$string['nofilesyet'] = 'هنوز فایلی به درس شما ارسال نشده است';
$string['nofiltersapplied'] = 'هیچ فیلتری اعمال نشده است';
$string['nograde'] = 'بدون نمره';
$string['nohelpforactivityorresource'] = 'هم‌اکنون هیچ راهنمایی دربارهٔ این منبع یا فعالیت وجود ندارد';
$string['noimagesyet'] = 'در این درس هیچ عکسی قرار نداده‌اید';
$string['nologsfound'] = 'هیچ logی پیدا نشده است';
$string['nomatchingusers'] = 'هیچ کاربری با «{$a}» مطابقت ندارد';
$string['nomorecourses'] = 'درس دیگری که تطابق داشته باشد پیدا نشد';
$string['nomoreidnumber'] = 'به منظور جلوگیری از تلاقی، از idnumber استفاده نمی‌شود';
$string['none'] = 'هیچ';
$string['noneditingteacher'] = 'استاد بدون حق ویرایش';
$string['noneditingteacherdescription'] = 'استادهای بدون حق ویرایش می‌توانند در درس‌ها تدریس کنند و به شاگردان نمره دهند، ولی نمی‌توانند فعالیت‌ها را تغییر دهند.';
$string['nonstandard'] = 'غیر استاندارد';
$string['nopendingcourses'] = 'هیچ درسی در انتظار تصویب نیست';
$string['nopotentialadmins'] = 'کسی که بتواند مدیر باشد وجود ندارد';
$string['nopotentialcreators'] = 'کسی که بتواند درس‌ساز باشد وجود ندارد';
$string['nopotentialstudents'] = 'کسی که بتواند شاگرد باشد وجود ندارد';
$string['nopotentialteachers'] = 'کسی که بتواند استاد باشد وجود ندارد';
$string['norecentactivity'] = 'اخیراً فعالیتی انجام نشده است';
$string['noreplybouncemessage'] = 'شما به یک آدرس پست الکترونیک «no-reply» پاسخ داده‌اید. اگر قصد پاسخ به یکی از مطالب تالارهای گفتگو را داشته‌اید، لطفاً توسط تالارها گفتگوی {$a} این کار را انجام دهید.

محتوای نامهٔ شما به این صورت است:';
$string['noreplybouncesubject'] = '{$a} - نامه‌های الکترونیکی برگشت خورده.';
$string['noreplyname'] = 'به این نامهٔ الکترونیکی پاسخ ندهید';
$string['noresetrecord'] = 'چنین درخواستی برای بازنشانی وجود ندارد. لطفا یک درخواست جدید برای بازنشانی رمز ورود بدهید.';
$string['noresults'] = 'بدون نتیجه';
$string['normal'] = 'عادی';
$string['normalfilter'] = 'جستجوی معمولی';
$string['nosite'] = 'درس سایت-تراز پیدا نشد';
$string['nostudentsfound'] = '{$a} پیدا نشد';
$string['nostudentsingroup'] = 'هنوز شاگردی در این گروه وجود ندارد';
$string['nostudentsyet'] = 'هنوز شاگردی در این درس ثبت‌نام نشده است';
$string['nosuchemail'] = 'چنین آدرس الکترونیکی‌ای وجود ندارد';
$string['notavailable'] = 'قابل دسترسی نیست';
$string['notavailablecourse'] = '{$a} آماده نیست';
$string['noteachersyet'] = 'این درس هنوز استاد ندارد';
$string['notenrolled'] = '{$a} در این درس ثبت‌نام نیست.';
$string['notenrolledprofile'] = 'این مشخصات فردی قابل مشاهده نمی‌باشد زیرا کاربر مورد نظر در این درس ثبت‌نام نیست.';
$string['noteusercannotrolldatesoncontext'] = '<strong>توجه:</strong> با توجه به اینکه شما مجوزهای مورد نیاز را ندارید، قابلیت تغییر تاریخ‌ها به هنگام بازیابی این پشتیبان غیر فعال شده است.';
$string['noteuserschangednonetocourse'] = '<strong>توجه:</strong> در صورت بازیابی داده‌های کاربر (در فعالیت‌ها، فایل‌ها یا پیغام‌ها) کاربران دری نیز باید بازیابی شوند. این تنظیم برای شما تغییر کرده است.';
$string['nothingnew'] = 'فعالیت جدیدی صورت نگرفته است';
$string['nothingtodisplay'] = 'چیزی برای نمایش وجود ندارد';
$string['notice'] = 'اخطار';
$string['noticenewerbackup'] = 'این فایل پشتیبان توسط مودل {$a->backuprelease} ({$a->backupversion}) ایجاد شده است و از مودل مورد استفادهٔ شما که {$a->serverrelease} ({$a->serverversion}) می‌باشد جدیدتر است. بازیابی از این فایل ممکن است موجب بی‌ثباتی شود زیرا سازگاری فایل‌های پشتیبان با نسخه‌های قدیمی قابل ضمانت نیست.';
$string['notifications'] = 'تذکرات';
$string['notifyloginfailuresmessage'] = '{$a->time}، آدرس IP: {$a->ip}، کاربر: {$a->info}، نام کامل کاربر: {$a->name}';
$string['notifyloginfailuresmessageend'] = 'این log ها را می‌توانید در {$a} ببینید.';
$string['notifyloginfailuresmessagestart'] = 'لیست زیر تلاش‌های ناموفقی که از دفعهٔ قبل تا این لحظه برای ورود به {$a} صورت گرفته است نمایش می‌دهد';
$string['notifyloginfailuressubject'] = '{$a} :: اعلان ورودهای ناموفق';
$string['notincluded'] = 'شامل نشده است';
$string['notingroup'] = 'متأسفیم، ولی برای دیدن این فعالیت باید جزئی از یک گروه باشید.';
$string['notpublic'] = 'عمومی نیست!';
$string['nousersfound'] = 'کاربری پیدا نشد';
$string['nousersmatching'] = 'کاربری منطبق با «{$a}» پیدا نشد';
$string['nousersyet'] = 'هنوز کاربری وجود ندارد';
$string['novalidcourses'] = 'درس معتبری برای نمایش وجود ندارد';
$string['now'] = 'اکنون';
$string['numattempts'] = '{$a} تلاش ناموفق برای ورود';
$string['numberofcourses'] = 'تعداد درس‌ها';
$string['numberweeks'] = 'تعداد قسمت‌ها';
$string['numday'] = '{$a} روز';
$string['numdays'] = '{$a} روز';
$string['numhours'] = '{$a} ساعت';
$string['numletters'] = '{$a} حرف';
$string['numminutes'] = '{$a} دقیقه';
$string['nummonth'] = '{$a} ماه';
$string['nummonths'] = '{$a} ماه';
$string['numseconds'] = '{$a} ثانیه';
$string['numviews'] = '{$a} بار مطالعه';
$string['numweek'] = '{$a} هفته';
$string['numweeks'] = '{$a} هفته';
$string['numwords'] = '{$a} کلمه';
$string['numyear'] = '{$a} سال';
$string['numyears'] = '{$a} سال';
$string['ok'] = 'تایید';
$string['oldpassword'] = 'رمز ورود فعلی';
$string['olduserdirectory'] = 'این دایرکتوری قدیمی کاربران است و بیش از این مورد نیاز نیست. می‌توانید بدون نگرانی آن را پاک کنید. فایل‌های درون آن به دایرکتوری جدید کاربران کپی شده‌اند.';
$string['optional'] = 'اختیاری';
$string['options'] = 'گزینه‌ها';
$string['order'] = 'ترتیب';
$string['originalpath'] = 'مسیر اولیه';
$string['orphanedactivitiesinsectionno'] = 'فعالیت‌های یتیم (قسمت {$a})';
$string['other'] = 'غیره';
$string['outline'] = 'خلاصه';
$string['outlinereport'] = 'گزارش اجمالی';
$string['page'] = 'صفحه';
$string['pagea'] = 'صفحه {$a}';
$string['pagedcontentnavigationactiveitem'] = 'صفحه جاری, صفحه‌ی {$a}';
$string['pagedcontentnavigationitem'] = 'برو به صفحه {$a}';
$string['pagedcontentpagingbaritemsperpage'] = 'نمایش {$a} مورد در هر صفحه';
$string['pageheaderconfigablock'] = 'پیکربندی یک بلوک در {$a->fullname}';
$string['pagepath'] = 'موقعیت صفحه';
$string['pageshouldredirect'] = 'باید به طور خودکار به صفحهٔ دیگری هدایت شوید. اگر هچ اتفاقی نیافتاد، از پیوند «ادامه» که در پائین وجود دارد استفاده کنید.';
$string['parentcategory'] = 'ایجاد در طبقهٔ';
$string['parentcoursenotfound'] = 'درس منشأ پیدا نشد!';
$string['parentfolder'] = 'پوشهٔ مادر';
$string['participants'] = 'شرکت کنندگان';
$string['participantscount'] = 'تعداد شرکت‌کنندگان : {$a}';
$string['participantslist'] = 'لیست شرکت کنندگان';
$string['participationratio'] = 'نسبت مشارکت';
$string['participationreport'] = 'گزارش مشارکت';
$string['password'] = 'رمز ورود';
$string['passwordchanged'] = 'رمز ورود تغییر یافته است';
$string['passwordconfirmchange'] = 'تایید تغییر رمز ورود';
$string['passwordextlink'] = 'پیوند زیر برای بازیابی رمز ورود فراموش‌شدهٔ شما تهیه شده است. با کلیک بر روی آن از مودل خارج خواهید شد.';
$string['passwordforgotten'] = 'رمز ورود فراموش شده';
$string['passwordforgotteninstructions'] = 'ابتدا باید اطلاعات مربوط به شما در پایگاه دادهٔ کاربران پیدا شود. لطفاً نام کاربری <strong>یا</strong> آدرس پست الکترونیک ثبت شدهٔ خود را در محل مناسب وارد نمائید. نیازی به ورود هر دو نیست.';
$string['passwordforgotteninstructions2'] = 'برای بازنشانی رمز ورودتان، نام کاربری یا آدرس پست الکترونیک خود را وارد نمائید. در صورتی که مشخصات ورودی شما در پایگاه داده پیدا شود، یک نامهٔ الکترونیکی شامل دستورالعمل‌هایی در مورد نحوه دسترسی دوباره به آدرس پست الکترونیک شما ارسال خواهد شد.';
$string['passwordnohelp'] = 'کمکی برای پیداکردن رمز ورود فراموش‌شدهٔ شما موجود نیست. لطفاً با مدیر مودل خود تماس بگیرید.';
$string['passwordrecovery'] = 'بله، کمک کنید وارد سایت شوم';
$string['passwordsdiffer'] = 'این رمزها با یکدیگر تطابق ندارند';
$string['passwordsent'] = 'رمز ورود ارسال شده است';
$string['passwordsenttext'] = '<p>یک نامهٔ الکترونیکی به آدرس شما در «{$a->email} ارسال شده است.</p>
   <p><b>لطفاً جهت دریافت رمز ورود جدید پست‌های الکترونیک خود را بررسی نمائید.</b></p>
   <p>رمز ورود جدید به صورت خودکار تولید شده است، در صورت تمایل می‌توانید این رمز را
به عبارت ساده‌تری که راحت‌تر بخاطر سپرده می‌شود <a href="{$a->link}">تغییر دهید</a>.</p>';
$string['passwordset'] = 'گذرواژه‌ی شما تنظیم شد.';
$string['path'] = 'مسیر';
$string['pathnotexists'] = 'این مسیر در کارگزار شما وجود ندارد!';
$string['pathslasherror'] = 'مسیر نمی‌تواند با اسلش خاتمه یابد!!';
$string['paymentinstant'] = 'از دکمهٔ زیر برای پرداخت و ثبت‌نام شدن در درس در ظرف چند دقیقه استفاده نمائید!';
$string['paymentpending'] = '(<small><b><u>{$a}</u></b> مورد معلق</small>)';
$string['paymentrequired'] = 'ثبت‌نام در این درس مستلزم پرداخت شهریه است.';
$string['payments'] = 'پرداخت‌ها';
$string['paymentsorry'] = 'از پرداخت شما متشکریم! متأسفانه هنوز پرداخت شما کاملاً پردازش نشده است، و هنوز برای ورود به درس «{$a->fullname}» ثبت‌نام نشده‌اید. لطفاً چند ثانیه بعد برای ورود به درس تلاش نمائید، اما اگر مشکل شما باز هم برقرار بود لطفاً {$a->teacher} یا مدیر سایت را مطلع نمائید.';
$string['paymentthanks'] = 'از پرداخت شما متشکریم! هم‌اکنون در درس خود ثبت‌نام شدید:<br />«{$a}»';
$string['pendingrequests'] = 'درخواست‌های معلق';
$string['percents'] = '{$a} ٪';
$string['periodending'] = 'خاتمهٔ دوره ({$a})';
$string['perpage'] = 'در هر صفحه';
$string['perpagea'] = 'در هر صفحه: {$a}';
$string['personal'] = 'شخصی';
$string['personalprofile'] = 'مشخصات فردی';
$string['phone'] = 'تلفن';
$string['phone1'] = 'تلفن';
$string['phone2'] = 'تلفن همراه';
$string['phpinfo'] = 'اطلاعات PHP';
$string['pictureof'] = 'عکس {$a}';
$string['pictureofuser'] = 'عکس کاربر';
$string['pleaseclose'] = 'لطفاً هم‌اکنون این پنجره را ببندید.';
$string['pleasesearchmore'] = 'لطفاً کمی بیشتر جستجو کنید';
$string['pleaseusesearch'] = 'لطفاً از جستجو استفاده کنید';
$string['plugin'] = 'پلاگین';
$string['plugincheck'] = 'بررسی پلاگین‌ها';
$string['plugindeletefiles'] = 'تمام اطلاعات مربوط به پلاگین «{$a->name}» از پایگاه داده حذف شدند. اکنون به منظور جلوگیری از نصب شدن مجدد به صورت خودکار، باید این پوشه را از روی کارگزار خود پاک کنید: {$a->directory}';
$string['pluginsetup'] = 'برپاسازی جدول‌های پلاگین';
$string['policyaccept'] = 'موافقم و می‌پذیرم';
$string['policyagree'] = 'برای ادامهٔ استفاده از این سایت باید موافقت خود را با خط مشی آن اعلام کنید. آیا موافقید؟';
$string['policyagreement'] = 'موافقت با خط مشی سایت';
$string['policyagreementclick'] = 'مشاهدهٔ موافقت‌نامهٔ خط مشی سایت';
$string['popup'] = 'واشونده';
$string['popupwindow'] = 'باز کردن فایل در پنجرهٔ جدید';
$string['popupwindowname'] = 'پنجرهٔ جدید';
$string['post'] = 'ارسال';
$string['posts'] = 'مطالب ارسالی';
$string['potentialadmins'] = 'کسانی که می‌توانند مدیر باشند';
$string['potentialcreators'] = 'کسانی که می‌توانند درس‌ساز باشند';
$string['potentialstudents'] = 'کسانی که می‌توانند شاگرد باشند';
$string['potentialteachers'] = 'کسانی که می‌توانند استاد باشند';
$string['preferences'] = 'ترجیحات';
$string['preferredlanguage'] = 'زبان دلخواه';
$string['preferredtheme'] = 'پوستهٔ دلخواه';
$string['preprocessingbackupfile'] = 'پیش‌پردازش فایل پشتیبان';
$string['prev'] = 'قبلی';
$string['preview'] = 'پیش‌نمایش';
$string['previewhtml'] = 'پیش‌نمایش قالب HTML';
$string['previeworchoose'] = 'مشاهدهٔ پیش‌نمایش یا انتخاب کردن یک پوسته';
$string['previous'] = 'قبلی';
$string['previouslyselectedusers'] = 'کاربران انتخاب شده ای که با «{$a}» جور در نیامده‌اند';
$string['previoussection'] = 'قسمت قبل';
$string['primaryadminsetup'] = 'برپاکردن حساب کاربری مدیر';
$string['privacy:metadata:config_log:oldvalue'] = 'مقدار قبلی برای این تنظیم.';
$string['privacy:metadata:config_log:plugin'] = 'پلاگین تغییر کرد';
$string['privacy:metadata:config_log:timemodified'] = 'زمان تغییر.';
$string['privacy:metadata:config_log:userid'] = 'کاربری که تغییر را ایجاد نموده است.';
$string['privacy:metadata:config_log:value'] = 'مقدار جدید برای این تنظیم.';
$string['privacy:metadata:events_queue'] = 'صف رویدادهای کاربران در انتظار پردازش.';
$string['privacy:metadata:events_queue:eventdata'] = 'اطلاعات ذخیره‌ه‌شده در رویداد.';
$string['privacy:metadata:events_queue:timecreated'] = 'زمان ایجاد این رویداد.';
$string['privacy:metadata:events_queue:userid'] = 'شناسه کاربر مرتبط با این رویداد';
$string['privacy:metadata:log'] = 'مجموعه رویدادهای قبلی';
$string['privacy:metadata:log:course'] = 'درس';
$string['privacy:metadata:log:info'] = 'اطلاعات اضافی';
$string['privacy:metadata:log:ip'] = 'آدرس آی‌پی مورد استفاده هنگام رویداد';
$string['privacy:metadata:log:module'] = 'ماژول';
$string['privacy:metadata:log:url'] = 'آدرس اینترنتی مرتبط با رویداد';
$string['privatefiles'] = 'فایل‌های شخصی';
$string['private_files_handler'] = 'ذخیرهٔ ضمیمه‌های یک نامهٔ الکترونیکی در محل نگهداری فایل‌های خصوصی کاربر';
$string['private_files_handler_name'] = 'پست الکترونیک به فایل‌های خصوصی';
$string['privatefilesmanage'] = 'مدیریت فایل‌های خصوصی';
$string['profile'] = 'مشخصات فردی';
$string['profilenotshown'] = 'توصیف شخصی این کاربر تا زمانی که حداقل در یک درس ثبت‌نام شود نمایش داده نخواهد شد.';
$string['publicprofile'] = 'مشخصات عمومی';
$string['publicsitefileswarning'] = 'توجه: فایل‌هایی که اینجا قرار داده می‌شوند، توسط همه قابل دستیابی هستند';
$string['publicsitefileswarning2'] = 'توجه: فایل‌هایی که در این قسمت قرار داده می‌شوند توسط هر کسی که آدرس آن‌ها را بداند (یا بتواند حدس بزند) قابل دستیابی هستند. بنا به دلایل امنیتی، توصیه می‌شود که فایل‌های پشتیبان سریعاً پس از بازنشانی شدن حذف شوند.';
$string['publicsitefileswarning3'] = 'توجه: فایل‌هایی که اینجا قرار دارند توسط هر کسی که آدرس آن‌ها را می‌داند (یا حدس می‌زند) قابل دستیابی هستند.<br />به دلایل امنیتی، فایل‌های پشتیبان باید فقط در پوشهٔ امن backupdata ذخیره شوند.';
$string['question'] = 'سؤال';
$string['questionsinthequestionbank'] = 'سؤال‌هایی در بانک سؤال';
$string['quotausage'] = 'شما در  حال حاضر {$a->used} از  {$a->total} محدودیت‌تان را استفاده کرده‌اید.';
$string['readinginfofrombackup'] = 'در حال خواندن اطلاعات از پشتیبان';
$string['readme'] = 'مرا بخوان';
$string['recentactivity'] = 'فعالیت‌های اخیر';
$string['recentactivityreport'] = 'گزارش کامل فعالیت‌های اخیر...';
$string['recipientslist'] = 'لیست دریافت کنندگان';
$string['recreatedcategory'] = 'دستهٔ {$a} دوباره ایجاد شد';
$string['redirect'] = 'تغییر مسیر';
$string['reducesections'] = 'کم کردن تعداد قسمت‌ها';
$string['refresh'] = 'تازه‌سازی';
$string['refreshingevents'] = 'تازه‌سازی رویدادها';
$string['registration'] = 'ثبت مودل';
$string['registrationcontact'] = 'تماس از عموم';
$string['registrationcontactno'] = 'خیر، فرمی برای تماس گرفتن با من در لیست سایت نباشد';
$string['registrationcontactyes'] = 'بله، فرمی به منظور تماس با من برای مودل‌کارهای آینده فراهم شود';
$string['registrationemail'] = 'اطلاع‌رسانی توسط پست الکترونیک';
$string['registrationinfo'] = '<p>این صفحه امکان ثبت سایت شما در moodle.org را فراهم می‌کند. مراحل ثبت کاملاً رایگان است.
مزیت اصلی ثبت کردن، این است که شما به یک لیست پستی کم ترافیک برای اطلاع از ابلاغات مهمی
مانند خطرهای امنیتی و انتشار نسخه‌های جدید مودل اضافه خواهید شد.</p>
<p>به صورت پیش‌فرض، اطلاعات شما محرمانه خواهد بود، و هرگز به کسی فروخته یا ارائه نخواهد شد.
تنها دلیل جمع‌آوری این اطلاعات اهداف پشتیبانی، و کمک به ایجاد
یک تصویر آماری از اجتماع مودل به صورت کلی است.</p>
<p>در صورت تمایل، می‌توانید اجازه دهید که نام سایت، کشور و آدرس آینترنتی سایتتان به لیست عمومی سایت‌های مودل اضافه شود.</p>
<p>تمام تقاضاهای ثبت جدید قبل از اضافه شدن به لیست، به صورت دستی تایید می‌شوند، ولی وقتی که اضافه شدید هر زمان که بخواهید می‌توانید اطلاعات مربوط به سایت خود را با ارائه مجدد این فرم به‌روز نمائید.</p>';
$string['registrationinfotitle'] = 'اطلاعات عضویت';
$string['registrationno'] = 'خیر، من مایل به دریافت پست الکترونیک نیستم';
$string['registrationsend'] = 'ارسال اطلاعات ثبت به moodle.org';
$string['registrationyes'] = 'بله، لطفاً من را از مسائل مهم مطلع کن';
$string['reject'] = 'رد';
$string['rejectdots'] = 'رد...';
$string['reload'] = 'بررسی مجدد';
$string['remoteappuser'] = 'کاربر {$a} خارجی';
$string['remove'] = 'حذف';
$string['removeadmin'] = 'حذف مدیر';
$string['removecreator'] = 'حذف درس‌ساز';
$string['removestudent'] = 'حذف شاگرد';
$string['removeteacher'] = 'حذف استاد';
$string['rename'] = 'تغییر نام';
$string['renamefileto'] = 'تغییر نام <b>{$a}</b> به';
$string['report'] = 'گزارش';
$string['reports'] = 'گزارش‌ها';
$string['repositories'] = 'انباره‌ها';
$string['requestcourse'] = 'درخواست ایجاد یک درس';
$string['requestedby'] = 'متقاضی';
$string['requestedcourses'] = 'درس‌های درخواستی';
$string['requestreason'] = 'دلیل درخواست درس';
$string['required'] = 'لازم است';
$string['requirespayment'] = 'دستیابی به این درس مستلزم پرداخت شهریه است';
$string['resendemail'] = 'ارسال مجدد ایمیل';
$string['reset'] = 'بازنشانی';
$string['resetcomponent'] = 'مؤلفه';
$string['resetcourse'] = 'بازنشانی درس';
$string['resetinfo'] = 'این صفحه به شما امکان خالی کردن یک درس از اطلاعات کاربران، با حفظ فعالیت‌ها و تنظیمات دیگر، را می‌دهد. لطفاً مراقب باشید که با انتخاب موارد زیر و ارسال این صفحه اطلاعات انتخاب شدهٔ مربوط به کاربران را برای همیشه از این درس پاک خواهید کرد!';
$string['resetnotimplemented'] = 'بازنشانی پیاده‌سازی نشده است';
$string['resetrecordexpired'] = 'پیوندی که برای بازنشانی رمز ورود استفاده کرده‌اید بیش از {$a} دقیقهٔ پیش ساخته شده و منقضی شده است. لطفا درخواست یک بازنشانی جدید را آغاز کنید.';
$string['resetstartdate'] = 'تغییر تاریخ شروع';
$string['resetstatus'] = 'وضعیت';
$string['resettable'] = 'بازنشانی تنظیمات جدول';
$string['resettask'] = 'وظیفه';
$string['resettodefaults'] = 'بازگشت به مقادیر پیش‌فرض';
$string['resortcourses'] = 'مرتب سازی دروس';
$string['resortsubcategoriesby'] = 'مرتب‌کردن زیرطبقه‌ها به‌صورت صعودی بر اساس {$a}';
$string['resortsubcategoriesbyreverse'] = 'مرتب‌کردن زیرطبقه‌ها به‌صورت نزولی بر اساس {$a}';
$string['resource'] = 'منبع';
$string['resourcedisplayauto'] = 'خودکار';
$string['resourcedisplaydownload'] = 'اجبار به دریافت فایل';
$string['resourcedisplayembed'] = 'جاسازی شدن';
$string['resourcedisplayframe'] = 'درون قاب';
$string['resourcedisplaynew'] = 'پنجرهٔ جدید';
$string['resourcedisplayopen'] = 'باز شدن';
$string['resourcedisplaypopup'] = 'در پنجرهٔ pop-up';
$string['resources'] = 'منابع';
$string['resources_help'] = 'با استفاده از منابع مختلف تقریباً هر نوع محتوای تحت وبی را می‌توان درون درس قرار داد.';
$string['restore'] = 'بازیابی';
$string['restorecancelled'] = 'فرآیند بازیابی لغو شد';
$string['restorecannotassignroles'] = 'در حین فرآیند بازیابی، نقش‌هایی باید منسوب شوند و شما مجوز انجام این کار را ندارید';
$string['restorecannotcreateorassignroles'] = 'در حین فرآیند بازیابی، نقش‌هایی باید ایجاد یا منسوب شوند و شما مجوز انجام این کار را ندارید';
$string['restorecannotcreateuser'] = 'فرآیند بازیابی نیاز به تعریف کاربر «{$a}» از روی فایل پشتیبان دارد و شما مجوز انجام این کار را ندارید';
$string['restorecannotoverrideperms'] = 'فرآیند می‌خواهد مجوزها را بازنویسی کند و شما مجوز انجام این کار را ندارید';
$string['restorecoursenow'] = 'شروع بازیابی این درس';
$string['restoredaccount'] = 'حساب کاربری بازیابی شده';
$string['restoredaccountinfo'] = 'این حساب کاربری از یک پایگاه دادهٔ دیگر وارد شده و رمز ورود آن از دست رفته است. برای تعیین یک رمز ورود جدید از طریق پست الکترونیک، روی «ادامه» کلیک کنید.';
$string['restorefinished'] = 'بازیابی با موفقیت انجام شد.';
$string['restoremnethostidmismatch'] = 'شناسهٔ میزبان MNet کاربر «{$a}» با شناسهٔ میزبان MNet محلی مطابقت ندارد.';
$string['restoreto'] = 'بازیابی در';
$string['restoretositeadding'] = 'هشدار: شما در آستانهٔ بازیابی صفحهٔ اول سایت و اضافه کردن داده به آن هستید!';
$string['restoretositedeleting'] = 'هشدار: شما در آستانهٔ حذف داده‌های صفحهٔ اول و سپس بازیابی آن هستید!';
$string['restoreuserconflict'] = 'تلاش برای بازیابی کاربر «{$a}» از فایل پشتیبان موجب ناسازگاری خواهد شد.';
$string['restoreuserinfofailed'] = 'روند بازیابی متوقف شده است زیرا شما اجازهٔ بازیابی داده‌های کاربران را ندارید.';
$string['restoreusersprecheck'] = 'بررسی داده‌های کاربران';
$string['restoreusersprecheckerror'] = 'در نتیجهٔ بررسی داده‌های کاربران مشکلاتی تشخیص داده شدند';
$string['restricted'] = 'محدود';
$string['returningtosite'] = 'قبلاً در این وب‌سایت بوده‌اید؟';
$string['returntooriginaluser'] = 'بازگشت به {$a}';
$string['revert'] = 'بازگردانی تغییرات';
$string['role'] = 'نقش';
$string['roleassignments'] = 'اختصاص وظایف';
$string['rolemappings'] = 'نگاشت نقش‌ها';
$string['rolerenaming'] = 'تغییرنام نقش‌ها';
$string['rolerenaming_help'] = 'به کمک این تنظیم می‌توان اسامی نمایش داده شده برای هر یک از نقش‌های مورد استفاده در درس را تغییر داد. تنها نامی که نمایش داده می‌شود تغییر می‌کند - مجوزهای نقش‌ها تغییری نمی‌کنند. اسامی جدید نقش‌ها در اسامی جدیدی که برای نقش‌ها تعیین می‌شود در صفحهٔ شرکت کنندگان درس و تمام صفحات دیگر داخل درس به جای نام اصلی نقش‌ها نمایش داده می‌شوند. اگر یک نام جدید برای نقشی که توسط مدیر سایت به عنوان نقش مدیر درس انتخاب شده است تعیین شده باشد، آنگاه هنگام نمایش لیست دروس، از نام جدیدی که برای این نقش تعیین شده است استفاده خواهد شد.';
$string['roles'] = 'نقش‌ها';
$string['rss'] = 'RSS';
$string['rssarticles'] = 'تعداد موارد جدید در RSS';
$string['rsserror'] = 'خواندن اطلاعات RSS با خطا مواجه شد';
$string['rsserrorauth'] = 'پیوند RSS شما شامل یک کلید رمز (توکن) شناسایی معتبر نیست.';
$string['rsserrorguest'] = 'این feed از دسترسی مهمان برای دستیابی به داده استفاده می‌کند، ولی مهمان‌ها اجازهٔ خواندن اطلاعات را ندارند. با یک کاربر معتبر به جایی که این feed از آنجا (آدرس) می‌آید بروید و یک پیوند RSS جدید از دریافت کنید.';
$string['rsskeyshelp'] = '<p>برای اطمینان از امنیت و حریم شخصی، آدرس‌های RSS feed ها شامل یک token خاص هستند که برای هر کاربر متفاوت است و موجب شناسایی کاربران می‌شود. این کار باعث می‌شود جلوی دسترسی سایر کاربران به قسمت‌هایی از سایت که مجاز نیستند گرفته شود.</p><p>Token اولین باری که به هر جایی که RSS feed تولید می‌کند بروید، به‌طور خودکار ساخته می‌شود. اگر فکر می‌کنید که RSS feed token‌ شما در معرض خطر قرار گرفته است، می‌توانید با کلیک بر روی پیوند بازنشانی، یک token جدید درخواست کنید. توجه داشته باشید که آدرس‌های RSS feed های فعلی شما نامعتبر خواهند شد.</p>';
$string['rsstype'] = 'RSS feed برای این فعالیت';
$string['save'] = 'ذخیره';
$string['saveandnext'] = 'ذخیره و نمایش بعدی';
$string['savechanges'] = 'ذخیرهٔ تغییرات';
$string['savechangesanddisplay'] = 'ذخیره و نمایش';
$string['savechangesandreturn'] = 'ذخیره و برگشت';
$string['savechangesandreturntocourse'] = 'ذخیره و بازگشت به درس';
$string['savecomment'] = 'ثبت نظر';
$string['savedat'] = 'تاریخ ذخیره:';
$string['savepreferences'] = 'ذخیرهٔ ترجیحات';
$string['saveto'] = 'ذخیره در';
$string['scale'] = 'مقیاس';
$string['scale_help'] = 'مقیاس‌ها راهی برای ارزیابی یا نمره‌دهی به چگونگی عملکرد در یک فعالیت فراهم می‌کنند. یک مقیاس به صورت لیستی از مقادیر که با کاما از هم جدا شده و به ترتیب از منفی (بد) تا مثبت (خوب) مرتب شده‌اند تعریف می‌شود. به عنوان مثال: «ناامید کننده,به اندازه کافی خوب نیست,متوسط,خوب,خیلی خوب,عالی!»';
$string['scale_link'] = 'نمره/مقیاس';
$string['scales'] = 'مقیاس‌ها';
$string['scalescustom'] = 'مقیاس‌های اختصاصی';
$string['scalescustomcreate'] = 'تعریف یک مقیاس جدید';
$string['scalescustomno'] = 'هنوز مقیاس اختصاصی‌ای ایجاد نشده است';
$string['scalesstandard'] = 'مقیاس‌های استاندارد';
$string['scalestandard'] = 'مقیاس استاندارد';
$string['scalestandard_help'] = 'مقیاس‌های استاندارد در کل سایت و در تمام درس‌ها قابل استفاده هستند.';
$string['scalestandard_link'] = 'نمره/مقیاس';
$string['scalestip'] = 'جهت تعریف مقیاس‌های اختصاصی، از پیوند «مقیاس‌ها» در منوی مدیریت درس خود استفاده نمائید.';
$string['scalestip2'] = 'به منظور تعریف مقیاس‌های اختصاصی، بر روی پیوند نمره‌ها واقع در منوی مدیریت درس کلیک نموده، سپس ویرایش -> مقیاس‌ها را انتخاب نمائید.';
$string['schedule'] = 'زمان‌بندی';
$string['screenshot'] = 'عکس صفحه';
$string['search'] = 'جستجو';
$string['searchagain'] = 'جستجوی مجدد';
$string['searchbyemail'] = 'جستجو بر اساس آدرس پست الکترونیک';
$string['searchbyusername'] = 'جستجو بر اساس نام کاربری';
$string['searchcourses'] = 'جستجو بین درس‌ها';
$string['search_help'] = 'برای جستجوی سادهٔ یک یا چند کلمه در داخل متن، کافی است آن‌ها را با فاصله از هم وارد کنید. تمامی کلماتی که بیش از دو حرف داشته باشند مورد جستجو قرار خواهند گرفت.

برای جستجوی پیشرفته، هیچ متنی را وارد نکنید و فقط دکمهٔ جستجو را فشار دهید تا فرم جستجوی پیشرفته نمایش داده شود.';
$string['searchoptions'] = 'نحوهٔ جستجو';
$string['searchresults'] = 'نتیجهٔ جستجو';
$string['sec'] = 'ثانیه';
$string['seconds'] = 'ثانیه';
$string['secondsleft'] = '{$a} ثانیه';
$string['secondstotime172800'] = '۲ روز';
$string['secondstotime259200'] = '۳ روز';
$string['secondstotime345600'] = '۴ روز';
$string['secondstotime432000'] = '۵ روز';
$string['secondstotime518400'] = '۶ روز';
$string['secondstotime604800'] = '۱ هفته';
$string['secondstotime86400'] = '۱ روز';
$string['secretalreadyused'] = 'از پیوند تایید تغییر رمز ورود قبلاً استفاده شده است؛ رمز ورود تغییر نکرد.';
$string['secs'] = 'ثانیه';
$string['section'] = 'قسمت';
$string['sectionactionnotsupported'] = 'عمل «{$a}» روی قسمت در اینجا پشتیبانی نمی‌شود';
$string['sectionname'] = 'نام این قسمت';
$string['sections'] = 'قسمت‌ها';
$string['seealsostats'] = 'آمار را هم ببینید';
$string['selctauser'] = 'یک کاربر را انتخاب کنید';
$string['select'] = 'انتخاب';
$string['selectacategory'] = 'لطفا یک طبقه بندی را انتخاب کنید';
$string['selectacountry'] = 'انتخاب کشور';
$string['selectacourse'] = 'یک درس را انتخاب کنید';
$string['selectacoursesite'] = 'سایت یا یکی از درس‌ها را انتخاب کنید';
$string['selectagroup'] = 'یک گروه را انتخاب کنید';
$string['selectall'] = 'انتخاب همه';
$string['selectallusersonpage'] = 'انتخاب همهٔ کاربران در این صفحه';
$string['selectalluserswithcount'] = 'انتخاب همهٔ {$a} کاربر';
$string['selectamodule'] = 'لطفاً یک ماژول فعالیت را انتخاب نمائید';
$string['selectanaction'] = 'یک عمل را انتخاب کنید';
$string['selectanoptions'] = 'یک گزینه انتخاب کنید';
$string['selectaregion'] = 'یک منطقه را انتخاب کنید';
$string['selectcategorysort'] = 'چه طبقه‌هایی را می‌خواهید مرتب کنید؟';
$string['selectcategorysortby'] = 'انتخاب کنید که می‌خواهید طبقه‌ها چگونه مرتب شوند';
$string['selectcoursesortby'] = 'انتخاب کنید که می‌خواهید درس‌ها چگونه مرتب شوند';
$string['selectdefault'] = 'انتخاب گزینه‌های پیش‌فرض';
$string['selectedcategories'] = 'طبقه بندی انتخاب شده';
$string['selectedfile'] = 'فایل انتخاب شده';
$string['selectednowmove'] = '{$a} فایل برای انتقال انتخاب شده‌است. حال به مسیر مقصد بروید و دکمهٔ «انتقال فایل‌ها به این محل» را فشار دهید';
$string['selectfiles'] = 'انتخاب فایل‌ها';
$string['selectmoduletoviewhelp'] = 'با انتخاب هر کدام از فعالیت‌ها یا منابع، راهنمای مربوطه‌اش را خواهید دید.

برای اضافه کردن سریع یک فعالیت یا منبع روی نام آن دو بار کلیک کنید.';
$string['selectnos'] = 'انتخاب همهٔ «خیر» ها';
$string['selectperiod'] = 'انتخاب مدت';
$string['senddetails'] = 'ارسال جزئیات من از طریق پست الکترونیک';
$string['separate'] = 'فردی';
$string['separateandconnected'] = 'یادگیری به صورت‌های فردی و جمعی';
$string['separateandconnectedinfo'] = 'این مقیاس بر اساس نظریه یادگیری انفرادی و اجتماعی طراحی شده است. این نظریه دو روش مختلف برای ارزیابی و یادگیری چیزهایی که می‌بینیم و می‌شنویم را شرح می‌دهد.<ul><li><strong>فراگیران انفرادی</strong> سعی می‌کنند که تا حد امکان بی‌طرف باشند و احساسات و عواطف را دخیل نکنند. این افراد در گفتگو با کسانی که نظرات متفاوتی دارند، سعی می‌کنند که تا حد امکان نظر خود را تغییر ندهند و برای دفاع از ایده‌های خود، با استفاده از منطق، سعی می‌کنند اشکالات موجود در نظرات دیگران را پیدا کنند.</li><li><strong>فراگیران اجتماعی</strong> به سایرین اهمیت بیشتری می‌دهند. آنها به راحتی هم‌صحبت می‌شوند، گوش می‌کنند و سؤال می‌پرسند تا زمانی که احساس کنند می‌توانند ارتباط برقرار کنند و «مسائل را از زاویهٔ دید دیگران متوجه شوند». آنها سعی می‌کنند با سهیم شدن در تجربیاتی که منجر به دانش دیگران شده است، چیزی یاد بگیرند.</li></ul>';
$string['servererror'] = 'هنگام ارتباط با کارگزار خطایی رخ داد';
$string['serverlocaltime'] = 'وقت محلی کارگزار';
$string['setcategorytheme'] = 'تعیین پوستهٔ طبقه';
$string['setpassword'] = 'تعیین رمز ورود';
$string['setpasswordinstructions'] = 'لطفا رمز ورود جدید خود را وارد کنید، سپس تغییرات را ذخیره نمائید.';
$string['settings'] = 'تنظیمات';
$string['shortname'] = 'نام کوتاه';
$string['shortnamecollisionwarning'] = '[*] = این نام کوتاه هم‌اکنون توسط درسی در حال استفاده است و به محض تصویب شدن نیاز به تغییر خواهد داشت';
$string['shortnamecourse'] = 'نام کوتاه درس';
$string['shortnamecourse_help'] = 'نام کوتاه درس در نوار راهبری و در موضوع نامه‌های الکترونیکی مربوط به درس نمایش داده می‌شود.';
$string['shortnametaken'] = 'این نام کوتاه برای درس دیگری استفاده شده است ({$a})';
$string['shortnameuser'] = 'نام کوتاه کاربر';
$string['shortsitename'] = 'نام کوتاه برای سایت (مثلاً یک کلمه)';
$string['show'] = 'نمایش';
$string['showactions'] = 'نمایش اقدامات';
$string['showadvancededitor'] = 'پیشرفته';
$string['showadvancedsettings'] = 'نمایش تنظیمات پیشرفته';
$string['showall'] = 'نمایش همه {$a}';
$string['showallcourses'] = 'نمایش همهٔ درس‌ها';
$string['showallusers'] = 'نمایش همهٔ کاربران';
$string['showblockcourse'] = 'نمایش لیست درس‌هایی که شامل این بلوک هستند';
$string['showcategory'] = 'نمایش {$a}';
$string['showchartdata'] = 'نمایش داده‌های نمودار';
$string['showcomments'] = 'نمایش/پنهان کردن نظرات';
$string['showcommentsnonjs'] = 'نمایش نظرات';
$string['showdescription'] = 'نمایش توضیح در صفحهٔ درس';
$string['showdescription_help'] = 'اگر انتخاب شود، متن مقدمه/توصیف نوشته شده در بالا در صفحهٔ درس و درست در پائین پیوند مربوط به این فعالیت/منبع نشان داده خواهد شد.';
$string['showgrades'] = 'نمایش دفتر نمره به شاگردان';
$string['showgrades_help'] = 'بسیاری از فعالیت‌های داخل درس اجازهٔ تعیین نمره را می‌دهند. این گزینه تعیین می‌کند که آیا یک شاگرد مجاز به دیدن لیستی از تمام نمره‌هایش در درس از طریق پیوند «نمره‌ها» در بلوک مدیریت درس هست یا خیر.';
$string['showingacourses'] = 'نمایش تمام {$a} درس';
$string['showingxofycourses'] = 'نمایش درس‌های {$a->start} تا {$a->end} از {$a->total} درس';
$string['showlistofcourses'] = 'نمایش لیست درس‌ها';
$string['showmodulecourse'] = 'نمایش لیست درس‌هایی که شامل این فعالیت هستند';
$string['showoncoursepage'] = 'نمایش در صفحهٔ درس';
$string['showonly'] = 'نمایش فقط';
$string['showperpage'] = 'نمایش {$a} مورد در هر صفحه';
$string['showpopoverwindow'] = 'نمایش پنجرهٔ ظاهرشونده';
$string['showrecent'] = 'نمایش فعالیت‌های اخیر';
$string['showreports'] = 'نمایش گزارش فعالیت‌ها';
$string['showreports_help'] = 'گزارش‌های فعالیت‌ها، فعالیت‌های هر کدام از شرکت کنندگان را در درس نشان می‌دهند. این گزارش‌ها علاوه بر لیست کردن مشارکت‌های شرکت کنندگان (مانند بیان مطالب در تالارهای گفتگو یا تحویل تکالیف)، شامل log های دسترسی نیز هستند. این تنظیم تعیین می‌کند که آیا شاگردان می‌توانند گزارش فعالیت‌های خود را از طریق صفحهٔ مشخصات فردی‌شان ببینند یا خیر.';
$string['showsettings'] = 'نمایش تنظیمات';
$string['showtheselogs'] = 'نمایش این log ها';
$string['showthishelpinlanguage'] = 'نمایش این راهنما به زبان {$a}';
$string['sidepanel'] = 'پنل کناری';
$string['signoutofotherservices'] = 'خروج از همه جا';
$string['signoutofotherservices_help'] = 'اگر انتخاب شود، حساب کاربری بر روی تمام دستگاه‌ها و سیستم‌هایی که از وب‌سرویس‌ها استفاده می‌کنند (مانند برنامهٔ موبایل مودل)  خارج خواهد شد.';
$string['since'] = 'از';
$string['sincelast'] = 'از زمان آخرین ورود';
$string['site'] = 'سایت';
$string['sitedefault'] = 'پیش‌فرض سایت';
$string['siteerrors'] = 'خطاهای سایت';
$string['sitefiles'] = 'فایل‌های سایت';
$string['sitefilesused'] = 'فایل‌های سایت که در این درس استفاده شده‌اند';
$string['sitehome'] = 'صفحهٔ اصلی سایت';
$string['sitelegacyfiles'] = 'فایل‌های موروثی سایت';
$string['sitelogs'] = 'log های سایت';
$string['sitemessage'] = 'مدیریت کاربران';
$string['sitenews'] = 'اعلانات سایت';
$string['sitepages'] = 'صفحه‌های سایت';
$string['sitepartlist'] = 'شما مجوزهای لازم برای مشاهدهٔ لیست شرکت کنندگان را ندارید';
$string['sitepartlist0'] = 'برای مشاهدهٔ لیست شرکت کنندگان سایت، باید یک استاد سایت باشید';
$string['sitepartlist1'] = 'برای مشاهدهٔ لیست شرکت کنندگان سایت، باید یک استاد باشید';
$string['sites'] = 'سایت‌ها';
$string['sitesection'] = 'دربرداشتن یک قسمت سرفصل';
$string['sitesettings'] = 'تنظیمات سایت';
$string['siteteachers'] = 'اساتید سایت';
$string['size'] = 'اندازه';
$string['sizeb'] = 'بایت';
$string['sizegb'] = 'گیگابایت';
$string['sizekb'] = 'کیلوبایت';
$string['sizemb'] = 'مگابایت';
$string['skipped'] = 'کنار گذاشته شد';
$string['skiptocategorylisting'] = 'بازگشت به لیست دسته بندی ها';
$string['skiptocoursedetails'] = 'پرش به اطلاعات مفصل درس';
$string['skiptocourselisting'] = 'بازگشت به لیست درس ها';
$string['skypeid'] = 'شناسهٔ Skype';
$string['socialheadline'] = 'تالار گفتگوی اجتماعی - جدیدترین موضوع‌ها';
$string['someallowguest'] = 'بعضی از درس‌ها ممکن است به مهمان‌ها اجازهٔ دسترسی بدهند';
$string['someerrorswerefound'] = 'بعضی از اطلاعات مورد نیاز ارائه نشده یا اشتباه وارد شده‌اند. برای مشاهدهٔ جزئیات قسمت پائین را ملاحظه نمائید.';
$string['sort'] = 'مرتب سازی';
$string['sortby'] = 'مرتب شدن بر اساس';
$string['sortbyx'] = 'مرتب شدن به صورت صعودی بر اساس «{$a}»';
$string['sortbyxreverse'] = 'مرتب شدن به صورت نزولی بر اساس «{$a}»';
$string['sorting'] = 'مرتب سازی';
$string['sourcerole'] = 'نقش مبدأ';
$string['specifyname'] = 'باید یک نام در نظر بگیرید.';
$string['standard'] = 'استاندارد';
$string['starpending'] = '([*] = در انتظار تصویب درس)';
$string['startdate'] = 'تاریخ شروع درس';
$string['startdate_help'] = 'این قسمت روز شروع هفتهٔ اول برای درسی با قالب هفتگی را تعیین می‌کند. این مقدار همچنین تعیین کنندهٔ زودترین تاریخی که وقایع ثبت شدهٔ درس برای آن تاریخ قابل دستیابی هستند می‌باشد. اگر درس بازنشانی شود و تاریخ شروع درس (در نتیجهٔ بازنشانی) تغییر کند، تمام تاریخ‌های داخل درس به همان نسبت جابه‌جا خواهند شد.';
$string['startingfrom'] = 'شروع از';
$string['startsignup'] = 'ایجاد حساب کاربری جدید';
$string['state'] = 'ایالت/استان';
$string['statistics'] = 'آمار';
$string['statisticsgraph'] = 'نمودار آماری';
$string['stats'] = 'آمار';
$string['statslogins'] = 'ورودها';
$string['statsmodedetailed'] = 'نمای مفصل (کاربر)';
$string['statsmodegeneral'] = 'نمای عمومی';
$string['statsnodata'] = 'دادهٔ کافی برای این ترکیب از درس و محدودهٔ زمانی وجود ندارد.';
$string['statsnodatauser'] = 'دادهٔ کافی برای این ترکیب از درس، کاربر و محدودهٔ زمانی وجود ندارد.';
$string['statsoff'] = 'آمارگیری در حال حاضر فعال نیست';
$string['statsreads'] = 'مشاهده‌ها';
$string['statsreport1'] = 'ورودها';
$string['statsreport10'] = 'فعالیت کاربر';
$string['statsreport11'] = 'فعال‌ترین درس‌ها';
$string['statsreport12'] = 'فعال‌ترین درس‌ها (وزن‌دار)';
$string['statsreport13'] = 'مشارکتی‌ترین درس‌ها (ثبت‌نام‌ها)';
$string['statsreport14'] = 'مشارکتی‌ترین درس‌ها (مطالعه‌ها/ارسال‌های مطالب)';
$string['statsreport2'] = 'مشاهده‌ها (همهٔ نقش‌ها)';
$string['statsreport3'] = 'ارسال‌ها (همهٔ نقش‌ها)';
$string['statsreport4'] = 'همهٔ فعالیت‌ها (همهٔ نقش‌ها)';
$string['statsreport5'] = 'همهٔ فعالیت‌ها (مشاهده‌ها و ارسال‌ها)';
$string['statsreport7'] = 'فعالیت کاربر (مشاهده‌ها و ارسال‌ها)';
$string['statsreport8'] = 'همهٔ فعالیت‌های کاربران';
$string['statsreport9'] = 'ورودها (درس سایت)';
$string['statsreportactivity'] = 'همهٔ فعالیت‌ها (همهٔ نقش‌ها)';
$string['statsreportactivitybyrole'] = 'همهٔ فعالیت‌ها (مشاهده‌ها و ارسال‌ها)';
$string['statsreportforuser'] = 'برای';
$string['statsreportlogins'] = 'ورودها';
$string['statsreportreads'] = 'مشاهده‌ها (همهٔ نقش‌ها)';
$string['statsreporttype'] = 'نوع گزارش';
$string['statsreportwrites'] = 'ارسال‌ها (همهٔ نقش‌ها)';
$string['statsstudentactivity'] = 'فعالیت شاگرد';
$string['statsstudentreads'] = 'مشاهده‌های شاگرد';
$string['statsstudentwrites'] = 'ارسال‌های شاگرد';
$string['statsteacheractivity'] = 'فعالیت استاد';
$string['statsteacherreads'] = 'مشاهده‌های استاد';
$string['statsteacherwrites'] = 'ارسال‌های استاد';
$string['statstimeperiod'] = 'محدودهٔ زمانی - آخرین:';
$string['statsuniquelogins'] = 'ورودهای یکتا';
$string['statsuseractivity'] = 'همهٔ فعالیت‌ها';
$string['statsuserlogins'] = 'ورودها';
$string['statsuserreads'] = 'مشاهده‌ها';
$string['statsuserwrites'] = 'ارسال‌ها';
$string['statswrites'] = 'ارسال‌ها';
$string['status'] = 'وضعیت';
$string['stringsnotset'] = 'عبارت‌های زیر در {$a} تعریف نشده‌اند';
$string['studentnotallowed'] = 'متأسفانه شما نمی‌توانید به‌عنوان «{$a}» وارد این درس شوید';
$string['students'] = 'شاگرد';
$string['studentsandteachers'] = 'اساتید و شاگردان';
$string['subcategories'] = 'زیرطبقه‌ها';
$string['subcategory'] = 'زیر طبقه';
$string['subcategoryof'] = 'زیرطبقه های {$a}';
$string['submit'] = 'ارسال';
$string['success'] = 'موفق';
$string['summary'] = 'خلاصه';
$string['summary_help'] = 'هدف از در نظر گرفتن یک خلاصه، نمایش یک متن کوتاه به منظور آماده کردن شاگردان برای فعالیت‌های درون بخش (موضوع یا هفته) است. این متن در صفحهٔ درس و در زیر نام بخش نشان داده می‌شود.';
$string['summaryof'] = 'خلاصهٔ {$a}';
$string['supplyinfo'] = 'جزئیات بیشتر';
$string['suspended'] = 'تعلیق شده';
$string['suspendedusers'] = 'کاربران تعلیق شده';
$string['switchdevicedefault'] = 'تغییر پوسته به پوستهٔ استاندارد';
$string['switchdevicerecommended'] = 'تغییر پوسته به پوستهٔ توصیه شده برای دستگاه شما';
$string['switchrolereturn'] = 'بازگشت به نقش عادی';
$string['switchroleto'] = 'تغییر نقش به...';
$string['switchroleto_help'] = 'یک نقش را انتخاب کنید تا ببینید کسی که دارای آن نقش است درس را چطور می‌بیند.

لطفا توجه داشته باشید که این نما ممکن است بی عیب و نقص نباشد (<a href="https://docs.moodle.org/en/Switch_roles">جزئیات و جایگزین‌ها را ببینید</a>).';
$string['tag'] = 'برچسب';
$string['tagalready'] = 'این برچسب وجود دارد';
$string['tagmanagement'] = 'اضافه کردن/حذف برچسب‌ها ...';
$string['tags'] = 'tag ها';
$string['targetrole'] = 'نقش هدف';
$string['teacheronly'] = 'فقط برای {$a}';
$string['teacherroles'] = 'نقش‌های {$a}';
$string['teachers'] = 'استاد';
$string['textediting'] = 'ویرایشگر متنی';
$string['textediting_help'] = 'اگر یک ویرایشگر HTML مانند آتو یا TinyMCE انتخاب شده باشد، ناحیه‌های ورود متن یک نوار ابزار شامل یک سری دکمه برای اضافه‌کردن محتوا به‌طور ساده خواهند داشت.

اگر «قالب سادهٔ متنی» انتخاب شده باشد، گزینه‌ای هم برای انتخاب قالب متن‌های وارد شده (مانند HTML یا Markdown) نمایش داده می‌شود.

لیست ویرایشگرهای متنی موجود توسط مدیر سایت تعیین می‌شود.';
$string['texteditor'] = 'از فرم‌های استاندارد وب استفاده شود';
$string['textformat'] = 'قالب سادهٔ متنی';
$string['thanks'] = 'متشکریم';
$string['theme'] = 'پوسته';
$string['themes'] = 'پوسته‌ها';
$string['themesaved'] = 'پوستهٔ جدید ذخیره شد';
$string['therearecourses'] = 'تعداد {$a} درس موجود است';
$string['thereareno'] = 'هیچ موردی از {$a} در این درس وجود ندارد';
$string['thiscategory'] = 'این طبقه بندی';
$string['thiscategorycontains'] = 'این طبقه شامل این موارد است:';
$string['time'] = 'زمان';
$string['timecreatedcourse'] = 'زمان درس ساخته شد';
$string['timezone'] = 'منطقهٔ زمانی';
$string['to'] = 'به';
$string['tocontent'] = 'به بخش «{$a}»';
$string['tocreatenewaccount'] = 'پرش به بخش ایجاد حساب کاربری جدید';
$string['today'] = 'امروز';
$string['todaylogs'] = 'Log های امروز';
$string['toeveryone'] = 'به همه';
$string['toomanybounces'] = 'نامه‌های زیادی از این آدرس پست الکترونیک برگشت خورده است. برای ادامه دادن <b>باید</b> آن را تغییر دهید.';
$string['toomanytags'] = 'این جستجو شامل تگ‌های زیادی است؛ برخی نادیده گرفته خواهند شد.';
$string['toomanytoshow'] = 'تعداد کاربران برای نمایش خیلی زیاد است.';
$string['toomanyusersmatchsearch'] = 'کاربران خیلی زیادی ({$a->count}) با جستجوی «{$a->search}» پیدا شدند';
$string['toomanyuserstoshow'] = 'تعداد کاربران ({$a}) برای نمایش خیلی زیاد است';
$string['toonly'] = 'فقط به {$a}';
$string['top'] = 'مبدأ';
$string['topic'] = 'موضوع';
$string['topichide'] = 'پنهان کردن این مبحث از دید {$a}';
$string['topicoutline'] = 'طرح موضوعی';
$string['topicshow'] = 'نمایش این مبحث به {$a}';
$string['toplevelcategory'] = 'بالاترین مرحله دسته بندی';
$string['total'] = 'مجموع';
$string['totopofsection'] = 'به ابتدای قسمت «{$a}»';
$string['trackforums'] = 'دنبال کردن تالارهای گفتگو';
$string['trackforumsno'] = 'خیر: حساب مطالبی که دیده‌ام را نگه ندار';
$string['trackforumsyes'] = 'بله: مطالب جدید را برایم مشخص کن';
$string['trysearching'] = 'در عوض از جستجو استفاده کنید.';
$string['turneditingoff'] = 'اتمام ویرایش';
$string['turneditingon'] = 'شروع ویرایش';
$string['unauthorisedlogin'] = 'حساب کاربری «{$a}» در این سایت موجود نیست';
$string['undecided'] = 'مردد';
$string['unfinished'] = 'ناتمام';
$string['unknowncategory'] = 'طبقهٔ نامعلوم';
$string['unknownerror'] = 'خطای نامشخص';
$string['unknownuser'] = 'کاربر نامشخص';
$string['unlimited'] = 'نامحدود';
$string['unpacking'] = 'در حال گشودن بستهٔ <span dir="ltr">{$a}</span>';
$string['unsafepassword'] = 'این رمز ورود ناامن است - رمز دیگری انتخاب نمائید';
$string['untilcomplete'] = 'تا تمام شدن کار';
$string['unusedaccounts'] = 'عضویت کاربرانی که بیش از {$a} روز به سایت مراجعه نکنند، به صورت خود کار لغو می‌شود.';
$string['unzip'] = 'خروج از فشردگی';
$string['unzippingbackup'] = 'خارج کردن پشتیبان از حالت فشرده';
$string['up'] = 'بالا';
$string['update'] = 'به‌روزرسانی';
$string['updated'] = '{$a} به‌روز شد';
$string['updatemymoodleoff'] = 'اتمام شخصی‌سازی این صفحه';
$string['updatemymoodleon'] = 'شخصی‌سازی این صفحه';
$string['updatemyprofile'] = 'به‌روزرسانی مشخصات فردی';
$string['updatesevery'] = 'هر {$a} ثانیه به‌روز می‌شود';
$string['updatethis'] = 'به‌روزرسانی این {$a}';
$string['updatethiscourse'] = 'به‌روزرسانی این درس';
$string['updatinga'] = 'به‌روزرسانی: {$a}';
$string['updatingain'] = 'به‌روزرسانی {$a->what} در {$a->in}';
$string['upload'] = 'ارسال فایل';
$string['uploadafile'] = 'قرار دادن یک فایل در سایت';
$string['uploadcantwrite'] = 'نوشتن فایل روی دیسک با مشکل مواجه شد';
$string['uploadedfile'] = 'فایل با موفقیت ارسال شد';
$string['uploadedfileto'] = '{$a->file} به {$a->directory} ارسال شد';
$string['uploadedfiletoobig'] = 'این فایل خیلی بزرگ است (حداکثر اندازهٔ مجاز {$a} بایت است)';
$string['uploadextension'] = 'ارسال فایل توسط یکی از افزونه‌های PHP متوقف شد';
$string['uploadfailednotrecovering'] = 'ارسال فایل به دلیل وجود مشکل در یکی از فایل‌ها ({$a->name}) با موفقیت انجام نشد.<br />log مشکلات بدین صورت است:<br />{$a->problem}<br />بازیابی نمی‌شود.';
$string['uploadfilelog'] = 'log ارسال برای فایل {$a}';
$string['uploadformlimit'] = 'اندازهٔ فایل ارسالی ({$a}) از حداکثر تعیین‌شده توسط فرم بیشتر است';
$string['uploadlabel'] = 'عنوان:';
$string['uploadlimitwithsize'] = 'محدودیت ارسال در {$a->contextname} ({$a->displaysize})';
$string['uploadnewfile'] = 'ارسال فایل جدید';
$string['uploadnofilefound'] = 'فایلی پیدا نشد. آیا مطمئن هستید که فایلی انتخاب کرده‌اید؟';
$string['uploadnotallowed'] = 'ارسال فایل مجاز نیست';
$string['uploadnotempdir'] = 'پوشهٔ موقت وجود ندارد';
$string['uploadoldfilesdeleted'] = 'فایل‌های قدیمی که در محل ارسال قرار داشتند حذف شدند';
$string['uploadpartialfile'] = 'فایل به صورت ناقص ارسال شد';
$string['uploadproblem'] = 'مشکل نامعلومی در حین ارسال فایل «{$a}» پیش آمد (شاید حجم آن خیلی زیاد بود)';
$string['uploadrenamedchars'] = 'به دلیل وجود حروف غیر مجاز نام فایل از {$a->oldname} به {$a->newname} تغییر داده شد.';
$string['uploadrenamedcollision'] = 'به دلیل وجود تداخل نام فایل از {$a->oldname} به {$a->newname} تغییر داده شد.';
$string['uploadserverlimit'] = 'اندازهٔ فایل ارسالی از حداکثر تعیین شده توسط کارگزار بیشتر است';
$string['uploadthisfile'] = 'ارسال این فایل';
$string['url'] = 'آدرس';
$string['used'] = 'استفاده شده';
$string['usedinnplaces'] = '{$a} جا استفاده شده';
$string['user'] = 'کاربر';
$string['useraccount'] = 'حساب کاربری';
$string['useractivity'] = 'فعالیت';
$string['userconfirmed'] = '{$a} تایید شد';
$string['userdata'] = 'داده‌های کاربران';
$string['userdeleted'] = 'این حساب کاربری حذف شده است';
$string['userdescription'] = 'توصیف';
$string['userdescription_help'] = 'در این قسمت می‌توانید متنی در مورد خود وارد کنید. این متن در صفحهٔ مشخصات فردی شما نمایش داده می‌شود و توسط سایرین قابل مشاهده خواهد بود.';
$string['userdetails'] = 'با جزئیات';
$string['userfiles'] = 'فایل‌های کاربران';
$string['userfilterplaceholder'] = 'جستجوی کلیدواژه یا انتخاب فیلتر';
$string['userlist'] = 'لیست کاربران';
$string['usermenu'] = 'منوی کاربری';
$string['username'] = 'نام کاربری';
$string['usernameemail'] = 'نام کاربری / ایمیل';
$string['usernameemailmatch'] = 'نام کاربری و پست الکترونیک مربوط به یک کاربر یکسان نیستند';
$string['usernameexists'] = 'این نام کاربری هم‌اکنون وجود دارد، یک نام کاربری دیگر انتخاب نمائید';
$string['usernamelowercase'] = 'فقط حروف کوچک مجاز هستند';
$string['usernamenotfound'] = 'نام کاربری در پایگاه داده پیدا نشد';
$string['usernameoremail'] = 'نام کاربری یا آدرس پست الکترونیک را وارد کنید';
$string['usernotconfirmed'] = '{$a} تایید نشد';
$string['userpic'] = 'عکس کاربر';
$string['users'] = 'کاربران';
$string['userselectorautoselectunique'] = 'اگر نتیجهٔ جستجو فقط یک کاربر بود، به صورت خودکار انتخاب شده باشد';
$string['userselectorpreserveselected'] = 'کاربران انتخاب شده حفظ شوند، حتی اگر دیگر جزء نتیجهٔ جستجو نباشند';
$string['userselectorsearchanywhere'] = 'متن مورد جستجو می‌تواند در هر قسمتی از فیلدهای نمایش‌داده‌شده باشد';
$string['usersnew'] = 'کاربران جدید';
$string['usersnoaccesssince'] = 'غیرفعال به مدت';
$string['userswithfiles'] = 'کاربران دارای فایل';
$string['useruploadtype'] = 'نوع ارسال کاربران: {$a}';
$string['userzones'] = 'مناطق زمانی کاربران';
$string['usetheme'] = 'استفاده از پوسته';
$string['usingexistingcourse'] = 'استفاده از درس موجود';
$string['valuealreadyused'] = 'این مقدار هم‌اکنون استفاده شده است.';
$string['version'] = 'نسخه';
$string['view'] = 'مشاهده';
$string['viewallcourses'] = 'نمایش همهٔ درس‌ها';
$string['viewallcoursescategories'] = 'نمایش همهٔ درس‌ها و طبقه‌ها';
$string['viewallsubcategories'] = 'مشاهده همه زیرطبقه‌ها';
$string['viewfileinpopup'] = 'نمایش فایل در یک پنجرهٔ pop-up';
$string['viewing'] = 'مشاهده:';
$string['viewmore'] = 'مشاهده موارد بیشتر';
$string['viewprofile'] = 'مشاهدهٔ صفحهٔ مشخصات فردی';
$string['views'] = 'مشاهده‌ها';
$string['viewsolution'] = 'مشاهدهٔ راه حل';
$string['visible'] = 'قابل مشاهده';
$string['visible_help'] = 'این گزینه تعیین می‌کند که درس در لیست درس‌ها نمایش داده شود یا خیر. به جز اساتید و مدیرها، سایر کاربران نمی‌توانند وارد درس شوند.';
$string['visibletostudents'] = 'قایل رویت توسط {$a}';
$string['warning'] = 'اخطار';
$string['warningdeleteresource'] = 'اخطار: در یک منبع به {$a} ارجاع شده است. آیا مایل به به‌روزرسانی منبع می‌باشید؟';
$string['webpage'] = 'صفحهٔ وب';
$string['week'] = 'هفته';
$string['weekhide'] = 'پنهان کردن این هفته از {$a}';
$string['weeklyoutline'] = 'طرح هفتگی';
$string['weeks'] = 'هفته';
$string['weekshow'] = 'نمایش این هفته به {$a}';
$string['welcometocourse'] = 'به {$a} خوش آمدید';
$string['welcometocoursetext'] = 'به {$a->coursename} خوش آمدید!

به منظور آشنائی بیشتر با شما، اگر قبلاً این کار را انجام نداده‌اید، باید صفحهٔ مشخصات فردی خود را ویرایش نمائید:

  {$a->profileurl}';
$string['whatforlink'] = 'با این لینک چه کاری می‌خواهید انجام دهید؟';
$string['whatforpage'] = 'با این متن چه کاری می‌خواهید انجام دهید؟';
$string['whatisyourage'] = 'سن شما چقدر است؟';
$string['whattocallzip'] = 'می‌خواهید نام این فایل فشرده چه باشد؟';
$string['whattodo'] = 'کاری که باید انجام شود';
$string['windowclosing'] = 'این پنجره باید به صورت خودکار بسته شود. اگر این طور نشد، لطفاً خودتان آن را ببندید.';
$string['withchosenfiles'] = 'فایل‌های انتخاب‌شده را';
$string['withdisablednote'] = '{$a} (غیر فعال شده)';
$string['withoutuserdata'] = 'بدون داده‌های کاربران';
$string['withselectedusers'] = 'با کاربران انتخاب شده...';
$string['withuserdata'] = 'به همراه داده‌های کاربران';
$string['wordforstudent'] = 'لغت شما برای شاگرد';
$string['wordforstudenteg'] = 'مثلاً شاگرد، دانشجو، شرکت‌کننده، ...';
$string['wordforstudents'] = 'لغت شما برای شاگردان';
$string['wordforstudentseg'] = 'مثلاً شاگردان، دانشجویان، شرکت‌کنندگان، ...';
$string['wordforteacher'] = 'لغت شما برای استاد';
$string['wordforteachereg'] = 'مثلاً استاد، معلم، کمک‌کننده، ...';
$string['wordforteachers'] = 'لغت شما برای اساتید';
$string['wordforteacherseg'] = 'مثلاً اساتید، معلمان، کمک‌کنندگان، ...';
$string['writingblogsinfo'] = 'نوشتن اطلاعات بلاگ‌ها';
$string['writingcategoriesandquestions'] = 'در حال نوشتن دسته‌ها و سؤال‌ها';
$string['writingcoursedata'] = 'در حال نوشتن داده‌های درس';
$string['writingeventsinfo'] = 'در حال نوشتن اطلاعات رویدادها';
$string['writinggeneralinfo'] = 'در حال نوشتن اطلاعات عمومی';
$string['writinggradebookinfo'] = 'در حال نوشتن اطلاعات دفتر نمره';
$string['writinggroupingsgroupsinfo'] = 'در حال نوشتن اطلاعات ابرگروه-گروه ها';
$string['writinggroupingsinfo'] = 'در حال نوشتن اطلاعات ابرگروه‌ها';
$string['writinggroupsinfo'] = 'در حال نوشتن اطلاعات گروه‌ها';
$string['writingheader'] = 'در حال نوشتن عناوین';
$string['writingloginfo'] = 'نوشتن اطلاعات log ها';
$string['writingmessagesinfo'] = 'نوشتن اطلاعات پیغام‌ها';
$string['writingmoduleinfo'] = 'در حال نوشتن اطلاعات ماژول‌ها';
$string['writingscalesinfo'] = 'در حال نوشتن اطلاعات مقیاس‌ها';
$string['writinguserinfo'] = 'در حال نوشتن اطلاعات کاربران';
$string['wrongpassword'] = 'رمز ورود نادرست برای این نام کاربری';
$string['yahooid'] = 'شناسهٔ Yahoo';
$string['year'] = 'سال';
$string['years'] = 'سال';
$string['yes'] = 'بله';
$string['youareabouttocreatezip'] = 'شما در آستانهٔ ایجاد یک فایل فشرده با محتویات زیر هستید';
$string['youaregoingtorestorefrom'] = 'شما در آستانهٔ شروع فرآیند بازیابی این فایل هستید';
$string['youneedtoenrol'] = 'برای انجام دادن این کار باید در این درس ثبت‌نام کنید.';
$string['yourlastlogin'] = 'آخرین ورود شما به سایت';
$string['yourself'] = 'خودتان';
$string['yourteacher'] = '{$a} شما';
$string['yourwordforx'] = 'لغت شما به جای «{$a}»';
$string['zippingbackup'] = 'در حال فشرده‌سازی فایل‌های پشتیبان';
