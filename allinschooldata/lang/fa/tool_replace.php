<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_replace', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_replace
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['cannotfit'] = 'عبارت جایگزین طولانی‌تر از عبارت اصلی است و کوتاه‌سازی مجاز نیست. نمی‌توان ادامه داد.';
$string['disclaimer'] = 'من متوجه خطرات این عمل هستم';
$string['doit'] = 'بله، انجام شود!';
$string['excludedtables'] = 'چند جدول هستند که این جایگزینی روی آنها اعمال نخواهد شد. این جدول‌ها شامل configuration و log و events و session می‌شوند.';
$string['invalidcharacter'] = 'کاراکترهای نامعتبری در بین متن جستجو یا جایگزینی پیدا شد.';
$string['notifyfinished'] = '... به پایان رسید';
$string['notifyrebuilding'] = 'ساختن دوبارهٔ cache درس...';
$string['notimplemented'] = 'متاسفیم این قابلیت در راه‌انداز پایگاه دادهٔ شما پیاده‌سازی نشده است.';
$string['notsupported'] = 'این اسکریپت باید به‌عنوان یک اسکریپت آزمایشی درنظر گرفته شود. تغییراتی که داده می‌شوند قابل بازگشت نیستند، بنابراین پیش از اجرای این اسکریپت باید یک پشتیبان کامل تهیه شود.';
$string['pageheader'] = 'جستجو و جایگزینی متن در سرتاسر پایگاه داده';
$string['pluginname'] = 'جستجو و جایگزینی در پایگاه داده';
$string['replacewith'] = 'جایگزین شدن با این عبارت';
$string['replacewithhelp'] = 'معمولا آدرس اینترنتی جدید کارگزار';
$string['searchwholedb'] = 'جستجوی کل پایگاه داده برای';
$string['searchwholedbhelp'] = 'معمولا آدرس اینترنتی قبلی کارگزار';
$string['shortenoversized'] = 'کوتاه‌کردن نتیجه در صورت لزوم';
