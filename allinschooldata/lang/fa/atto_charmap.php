<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_charmap', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_charmap
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aacute'] = 'aیِ تیز';
$string['aacute_caps'] = 'Aیِ تیز';
$string['acircumflex'] = 'aیِ هشتک';
$string['acircumflex_caps'] = 'Aیِ هشتک';
$string['acuteaccent'] = 'اکسان اگو';
$string['adiaeresis'] = 'a اوملاوت';
$string['adiaeresis_caps'] = 'A اوملاوت';
$string['agrave'] = 'aیِ بَم';
$string['agrave_caps'] = 'Aیِ بَم';
$string['almostequalto'] = 'تقریبا برابر';
$string['alpha'] = 'آلفا';
$string['alpha_caps'] = 'آلفا';
$string['amacron'] = 'a ماکرون';
$string['amacron_caps'] = 'A ماکرون';
$string['ampersand'] = 'امپرسند';
$string['beta'] = 'بتا';
$string['beta_caps'] = 'بتا';
$string['centsign'] = 'علامت سنت';
$string['copyrightsign'] = 'نشان کپی‌رایت';
$string['currencysign'] = 'نشان واحد پول';
$string['dagger'] = 'خنجر';
$string['degreesign'] = 'علامت درجه';
$string['delta'] = 'دلتا';
$string['delta_caps'] = 'دلتا';
$string['diaeresis'] = 'دونقطه';
$string['divisionsign'] = 'علامت تقسیم';
$string['dotoperator'] = 'عملگر نقطه';
$string['doubledagger'] = 'خنجر دوتایی';
$string['downwardsarrow'] = 'فلش سمت پایین';
$string['epsilon'] = 'اپسیلون';
$string['epsilon_caps'] = 'اپسیلون';
$string['eurosign'] = 'علامت یورو';
$string['fractiononehalf'] = 'کسرِ یک دوم';
$string['fractiononequarter'] = 'کسرِ یک چهارم';
$string['fractionslash'] = 'خط کسری مورب';
$string['fractionthreequarters'] = 'کسرِ سه چهارم';
$string['functionflorin'] = 'تابع / فلورین';
$string['gamma'] = 'گاما';
$string['gamma_caps'] = 'گاما';
$string['greaterthanorequalto'] = 'بزرگتر-مساوی';
$string['greaterthansign'] = 'علامت بزرگ‌تر';
$string['infinity'] = 'بی‌نهایت';
$string['insertcharacter'] = 'درج کاراکتر';
$string['integral'] = 'انتگرال';
$string['kappa'] = 'کاپا';
$string['kappa_caps'] = 'کاپا';
$string['lambda'] = 'لاندا';
$string['lambda_caps'] = 'لاندا';
$string['leftpointingguillemet'] = 'گیومه باز';
$string['leftwardsarrow'] = 'فلش سمت چپ';
$string['lessthansign'] = 'علامت کوچک‌تر';
$string['lozenge'] = 'لوزی';
$string['microsign'] = 'علامت میکرو';
$string['minussign'] = 'علامت منفی';
$string['minutesfeet'] = 'دقیقه / فوت';
$string['mu'] = 'مو';
$string['mu_caps'] = 'مو';
$string['multiplicationsign'] = 'علامت ضرب';
$string['notequalto'] = 'نابرابر';
$string['nu'] = 'نو';
$string['nu_caps'] = 'نو';
$string['omega'] = 'اُمگا';
$string['omega_caps'] = 'اُمگا';
$string['omicron'] = 'اُمیکرون';
$string['omicron_caps'] = 'اُمیکرون';
$string['paragraphsign'] = 'علامت پاراگراف';
$string['permillesign'] = 'علامتِ در هزار';
$string['phi'] = 'فی';
$string['phi_caps'] = 'فی';
$string['pi'] = 'پی';
$string['pi_caps'] = 'پی';
$string['pisymbol'] = 'علامت پی';
$string['pluginname'] = 'درج کاراکتر';
$string['plusminussign'] = 'علامت به‌علاوه-منفی';
$string['poundsign'] = 'علامت پوند';
$string['privacy:metadata'] = 'پلاگین atto_charmap هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['psi'] = 'پسی';
$string['psi_caps'] = 'پسی';
$string['quotationmark'] = 'علامت نقل‌قول';
$string['registeredsign'] = 'نشان ثبت';
$string['rho'] = 'رو';
$string['rho_caps'] = 'رو';
$string['rightpointingguillemet'] = 'گیومه بسته';
$string['righttoleftmark'] = 'علامت راست-به-چپ';
$string['rightwardsarrow'] = 'فلش سمت راست';
$string['secondsinches'] = 'ثانیه / اینچ';
$string['sigma'] = 'سیگما';
$string['sigma_caps'] = 'سیگما';
$string['squareroot'] = 'ریشهٔ دوم';
$string['superscriptone'] = '1 بالانویس';
$string['superscriptthree'] = '3 بالانویس';
$string['superscripttwo'] = '2 بالانویس';
$string['tau'] = 'تاو';
$string['tau_caps'] = 'تاو';
$string['theta'] = 'تتا';
$string['theta_caps'] = 'تتا';
$string['thorn'] = 'تورن';
$string['thorn_caps'] = 'تورن';
$string['threedotleader'] = 'خط سه‌نقطه';
$string['trademarksign'] = 'نشان علامت تجاری';
$string['turnedquestionmark'] = 'علامت سؤال وارونه';
$string['upsilon'] = 'اوپسیلون';
$string['upsilon_caps'] = 'اوپسیلون';
$string['upwardsarrow'] = 'فلش سمت بالا';
$string['xi'] = 'کسی';
$string['xi_caps'] = 'کسی';
$string['yensign'] = 'علامت ین';
$string['zerowidthjoiner'] = 'اتصال مجازی';
$string['zerowidthnonjoiner'] = 'فاصلهٔ مجازی';
$string['zeta'] = 'زتا';
$string['zeta_caps'] = 'زتا';
