<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_eventlist', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   report_eventlist
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['affectedtable'] = 'جدول تاثیر پذیرفته';
$string['all'] = 'همه';
$string['clear'] = 'پاک‌کردن فرم';
$string['component'] = 'کامپوننت';
$string['create'] = 'ساختن';
$string['crud'] = 'نوع کوئری پایگاه داده';
$string['delete'] = 'حذف';
$string['details'] = 'جزئیات';
$string['dname'] = 'نام';
$string['edulevel'] = 'سطح آموزشی';
$string['errorinvaliddirectory'] = 'دایرکتوری رویداد وجود ندارد یا نامعتبر است.';
$string['errorinvalidevent'] = 'رویداد ارائه شده یک رویداد معتبر نیست.';
$string['eventcode'] = 'کد رویداد';
$string['eventexplanation'] = 'توضیح رویداد';
$string['eventname'] = 'نام رویداد';
$string['filter'] = 'فیلتر';
$string['legacyevent'] = 'رویداد قدیمی';
$string['name'] = 'نام';
$string['other'] = 'سایر';
$string['otherinformation'] = 'سایر اطلاعات:';
$string['parentevent'] = 'رویداد والد';
$string['participating'] = 'شرکت کردن';
$string['pluginname'] = 'لیست رویدادها';
$string['read'] = 'خواندن';
$string['relatedobservers'] = 'پلاگین‌هایی که این رویداد را نظارت می‌کنند';
$string['since'] = 'از';
$string['teaching'] = 'تدریس';
$string['update'] = 'به‌روز کردن';
