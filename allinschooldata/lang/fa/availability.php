<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'availability', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   availability
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accessrestrictions'] = 'محدودیت‌های دسترسی';
$string['addrestriction'] = 'اضافه کردن محدودیت...';
$string['and'] = 'و';
$string['condition_group'] = 'مجموعه شرایط';
$string['condition_group_info'] = 'اضافه‌کردن مجموعه‌ای از محدودیت‌های تودرتو برای اعمال منطق‌های پیچیده.';
$string['enableavailability'] = 'فعال بودن دسترسی محدود';
$string['enableavailability_desc'] = 'درصورت فعال بودن، می‌توانید شرایطی (بر اساس تاریخ، نمره، یا تکمیل) برای کنترل در دسترس بودن یا نبودن هر یک از فعالیت‌ها یا منابع تعیین کنید.';
$string['error_list_nochildren'] = 'محموعه شرایط باید شامل حداقل یک شرط باشد.';
$string['hidden_all'] = 'اگر کاربر شرایط را احراز نکند به‌طور کامل پنهان است';
$string['hidden_individual'] = 'اگر کاربر این شرط را احراز نکند به‌طور کامل پنهان است';
$string['hidden_marker'] = '(در غیر این صورت پنهان است)';
$string['hide_verb'] = 'برای پنهان‌شدن کلیک کنید';
$string['invalid'] = 'لطفا تعیین کنید';
$string['itemheading'] = '{$a->number} محدودیت {$a->type}';
$string['item_unknowntype'] = 'این محدودیت‌ها از پلاگینی استفاده می‌کنند که دیگر موجود نیست (اگر حذف کردن آن محدودیت اشکالی ندارد، آن را پاک کنید)';
$string['label_multi'] = 'محدودیت‌های لازم';
$string['label_sign'] = 'نوع محدودیت';
$string['list_and'] = 'همهٔ:';
$string['list_and_hidden'] = 'همهٔ (در غیر این صورت پنهان است):';
$string['listheader_multi_after'] = 'موارد زیر شود';
$string['listheader_multi_and'] = 'تمام';
$string['listheader_multi_before'] = 'حائز';
$string['listheader_multi_or'] = 'هرکدام از';
$string['listheader_sign_before'] = 'شاگرد';
$string['listheader_sign_neg'] = 'نباید';
$string['listheader_sign_pos'] = 'باید';
$string['listheader_single'] = 'حائز موارد زیر شود';
$string['list_or'] = 'هرکدام از:';
$string['list_or_hidden'] = 'هرکدام از (در غیر این صورت پنهان است):';
$string['list_root_and'] = 'قابل دسترسی نیست مگر اینکه:';
$string['list_root_and_hidden'] = 'قابل دسترسی نیست (پنهان است) مگر اینکه:';
$string['list_root_or'] = 'قابل دسترسی نیست مگر اینکه یکی از این شرایط برقرار باشد:';
$string['list_root_or_hidden'] = 'قابل دسترسی نیست (پنهان است) مگر اینکه یکی از این شرایط برقرار باشد:';
$string['manageplugins'] = 'مدیریت محدودیت‌ها';
$string['missingplugin'] = 'پلاگین محدودیت حذف‌شده';
$string['or'] = 'یا';
$string['privacy:metadata'] = 'زیرسیستم محدودیت دسترسی هیچ اطلاعات شخصی‌ای ذخیره نمی‌کند.';
$string['restrictaccess'] = 'محدودکردن دسترسی';
$string['restrictbygroup'] = 'اضافه‌کردن محدودیت دسترسی گروه/ابرگروه';
$string['setheading'] = '{$a->number} مجموعه شامل {$a->count} محدودیت';
$string['shown_all'] = 'اگر کاربر شرایط را احراز نکند به‌فرم غیرفعال (خاکستری) نشان داده می‌شود';
$string['shown_individual'] = 'اگر کاربر این شرط را احراز نکند به‌فرم غیرفعال (خاکستری) نشان داده می‌شود';
$string['show_verb'] = 'برای نمایش‌داده‌شدن کلیک کنید';
$string['unknowncondition'] = 'شرط نامعلوم (پلاگین شرط حذف‌شده)';
