<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_customlang', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_customlang
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['checkin'] = 'ذخیرهٔ تغییرات در بستهٔ زبانی';
$string['checkout'] = 'باز کردن بستهٔ زبانی برای ویرایش';
$string['checkoutdone'] = 'بستهٔ زبانی بارگیری شد';
$string['checkoutinprogress'] = 'بارگیری بستهٔ زبانی';
$string['confirmcheckin'] = 'شما در آستانهٔ ذخیرهٔ تغییرات در بستهٔ زبانی محلی خود هستید. این کار موجب صادر شدن عبارت‌های سفارشی شده از مترجم به دایرکتوری دادهٔ مودل شما خواهد شد و مودل شروع به استفاده از عبارت‌های جدید خواهد کرد. برای شروع فرآیند ذخیره شدن دکمهٔ «ادامه» را فشار دهید.';
$string['customlang:edit'] = 'ویرایش ترجمهٔ محلی';
$string['customlang:view'] = 'مشاهدهٔ ترجمهٔ محلی';
$string['filter'] = 'فیلتر کردن عبارات';
$string['filtercomponent'] = 'نمایش عبارت‌های این کامپوننت‌ها';
$string['filtercustomized'] = 'فقط سفارشی شده';
$string['filtermodified'] = 'فقط تغییر کرده';
$string['filteronlyhelps'] = 'فقط راهنما';
$string['filtershowstrings'] = 'نمایش عبارت‌ها';
$string['filterstringid'] = 'شناسهٔ عبارت';
$string['filtersubstring'] = 'فقط عبارت‌های شامل';
$string['headingcomponent'] = 'کامپوننت';
$string['headinglocal'] = 'متن سفارشی شده';
$string['headingstandard'] = 'متن استاندارد';
$string['headingstringid'] = 'شناسهٔ عبارت';
$string['markinguptodate_help'] = 'در صورتی‌که متن اصلی انگلیسی و یا ترجمهٔ رسمی ارائه‌شده توسط فودل تغییر کند، آنگاه ممکن است ترجمهٔ سفارشی شما قدیمی و از رده خارج شود. ترجمهٔ سفارشی خود را مرور کنید. اگر فکر می‌کند به‌روز است، مربع را علامت بزنید. در غیر این‌صورت متن ترجمه را ویرایش کنید.';
$string['markuptodate'] = 'به عنوان به‌روز شناخته شود';
$string['modifiedno'] = 'هیچ عبارت تغییر کرده‌ای برای ذخیره وجود ندارد.';
$string['modifiednum'] = '{$a} عبارت تغییر کرده وجود دارد. آیا می‌خواهید این تغییرات را در بستهٔ زبانی محلی خود ذخیره کنید؟';
$string['nostringsfound'] = 'عبارتی پیدا نشد، لطفاً تنظیمات فیلتر را تغییر دهید';
$string['placeholder'] = 'متغیرها';
$string['placeholder_help'] = 'متغیرها عبارت‌های خاصی مانند <span dir="ltr" style="display:inline-block;direction:ltr">`{$a}`</span> یا <span dir="ltr" style="display:inline-block;direction:ltr">`{$a->something}`</span> در درون متن‌ها هستند. به هنگام نمایش، آنها با مقداری دیگر جایگزین می‌شوند.

مهم است که آنها را دقیقاً همانگونه که در متن استاندارد هستند کپی کنید. نه آنها را ترجمه کنید و نه جهت چپ–به–راست آنها تغییر دهید.';
$string['placeholderwarning'] = 'عبارت شامل متغیر است';
$string['pluginname'] = 'سفارشی‌کردن زبان';
$string['savecheckin'] = 'ذخیرهٔ تغییرات در بستهٔ زبانی';
$string['savecontinue'] = 'اعمال تغییرات و ادامهٔ ویرایش';
