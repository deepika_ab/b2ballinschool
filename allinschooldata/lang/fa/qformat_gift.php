<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qformat_gift', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qformat_gift
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['braceerror'] = 'پیدا کردن علامت‌های آکولاد باز و بستهٔ دور پاسخ‌ها مقدور نبود';
$string['giftleftbraceerror'] = 'علامت آکولاد باز پیدا نشد';
$string['giftmatchingformat'] = 'قالب‌بندی پاسخ‌های سؤال جور کردنی نامناسب است';
$string['giftnonumericalanswers'] = 'هیچ پاسخی برای سؤال عددی پیدا نشد';
$string['giftnovalidquestion'] = 'هیچ سؤال معتبری پیدا نشد';
$string['giftqtypenotset'] = 'نوع سؤال تعیین نشده است';
$string['giftrightbraceerror'] = 'علامت آکولاد بسته پیدا نشد';
$string['importminerror'] = 'خطایی در سؤال وجود دارد. تعداد پاسخ‌ها برای این نوع سؤال کافی نیست.';
$string['nohandler'] = 'کنترل کننده‌ای برای نوع سؤال {$a} موجود نیست';
$string['pluginname'] = 'قالب Gift';
$string['pluginname_help'] = 'با استفاده از قالب Gift می‌توان سؤال‌های چند گزینه‌ای، صحیح/غلط، کوتاه جواب، جور کردنی، جای خالی، عددی و تشریحی را توسط فایل متنی وارد یا صادر کرد.';
