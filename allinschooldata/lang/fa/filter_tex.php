<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filter_tex', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   filter_tex
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configconvertformat'] = 'اگر علاوه بر <i>latex</i> و <i>dvips</i>، بسته‌های <i>convert</i> یا <i>dvisvgm</i> هم موجود هستند، نوع تصویر مرجح را انتخاب کنید. (<i>convert</i> تصاویر GIF یا PNG تولید می‌کند؛ <i>dvisvgm</i> تصاویر SVG تولید می‌کند). در غیر اینصورت از <i>mimeTeX</i> استفاده شده و تصاویر GIF تولید خواهد شد.';
$string['convertformat'] = 'قالب تصویر خروجی';
$string['filtername'] = 'علائم TeX';
$string['latexpreamble'] = 'مقدمهٔ LaTeX';
$string['latexsettings'] = '';
$string['pathconvert'] = 'مسیر فایل اجرایی <i>convert</i>';
$string['pathdvips'] = 'مسیر فایل اجرایی <i>dvips</i>';
$string['pathlatex'] = 'مسیر فایل اجرایی <i>latex</i>';
