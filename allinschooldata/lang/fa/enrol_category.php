<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_category', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   enrol_category
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['category:config'] = 'پیکربندی نمونه‌های ثبت‌نام در طبقه';
$string['category:synchronised'] = 'نقش نسبت‌داده‌شده در طبقه به سمت ثبت‌نام در درس‌های طبقه همگام شود';
$string['pluginname'] = 'ثبت نام در طبقه';
$string['pluginname_desc'] = 'پلاگین ثبت‌نام در طبقه، روشی سنتی برای ثبت‌نام کاربران در سطح طبقهٔ درسی با استفاده از انتساب نقش است. توصیه می‌شود که به‌جای استفاده از این پلاگین از همگام‌سازی هم‌دوره‌ای‌ها استفاده شود.';
