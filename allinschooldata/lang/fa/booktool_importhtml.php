<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'booktool_importhtml', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   booktool_importhtml
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['doimport'] = 'وارد کردن';
$string['import'] = 'وارد کردن فصل';
$string['importhtml:import'] = 'وارد کردن فصل‌ها';
$string['type'] = 'نوع';
$string['typeonefile'] = 'یک فایل HTML با تیترهایی برای فصل‌ها';
$string['typezipdirs'] = 'هر پوشه بیانگر یک فصل است';
$string['typezipfiles'] = 'هر فایل HTML بیانگر یک فصل است';
$string['ziparchive'] = 'فایل zip';
$string['ziparchive_help'] = 'یک فایل zip که شامل فایل‌های HTML و به‌طور اختیاری شامل فایل‌های چندرسانه‌ای و پوشه‌ها است را انتخاب کنید. برای بارگذاری زیرفصل‌ها، «<span dir="ltr" style="direction:ltr;display:inline-block;">_sub</span>» را به انتهای نام فایل‌های HTML یا پوشه‌ها اضافه کنید.';
