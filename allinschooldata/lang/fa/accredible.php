<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'accredible', language 'fa', branch 'MOODLE_37_STABLE'
 *
 * @package   accredible
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accredible:addinstance'] = 'اضافه کردن یک نمونه گواهی / نشان';
$string['accredible:manage'] = 'مدیریت یک گواهی / نشان';
$string['accredible:student'] = 'بازیابی یک گواهی یا نشان';
$string['accredible:view'] = 'دیدن یک گواهی یا نشان';
$string['activityname'] = 'نام فعالیت';
$string['apikeylabel'] = 'کلید API';
$string['certificatename'] = 'نام گواهینامه/مدال';
$string['chooseexam'] = 'آزمون نهایی را انتخاب نمایید';
$string['datecreated'] = 'تاریخ ایجاد شده';
$string['description'] = 'توضیحات';
$string['groupselect'] = 'گروه';
$string['recipient'] = 'گیرنده';
