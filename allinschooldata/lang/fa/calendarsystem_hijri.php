<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'calendarsystem_hijri', language 'fa', branch 'MOODLE_26_STABLE'
 *
 * @package   calendarsystem_hijri
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['am'] = 'صبح';
$string['am_caps'] = 'صبح';
$string['month1'] = 'محرم';
$string['month10'] = 'شوال';
$string['month11'] = 'ذیقعده';
$string['month12'] = 'ذیحجه';
$string['month2'] = 'صفر';
$string['month3'] = 'ربیع‌الاول';
$string['month4'] = 'ربیع‌الثانی';
$string['month5'] = 'جمادی‌الاول';
$string['month6'] = 'جمادی‌الثانی';
$string['month7'] = 'رجب';
$string['month8'] = 'شعبان';
$string['month9'] = 'رمضان';
$string['name'] = 'هجری قمری';
$string['pluginname'] = 'تقویم هجری قمری';
$string['pm'] = 'عصر';
$string['pm_caps'] = 'عصر';
$string['weekday0'] = 'یکشنبه';
$string['weekday1'] = 'دوشنبه';
$string['weekday2'] = 'سه‌شنبه';
$string['weekday3'] = 'چهارشنبه';
$string['weekday4'] = 'پنج‌شنبه';
$string['weekday5'] = 'جمعه';
$string['weekday6'] = 'شنبه';
