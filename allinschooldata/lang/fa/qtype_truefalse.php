<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_truefalse', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_truefalse
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['correctanswer'] = 'جواب صحیح';
$string['correctanswerfalse'] = 'پاسخ درست گزینهٔ «غلط» است.';
$string['correctanswertrue'] = 'پاسخ درست گزینهٔ «صحیح» است.';
$string['false'] = 'غلط';
$string['feedbackfalse'] = 'توضیح دریافتی در صورت انتخاب گزینهٔ «غلط»';
$string['feedbacktrue'] = 'توضیح دریافتی در صورت انتخاب گزینهٔ «صحیح»';
$string['pluginname'] = 'صحیح/غلط';
$string['pluginnameadding'] = 'تعریف یک سؤال صحیح/غلط';
$string['pluginnameediting'] = 'ویرایش یک سؤال صحیح/غلط';
$string['pluginname_help'] = 'در پاسخ به یک سؤال (که می‌تواند شامل عکس هم باشد)، می‌توان گزینه‌های «صحیح» یا «غلط» را انتخاب کرد.';
$string['pluginnamesummary'] = 'حالت ساده‌ای از سؤال چند گزینه‌ای که فقط دارای دو گزینه با عنوان‌های «صحیح» و «غلط» است.';
$string['selectone'] = 'یک گزینه را انتخاب کنید:';
$string['true'] = 'صحیح';
