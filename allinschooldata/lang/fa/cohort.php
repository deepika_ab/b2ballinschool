<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'cohort', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   cohort
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcohort'] = 'اضافه کردن یک گروه جدید هم‌دوره‌ای‌ها';
$string['allcohorts'] = 'تمام هم‌دوره‌ای‌ها';
$string['anycohort'] = 'هر چه';
$string['assign'] = 'ویرایش اعضا';
$string['assigncohorts'] = 'انتساب اعضای هم‌دوره‌ای';
$string['assignto'] = 'اعضای هم‌دوره‌ای «{$a}»';
$string['backtocohorts'] = 'بازگشت به لیست گروه‌ها';
$string['bulkadd'] = 'اضافه کردن به گروه هم‌دوره‌ای‌ها';
$string['bulknocohort'] = 'هیچ گروه هم‌دوره‌ای موجودی پیدا نشد';
$string['categorynotfound'] = 'طبقهٔ <b>{$a}</b> پیدا نشد یا اینکه شما مجوز ساختن هم‌دوره‌ای در آنجا را ندارید. زمینهٔ پیش‌فرض استفاده خواهد شد.';
$string['cohort'] = 'گروه هم‌دوره‌ای‌ها';
$string['cohorts'] = 'هم‌دوره‌ای‌ها';
$string['cohortsin'] = '{$a}: گروه‌های هم‌دوره‌ای موجود';
$string['component'] = 'مبدأ';
$string['contextnotfound'] = 'زمینهٔ <b>{$a}</b> پیدا نشد یا اینکه شما مجوز ساختن هم‌دوره‌ای در آنجا را ندارید. زمینهٔ پیش‌فرض استفاده خواهد شد.';
$string['csvcontainserrors'] = 'در داده‌های CSV خطاهایی پیدا شد. جزئیات را در زیر ببینید.';
$string['csvcontainswarnings'] = 'در داده‌های CSV اخطارهایی پیدا شد. جزئیات را در زیر ببینید.';
$string['csvextracolumns'] = 'ستون(های) <b>{$a}</b> نادیده گرفته خواهد/خواهند شد.';
$string['currentusers'] = 'کاربران فعلی';
$string['currentusersmatching'] = 'کاربران فعلی منطبق';
$string['defaultcontext'] = 'زمینهٔ پیش‌فرض';
$string['delcohort'] = 'حذف گروه';
$string['delconfirm'] = 'آیا واقعاً می‌خواهید گروه «{$a}» را حذف کنید؟';
$string['description'] = 'شرح';
$string['displayedrows'] = '{$a->displayed} سطر از کل {$a->total} سطر نمایش داده شده‌اند.';
$string['duplicateidnumber'] = 'یک گروه از هم‌دوره‌ای‌ها با شناسهٔ یکسان وجود دارد';
$string['editcohort'] = 'ویرایش هم‌دوره‌ای‌ها';
$string['editcohortidnumber'] = 'ویرایش شناسهٔ گروه هم‌دوره‌ای‌ها';
$string['editcohortname'] = 'ویرایش نام گروه هم‌دوره‌ای‌ها';
$string['eventcohortcreated'] = 'هم‌دوره‌ای ساخته شد';
$string['eventcohortdeleted'] = 'هم‌دوره‌ای حذف شد';
$string['eventcohortmemberadded'] = 'کاربر به یک گروه هم‌دوره‌ای‌ها اضافه شد';
$string['eventcohortmemberremoved'] = 'کاربر از یک گروه هم‌دوره‌ای‌ها حذف شد';
$string['eventcohortupdated'] = 'هم‌دوره‌ای به‌روز شد';
$string['external'] = 'هم‌دوره‌ای خارجی';
$string['idnumber'] = 'شناسهٔ گروه';
$string['memberscount'] = 'تعداد اعضا';
$string['name'] = 'نام';
$string['namecolumnmissing'] = 'قالب فایل CSV مشکل دارد. لطفا بررسی کنید که شامل نام ستون‌ها باشد.';
$string['namefieldempty'] = 'نام فیلد نمی‌تواند خالی باشد';
$string['newidnumberfor'] = 'کد شناسایی جدید برای هم‌دوره‌ای‌های {$a}';
$string['newnamefor'] = 'نام جدید هم‌دوره‌ای‌های {$a}';
$string['nocomponent'] = 'ایجاد به صورت دستی';
$string['potusers'] = 'کاربران بالقوه';
$string['potusersmatching'] = 'کاربران بالقوهٔ منطبق';
$string['preview'] = 'پیش‌نمایش';
$string['removeuserwarning'] = 'حذف کاربران از هم‌دوره‌ای ممکن است باعث لغو ثبت‌نام آنها از برخی دروس و به‌طبع آن منجر به حذف تنظیمات، نمرات، عضویت گروه‌ها و سایر اطلاعات کاربری آن کاربران از درس‌های تحت تأثیر قرار گرفته شود.';
$string['search'] = 'جستجو';
$string['searchcohort'] = 'جستجوی هم‌دوره‌ای';
$string['selectfromcohort'] = 'انتخاب اعضا از این گروه هم‌دوره‌ای‌ها';
$string['systemcohorts'] = 'هم‌دوره‌ای‌های سیستم';
$string['unknowncohort'] = 'هم‌دوره‌ای ناشناخته ({$a})!';
$string['uploadcohorts'] = 'ارسال فایل هم‌دوره‌ای‌ها';
$string['uploadcohorts_help'] = 'هم‌دوره‌ای‌ها را می‌توان از طریق یک فایل متنی در سایت وارد کرد. قالب فایل باید به‌این‌صورت باشد:

* هر خط از فایل شامل یک رکورد است
* هر رکورد دنباله‌ای از داده‌ها است که با کاما (ویرگول انگلیسی یا ,) یا یک جداکنندهٔ دیگر از هم جدا شده‌اند
* سطر اول شامل لیستی از نام فیلدها است و قالب‌بندی ادامه فایل را تعیین می‌کند
* name یک نام فیلد اجباری است
* فیلدهای اختیاری عبارتند از idnumber و description و descriptionformat و visible و context و category و category_id و category_idnumber و category_path';
$string['uploadedcohorts'] = '{$a} هم‌دوره‌ای بارگذاری شد';
$string['useradded'] = 'کاربر به گروه هم‌دوره‌ای‌های «{$a}» اضافه شد';
$string['visible'] = 'قابل مشاهده';
$string['visible_help'] = 'کاربرانی که مجوز «moodle/cohort:view» را روی هم‌دوره‌ای داشته باشند می‌توانند تمام هم‌دوره‌ای‌ها را ببینند.<br/> هم‌دوره‌ای‌های قابل مشاهده توسط کاربران درس‌های زیرین هم قابل دیدن هستند.';
