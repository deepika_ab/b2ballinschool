<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quiz_statistics', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   quiz_statistics
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allattempts'] = 'تمام دفعات تلاش';
$string['allattemptsavg'] = 'میانگین نمرهٔ تمام تلاش‌ها';
$string['allattemptscount'] = 'تعداد کل تلاش‌های کاملا نمره‌داده‌شده';
$string['attempts'] = 'تلاش‌ها';
$string['calculatefrom'] = 'محاسبهٔ آمار از روی';
$string['cic'] = 'ضریب همسانی درونی (برای {$a})';
$string['coursename'] = 'نام درس';
$string['discrimination_index'] = 'شاخص افتراق';
$string['discriminative_efficiency'] = 'کارایی افتراق';
$string['downloadeverything'] = 'دریافت گزارش کامل در قالب';
$string['effective_weight'] = 'وزن مؤثر';
$string['errorratio'] = 'نسبت خطا (برای {$a})';
$string['facility'] = 'شاخص سادگی';
$string['firstattempts'] = 'اولین دفعهٔ شرکت';
$string['firstattemptsavg'] = 'میانگین نمرهٔ اولین تلاش‌ها';
$string['firstattemptscount'] = 'تعداد اولین تلاش‌های کاملا نمره‌داده‌شده';
$string['highestattempts'] = 'تلاشی که بیشترین نمره را دارد';
$string['highestattemptsavg'] = 'میانگین نمرهٔ تلاش‌هایی که بالاترین نمرهٔ کاربران است';
$string['intended_weight'] = 'وزن مورد نظر';
$string['kurtosis'] = 'کشیدگی توزیع نمره (برای {$a})';
$string['lastattempts'] = 'آخرین تلاش';
$string['lastattemptsavg'] = 'میانگین نمرهٔ آخرین تلاش‌ها';
$string['lastcalculated'] = 'آخرین بار {$a->lastcalculated} پیش محاسبه شده است. از آن لحظه {$a->count} تلاش دیکر صورت گرفته است.';
$string['median'] = 'نمرهٔ میانه (برای {$a})';
$string['nogradedattempts'] = 'هیچ تلاشی در این آزمون ضورت نگرفته است، یا اینکه تمام تلاش‌ها دارای سؤال‌هایی هستند که نیازمند نمره‌دهی دستی هستند.';
$string['pluginname'] = 'آمار';
$string['questionname'] = 'نام سؤال';
$string['questionnumber'] = '#س';
$string['quizinformation'] = 'اطلاعات آزمون';
$string['quizname'] = 'نام آزمون';
$string['quizoverallstatistics'] = 'آمارهای وضعیت نهایی آزمون';
$string['quizstructureanalysis'] = 'تحلیل ساختار آزمون';
$string['random_guess_score'] = 'امتیاز حدس تصادفی';
$string['recalculatenow'] = 'محاسبهٔ مجدد';
$string['reportsettings'] = 'تنظیمات محاسبهٔ آمار';
$string['skewness'] = 'چولگی توزیع نمره (برای {$a})';
$string['standarddeviation'] = 'انحراف معیار (برای {$a})';
$string['standarddeviationq'] = 'انحراف معیار';
$string['standarderror'] = 'خطای استاندارد (برای {$a})';
$string['statisticsreport'] = 'گزارش آماری';
$string['statisticsreportgraph'] = 'آمار سؤال برحسب شماره';
$string['statistics:view'] = 'دیدن گزارش آمار';
$string['statsfor'] = 'آمارهای آزمون (برای {$a})';
