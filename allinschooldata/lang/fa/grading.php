<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'grading', language 'fa', branch 'MOODLE_38_STABLE'
 *
 * @package   grading
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['changeactivemethod'] = 'تغییر شیوهٔ نمره‌دهی فعال به';
$string['clicktoclose'] = 'کلیک کنید تا بسته شود';
$string['gradingformunavailable'] = 'لطفا توجه فرمایید: فرم نمره دهی پیشرفته هنوز آماده نیست. تا زمانی که این فرم وضعیت معتبری پیدا کند، از شیوه نمره دهی ساده استفاده خواهد شد.';
$string['gradingmanagement'] = 'نمره‌دهی پیشرفته';
$string['gradingmanagementtitle'] = 'نمره‌دهی پیشرفته: {$a->component} ({$a->area})';
$string['gradingmethod'] = 'روش نمره‌دهی';
$string['gradingmethod_help'] = 'روش نمره‌دهی پیشرفته‌ای که باید برای محاسبهٔ نمره‌ها در زمینهٔ داده شده استفاده شود انتخاب کنید.

برای غیر فعال کردن نمره‌دهی پیشرفته و بازگشت به شیوهٔ نمره‌دهی پیش‌فرض، گزینهٔ «نمره‌دهی مستقیم ساده» را انتخاب کنید.';
$string['gradingmethodnone'] = 'نمره‌دهی مستقیم ساده';
$string['gradingmethods'] = 'روش‌های نمره‌دهی';
$string['manageactionclone'] = 'ساختن یک فرم نمره دهی از روی یک الگوی آماده';
$string['manageactiondelete'] = 'پاک کردن فرم تعریف شده حاضر';
$string['manageactiondeleteconfirm'] = 'شما می‌خواهید که فرم نمره دهی «{$a->formname}» و تمام اطلاعات مرتبط با آن را از «{$a->component} ({$a->area})» پاک کنید. لطفا از آگاه بودن نسبت به پی‌آمدهای زیر اطمینان حاصل کنید:

* راهی برای لغو و واگردانی این عملیات وجود ندارد.
* برای تغییر شیوه نمره دهی (شامل بازگشت به شیوه «نمره‌دهی مستقیم ساده») نیازی به پاک کردن پاک کردن این فرم نیست.
* تمام اطلاعات مربوط به چگونگی پر کردن فرم‌های نمره دهی از دست خواهند رفت.
* نمره‌های محاسبه شده در دفتر نمره تغییری نخواهند کرد. هر چند توضیح مربوط به چگونگی محاسبه آنها موجود نخواهد بود.
* این عملیات روی کپی‌های احتمالی این فرم در سایر فعالیت‌ها تاثیری نمی‌گذارد.';
$string['manageactiondeletedone'] = 'فرم با موفقیت پاک شد';
$string['manageactionedit'] = 'ویرایش فرم تعریف شده حاضر';
$string['manageactionnew'] = 'تعریف یک فرم نمره دهی جدید از ابتدا';
$string['manageactionshare'] = 'انتشار فرم به صورت یک الگوی جدید';
$string['statusdraft'] = 'پیش‌نویس';
$string['statusready'] = 'آماده استفاده';
$string['templateedit'] = 'ویرایش';
