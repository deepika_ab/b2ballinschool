<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_rebuildcoursecache', language 'ar', branch 'MOODLE_24_STABLE'
 *
 * @package   tool_rebuildcoursecache
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['areyousure'] = 'هل أنت متأكد من الاستمرار؟';
$string['disclaimer'] = 'ستقوم هذه الأداة بإعادة إنشاء ذاكرة التخزين المؤقت للدورة التدريبية (modinfo و sectioncache) لجميع الدورات التدريبية على موقعك. <br/> قد يستغرق هذا وقتًا طويلاً على المواقع الكبيرة ، لذا قد ترغب في القيام بذلك أثناء الليل أو التوقف.';
$string['fail'] = 'فشل - تعذر العثور على معرف الدورة التدريبية
: {$a} <br/>';
$string['notifyfinished'] = 'تم إعادة بناء ذاكرة التخزين المؤقت للدورة التدريبية';
$string['notifyrebuilding'] = 'البدء في إعادة إنشاء ذاكرة التخزين المؤقت للدورة التدريبية';
$string['pageheader'] = 'إعادة إنشاء ذاكرة التخزين المؤقت للدورة التدريبية';
$string['pluginname'] = 'إعادة إنشاء ذاكرة التخزين المؤقت للدورة التدريبية';
$string['specifyids'] = 'اترك مربع النص فارغًا لإعادة إنشاء جميع الدورات التدريبية أو تحديد معرفات الدورات التدريبية الفردية التي سيتم إعادة بنائها (مفصولة بفواصل أو مسافة) <br/> مثال: 1 أو 2 أو 30 أو 100 أو 1 2 30 100';
$string['success'] = 'نجاح - معرف الدورة التدريبية: {$a} <br/>';
