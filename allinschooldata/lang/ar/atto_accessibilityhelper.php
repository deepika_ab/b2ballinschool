<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_accessibilityhelper', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   atto_accessibilityhelper
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['listimages'] = 'الصور في محرر النص:';
$string['listlinks'] = 'الروابط في محرر النص:';
$string['liststyles'] = 'أنماط للجزء المحدد:';
$string['noimages'] = 'لا يوجد صور:';
$string['nolinks'] = 'لا يوجد روابط';
$string['nostyles'] = 'لا يوجد أنماط';
$string['pluginname'] = 'مساعد ترويسة الشاشة';
$string['privacy:metadata'] = 'لا يقوم الملحق الإضافي atto_accessibilityhelper بتخزين أيّ بيانات شخصية.';
$string['selectimage'] = 'حدد الصورة';
$string['selectlink'] = 'حدد الرابط';
