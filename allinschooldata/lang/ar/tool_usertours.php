<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_usertours', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_usertours
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['above'] = 'في الاعلى';
$string['actions'] = 'الإجراءات';
$string['appliesto'] = 'ينطبق على';
$string['backdrop'] = 'تظهر مع خلفية';
$string['backdrop_help'] = 'يمكنك استخدام خلفية لتمييز جزء الصفحة الذي تشير إليه. ملاحظة: الخلفيات غير متوافقة مع بعض أجزاء الصفحة مثل شريط التنقل.';
$string['below'] = 'أدناه';
$string['block'] = 'كتلة';
$string['block_named'] = 'كتلة باسم \'{$a}\\';
$string['cachedef_stepdata'] = 'قائمة خطوات جولة المستخدم';
$string['cachedef_tourdata'] = 'قائمة بمعلومات جولات المستخدم الممكّنة و التي يتم جلبها في كل صفحة';
$string['confirmstepremovalquestion'] = 'هل أنت متأكد أنك تريد إزالة هذه الخطوة؟';
$string['confirmstepremovaltitle'] = 'تأكيد إزالة الخطوة';
$string['confirmtourremovalquestion'] = 'هل أنت متأكد أنك تريد إزالة هذه الجولة؟';
$string['confirmtourremovaltitle'] = 'تأكيد إزالة الجولة';
$string['content'] = 'يحتوى';
$string['content_heading'] = 'المحتوى';
$string['content_help'] = 'يمكن إضافة محتوى يصف الخطوة كنص عادي ، مرفقًا به علامات متعددة اللغات (للاستخدام مع مرشح المحتوى متعدد اللغات) إذا لزم الأمر.
بدلاً من ذلك ، يمكن إدخال معرف سلسلة اللغة في معرف التنسيق أو المكون (بدون أقواس أو مسافة بعد الفاصلة).';
$string['cssselector'] = 'محدد CSS';
$string['defaultvalue'] = 'الافتراضي ({$a})';
$string['delay'] = 'التأخير قبل إظهار الخطوة';
$string['delay_help'] = 'يمكنك اختياريا إضافة تأخير قبل عرض الخطوة. هذا التأخير بالميلي ثانية.';
$string['description'] = 'الوصف';
$string['done'] = 'منجز';
$string['editstep'] = 'التحرير "{$a}"';
$string['enabled'] = 'تمكين';
$string['endtour'] = 'نهاية الجولة';
$string['event_step_shown'] = 'الخطوة المبينة';
$string['event_tour_ended'] = 'انتهت الجولة';
$string['event_tour_reset'] = 'إعادة تعيين الجولة';
$string['event_tour_started'] = 'بدأت الجولة';
$string['exporttour'] = 'تصدير الجولة';
$string['filter_category'] = 'الفئة';
$string['filter_category_help'] = 'اعرض الجولة على الصفحة المرتبطة بمادة في الفئة المحددة.';
$string['filter_course'] = 'المقررات';
$string['filter_courseformat'] = 'تنسيق المادة';
$string['filter_courseformat_help'] = 'اعرض الجولة على الصفحة المرتبطة بالمادة باستخدام تنسيق المادة المحددة.';
$string['filter_course_help'] = 'اعرض الجولة على الصفحة المرتبطة بالمادة المحددة.';
$string['filter_header'] = 'مرشحات جولة';
$string['filter_help'] = 'حدد الشروط التي سيتم خلالها عرض الجولة.
يجب أن تتطابق جميع المرشحات مع جولة يتم عرضها للمستخدم.';
$string['filter_role'] = 'الدور';
$string['filter_role_help'] = 'قد تقتصر الجولة على المستخدمين ذوي الأدوار المحددة في السياق الذي يتم عرض الجولة فيه. على سبيل المثال ، لن ينحصر تقييد جولة لوحة القيادة للمستخدمين مع دور الطالب إذا كان لدى المستخدمين دور الطالب في المادة (كما هو الحال عمومًا). لا يمكن قصر جولة لوحة القيادة على المستخدمين الذين لديهم دور في النظام.';
$string['filter_theme'] = 'المظهر';
$string['filter_theme_help'] = 'اعرض الجولة عندما يستخدم المستخدم أحد المظاهر المحددة.';
$string['importtour'] = 'استيراد جولة';
$string['left'] = 'اليسار';
$string['modifyshippedtourwarning'] = 'هذه جولة مستخدم تم شحنها مع Moodle. قد يتم تجاوز أيّ تعديلات تجريها أثناء ترقية الموقع التالي.';
$string['movestepdown'] = 'تحرك خطوة لأسفل';
$string['movestepup'] = 'تحرك خطوة لأعلى';
$string['movetourdown'] = 'تحرك جولة لأسفل';
$string['movetourup'] = 'تحرك جولة لأعلى';
$string['name'] = 'الاسم';
$string['newstep'] = 'خطوة جديدة';
$string['newtour'] = 'إنشاء جولة جديدة';
$string['next'] = 'التالى';
$string['options_heading'] = 'الخيارات';
$string['orphan'] = 'اعرض إذا لم يتم العثور على الهدف';
$string['orphan_help'] = 'أظهر الخطوة إذا تعذر العثور على الهدف على الصفحة.';
$string['pathmatch'] = 'تنطبق على مطابقة الرابط';
$string['pathmatch_help'] = 'سيتم عرض الجولات في أي صفحة يتطابق الرابط  الخاص بها مع هذه القيمة. يمكنك استخدام الحرف٪ كحرف بدل ليعني أي شيء.
تتضمن بعض أمثلة الأمثلة: * / my /٪ - لمطابقة Dashboard * /course/view.php؟id=2 - لمطابقة دورة تدريبية محددة
 * /mod/forum/view.php٪ - لمطابقة قائمة مناقشات المنتدى
* /user/profile.php٪ - لمطابقة صفحة ملف تعريف المستخدم إذا كنت ترغب في عرض جولة على الصفحة الرئيسية للموقع ، يمكنك استخدام القيمة: "الصفحة الرئيسية".';
$string['pausetour'] = 'وقفة';
$string['placement'] = 'تحديد مستوى';
$string['placement_help'] = 'يمكن وضع خطوة أعلى أو أسفل أو يسار أو يمين الهدف. يوصى بالأعلى أو أدناه ، حيث يتم ضبطها بشكل أفضل لعرض الهاتف المحمول. إذا لم تكن الخطوة مناسبة لصفحة معينة في المكان المحدد ، فسيتم وضعها تلقائيًا في مكان آخر.';
$string['pluginname'] = 'جولات المستخدم';
$string['privacy:metadata:preference:completed'] = 'الوقت الذي أتم فيه المستخدم آخر جولة للمستخدم.';
$string['privacy:metadata:preference:requested'] = 'الوقت الذي طلب فيه المستخدم آخر جولة للمستخدم يدويًا.';
$string['privacy:request:preference:completed'] = 'لقد قمت مؤخرًا بتحديد جولة المستخدم "{$a->name}" على أنها مكتملة في {$a->time}';
$string['privacy:request:preference:requested'] = 'لقد طلبت آخر مرة للمستخدم "{$a->name}" في {$a->time}';
$string['reflex'] = 'تابع النقرة';
$string['reflex_help'] = 'انتقل إلى الخطوة التالية عند النقر فوق الهدف.';
$string['resettouronpage'] = 'إعادة تعيين جولة المستخدم في هذه الصفحة';
$string['resumetour'] = 'استئنف';
$string['right'] = 'يمين';
$string['select_block'] = 'حدد كتلة';
$string['selector_defaulttitle'] = 'أدخل عنوانًا وصفيًا';
$string['selectordisplayname'] = 'محدد CSS يطابق \'{$a}\\';
$string['selecttype'] = 'اختر نوع الخطوة';
$string['sharedtourslink'] = 'مستودع الجولة';
$string['skip'] = 'تخطى';
$string['target'] = 'الهدف';
$string['target_block'] = 'كتلة';
$string['target_heading'] = 'الهدف الخطوة';
$string['target_selector'] = 'محدد';
$string['target_selector_targetvalue'] = 'محددات CSS';
$string['target_selector_targetvalue_help'] = 'يمكن استخدام محدد CSS لاستهداف أيّ عنصر تقريبًا على الصفحة. يمكن العثور على المحدد المناسب بسهولة باستخدام أدوات المطور لمتصفح الويب الخاص بك.';
$string['targettype'] = 'نوع الهدف';
$string['targettype_help'] = 'ترتبط كل خطوة بجزء من الصفحة - الهدف. أنواع الاستهداف هي:
* حظر - لعرض خطوة بجانب كتلة محددة
* محدد CSS - لتحديد المنطقة المستهدفة بدقة باستخدام CSS
* العرض في منتصف الصفحة - لخطوة لا تحتاج إلى ربطها بجزء معين من الصفحة';
$string['target_unattached'] = 'عرض في منتصف الصفحة';
$string['title'] = 'العنوان';
$string['title_help'] = 'يمكن إضافة عنوان الخطوة كنص عادي ، مرفقًا به علامات متعددة اللغات (للاستخدام مع مرشح المحتوى متعدد اللغات) إذا لزم الأمر.
بدلاً من ذلك ، يمكن إدخال معرف سلسلة اللغة في معرض التنسيق أو المكون (بدون أقواس أو مسافة بعد الفاصلة).';
$string['tour1_content_addingblocks'] = 'في الواقع ، فكر جيدًا في تضمين أي كتل على صفحاتك. لا يتم عرض الكتل في تطبيق Moodle ، كقاعدة عامة ، من الأفضل لك التأكد من أن موقعك يعمل بشكل جيد دون أي كتل.';
$string['tour1_content_blockregion'] = 'لا تزال هناك منطقة كتلة هنا. نوصي بإزالة كتل التنقل والإدارة بشكل كامل ، حيث أن جميع الوظائف موجودة في مكان آخر في مظهر Boost.';
$string['tour1_content_customisation'] = 'لتخصيص مظهر موقعك والصفحة الأولى ، استخدم قائمة الإعدادات في زاوية هذا الرأس. حاول تشغيل التحرير في الوقت الحالي.';
$string['tour1_content_end'] = 'هذه هي نهاية جولة المستخدم الخاصة بك.
لن تظهر مرة أخرى إلا إذا قمت بإعادة تعيينها باستخدام الرابط في تذييل الصفحة. كمشرف ، يمكنك أيضًا إنشاء جولات خاصة بك مثل هذا!';
$string['tour1_content_navigation'] = 'الملاحة الرئيسية هي الآن من خلال درج التنقل هذا. يتم تحديث المحتويات وفقًا لموقعك في الموقع. استخدم الزر الموجود في الأعلى لإخفائه أو إظهاره.';
$string['tour1_content_welcome'] = 'مرحبًا بك في مظهر Boost. إذا قمت بالترقية من إصدار سابق ، فقد تجد بعض الأشياء تبدو مختلفة بعض الشيء مع هذا المظهر.';
$string['tour1_title_addingblocks'] = 'إضافة كتل';
$string['tour1_title_blockregion'] = 'منطقة كتلة';
$string['tour1_title_customisation'] = 'التخصيص';
$string['tour1_title_end'] = 'نهاية الجولة';
$string['tour1_title_navigation'] = 'التنقل';
$string['tour1_title_welcome'] = 'أهلا بك  ...';
$string['tour2_content_addblock'] = 'إذا قمت بتشغيل التحرير ، فيمكنك إضافة كتل من درج التنقل. ومع ذلك ، فكر جيدًا في تضمين أي كتل في صفحاتك. لا يتم عرض الكتل في تطبيق Moodle ، لذا فمن الأفضل أن تكون المادة الخاصة بك أفضل تجربة للمستخدم دون أي كتل.';
$string['tour2_content_addingblocks'] = 'يمكنك إضافة كتل لهذه الصفحة باستخدام هذا الزر. ومع ذلك ، فكر جيدًا في تضمين أيّ كتل في صفحاتك. لا يتم عرض الكتل في تطبيق Moodle ، لذا فمن الأفضل أن تكون المادة الخاصة بك أفضل تجربة للمستخدم دون أي كتل.';
$string['tour2_content_customisation'] = 'لتغيير أيّ إعدادات للمادة، استخدم قائمة الإعدادات في زاوية هذا الرأس. ستجد قائمة إعدادات مشابهة في الصفحة الرئيسية لكل نشاط ، أيضًا. حاول تشغيل التحرير في الوقت الحالي.';
$string['tour2_content_end'] = 'هذه هي نهاية جولة المستخدم. لن تظهر مرة أخرى إلا إذا قمت بإعادة تعيينها باستخدام الرابط في تذييل الصفحة.
يمكن لمشرف الموقع أيضًا إنشاء جولات إضافية لهذا الموقع إذا لزم الأمر.';
$string['tour2_content_navigation'] = 'التنقل الآن من خلال درج التنقل هذا. استخدم الزر الموجود في الأعلى لإخفائه أو إظهاره. سترى أن هناك روابط لأقسام مادتك.';
$string['tour2_content_opendrawer'] = 'حاول فتح درج التنقل الآن.';
$string['tour2_content_participants'] = 'عرض المشاركين هنا.
هذا هو المكان الذي تذهب إليه لإضافة الطلاب أو إزالتهم.';
$string['tour2_content_welcome'] = 'مرحبًا بك في مظهر Boost. إذا تمت ترقية موقعك من إصدار سابق ، فقد تجد الأشياء تبدو مختلفة بعض الشيء هنا في صفحة المادة.';
$string['tour2_title_addblock'] = 'أضف كتلة';
$string['tour2_title_addingblocks'] = 'أضف كتل';
$string['tour2_title_customisation'] = 'التخصيص';
$string['tour2_title_end'] = 'نهاية الجولة';
$string['tour2_title_navigation'] = 'التنقل';
$string['tour2_title_opendrawer'] = 'افتح درج التنقل';
$string['tour2_title_participants'] = 'المشاركون في الدورة';
$string['tour2_title_welcome'] = 'أهلا بك ...';
$string['tour3_content_dashboard'] = 'تحتوي لوحة القيادة الجديدة على العديد من الميزات لمساعدتك في الوصول بسهولة إلى المعلومات الأكثر أهمية بالنسبة لك.';
$string['tour3_content_displayoptions'] = 'قد يتم فرز المواد حسب اسم المادة أو حسب تاريخ الوصول الأخير. يمكنك أيضًا اختيار عرض المواد في قائمة ، مع معلومات موجزة ، أو عرض "البطاقة" الافتراضي.';
$string['tour3_content_overview'] = 'تُظهر كتلة نظرة عامة على المادة جميع المواد التي قمت بتسجيلها. يمكنك اختيار عرض المواد الجارية حاليًا ، أو في الماضي أو المستقبل ، أو المواد التي حددتها بنجمة.';
$string['tour3_content_recentcourses'] = 'تعرض كتلة المواد التي تم الوصول إليها مؤخرًا المواد التي قمت بزيارتها آخر مرة ، مما يسمح لك بالقفز إلى الخلف مباشرة.';
$string['tour3_content_starring'] = 'يمكنك اختيار تمييز مادة لجعلها بارزة أو إخفاء مادة لم تعد مهمة بالنسبة لك. هذه الإجراءات تؤثر فقط على وجهة نظرك.
يمكنك أيضًا اختيار عرض المواد في قائمة ، أو مع معلومات موجزة ، أو عرض "البطاقة" الافتراضي.';
$string['tour3_content_timeline'] = 'كتلة الجدول الزمني توضح الأحداث القادمة الهامة. يمكنك اختيار إظهار الأنشطة في أسبوع أو شهر مقبل أو في المستقبل.
يمكنك أيضا عرض العناصر التي تأخر موعدها.';
$string['tour3_title_dashboard'] = 'لوحتك القيادة';
$string['tour3_title_displayoptions'] = 'خيارات العرض';
$string['tour3_title_overview'] = 'نظرة عامة على المقرر';
$string['tour3_title_recentcourses'] = 'المواد التي تم الوصول إليها مؤخرا';
$string['tour3_title_starring'] = 'التمييز بنجمة و إخفاء المواد';
$string['tour3_title_timeline'] = 'كتلة الجدول الزمني';
$string['tour4_content_groupconvo'] = 'إذا كنت عضوًا في مجموعة مع تمكين المراسلة الجماعية ، فسترى محادثات جماعية هنا. تتيح لك محادثات مجموعة المادة و التفاعل مع الآخرين في مجموعتك في موقع خاص وملائم.';
$string['tour4_content_icon'] = 'يمكنك الوصول إلى رسائلك من أي صفحة باستخدام هذا الرمز. إذا كان لديك أي رسائل غير مقروءة ، فسيظهر هنا عدد الرسائل غير المقروءة أيضًا. انقر على الأيقونة لفتح درج الرسائل و متابعة الجولة.';
$string['tour4_content_messaging'] = 'تتضمن ميزات المراسلة الجديدة المراسلة الجماعية داخل المادة و التحكم بشكل أفضل في من يمكنه إرسال رسائل إليك.';
$string['tour4_content_settings'] = 'يمكنك الوصول إلى إعدادات المراسلة الخاصة بك عن طريق رمز الترس. يتيح لك إعداد الخصوصية الجديد و تقييد من يمكنه إرسال رسائل إليك.';
$string['tour4_content_starred'] = 'يمكنك اختيار تمييز محادثات معينة لجعلها أكثر سهولة.';
$string['tour4_title_groupconvo'] = 'رسائل المجموعة';
$string['tour4_title_icon'] = 'مراسلة';
$string['tour4_title_messaging'] = 'واجهة المراسلة الجديدة';
$string['tour4_title_settings'] = 'إعدادات المراسلة';
$string['tour4_title_starred'] = 'تمييز بنجمة';
$string['tourconfig'] = 'ملف تكوين الجولة للاستيراد';
$string['tour_final_step_content'] = 'هذه هي نهاية جولة المستخدم الخاصة بك. لن تظهر مرة أخرى إلا إذا قمت بإعادة تعيينها باستخدام الرابط في تذييل الصفحة.';
$string['tour_final_step_title'] = 'نهاية الجولة';
$string['tourisenabled'] = 'تم تمكين الجولة';
$string['tourlist_explanation'] = 'يمكنك إنشاء أكبر عدد تريده من الجولات و تمكينها لأجزاء مختلفة من Moodle. يمكن إنشاء جولة واحدة فقط لكل صفحة.';
$string['tour_resetforall'] = 'تم إعادة تعيين حالة الجولة. سيتم عرضه لجميع المستخدمين مرة أخرى.';
$string['tours'] = 'الجولات';
$string['usertours'] = 'جولات المستخدم';
$string['usertours:managetours'] = 'إنشاء و تحرير و إزالة جولات المستخدم';
$string['viewtour_edit'] = 'يمكنك تعديل الإعدادات الافتراضية للجولة و فرض جولة للظهور لجميع المستخدمين مرة أخرى.';
$string['viewtour_info'] = 'هذه هي جولة \'{$a->tourname}\'. ينطبق على المسار \'{$a->path}\'.';
