<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_certificate', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_certificate
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcertpage'] = 'إضافة صفحة';
$string['addelement'] = 'إضافة عنصر';
$string['aissueswerecreated'] = 'تم إنشاء المشكلات {$a}';
$string['aligncentre'] = 'مركز';
$string['alignleft'] = 'اليسار';
$string['alignment'] = 'محاذاة النص';
$string['alignment_help'] = 'تعني المحاذاة اليمنى للنص أن إحداثيات العنصر (الموضع X والموضعY) ستشير إلى الركن الأيمن العلوي من مربع النص ، وفي المحاذاة الوسطية ستشير إلى الجزء العلوي الأوسط وفي المحاذاة اليسرى إلى الزاوية اليسرى العليا';
$string['alignright'] = 'اليمين';
$string['certificate'] = 'شهادة';
$string['certificatecopy'] = '{$a} (نسخة)';
$string['certificate_customfield'] = 'الحقول المخصصة الشهادات';
$string['certificate:image'] = 'إدارة صور الشهادة';
$string['certificateimages'] = 'صور الشهادة';
$string['certificate:issue'] = 'شهادة إصدار للمستخدمين';
$string['certificate:manage'] = 'إدارة الشهادات';
$string['certificates'] = 'الشهادات';
$string['certificatesissued'] = 'الشهادات الصادرة';
$string['certificate:verify'] = 'تحقق من أي شهادات';
$string['certificate:viewallcertificates'] = 'عرض جميع قوالب الشهادات والقضايا للمستأجر الحالي';
$string['changeelementsequence'] = 'تقدم إلى الأمام أو العودة';
$string['code'] = 'الكود';
$string['createtemplate'] = 'قالب جديد';
$string['customfield_previewvalue'] = 'قيمة المعاينة';
$string['customfield_visible'] = 'ظاهر';
