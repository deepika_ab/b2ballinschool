<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_snap', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_snap
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aboutcourse'] = 'حول هذا المقرر الدراسي';
$string['accesscalendarstring'] = 'التقويم';
$string['accessforumstringdis'] = 'خيارات العرض';
$string['accessforumstringmov'] = 'خيارات النقل';
$string['accessglobalsearchstring'] = 'بحث';
$string['action:changeassetvisibility'] = 'تغيير رؤية الأصول';
$string['action:changesectionvisibility'] = 'تغيير رؤية القسم';
$string['action:duplicateasset'] = 'تكرار الأصل';
$string['action:highlightsectionvisibility'] = 'تسليط الضوء على رؤية القسم';
$string['activity'] = 'النشاط';
$string['addanewsection'] = 'إنشاء قسم جديد';
$string['addresourceoractivity'] = 'إنشاء نشاط التعليم';
$string['admin'] = 'المسؤول';
$string['admineventwarning'] = 'لرؤية الأحداث من جميع المقررات الدراسية،';
$string['advancedbrandingheading'] = 'العلامات التجارية المتقدمة';
$string['ago'] = 'مضت';
$string['answered'] = 'مجابة';
$string['appendices'] = 'الأدوات';
$string['assigndraft'] = 'مسودة تتطلب تأكيدك';
$string['assignreopened'] = 'تم إعادة الفتح';
$string['at'] = 'عند';
$string['attempted'] = 'تمت المحاولة';
$string['basics'] = 'الأساسيات';
$string['brandingheading'] = 'إدراج علامات تجارية';
$string['browse'] = 'استعراض';
$string['browseallcourses'] = 'استعراض كل المقررات الدراسية';
$string['cachedef_activity_deadlines'] = 'التخزين المؤقت للمواعيد النهائية لنشاط المستخدمين.';
$string['cachedef_course_card_bg_image'] = 'التخزين المؤقت لصورة خلفية المقرر الدراسي.';
$string['cachedef_course_card_teacher_avatar'] = 'التخزين المؤقت للصورة الرمزية الشخصيه للمعلم.';
$string['cachedef_course_card_teacher_avatar_index'] = 'التخزين المؤقت لفهرس الصور الرمزية الشخصيه للمعلم.';
$string['cachedef_course_completion_progress'] = 'ذلك لتخزين بيانات الإكمال مؤقتًا لكل مقرر دراسي / مستخدم.';
$string['cachedef_course_completion_progress_ts'] = 'يستخدم ذلك بحيث نستطيع إبطال ذاكرات التخزين المؤقت على مستوى الجلسات إذا تم تغيير إعدادات إكمال المقرر الدراسي لأحد المقررات الدراسية أو الوحدات النمطية.';
$string['cachedef_generalstaticappcache'] = 'محاذاة ذاكرة التخزين المؤقت على مستوى التطبيق الثابت العام';
$string['cachedef_profile_based_branding'] = 'التخزين المؤقت للعلامة التجارية المستندة إلى ملف التعريف.';
$string['cachedef_webservicedefinitions'] = 'التخزين المؤقت لتعريفات خدمة الويب المنشأة تلقائيًا.';
$string['card'] = 'بطاقة';
$string['category_color'] = 'لون الفئة';
$string['category_color_description'] = 'لون فئة المقرر الدراسي. تتخذ المقررات الدراسية الفرعية أقرب تكوين للفئة الأساسية';
