<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'customfield', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   customfield
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'إجراء';
$string['addingnewcustomfield'] = 'إضافة {$a} جديد';
$string['addnewcategory'] = 'إضافة فئة جديدة';
$string['afterfield'] = 'بعد حقل {$a}';
$string['categorynotfound'] = 'الفئة غير موجود';
$string['checked'] = 'تم اختباره';
$string['commonsettings'] = 'الاعدادات العامة';
$string['componentsettings'] = 'إعدادات المكون';
$string['confirmdeletecategory'] = 'هل أنت متأكد من رغبتك بحذف هذا الفئة؟ سيتم أيضًا حذف جميع الحقول الموجودة داخل الفئة وجميع البيانات المرتبطة بها. لا يمكن التراجع عن هذا الإجراء.';
$string['confirmdeletefield'] = 'هل أنت متأكد من رغبتك بحذف هذا الحقل وجميع البيانات المرتبطة به؟
هذا الإجراء لا يمكن التراجع عنه.';
$string['createnewcustomfield'] = 'إضافة حقل مخصص جديد';
$string['customfield'] = 'حقل مخصص';
$string['customfielddata'] = 'بيانات الحقل المخصص';
$string['customfields'] = 'الحقول المخصصة';
$string['defaultvalue'] = 'القيمة الافتراضية';
$string['description'] = 'الوصف';
$string['description_help'] = 'يتم عرض الوصف في النموذج أسفل الحقل.';
$string['edit'] = 'تعديل';
$string['editcategoryname'] = 'تعديل اسم الفئة';
$string['editingfield'] = 'تحديث {$a}';
$string['errorfieldtypenotfound'] = 'نوع الحقل  {$a} غير موجود';
$string['erroruniquevalues'] = 'هذه القيمة مستخدمة مسبقاً.';
$string['eventcategorycreated'] = 'تم إنشاء فئة حقل مخصص';
$string['eventcategorydeleted'] = 'تم حذف فئة حقل مخصص';
$string['eventcategoryupdated'] = 'تم تحديث فئة حقل مخصص';
$string['eventfieldcreated'] = 'تم إنشاء الحقل المخصص';
$string['eventfielddeleted'] = 'تم حذف الحقل المخصص';
$string['eventfieldupdated'] = 'تم تحديث الحقل المخصص';
$string['fieldname'] = 'الاسم';
$string['fieldnotfound'] = 'الحقل غير موجود';
$string['fieldshortname'] = 'الاسم المختصر';
$string['formfieldcheckshortname'] = 'الاسم المختصر موجود مسبقاً';
$string['invalidshortnameerror'] = 'يمكن أن يحتوي الاسم المختصر فقط على أحرف صغيرة أبجدية رقمية وشرطات سفلية (_).';
$string['isdataunique'] = 'بيانات فريدة';
$string['isdataunique_help'] = 'هل يجب أن تكون البيانات فريدة؟';
$string['isfieldrequired'] = 'مطلوب';
$string['isfieldrequired_help'] = 'هل هذا الحقل مطلوب؟';
$string['link'] = 'الرابط';
$string['linktarget'] = 'رابط الهدف';
$string['modify'] = 'تعديل';
$string['movecategory'] = 'نقل "{$a}"';
$string['movefield'] = 'نقل "{$a}"';
$string['no'] = 'لا';
$string['nocategories'] = 'لا توجد حقول مخصصة وفئات.';
$string['nopermissionconfigure'] = 'ليس لديك صلاحية تكوين حقول هنا.';
$string['notchecked'] = 'لم يتم اختباره';
$string['otherfields'] = 'الحقول الأخرى';
$string['otherfieldsn'] = 'الحقول الأخرى {$a}';
$string['privacy:metadata:customfield_data'] = 'يمثل بيانات الحقل المخصصة المحفوظة في النص';
$string['privacy:metadata:customfield_data:charvalue'] = 'قيمة البيانات عندما تكون حرفية';
$string['privacy:metadata:customfield_data:contextid'] = 'الرقم التعريفي للنص مكان حفظ البيانات';
$string['privacy:metadata:customfield_data:decvalue'] = 'قيمة البيانات ، عندما تكون عشرية';
$string['privacy:metadata:customfield_data:fieldid'] = 'الرقم التعريفي للحقل';
$string['privacy:metadata:customfield_data:instanceid'] = 'الرقم التعريفي المرتبط بالبيانات';
$string['privacy:metadata:customfield_data:intvalue'] = 'قيمة البيانات، عندما تكون رقمية';
$string['privacy:metadata:customfield_data:shortcharvalue'] = 'قيمة البيانات عندما تكون حرفية قصيرة';
$string['privacy:metadata:customfield_data:timecreated'] = 'وقت إنشاء البيانات';
$string['privacy:metadata:customfield_data:timemodified'] = 'وقت آخر تعديل للبيانات';
$string['privacy:metadata:customfield_data:value'] = 'قيمة البيانات، عندما تكون نصية';
$string['privacy:metadata:customfield_data:valueformat'] = 'تنسيق القيمة، عندما تكون نصية';
$string['privacy:metadata:customfieldpluginsummary'] = 'حقول لمختلف المكونات';
$string['privacy:metadata:filepurpose'] = 'ملف مرفق ببيانات الحقل المخصص';
$string['shortname'] = 'الاسم المختصر';
$string['shortname_help'] = 'يجب أن يكون الاسم المختصر فريدًا ويمكن أن يحتوي فقط على أحرف صغيرة أبجدية رقمية وشرطات سفلية (_). لا يتم عرضه في أي مكان على الموقع ، ولكن يمكن استخدامه للتزامن مع الأنظمة الخارجية أو في خدمات الويب.';
$string['showdate'] = 'عرض البيانات';
$string['specificsettings'] = 'الاعدادات المحددة';
$string['therearenofields'] = 'لا توجد حقول بهذه الفئة';
$string['totopofcategory'] = 'إلى الفئة الأعلى';
$string['type'] = 'النوع';
$string['unknownhandler'] = 'يتعذر العثور على معالج للحقول المخصصة للمكون  {$a->component} ومنطقة {$a->area}.';
$string['yes'] = 'نعم';
