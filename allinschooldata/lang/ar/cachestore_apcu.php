<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'cachestore_apcu', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   cachestore_apcu
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['clusternotice'] = 'يرجى العلم بأن APCu هو مجرد خيار مناسب لمواقع العقدة المفردة أو ذاكرات التخزين المؤقت التي يمكن تخزينها محليًا.
لمزيد من المعلومات ، راجع وثائق ذاكرة التخزين المؤقت للمستخدم APC .';
$string['notice'] = 'تنويه';
$string['pluginname'] = 'ذاكرة التخزين المؤقت للمستخدم APC (APCu)';
$string['prefix'] = 'اختصار';
$string['prefix_help'] = 'يتم استخدام البادئة أعلاه لجميع المفاتيح التي يتم تخزينها في مثيل متجر APC هذا. بشكل افتراضي ، يتم استخدام بادئة قاعدة البيانات.';
$string['prefixinvalid'] = 'البادئة التي حددتها غير صالحة. يمكنك فقط استخدام az AZ 0-9-_.';
$string['prefixnotunique'] = 'البادئة التي حددتها ليست فريدة. يرجى اختيار بادئة فريدة من نوعها.';
$string['privacy:metadata'] = 'يخزن البرنامج المساعد للمستخدم APC (APCu) البيانات لفترة وجيزة كجزء من وظيفة التخزين المؤقت و لكن يتم مسح هذه البيانات بانتظام و لا يتم إرسالها خارجيًا بأيّ طريقة.';
$string['testperformance'] = 'اختبار الأداء';
$string['testperformance_desc'] = 'في حالة التمكين ، سيتم تضمين أداء APCu عند عرض صفحة اختبار الأداء. تمكين هذا على موقع الإنتاج غير مستحسن.';
