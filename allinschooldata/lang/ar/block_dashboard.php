<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_dashboard', language 'ar', branch 'MOODLE_30_STABLE'
 *
 * @package   block_dashboard
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['backtocourse'] = 'العودة إلى المقرر';
$string['bar'] = 'شريط';
$string['blockname'] = 'لوحة القيادة';
$string['checktohide'] = 'حدد خانة الاختيار لإخفاء العنوان';
$string['choicevalue'] = 'خيار';
$string['cleararea'] = 'امسح كل منطقة الملف';
$string['configcharset'] = 'ترميز';
$string['configcolors'] = 'ألوان الخلفية';
$string['configcronfrequency'] = 'تكرر';
$string['configcronhour'] = 'ساعة';
$string['configcronmin'] = 'دقائق';
$string['configcrontime'] = 'ساعه';
$string['configdashboardparams'] = 'إعدادات بيانات لوحة القيادة';
$string['configdatatitles'] = 'عناوين البيانات';
$string['configdatatypes'] = 'أنواع البيانات';
$string['configdisplay'] = 'عناصر الإعداد للعرض';
$string['configfileformat'] = 'تنسيق الملف';
$string['configfilelocation'] = 'موقع الملف الذي تم إنشاؤه';
$string['configfilterlabels'] = 'تصفية التسميات';
$string['configfilters'] = 'تصفية';
$string['configformatting'] = 'تنسيق بيانات الإخراج';
$string['configgraphtype'] = 'نوع الرسم البياني';
$string['configgraphwidth'] = 'عرض الرسم البياني';
$string['configlayout'] = 'نشر البيانات';
$string['configlocation'] = 'الموقع على الخريطة';
$string['configmaptype'] = 'أنواع الخرائط';
$string['datevalue'] = 'تاريخ';
$string['day'] = 'يوم';
$string['sunday'] = 'الأحد';
$string['thursday'] = 'الخميس';
$string['total'] = 'المجموع';
$string['tuesday'] = 'الثلاثاء';
$string['wednesday'] = 'الأربعاء';
$string['week'] = 'أسبوع';
$string['year'] = 'عام';
