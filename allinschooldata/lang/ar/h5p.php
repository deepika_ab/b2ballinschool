<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'h5p', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   h5p
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['cancellabel'] = 'إلغاء';
$string['changedby'] = 'تغيير من قبل';
$string['close'] = 'قريب';
$string['confirmdialogheader'] = 'أكد الإجراء';
$string['confirmlabel'] = 'تؤكد';
$string['contenttype'] = 'نوع المحتوى';
$string['copyrightinfo'] = 'معلومات حقوق التأليف والنشر';
$string['copyrightstring'] = 'حقوق النشر';
$string['date'] = 'تاريخ';
$string['download'] = 'تحميل';
$string['editor'] = 'محرر';
$string['fullscreen'] = 'تكبير الشاشة';
$string['offlineDialogRetryButtonLabel'] = 'أعد المحاولة الآن';
$string['offlineSuccessfulSubmit'] = 'تم تقديم النتائج بنجاح.';
$string['size'] = 'بحجم';
$string['title'] = 'عنوان';
$string['year'] = 'عام';
$string['years'] = 'سنوات';
