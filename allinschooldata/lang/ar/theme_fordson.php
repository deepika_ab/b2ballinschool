<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_fordson', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_fordson
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activityiconsize'] = 'حجم الأيقونة النشطة';
$string['activityiconsize_desc'] = 'اضبط حجم الأيقونات النشطة المستخدمة في المقررات.';
$string['activitylinkstitle'] = 'الأنشطة';
$string['activitylinkstitle_desc'] = 'عرض جميع الأنشطة في المادة';
$string['activitymenu'] = 'إظهار قائمة الأنشطة المجمّعة';
$string['activitymenu_desc'] = 'عرض قوائم النشاط المجمعة في لوحات الطلاب و المدرسين. تعرض هذه القائمة قائمة مجمعة بجميع الأنشطة للطالب و المعلم.';
$string['advancedsettings'] = 'إعدادات متقدة';
$string['alert'] = 'تنبيه الصفحة الرئيسية';
$string['alert_desc'] = 'هذه رسالة تنبيه خاصة ستظهر على الصفحة الرئيسية.';
$string['backgroundimage'] = 'صورة خلفية الصفحة الافتراضية';
$string['backgroundimage_desc'] = 'صورة خلفية للصفحات';
$string['backtotop'] = 'العودة إلى الأعلى و بسلاسة';
$string['badges'] = 'الشارات';
$string['badges_desc'] = 'مكافأة طلابك';
$string['blockdisplay'] = 'حظر خيارات عرض الموقع';
$string['blockdisplay_desc'] = 'اختر كيفية عرض الكتل على الصفحة الرئيسية و صفحات المواد. يضيف Fordson على اللوحة 3 أعمدة قابلة للطي يمكن إخفاءها بواسطة المستخدم. اختر الخيار Boost الافتراضي لاستخدام عمود جانبي يميني واحد للكتل. يقوم الخيار Boost الافتراضي أيضًا بنقل الزر "إضافة كتلة" مرة أخرى إلى "درج التنقل" على يسار الصفحة. نوصي أيضًا باستخدام "Single Column Boost Default" عند استخدام أيّ من تخطيطات صفحة Boost من الإعدادات أعلاه.';
$string['blockdisplay_off'] = 'عمود واحد Boost الافتراضي';
$string['blockdisplay_on'] = 'ثلاثة أعمدة Fordson الافتراضي';
$string['blockwidthfordson'] = 'عرض عمود الكتل';
$string['blockwidthfordson_desc'] = 'ضبط عرض عمود الكتلة.';
$string['blog'] = 'رابط المدونة';
$string['blogdesc'] = 'أدخل رابط لمدونة مؤسستك. (أي http://dearbornschools.org)';
$string['bodybackground'] = 'لون خلفية مساحة النص';
$string['bodybackground_desc'] = 'اللون الرئيسي لاستخدامه في الخلفية.';
$string['brandColour'] = 'لون العلامة التجارية';
$string['brandColour_desc'] = 'لون علامتك التجارية الرئيسية';
$string['branddanger'] = 'خطر العلامة التجارية';
$string['branddanger_desc'] = 'لون العلامة التجارية لتنبيهات الخطر و اللوحات ، و ما إلى ذلك';
$string['brandemail'] = 'بريد الموقع';
$string['brandemaildesc'] = 'عنوان البريد الإلكتروني للمؤسسة الذي يظهر في الحاشية.';
$string['brandinfo'] = 'معلومات العلامة التجارية';
$string['brandinfo_desc'] = 'تنبيهات وأ لوان معلومات العلامة التجارية ، إلخ';
$string['brandorganization'] = 'اسم الموقع';
$string['brandorganizationdesc'] = 'اسم الموقع الذي يظهر في الحاشية.';
$string['brandphone'] = 'هاتف الموقع';
$string['brandphonedesc'] = 'هاتف الموقع الذي يظهر في الحاشية';
$string['brandprimary'] = 'العلامة التجارية الابتدائية';
$string['brandprimary_desc'] = 'لون علامتك التجارية الرئيسية';
$string['brandsuccess'] = 'نجاح العلامة التجارية';
$string['brandsuccess_desc'] = 'لون العلامة التجارية للتنبيهات الناجحة و اللوحات الإيجابية و الأزرة و ما إلى ذلك ...';
$string['brandwarning'] = 'تحذير العلامة التجارية';
$string['brandwarning_desc'] = 'لون العلامة التجارية للتنبيهات و اللوحات التحذيرية ، إلخ';
$string['brandwebsite'] = 'موقع المنظمة';
$string['brandwebsitedesc'] = 'عنوان الموقع الذي يظهر في الحاشية.';
$string['breadcrumbbkg'] = 'لون الخلفية المجزاءة';
$string['breadcrumbbkg_desc'] = 'لون الخلفية المجزاءة.';
$string['cardbkg'] = 'لون خلفية المحتوى';
$string['cardbkg_desc'] = 'لون خلفية المحتوى لكتلة محتوى المادة.';
$string['catsicon'] = 'رمز الفئة';
$string['catsicon_desc'] = 'اختر رمزًا لتمثيل فئات المواد.';
$string['choosereadme'] = 'يوفر Fordson تجربة بصرية فريدة لسمة Boost الافتراضية مع ميزات التخصيص مثل منتقي الألوان و تجربة الصفحة الرئيسية المحسّنة.';
$string['colours_desc'] = 'سيسمح لك منتقي الألوان بتخصيص شكل و مظهر العناصر الرئيسية في الصفحة. إذا كنت تستخدم إعدادًا مسبقًا بخلاف الإعدادات الافتراضية ، فستحتاج إلى إزالة أيّ ألوان مخصصة أدناه للحصول على أفضل النتائج ، حيث إنها ستؤدي إلى تجاوز الإعداد المسبق بنتائج غير مرغوب فيها. بشكل عام ، سيكون للإعداد المسبق ألوان افتراضية تريد رؤيتها قبل تخصيصها هنا.';
$string['colours_headingsub'] = 'إعدادات الألوان';
$string['colours_settings'] = 'الألوان';
$string['commonlyused'] = 'تخصيص وحدات النشاط و المصادر.';
$string['commonlyuseddesc'] = 'قائمة مفصولة بفواصل للقائمة  الأنشطة / المصادر لعرضها على محدد النشاط / المصادر.
الأنشطة الافتراضية:
تعيين، و الدردشة، و الاختيار، و البيانات، و ردود الفعل، المنتدى،المسرد، الدرس،  مسابقة، سكورم، و المسح، ويكي، ورشة عمل
موارد افتراضية:
الكتاب، ملف أو مجلد، imscp، و التسمية، الصفحة، الموارد، رابط';
$string['configtitle'] = 'Fordson';
$string['contentsettings'] = 'مجالات المحتوى';
$string['courseactivities'] = 'أنشطة المادة';
$string['courseblockpanelbtn'] = 'كتل المواد';
$string['courseblockpanelbtnclose'] = 'أغلق';
$string['courseboxheight'] = 'لون ارتفاع البلاطة';
$string['courseboxheight_desc'] = 'تحكم في ارتفاع بلاطة المادة في الصفحة الأولى و فئات المواد.';
$string['courseeditingcog'] = 'إظهار قائمة إعدادات المادة الافتراضية';
$string['courseeditingcog_desc'] = 'في حالة استخدام لوحة إدارة المادة، ستكون القائمة الافتراضية مخفية. من خلال التحديد هذا ، يمكنك إظهار القائمة الافتراضية و كذلك لوحة إدارة مادة المعلم. يعد هذا الأمر مثاليًا في حالة استخدام مكون إضافي من طرف ثالث يستخدم قائمة المواد للوصول إلى الإعدادات.';
$string['coursehome'] = 'الرئيسية';
$string['courseinfo'] = 'وصف المادة';
$string['coursemanage'] = 'إعدادات المادة';
$string['coursemanage_desc'] = 'إدارة المادة بأكملها';
$string['coursemanagementbutton'] = 'إدارة المادة';
$string['coursemanagementinfo'] = 'لوحة قائمة إدارة المادة';
$string['coursemanagementinfodesc'] = 'يوفر Fordson لوحة إدارة مادة منظمة و منفردة .. يمكن للمدرسين الوصول إليها من أيّ مكان داخل المادة .. للوصول إلى روابط إدارة المادة الخاصة بهم. يمكن للطلاب أيضًا الوصول إلى لوحة معلومات مادة الطالب و التي تتضمن معلومات ذات صلة بالمادة.
ننصح بشدة أن تبقي هذا قيد التشغيل.';
$string['coursemanagementtextbox'] = 'رسالة إدارة المادة';
$string['coursemanagementtextbox_desc'] = 'أضف رسالة للمعلمين في لوحة إدارة المادة في كل صفحة.';
$string['coursemanagementtoggle'] = 'عرض لوحات إدارة الطلاب و المعلمين';
$string['coursemanagementtoggle_desc'] = 'يؤدي ذلك إلى عرض روابط إدارة المادة في لوحة الموقع للمعلمين .. توفر لوحة معلومات لجميع الروابط التي يحتاجونها لإدارة المادة و المستخدمين. سيعرض أيضًا على اللوحة نظرة عامة عن المادة للطلاب الحاصلين على درجات ، و إكمال المادة، و عناصر أخرى من المادة.';
$string['coursestaff'] = 'معلّمي المادة';
$string['coursestyle1'] = 'نمط بلاطة  1';
$string['coursestyle10'] = 'عرض مادة Moodle الافتراضية';
$string['coursestyle2'] = 'نمط بلاطة  2';
$string['coursestyle3'] = 'نمط بلاطة 3';
$string['coursestyle4'] = 'نمط بلاطة  4 مع ملخص المواد';
$string['coursestyle5'] = 'نمط أفقي 1';
$string['coursestyle6'] = 'التفاصيل الكاملة لصورة الخلفية الأفقية';
$string['coursestyle7'] = 'خلفية عنوان الصورة الأفقية و المعلم فقط';
$string['coursestyle8'] = 'عامودين أفقيين';
$string['coursestyle9'] = 'تدريب الشركات - الحد الأدنى مع شريط التقدم و الانتهاء';
$string['coursetileinfo'] = 'خيارات عرض المادة';
$string['coursetileinfodesc'] = 'تتيح لك هذه الإعدادات تخصيص كيفية عرض المواد على الصفحة الأولى و كذلك فئات المواد.';
$string['coursetilestyle'] = 'عرض بلاطة المادة';
$string['coursetilestyle_desc'] = 'عند عرض فئات المواد ، يمكنك الاختيار من بين الأنماط التالية لعرض المواد. تعليمات لـ Fordson 3.5 Theme';
$string['createinfo'] = 'زر بناء مادة خاصة';
$string['createinfodesc'] = 'يظهر هذا الزر على الصفحة الرئيسية عندما يكون ممكن للمستخدم إنشاء مادة جديدة.
سيشاهد هذا الزر مع منشئ المادة على مستوى الموقع.';
$string['currentinparentheses'] = '(حالياً)';
$string['customloginheading'] = 'صفحة تسجيل الدخول المخصصة';
$string['customlogininfo'] = 'صفحة إعدادات  تسجيل الدخول المخصصة';
$string['customlogininfo_desc'] = 'هذا يسمح لك بإنشاء صفحة تسجيل دخول مخصصة. تتضمن الإعدادات الأخرى في السمة التي سيتم عرضها على صفحة تسجيل الدخول ما يلي:
* إدارة الموقع> الأمان> سياسات الموقع> إجبار المستخدمين على تسجيل الدخول = تأكد من تحديد هذا بحيث يتم نقل المستخدمين إلى صفحة تسجيل الدخول المخصصة الخاصة بك..
* إدارة الموقع> المظهر> الشعارات> الشعار =  قم بتحميل صورة هنا وستظهر أعلى نموذج تسجيل الدخول.
* Fordson> إعدادات الصور المخصصة> صورة تسجيل الدخول الافتراضية .. يمكن استخدامها لتغيير صورة الخلفية لصفحة تسجيل الدخول.
* Fordson> مجالات المحتوى> الصفحة الرئيسية .. يمكن استخدام التنبيه لتقديم إشعار في الجزء العلوي من الصفحة.';
$string['dashactivityoverview'] = 'نظرة عامة على الأنشطة';
$string['displaybottom'] = 'عرض في الحاشية';
$string['displaymycourses'] = 'عرض المواد المسجلة';
$string['displaymycoursesdesc'] = 'عرض المواد المسجلة للمستخدمين في شريط التنقل العلوي.';
$string['displaythiscourse'] = 'عرض قائمة المادة';
$string['displaythiscoursedesc'] = 'عرض الانتقال إلى قسم في قائمة المادة للمستخدمين في شريط التنقل العلوي. يحتوي هذا على عناصر القائمة التي تم العثور عليها مسبقًا في درج التنقل.';
$string['displaytop'] = 'عرض في أعلى الصفحة';
$string['drawerbkg'] = 'لون خلفية الدُرْج الجانبي';
$string['drawerbkg_desc'] = 'لون خلفية الدُرج الجانبي للقائمة الموجودة على الجانب الأيسر من الصفحة.';
$string['editoff'] = 'ايقاف التحرير';
$string['editon'] = 'تشغيل التحرير';
$string['enablecategoryicon'] = 'أيقونات عرض الفئة';
$string['enablecategoryicon_desc'] = 'عند تحديد هذا ، سيعرض فئات المواد كرموز';
$string['enhancedmydashboard'] = 'تعزيز لوحتي القيادية';
$string['enhancedmydashboard_desc'] = 'سيؤدي تشغيل هذا إلى تحسين صفحة لوحتي لتضمين جميع ميزات fordson .. مثل نموذج التسجيل السهل و شريط الكتل و  أيقونات شريط التنقل و نص الصفحة الرئيسية المخصصة وعرض الشرائح و المزيد.';
$string['enrollcoursecard'] = 'الوصول';
$string['facebook'] = 'رابط فيس بوك';
$string['facebookdesc'] = 'ادخل رابط فيس بوك  (i.e http://www.facebook.com/)';
$string['favicon'] = 'الأيقونات المفضلة';
$string['favicon_desc'] = 'تغيير الرمز المفضل لـ Fordson. الصور ذات الخلفية الشفافة و ارتفاعها 32 بكسل ستعمل بشكل أفضل. الأنواع المسموح بها: PNG ، JPG ، ICO';
$string['feature1info'] = 'ميزة 1';
$string['feature2info'] = 'ميزة 2';
$string['feature3info'] = 'ميزة 3';
$string['featureimage'] = 'ميزة الصورة';
$string['featureimage_desc'] = 'ستظهر هذه الصورة بجانب النص المميز في صف واحد.';
$string['featureinfo_desc'] = 'تتكون الميزة من صورة و نصّ سيظهران على صفحة تسجيل الدخول المخصصة على التوالي. يجب عليك إضافة صورة و نصّ حتى تظهر الميزة.';
$string['featuretext'] = 'ميزة النصّ';
$string['featuretext_desc'] = 'سيظهر هذا النص بجانب الصورة المميزة في صف واحد. استخدم Heading4 لإنشاء عنوان خاص داخل مربع النص. في Atto Editor H4 هو Heading Medium.';
$string['flickr'] = 'رابط Flickr';
$string['flickrdesc'] = 'ادخل رابط صفحتك في Flickr (i.e http://www.flickr.com/)';
$string['footerbkg'] = 'لون خلفية الحاشية';
$string['footerbkg_desc'] = 'لون خلفية الحاشية أسفل الصفحة.';
$string['footerdesc'] = 'تتيح لك العناصر أدناه تقديم العلامة التجارية إلى حاشية المظهر.';
$string['footerheading'] = 'الحاشية';
$string['footerheadingsocial'] = 'الرموز الإجتماعية';
$string['footerheadingsub'] = 'تخصيص حاشية  الصفحة الرئيسية';
$string['footnote'] = 'الحاشية';
$string['footnotedesc'] = 'محرر محتوى الحاشية السفلية الرئيسية';
$string['fploginform'] = 'لون نموذج تسجيل الدخول';
$string['fploginform_desc'] = 'لون خلفية نموذج تسجيل الدخول على الصفحة الرئيسية المخصصة.';
$string['fpsignup'] = 'الدخول';
$string['fptextbox'] = 'نص الصفحة الرئيسية لمصادقة المستخدم';
$string['fptextbox_desc'] = 'يظهر مربع النص هذا على الصفحة الرئيسية بمجرد قيام المستخدم بالتوثيق .. إنه مثالي لوضع رسالة ترحيب و تقديم إرشادات للمشترك.';
$string['fptextboxlogout'] = 'مربع نص الصفحة الرئيسية  للزائر';
$string['fptextboxlogout_desc'] = 'يظهر مربع النص هذا على الصفحة الرئيسية للزوار .. و هو مثالي لوضع رسالة ترحيب أو رابط لصفحة تسجيل الدخول.';
$string['frontpagemycoursessorting'] = 'ترتيب المواد حسب ترتيب الدخول الأخير';
$string['frontpagemycoursessorting_desc'] = 'عند تحديد هذه الميزة ، سيتم فرز عرض "المواد" (المادة المسجلة) حسب آخر وصول للمستخدم. سيؤدي ذلك إلى تجاوز إعداد "فرز المواد" ضمن "التنقل". إذا لم يتم تحديده ، فسيتم عرض (موادي) كالمعتاد. يتضمن ذلك الدورات الخاصة بي المعروضة في القائمة المنسدلة أعلى الصفحة بالإضافة إلى عرض المقرر المسجل على الصفحة الرئيسية للموقع. هذا لا يؤثر على كتلة اللوحة الرئيسية أو لوحة القيادة.';
$string['generalcontentinfo'] = 'إعدادات عرض المحتوى العام';
$string['generalcontentinfodesc'] = 'تساعدك الخيارات أدناه في تخصيص طريقة عرض المحتوى و تشغيل ميزات إضافية لـ Fordson.';
$string['generalsettings'] = 'الاعدادات العامة';
$string['googleplus'] = 'رابط Google+';
$string['googleplusdesc'] = 'أدخل رابط لملفك الشخصي في Google+. (أي https://google.com/)';
$string['gutterwidth'] = 'محتوى الحشو';
$string['gutterwidth_desc'] = 'يتحكم هذا الإعداد في مقدار التباعد المستخدم على يسار و يمين المحتوى الرئيسي.';
$string['headerdefaultimage'] = 'صورة الرأس الافتراضية';
$string['headerdefaultimage_desc'] = 'الصورة الافتراضية لرؤوس المادة و صفحات غير الدورة التدريبية';
$string['headerimagepadding'] = 'ارتفاع صورة الرأس';
$string['headerimagepadding_desc'] = 'التحكم في الحشو و ارتفاع صورة الرأس للمواد.';
$string['headerlogo'] = 'شعار الرأس';
$string['headerlogo_desc'] = 'سيتم عرض هذا الشعار في الجزء العلوي من الصفحة في منطقة الرأس. و يستخدم تحجيم الصور استجابة bootstrap.';
$string['homemyclasses'] = 'الفئة الرئيسية';
$string['homemycomp'] = 'الكفاءة الرئيسية';
$string['homemycourses'] = 'الصفحة الرئيسية للمقرر';
$string['homemycred'] = 'الصفحة الرئيسية الاعتماد';
$string['homemylectures'] = 'الصفحة الرئيسية للمحاضرة';
$string['homemylessons'] = 'الصفحة الرئيسية للدرس';
$string['homemymodules'] = 'الصفحة الرئيسية للنموذج';
$string['homemyplans'] = 'الصفحة الرئيسية للخطة';
$string['homemyprofessionaldevelopment'] = 'الصفحة الرئيسية للمطور';
$string['homemyprograms'] = 'الصفحة الرئيسية للبرنامج';
$string['homemytraining'] = 'الصفحة الرئيسية للتدريب';
$string['homemyunits'] = 'الصفحة الرئيسية للوحدة';
$string['iconnavheading'] = 'رمز التنقل';
$string['iconnavheadingsub'] = 'إنشاء أزراة مع أيقونات للاستخدام على الصفحة الرئيسية.
الروابط يمكن أن تذهب بك إلى أيّ مكان.';
$string['iconwidth'] = 'عرض أيقونة الصفحة الرئيسية';
$string['iconwidth_desc'] = 'عرض الرموز الفردية الثمانية في شريط التنقل في الأيقونة على الصفحة الرئيسية.';
$string['iconwidthinfo'] = 'إعداد عرض زر الأيقونة';
$string['iconwidthinfodesc'] = 'حدد عرضًا يسمح بنص الارتباط الخاص بك ليناسب داخل أزرار التنقل في الأيقونة.';
$string['ilearnsecurebrowser'] = 'تم تأمين هذا الاختبار باستخدام iLearn Secure Browser (يجب استخدام جهاز Chromebook باستخدام تطبيق iLearn لمحاولة هذا الاختبار)';
$string['imagesettings'] = 'إعدادات الصور المخصصة';
$string['instagram'] = 'رابط الإنستغرام';
$string['instagramdesc'] = 'أدخل رابط لصفحة الإنستغرام الخاصة بك. (بمعنى http://instagram.com/)';
$string['layoutinfo'] = 'إعدادات الشكل الظاهري';
$string['layoutinfodesc'] = 'التحكم في تخطيط الصفحة عن طريق اختيار تصميم.';
$string['learningcontentpadding'] = 'تباعد المحتوى التعليمي';
$string['learningcontentpadding_desc'] = 'يتحكم هذا في مقدار المسافة بين أعلى الصفحة و محتوى المادة الرئيسية. بشكل عام ، تريد أن يكون هذا أقل من ارتفاع صورة الرأس.';
$string['linkedin'] = 'رابط لينكد إن';
$string['linkedindesc'] = 'أدخل رابط لملفك الشخصي على LinkedIn. (أي http://www.linkedin.com/)';
$string['loginimage'] = 'صورة تسجيل الدخول الافتراضية';
$string['loginimage_desc'] = 'صورة خلفية صفحة تسجيل الدخول';
$string['loginnavicon1'] = 'الأيقونة 1';
$string['loginnavicon2'] = 'الأيقونة 2';
$string['loginnavicon3'] = 'الأيقونة 3';
$string['loginnavicon4'] = 'الأيقونة 4';
$string['loginnavicontext'] = 'نص الأيقونة';
$string['loginnavicontextdesc'] = 'النص الذي سيظهر أسفل الرمز. اجعل الموضوع قصير للحصول على أفضل النتائج.';
$string['loginnavicontitletext'] = 'عنوان الأيقونة';
$string['loginnavicontitletextdesc'] = 'النص الذي يظهر أسفل الرمز كعنوان.';
$string['logintopimage'] = 'صورة صفحة تسجيل الدخول';
$string['logintopimage_desc'] = 'تظهر هذه الصورة في صفحة تسجيل الدخول، إلى يمين نموذج تسجيل الدخول. هذا مثالي للشعار أو شعار مع خلفية شفافة.';
$string['marketboxbuttoncolor'] = 'لون زر مربع التسويق';
$string['marketboxbuttoncolor_desc'] = 'لون خلفية زر مربع التسويق.';
$string['marketboxcolor'] = 'لون خلفية مربع التسويق';
$string['marketboxcolor_desc'] = 'لون خلفية مربع التسويق.';
$string['marketboxcontentcolor'] = 'لون خلفية محتوى مربع التسويق';
$string['marketboxcontentcolor_desc'] = 'لون الخلفية لمحتوى مربع التسويق. هذا هو المكان الذي يظهر فيه النص في موضع التسويق و يمكن أن يكون مختلفًا عن لون خلفية المربع لجذب الانتباه إلى النص.';
$string['marketing1'] = 'بلاطة التسويق 1';
$string['marketing2'] = 'بلاطة التسويق 2';
$string['marketing3'] = 'بلاطة التسويق 3';
$string['marketing4'] = 'بلاطة التسويق 4';
$string['marketing5'] = 'بلاطة التسويق 5';
$string['marketing6'] = 'بلاطة التسويق 6';
$string['marketing7'] = 'بلاطة التسويق 7';
$string['marketing8'] = 'بلاطة التسويق 8';
$string['marketing9'] = 'بلاطة التسويق 9';
$string['marketingbuttontext'] = 'نص الرابط';
$string['marketingbuttontextdesc'] = 'النص الذي يظهر على الزر.';
$string['marketingbuttonurl'] = 'الرابط';
$string['marketingbuttonurldesc'] = 'العنوان الذي سوف يشير الزر إليه.';
$string['marketingcontent'] = 'المحتوى';
$string['marketingcontentdesc'] = 'المحتوى لعرضه في مربع التسويق. أبقه مختصر و لطيف.';
$string['marketingdesc'] = 'يوفر هذا المظهر خيار تمكين ثلاثة صناديق "تسويق" أو "إعلان" أسفل عرض الشرائح.
هذه تتيح لك تحديد المعلومات الأساسية للمستخدمين بسهولة و توفر روابط مباشرة.';
$string['marketingheading'] = 'بلاطات التسويق';
$string['marketingheadingsub'] = 'ثلاثة مواقع على الصفحة الأولى لإضافة معلومات و روابط';
$string['marketingheight'] = 'ارتفاع صور التسويق';
$string['marketingheightdesc'] = 'إذا كنت تريد عرض الصور في مربعات التسويق ، يمكنك تحديد طولها هنا.';
$string['marketingicon'] = 'أيقونة الرابط';
$string['marketingicondesc'] = 'اسم الرمز الذي ترغب في استخدامه في زر رابط التسويق.
 القائمة هنا. فقط أدخل ما بعد "fa-" ، على سبيل المثال "نجمة".';
$string['marketingimage'] = 'الصورة';
$string['marketingimage_desc'] = 'يوفر هذا الخيار من عرض الصورة في بلاطة التسويق';
$string['marketinginfodesc'] = 'أدخل إعدادات بلاطة التسويق الخاصة بك. يجب عليك تضمين عنوان لكي تظهر بلاطة التسويق.
سوف ينشط العنوان بلاطة التسويق كل على حدة.';
$string['marketingstyle'] = 'منتقي نمط بلاطة تسويق';
$string['marketingstyle1'] = 'شريط تسليط الضوء العلوي';
$string['marketingstyle2'] = 'أرسل ملاحظه';
$string['marketingstyle3'] = 'بساطة';
$string['marketingstyle4'] = 'صندوق مظلل';
$string['marketingstyle_desc'] = 'اختر من بين أساليب التسويق التالية.
سيؤدي ذلك إلى تغيير شكل صناديق التسويق و أسلوبها على الصفحة الرئيسية للموقع.';
$string['marketingtitle'] = 'العنوان';
$string['marketingtitledesc'] = 'عرض العنوان في هذا بلاطة التسويقي.
يجب عليك تضمين عنوان حتى تظهر "بلاطة التسويق".';
$string['marketingurltarget'] = 'رابط الهدف';
$string['marketingurltargetdesc'] = 'اختر كيف ينبغي فتح الرابط';
$string['marketingurltargetnew'] = 'صفحة جديدة';
$string['marketingurltargetparent'] = 'في الإطر الأصلي';
$string['marketingurltargetself'] = 'الصفحة الحالية';
$string['markettextbg'] = 'خلفية نص بلاطة التسويق';
$string['markettextbg_desc'] = 'لون الخلفية لمنطقة النص في بلاطة التسويق.';
$string['menusettings'] = 'إعدادات القائمة';
$string['modchoosercommonlyused'] = 'الاكثر استعمالاً';
$string['modchoosercommonlyusedtitle'] = 'النماذج الشائعة';
$string['modchoosercommonlyusedtitlecustom'] = '{$a}';
$string['modchoosercustomlabel'] = 'تخصيص  منتقي التسمية';
$string['modchoosercustomlabel_desc'] = 'إضافة تسمية خاصة بك لهذه القائمة الجديدة في لوحة منتقي النماذج.';
$string['modchoosersettingspage'] = 'الأنشطة / المصادر';
$string['myclasses'] = 'موادي';
$string['mycomp'] = 'كفاءاتي';
$string['mycourses'] = 'مقرراتي';
$string['mycoursesinfo'] = 'ديناميكية  قائمة المواد المسجلة و قوائم التنقل بين للمقررات';
$string['mycoursesinfodesc'] = 'يعرض قائمة ديناميكية من الموادالمسجلة للمستخدم في شريط التنقل العلوي. سيؤدي ذلك أيضًا إلى التحكم في قائمة التنقل في المادة لكل مادة على حدة.';
$string['mycoursetitle'] = 'المصطلح';
$string['mycoursetitledesc'] = 'تغيير المصطلحات رابط "المواد الخاصة بي" في القائمة المنسدلة';
$string['mycred'] = 'بيانات الاعتماد الخاصة بي';
$string['mygradestext'] = 'درجاتي';
$string['mylectures'] = 'محاضراتي';
$string['mylessons'] = 'دروسي';
$string['mymodules'] = 'نماذجي';
$string['myplans'] = 'خططي';
$string['myprofessionaldevelopment'] = 'تطويراتي';
$string['myprograms'] = 'برامجي';
$string['myprogresspercentage'] = 'النسبة %';
$string['myprogresstext'] = 'تقدّمي';
$string['mytraining'] = 'تدريبي';
$string['myunits'] = 'وحداتي';
$string['navbarcolorswitch'] = 'تبديل لون شريط التمرير';
$string['navbarcolorswitch_desc'] = 'تعمل هذه الميزة على تغيير لون شريط التنقل بناءً على دور المستخدم. سيرى الطالب لونًا واحدًا و سيشاهد المعلم لونًا آخر. يكون هذا مفيدًا عندما يغير المعلم الأدوار و يساعد على التمييز بين طريقة عرض الطالب و طريقة عرض المعلم.';
$string['navbarcolorswitch_off'] = 'لا تقم بتغيير لون شريط التنقل بناءً على الدور.';
$string['navbarcolorswitch_on'] = 'تغيير لون شريط التنقل بناءً على الدور.';
$string['navdrawerbtn'] = 'التنقل';
$string['navicon'] = 'الأيقونة';
$string['navicon1'] = 'الصفحة الرئيسية الأيقونة الأولى';
$string['navicon2'] = 'الصفحة الرئيسية الأيقونة الثانية';
$string['navicon3'] = 'الصفحة الرئيسية الأيقونة الثالثة';
$string['navicon4'] = 'الصفحة الرئيسية الأيقونة الرابعة';
$string['navicon5'] = 'الصفحة الرئيسية الأيقونة الخامسة';
$string['navicon6'] = 'الصفحة الرئيسية الأيقونة السادسة';
$string['navicon7'] = 'الصفحة الرئيسية الأيقونة السابعة';
$string['navicon8'] = 'الصفحة الرئيسية الأيقونة الثامنة';
$string['naviconbutton1textdefault'] = 'لوحة القيادة';
$string['naviconbutton2textdefault'] = 'التقويم';
$string['naviconbutton3textdefault'] = 'الشارات';
$string['naviconbutton4textdefault'] = 'جميع المواد';
$string['naviconbuttoncreatetextdefault'] = 'إنشاء مقرر';
$string['naviconbuttontext'] = 'الرابط';
$string['naviconbuttontextdesc'] = 'النص الذي يظهر أسفل الأيقونة.';
$string['naviconbuttonurl'] = 'الرابط';
$string['naviconbuttonurldesc'] = 'العنوان الذي سوف يشير الزر إليه. يمكنك الارتباط بأيّ مكان بما في ذلك المواقع الخارجية .. فقط أدخل عنوان الرابط الصحيح. إذا كان موقع Moodle الخاص بك في دليل فرعي ، فلن يعمل الرابط الافتراضي.
يرجى ضبط الرابط ليعكس الدليل الفرعي.
مثال إذا كان "moodle" هو مجلد الدليل الفرعي الخاص بك ، فسيلزم تغيير عنوان URL إلى / moodle / my /';
$string['navicondesc'] = 'اسم الرمز الذي ترغب في استخدامه.
القائمة هنا. فقط أدخل ما بعد "fa-" ، على سبيل المثال "نجمة".';
$string['naviconslidedesc'] = 'نص الرمز المقترح: سهم لأسفل. أو اختر من القائمة هنا.
فقط أدخل ما بعد "fa-" ، على سبيل المثال "نجمة".';
$string['noenrolments'] = 'ليس لديك أيّ تسجيلات حالية';
$string['nomycourses'] = 'أنت غير مسجّل في أيّ مادة.';
$string['pagelayout'] = 'منتقي التخطيط';
$string['pagelayout1'] = 'تخطيط Boost افتراضي';
$string['pagelayout2'] = 'كامل العرض / أعلى رأس الصورة';
$string['pagelayout3'] = 'توسيط المحتوى / تداخل رأس الصورة';
$string['pagelayout4'] = 'توسيط المحتوى / صورة رأس الشاشة الكاملة';
$string['pagelayout5'] = 'صورة تخطيط  Boost الافتراضية / مربع عنوان المادة';
$string['pagelayout_desc'] = 'اختر من بين التخطيطات التالية. تتطلب بعض تخطيطات الصفحة تعديلات إضافية على صفحة معايرات الإعداد المسبق.
تأكد من الانتباه إلى: تعلّم تباعد المحتوى ، و ارتفاع صورة الرأس ، و حشو المحتوى ، حيث سيساعد ذلك في ضبط موضع صورة الرأس و الحشو على يسار و يمين محتوى التعلم الرئيسي. شاهد تعليمات لـ Fordson 3.5 Theme';
$string['pinterest'] = 'رابط موقع Pinterest';
$string['pinterestdesc'] = 'أدخل رابط صفحتك Pinterest . (أي http://pinterest.com/)';
$string['pluginname'] = 'Fordson';
$string['preset'] = 'مظهر سابق';
$string['presetadjustmentsettings'] = 'التعديلات السابقة';
$string['preset_desc'] = 'اختر إعدادًا مسبقًا لتغيير المظهر على نطاق واسع.
شاهد تعليمات لـ Fordson 3.5 Theme';
$string['presetfiles'] = 'ملفات إضافية لمظهر سابق';
$string['presetfiles_desc'] = 'يمكن استخدام ملفات الإعداد المسبق لتغيير مظهر السمة بشكل كبير. راجع https://docs.moodle.org/dev/Boost_Presets للحصول على معلومات حول إنشاء و مشاركة ملفات الإعداد المسبق الخاصة بك.';
$string['presets_settings'] = 'المسبقة';
$string['privacy:metadata'] = 'لا يقوم مظهر Fordson بتخزين أيّ بيانات مستخدم فردية.';
$string['qbank'] = 'بنك الأسئلة';
$string['qbank_desc'] = 'إنشاء و تنظيم أسئلة الاختبارات';
$string['rawscss'] = 'SCSS الأصلية';
$string['rawscss_desc'] = 'استخدم هذا الحقل لتوفير كود SCSS الذي سيتم حقنه في نهاية ورقة الأنماط.';
$string['rawscsspre'] = 'SCSS الأولية و الأصلية';
$string['rawscsspre_desc'] = 'في هذا الحقل ، يمكنك توفير تهيئة كود SCSS ، سيتم حقنه قبل أيّ شيء آخر.
معظم الوقت سوف تستخدم هذا الإعدادات لتحديد المتغيرات.';
$string['region-fp-a'] = 'عمود A';
$string['region-fp-b'] = 'عمود B';
$string['region-fp-c'] = 'عمود C';
$string['region-side-pre'] = 'اليمين';
$string['sectionlayout'] = 'منتقي نمط الأقسام';
$string['sectionlayout1'] = 'الافتراضي Boost';
$string['sectionlayout2'] = 'بطاقة ملاحظة بالغامق';
$string['sectionlayout3'] = 'علامات تبويب المجلد';
$string['sectionlayout4'] = 'لوحة القص';
$string['sectionlayout5'] = 'صندوق بسيط';
$string['sectionlayout6'] = 'عنوان القسم البارز';
$string['sectionlayout7'] = 'متعلم جامعي';
$string['sectionlayout8'] = 'متعلم الشركات';
$string['sectionlayout_desc'] = 'اختر من الموضوع التالي / أنماط القسم الأسبوعية.
شاهد تعليمات لمظهر  Fordson 3.5';
$string['section_mods'] = 'النماذج:';
$string['setting_navdrawersettings'] = 'إعدادات شريط التنقل';
$string['setting_navdrawersettings_desc'] = 'تمكين ميزة درج التنقل  Boost .
لا يتطلب Fordson درج للتنقل. لقد استبدلناها بقائمة منسدلة للانتقال إلى القسم.
يمكنك إعادة تمكين درج التنقل أدناه.';
$string['showactivitynav'] = 'إظهار التنقل في النشاط';
$string['showactivitynav_desc'] = 'ألغ تحديد هذا الخيار لإيقاف التنقل في النشاط أسفل صفحات النشاط.';
$string['showalltomanager'] = 'عرض جميع الأنشطة و المصادر للمدراء';
$string['showalltomanager_desc'] = 'سيتيح هذا الإعداد للمستخدمين الذين لديهم دور مدير الرؤية و الوصول إلى جميع الأنشطة و الموارد حتى إذا تم تعيينه لإظهار القائمة المخصصة فقط.
بشكل عام ، يتم إعطاء دور المدير للمستخدم على مستوى الموقع أو الفئة. سيظل المعلمون يرون القائمة المخصصة فقط. يتم تحديد هذه الميزة بإذن المستخدم:
عرض شجرة إدارة الموقع - moodle / site: configview.
سيرى مسؤولوا الموقع دائمًا جميع الأنشطة و المصادر.';
$string['showblockregions'] = 'إظهار مناطق الكتل الإضافية في الصفحات الأمامية';
$string['showblockregions_desc'] = 'قم بتشغيل ثلاث مناطق للكتل على الصفحة الأولى للموقع.
تظهر هذه تحت أيقونة شريط التنقل.';
$string['showcourseadminstudents'] = 'عرض إعدادات الطالب للمادة على المدير';
$string['showcourseadminstudents_desc'] = 'هذا يعرض إعدادات المادة للطلاب.
و هذا مطلوب إذا كنت تريد السماح لهم بإلغاء التسجيل في المواد.';
$string['showcourseheaderimage'] = 'عرض صور المادة';
$string['showcourseheaderimage_desc'] = 'اسمح للمعلمين بتخصيص صورة رأس المادة عن طريق تحميل ملف صورة في إعدادات المادة.';
$string['showcustomlogin'] = 'قم بتشغيل تسجيل الدخول المخصص';
$string['showcustomlogin_desc'] = 'يجب تشغيل هذا لتفعيل للإعدادات المخصصة أدناه.';
$string['showloginform'] = 'عرض نموذج تسجيل الدخول';
$string['showloginform_desc'] = 'قم بإلغاء التحديد لإخفاء نموذج تسجيل الدخول المخصص على الصفحة الرئيسية .. للمستخدمين الذين تم تسجيل خروجهم.';
$string['shownavclosed'] = 'درج التنقل مغلق بشكل افتراضي';
$string['shownavclosed_desc'] = 'إظهار درج التنقل منغلق لجميع المستخدمين افتراضيًا في كل صفحة.';
$string['shownavdrawer'] = 'إظهار درج التنقل';
$string['shownavdrawer_desc'] = 'ألغت Fordson الحاجة إلى درج التنقل من خلال استخدام قائمة منسدلة للتنقل داخل المادة.
إذا كان يجب عليك استخدام درج التنقل ، فيمكنك تحديد هذا المربع لإعادة تمكينه.';
$string['showonlycustomactivities'] = 'إظهار القائمة المخصصة فقط';
$string['showonlycustomactivities_desc'] = 'إذا تم تحديده ، فسيتم عرض القائمة المخصصة فقط. يتيح ذلك للمنظمة اختيار الأنشطة و المصادر التي سيتم استخدامها في المقرر و اختيارها. تحذير: يجب أن يكون لديك قائمة بأنشطة / مصادر منفصلة مفصولة بفواصل في مربع النص أعلاه. و إلا فلن ترى أي أنشطة أو موارد عند تحرير المادة.';
$string['showonlygroupteachers'] = 'فقط أظهر مجموعة المعلمين';
$string['showonlygroupteachers_desc'] = 'عند التمكين ، سيتم عرض المعلمين فقط  في نفس المجموعة .. مثل الطالب في لوحة إدارة الطالب للمادة .';
$string['showslideshow'] = 'تنشيط عرض الشرائح';
$string['showslideshow_desc'] = 'حدد هذا الخيار لتشغيل ميزة عرض الشرائح.';
$string['showstudentcompletion'] = 'إظهار إكمال الطالب';
$string['showstudentcompletion_desc'] = 'إظهار شريط  إتمام الطالب في لوحة الطالب. حتى مع تحديد هذا الخيار ، يجب أن تكون المادة قيد التشغيل حتى يتم عرضها.';
$string['showstudentgrades'] = 'عرض درجات الطلاب';
$string['showstudentgrades_desc'] = 'إظهار رابط دفتر التقديرات للطالب في لوحة معلومات الطالب. حتى مع تحديد هذا الخيار ، يجب أن يكون البرنامج الدراسي قيد التشغيل لإظهار درجات الطالب.';
$string['siteadminquicklink'] = 'إدارة الموقع';
$string['skype'] = 'حساب Skype';
$string['skypedesc'] = 'أدخل اسم مستخدم Skype الخاص بحسابك على Skype';
$string['slide1info'] = 'الشريحة 1';
$string['slide1infodesc'] = 'ثفاصيل الشريحة 1.';
$string['slide2info'] = 'الشريحة 2';
$string['slide2infodesc'] = 'تفاصيل الشريحة 2 .';
$string['slide3info'] = 'الشريحة 3';
$string['slide3infodesc'] = 'تفاصيل الشريحة 3 .';
$string['slidecontent'] = 'وصف الشريحة';
$string['slidecontent_desc'] = 'أضف وصفًا لهذه الشريحة.';
$string['slideimage'] = 'صورة الشريحة';
$string['slideimage_desc'] = 'أضف صورة خلفية لهذه الشريحة.';
$string['sliderinfo'] = 'زر أيقونة الشريحة الخاصة';
$string['sliderinfodesc'] = 'هذا الزر سيعرض / يخفي مربع نص خاص ينزلق من شريط التنقل في الأيقونة.
هذا مثالي لعرض المواد أو تقديم المساعدة أو إدراج تدريب المطلوب للموظفين.';
$string['slideshowheight'] = 'ارتفاع عرض الشرائح';
$string['slideshowheight_desc'] = 'اضبط ارتفاع عرض الشرائح.';
$string['slideshowsettings'] = 'عرض الشرائح';
$string['slidetextbox'] = 'مربع نص الشريحة';
$string['slidetextbox_desc'] = 'سيتم عرض محتوى مربع النص هذا عند الضغط على زر الشريحة.';
$string['slidetitle'] = 'عنوان الشريحة';
$string['slidetitle_desc'] = 'أدخل عنوانًا لهذه الشريحة.';
$string['sociallink'] = 'تخصيص الرابط الاجتماعي';
$string['sociallinkdesc'] = 'أدخل رابط الوسائط الاجتماعية المخصص الخاص بك. (أي http://dearbornschools.org)';
$string['sociallinkicon'] = 'أيقونة الرابط';
$string['sociallinkicondesc'] = 'أدخل اسم fontawesome للأيقونة الخاصة بك
يمكن العثور على قائمة كاملة بأيقونات FontAwesome على https://fontawesome.com/v4.7.0/icons/';
$string['socialnetworks'] = 'الشبكات الإجتماعية';
$string['studentdashboardtextbox'] = 'رسالة لوحة القيادة  للطالب';
$string['studentdashboardtextbox_desc'] = 'أضف رسالة للطلاب في لوحة القيادة للطلاب..  في كل صفحة من صفحات المقرر.';
$string['studentdashbutton'] = 'لوحة القيادة للمادة';
$string['textcontentinfo'] = 'محتوى مخصص';
$string['textcontentinfodesc'] = 'استخدم مربعات النص أدناه لإضافة معلومات مخصصة للمستخدمين.';
$string['thiscourse'] = 'هذا المقرر';
$string['thismyclasses'] = 'هذا الفصل';
$string['thismycomp'] = 'هذه الكفاءة';
$string['thismycourses'] = 'هذه المادة';
$string['thismycred'] = 'هذا الاعتماد';
$string['thismylectures'] = 'هذه المحاضرة';
$string['thismylessons'] = 'هذا الدرس';
$string['thismymodules'] = 'هذه الوحدة';
$string['thismyplans'] = 'هذه الخطة';
$string['thismyprofessionaldevelopment'] = 'هذا التطوير المهني';
$string['thismyprograms'] = 'هذا البرنامج';
$string['thismytraining'] = 'هذا التدريب';
$string['thismyunits'] = 'هذه الوحدة';
$string['titletooltip'] = 'تلميح لعنوان المقرر';
$string['titletooltip_desc'] = 'إذا كنت تستخدم عنوان المادة المقلّم ، يمكنك استخدام التلميحات التي ستعرض عنوان المادة بالكامل على شكل تلميح . حدد هذا المربع لتشغيل التلميحات.';
$string['togglemarketing'] = 'موقع بلاطة التسويق';
$string['togglemarketing_desc'] = 'حدد المكان الذي ستوضع به مربعات التسويق في الصفحة الرئيسية.';
$string['topnavbarbg'] = 'شريط التنقل العلوي الافتراضي';
$string['topnavbarbg_desc'] = 'لون خلفية المحتوى لشريط التنقل العلوي.';
$string['topnavbarteacherbg'] = 'شريط التنقل العلوي في دور المعلم';
$string['topnavbarteacherbg_desc'] = 'لون خلفية المحتوى لشريط التنقل العلوي عندما يكون المستخدم مدرسًا. يجب تشغيل هذه الميزة في إعدادات القائمة.
يرجى الاطلاع على إعداد مفتاح لون شريط التنقل.';
$string['trimsummary'] = 'تقليم ملخص المادة';
$string['trimsummary_desc'] = 'أدخل رقمًا لقص طول الملخص. يمثل هذا الرقم الأحرف التي سيتم عرضها.';
$string['trimtitle'] = 'تقليم عنوان المادة';
$string['trimtitle_desc'] = 'أدخل رقمًا لتقليص طول العنوان. يمثل هذا الرقم الأحرف التي سيتم عرضها.';
$string['tumblr'] = 'رابط Tumblr';
$string['tumblrdesc'] = 'أدخل رابط خاص Tumblr   (أي http://www.tumblr.com)';
$string['twitter'] = 'رابط تويتر';
$string['twitterdesc'] = 'أدخل رابطكم في تويتر. (أي http://www.twitter.com/)';
$string['userlinks'] = 'روابط المستخدم';
$string['userlinks_desc'] = 'إدارة طلابك';
$string['viewsectionmodules'] = 'عرض وحدات القسم';
$string['vimeo'] = 'رابط Vimeo';
$string['vimeodesc'] = 'أدخل رابطك  لقناة Vimeo . (أي http://vimeo.com/)';
$string['vk'] = 'رابط VKontakte';
$string['vkdesc'] = 'أدخل رابط لصفحة VKontakte . (أي http://www.vk.com/)';
$string['website'] = 'رابط الموقع';
$string['websitedesc'] = 'أدخل رابط لموقع الويب الخاص بك. (أي http://dearbornschools.org)';
$string['youtube'] = 'رابط اليوتيوب';
$string['youtubedesc'] = 'أدخل رابط قناتك على YouTube. (أي http://www.youtube.com/)';
