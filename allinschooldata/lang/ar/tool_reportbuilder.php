<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_reportbuilder', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_reportbuilder
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accesspeoplecapabilities'] = 'يمكن للأشخاص الذين لديهم قدرات (عرض / إدارة) التقارير عرض هذا التقرير على جميع المستخدمين';
$string['accesspeopleonlycapabilities'] = 'يمكن فقط للأشخاص الذين لديهم قدرات على النظام (عرض / إدارة التقارير) عرض هذا التقرير';
$string['accesspeopleorgmanagers'] = 'يمكن للأشخاص في المواقع التالية عرض هذا التقرير على المستخدمين في فرقهم';
$string['accesstab'] = 'الاذن بالدخول إلى';
$string['actions'] = 'اجراءات';
$string['addacondition'] = 'إضافة شرط ...';
$string['addafilter'] = 'إضافة عامل تصفية';
$string['addcolumn'] = 'إضافة العمود';
$string['adddefault'] = 'إضافة الإعدادات الافتراضية';
$string['adddefault_help'] = 'يحدد كل مصدر تقريرالأعمدة والظروف وعوامل التصفية ( فلتر). إذا كنت تريد إنشاء التقرير باستخدام هذه الإعدادات الافتراضية ، فحدد هذا الحقل';
$string['addemails'] = 'ًإضافة رسائل البريد الإلكتروني يدويا';
$string['addfieldbyname'] = 'الى التقرير \'{$a}\' إضافة حقل';
$string['addreport'] = 'تقرير جديد';
$string['addschedule'] = 'جدول جديد';
$string['addusers'] = 'إضافة مستخدمين يدوياً';
$string['aggregation_avg'] = 'المعدل';
$string['aggregation_count'] = 'التعداد';
$string['aggregation_countdistinct'] = 'عد القيّم الفريدة';
$string['aggregation_groupconcat'] = 'قيّم منفصلة بفواصل';
$string['aggregation_groupconcatdistinct'] = 'قيّم مختلفة منفصلة بفواصل';
$string['aggregation_max'] = 'الحد الأقصى';
$string['aggregation_min'] = 'الحد الأدنى';
$string['aggregation_percent'] = 'النسبة المئوية';
$string['aggregation_sum'] = 'المجموع';
$string['aggregation_unique'] = 'القيّم الفريدة';
$string['and'] = 'و';
$string['asc'] = 'يحتوي العمود \'{$a}\' على اتجاه فرز تصاعدي';
$string['audience'] = 'الجماع';
$string['audiencejobadd'] = 'أضف الوظيفة';
$string['audiencejobremove'] = 'إزالة الوظيفة';
$string['audiencejobs'] = 'الوظائف';
$string['audiencejobsempty'] = 'لا وظائف مضافة';
$string['audiencejobs_help'] = 'يمكن للمستخدمين عرض هذا التقرير إذا كانوا ينتمون إلى واحدة من الوظائف المحددة أدناه';
$string['basicinformation'] = 'المعلومات الأساسية';
$string['cachedef_userreports'] = 'التقارير المخصصة التي تمكّن المستخدم الى الوصول اليها';
$string['checkboxanyvalue'] = 'أي قيمة';
$string['checkboxischecked'] = 'نعم';
$string['checkboxisnotchecked'] = 'كلا';
$string['choose'] = 'اختر';
$string['conditiondatastorecoursefullname'] = 'مقررالاسم الكامل من مخزن البيانات';
$string['conditiondatastoreuserfirstname'] = 'الاسم الأول للمستخدم من مخزن البيانات';
$string['conditiondatastoreuserlastname'] = 'اسم عائلة المستخدم من مخزن البيانات';
$string['conditionshelp'] = 'الشروط';
$string['conditionshelp_help'] = 'قائمة الشروط المحددة مسبقًا والتي تنطبق دائمًا عند عرض هذا التقرير.';
$string['conditionstab'] = 'الشروط';
$string['confirm'] = 'التأكيد';
$string['confirmdeletecondition'] = '؟ \'{$a}\' هل أنت متأكد من حذف شرط';
$string['confirmdeleteschedule'] = 'هل أنت متأكد من حذف جدول \'{$a}\'
وجميع البيانات المرتبطة ؟ لا يمكن التراجع عن هذا الإجراء.';
$string['confirmresetallconditions'] = 'هل أنت متأكد من إعادة تعيين جميع الشروط؟';
$string['confirmresetconditions'] = 'هل أنت متأكد من إعادة تعيين  \'{$a}\'
شرط ؟';
$string['confirmsendschedule'] = 'هل أنت متأكد من رغبتك في الانضمام الى قائمة الانتظار \'{$a}\' للإرسال؟';
$string['course_completion_days_course'] = 'أيام أخذ المقرر';
$string['course_completion_days_enrolled'] = 'أيام التسجيل في المقرر';
$string['course_completion_progress'] = 'التقدم';
$string['course_completion_progress_percent'] = 'التقدم %';
$string['course_completion_reaggregate'] = 'وقت اعادة التجميع';
$string['course_completion_timecompleted'] = 'وقت الاكتمال';
$string['course_completion_timeenrolled'] = 'وقت التسجيل';
$string['course_completion_timestarted'] = 'وقت البدء';
