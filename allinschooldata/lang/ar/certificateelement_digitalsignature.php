<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'certificateelement_digitalsignature', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   certificateelement_digitalsignature
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['digitalsignature'] = 'توقيع إلكتروني';
$string['errormultipleimages'] = 'يمكنك تحديد صورة واحدة فقط (سواء تم تحميلها أو مشاركتها)';
$string['height'] = 'ارتفاع';
$string['height_help'] = 'ارتفاع العنصر في مم. يمكن حساب القيمة تلقائيًا للصور التي تم تحميلها / مشاركتها إذا تم تعيينها على 0 ، وإلا يجب تحديدها';
$string['pluginname'] = 'توقيع إلكتروني';
$string['privacy:metadata'] = 'لا يقوم ملحق التوقيع الالكتروني بتخزين أي بيانات شخصية.';
$string['signaturecontactinfo'] = 'معلومات اتصال التوقيع';
$string['signaturelocation'] = 'موقع التوقيع';
$string['signaturename'] = 'اسم التوقيع';
$string['signaturepassword'] = 'كلمة مرور التوقيع';
$string['signaturereason'] = 'سبب التوقيع';
$string['width'] = 'عرض';
$string['width_help'] = 'عرض العنصر في مم. يمكن حساب القيمة تلقائيًا للصور التي تم تحميلها / مشاركتها إذا تم تعيينها على 0 ، وإلا يجب تحديدها';
