<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'webinar', language 'ar', branch 'MOODLE_26_STABLE'
 *
 * @package   webinar
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addingsession'] = 'إضافة جلسة جديدة';
$string['addmanageremailaddress'] = 'إضافة عنوان البريد الإلكتروني للمدير';
$string['addmanageremailinstruction'] = 'لم تقم بإدخال عنوان البريد الإلكتروني لمديرك من قبل. يرجى إدخالها أدناه للتسجيل في هذه الجلسة.';
$string['addnewfield'] = 'إضافة حقل مخصص جديد';
$string['addnewfieldlink'] = 'إنشاء حقل مخصص جديد';
$string['addnewnotice'] = 'إضافة إشعار موقع جديد';
$string['addnewnoticelink'] = 'إنشاء إشعار موقع جديد';
$string['addremoveattendees'] = 'إضافة / إزالة الحضور';
$string['addsession'] = 'أضف جلسة جديدة';
$string['addstudent'] = 'إضافة طالب';
$string['alllocations'] = 'كل المواقع';
$string['allowoverbook'] = 'السماح بالحجوزات الزائدة';
$string['allsessionsin'] = 'كل الجلسات';
$string['alreadysignedup'] = 'لقد اشتركت بالفعل في هذه الندوة.';
$string['answer'] = 'تسجيل الدخول';
$string['answercancel'] = 'تسجيل الخروج';
$string['approvalreqd'] = 'الموافقة مطلوبة';
$string['approve'] = 'يوافق';
$string['assessmentyour'] = 'تقييمك';
$string['attendance'] = 'الحضور';
$string['attendanceinstructions'] = 'حدد المستخدمين الذين حضروا الجلسة:';
$string['attendedsession'] = 'حضر الجلسة';
$string['attendees'] = 'الحاضرين';
$string['attendeestablesummary'] = 'الأشخاص الذين يخططون لهذه الجلسة أو يحضرونها.';
$string['booked'] = 'حجز';
$string['bookingcancelled'] = 'لقد ألغيت حجزك لـ {$a}.';
$string['bookingcompleted'] = 'تم الانتهاء من حجزك.';
$string['bookingfull'] = 'الحجز مكتمل';
$string['bookingopen'] = 'الحجز مفتوح';
$string['bookingstatus'] = 'لقد حجزت للجلسة التالية';
$string['calendareventdescriptionbooking'] = 'أنت محجوز لهذه الجلسة.';
$string['calendareventdescriptionsession'] = 'لقد أنشأت هذه الندوة.';
$string['calendaroptions'] = 'خيارات التقويم';
$string['cancelbooking'] = 'إلغاء الحجز';
$string['cancelbookingfor'] = 'إلغاء الحجز';
$string['cancellation'] = 'الإلغاء';
$string['cancellationconfirm'] = 'هل أنت متأكد من رغبتك بحذف هذه الجلسة؟';
$string['cancellationmessage'] = 'رسالة الإلغاء';
$string['cancellations'] = 'الإلغاءات';
$string['cancellationsent'] = 'يجب أن تتلقى على رسالة الإلغاء على البريد إلكتروني فوراً.';
$string['cancellationsentmgr'] = 'يجب أن يتلقى مديرك رسالة الإلغاء على البريد الالكتروني فوراً.';
$string['cancellationstablesummary'] = 'قائمة الأشخاص الذين ألغوا اشتراكاتهم في الجلسة.';
$string['cancelreason'] = 'السبب';
$string['capacity'] = 'السعة';
$string['changemanageremailaddress'] = 'تغيير البريد الالكتروني للمدير';
$string['changemanageremailinstruction'] = 'يرجى إدخال عنوان البريد الإلكتروني لمديرك الحالي أدناه.';
$string['clearall'] = 'مسح الكل';
$string['closed'] = 'مغلق';
$string['conditions'] = 'الحالات';
$string['conditionsexplanation'] = 'يجب استيفاء جميع هذه المعايير لإظهار الإشعار في تقويم التدريب:';
$string['confirm'] = 'تأكيد';
$string['confirmanager'] = 'تأكيد البريد الالكتروني للمدير';
$string['confirmation'] = 'التأكيد';
$string['confirmationmessage'] = 'رسالة التأكيد';
$string['confirmationsent'] = 'يجب أن تتلقى رسالة التأكيد بالبريد الالكتروني حالاً.';
$string['confirmationsentmgr'] = 'يجب أن يتلقى مديرك رسالة التأكيد بالبريد الالكتروني حالاً.';
$string['confirmcancelbooking'] = 'تأكيد إلغاء الحجز';
$string['confirmed'] = 'تم التأكيد';
$string['confirmmanageremailaddress'] = 'تأكيد البريد الإلكتروني للمدير';
$string['confirmmanageremailaddressquestion'] = 'هل لا يزال هذا عنوان البريد الإلكتروني لمديرك؟';
$string['confirmmanageremailinstruction1'] = 'سبق لك إدخال ما يلي كعنوان بريد إلكتروني لمديرك:';
$string['confirmmanageremailinstruction2'] = 'هل لا يزال هذا عنوان البريد الإلكتروني لمديرك؟';
$string['copy'] = 'نسخ';
$string['copyingsession'] = 'نسخ كجلسة جديدة';
$string['copysession'] = 'نسخ الجلسة';
$string['cost'] = 'التكلفة';
$string['costheading'] = 'تكلفة الجلسة';
$string['currentstatus'] = 'الحالة الحالية';
$string['customfieldsheading'] = 'الحقول المخصصة للجلسة';
$string['date'] = 'التاريخ';
$string['dateadd'] = 'إضافة تاريخ جديد';
$string['dateremove'] = 'إزالة هذا التاريخ';
$string['datetext'] = 'لقد سجلت الدخول للتاريخ';
$string['decidelater'] = 'تقرر لاحقاً';
$string['deleteall'] = 'حذف الكل';
$string['deletesession'] = 'حذف الجلسة';
$string['deletesessionconfirm'] = 'هل أنت متأكد تمامًا من أنك تريد حذف هذه الجلسة وجميع الاشتراكات في هذه الجلسة؟';
$string['deletingsession'] = 'حذف الجلسة';
$string['description'] = 'نص المقدمة';
$string['details'] = 'التفاصيل';
$string['discountcode'] = 'رمز الخصم';
$string['discountcost'] = 'تكلفة الخصم';
$string['due'] = 'بسبب';
$string['duration'] = 'المدة الزمنية';
$string['early'] = '\\{$a} مبكراً';
$string['edit'] = 'تحرير';
$string['editingsession'] = 'جلسة التحرير';
$string['editsession'] = 'تحرير الجلسة';
$string['email:instrmngr'] = 'إشعار للمدير';
$string['emailmanager'] = 'ارسال إشعار للمدير';
$string['email:message'] = 'الرسالة';
$string['email:subject'] = 'الموضوع';
$string['emptylocation'] = 'الموقع فارغ';
$string['enrolled'] = 'المسجلين';
$string['error:addalreadysignedupattendee'] = '{$a} سجل بالفعل في هذه الندوة';
$string['error:addattendee'] = 'لا يمكن إضافة {$a} لهذه الجلسة.';
$string['error:cancelbooking'] = 'هنالك مشكلة في إلغاء حجزك';
$string['sessioninprogress'] = 'الجلسة قيد التنفيذ';
