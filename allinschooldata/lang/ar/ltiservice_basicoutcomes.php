<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'ltiservice_basicoutcomes', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   ltiservice_basicoutcomes
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allow'] = 'استخدم هذه الخدمة لقبول التقديرات من الأداة';
$string['ltiservice_basicoutcomes'] = 'المخرجات الأساسية';
$string['ltiservice_basicoutcomes_help'] = 'اسمح للأداة بحفظ و استرجاع درجاتها إلى عمود دفتر التقديرات المرتبط بكل رابط.';
$string['notallow'] = 'لا تستخدم هذه الخدمة';
$string['pluginname'] = 'خدمة المخرجات الأساسية';
$string['privacy:metadata:externalpurpose'] = 'يتم إرسال هذه المعلومات إلى مزود أدوات التعلم المشترك (LTI) الخارجي.';
$string['privacy:metadata:grade'] = 'أدوات الدرجات للمستخدم الذي يستخدم أدوات التعلم المشترك (LTI) كمستفيد.';
$string['privacy:metadata:userid'] = 'معرف المستخدم  الذي يستخدم أدوات التعلم المشترك (LTI) كمستفيد.';
