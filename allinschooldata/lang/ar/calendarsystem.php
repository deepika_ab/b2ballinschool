<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'calendarsystem', language 'ar', branch 'MOODLE_26_STABLE'
 *
 * @package   calendarsystem
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['calendarsystem'] = 'نظام التقويم';
$string['calendarsystems'] = 'نظام التقويم';
$string['calendarsystemsmanage'] = 'إدارة نظام التقويم';
$string['changecalendar'] = 'تغير';
$string['checkforupdates'] = 'البحث عن تحديثات';
$string['configcalendarsystem'] = 'التقويم الافتراضي';
$string['forcecalendarsystem'] = 'إجبار التقويم';
$string['helpcalendarsystem'] = 'المساعد لنظام التقويم';
$string['preferredcalendar'] = 'التقويم المفضل';
$string['system'] = 'التقويم';
$string['updateavailable'] = 'يتوفر إصدر جديد لنظام التقويم';
$string['updateavailable_moreinfo'] = 'المزيد من المعلومات';
$string['updateavailable_version'] = '{a$}الإصدار';
