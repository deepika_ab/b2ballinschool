<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'ltiservice_memberships', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   ltiservice_memberships
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allow'] = 'استخدم هذه الخدمة لاسترداد معلومات الأعضاء حسب إعدادات الخصوصية';
$string['ltiservice_memberships'] = 'أسماء IMS LTI و الأدوار المعطاة';
$string['ltiservice_memberships_help'] = 'اسمح للأداة باسترداد معلومات العضو من المادة باستخدام خدمة IMS LTI للأسماء و الأدوار المعطاة.
سيتم تطبيق إعدادات الخصوصية. بالنسبة لطلبات مستوى المادة، ستستند هذه إلى إعدادات ضبط الأدوات.
إذا كنت ترغب في إرسال مثل هذه التفاصيل دائمًا ، فلا تفوض الخيار أمام المعلمين. ستستخدم طلبات مستوى الارتباط دائمًا إعدادات الخصوصية التي تنطبق على الرابط.';
$string['notallow'] = 'لا تستخدم هذه الخدمة';
$string['pluginname'] = 'الأسماء و الأدوار المقدمة من خدمة  أدوات التعلم المشترك (LTI)';
$string['privacy:metadata:email'] = 'البريد الإلكتروني للمستخدم الذي يستخدم  أدوات التعلم المشترك (LTI) كعميل.';
$string['privacy:metadata:externalpurpose'] = 'يتم إرسال هذه المعلومات إلى مزود  أدوات التعلم المشترك (LTI) الخارجي.';
$string['privacy:metadata:firstname'] = 'اسم المستخدم الذي يستخدم  أدوات التعلم المشترك (LTI) كعميل.';
$string['privacy:metadata:fullname'] = 'الاسم الكامل للمستخدم الذي يستخدم LTI كعميل.';
$string['privacy:metadata:lastname'] = 'اسم العائلة للمستخدم الذي يستخدم LTI كعميل .';
$string['privacy:metadata:userid'] = 'معرف المستخدم الذي يستخدم LTI كعميل .';
$string['privacy:metadata:useridnumber'] = 'رقم معرف المستخدم الذي يستخدم LTI كعميل';
