<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_intelliboard', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   local_intelliboard
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['a0'] = 'لوحة الكفاءة';
$string['a1'] = 'الكفاءة';
$string['a10'] = 'المتدربين المسجلين';
$string['a11'] = 'قائمة الكفاءاة المشارة في الدورة';
$string['a12'] = 'حالة المتدرب';
$string['a13'] = 'اسم الكفاءة';
$string['a14'] = 'وضع التشغيل';
$string['a15'] = 'تشغيل التأشير';
$string['a17'] = 'التقييم';
$string['a19'] = 'تاريخ تقييم الكفاءة';
$string['a2'] = 'المهارة';
$string['a20'] = 'مقيم الكفاءة';
$string['a21'] = 'أنشطة المستخدم النشطك';
$string['a22'] = 'الكفاءة المحققة';
$string['a23'] = 'تقييم الكفاءة';
$string['a24'] = 'الأدلة';
$string['a25'] = 'المتدربون المكتلمون';
$string['a26'] = 'يعرض هذا الجدول عدد الكفاءات المخصصة للدورة ، والمتعلمين الذين تم تقييمهم (سواء أكانوا بارعين أم لا) ، والمتعلمين الذين تم تعيينهم بارعين في الكفاءة.';
$string['a27'] = 'من';
$string['a28'] = 'التفاصيل';
$string['a29'] = 'تشغيل لوحة مؤشر الكفاءة';
$string['a3'] = 'نشاطات المسجلين';
$string['a30'] = 'تشغيل تقرير الكفاءة';
$string['a31'] = 'الواجهة';
$string['a32'] = 'خطة التدريب';
$string['a33'] = 'تقييم المتقنات';
$string['a34'] = 'تقييم ما لم يتقن';
$string['a35'] = 'اللغير مقيم';
$string['reports'] = 'تقارير';
$string['time_period_due'] = 'فترة زمنية';
