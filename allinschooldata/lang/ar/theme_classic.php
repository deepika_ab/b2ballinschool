<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_classic', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_classic
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['choosereadme'] = 'Classic هو مظهر قابل للتخصيص بدرجة كبيرة ، تعتمد على Boost ، مع تصميم تنقل بديل. الغرض من هذا النسق هو الاستخدام مباشرة ، أو كمسألة رئيسية عند إنشاء أو تحديث المظاهر المخصصة لاستخدام Bootstrap 4.';
$string['configtitle'] = 'Classic';
$string['navbardark'] = 'استخدم شريط التنقل المظلم';
$string['navbardarkdesc'] = 'قم بتبديل ألوان النص و الخلفية لشريط التنقل في أعلى الصفحة بين مظلم و مضئ.';
$string['pluginname'] = 'Classic';
$string['preset'] = 'مظهر سابق';
$string['preset_desc'] = 'اختر إعدادًا مسبقًا لتغيير سمة المظهر على نطاق واسع.';
$string['presetfiles'] = 'ملفات محددة مسبقاً للمظهر';
$string['presetfiles_desc'] = 'يمكن استخدام ملفات الإعداد المسبق لتغيير سمة المظهر بشكل كبير. راجع إعدادات Boost المسبقة للحصول على معلومات حول إنشاء و مشاركة ملفات الإعداد المسبق الخاصة بك ، و انظر لمستودع الإعدادات المسبقة للإعدادات المسبقة التي شاركها الآخرون.';
$string['privacy:metadata'] = 'المظهر Classic لا يخزن أيّ بيانات شخصية.';
$string['region-side-post'] = 'يمين';
$string['region-side-pre'] = 'يسار';
$string['sitetitle'] = 'عنوان الموقع';
