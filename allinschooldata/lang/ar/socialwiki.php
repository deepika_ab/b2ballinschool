<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'socialwiki', language 'ar', branch 'MOODLE_32_STABLE'
 *
 * @package   socialwiki
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['ratingmode'] = 'وضع التصنيف';
$string['recommended'] = 'صفحات موصي بها';
$string['removepages'] = 'إزالة صفحات';
$string['restore'] = 'استعادة';
$string['restoreconfirm'] = 'هل أنت متأكد أنك تريد استعادة الإصدار #{$a}?';
$string['restorethis'] = 'استعادة هذا الإصدار';
$string['restoreversion'] = 'استعادة النسخة القديمة';
$string['restoring'] = 'استعادة نسخة #{$a}';
$string['return'] = 'تراجع';
$string['save'] = 'حفظ';
$string['savingerror'] = 'خطأ في الحفظ';
$string['searchcontent'] = 'البحث في محتوى الصفحة';
$string['searchresult'] = 'نتائج البحث عن:';
$string['searchresults_empty'] = 'لا نتائج';
$string['searchterms'] = 'مصطلحات البحث';
$string['title'] = 'عنوان';
$string['unfollow'] = 'الغاء المتابعة';
$string['unfollow_tip'] = 'انقر لإلغاء  متابعة هذا المُستخدم';
$string['unknownfirstname'] = 'غير معروف';
$string['unknownlastname'] = 'مُستخدم';
$string['updated'] = 'تحديث';
$string['updated_help'] = 'منذ متى تم إنشاء الصفحة.';
$string['updatedpages'] = 'الصفحات التي تم تحديثها';
$string['upload'] = 'تحميل وحذف';
$string['uploadactions'] = 'الإجراءات';
$string['uploadfiletitle'] = 'مرفقات';
$string['uploadname'] = 'اسم الملف';
$string['uploadtitle'] = 'إرفاق الملفات';
$string['userdata'] = 'بيانات المستخدم';
$string['userdata_help'] = 'وتشمل بيانات المستخدم :أسماء مؤلفي الصفحاتة ، الإعجابات، وكذلك المُتابعات.';
$string['userfaves'] = 'الصفحات المُفضَّلة';
$string['userfaves_empty'] = 'هذا المستخدم ليس لديه صفحات المُفضلة.';
$string['userpages'] = 'الصفحات التي تم إنشاؤها';
$string['versions'] = 'الإصدارات';
$string['view'] = 'عرض';
$string['viewallversions'] = 'عرض جميع الإصدارات';
$string['viewcurrent'] = 'الإصدار الحالي';
$string['views'] = 'المشاهدات';
$string['wikiimage'] = 'صورة';
$string['wikiintro'] = 'الوصف';
$string['wikiunorderedlist'] = 'قائمة غير مرتبة';
