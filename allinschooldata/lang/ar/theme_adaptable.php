<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_adaptable', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_adaptable
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aboutme'] = 'نبذة عنا';
$string['actingasrole'] = 'أنت حالياً تمثل دور مختلف';
$string['activebreadcrumb'] = 'لون خلفية مسار التنقل النشط';
$string['activebreadcrumbdesc'] = 'تعيين لون خلفية لون مسار التنقل النشط، وبقية شريط مسار التنقل النشط.';
$string['activitiesheading'] = 'الأنشطة';
$string['alertaccess'] = 'رؤية التنبيهات';
$string['alertaccessadmins'] = 'ظاهر للمدراء';
$string['alertaccessglobal'] = 'ظاهر للجميع';
$string['alertaccessusers'] = 'ظاهر للمستخدمين الذين سجلوا دخولهم للنظام';
$string['alertannounce'] = 'الاعلانات';
$string['alertannouncedesc'] = 'عرض اعلان في صندوق التنبيهات';
$string['alertbackgroundcolorsuccess'] = 'خلفية لون الاعلان';
$string['alertbackgroundcolorwarning'] = 'لون خلفية التحذير';
