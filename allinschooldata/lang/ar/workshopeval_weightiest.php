<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'workshopeval_weightiest', language 'ar', branch 'MOODLE_34_STABLE'
 *
 * @package   workshopeval_weightiest
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['comparison'] = 'مقارنة التقييمات';
$string['comparisonlevel0'] = '0% تسامح';
$string['comparisonlevel1'] = '10% تسامح';
$string['comparisonlevel2'] = '20% تسامح';
$string['comparisonlevel3'] = '30% تسامح';
$string['comparisonlevel4'] = '40% تسامح';
$string['comparisonlevel5'] = '50% تسامح';
$string['comparisonlevel6'] = '60% تسامح';
$string['comparisonlevel7'] = '70% تسامح';
$string['comparisonlevel8'] = '80% تسامح';
$string['comparisonlevel9'] = '90% تسامح';
$string['pluginname'] = 'مقارنة مع التقييم الأكبر وزناً';
