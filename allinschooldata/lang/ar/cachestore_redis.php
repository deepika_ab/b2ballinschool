<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'cachestore_redis', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   cachestore_redis
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['compressor_none'] = 'بدون ضغط.';
$string['compressor_php_gzip'] = 'استعمل ضغط gzip';
$string['compressor_php_zstd'] = 'استعمل ضغط Zstandard';
$string['password'] = 'كلمه المرور';
$string['password_help'] = 'هذا يعين كلمة مرور خادم Redis.';
$string['pluginname'] = 'Redis';
$string['prefix'] = 'بادئة المفتاح';
$string['prefix_help'] = 'يتم استخدام هذه البادئة لجميع أسماء المفاتيح على خادم Redis.
* إذا كان لديك مثيل Moodle واحد فقط يستخدم هذا الخادم ، يمكنك ترك هذه القيمة الافتراضية.
* بسبب قيود طول المفتاح ، يُسمح بحد أقصى 5 أحرف.';
$string['prefixinvalid'] = 'بادئة غير صالحة. يمكنك فقط استخدام az AZ 0-9-_.';
$string['privacy:metadata:redis'] = 'يقوم المكون الإضافي Redis cachestore بتخزين البيانات لفترة وجيزة كجزء من وظائف التخزين المؤقت الخاصة به. يتم تخزين هذه البيانات على خادم Redis حيث تتم إزالة البيانات بانتظام.';
$string['privacy:metadata:redis:data'] = 'البيانات المختلفة المخزنة في ذاكرة التخزين المؤقت';
$string['serializer_igbinary'] = 'المسلسل igbinary.';
$string['serializer_php'] = 'التسلسل الافتراضي PHP .';
$string['server'] = 'الخادم';
$string['server_help'] = 'هذا يعين اسم المضيف أو عنوان IP لخادم Redis لاستخدامه.';
$string['test_password'] = 'اختبار كلمة مرور الخادم';
$string['test_password_desc'] = 'اختبار كلمة مرور الخادم Redis .';
$string['test_serializer'] = 'مسلسل';
$string['test_serializer_desc'] = 'Serializer لاستخدامه للاختبار.';
$string['test_server'] = 'خادم الاختبار';
$string['test_server_desc'] = 'خادم Redis لاستخدامه للاختبار.';
$string['usecompressor'] = 'استعمل الضغط';
$string['usecompressor_help'] = 'حدد برنامج الضغط المستعمل بعد التحويل إلى سلسلة. هذا الأمر يتم على مستوى واجهة برمجة التطبيق لمخبأ مودل، وليس على مستوى تقليعة php';
$string['useserializer'] = 'استخدام التسلسل';
$string['useserializer_help'] = 'يحدد برنامج التسلسل الذي يجب استخدامه للتسلسل. المتسلسلون صالحون هم Redis :: SERIALIZER_PHP أو Redis :: SERIALIZER_IGBINARY. يتم دعم الأخير فقط عندما يتم تكوين phpredis بخيار --enable-redis-igbinary و يتم تحميل الامتداد igbinary.';
