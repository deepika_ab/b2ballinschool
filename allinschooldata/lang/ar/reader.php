<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'reader', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   reader
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['attempts'] = 'محاولات';
$string['confirmstarttimelimit'] = 'هذا الاختبار له وقت محدد. هل أنت متأكد أنك تريد بدأ الاختبار؟';
$string['incorrect'] = '- خطأ';
$string['quizattemptinprogress'] = 'محاولة الاختبار قيد التنفيذ';
$string['reports'] = 'تقارير';
$string['settings'] = 'إعدادت';
$string['show'] = 'عرض';
$string['showall'] = 'عرض الكل';
$string['showdeleted'] = 'عرض المحاولات المحذوفة';
$string['showhide'] = 'عرض/اخفاء';
$string['showhidebook'] = 'عرض أو اخفاء';
$string['showpercentgrades_help'] = '**نعم **
: عرض الدرجة (كنسبة مئوية) لكل محاولة في اختبار القارئ

**لا**
: لا تظهر درجات المحاولات الفردية في اختبار القارئ';
$string['studentusername'] = 'اسم المستخدم للطالب';
$string['thisattempt'] = 'هذه المحاولة';
$string['timefinish'] = 'وقت الانتهاء';
$string['timeleft'] = 'الوقت المتبقي';
$string['timestart'] = 'وقت البدء';
$string['to'] = 'إلى';
$string['tools'] = 'أدوات';
