<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_moodlemobileapp', language 'ar', branch 'MOODLE_26_STABLE'
 *
 * @package   local_moodlemobileapp
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addon.calendar.calendarevents'] = 'أحداث التقويم';
$string['addon.calendar.errorloadevent'] = 'خطأ في تحميل الحدث';
$string['addon.calendar.errorloadevents'] = 'خطأ في تحميل الأحداث';
$string['addon.calendar.noevents'] = 'لا يوجد أي أحداث';
$string['addon.competency.errornocompetenciesfound'] = 'لا يوجد أي قدرات موجودة';
$string['addon.competency.nocompetencies'] = 'لا يوجد أي قدرات';
$string['addon.coursecompletion.complete'] = 'مكتمل';
$string['addon.coursecompletion.couldnotloadreport'] = 'لا يمكن تحميل تقرير إكمال المقرر، الرجاء المحاولة في وقت آخر';
$string['addon.files.couldnotloadfiles'] = 'لا يمكن تحميل قائمة الملفات';
$string['addon.files.emptyfilelist'] = 'لا يوجد ملفات للعرض';
$string['addon.messages.contactlistempty'] = 'قائمة الاتصال فارغة';
$string['addon.messages.errordeletemessage'] = 'خطأ عند حذف الرسالة';
$string['addon.messages.messagenotsent'] = 'لم يتم إرسال الرسالة، يرجي المحاولة لاحقا';
$string['addon.messages.nousersfound'] = 'لا يوجد مستخدمين';
$string['addon.messages.type_offline'] = 'غير متصل';
$string['addon.messages.type_online'] = 'متصل';
$string['addon.messages.type_search'] = 'نتائج البحث';
$string['addon.messages.type_strangers'] = 'رسائل أخرى';
$string['addon.mod_chat.errorwhileconnecting'] = 'خطأ عند الاتصال بالدردشة';
$string['addon.mod_chat.errorwhilegettingchatdata'] = 'خطأ عند الحصول على معلومات الدردشة';
$string['addon.mod_chat.errorwhilegettingchatusers'] = 'خطأ عند الحصول على مستخدمي الدردشة';
$string['addon.mod_chat.errorwhileretrievingmessages'] = 'خطأ عند استرجاع الرسائل من الخادم';
$string['addon.mod_chat.errorwhilesendingmessage'] = 'خطأ عند إرسال الرسالة';
$string['addon.mod_folder.emptyfilelist'] = 'لا يوجد أي ملفات ليتم إظهارها';
$string['addon.mod_forum.group'] = 'مجموعة';
$string['addon.mod_glossary.byauthor'] = 'التجميع طبقا للمؤلف';
$string['addon.mod_glossary.bynewestfirst'] = 'الأحدث أولا';
$string['addon.mod_glossary.byrecentlyupdated'] = 'تم تحديثه مؤخرا';
$string['addon.mod_glossary.bysearch'] = 'بحث';
$string['addon.mod_imscp.showmoduledescription'] = 'اظهر الوصف';
$string['addon.mod_quiz.cannotsubmitquizdueto'] = 'محاولة هذا الاختبار لا يمكن إرسالها للأسباب التالية:';
$string['addon.mod_quiz.errordownloading'] = 'خطأ عن تنزيل البيانات المطلوبة';
$string['addon.mod_quiz.errorgetquiz'] = 'خطأ عند الحصول على بيانات الاختبار';
$string['addon.mod_resource.openthefile'] = 'افتح الملف';
$string['addon.mod_scorm.errordownloadscorm'] = 'خطأ عن تنزيل الحزمة التعليمية: "{{name}}"';
$string['addon.mod_survey.results'] = 'النتائج';
$string['addon.notifications.errorgetnotifications'] = 'خطأ في الحصول على الإشعارات';
$string['addon.notifications.notifications'] = 'الإشعارات';
$string['addon.notifications.therearentnotificationsyet'] = 'لا توجد إشعارات';
$string['appstoredescription'] = 'ملحوظة: هذا تطبيق نظام التعلم الإلكتروني مودل الرسمي ويعمل فقط مع مواقع نظم التعلم الإلكتروني مودل  التي تم إعدادها للسماح بذلك. لو واجهة أي مشاكل في الاتصال من فضلك تواصل مع مدير نظام التعلم الإلكتروني مودل.

لو تم إعداد نظام التعلم الإلكتروني مودل الخاص بك بشكل صحيح يمكن ان تستخدم هذا التطبيق لكي:
- تتصفح محتوى مقرراتك حتى لو كنت غير متصل بالأنترنت
- تستلم إعلامات بالرسائل والأحداث الاخرى
- تجد وتتواصل بسرعة مع أي من المشاركين في مقرراتك
- ترفع صور وملفات صوتية ومرئية وأي ملفات أخرى من جهازك الجوال
- تشاهد درجاتك في المقرر
- وأشياء أكتر ......

من فضلك راجع (http://docs.moodle.org/en/Mobile_app) للإطلاع على آخر المعلومات الخاصة بالتطبيق';
$string['core.cannotconnect'] = 'لا يمكن الاتصال: تحقق من أنك كتبت عنوان URL بشكل صحيح وأنك تستخدم موقع موودل {{$a}} أو أحدث.';
$string['core.contentlinks.chooseaccount'] = 'أختر الحساب';
$string['core.course.allsections'] = 'كل الأقسام';
$string['core.course.contents'] = 'المحتويات';
$string['core.course.couldnotloadsections'] = 'لم يتم تحميل كل الأقسام، من فضلك حاول مرة أخرى لاحقاَ';
$string['core.course.errordownloadingsection'] = 'خطأ عن تنزيل الأقسام';
$string['core.downloading'] = 'يتم التنزيل';
$string['core.errordownloading'] = 'خطأ عن تنزيل الملف';
$string['core.fileuploader.audio'] = 'صوتي';
$string['core.fileuploader.camera'] = 'الكاميرا';
$string['core.fileuploader.errorcapturingaudio'] = 'خطأ في التقاط الصوت';
$string['core.fileuploader.errorcapturingimage'] = 'خطأ في التقاط الصورة';
$string['core.fileuploader.errorcapturingvideo'] = 'خطأ في التقاط الفيديو';
$string['core.fileuploader.errormustbeonlinetoupload'] = 'لابد أن تكون متصل بالأنترنت لكي يتم رفع الملفات';
$string['core.fileuploader.errorreadingfile'] = 'خطأ في قراءة الملف';
$string['core.fileuploader.file'] = 'ملف';
$string['core.fileuploader.fileuploaded'] = 'الملف الذي تم رفعه';
$string['core.fileuploader.readingfile'] = 'يتم قراءة الملف';
$string['core.fileuploader.uploadafile'] = 'إرفع ملف';
$string['core.fileuploader.uploading'] = 'يتم الرفع';
$string['core.fileuploader.video'] = 'فيديو';
$string['core.lastsync'] = 'آخر تزامن';
$string['core.login.authenticating'] = 'مصادقة';
$string['core.login.connect'] = 'دخول';
$string['core.login.connecttomoodle'] = 'بيانات الدخول';
$string['core.login.credentialsdescription'] = 'من فضلك قم بإدخال اسم المستخدم وكلمة المرور للدخول إلى';
$string['core.login.invalidaccount'] = 'يرجى مراجعة تفاصيل تسجيل الدخول الخاصة بك أو الطلب من مسؤول موقع الويب الخاص بك للتحقق من تكوين موقع.';
$string['core.login.invalidmoodleversion'] = 'نسخة موودل غير صالحة. الحد الأدنى للنسخة المطلوبة هو:';
$string['core.login.invalidsite'] = 'رابط عنوان الموقع غير صالح.';
$string['core.login.loginbutton'] = 'دخول';
$string['core.login.newsitedescription'] = 'من فضلك أدخل رابط موقع نظام التعلم الإلكتروني مودل الخاص بك. لاحظ انه من الممكن أن يكون لم يتم إعداده للعمل مع هذا التطبيق.';
$string['core.login.passwordrequired'] = 'كلمة المرور مطلوبة';
$string['core.login.reconnect'] = 'إعادة الدخول';
$string['core.login.siteaddress'] = 'عنوان الموقع';
$string['core.login.siteurl'] = 'رابط الموقع';
$string['core.login.siteurlrequired'] = 'رابط الموقع مطلوب ..مثل  <i>http://www.yourmoodlesite.abc أو https://www.yourmoodlesite.efg</i>';
$string['core.login.usernamerequired'] = 'اسم المستخدم مطلوب';
$string['core.lostconnection'] = 'فقدنا الاتصال تحتاج إلى إعادة الاتصال. المميز الخاص بك هو الآن غير صالح';
$string['core.mainmenu.appsettings'] = 'إعدادات التطبيق';
$string['core.mainmenu.website'] = 'الموقع';
$string['core.networkerrormsg'] = 'لم يتم تمكين الشبكة أو أنها لا تعمل.';
$string['core.pulltorefresh'] = 'اسحب للأسفل ليتم التحديث';
$string['core.searching'] = 'يتم البحث';
$string['core.settings.about'] = 'حول';
$string['core.settings.deletesitefiles'] = 'هل أنت متأكد أنك تريد حذف الملفات التي تم تنزيلها من هذا الموقع  \'{{sitename}}\'؟';
$string['core.settings.deviceinfo'] = 'معلومات الجهاز';
$string['core.settings.deviceos'] = 'نظام تشغيل الجهاز';
$string['core.settings.enabledownloadsection'] = 'تفعيل تنزيل الأقسام';
$string['core.settings.estimatedfreespace'] = 'تقدير المساحة الحرة';
$string['core.settings.localnotifavailable'] = 'إشعارات محلية موجودة';
$string['core.settings.spaceusage'] = 'المساحة المستخدمة';
$string['core.settings.synchronization'] = 'تزامن';
$string['core.settings.synchronizenow'] = 'زامن الأن';
$string['core.settings.syncsettings'] = 'إعدادات المزامنة';
$string['core.settings.versioncode'] = 'رمز الإصدار';
$string['core.settings.versionname'] = 'اسم الإصدار';
$string['core.unexpectederror'] = 'خطأ غير متوقع. الرجاء الإغلاق وإعادة فتح التطبيق للمحاولة مرة أخرى';
$string['core.unknown'] = 'غير معروف';
$string['core.user.contact'] = 'جهة اتصال';
