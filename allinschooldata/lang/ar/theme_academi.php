<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_academi', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_academi
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['about'] = 'حول';
$string['aboutus'] = 'معلومات عننا';
$string['address'] = 'العنوان';
$string['calendar'] = 'التقويم';
$string['callus'] = 'الاتصال بنا';
$string['choosereadme'] = '<div class="clearfix"><div class="theme_screenshot"><img class=img-polaroid src="academi/pix/screenshot.jpg" /><br/><p></p><h3>حقوق القالب</h3><p><h3>قالب موودل الأكاديمي</h3><p>هذا القالب مبني على قالب موودل المحسن</p><p>المصممون: LMSACE Dev Team<br>Contact: info@lmsace.com<br>الموقع: <a href="http://www.lmsace.com">www.lmsace.com</a><br></p>';
$string['configtitle'] = 'الأكاديمي';
$string['connectus'] = 'تواصل معنا';
$string['contact'] = 'الاتصال';
$string['contact_us'] = 'اتصل بنا';
$string['copyright_default'] = 'Copyright &copy; 2017 - Developed by <a href="http://lmsace.com">LMSACE.com</a>. Powered by <a href="https://moodle.org">Moodle</a>';
$string['copyright_footer'] = 'حقوق الملكية';
$string['customcss'] = 'تنسيق CSS مخصص';
$string['customcssdesc'] = 'أيًا كانت قواعد CSS التي تضيفها إلى هذا المربع النصي ، ستنعكس في كل صفحة ، مما يجعل التخصيص أسهل لهذا القالب.';
$string['defaultaddress'] = '308 Negra Narrow Lane, Albeeze, New york, 87104';
$string['defaultemailid'] = 'info@example.com';
$string['defaultphoneno'] = '(000) 123-456';
$string['email'] = 'بريد الكتروني';
$string['emailid'] = 'البريد الالكتروني';
$string['enable'] = 'تم تمكين التحكم بالصورة الرمزية (لوغو) بذيل الصفحة';
$string['fburl'] = 'فيسبوك';
$string['fburl_default'] = 'https://www.facebook.com/yourfacebookid';
$string['fburldesc'] = 'رابط صفحة الفيسبوك الخاصة بمؤسستك';
$string['fcourses'] = 'المقررات المميزة';
$string['fcoursesdesc'] = 'الرجاء اختيار مقررات مميزة';
$string['featuredcoursesheading'] = 'مقررات مميزة';
$string['followus'] = 'تابعنا';
$string['footbgimg'] = 'صورة الخلفية';
$string['footbgimgdesc'] = 'حجم صورة الخلفية يجب أن يكون 1345 X 517';
$string['footerheading'] = 'التذييل';
$string['footnote'] = 'الحاشية';
$string['footnotedefault'] = '<p>أبجد هوز هو مجرد نص صوري لصناعة الطباعة والتنضيد. كان  هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ، عندما أخذت طابعة غير معروفة مجموعة من أنواع التنضيد من النوع والأوراق المزمنة ، وأوراقًا تلصق مقاطع أبجد هوز باللغة الانجليزية. <br><a href="#">اقرأ المزيد »</a></p>';
$string['footnotedesc'] = 'سيتم عرض كل ما تضيفه إلى مربع النص هذا في تذييل الصفحة عبر موقع موودل الخاص بك.';
$string['frontpageheading'] = 'الصفحة الأولى';
$string['gpurl'] = 'Google+';
$string['gpurl_default'] = 'https://www.google.com/+yourgoogleplusid';
$string['gpurldesc'] = 'رابط Google+ الخاص بمؤسستك';
$string['headerheading'] = 'عام';
$string['home'] = 'الرئيسية';
$string['homebanner'] = 'بانر الصفحة الرئيسية';
$string['homebannerdesc'] = 'الصورة يجب أن تكون بحجم 1345px X 535px.';
$string['homebanner_slogan'] = 'لدينا أكبر مجموعة من المقررات';
$string['info'] = 'معلومات';
$string['infoblockheading'] = 'كتلة المعلومات';
$string['infoblocktext'] = 'نص كتلة المعلومات';
$string['infoblocktextdefault'] = '<p>أبجد هوز هو مجرد نص صوري لصناعة الطباعة والتنضيد. كان  هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ، عندما أخذت طابعة غير معروفة مجموعة من أنواع التنضيد من النوع والأوراق المزمنة ، وأوراقًا تلصق مقاطع أبجد هوز باللغة الانجليزية. <a href="javascript:void(0);">اقرأ المزيد <i class="fa fa-angle-double-right"></i></a></p>';
$string['infoblocktextdesc'] = 'نص كتلة المعلومات';
$string['infoblocktitle'] = 'عنوان كتلة المعلومات';
$string['infoblocktitledefault'] = 'من نحن';
$string['infoblocktitledesc'] = 'عنوان الكتلة المخصصة للمعلومات';
$string['infolink'] = 'رابط المعلومات';
$string['infolinkdefault'] = 'مجتمع موودل|https://moodle.org
الدعم المجاني لنظام مودل|https://moodle.org/support
تطوير موودل|https://moodle.org/development
وثائق موودل|http://docs.moodle.org|Moodle Docs
Moodle.com|http://moodle.com/';
$string['infolink_desc'] = 'يمكنك وضع معلومات وروابط مخصصة هنا لتظهر بالقالب. يتكون كل سطر من نص القائمة، رابط (اختياري)، تلميح شاشة (اختياري)، ورمز اللغة أو مجموعة رموز مفصولة بفاصلة (اختياري، لإظهار السطر للغات المخصصة فقط)، وجملة مفصولة بعلامة الأنبوب. مثل:
<pre> مجتمع موودل|https://moodle.org
الدعم الفني المجاني لنظام موودل|https://moodle.org/support
تطوير موودل|https://moodle.org/development
وثائق موودل|http://docs.moodle.org|Moodle Docs
وثائق موودل باللغة الألمانية|http://docs.moodle.org/de|Documentation in German|de
Moodle.com|http://moodle.com/ </pre>';
$string['logo'] = 'الصورة الرمزية (لوغو)';
$string['logodesc'] = 'يرجى تحميل شعار مخصص هنا إذا كنت ترغب في إضافته إلى الترويسة. <br> الصورة يجب أن تكون بارتفاع 37px وعرض مقبول (حد أدنى: 250px) هذا مناسب.';
$string['moodle_community'] = 'مجتمع موودل';
$string['moodle_docs'] = 'وثائق موودل';
$string['moodle_support'] = 'الدعم الفني لنظام موودل';
$string['newsblockcontent'] = 'محتويات كتلة الأخبار والأحداث';
$string['newsblockcontentdesc'] = 'سيتم عرض كل ما تضيفه إلى هذا النص في كتلة الأخبار والأحداث.';
$string['newsblockheading'] = 'كتلة الأخبار والأحداث';
$string['newseventbg'] = 'صورة خلفية كتلة الأخبار والأحداث';
$string['newseventbgdesc'] = 'الصورة يجب أن تكون بحجم 1345px X 760px .';
$string['newseventcontent'] = '<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="embed-wrap">
<div class="embed-responsive embed-responsive-16by9">
<iframe src="https://www.youtube.com/embed/fNE7pyDyw3Y" allowfullscreen="" frameborder="0" height="391" width="545"></iframe>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="info-wrap">
<h2 class="nomargint">news & events of 2017</h2>
<p>أبجد هوز هو مجرد نص صوري لصناعة الطباعة والتنضيد. كان  هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ، عندما أخذت طابعة غير معروفة مجموعة من أنواع التنضيد من النوع والأوراق المزمنة ، وأوراقًا تلصق مقاطع أبجد هوز باللغة الانجليزية.</p>
<div class="readmore-btn"><a href="#">اقرأ المزيد</a></div>
</div>
</div>
</div>';
$string['numberofslides'] = 'عدد الشرائح';
$string['numberofslides_desc'] = 'عدد الشرائح على شريط التمرير (اسلايدر)';
$string['numberoftmonials'] = 'عدد الشهادات';
$string['numberoftmonials_desc'] = 'عدد الشهادات في الصفحة الرئيسية';
$string['phone'] = 'الهاتف';
$string['phoneno'] = 'رقم الهاتف';
$string['pinurl'] = 'موقع Pinterest';
$string['pinurl_default'] = 'https://in.pinterest.com/yourpinterestname/';
$string['pinurldesc'] = 'رابط موقع pinterest الخاص بالمؤسسة.';
$string['pluginname'] = 'أكاديمي';
$string['primarymenu'] = 'القوائم الأساسية';
$string['primarymenudesc'] = 'يمكنك هنا إعداد القائمة الأساسية لتظهر في القالب.  يتكون كل سطر من نص القائمة، رابط (اختياري)، تلميح شاشة (اختياري)، ورمز اللغة أو مجموعة رموز مفصولة بفاصلة (اختياري، لإظهار السطر للغات المخصصة فقط)، وجملة مفصولة بعلامة الأنبوب. الأسطر التي تبدأ بالشرطة ستظهر كعناصر قائمة في القائمة العلوية السابقة، والفواصل يمكن استخدامها بإضافة سطر من علامات الهاش # عند الحاجة. مثال:
مجتمع موودل|https://moodle.org
-الدعم المجاني لنظام موودل|https://moodle.org/support
-وثائق موودل|http://docs.moodle.org|Moodle Docs
-وثائق موودل باللغة الألمانية|http://docs.moodle.org/de|Documentation in German|de
-###
-تطوير موودل|https://moodle.org/development
Moodle.com|http://moodle.com/';
$string['readmore'] = 'اقرأ المزيد';
$string['region-side-post'] = 'اليمين';
$string['region-side-pre'] = 'اليسار';
$string['sb1_default_cnt'] = '<p>أبجد هوز هو مجرد نص صوري  <br class="visible-lg">لصناعة الطباعة والتنضيد.</p>';
$string['sb1_default_title'] = 'ادرس مقررات عبر النت';
$string['sb2_default_cnt'] = '<p>أبجد هوز هو مجرد نص صوري <br class="visible-lg"> لصناعة الطباعة والتنضيد.</p>';
$string['sb2_default_title'] = 'كن مدرساً';
$string['sb3_default_cnt'] = '<p>أبجد هوز هو مجرد نص صوري <br class="visible-lg"> لصناعة الطباعة والتنضيد..</p>';
$string['sb3_default_title'] = 'تسجيل المقررات';
$string['sb4_default_cnt'] = '<p>أبجد هوز هو مجرد نص صوري <br class="visible-lg"> لصناعة الطباعة والتنضيد.</p>';
$string['sb4_default_title'] = 'اكسب أموالاً';
$string['signup'] = 'التسجيل';
$string['sitefblock1'] = 'الكتلة الأولى لمميزات الموقع';
$string['sitefblock2'] = 'الكتلة الثانية لمميزات الموقع';
$string['sitefblock3'] = 'الكتلة الثالثة لمميزات الموقع';
$string['sitefblock4'] = 'الكتلة الرابعة لمميزات الموقع';
$string['sitefblockbuttontext'] = 'نص الرابط';
$string['sitefblockbuttontextdesc'] = 'نص يظهر في الأسفل.';
$string['sitefblockbuttonurl'] = 'الرابط';
$string['sitefblockbuttonurldesc'] = 'زر رابط الموقع سيشير إلى.';
$string['sitefblockcontent'] = 'المحتوى';
$string['sitefblockcontentdesc'] = 'المحتوى المراد عرضه في كتلة ميزات الموقع. اجعلها مختصرة و لطيفة.';
$string['sitefblockimage'] = 'الصورة';
$string['sitefblockimagedesc'] = 'يوفر هذا خيار عرض صورة أعلى النص في كتلة ميزات الموقع';
$string['sitefblockimgcolors'] = 'لون صورة الخلفية';
$string['sitefblockimgcolorsdesc'] = 'تغيير الألوان الموجودة في لون خلفية صورة كتلات ميزة الموقع';
$string['sitefblocksheading'] = 'كتلات ميزة الموقع';
$string['sitefblocktitle'] = 'العنوان';
$string['sitefblocktitledesc'] = 'العنوان الذي سيظهر في كتلة ميزات الموقع';
$string['sitefblockurltarget'] = 'رابط الهدف';
$string['sitefblockurltargetdesc'] = 'اختر طريقة فتح الرابط';
$string['sitefblockurltargetnew'] = 'صفحة جديدة';
$string['sitefblockurltargetparent'] = 'الإطار الأب';
$string['sitefblockurltargetself'] = 'الصفحة الحالية';
$string['sitefeaturebg'] = 'صورة خلفية ميزات الموقع';
$string['sitefeaturebgdesc'] = 'الصورة يجب أن تكون بحجم 1345px X 379px .';
$string['sitenews'] = 'أخبار الموقع';
$string['slidecapbgcolor'] = 'لون خلفية التسمية التوضيحية للشريحة.';
$string['slidecapbgcolordesc'] = 'اللون المفترض لخلفية التسمية التوضيحية الشريحة .';
$string['slidecapcolor'] = 'لون نص التسمية التوضيحية للشريحة';
$string['slidecapcolordesc'] = 'اللون المفترض لنص التسمية التوضيحية الشريحة.';
$string['slidecaption'] = 'التسمية التوضيحية للشريحة';
$string['slidecaptiondefault'] = 'Bootstrap Based Slider - {$a->slideno}';
$string['slidecaptiondesc'] = 'أدخل نص التسمية التوضيحية لاستخدامها في الشريحة';
$string['slidedesc'] = 'وصف الشريحة';
$string['slidedescbgcolor'] = 'لون خلفية وصف الشريحة';
$string['slidedescbgcolordesc'] = 'اللون المفترض لوصف الشريحة.';
$string['slidedesccolor'] = 'لون نص وصف الشريحة';
$string['slidedesccolordesc'] = 'اللون المفترض لوصف الشريحة';
$string['slidedescdefault'] = '<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>';
$string['slidedesctext'] = 'أدخل وصف الشريحة.';
$string['slideimage'] = 'صورة الشريحة';
$string['slideimagedesc'] = 'الصورة يجب أن تكو بحجم 1400px X 418px.';
$string['slideno'] = 'الشريحة {$a->slide}';
$string['slidenodesc'] = 'ادخل اعدادات الشريحة {$a->slide}.';
$string['slideshowdesc'] = 'وهذا يجعل عرض شرائح  تصل إلى اثني عشر شريحة لتتمكن من ترويج العناصر الهامة في موقعك. العرض متجاوب مع حجم الشاشة حيث يتم تعيين ارتفاع الصورة وفقا لحجم الشاشة. الارتفاع الموصى به هو 418px. يتم تعيين العرض في 100٪، وبالتالي فإن الارتفاع الفعلي سيكون أقل إذا كان العرض أكبر من حجم الشاشة. في أحجام الشاشة الصغيرة يتم تقليل الارتفاع آلياً دون الحاجة إلى توفير صور منفصلة. للمرجعية فإن عرض الشاشة <767px = ارتفاع438px، والعرض بين  768px  و 979px = ارتفاع 418px والعرض> 980px = ارتفاع 418px. إذا لم يتم تحديد صورة للشريحة، ثم يتم استخدام الصورة الافتراضية الموجودة في مجلد pix.';
$string['slideshowheading'] = 'عرض الشرائح';
$string['slideshowheadingsub'] = 'عرض الشرائح للصفحة الأولى';
$string['toggleslideshow'] = 'عرض الشرائح';
$string['toggleslideshowdesc'] = 'اختيار ما إذا كنت ترغب في إخفاء أو إظهار عرض الشرائح.';
$string['twurl'] = 'تويتر';
$string['twurl_default'] = 'https://twitter.com/yourtwittername';
$string['twurldesc'] = 'رابط تويتر لمؤسستتك.';
