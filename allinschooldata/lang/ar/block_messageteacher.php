<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_messageteacher', language 'ar', branch 'MOODLE_37_STABLE'
 *
 * @package   block_messageteacher
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['enablegroups'] = 'تفعيل دعم المجموعة';
$string['groupsdesc'] = 'عند التفعيل, سيشاهد الطلاب المعلمين في مجموعتهم فقط';
$string['includecoursecat'] = 'تضمين المعلمين من فئة مجموعة المواد';
$string['messagefailed'] = 'فشل ارسال الرسالة';
$string['messagefrom'] = 'رسالة من {$a}';
$string['messageheader'] = 'ادخل رسالتك الى {$a}';
$string['messageprovider:message'] = 'رسالة تم ارسالها بستخدام خاصية مراسلة معلمي';
$string['messagesent'] = 'تم ارسال الرسالة!';
$string['messageteacher:addinstance'] = 'اضافة مربع مراسلة معلمي جديد';
$string['messagetext'] = 'رسالة نصية';
$string['nogroupmembership'] = 'أنت لست عضوًا في أي مجموعة';
$string['nogroupteachers'] = 'لم يتم تعين استاذ الى مجموعتك بعد';
$string['norecipient'] = 'لا يمكن تحديد أي مستلم للمستخدم{$a}';
$string['pluginname'] = 'مراسلة استاذي';
$string['pluginnameplural'] = 'مراسلة اساتذتي';
$string['send'] = 'إرسل';
$string['sentfrom'] = 'هذه الرسالة تم ارسالها بواسطة {$a}';
$string['showuserpictures'] = 'اظهار صورة المستخدم';
$string['showuserpicturesdesc'] = 'اذا تم تفعليه, صورة المدرس سوف تظهر مع اسمه';
