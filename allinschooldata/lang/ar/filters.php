<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filters', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   filters
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actfilterhdr'] = 'الفلاتر المفعلة';
$string['addfilter'] = 'إضافة فلتر';
$string['anycategory'] = 'أي تصنيف';
$string['anycourse'] = 'أي مقرر دراسي';
$string['anycourses'] = 'منضم إلى أي مساق';
$string['anyfield'] = 'أي حقل';
$string['anyrole'] = 'أي دور';
$string['anyvalue'] = 'أي قيمة';
$string['applyto'] = 'تطبيق';
$string['categoryrole'] = 'دور التصنيف';
$string['contains'] = 'يحتوي';
$string['content'] = 'محتوى';
$string['contentandheadings'] = 'المحتوى والترويسات';
$string['coursecategory'] = 'تصنيف المساق';
$string['courserole'] = 'دور المقرر الدراسي';
$string['courserolelabel'] = '{$a->label} is {$a->rolename} in {$a->coursename} from {$a->categoryname}';
$string['courserolelabelerror'] = 'خطأ {$a->label}: المساق {$a->coursename} ليس له وجود';
$string['coursevalue'] = 'قيمة المساق';
$string['datelabelisafter'] = '{$a->label} is after {$a->after}';
$string['datelabelisbefore'] = '{$a->label} is before {$a->before}';
$string['datelabelisbetween'] = '{$a->label} is between {$a->after} and {$a->before}';
$string['defaultx'] = 'الأفتراضي ({$a})';
$string['disabled'] = 'معطل';
$string['doesnotcontain'] = 'لا يحتوي';
$string['endswith'] = 'ينتهي بـ';
$string['filterallwarning'] = 'تطبيق المرشحات على رؤوس الصفحات فضلاً عن محتوياتها يمكنه زيادة الحمل على مخدمك إلى حد كبير. لطفاً، استعمل إعدادات "التطبيق على" بشكل متحفظ. الاستعمال الرئيسي هو مع مرشح تعدد اللغات.';
$string['filtersettings'] = 'إعدادات المنقح';
$string['filtersettingsforin'] = 'إعدادات المرشح لـ {$a->filter} في {$a->context}';
$string['filtersettings_help'] = 'هذه الصفحة تتيح لك تفعيل المرشحات وتعطيلها في أجزاء معينة من الموقع.

بعض المرشحات قد تسمح لك أيضاً بضبط إعدادات محلية، وفي تلك الحالة، سيظهر لك رابط لإجراء الضبط مجاوراً لأسمائها.';
$string['filtersettingsin'] = 'إعدادات المنقح في {$a}';
$string['firstaccess'] = 'الدخول لإول مرة';
$string['globalrolelabel'] = '{$a->label} is {$a->value}';
$string['isactive'] = 'هل هو نشط؟';
$string['isafter'] = 'هو بعد';
$string['isanyvalue'] = 'هو أي قيمة';
$string['isbefore'] = 'هو قبل';
$string['isdefined'] = 'هو معرف';
$string['isempty'] = 'هو فارغ';
$string['isequalto'] = 'هو يساوي';
$string['isnotdefined'] = 'غير معرف';
$string['isnotequalto'] = 'لا يساوي';
$string['limiterfor'] = 'محدد حقل {$a}';
$string['neveraccessed'] = 'لم يتم استخدامه';
$string['nevermodified'] = 'لم يتم تعديله';
$string['newfilter'] = 'منقح جديد';
$string['nofiltersenabled'] = 'لم يتم تمكين موصلات أي منقح في هذا الموقع';
$string['off'] = 'مغلق';
$string['offbutavailable'] = 'مغلق ولكنة متوفر';
$string['on'] = 'فعال';
$string['privacy:reason'] = 'نظام المرشحات الفرعي لا يقوم بخزن أي بيانات شخصية.';
$string['profilefilterfield'] = 'اسم الحقل الشخصي';
$string['profilefilterlimiter'] = 'عامل الحقل الشخصي';
$string['profilelabel'] = '{$a->label}: {$a->profile} {$a->operator} {$a->value}';
$string['profilelabelnovalue'] = '{$a->label}: {$a->profile} {$a->operator}';
$string['removeall'] = 'استبعد كل المنقحات';
$string['removeselected'] = 'أستبعد كل ما تم اختياره';
$string['selectlabel'] = '{$a->label} {$a->operator} {$a->value}';
$string['startswith'] = 'يبداء بـ';
$string['tablenosave'] = 'سيتم حفط التغيرات في الجدول السابق آلياً';
$string['textlabel'] = '{$a->label} {$a->operator} {$a->value}';
$string['textlabelnovalue'] = '{$a->label} {$a->operator}';
$string['valuefor'] = 'قيمة {$a}';
