<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'competency', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   competency
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allchildrenarecomplete'] = 'كل الفروع قد اكتملت';
$string['competencies'] = 'الكفايات';
$string['competenciessettings'] = 'إعدادات الكفايات';
$string['coursecompetencyoutcome_recommend'] = 'إرسل للمراجعة';
$string['coursemodulecompetencyoutcome_recommend'] = 'إرسل للمراجعة';
$string['syncplanscohorts'] = 'مزامنة الخطط من أفواج قالب خطة التعلم';
$string['usercommentedonacompetencyhtml'] = '<p>{$a->fullname} أعطى تعليقاً عن الكفاءة "{$a->competency}":</p>
<div>{$a->comment}</div>
<p>أنظر: <a href="{$a->url}">{$a->urlname}</a>.</p>';
$string['usercommentedonacompetencysmall'] = '{$a->fullname} أعطى تعليقاً عن الكفاءة "{$a->competency}".';
$string['usercommentedonacompetencysubject'] = '{$a} أعطى تعليقاً عن كفاءة.';
$string['usercommentedonaplan'] = '{$a->fullname} أعطى تعليقاً عن خطة التعلم "{$a->plan}":

{$a->comment}

أنظر: {$a->url}';
$string['usercommentedonaplanhtml'] = '<p>{$a->fullname} أعطى تعليقاً عن خطة التعلم "{$a->plan}":</p>
<div>{$a->comment}</div>
<p>أنظر: <a href="{$a->url}">{$a->urlname}</a>.</p>';
$string['usercommentedonaplansmall'] = '{$a->fullname} أعطى تعليقاً عن خطة التعلم "{$a->plan}".';
$string['usercommentedonaplansubject'] = '{$a} أعطى تعليقاً عن خطة تعلم.';
$string['usercompetencystatus_idle'] = 'خاملة';
$string['usercompetencystatus_inreview'] = 'في المراجعة';
$string['usercompetencystatus_waitingforreview'] = 'في انتظار المراجعة';
$string['userplans'] = 'خطط التعلم';
