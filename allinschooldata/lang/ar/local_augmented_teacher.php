<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_augmented_teacher', language 'ar', branch 'MOODLE_35_STABLE'
 *
 * @package   local_augmented_teacher
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activity'] = 'نشاط';
$string['add'] = 'اضافة';
$string['addedit'] = 'اضافة/تعديل';
$string['addnewreminder'] = 'اضافة تذكير جديد';
$string['after'] = 'بعد';
$string['augmented_teacher:receivereminder'] = 'استلام تذكير';
$string['backtomainmenu'] = 'العودة الى القائمة الرئيسية';
$string['before'] = 'قبل';
$string['cannotbesame'] = 'الأنشطة المختارة لا يمكن أن تكون هي نفسها';
$string['choosetask'] = 'اختار مهمة';
$string['delete'] = 'حذف';
$string['deleted'] = 'محذوفة';
$string['deleteexclusionwarn'] = 'هل انت متاكد تمام انك تريد مسح هذا السجل ؟';
$string['deletereminderwarn'] = 'هل انت متاكد تمام انك تريد مسح هذا التذكير وجميع البيانات الخاصة به؟';
$string['duration'] = 'المدة';
$string['duration_desc'] = 'المدة التي لم يقوم بتسجيل الدخول فيها';
$string['duration_help'] = 'يقوم بارسال رسالى الى المستخدمين ان يعودو الى الموقع, عندما لم يقومو بالتسجيل لمدة xyz يوم للموقع';
$string['durationlessthanday'] = 'المدة يجب ان لا تقل عن يوم واحد';
$string['edit'] = 'تعديل';
$string['enabled'] = 'مفعل';
$string['eventremindercreated'] = 'تم انشاء تذكير';
$string['eventreminderdeleted'] = 'تم حذف التذكير';
$string['eventreminderupdated'] = 'تم تحديث التذكير';
$string['excludeusersfromreminders'] = 'استبعاد المستخدمين من استقبال التذكير';
$string['firstname'] = 'الاسم الاول';
$string['lastname'] = 'الاسم الاخير';
$string['list'] = 'قائمة';
$string['log'] = 'سجل';
$string['logs'] = 'سجلات';
$string['message'] = 'رسالة';
$string['no'] = 'لا';
$string['notloggedinreminder'] = 'تذكير بعدم تسجيل الدخول';
$string['notloggedinreminderdelete'] = 'مسح تذكير عدم الدخول';
$string['notloggedinreminders'] = 'تذكيرات عدم تسجيل الدخول';
$string['numberofreminders'] = 'عدد التذكيرات';
$string['recommendactivity'] = 'الانشطة الموصى بها';
$string['recommendactivitydelete'] = 'مسح النشاط الموصى به';
$string['recommendedactivity'] = 'النشاط الموصى به';
$string['reminder'] = 'تذكير';
$string['reminderdelete'] = 'مسح التذكير';
$string['reminders'] = 'تذكيرات';
$string['scheduled'] = 'مقررة';
$string['select'] = 'اختيار';
$string['sendremindermessage'] = 'ارسال رسالة تذكير';
$string['shortcodes'] = 'رموز قصيرة';
$string['smallmessage'] = 'رسالة';
$string['subject'] = 'موضوع';
$string['submit'] = 'ارسال';
$string['timecreated'] = 'وقت الانشاء';
$string['timeend'] = 'الاقصاء ينتهي';
$string['timeinterval'] = 'فترة';
$string['timemodified'] = 'وقت التعديل';
$string['title'] = 'موضوع';
$string['type'] = 'اكتب';
$string['yes'] = 'نعم';
