<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'accredible', language 'ar', branch 'MOODLE_37_STABLE'
 *
 * @package   accredible
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accredible:addinstance'] = 'إضافة نموذج  شهادة / شارة';
$string['accredible:manage'] = 'إدارة  نموذج  شهادة / شارة';
$string['accredible:student'] = 'استرجاع شهادة';
$string['accredible:view'] = 'عرض الشهادة أو الشارة';
$string['achievementid'] = 'رقم الإنجازات \\ أسم الجماعة';
$string['activitydescription'] = 'هذا النموذج يصدر ويحدث مجموعة معتمدة عند Accredible.أي شهادات أو شارات تم إصدارها ستكون موجودة في هذه المجموعة بحيث يمكن تحديث شكلها ومعلوماتها على الرابط: <a href="https://dashboard.accredible.com" target="_blank">the dashboard</a>.';
$string['activityname'] = 'اسم النشاط';
$string['additionalactivitiesone'] = 'تحذير : أنت تضيف أكثر من نشاط لمقرر دراسي واحد
<br/>  كلا النشاطين مُشاهدة من قبل الطلاب، لذلك تأكد من منحهم أسماء مختلفة .';
$string['additionalactivitiesthree'] = 'الاسم المُدخل هنا سيظهر في الشهادة';
$string['additionalactivitiestwo'] = 'الشهادات\\الشارات ستكون مدرجة على صفحة النشاط فقط إذا تم إصدارها باستخدام معرف الإنجاز هذا';
$string['apikeyhelp'] = 'أدخل مفتاح API من accredible.com';
$string['apikeylabel'] = 'مفتاح API';
$string['autoissueheader'] = 'معايير الإصدار التلقاءي';
$string['certificatename'] = 'اسم الشهادة';
$string['certificateurl'] = 'رابط الشهادة';
$string['chooseexam'] = 'اختر اختبار نهائي';
$string['completionissuecheckbox'] = 'نعم. أصدر بمجرد إتمام المقرر';
$string['completionissueheader'] = 'معيار الإصدار التلقاءي: إتمام المقرر';
$string['dashboardlink'] = 'رابط واجهة استعراض Accredible';
$string['dashboardlinktext'] = 'لمحو أو تصميم المعرفات، قم بالدخول إلى هذا الرابط : <a href="https://dashboard.accredible.com" target="_blank">dashboard</a>';
$string['datecreated'] = 'تاريخ الإنشاء';
$string['description'] = 'الوصف';
$string['eventcertificatecreated'] = 'معرف ام تحريره إلى Accredible';
$string['gotodashboard'] = 'لتحديث مظهر شاراتك وشهاداتك، قم بزيارة: <a href="https://dashboard.accredible.com" target="_blank">https://dashboard.accredible.com</a>';
$string['gradeissueheader'] = 'معايير الإصدار التلقائي: بحسب درجة الاختبار النهائي';
$string['groupselect'] = 'مجموعة';
$string['id'] = 'المُعرِّف';
$string['indexheader'] = '{$a} كل الشهادات/الأوسمة الخاصة ب';
$string['issued'] = 'تم اصدارها';
$string['manualheader'] = 'أصدر شهادات/أوسمة يدويا';
$string['modulename'] = 'شهادات معتمدة وأوسمة';
$string['modulename_help'] = 'تسمح لك وحدة نشاط الشهادة والشارة المعتمدين بإصدار شهادات الدورة أو أوسمة للطلاب على
accredible.com.

أضف النشاط أينما تريد أن يرى طلابك شهادتهم أو أوسمتهم.';
$string['modulenameplural'] = 'شهادات / أوسمة معتمدة';
$string['nocertificates'] = 'لا يوجد شهادات/أوسمة';
$string['overview'] = 'نظرة عامة ( لمحة موجزة)';
$string['passinggrade'] = 'نسبة درجة النجاح المطلوبة لإجتياز المقرر (%)';
$string['pluginadministration'] = 'إدارة الشهادات / الأوسمة المعتمدة';
$string['pluginname'] = 'شهادات وشارات معتمدة';
$string['recipient'] = 'المستلم';
$string['templatename'] = 'اسم الدفعة (من لوحة القيادة)';
$string['unissueddescription'] = 'استوفى هؤلاء المستخدمون متطلبات هذه الشهادة ولكنهم لم يحصلوا عليها بعد. حدد المستخدمين الذين ترغب في إصدار شهادات لهم.';
$string['unissuedheader'] = 'شهادات / أوسمة غير مصدرة';
$string['usestemplatesdescription'] = 'تأكد من وجود دفعة نموذجية على لوحة التحكم بنفس اسم معرف الإنجاز الخاص بك.';
$string['viewheader'] = '{$a} الشهادات والشارات لـ';
$string['viewimgcomplete'] = 'انقر لعرض شهادتك أو شارتك';
$string['viewimgincomplete'] = 'الدورة لا تزال جارية';
$string['viewsubheader'] = 'مُعرِّف المجموعة : {$a}';
$string['viewsubheaderold'] = 'مُعرِّف الإنجاز : {$a}';
