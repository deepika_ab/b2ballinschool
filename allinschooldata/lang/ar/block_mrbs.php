<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_mrbs', language 'ar', branch 'MOODLE_31_STABLE'
 *
 * @package   block_mrbs
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['match_area'] = 'منطقة التطابق';
$string['match_descr'] = 'وصف كامل للتطابق';
$string['match_entry'] = 'وصف مختصر للتطابق';
$string['match_room'] = 'غرفة التطابق';
$string['match_type'] = 'نوع التطابق';
$string['mincapacity'] = 'الحد الأدنى للسعة';
$string['minutes'] = 'دقائق';
$string['month'] = 'شهر';
$string['monthafter'] = 'الذهاب إلى الشهر التالي';
$string['monthbefore'] = 'الذهاب إلى الشهر السابق';
$string['movedto'] = 'انتقل إلى';
$string['mrbs'] = 'نظام الحجز لغرفة الاجتماعات';
$string['must_set_name'] = 'يجب عليك تعيين اسم';
$string['namebooker'] = 'حجز لـ';
$string['newwindow'] = 'نافذة جديدة';
$string['noarea'] = 'لم يتم تحديد أي منطقة';
$string['noareas'] = 'لا يوجد مناطق';
$string['norights'] = 'ليس لديك صلاحية الوصول إلى تعديل هذا البند.';
$string['norooms'] = 'لا غرف';
$string['no_rooms_for_area'] = 'لم يتم تحديد لهذه المنطقة أي غرف';
$string['noroomsfound'] = 'عذراً لم يتم إيجاد أي غرف';
$string['notallowedbook'] = 'لست ضمن قائمة المستخدمين المسموح لهم حجز هذه الغرفة';
$string['not_found'] = 'غير موجود';
$string['pagewindow'] = 'نفس الإطار/النافذة';
$string['please_contact'] = 'الرجاء التواصل';
$string['postbrowserlang'] = 'لغة.';
$string['ppreview'] = 'معاينة قبل الطباعة';
$string['records'] = 'سجلات';
$string['rep_dsp_dur'] = 'مدة';
$string['rep_dsp_end'] = 'وقت النهاية';
$string['rep_end_date'] = 'تاريخ انتهاء التكرار';
$string['rep_freq'] = 'تكرار';
$string['rep_num_weeks'] = 'عدد الأسابيع';
$string['report_and_summary'] = 'تقرير وملخص';
$string['report_end'] = 'تاريخ انتهاء التقرير';
$string['report_on'] = 'تقرير عن اجتماعات';
$string['report_only'] = 'فقط التقرير';
$string['report_start'] = 'تاريخ بدء التقرير';
$string['rep_rep_day'] = 'يوم التكرار';
$string['rep_type'] = 'نوع التكرار';
$string['rep_type_0'] = 'لا يوجد';
$string['rep_type_1'] = 'يومياً';
$string['rep_type_2'] = 'أسبوعياً';
$string['rep_type_3'] = 'شهرياً';
$string['rep_type_4'] = 'سنوياً';
$string['resolution_units'] = 'دقائق';
$string['room'] = 'غرفة';
$string['rooms'] = 'الغرف';
$string['search_for'] = 'ابحث عن';
$string['search_results'] = 'نتائج البحث عن';
$string['seconds'] = 'ثواني';
$string['sort_rep'] = 'رتِّب التقرير بطريقة';
