<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_certification', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_certification
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'أجراءات';
$string['active'] = 'نشط';
$string['activecertifications'] = 'الشهادات النشطة';
$string['addnewcertification'] = 'اضافة الشهادات الجديدة';
$string['afteractualcertcompletion'] = 'بعد الانتهاء من شهادة الحالية';
$string['afterallocation'] = 'بعد التخصيص';
$string['afterallocationdate'] = 'بعد تاريخ التخصيص';
$string['aftercompletion'] = 'بعد الاتمام';
$string['aftercurrentcertexpdate'] = 'بعد تاريخ انتهاء صلاحية الشهادة الحالية';
$string['afterduedate'] = 'بعد تاريخ الاستحقاق';
$string['afterlatest'] = 'بعد الانتهاء الحالي أو انتهاء الصلاحية';
$string['afterpreviouscertexpdate'] = 'بعد تاريخ انتهاء صلاحية الشهادة السابقة';
$string['afterstartdate'] = 'بعد تاريخ البدء';
$string['allocateusers'] = 'تخصيص المستخدمين';
$string['allocationdate'] = 'تاريخ التخصيص';
$string['allocationdateisonorafter'] = 'في تاريخ التخصيص أو بعده';
$string['allocationenddate'] = 'تاريخ انتهاء التخصيص';
$string['allocationfor'] = 'تخصيص \'{$a} \\';
$string['allocationnoteditable'] = 'التخصيص غير قابل للتحرير';
$string['allocationsource'] = 'مصدر التخصيص';
$string['allocationstartdate'] = 'تاريخ بداية التخصيص';
$string['allocationwindow'] = 'تخصيص الإطار';
$string['allocationwindowenddate'] = 'تاريخ الإنتهاء';
$string['allocationwindowenddate_help'] = 'تاريخ الإنتهاء لتخصيص الإطار';
$string['allocationwindowstartdate'] = 'تاريخ البداية';
$string['allocationwindowstartdate_help'] = 'تاريخ البداية لتخصيص الإطار';
$string['archive'] = 'الأرشيف';
$string['archived'] = 'مؤرشف';
$string['archivedcertifications'] = 'الشهادات المؤرشفة';
$string['archivedconfirmation'] = 'هل أنت متأكد من أرشفة الشهادة \'{$a}\'؟ لن تكون الشهادات المؤرشفة متاحة للمديرين أو المتعلمين وقد يتم عرضها فقط في التقارير التاريخية.';
$string['archivedon'] = 'مؤرشفة على';
$string['autocreategroups'] = 'الاضافة على مجموعة المقرر';
$string['autocreategroupsasinprogram'] = 'كما هو محدد في البرنامج';
$string['autocreategroupscertification'] = 'إنشاء مجموعات لهذه الشهادة';
$string['autocreategroups_help'] = 'عندما يتم تسجيل المستخدمين في دورات البرنامج ، يمكن إضافتهم تلقائيًا إلى نفس المجموعة. في حالة اختيار "لهذه الشهادة" ، سيتم استخدام مجموعة منفصلة لهذه الشهادة وستختلف عن الشهادات الأخرى التي تستخدم نفس البرنامج.';
$string['basic'] = 'أساسي';
$string['basicdetails'] = 'التفاصيل الأساسية';
$string['becamecertified'] = 'أصبحت معتمدة ({$a})';
$string['beforepreviouscertexpdate'] = 'قبل تاريخ انتهاء الشهادة المعتمدة السابقة';
$string['calendarduedate'] = 'تاريخ استحقاق الشهادة {$a}';
$string['calendarexpirydate'] = '{$a} تاريخ انتهاء صلاحية الشهادة';
$string['cannotallocateuser'] = '{$a} لا يمكن تخصيص المستخدم للشهادة';
$string['certification'] = 'الشهادة';
$string['certification:allocateuser'] = 'إذن لتخصيص المستخدم';
$string['certification:configurecustomfields'] = 'ضبط شهادات الحقول المخصصة';
$string['certificationcreated'] = 'تم إنشاء الشهادة';
$string['certification:edit'] = 'إذن للتحرير';
$string['certificationfullname'] = 'الاسم الكامل للشهادة';
$string['certificationfullname_help'] = 'الاسم الكامل للشهادة';
$string['certificationidnumber'] = 'رقم معرف الشهادة';
$string['certificationidnumber_help'] = 'يتم استخدام رقم معرّف الشهادة فقط عند مطابقة الشهادة مع الأنظمة الخارجية ولا يتم عرضها في أي مكان على الموقع. إذا كان للشهادة رمز رسمي ، فيمكن إدخاله ، وإلا يمكن ترك الحقل فارغًا';
$string['certificationname'] = 'إسم الشهادة';
$string['certificationnamewithlink'] = 'إسم الشهادة مع الرابط';
$string['certificationprogress'] = 'تقدم الشهادة';
$string['certificationrules'] = 'قواعد الشهادة';
$string['certifications'] = 'الشهادات';
$string['certificationscustomfield'] = 'الحقول المخصصة للشهادات';
$string['certificationstatus'] = 'حالة الشهادة';
$string['certificationstatus_help'] = 'حالة الشهادة الحالية';
$string['certificationtags'] = 'علامات التصديق';
$string['certificationtags_help'] = 'علامات لهذه الشهادة';
$string['certificationuserlog'] = '{$a} سجل النشاط';
$string['certificationuserlogempty'] = 'لا توجد سجلات أحداث لشهادة المستخدم هذه';
$string['certificationuserlogfilename'] = '{$a->user} سجل النشاط {$a->certification}';
$string['certification:view'] = 'إذن للعرض';
$string['certified'] = 'مصدق عليه بشهادة';
$string['certifiedby'] = 'منح الشهادة من قبل';
$string['certifiedcertifications'] = 'الشهادات المُعتمدة';
$string['certifieddate'] = 'تاريخ منح الشهادة';
$string['certifieddateisonorafter'] = 'تاريخ منح الشهادة في أو على';
$string['certifiednotification'] = 'تم منح الشهادة للمستخدم بنجاح';
$string['certifiedtype'] = 'نوع الشهادة';
$string['certify'] = 'منح شهادة';
$string['certifyexpirydate'] = 'تاريخ الانتهاء';
$string['certifyexpirydate_help'] = 'حدد تاريخ انتهاء الصلاحية لهذا المستخدم المصدق عليه بشهادة بين "الافتراضي" (يعرض تكوين الشهادة الحالي) أو "أبدًا" أو تاريخ مخصص.';
$string['certifyuser'] = 'منح الشهادة للمستخدم';
$string['completedtheprogram'] = '{$a} اكتمال البرنامج';
$string['conditioncertificationcertified'] = 'اعتماد منح الشهادة';
$string['conditioncertificationexpired'] = 'تاريخ انتهاء الشهادة';
$string['conditioncertificationnotcertified'] = 'الشهادة غير معتمدة';
$string['conditioncertificationoverdue'] = 'تأخير الشهادة';
$string['conditioncertificationstatus'] = 'حالة الشهادة';
$string['conditioncertificationstatusdescription'] = 'المستخدمون الذين لديهم حالة
 "{$a->status}"
 في الشهادة "
{$a->fullname} \\';
$string['conditioncertificationsuspended'] = 'تم تعليق الشهادة';
$string['conditionrecertificationgraceperiod'] = 'تنتهي فترة إعادة منح الشهادة';
$string['conditionrecertificationstarted'] = 'بدأت فترة إعادة منح الشهادة';
$string['conditionuserallocated'] = 'تخصيص المستخدمين لتصدير شهادة لهم';
$string['conditionuserallocateddescription'] = 'المستخدمون المخصصون للشهادة
 {$a}';
$string['conditionusernotallocated'] = 'المستخدمون غير المخصصين لإصدار الشهادات';
$string['conditionusernotallocateddescription'] = 'المستخدمون غير المخصصين للشهادة
{$a}';
$string['confirm'] = 'تأكيد';
$string['confirmdelete'] = 'هل أنت متأكد؟';
$string['confirmdeletecertification'] = 'هل أنت متأكد من الغاء الشهادة \'{$a}\' والبيانات المرتبطة بها؟ لا يمكن التراجع عن هذا الاجراء';
$string['confirmdeleteuserallocation'] = 'هل أنت متأكد من حذف تخصيص المستخدم
\'{$a}\'
 والبيانات المرتبطة به تمامًا؟ لا يمكن التراجع عن هذا الإجراء.';
$string['confirmduplicate'] = 'سيتم إنشاء نسخة من تكوين الشهادة وستتمكن من تغيير البرنامج والخصائص الأخرى. لن يتم ترحيل أي تخصيصات للمستخدم إلى النسخة.';
$string['currentprogram'] = 'البرنامج الحالي';
$string['currentprogram_help'] = 'هذا هو البرنامج الحالي الذي يستخدمه هذا المستخدم';
$string['dateisonorafter'] = 'في التاريخ أو بعد';
$string['dateoverrided'] = 'تاريخ التجاوز';
$string['dayssinceallocation'] = 'الأيام منذ التخصيص';
$string['daystakingcertification'] = 'أيام أخذ الشهادات';
$string['default'] = 'إفتراضي';
$string['deleted'] = 'ملغى';
$string['deleteuserallocation'] = 'حذف تخصيص المستخدم';
$string['displaycertificationdate'] = 'تاريخ منح الشهادة';
$string['displaycertificationid'] = 'معرّف الشهادة الداخلي  URL المستخدم في عناوين';
$string['displaycertificationname'] = 'اسم الشهادة';
$string['displayexpirydate'] = 'تاريخ انتهاء الشهادة';
$string['duedate'] = 'تاريخ الاستحقاق';
$string['duedate_help'] = 'تاريخ استحقاق الشهادة';
$string['duedateisonorafter'] = 'في تاريخ الاستحقاق أو بعد';
$string['duplicate'] = 'مكرر';
$string['dynamic'] = 'متفاعل';
$string['dynamicrules'] = 'قوانين التفاعل';
$string['dynamicrulesplugincheck'] = 'من المستحيل عرض قائمة القواعد التفاعل بدون أداة tool_dynamicrules';
$string['dynamicrulewarningdeallocation'] = 'لا يمكن تخصيص المستخدمين إلا إذا تم تخصيصهم بواسطة قاعدة تفاعل أخرى ، ولن تتأثر التخصيصات اليدوية.';
$string['editcertification'] = 'تحرير الشهادة  \'{$a}\\';
$string['editcertificationname'] = 'تحرير اسم الشهادة';
$string['editcertificationsettings'] = 'تحرير اعدادات الشهادة';
$string['editcontent'] = 'تحرير المحتوى';
$string['editdetails'] = 'تحرير التفاصيل';
$string['edituser'] = 'تحرير المستخدم';
$string['enddate'] = 'تاريخ النهاية';
$string['entitycertification'] = 'منح الشهادة';
$string['entitycertificationusers'] = 'تخصيص منح شهادة المستخدمين والانتهاء';
$string['errorallocatinguserintorelatedprogram'] = 'لا يمكن تخصيص المستخدم في البرنامج ذي الصلة';
$string['errorallocationsourcenotfound'] = 'مصدر التخصيص غير موجود';
$string['erroralreadycertified'] = 'المستخدم معتمد بالفعل في هذه الشهادة مع تاريخ البدء هذا';
$string['errorcannotduplicate'] = 'لا يمكن التكرار';
$string['errorcantdeletecertification'] = 'لا يمكن إلغاء الشهادة';
$string['errorcantdeletenotarchivedcertification'] = 'لا يمكن إلغاء الشهادة الغيّر مؤرشفة';
$string['errorcantmanageusers'] = 'لا يمكن إدارة المستخدمين';
$string['errorcantrestorecertification'] = 'لا يمكن استعادة الشهادة';
$string['errorcertificationisarchived'] = 'الشهادة مؤرشفة';
$string['errorcertificationmustbearchived'] = 'يجب أرشفة الشهادة';
$string['errorcertificationnotfound'] = 'لم يتم العثور على الشهادة';
$string['errorevaluatinguserallocationstatus'] = 'خطأ في تقييم حالة تخصيص المستخدم';
$string['errorexpirydatepreviousduedate'] = 'تاريخ الانتهاء لا يمكن أن يكون قبل تاريخ الاستحقاق';
$string['erroridnumberuniquetenant'] = 'رقم التعريف هذا مستخدم بالفعل في شهادة أخرى';
$string['errorinvalidcertification'] = 'شهادة غير صالحة';
$string['errorinvalidcertificationstatus'] = 'حالة الشهادة غير صالحة';
$string['errorinvalidcertifytimecreated'] = 'تاريخ غير صالح في
certifytimecreated';
$string['errorinvaliddate'] = 'تاريخ غير صالح';
$string['errorinvalidexpirydate'] = 'تاريخ غير صالح في
certificationcertifyExire';
$string['errorinvalidgraceperioddate'] = 'لا يمكن أن يكون تاريخ فترة السماح أقل من تاريخ الاستحقاق';
$string['errorinvalidpastexpirydate'] = 'لا يمكن أن يكون تاريخ انتهاء الصلاحية في الماضي';
$string['errorinvalidpaststartdate'] = 'لا يمكن أن يكون تاريخ البدء في الماضي';
$string['errorinvalidtimecertified'] = 'لا يمكن أن يكون الوقت معتمد في المستقبل';
$string['errormissingassociatedprogram'] = 'البرنامج المرتبط مفقود';
$string['errornopermissionmanagecertifications'] = 'لا يوجد إذن لإدارة الشهادات';
$string['errornopermissionviewcertificationlogs'] = 'لا يوجد إذن لعرض سجلات الشهادات';
$string['errornopermissionviewreports'] = 'لا يوجد إذن لعرض التقارير';
$string['errorrecertificationprogram'] = 'يجب أن يكون البرنامج مختلفًا عن البرنامج الأولي';
$string['errorreporttypedoesnotexist'] = 'نوع التقرير غير موجود';
$string['erroruseralreadyallocatedincertification'] = 'تم قبل الأن تخصيص المستخدم في الشهادة';
$string['erroruseralreadyallocatedinprogram'] = 'المستخدم مخصص بالفعل في البرنامج';
$string['errorusernotallocated'] = 'لا يتم تخصيص المستخدم في الشهادة';
$string['errorusernotinsametenant'] = 'المستخدم ليس في نفس حَاصِل عَلَى';
$string['eventcertificationcompleted'] = 'تم اكمال الشهادة';
$string['eventcertificationcreated'] = 'تم إنشاء الشهادة';
$string['eventcertificationdeleted'] = 'تم إلغاء الشهادة';
$string['eventcertificationupdated'] = 'تم تحديث الشهادة';
$string['eventrecertificationgraceperiodended'] = 'انتهت فترة إعادة منح الشهادة';
$string['eventrecertificationstarted'] = 'بدء إعادة منح الشهادة';
$string['eventuserallocated'] = 'تم تخصيصها لشّهادة';
$string['eventuserdeallocated'] = 'المستخدم غير مخصص';
$string['eventuserupdated'] = 'تخصيص المستخدم المحدث';
$string['expired'] = 'منتهية الصلاحية';
$string['expiredcertifications'] = 'الشهادات المنتهية';
$string['expiredcertificationslink'] = '<a href="{$a->href} "> الشهادات منتهية الصلاحية: {$a->count} </a>';
$string['expireddateisonorafter'] = 'تاريخ الانتهاء في أو بعد';
$string['expireson'] = 'تنتهي في
 {$a}';
$string['expirydate'] = 'تاريخ الانتهاء';
$string['expirydate_help'] = 'تاريخ انتهاء الشهادة';
$string['fullname'] = 'الاسم الكامل';
$string['futureallocation'] = 'التخصيص المستقبلي';
$string['helperactionnotallowed'] = 'عمل مساعد غير مسموح به';
$string['idnumber'] = 'معرّف المستخدم';
$string['keepcertificationdefaults'] = 'الحفاظ على الشهادات الافتراضية';
$string['lessthanaday'] = 'أقلّ من يوم';
$string['manageprograms'] = 'إدارة البرامج';
$string['manual'] = 'يدوي';
$string['markcertificationcompletednotice'] = 'وضع علامة على الشهادة على أنها مكتملة دون انتظار إكمال البرنامج';
$string['markcertifyleaveusernotice'] = 'اترك المستخدم مخصصًا للبرنامج دون تاريخ الاستحقاق';
$string['markcertifysuspendnotice'] = 'تعليق المستخدم من البرنامج';
$string['markprogramnotcompleted'] = 'وضع علامة على البرنامج غير مكتمل';
$string['markprogramnotcompleted_help'] = 'وضع علامة على البرنامج المتعلق بهذه الشهادات على أنه غير مكتمل لهذا المستخدم';
$string['messageprovider:certificationcompleted'] = 'الانتهاء من الشهادة';
$string['messageprovider:certificationuserallocated'] = 'تخصيص المستخدم';
$string['messageprovider:certificationuserdeallocated'] = 'المستخدم غير مخصص';
$string['missingcertification'] = 'الشهادة مفقودة';
$string['missingfullname'] = 'الاسم الكامل مفقود';
$string['missingprogram'] = 'البرنامج مفقود';
$string['name'] = 'الاسم';
$string['never'] = 'لا أبدًا';
$string['resetallcoursesprogram'] = 'إعادة تعيين جميع المقرر في هذا البرنامج';
$string['resetallcoursesprogram_help'] = 'إعادة تعيين جميع المقرر لهذا المستخدم في البرنامج المتعلق بهذه الشهادة';
$string['restore'] = 'إستعادة';
$string['revoke'] = ' إبطال';
$string['revokecertification'] = 'إبطال الشهادة';
$string['revoked'] = 'مبطِل';
$string['revokedby'] = 'مبطِل من قبل';
$string['revokednotification'] = 'تم إبطال الشهادة بنجاح';
$string['revokedon'] = 'تم الإبطال الشهادة في';
$string['revokewarning'] = '؟ \'{$a}\' هل أنت متأكد من إبطال الشهادة من
لن يتم إلغاء الجوائز التي قد يتم منحها عند الانتهاء.';
$string['rolemanager'] = 'مدير الشهادات';
$string['rolemanagerdescription'] = 'يسمح بإنشاء وإدارة الشهادات داخل النزيل الحالي وتخصيص المستخدمين لهم';
$string['schedule'] = 'تواريخ الشهادات';
$string['scheduleupdatesuccess'] = 'تم حفظ التغييرات الخاصة بك بنجاح.';
$string['selectadifferentprogram'] = 'ًََاختر برنامجاََ مختلفا';
$string['selectadifferentprogram_help'] = 'حدد برنامجًا مختلفًا لإعادة منح الشهادة';
$string['selectcertification'] = 'منح الشهادة';
$string['selectcertificationcondition'] = 'منح الشهادة';
$string['selectcertificationcondition_help'] = 'حدد الشهادة بحيث يتم تطبيق هذا الشرط فيها';
$string['selectcertificationoutcome'] = 'منح الشهادة';
$string['selectcertificationoutcome_help'] = 'حدد الشهادة بحيث يتم تطبيق هذا الإجراء فيها';
$string['selectdate'] = 'إختر التاريخ';
$string['selectprogram'] = 'إختر البرنامج';
$string['selectprogram_help'] = 'إختر برنامج';
$string['startdate'] = 'تاريخ البداية';
$string['startdate_help'] = 'تاريخ بداية الشهادة';
$string['status'] = 'الحالة';
$string['suspended'] = 'معلق';
$string['suspendeddateisonorafter'] = 'تاريخ التعليق أو بعده';
$string['suspendprogallocation'] = 'تعليق تخصيص البرنامج';
$string['suspendprogallocation_help'] = 'إذا لم يتم تعليق البرنامج ، فسيظهر البرنامج على لوحة معلومات المستخدم على أنه لم يكتمل بدون تاريخ الاستحقاق';
$string['system'] = 'النظام';
$string['tagarea_tool_certification'] = 'الشهادات';
$string['tags'] = 'العلامات';
$string['tags_help'] = 'مساعدة العلامات';
$string['timecertified'] = 'الوقت المحقق';
$string['timecertified_help'] = 'تم اعتماد تاريخ ووقت المستخدم. يمكنك فقط التواريخ الماضية أو الحالية';
$string['timecreated'] = 'تم الانشاء في';
$string['timemodified'] = 'آخر التعديل كان في';
$string['timesuspended'] = 'التعليق كان في';
$string['toomanycertificationstoshow'] = 'هناك شهادات كثيرة لاظهارها';
$string['toomanyprogramstoshow'] = 'هناك برامج كثيرة لاظهارها';
$string['uponcompletion'] = 'عند الاكتمال';
$string['userallocation'] = 'تخصيص المستخدمين';
$string['usercompletion'] = 'اكتمال المستخدمين';
$string['userduedate'] = 'تاريخ الاستحقاق';
$string['userduedate_help'] = 'حدد تاريخ استحقاق الشهادة لهذا المستخدم.';
$string['usergotsuspended'] = 'تم ايقاف المستخدم';
$string['users'] = 'المستخدمون';
$string['userstartdate'] = 'تاريخ البداية';
$string['userstartdate_help'] = 'حدد التاريخ الذي سيتمكن فيه هذا المستخدم من بدء الشهادة. سيتم تطبيق هذا التاريخ فقط على هذا المستخدم.';
$string['userstatus'] = 'الحالة';
$string['userstatus_help'] = 'ستقوم حالة التوقيف بإخفاء هذه الشهادة من هذا المستخدم بينما ستظهر الحالة الإفتراضية.';
$string['viewcertificationuserlog'] = 'سجل نشاط الشهادة';
$string['warningcertificationprogram'] = 'تحذير: بمجرد إنشاء شهادة ، لن يكون هذا الإعداد قابلاً للتحرير';
