<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'widgetspace', language 'ar', branch 'MOODLE_21_STABLE'
 *
 * @package   widgetspace
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['contentheader'] = 'المحتوى';
$string['displayoptions'] = 'خيارات العرض المتاحة';
$string['displayselect'] = 'عرض';
$string['displayselectexplain'] = 'اختر نوع العرض';
$string['gadget'] = 'الأدوات الذكية';
$string['gadgetoptions'] = 'خيارات الأدوات الذكية';
$string['gadgeturl'] = 'رابط الأداة الذكية';
$string['layoutsettings'] = 'اعدادات طريقة عرض الأدوات الذكية';
$string['legacyfiles'] = 'نقل ملف المقرر القديم';
$string['legacyfilesactive'] = 'نشط';
$string['legacyfilesdone'] = 'منتهي';
$string['neverseen'] = 'لم تتم مشاهدته';
$string['numbercolumn'] = 'عدد الأعمدة';
$string['optionsheader'] = 'الخيارات';
