<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'course', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   course
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['aria:coursecategory'] = 'تصنيف المقرر';
$string['aria:courseimage'] = 'صورة المقرر';
$string['aria:coursename'] = 'اسم المقرر';
$string['aria:courseshortname'] = 'الاسم القصير للمقرر';
$string['aria:favourite'] = 'المقرر مميز بنجمة';
$string['coursealreadyfinished'] = 'لقد انتهى المقرر بالفعل';
$string['coursenotyetfinished'] = 'لم ينتهي المقرر الدراسي بعد';
$string['coursenotyetstarted'] = 'لم يبدأ المقرر بعد';
$string['coursetoolong'] = 'المقرر طويل جداً';
$string['customfield_islocked'] = 'مغلق';
$string['customfield_islocked_help'] = 'إذا تم تأمين الحقل ، فسيتمكن المستخدمون الذين لديهم القدرة على تغيير الحقول المخصصة المقفلة فقط (بشكل افتراضي المستخدمين ذوي الدور الافتراضي للمدير فقط) من تغييره في إعدادات المقرر.';
$string['customfield_notvisible'] = 'لا أحد';
$string['customfieldsettings'] = 'اعدادات الحقول المخصصة للمقرر';
$string['customfield_visibility'] = 'ظاهر لـ';
$string['customfield_visibility_help'] = 'يحدد هذا الإعداد من يمكنه عرض اسم وقيمة الحقل المخصص في قائمة المقررات.';
$string['customfield_visibletoall'] = 'الكل';
$string['customfield_visibletoteachers'] = 'المدرسون';
$string['errorendbeforestart'] = 'تاريخ النهاية ({$a}) يسبق تاريخ بداية المقرر';
$string['favourite'] = 'المقررات المميزة بنجمه';
$string['gradetopassnotset'] = 'لا تحتوي هذه الدورة على تقدير. ربما يكون تم وضعه على عناصر التقدير للمقرر (إعداد دفتر التقديرات).';
$string['nocourseactivity'] = 'لا يوجد نشاط كافي بين بداية ونهاية المقرر';
$string['nocourseendtime'] = 'المقرر لا يحتوي على تاريخ نهاية';
$string['nocoursesections'] = 'لا توجد أقسام بالمقرر';
$string['nocoursestudents'] = 'لا يوجد طلاب';
$string['privacy:completionpath'] = 'إكتمال مقرر دراسي';
$string['privacy:favouritespath'] = 'معلومات المقررات المميزة بنجمة';
$string['privacy:metadata:completionsummary'] = 'يحتوي المقرر على معلومات الإكمال عن المستخدم';
$string['privacy:metadata:favouritessummary'] = 'يحتوي المقرر على معلومات تتعلق بالمقرر المميز بنجمة من قبل المستخدم.';
$string['privacy:perpage'] = 'عدد المقررات الدراسية في الصفحة الواحدة';
$string['studentsatriskincourse'] = 'الطالب في خطر في مقرر {$a}';
$string['target:coursecompetencies'] = 'الطلاب المعرضين لخطر عدم تحقيق الكفاءات المخصصة للمقرر';
$string['target:coursecompetencies_help'] = 'يصف هذا الهدف ما إذا كان الطالب معرضًا لخطر عدم تحقيق الكفاءات المخصصة للمقرر. يعتبر هذا الهدف أن جميع الكفاءات المخصصة للدورة يجب أن تتحقق بحلول نهاية المقرر.';
$string['target:coursecompletion'] = 'الطلاب المعرضون لخطر عدم استيفاء شروط اكمال المقرر';
$string['target:coursecompletion_help'] = 'يصف هذا الهدف ما إذا كان الطالب يعتبر معرضًا لخطر عدم استيفاء شروط إكمال المقرر.';
$string['target:coursedropout'] = 'الطلاب المعرضون لخطر الفصل';
$string['target:coursedropout_help'] = 'يصف هذا الهدف ما إذا كان الطالب يعتبر معرضًا لخطر الفصل.';
$string['target:coursegradetopass'] = 'الطلاب المعرضين لخطر عدم تحقيق الحد الأدنى من الدرجة لاجتياز المقرر';
$string['target:coursegradetopass_help'] = 'يصف هذا الهدف ما إذا كان الطالب معرضًا لخطر عدم تحقيق الحد الأدنى من الدرجة لاجتياز المقرر.';
$string['targetlabelstudentcompetenciesno'] = 'الطالب الذي من المحتمل أن يحقق الكفاءات المخصصة للمقرر';
$string['targetlabelstudentcompetenciesyes'] = 'طالب في خطر عدم تحقيق الكفاءات المخصصة للمقرر';
$string['targetlabelstudentcompletionno'] = 'الطالب الذي من المحتمل أن يستوفي شروط إكمال الدورة';
$string['targetlabelstudentcompletionyes'] = 'طالب في خطر عدم استيفاء شروط إكمال المقرر';
$string['targetlabelstudentdropoutno'] = 'ليس في خطر';
$string['targetlabelstudentdropoutyes'] = 'طالب معرض للفصل';
$string['targetlabelstudentgradetopassno'] = 'الطالب الذي من المرجح أن يحقق الحد الأدنى من الدرجة لاجتياز المقرر.';
$string['targetlabelstudentgradetopassyes'] = 'طالب في خطر عدم استيفاء الحد الأدنى من الدرجة لاجتياز المقرر.';
$string['targetlabelteachingno'] = 'بدون تدريس';
$string['targetlabelteachingyes'] = 'المستخدمون الذين لديهم صلاحيات التدريس الذين يستطيعون الوصول للمقرر';
$string['target:noteachingactivity'] = 'بدون تدريس';
$string['target:noteachingactivity_help'] = 'يصف هذا الهدف ما إذا كانت الدورات التدريبية المقرر أن تبدأ في الأسبوع القادم سيكون لها نشاط تعليمي.';
