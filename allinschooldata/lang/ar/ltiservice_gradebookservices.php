<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'ltiservice_gradebookservices', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   ltiservice_gradebookservices
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alwaysgs'] = 'استخدام هذه الخدمة لمزامنة الدرجات و إدارة الأعمدة';
$string['grade_synchronization'] = 'IMS LTI المهام و خدمات الدرجات';
$string['grade_synchronization_help'] = 'ما إذا كنت تريد استخدام IMS LTI Assignment and Grade Services لمزامنة التقديرات بدلاً من خدمة Outcomes Basic.
* ** لا تستخدم هذه الخدمة
** - سيتم استخدام ميزات التكوين الأساسي و التكوينات
 ** ** استخدم هذه الخدمة لمزامنة التقديرات فقط
** - ستعمل الخدمة على ملء التقديرات في عمود دفتر التقديرات الموجود بالفعل ، و لكنها لن تكون قادرة على إنشاء أعمدة جديدة
 ** ** استخدم هذه الخدمة لإدارة الدرجات و المزامنة
** - ستكون الخدمة قادرة على إنشاء و تحديث أعمدة دفتر التقديرات و إدارة التقديرات.';
$string['ltiservice_gradebookservices'] = 'IMS LTI المهام و خدمات الدرجات';
$string['modulename'] = 'درحات أدوات التعلم المشترك (LTI).';
$string['nevergs'] = 'لا تستخدم هذه الخدمة';
$string['partialgs'] = 'استخدم هذه الخدمة لمزامنة الدرجات فقط';
$string['pluginname'] = 'أدوات التعلم المشترك (LTI) للمهام و خدمات الدرجات';
$string['privacy:metadata:externalpurpose'] = 'يتم إرسال هذه المعلومات إلى مزود  أدوات التعلم المشترك (LTI) الخارجي.';
$string['privacy:metadata:feedback'] = 'التعليقات التي تلقاها المستخدم من نشاط  أدوات التعلم المشترك (LTI).';
$string['privacy:metadata:grade'] = 'الدرجة التي تلقاها المستخدم في Moodle لهذا النشاط من أدوات التعلم المشترك (LTI).';
$string['privacy:metadata:maxgrade'] = 'الدرجة القصوى التي يمكن تحقيقها لهذا النشاط  من أدوات التعلم المشترك (LTI).';
$string['privacy:metadata:timemodified'] = 'آخر مرة تم فيها تحديث الدرجة';
$string['privacy:metadata:userid'] = 'معرف المستخدم الذي يستخدم  أدوات التعلم المشترك (LTI) كعميل .';
$string['taskcleanup'] = 'تنظيف جدول المهام و خدمات التقويم في  أدوات التعلم المشترك (LTI)';
