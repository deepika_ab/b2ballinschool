<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'grouptool', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   grouptool
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['createinsertgrouping'] = 'إضافة للتجميع';
$string['deactivated_groups'] = 'المجموعات المُعطَّلة';
$string['description'] = 'الوصف';
$string['deselect'] = 'إلغاء التحديد';
$string['disabled'] = 'تعطيل';
$string['drag'] = 'نقل';
$string['duedateno'] = 'لا تاريخ استحقاق';
$string['error_at'] = 'خطأ في';
$string['eventgroupingscreated'] = 'تم إنشاء التجميعات';
$string['eventgrouprecreated'] = 'تم إعادة  إنشاء المجموعة';
$string['fullgroup'] = 'المجموعة كاملة';
$string['group_creation_alt'] = 'أنشئ مجموعات';
