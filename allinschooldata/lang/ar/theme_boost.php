<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_boost', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_boost
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['advancedsettings'] = 'إعدادات متقدمة';
$string['backgroundimage'] = 'صورة الخلفية';
$string['backgroundimage_desc'] = 'الصورة التي سيتم عرضها كخلفية للموقع. ستتجاوز صورة الخلفية التي تحمّلها هنا صورة الخلفية في ملفات الإعدادات المسبقة للمظهر.';
$string['bootswatch'] = 'Bootswatch';
$string['bootswatch_desc'] = 'Bootswatch عبارة عن مجموعة من متغيرات Bootstrap و css إلى نمط Bootstrap';
$string['brandcolor'] = 'لون العلامة التجارية';
$string['brandcolor_desc'] = 'اللون السائد.';
$string['choosereadme'] = 'Boost هو مظهر حديث قابل للتخصيص بدرجة عالية. الغرض من هذا المظهر هو الاستخدام مباشرة ، أو كمسألة رئيسية عند إنشاء مظاهر جديدة باستخدام Bootstrap 4.';
$string['configtitle'] = 'Boost';
$string['currentinparentheses'] = '(حالي)';
$string['generalsettings'] = 'الاعدادات العامة';
$string['nobootswatch'] = 'لا شيء';
$string['pluginname'] = 'Boost';
$string['preset'] = 'المظهر السابق';
$string['preset_desc'] = 'اختر إعدادًا مسبقًا لتغيير سمة المظهر على نطاق واسع.';
$string['presetfiles'] = 'ملفات المظهر المحددة مسبقا';
$string['presetfiles_desc'] = 'يمكن استخدام ملفات الإعداد المسبق لتغيير سمة المظهر بشكل كبير. راجع إعدادات Boost المسبقة للحصول على معلومات حول إنشاء ومشاركة ملفات الإعداد المسبق الخاصة بك ، وانظر مستودع الإعدادات المسبقة للإعدادات المسبقة التي شاركها الآخرون.';
$string['privacy:drawernavclosed'] = 'التفضيل الحالي لدرج التنقل مغلق.';
$string['privacy:drawernavopen'] = 'التفضيل الحالي لدرج التنقل مفتوح.';
$string['privacy:metadata'] = 'مظهر Boost لا يخزّن أي بيانات شخصية عن أيّ مستخدم.';
$string['privacy:metadata:preference:draweropennav'] = 'تفضيل المستخدم لإخفاء أو إظهار درج التنقل في القائمة الرئيسية.';
$string['rawscss'] = 'SCSS الأصلية';
$string['rawscss_desc'] = 'استخدم هذا الحقل لتوفير كود SCSS أو CSS الذي سيتم حقنه في نهاية ورقة الأنماط.';
$string['rawscsspre'] = 'SCSS الأصلية الأولية';
$string['rawscsspre_desc'] = 'في هذا الحقل ، يمكنك توفير تهيئة كود SCSS ، سيتم حقنه قبل أيّ شيء آخر. معظم الوقت سوف تستخدم هذا الإعداد لتحديد المتغيرات.';
$string['region-side-pre'] = 'يمين';
