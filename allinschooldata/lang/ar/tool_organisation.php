<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_organisation', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   tool_organisation
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addpositionframework'] = 'إطار عمل جديد';
$string['anydepartment'] = 'أَيّما';
$string['anyposition'] = 'أَيّما';
$string['assfirstchildof'] = 'كأول فرع ل \'{$a}\\';
$string['audienceand'] = 'و';
$string['audiencecustomise'] = 'تعديل';
$string['audienceor'] = 'أو';
$string['audienceselect'] = 'العلاقة بمشاهد التقرير';
$string['audienceselectinitial'] = 'اختر جمهور';
$string['audienceusersall'] = 'جميع المستخدمين';
$string['audienceusersdept'] = 'في نفس قسم عارض التقرير';
$string['audienceusersreporting'] = 'التقارير لمُشاهد التقرير';
$string['audienceusersreporting_help'] = 'قم بتضمين المستخدمين الذين يقدمون تقارير مباشرة إلى عارض التقرير (الذين يجب أن يكون لديهم مدير )';
$string['cachedef_myjob'] = 'وظيفة المستخدم الحالي وفريقهم';
$string['conditioncanallocateprograms'] = 'يمكن التخصيص للبرامج';
$string['conditioncanreceivenotifications'] = 'يمكن تلقي الإشعارات';
$string['conditioncanviewreports'] = 'يمكن رؤية التقارير';
$string['conditionuserdepartment'] = 'المستخدم في القسم';
$string['conditionuserdepartmentdescription'] = 'تم تخصيص المستخدم للقسم';
$string['conditionuserdepartmentdescriptionnegated'] = 'لم يتم تخصيص المستخدم للقسم
\'{$a} \\';
$string['conditionusernotindepartment'] = 'المستخدم ليس في القسم';
$string['conditionuserposition'] = 'المستخدم لديه موضع';
$string['conditionuserpositiondescription'] = 'المستخدم لديه موضع
 "{$a} \\';
