<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'bigbluebuttonbn', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   bigbluebuttonbn
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['mod_form_field_notification'] = 'إرسل إشعار';
$string['mod_form_field_participant_bbb_role_viewer'] = 'مشاهد';
$string['mod_form_field_participant_list_action_add'] = 'اضافة';
$string['mod_form_field_participant_list_type_user'] = 'مستخدم';
$string['starts_at'] = 'البدء';
$string['view_conference_action_end'] = 'انهاء الجلسة';
$string['view_conference_action_join'] = 'انضم الى الجلسة';
$string['view_message_conference_has_ended'] = 'هذا المؤتمر انتهى';
$string['view_message_conference_in_progress'] = 'هذا المؤتمر قيد التنفيذ';
$string['view_message_conference_not_started'] = 'هذا المؤتمر لم يبدأ بعد';
$string['view_message_hour'] = 'ساعة';
$string['view_message_hours'] = 'ساعات';
$string['view_message_minute'] = 'دقيقة';
$string['view_message_minutes'] = 'دقائق';
$string['view_message_user'] = 'مستخدم';
$string['view_message_users'] = 'مستخدمين';
$string['view_message_viewer'] = 'مشاهد';
$string['view_message_viewers'] = 'مشاهدين';
$string['view_recording'] = 'تسجيل';
$string['view_recording_date'] = 'التاريخ';
$string['view_recording_description'] = 'وصف';
$string['view_recording_duration'] = 'مدة';
$string['view_recording_duration_min'] = 'دقيقة';
$string['view_recording_format_video'] = 'فيديو';
$string['view_recording_list_actionbar_delete'] = 'حذف';
$string['view_recording_list_course'] = 'الدورة';
$string['view_recording_list_date'] = 'التاريخ';
$string['view_recording_list_duration'] = 'مدة';
$string['view_recording_list_recording'] = 'تسجيل';
$string['view_recording_name'] = 'اسم';
$string['view_recording_recording'] = 'تسجيلات';
$string['view_section_title_presentation'] = 'ملف العرض';
$string['view_section_title_recordings'] = 'التسجيلات';
