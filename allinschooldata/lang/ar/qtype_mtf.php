<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_mtf', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   qtype_mtf
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['answernumbering'] = 'ترقيم الخيارات؟';
$string['answernumbering123'] = '1., 2., 3., ...';
$string['answernumberingabc'] = 'a., b., c., ...';
$string['answernumberingABCD'] = 'A., B., C., ...';
$string['answernumberingiii'] = 'i., ii., iii., ...';
$string['answernumberingIIII'] = 'I., II., III., ...';
$string['answernumberingnone'] = 'بدون ترقيم';
$string['answersingleno'] = 'إجابات متعددة';
$string['configintro'] = 'القيم الافتراضية للأسئلة صح/حطأ المتعددة.';
$string['configscoringmethod'] = 'طريقة التسجيل الافتراضية للأسئلة صح/حطأ المتعددة.';
$string['configshuffleanswers'] = 'الإعداد الافتراضي لخلط الخيار في أسئلة صح/حطأ المتعددة.';
$string['correctresponse'] = 'الرد الصحيح';
$string['deleterawswarning'] = 'عند خفض عدد الخيارات ، سيتم حذف الخيارات الفائضة. هل أنت متأكد أنك تريد المتابعة؟';
$string['enterfeedbackhere'] = 'أدخل التعليق هنا.';
$string['entergeneralfeedbackhere'] = 'أدخل التعليقات العامة هنا.';
$string['enterstemhere'] = 'أدخل الصلب أو موجه السؤال هنا.';
$string['false'] = 'الخاطئة';
$string['feedbackforoption'] = 'ردود الفعل لـ';
$string['generalfeedback'] = 'ملاحظات عامة.';
$string['generalfeedback_help'] = 'يتم عرض نفس التعليقات العامة بغض النظر عن الإجابة المختارة.
استخدم التغذية الراجعة العامة .. على سبيل المثال لشرح الإجابات الصحيحة أو إعطاء الطلاب رابطًا لمعلومات إضافية.';
$string['incorrect'] = 'غير صحيح';
$string['maxpoints'] = 'أقصى نقاط';
$string['mustdeleteextrarows'] = 'خيارات الحد الأقصى المسموح به في MTF هي 5 خيارات. سيتم حذف الخيار (الخيارات) {$a}. إذا قمت بإلغاء التحرير دون حفظ ، فستبقى الخيارات الفائضة.';
$string['mustsupplyresponses'] = 'يجب عليك توفير قيم لجميع الردود.';
$string['mustsupplyvalue'] = 'يجب عليك توفير قيمة هنا.';
$string['notenoughanswers'] = 'يتطلب هذا النوع من الأسئلة خيار {$a} على الأقل';
$string['numberofrows'] = 'عدد الخيارات';
$string['numberofrows_help'] = 'حدد عدد الخيارات. عند التغيير إلى خيارات أقل ، سيتم حذف خيارات الفائض بمجرد حفظ العنصر.';
$string['optionno'] = 'الخيار {$a}';
$string['optionsandfeedback'] = 'خيارات و ردود الفعل';
$string['pluginname'] = 'MTF (ETH)';
$string['pluginnameadding'] = 'إضافة سؤال متعدد صح/خطأ';
$string['pluginnameediting'] = 'تحرير سؤال صح/خطأ متعدد';
$string['pluginname_help'] = 'رداً على سؤال ، حث خيارات معدل المرشحين وفقًا للمعايير المقدمة ، مثل "صواب" / "خطأ".';
$string['pluginnamesummary'] = 'في أسئلة  صح/خطأ  المتعددة ("النوع X") ، يجب تصنيف عدد من الخيارات بشكل صحيح على أنها "صواب" أو "خطأ".';
$string['privacy:metadata'] = 'لا يخزن المكون الإضافي  MTF question type  أيّ بيانات شخصية.';
$string['responsedesc'] = 'النص المستخدم كإعداد افتراضي للاستجابة {$a}.';
$string['responseno'] = 'الرد {$a}';
$string['responsetext'] = 'نص الرد {$a}';
$string['responsetext1'] = 'صحيح';
$string['responsetext2'] = 'خاطئة';
$string['responsetexts'] = 'خيارات الحكم';
$string['save'] = 'حفظ';
$string['scoringmethod'] = 'طريقة التسجيل';
$string['scoringmethod_help'] = 'هناك طريقتان بديلتان لتسجيل الأهداف.
النقاط الفرعية (مستحسن): يتم منح الطالب نقاط فرعية لكل استجابة صحيحة.
MTF1 / 0 : يتلقى الطالب نقاط كاملة إذا كانت جميع الردود صحيحة ، و نقطة الصفر غير ذلك.';
$string['scoringmtfonezero'] = 'MTF1 / 0';
$string['scoringsubpoints'] = 'نقاط فرعية';
$string['shuffleanswers'] = 'خيارات خلط الإستجابات';
$string['shuffleanswers_help'] = 'في حالة التمكين ، يتم ترتيب الخيارات بشكل عشوائي لكل محاولة ، شريطة أن يتم أيضًا تمكين "التبديل بين الأسئلة" في إعدادات النشاط.';
$string['stem'] = 'الأصل';
$string['tasktitle'] = 'عنوان المهمة';
$string['true'] = 'صيح';
