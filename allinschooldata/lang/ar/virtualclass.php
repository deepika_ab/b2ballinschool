<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'virtualclass', language 'ar', branch 'MOODLE_30_STABLE'
 *
 * @package   virtualclass
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['anyonepresenter'] = 'يمكن لأي شخص أن يكون المقدّم';
$string['anyonepresenter_help'] = 'إذا تم تفعيله أي شخص (حتى الطالب) يستطيع التقديم';
$string['askplaymsg'] = '<span id="askplaymsg"> "التنزيل قيد التقدم، اضغط تشغيل للبدء </span>"';
$string['audio'] = 'الصوت';
$string['audio_help'] = 'إذا تم تفعيله، سيتم تفعيل الصوت لدى كل الطلاب';
$string['audiotest'] = 'اختبر الصوت';
$string['closebeforeopen'] = 'لا يمكن حفظ الفصل الافتراضي. لقد حددت تاريخاً قريباً من تاريخ الفتح.';
$string['closenotset'] = 'يحب تحديد تاريخ قريب.';
$string['closesameopen'] = 'لا يجب أن يتشابه زمنا بدء الجلسة وانتهائها';
$string['closetime'] = 'تم إغلاق الجلسات';
$string['configactiontolocalrun'] = 'إذا اخترت "محلي"  ستكون شهادة السرية SSL (https://) مطلوبة عند استخدام ميزة مشاركة الشاشة.';
$string['customsetting'] = 'إعدادات مخصصة';
$string['disableAudio'] = 'تعطيل الصوت';
$string['disablespeaker'] = 'كتم';
$string['downloadsession'] = 'الرجاء الانتظار أثناء يتم تحميل التسجيل.';
$string['enableAudio'] = 'تشغيل الصوت';
$string['enablespeaker'] = 'تشغيل الصوت';
$string['indvprogress'] = 'المهمة الحالية';
$string['joinroom'] = 'الانضمام إلى غرفة الفصل الافتراضي';
$string['liverun'] = 'عبر الانترنت - استخدم سيرفر vidya.io';
$string['localrun'] = 'محلي - استخدم ملفات موودل';
$string['modulename'] = 'الفصل الافتراضي';
$string['modulename_help'] = 'استخدم وحدة الفصول الافتراضية للتعلم عبر الانترنت في الوقت الفعلي.
تسمح لك الفصول الافتراضية بالمشاركة في التعلم المتزامن، مما يعني أن الأساتذة والطلاب يدخلون بيئة التعلم الافتراضي ويتفاعلون مع بعضهم البعض في ذات الوقت.
تزود هذه الوحدة الطلاب  بأدوات تواصل متزامنة مثل السبورة البيضاء وامكانيات الدردشة';
$string['modulenameplural'] = 'الفصل الافتراضي';
$string['opentime'] = 'الجلسات مفتوحة';
$string['overallprogress'] = 'التقدم الكامل';
$string['play'] = 'تشغيل';
$string['pluginadministration'] = 'إدارة الفصل الافتراضي';
$string['pluginname'] = 'الفصل الافتراضي';
$string['pressalways'] = 'اضغط دائماً للتحدث';
$string['pressAlwaysToSpeak'] = 'اضغط دائماً للتحدث.';
$string['pressonce'] = 'اضغط مرة واحدة للتحدث';
$string['pushtotalk'] = 'اضغط للتحدث';
$string['pushtotalk_help'] = 'إذا تم تفعيله، سيتوفر خيار إضافي لعرض اختبار الصوت.';
$string['replay'] = 'إعادة تشغيل';
$string['replay_message'] = 'شكراً للمشاهدة.';
$string['selectcolor'] = 'اختر لون';
$string['selectcolor_help'] = 'اختر لونك الخاص لفصلك';
$string['selectteacher'] = 'اختر مدرس';
