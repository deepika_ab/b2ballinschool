<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_moove', language 'ar', branch 'MOODLE_38_STABLE'
 *
 * @package   theme_moove
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['access'] = 'الوصول';
$string['addcontact'] = 'أضف اتصال';
$string['advancedsettings'] = 'متقدم';
$string['bannercontent'] = 'محتوى الرآية';
$string['bannercontentdesc'] = 'ادخل محتوى عنوان الرآية';
$string['bannerheading'] = 'عنوان الرآية';
$string['bannerheadingdesc'] = 'ادخل نص عنوان الرآية';
$string['brandcolor'] = 'لون العلامة التجارية';
$string['brandcolor_desc'] = 'اللون السائد';
$string['by'] = 'مع';
$string['cachedef_admininfos'] = 'لوحة معلومات مدير الموقع';
$string['choosereadme'] = 'Moove هو مظهر حديث لغاية التخصيص. الغرض من هذا المظهر هو استخدامه مباشرة ، أو كمظهر رئيس عند إنشاء مظاهر جديدة باستخدام Bootstrap 4.';
$string['clientscount'] = 'عدد العملاء';
$string['clientscountdesc'] = 'حدد عدد شعارات العملاء ثم انقر (حفظ) لتحميل حقول الإدخال';
$string['clientsfrontpage'] = 'تمكين صف العملاء';
$string['clientsfrontpagedesc'] = 'في حالة التمكين ، سيعرض صف باستخدام شعارات العملاء و روابطها.';
$string['clientsimage'] = 'شعار العميل';
$string['clientsimagedesc'] = 'اضف شعارات العملاء.';
$string['clientssubtitle'] = 'العنوان الثانوي للعملاء';
$string['clientssubtitledefault'] = 'عملائنا الصادقين معنا';
$string['clientssubtitledesc'] = 'العنوان الثانوي لصف العملاء';
$string['clientstitle'] = 'ألقاب العملاء';
$string['clientstitledefault'] = 'عملائنا';
$string['clientstitledesc'] = 'عنوان صف العملاء.';
$string['clientsurl'] = 'رابط العميل';
$string['clientsurldesc'] = 'ادخل رابط العميل';
$string['competencyplans'] = 'خطط الكفاءة';
$string['configtitle'] = 'Moove';
$string['coursecover'] = 'صورة الغلاف';
$string['coursedefault'] = 'العرض الافتراضي';
$string['courselistview'] = 'تمكين عرض قائمة المواد';
$string['courselistviewdesc'] = 'تبديل عرض المواد من مربعات إلى قائمة';
$string['coursepresentation'] = 'عروض المواد';
$string['coursepresentationdesc'] = 'العرض الافتراضي: مظهر المواد الافتراضي.
صورة الغلاف: ستظهر صورة الغلاف في مقدمة صفحة المادة (سنستخدم أول صورة في ملف ملخص لمادة).';
$string['coursesections'] = 'أقسام المادة';
$string['currentinparentheses'] = '(حالياً)';
$string['details'] = 'تفاصيل';
$string['disablebottomfooter'] = 'تعطيل الحاشية السفلية';
$string['disablebottomfooterdesc'] = 'تعطيل الحاشية السفلية البرتقالية';
$string['discipline_progress'] = 'التقدم في الموضيع';
$string['diskusage'] = 'المساحة المستخدمة (بيانات مودل)';
$string['displaymarketingbox'] = 'عرض صندوق التسويق في الصفحة الأولى';
$string['displaymarketingboxdesc'] = 'عرض أو إخفاء صندوق التسويق في الصفحة الأولى';
$string['facebook'] = 'رابط الفيس بوك';
$string['facebookdesc'] = 'ادخل رابط الفيس بوك ...';
$string['favicon'] = 'تخصيص الأيقونة المفضلة';
$string['favicondesc'] = 'قم بتحميل الأيقونة الخاصة بك. يجب أن يكون ملف .ico.';
$string['footersettings'] = 'الحاشية';
$string['forumcustomtemplate'] = 'استخدام قالب جميل لرسائل المنتدى';
$string['forumcustomtemplatedesc'] = 'تمكين ذلك من أجل استخدام قالب جميل عند إرسال رسائل المنتدى عبر البريد الإلكتروني.
إذا لم يتم التمكين سيتم استخدامه التنسيق القياسي لرسائل المنتدى .';
$string['forumhtmlemailfooter'] = 'حاشية البريد';
$string['forumhtmlemailfooterdesc'] = 'إعداد حاشية البريد لرسائل الموقع';
$string['forumhtmlemailheader'] = 'مقدمة البريد الإلكتروني';
$string['forumhtmlemailheaderdesc'] = 'إعداد مقدمة البريد لرسائل الموقع';
$string['forumsettings'] = 'المنتدى';
$string['forumsettingsdesc'] = 'الإعدادات العامة للموقع للمظهر من هنا.';
$string['frontpagenumberactivities'] = 'الأنشطة';
$string['frontpagenumbercourses'] = 'المواد';
$string['frontpagenumbernumbers'] = 'الأرقام';
$string['frontpagenumbersome'] = 'بعض';
$string['frontpagenumberusers'] = 'المستخدمين';
$string['frontpagesettings'] = 'صفحة الواجهة';
$string['generalsettings'] = 'عام';
$string['getintouchcontent'] = 'محتوى "ابق على اتصال"';
$string['getintouchcontentdesc'] = 'ادخل نص "ابق على اتصال"';
$string['googleanalytics'] = 'شفرة إحصائيات قوقل';
$string['googleanalyticsdesc'] = 'الرجاء إدخال رمز Google Analytics لتمكين التحليلات على موقع الويب الخاص بك. يجب أن يكون تنسيق الشفرة مثل [UA-XXXXX-Y]';
$string['googleplus'] = 'رابط Google+';
$string['googleplusdesc'] = 'ادخل رابط Google+';
$string['headerimg'] = 'صورة المقدمة';
$string['headerimgdesc'] = 'تحميل صورة المقدمة الخاصة بك هنا إذا كنت تريد إضافتها إلى المقدمة. تعمل الصورة بالشكل الأفضل إذا (يجب أن يكون حجم الصورة 1500 بكسل × 700 بكسل)';
$string['instagram'] = 'رابط الإنستغرام';
$string['instagramdesc'] = 'ادخل رابط الإنستغرام';
$string['linkedin'] = 'رابط لينكد إن';
$string['linkedindesc'] = 'ادخل رابط اللينكد إن';
$string['login'] = 'لديّ حساب فعلاً !!';
$string['loginbgimg'] = 'خلفية صفحة الدخول';
$string['loginbgimg_desc'] = 'قم بتحميل صورة الخلفية المخصصة لصفحة تسجيل الدخول.';
$string['logo'] = 'الشعار';
$string['logodesc'] = 'الشعار يظهر في قمة الموقع.';
$string['madeby'] = 'تم بناءه مع';
$string['madewitmoodle'] = 'تم إنشاؤه بفخر مع';
$string['mail'] = 'البريد الإلكتروني';
$string['maildesc'] = 'ادخل معرف البريد الإلكتروني';
$string['marketing1content'] = 'محتوى الصندوق1';
$string['marketing1contentdesc'] = 'أدخل نص محتوى الصندوق1';
$string['marketing1heading'] = 'عنوان الصندوق1';
$string['marketing1headingdesc'] = 'أدخل نص عنوان الصندوق1';
$string['marketing1icon'] = 'أيقونة الصندوق 1';
$string['marketing1icondesc'] = 'حمل أيقونة الصندوق1';
$string['marketing1subheading'] = 'العنوان الثانوي في الصندوق1';
$string['marketing1subheadingdesc'] = 'أدخل نص العنوان الثانوي في الصندوق1';
$string['marketing1url'] = 'رابط الصندوق1';
$string['marketing1urldesc'] = 'أدخل رابط الصندوق1';
$string['marketing2content'] = 'محتوى الصندوق2';
$string['marketing2contentdesc'] = 'ادخل نص محتوى الصندوق2';
$string['marketing2heading'] = 'مقدمة الصندوق2';
$string['marketing2headingdesc'] = 'ادخل نص مقدمة الصندوق2';
$string['marketing2icon'] = 'أيقونة الصندوق2';
$string['marketing2icondesc'] = 'تحميل أيقونة الصندوق2';
$string['marketing2subheading'] = 'العنوان الثانوي الصندوق2';
$string['marketing2subheadingdesc'] = 'ادخل نص العنوان الثانوي الصندوق2';
$string['marketing2url'] = 'رابط الصندوق2';
$string['marketing2urldesc'] = 'ادخل رابط الصندوق2';
$string['marketing3content'] = 'محتوى الصندوق3';
$string['marketing3contentdesc'] = 'ادخل نص محتوى الصندوق3';
$string['marketing3heading'] = 'مقدمة الصندوق3';
$string['marketing3headingdesc'] = 'ادخل نص مقدمة الصندوق 3';
$string['marketing3icon'] = 'أيقونة الصندوق3';
$string['marketing3icondesc'] = 'حمل أيقونة الصندوق3';
$string['marketing3subheading'] = 'العنوان الثانوي الصندوق3';
$string['marketing3subheadingdesc'] = 'ادخل نص العنوان الثانوي الصندوق3';
$string['marketing3url'] = 'رابط الصندوق3';
$string['marketing3urldesc'] = 'ادخل رابط الصندوق3';
$string['marketing4content'] = 'محتوى الصندوق4';
$string['marketing4contentdesc'] = 'ادخل محتوى الصندوق4';
$string['marketing4heading'] = 'مقدمة الصندوق4';
$string['marketing4headingdesc'] = 'ادخل نص مقدمة الصندوق4';
$string['marketing4icon'] = 'أيقونة الصندوق4';
$string['marketing4icondesc'] = 'حمّل أيقونة الصندوق4';
$string['marketing4subheading'] = 'العنوان الثانوي الصندوق4';
$string['marketing4subheadingdesc'] = 'ادخل العنوان الثانوي الصندوق4';
$string['marketing4url'] = 'رابط الصندوق4';
$string['marketing4urldesc'] = 'ادخل رابط الصندوق4';
$string['mobile'] = 'جوال';
$string['mobiledesc'] = 'ادخل رقم الجوال';
$string['navbarbg'] = 'لون نشريط التنقل';
$string['navbarbg_desc'] = 'لون شريط التنقل الأيسر';
$string['navbarbghover'] = 'لون المرور على شريط التنقل';
$string['navbarbghover_desc'] = 'لون المرور على شريط التنقل الأيسر';
$string['navbarheadercolor'] = 'لون مقدمة شريط التنقل';
$string['navbarheadercolor_desc'] = 'لون مقدمة شريط التنقل العلوية';
$string['next_section'] = 'القسم التالي';
$string['numbersfrontpage'] = 'عرض عداد المواقع على الصفحة الأولى';
$string['numbersfrontpagedesc'] = 'في حالة التمكين ، قم بعرض عدد المستخدمين النشطين و المواد و الأنشطة في الصفحة الأولى.';
$string['onlineusers'] = 'المستخدمون المتواجدون (آخر 5 دقائق)';
$string['platform_access'] = 'الدخول إلى المنصة';
$string['pluginname'] = 'Moove';
$string['preset'] = 'المظهر السابق';
$string['preset_desc'] = 'اختر إعدادًا مسبقًا لتغيير مظهر السمة على نطاق واسع.';
$string['presetfiles'] = 'ملفات إضافية للمظهر سابقا';
$string['prev_section'] = 'القسم السابق';
$string['privacy:metadata'] = 'مظهر Moove لا يخزن أي بيانات شخصية عن أي مستخدم.';
$string['rawscss'] = 'SCSS  الأصلية';
$string['rawscss_desc'] = 'استخدم هذا الحقل لتوفير كود SCSS أو CSS الذي سيتم حقنه في نهاية ورقة الأنماط.';
$string['rawscsspre'] = 'SCSS الأولية الأصلية';
$string['rawscsspre_desc'] = 'في هذا الحقل ، يمكنك توفير تهيئة كود SCSS ، سيتم حقنه قبل أي شيء آخر. معظم الوقت سوف تستخدم هذا الإعداد لتحديد المتغيرات.';
$string['readmore'] = 'إقراء أكثر';
$string['region-side-pre'] = 'يمين';
$string['removecontact'] = 'احذف الاتصالات';
$string['search_forums'] = 'ابحث في منتدى المقرر';
$string['search_site'] = 'ابحث في الموقع';
$string['showhideblocks'] = 'إظهار/إخفاء الكتل';
$string['slidercaption'] = 'التسمية التوضيحية لشريط التمرير';
$string['slidercaptiondesc'] = 'إضافة تعليق على الشريحة الخاصة بك';
$string['slidercount'] = 'عداد الشرائح';
$string['slidercountdesc'] = 'حدد عدد الشرائح التي تريد إضافتها ثم انقر فوق حفظ لتحميل حقول الإدخال.';
$string['sliderenabled'] = 'تمكين الشرائح';
$string['sliderenableddesc'] = 'تمكين الشرائح في مقدمة الصفحة الرئسيسة';
$string['sliderfrontpage'] = 'عرض الشرائح في الصفحة الأولى';
$string['sliderfrontpagedesc'] = 'في حالة التمكين، سيتم عرض عرض الشرائح في الصفحة الأولى لتحل محل صورة المقدمة.';
$string['sliderimage'] = 'صورة شريط التمرير';
$string['sliderimagedesc'] = 'إضافة صورة لشريحة الخاص بك. الحجم الموصى به هو 1500 بكسل × 540 بكسل أو أعلى.';
$string['slidertitle'] = 'عنوان الشريحة';
$string['slidertitledesc'] = 'أضف عنوان الشريحة.';
$string['sponsorscount'] = 'عدد الرعاة';
$string['sponsorscountdesc'] = 'حدد عدد شعارات الجهة الراعية التي تريد إضافتها ، ثم انقر فوق "حفظ" لتحميل حقول الإدخال.';
$string['sponsorsfrontpage'] = 'تمكين صف الرعاة';
$string['sponsorsfrontpagedesc'] = 'في حالة التمكين ، سيعرض صفًا به شعارات الجهات الراعية بروابطها.';
$string['sponsorsimage'] = 'شعارات الرعاة';
$string['sponsorsimagedesc'] = 'اضف شعار راعي.';
$string['sponsorssubtitle'] = 'العنوان الفرعي للرعاة';
$string['sponsorssubtitledefault'] = 'الشركات والمؤسسات الراعية التي تثق بنا';
$string['sponsorssubtitledesc'] = 'العنوان الفرعي لصف الرعاة.';
$string['sponsorstitle'] = 'عنوان الرعاة';
$string['sponsorstitledefault'] = 'الرعاة';
$string['sponsorstitledesc'] = 'رأس صف الرعاة.';
$string['sponsorsurldesc'] = 'ادخل رابط موقع الراعي';
$string['startedon'] = 'ابتداءً من';
$string['stayintouch'] = 'ابق على تواصل';
$string['topfooterimg'] = 'صورة الحاشية';
$string['topfooterimgdesc'] = 'حمّل صورة الحاشيةالخاصة بك هنا إذا كنت تريد استبدال الصورة الافتراضية. الحجم الموصى به هو 1500 بكسل × 400 بكسل أو أعلى.';
$string['totalcourses'] = 'عدد المقررات';
$string['totalusers'] = 'المستخدِمون (نشط / معلق)';
$string['twitter'] = 'رابط تويتر';
$string['twitterdesc'] = 'أدخل رابط تويتر الخاص بك. (مثل http://www.twitter.com/moodlehq)';
$string['userprofile'] = 'ملف التعريف بالمستخدم';
$string['website'] = 'رابط الموقع';
$string['websitedesc'] = 'الموقع الرئيسي للمؤسسة';
$string['youtube'] = 'رابط اليوتيوب';
$string['youtubedesc'] = 'أدخل رابط Youtube الخاص بكـ . (مثل https://www.youtube.com/user/moodlehq)';
