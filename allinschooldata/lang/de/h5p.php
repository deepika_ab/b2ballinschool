<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'h5p', language 'de', branch 'MOODLE_38_STABLE'
 *
 * @package   h5p
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addedandupdatedpp'] = '{$a->%new} neue H5P-Bibliotheken hinzugefügt und {$a->%old} alte aktualisiert.';
$string['addedandupdatedps'] = '{$a->%new} neue H5P-Bibliotheken hinzugefügt und {$a->%old} alte aktualisiert.';
$string['addedandupdatedsp'] = '{$a->%new} neue H5P-Bibliothek hinzugefügt und {$a->%old} alte aktualisiert.';
$string['addedandupdatedss'] = '{$a->%new} neue H5P-Bibliothek hinzugefügt und {$a->%old} alte aktualisiert.';
$string['addednewlibraries'] = '{$a->%new} neue H5P-Bibliotheken hinzugefügt';
$string['addednewlibrary'] = '{$a->%new} neue H5P-Bibliothek hinzugefügt';
$string['additionallicenseinfo'] = 'Zusätzliche Infos zur Lizenz';
$string['author'] = 'Autor/in';
$string['authorcomments'] = 'Kommentare';
$string['authorcommentsdescription'] = 'Kommentare an Herausgeber/in des Inhalts. Dieser Text wird nicht als Teil der Copyright-Infos veröffentlicht.';
$string['authorname'] = 'Autorenname';
$string['authorrole'] = 'Autorenrolle';
$string['by'] = 'von';
$string['cancellabel'] = 'Abbrechen';
$string['ccattribution'] = 'Namensnennung (CC BY)';
$string['ccattributionnc'] = 'Namensnennung - nicht kommerziell (CC BY-NC)';
$string['ccattributionncnd'] = 'Namensnennung - nicht kommerziell - keine Bearbeitung (CC BY-NC-ND)';
$string['ccattributionncsa'] = 'Namensnennung - nicht kommerziell - Weitergabe unter gleichen Bedingungen (CC BY-NC-SA)';
$string['ccattributionnd'] = 'Namensnennung - keine Bearbeitung (CC BY-ND)';
$string['ccattributionsa'] = 'Namensnennung - Weitergabe unter gleichen Bedingungen (CC BY-SA)';
$string['ccpdd'] = 'Public Domain (CC0)';
$string['changedby'] = 'Geändert von';
$string['changedescription'] = 'Änderungsbeschreibung';
$string['changelog'] = 'Änderungsverlauf';
$string['changeplaceholder'] = 'Foto beschnitten, Text geändert, usw.';
$string['close'] = 'Schließen';
$string['confirmdialogbody'] = 'Möchten Sie wirklich fortfahren? Die Aktion kann nicht zurückgenommen werden.';
$string['confirmdialogheader'] = 'Aktion bestätigen';
$string['confirmlabel'] = 'Bestätigen';
$string['connectionLost'] = 'Verbindung unterbrochen. Die Ergebnisse werden gespeichert und gesendet, wenn die Verbindung wiederhergestellt ist.';
$string['connectionReestablished'] = 'Verbindung wiederhergestellt';
$string['contentchanged'] = 'Dieser Inhalt hat sich seit Ihrer letzten Verwendung geändert.';
$string['contentCopied'] = 'Der Inhalt wurde in die Zwischenablage kopiert';
$string['contenttype'] = 'Inhaltstyp';
$string['copyright'] = 'Nutzungsrechte';
$string['copyrightinfo'] = 'Copyright-Info';
$string['copyrightstring'] = 'Copyright';
$string['copyrighttitle'] = 'Copyright-Info für diesen Inhalt anzeigen';
$string['couldNotParseJSONFromZip'] = 'JSON kann nicht aus dem Paket analysiert werden: {$a->%fileName}';
$string['couldNotReadFileFromZip'] = 'Datei aus dem Paket kann nicht gelesen werden: {$a->%fileName}';
$string['creativecommons'] = 'Creative Commons';
$string['date'] = 'Datum';
$string['disablefullscreen'] = 'Vollbild deaktivieren';
$string['download'] = 'Herunterladen';
$string['downloadtitle'] = 'Diesen Inhalt als H5P-Datei herunterladen';
$string['editor'] = 'Editor';
$string['embed'] = 'Einbetten';
$string['embedtitle'] = 'Einbettcode für diesen Inhalt anzeigen';
$string['eventh5pdeleted'] = 'H5P gelöscht';
$string['eventh5pviewed'] = 'H5P-Inhalt angezeigt';
$string['fetchtypesfailure'] = 'Fehler bei der Verbindung zum H5P-Repository. Es können keine Infos über die verfügbaren Inhaltstypen von H5P abgerufen werden.';
$string['fileExceedsMaxSize'] = 'Eine der Dateien im Paket überschreitet die maximal zulässige Dateigröße. ({$a->%file} {$a ->%used} > {$a->%max})';
$string['fullscreen'] = 'Vollbild';
$string['gpl'] = 'General Public License v3';
$string['h5p'] = 'H5P';
$string['h5pfilenotfound'] = 'H5P-Datei nicht gefunden';
$string['h5pinvalidurl'] = 'Ungültige URL zum H5P-Inhalt';
$string['h5pmanage'] = 'H5P-Inhaltstypen verwalten';
$string['h5ppackage'] = 'H5P-Inhaltstyp';
$string['h5ppackage_help'] = 'Ein H5P-Inhaltstyp ist eine Datei mit einer H5P- oder ZIP-Erweiterung, die alle Bibliotheken enthält, die zur Anzeige des Inhalts notwendig sind.';
$string['h5pprivatefile'] = 'Dieser H5P-Inhalt kann nicht angezeigt werden, da Sie keinen Zugriff auf die .h5p-Datei haben.';
$string['h5ptitle'] = 'Besuchen Sie die Website H5P.org, um weitere Infos zu erhalten.';
$string['hideadvanced'] = 'Erweitert ausblenden';
$string['installedcontentlibraries'] = 'Installierte H5P-Bibliotheken';
$string['installedcontenttypes'] = 'Installierte H5P-Inhaltstypen';
$string['installedh5p'] = 'Installiertes H5P';
$string['invalidcontextid'] = 'H5P-Datei nicht gefunden (ungültige Kontext-ID)';
$string['invalidfile'] = 'Die Datei "{$a->%filename}" ist nicht erlaubt. Es sind nur Dateien mit den folgenden Extensions zulässig: {$a->%files-allowed}.';
$string['invalidlanguagefile'] = 'Ungültige Sprachdatei {$a->%file} in der Bibliothek {$a->%library}';
$string['invalidlanguagefile2'] = 'Ungültige Sprachdatei {$a->%file} wurde in die Bibloithek {$a->%name} aufgenommen.';
$string['invalidlibrarydata'] = 'Ungültige Daten für {$a->%property} in {$a->%library} bereitgestellt';
$string['invalidlibrarydataboolean'] = 'Ungültige Daten für {$a->%property} in {$a->%library} bereitgestellt. Wahrheitswert erwartet.';
$string['invalidlibraryname'] = 'Ungültiger Bibliotheksname: {$a->%name}';
$string['invalidlibrarynamed'] = 'Die im Inhalt verwendete H5P-Bibliothek {$a->%library} ist ungültig.';
$string['invalidlibraryoption'] = 'Ungültige Option {$a->%option} in {$a->%library}';
$string['invalidlibraryproperty'] = 'Die Eigenschaft {$a->%property} in {$a->%library} kann nicht gelesen werden.';
$string['invalidmainjson'] = 'Eine gültige Hauptdatei h5p.json fehlt.';
$string['invalidmultiselectoption'] = 'Ungültig ausgewählte Option bei Mehrfachauswahl';
$string['invalidpackage'] = 'Ungültiger H5P-Inhaltstyp';
$string['invalidselectoption'] = 'Ungültig ausgewählte Option bei Auswahl';
$string['invalidsemanticsjson'] = 'Ungültige Datei semantics.json wurde in die Bibliothek {$a->%name} aufgenommen.';
$string['invalidsemanticstype'] = 'Interner H5P-Fehler: unbekannter Inhaltstyp "{$a->@type}" in der Semantik. Inhalte werden entfernen!';
$string['invalidstring'] = 'Die angegebene Zeichenfolge ist bezüglich regulärer Ausdrücke in der Semantik nicht gültig. (Wert: "{$a->%value}", regexp: "{$a->%regexp}")';
$string['librariesmanagerdescription'] = '<p>Mit H5P können interaktive Inhalte erstellt werden.</p><p>Um sicherzustellen, dass auf Ihrer Website nur vertrauenswürdige H5P-Inhaltstypen verwendet werden, müssen Sie <i> entweder </i></p><ul><li>H5P-Inhaltstypen von H5P.org hochladen <i> oder </i></li><li>den geplanten Vorgang \'Verfügbare H5P-Inhaltstypen von H5P.org herunterladen\' aktivieren</li ></ul><p>Beachten Sie, dass Nutzer/innen nur die auf Ihrer Website installierten H5P-Inhaltstypen verwenden können.</p>';
$string['librarydirectoryerror'] = 'Der Name des Bibliotheksverzeichnisses muss mit machineName oder machineName-majorVersion.minorVersion (aus library.json) übereinstimmen. (Verzeichnis: {$a->%directoryName}, machineName: {$a->%machineName}, majorVersion: {$a->%majorVersion}, minorVersion: {$a->%minorVersion})';
$string['license'] = 'Lizenz';
$string['licenseCC010'] = 'CC0 1.0 Universal (CC0 1.0) Public Domain';
$string['licenseCC010U'] = 'CC0 1.0 Universal';
$string['licenseCC10'] = '1.0 Generic';
$string['licenseCC20'] = '2.0 Generic';
$string['licenseCC25'] = '2.5 Generic';
$string['licenseCC30'] = '3.0 Unported';
$string['licenseCC40'] = '4.0 International';
$string['licensee'] = 'Lizenznehmer/in';
$string['licenseextras'] = 'Lizenzextras';
$string['licenseGPL'] = 'General Public License';
$string['licenseV1'] = 'Version 1';
$string['licenseV2'] = 'Version 2';
$string['licenseV3'] = 'Version 3';
$string['licenseversion'] = 'Lizenzversion';
$string['lockh5pdeploy'] = 'Auf diesen H5P-Inhalt kann nicht zugegriffen werden, bis er fertig bereitgestellt ist. Bitte versuchen Sie es später noch einmal.';
$string['missingcontentfolder'] = 'Ein gültiger Inhaltsordner fehlt.';
$string['missingcoreversion'] = 'Das System konnte die Komponente {$a->%component} aus dem Paket nicht installieren. Es ist eine neuere Version des H5P-Plugins erforderlich. Auf dieser Website ist derzeit die Version {$a->%current} installiert, aber es ist mindestens {$a->%required} erforderlich. Versuchen sie es nach einer Aktualisierung erneut.';
$string['missingdependency'] = 'Fehlende Abhängigkeit {$a->@dep} erforderlich für {$a->@lib}.';
$string['missinglibrary'] = 'Fehlende erforderliche Bibliothek {$a->@library}';
$string['missinglibraryfile'] = 'Die Datei "{$a->%file}" fehlt in der Bibliothek: "{$a->%name}"';
$string['missinglibraryjson'] = 'Für die Bibliothek {$a->%name} konnte die Datei library.json nicht mit dem gültigen json-Format gefunden werden.';
$string['missinglibraryproperty'] = 'Die erforderliche Eigenschaft {$a->%property} fehlt in {$a ->%library}';
$string['missingmbstring'] = 'Die PHP-Extension mbstring ist nicht geladen. H5P benötigt diese Extension, um richtig zu funktionieren.';
$string['missinguploadpermissions'] = 'Beachten Sie, dass in der von Ihnen hochgeladenen Datei möglicherweise Bibliotheken enthalten sind, aber dass Sie keine neuen Bibliotheken hochladen dürfen. Wenden Sie sich hierzu an Administrator/innen der Website.';
$string['nocopyright'] = 'Für diesen Inhalt sind keine Copyright-Infos verfügbar.';
$string['noextension'] = 'Die von Ihnen hochgeladene Datei ist kein gültiges HTML5-Paket. Sie hat nicht die Extension .h5p.';
$string['nojson'] = 'Die Hauptdatei h5p.json ist nicht gültig.';
$string['nopermissiontodeploy'] = 'Diese Datei kann nicht angezeigt werden, da sie von einer Person hochgeladen wurde, die nicht über die erforderlichen Rechte zum Bereitstellen von H5P-Inhalten verfügt.';
$string['notrustablefile'] = 'Dieser Inhalt kann nicht angezeigt werden, da sie von einem/r Nutzer/in hochgeladen wurde, der/die nicht berechtigt ist, die Inhaltstypen von H5P zu aktualisieren.  Bitte wenden Sie sich an Ihre/n Administrator/in, um nach dem zu installierenden Inhaltstyp zu fragen.';
$string['nounzip'] = 'Die von Ihnen hochgeladene Datei ist kein gültiges HTML5-Paket. Das Entpacken ist nicht möglich.';
$string['offlineDialogBody'] = 'Infos zum Abschluss dieser Aufgabe konnten nicht gesendet werden. Überprüfe Sie die Internetverbindung.';
$string['offlineDialogHeader'] = 'Ihre Verbindung zum Server wurde unterbrochen.';
$string['offlineDialogRetryButtonLabel'] = 'Sofort wiederholen';
$string['offlineDialogRetryMessage'] = 'Wiederholung in :num ...';
$string['offlineSuccessfulSubmit'] = 'Ergebnisse erfolgreich übertragen';
$string['originator'] = 'Urheber/in';
$string['pd'] = 'Public Domain';
$string['pddl'] = 'Public Domain Widmung und Lizenz';
$string['pdm'] = 'Public Domain Mark (PDM)';
$string['pluginname'] = 'H5P-Paket';
$string['privacy:metadata'] = 'Das H5P-Subsystem speichert keine personenbezogenen Daten.';
$string['resizescript'] = 'Fügen Sie dieses Skript in Ihre Website ein, wenn Sie eine dynamische Größenanpassung des eingebetteten Inhalts wünschen:';
$string['resubmitScores'] = 'Versuch, gespeicherte Ergebnisse zu übermitteln.';
$string['reuse'] = 'Wiederverwenden';
$string['reuseContent'] = 'Inhalt wiederverwenden';
$string['reuseDescription'] = 'Diesen Inhalt wiederverwenden';
$string['showadvanced'] = 'Erweitert anzeigen';
$string['showless'] = 'Weniger anzeigen';
$string['showmore'] = 'Mehr anzeigen';
$string['size'] = 'Größe';
$string['source'] = 'Quelle';
$string['startingover'] = 'Sie werden von vorne anfangen.';
$string['sublevel'] = 'Sublevel';
$string['thumbnail'] = 'Vorschaubild';
$string['title'] = 'Titel';
$string['undisclosed'] = 'Geheim gehalten';
$string['unpackedFilesExceedsMaxSize'] = 'Die Gesamtgröße der entpackten Dateien überschreitet die maximal zulässige Größe. ({$a->%used}> {$a->%max})';
$string['updatedlibraries'] = '{$a->%old} H5P-Bibliotheken aktualisiert.';
$string['updatedlibrary'] = '{$a->%old} H5P-Bibliothek aktualisiert.';
$string['uploadlibraries'] = 'Hochladen von H5P-Inhaltstypen';
$string['uploadsuccess'] = 'H5P-Inhaltstypen erfolgreich hochgeladen';
$string['wrongversion'] = 'Die in diesem Inhalt verwendete Version der H5P-Bibliothek {$a->%machineName} ist ungültig. Der Inhalt enthält {$a->%contentLibrary}, sollte jedoch {$a->%semanticsLibrary} sein.';
$string['year'] = 'Jahr';
$string['years'] = 'Jahre';
$string['yearsfrom'] = 'Jahre (von)';
$string['yearsto'] = 'Jahre (bis)';
