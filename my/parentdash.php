<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');

redirect_if_major_upgrade_required();

require_login();

global $CFG, $USER, $DB;

$kuserid = optional_param('kuserid', null, PARAM_INT);
$strmymoodle = get_string('myhome');

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/parentdash.php', $params);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_pagetype('parentdash');
$PAGE->blocks->add_region('content');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/myclass_remove_parent.css'));

echo $OUTPUT->header();
$roleid = $DB->get_record('role', array('shortname' => 'parent'))->id;
if(!user_has_role_assignment($USER->id, $roleid)){
    throw new moodle_exception('Please login as parent account');
}
$allusernames = get_all_user_name_fields(true, 'u');
$usercontexts = $DB->get_records_sql("SELECT c.instanceid, c.instanceid, $allusernames
                                                    FROM {role_assignments} ra, {context} c, {user} u
                                                   WHERE ra.userid = ?
                                                         AND ra.contextid = c.id
                                                         AND c.instanceid = u.id
                                                         AND c.contextlevel = " . CONTEXT_USER, array($USER->id));
//$choices[''] = 'Select kid';
foreach ($usercontexts as $usercontext) {
    $choices[$usercontext->instanceid] = fullname($usercontext);
}
if(!isset($kuserid)){
    echo 'Select kid : '. $OUTPUT->single_select($url, 'kuserid', $choices);

} else {
    $kiduserobj = $DB->get_record('user', array('id' => $kuserid));
    $url = $CFG->wwwroot . '/my/parentdash.php';

    echo 'Select kid : ' . $OUTPUT->single_select($url, 'kuserid', $choices, $kuserid);
    
    echo html_writer::tag('h2', 'Dashboard for ' . fullname($kiduserobj));

    
    echo html_writer::start_div('row bg-white');
    echo html_writer::start_div('row col-md-10 bg-white');
        echo html_writer::link($CFG->wwwroot.'/user/profile.php?id='.$kuserid, html_writer::img($CFG->wwwroot . '/theme/edumy/pix/img5.png', 'Grade') . html_writer::div('Profile'), array('class'=> 'col-md-4 text-center'));
        echo html_writer::link($CFG->wwwroot.'/grade/report/overview/index.php?userid='.$kuserid.'&id=1', html_writer::img($CFG->wwwroot . '/theme/edumy/pix/img1.png', 'Grade') . html_writer::div('Grade'), array('class'=> 'col-md-4 text-center'));
        echo html_writer::link('javascript:;', html_writer::img($CFG->wwwroot . '/theme/edumy/pix/img4.png', 'Grade') . html_writer::div('Class Recording'), array('class'=> 'col-md-4 text-center'));
    echo html_writer::end_div();
    
    
    echo html_writer::start_div('row col-md-10 bg-white');
    echo html_writer::tag('h2', 'Courses & teachers list');
    $courses = enrol_get_users_courses($kuserid);
    $roleid = $DB->get_record('role', array('shortname' => 'editingteacher'))->id;
    $data = '';
    $table = new html_table();
    $table->head = array('Sl no', 'Class Name', 'Teacher name');
    $table->id = 'teacherlist';
    $i = 1;
    foreach($courses as $course){
        $userroles = enrol_get_course_users_roles($course->id);
        foreach($userroles as  $key => $userrole){
            $dataobj = $userrole[$roleid];
            if($dataobj->roleid == $roleid){
                $teacher = $DB->get_record('user', array('id' => $userrole[$roleid]->userid));
                $table->data[] = array(
                $i,
                html_writer::link($CFG->wwwroot.'/course/view.php?id='.$course->id,$course->fullname),
                html_writer::link($CFG->wwwroot.'/user/profile.php?id='.$teacher->id,fullname($teacher))
            );
                $i++;
            }
        }
        
    }
    //print_object($dd);
    echo html_writer::table($table);;
    echo html_writer::end_div();
    
    echo html_writer::start_div('row col-md-10 bg-white');
    
    echo html_writer::end_div();
        
    echo html_writer::start_div('row col-md-10 bg-white');
    echo html_writer::tag('h2', 'Assignment submission details');
    $courses = enrol_get_users_courses($kuserid);
    $roleid = $DB->get_record('role', array('shortname' => 'editingteacher'))->id;
    $data = '';
    $table = new html_table();
    $table->head = array('Sl no', 'Class Name', 'Assignment Name', 'Due Date', 'Status');
    $table->id = 'assignmentlist';
    $i = 1;
    
    
    foreach($courses as $course){
        $assignments = $DB->get_records('assign', array('course' => $course->id));
        foreach($assignments as $assignment){
            if($assignment->duedate > time()){
                $duedate = html_writer::tag('font', userdate($assignment->duedate), array('class' => 'text-success'));
            } else {
                $duedate = html_writer::tag('font', userdate($assignment->duedate), array('class' => 'text-danger'));
            }
            $assign_status = $DB->get_record('assign_submission', array('assignment' => $assignment->id, 'userid' => $kuserid));
            if($assign_status){
                $assignment_status = 'Submitted';
            } else {
                $assignment_status = 'Not yet submitted';
            }
                $table->data[] = array(
                $i,
                html_writer::link($CFG->wwwroot.'/course/view.php?id='.$course->id,$course->fullname),
                $assignment->name,
                $duedate,
                $assignment_status
            );
                $i++;
            
        }
        
    }
    //print_object($dd);
    echo html_writer::table($table);;
    echo html_writer::end_div();
    
    
    echo html_writer::end_div();
}

echo $OUTPUT->footer();
?>
<script>
$(document).ready(function () {
    $("select").each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
})
</script>

