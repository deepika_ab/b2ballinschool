<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');

redirect_if_major_upgrade_required();

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/solution.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_pagetype('Features');
$PAGE->blocks->add_region('content');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/css/style.css'));
$PAGE->requires->js(new moodle_url('js/jquery.min.js'));
$PAGE->requires->js(new moodle_url('js/custom.js'));

echo $OUTPUT->header();

 $html = '<div class="wrapper">
       
        <div class="main">

            <div class="item about solution_wrap">
                <h1>{mlang EN} Solution for Schools {mlang}{mlang FA}راه حلی برای مدارس {mlang}{mlang AR} بدائل للمدارس {mlang}{mlang FR}Solution pour les Écoles{mlang}{mlang ES} Solución para escuelas {mlang}{mlang DE}Lösung für Schulen{mlang}  </h1>
                <div class="right_sec">
                    <img src="images/4.png" />
                </div>
                <div class="left_sec">
                  
              
                    <h4>{mlang EN} All In School e-Learning Platform Key Benefits for Schools: {mlang} {mlang FA}فوائد کلیدی پلتفرم آموزش الکترونیکی برای مدارس در All In School : {mlang} {mlang AR}المزايا الرئيسية لمنصة التعلم الإلكتروني All In School: {mlang} {mlang FR}Principaux avantages de la plateforme e-Learning All In School :{mlang} {mlang ES}Beneficios principales de la Plataforma de e-Learning All In School para las escuelas: {mlang}{mlang DE}All In School e-Learning-Plattform - Hauptvorteile für Schulen:{mlang}</h4>
                    <hr class="purple">
                    <ul class="features">
                        <li> {mlang EN} Interactive teacher - student virtual classroom experience  {mlang}{mlang FA}معلم فعال - کار در کلاسهای مجازی  {mlang}{mlang AR} المعلم المتفاعل - تجربة الفصل الافتراضي للطالب {mlang}{mlang FR} Une expérience interactive entre l\'enseignant et l\'élève en classe virtuelle {mlang}{mlang ES}  Profesor interactivo - aula virtual para el estudiante{mlang}{mlang DE} Interaktiver Lehrer - virtuelles Klassenzimmer für Schüler{mlang}</li>

                        <li>{mlang EN}  User friendly app supporting cross platforms {mlang}{mlang FA} پلتفرمهای کراس که قابلیت استفاده از اپلیکیشنها را دارا هستند.  {mlang}{mlang AR} تطبيق سهل الاستخدام يدعم منصات متعددة {mlang}{mlang FR} Application intuitive compatible cross-platform {mlang}{mlang ES} App fácil de usar disponible para varias plataformas  {mlang}{mlang DE}Benutzerfreundliche Anwendung, die Cross-Plattformen unterstützt{mlang}</li>

                        <li> {mlang EN}  Cost & time savings {mlang}{mlang FA} قابلیت صرفه جویی در هزینه و زمان {mlang}{mlang AR}•توفير التكلفة والوقت {mlang}{mlang FR} Gain de temps et d\'argent {mlang}{mlang ES} Ahorro de tiempo y dinero  {mlang}{mlang DE}Kosten- und Zeiteinsparungen{mlang}</li>

                      
                        <li> {mlang EN} Parents have more access and supervision on the child’s learning  {mlang}{mlang FA} والدین میتوانند مدیریت بیشتری به یادگیری کودکانشان داشته باشند.  {mlang}{mlang AR} يتمتع الوالدان بمزيد من التَحكُّم والإشراف على تَعلُّم الطفل {mlang}{mlang FR} Les parents ont un meilleur accès et une meilleur supervision sur l\'expérience d\'apprentissage de l\'enfant {mlang}{mlang ES}  Los padres tienen mayor acceso y supervisión de la evolución en el aprendizaje del niño {mlang} {mlang DE}Eltern haben mehr Zugang und Aufsicht über das Lernen des Kindes{mlang}</li>

                        <li> {mlang EN} Free platform training for teachers, parents and students  {mlang}{mlang FA} پلتفرم  رایگان آموزشی برای معلمین،  والدین و شاگردان  {mlang}{mlang AR}  منصة تدريب مجانية للمعلمين وأولياء الأمور والطلاب {mlang}{mlang FR}Formation gratuite sur la plateforme pour les enseignants, parents et etudiants {mlang}{mlang ES}  Formación gratuita en el uso de la plataforma para profesores, padres y alumnos {mlang}{mlang DE}Kostenlose Plattform-Schulungen für Lehrer, Eltern und Schüler{mlang}</li>

                        <li> {mlang EN} Customer technical support available  {mlang}{mlang FA} حمایت و پشتیبانی تکنیکال از مصرف کننده {mlang}{mlang AR} توافر الدعم الفني للعملاء {mlang}{mlang FR} Assistance technique disponible pour la clientèle  {mlang}{mlang ES}  Disponible asistencia técnica para el cliente {mlang}{mlang DE}Technischer Kundensupport{mlang}</li>

                        <li> {mlang EN} Close collaborative approach - All In School is a partner solution  {mlang}{mlang FA} داشتن همکاری نزدیک - All in School محلی است برای تعامل همکاری. {mlang}{mlang AR} نهج تعاوني وثيق - All In School  هو حل مُشارِك {mlang}{mlang FR}Une approche collaborative-All in School est un partenaire {mlang}{mlang ES}  Orientada a la colaboración estrecha: All In School es una solución asociada {mlang}{mlang DE}Enger kooperativer Ansatz - All In School ist eine Partnerlösung{mlang}</li>

           
                    </ul>
              
                </div>
            </div>
                <div class="item about  grey">
                <div class="left_sec">
                  
              
                    <h4>{mlang EN}About the All In School e-Learning Software:  {mlang}{mlang FA}در مورد نرم افزار آموزش الکترونیکی All in School  {mlang}{mlang AR}حول برنامج التَعلُّم الإلكتروني All In School :{mlang}{mlang FR} À propos du logiciel e-Learning en ligne de All In School :{mlang}{mlang ES} Sobre el software de e-Learning All In School: {mlang}{mlang DE}Über die All In School e-Learning Software:{mlang}</h4>
                    <hr class="purple">
                    <ul class="features">
                        <li>{mlang EN} Live sessions anywhere anytime: Virtual classrooms with live video interaction between the teachers and students using virtual whiteboard, screen sharing, files sharing, video streaming and text chats. {mlang}{mlang FA}تشکیل جلسات زنده در هر مکان و هر زمان: کلاس های مجازی با قابلیت استفاده از ابزار ویدئویی زنده بین معلمان و دانش آموزان، استفاده از تختۀ وایتبورد مجازی، اشتراک گذاری صفحه ، اشتراک فایل ها، پخش ویدیو و چتهای نوشتاری.{mlang}{mlang AR}محاضرات مباشرة في أي مكان وفي أي وقت: فصول دراسية افتراضية مع تفاعل مباشر بالفيديو بين المعلمين والطلاب باستخدام السبورة الافتراضية ومشاركة الشاشة ومشاركة الملفات بث الفيديو والمحادثات النصية. {mlang}{mlang FR} Des sessions en direct n\'importe où et n\'importe quand : Salles de classe virtuelles avec interaction vidéo en direct entre les enseignants et les élèves grâce à un tableau blanc virtuel, avec partage d\'écran,  partage de fichiers, diffusion vidéos en continu et systeme de texto {mlang}{mlang ES} Sesiones en directo en cualquier momento y lugar: Clases virtuales interactivas con vídeo en directo entre los profesores y los alumnos usando una pizarra virtual, compartición de pantalla, compartición de archivos, emisión de vídeo y chats de texto. {mlang}{mlang DE}Live-Sitzungen überall und jederzeit: Virtuelle Klassenzimmer mit Live-Video-Interaktion zwischen Lehrern und Schülern unter Verwendung von virtueller Tafel, gemeinsamer Bildschirmnutzung, gemeinsamer Nutzung von Dateien, Video-Streaming und Text-Chats.{mlang}</li>

                        <li>{mlang EN} Integrated library: Allows teachers and school admins to upload files in private or in library for the students to access.   {mlang}{mlang FA} کتابخانۀ یکپارچه: این کتابخانه به معلمان و مدیران مدارس اجازه می دهد تا  فایلها را به صورت خصوصی یا از طریق کتابخانه برای دسترسی دانش آموزان بارگذاری کنند. {mlang}{mlang AR} مكتبة متكاملة: تسمح للمدرسين ومسؤولي المدارس بتحميل الملفات في مكان خاص أو في المكتبة ليتمكن الطلاب من الوصول إليها. {mlang}{mlang FR}Bibliothèque intégrée : Permet aux enseignants et aux administrateurs de l\'école de télécharger des fichiers en privé ou en bibliothèque pour que les élèves puissent y accéder.   {mlang}{mlang ES} Biblioteca integrada: Permite a los profesores y administradores de la escuela subir archivos en privado o en una biblioteca para que los alumnos accedan a ellos.  {mlang}{mlang DE}Integrierte Bibliothek: Ermöglicht es Lehrern und Schuladministratoren, Dateien privat oder in der Bibliothek hochzuladen, auf welche die Schüler dann zugreifen können.{mlang}</li>

                        <li> {mlang EN} Create unlimited courses: Teachers and school admins can create unlimited courses and can assign them to the students.    {mlang}{mlang FA} ایجاد دوره های نامحدود: معلمان و مدیران مدارس خواهند توانست  دوره های نامحدود ایجاد کرده و آنها را در اختیار دانش آموزان قرار دهند. {mlang}{mlang AR} إنشاء دورات غير محدودة: يمكن للمدرسين ومسؤولي المدارس إنشاء دورات غير محدودة ويمكنهم تخصيصها للطلاب. {mlang}{mlang FR} Crée des cours illimités : Les enseignants et les administrateurs de llécole peuvent créer des cours illimités et les attribuer aux élèves.   {mlang}{mlang ES} Creación de cursos ilimitados: Los profesores y administradores de la escuela pueden crear un número ilimitado de cursos y asignarlos a los estudiantes.  {mlang}{mlang DE}Erstellen Sie so viele Kurse wie Sie wünschen: Lehrer und Schulverwalter können unbegrenzt viele Kurse erstellen und sie den Schülern zuweisen. {mlang}</li>

                        <li> {mlang EN} Create online assessments: Teachers can create assessments in minutes and conduct tests. Gives students access to unlimited tests to analyze their performance.    {mlang}{mlang FA}  ارزیابی بصورت آنلاین : معلمان می توانند تنها در عرض چند دقیقه کار ارزیابی آزمون ها را انجام دهند و دانش آموزان  نیز خواهند توانست به آزمونهای نامحدود برای تجزیه و تحلیل عملکرد خود دسترسی پیدا کنند. {mlang}{mlang AR}إنشاء تقييمات عبر الإنترنت: يمكن للمدرسين إنشاء التقييمات في دقائق وإجراء الاختبارات. يمنح الطلاب اجتياز اختبارات غير محدودة لتحليل أدائهم. {mlang}{mlang FR} Crée des évaluations en ligne : Les enseignants peuvent créer des évaluations en quelques minutes et effectuer des tests. Donne aux élèves l\'accès à un nombre illimité de tests pour analyser leurs performances.   {mlang}{mlang ES} Creación de evaluaciones online: Los profesores pueden crear evaluaciones en unos minutos y llevar a cabo exámenes. Le proporciona a los estudiantes un acceso ilimitado a pruebas para analizar su rendimiento. {mlang}{mlang DE}Erstellen Sie Online-Bewertungen: Lehrkräfte können Beurteilungen in Minutenschnelle erstellen und Tests durchführen. Ermöglicht den Schülern den Zugang zu unbegrenzten Tests zur Analyse ihrer Leistungen.  {mlang}</li>

                        <li> {mlang EN} Interactive content builder: Allows teachers to create and share interactive content.  {mlang}{mlang FA} ایجاد محتوایی که همه بتوانند به آن دسترسی داشته باشند: در این قسمت امکاناتی در اختیار معلمان گذاشته میشود که بتوانند یک محتوای درسی همگانی ایجاد کرده و آنرا به اشتراک بگذارند. {mlang}{mlang AR}منشئ المحتوى التفاعلي: يسمح للمدرسين بإنشاء محتوى تفاعلي ومشاركته. {mlang}{mlang FR} Créateur de contenu interactif : Permet aux enseignants de créer et de partager du contenu interactif. {mlang}{mlang ES} Herraminta para crear contenido interactivo: Permite que los profesores creen y compartan contenido interactivo.  {mlang}{mlang DE}Builder für interaktive Inhalte: Ermöglicht Lehrkräften die Erstellung und gemeinsame Nutzung interaktiver Inhalte.{mlang}</li>

                        <li> {mlang EN} Assignment submission and assessments: Teachers can create assignments with a set timeline. Students submit the assignments online and they get automated badges based on their results. The opportunity of earning badges promotes motivation amongst the students. {mlang}{mlang FA} ارائه و ارزیابی تکالیف: معلمان می توانند برای دادن تکلیف یک جدول زمانی مشخص  ایجاد کنند. دانش آموزان کارهایشان را بصورت آنلاین ارسال کرده و بر اساس نتایجی که بدست می آورند نشان های خودکار دریافت می کنند. داشتن شانس کسب نشان باعث ایجاد انگیزه در دانشجویان می شود.{mlang}{mlang AR} إرسال الواجب والتقييمات: يمكن للمدرسين إنشاء واجبات باستخدام جدول زمني محدد. يرسل الطلاب الواجبات عبر الإنترنت ويحصلون على شارات آلية بناءً على نتائجهم. تعزز فرصة الحصول على الشارات التحفيز بين الطلاب.{mlang}{mlang FR} Soumission de devoirs et des évaluations : Les enseignants peuvent créer des devoirs avec une date butoire précise. Les élèves soumettent les devoirs en ligne et reçoivent des badges automatiques en fonction de leurs résultats. La possibilité de gagner des badges favorise la motivation des élèves. {mlang}{mlang ES}Envío de tareas y evaluaciones: Los profesores pueden crear tareas con un periodo de tiempo. Los estudiantes envían las tareas a través de internet y obtienen medallas automáticamente en función de sus resultados. La oportunidad de obtener medallas fomenta la motivación entre los estudiantes.  {mlang}{mlang DE}Einreichung von Aufgaben und Bewertungen: Lehrkräfte können Aufgaben mit einem festgelegten Zeitrahmen erstellen. Die Schüler reichen die Aufgaben online ein und erhalten auf der Grundlage ihrer Ergebnisse automatisch Abzeichen. Die Möglichkeit, Abzeichen zu verdienen, fördert die Motivation der Schüler.{mlang}</li>

                        <li> {mlang EN} Stakeholder segregations: Parents, teachers, students and school admin have different access levels and customized views {mlang}{mlang FA}تفکیک افراد ذینفع:  دسترسی و بازبینی والدین، معلمان، دانش آموزان و مدیر مدرسه  در سطوح متفاوتی انجام خواهد شد.  {mlang}{mlang AR}الفصل بين المستخدمين: لدى الآباء والمعلمين والطلاب ومسؤول المدرسة مستويات وصول مختلفة وطرق عرض مُخصصة لكل مُستخدم {mlang}{mlang FR} Segregation des parties prenantes : Les parents, les enseignants, les élèves et l\'administration de l\'école ont des niveaux d\'accès différents et des vues personnalisées {mlang}{mlang ES}Segregación de cada tipo de usuario: Padres, profesores, estudiantes y administradores de la escuela poseen diferentes niveles de acceso y vistas personalizadas.  {mlang}{mlang DE}Stakeholder-Trennung: Eltern, Lehrer, Studenten und Schulverwaltung haben verschiedene Zugriffsebenen und angepasste Ansichten der Plattform{mlang}</li>

                        <li> {mlang EN}Reports and analytics: Each stakeholder has access to relevant and customizable and automated reports that can be scheduled.  {mlang}{mlang FA} گزارش ها و تجزیه و تحلیل ها: هر کدام از افراد ذینفع میتوانند به گزارش های مربوط قابل تنظیم و خودکار  دسترسی داشته باشند. {mlang}{mlang AR} التقارير والتحليلات: يتمتع كل مُستخدم بإمكانية الوصول إلى التقارير الآلية ذات الصلة والقابلة للتعديل والتي يمكن جدولتها. {mlang}{mlang FR} Rapports et analyses : Chaque partie prenante a accès à des rapports pertinents, personnalisables et automatisés qui peuvent être programmés. {mlang}{mlang ES} Informes y analíticas: Cada grupo de usuarios posee acceso a informes relevantes y personalizables que se pueden programar.  {mlang}{mlang DE}Berichte und Analysen: Jeder Stakeholder hat Zugang zu relevanten, anpassbaren und automatisierten Berichten, die zeitlich geplant werden können.{mlang}</li>


           
                    </ul>
              
                </div>
                <div class="right_sec right_padding">
                    <img src="images/2.png" />
                </div>
            </div>
            
            
            <div class="solu_wrap">
				<div class="head">
					<h1>{mlang EN} Solution for Schools {mlang}{mlang FA}راه حلی برای مدارس {mlang}{mlang AR} بدائل للمدارس {mlang}{mlang FR}Solution pour les Écoles{mlang}{mlang ES} Solución para escuelas {mlang}{mlang DE}Lösung für Schulen{mlang}</h1>
					<p>{mlang EN}Live sessions anywhere anytime: Virtual classrooms with live video interaction between the teachers and students using virtual whiteboard, screen sharing, files sharing, video streaming and text chats.  {mlang}{mlang FA} تشکیل جلسات زنده در هر مکان و هر زمان: کلاس های مجازی با قابلیت استفاده از ابزار ویدئویی زنده بین معلمان و دانش آموزان، استفاده از تختۀ وایتبورد مجازی، اشتراک گذاری صفحه ، اشتراک فایل ها، پخش ویدیو و چتهای نوشتاری. {mlang}{mlang AR} محاضرات مباشرة في أي مكان وفي أي وقت: فصول دراسية افتراضية مع تفاعل مباشر بالفيديو بين المعلمين والطلاب باستخدام السبورة الافتراضية ومشاركة الشاشة ومشاركة الملفات بث الفيديو والمحادثات النصية.{mlang}{mlang FR} Des sessions en direct n\'importe où et n\'importe quand : Salles de classe virtuelles avec interaction vidéo en direct entre les enseignants et les élèves grâce à un tableau blanc virtuel, avec partage d\'écran,  partage de fichiers, diffusion vidéos en continu et systeme de texto {mlang}{mlang ES} Sesiones en directo en cualquier momento y lugar: Clases virtuales interactivas con vídeo en directo entre los profesores y los alumnos usando una pizarra virtual, compartición de pantalla, compartición de archivos, emisión de vídeo y chats de texto. {mlang}{mlang DE}Live-Sitzungen überall und jederzeit: Virtuelle Klassenzimmer mit Live-Video-Interaktion zwischen Lehrern und Schülern unter Verwendung von virtueller Tafel, gemeinsamer Bildschirmnutzung, gemeinsamer Nutzung von Dateien, Video-Streaming und Text-Chats.{mlang}</p>
				</div>
				<div class="image_wrap">
				</div>
				<div class="solu_item_wrap">
					<div class="item_30">
						<h5>{mlang EN}Follow a student  {mlang}{mlang FA} تعقیب فعالیتهای دانش آموز {mlang}{mlang AR} متابعة طالب*مستخدم للموقع  {mlang}{mlang FR} Suivre un élève {mlang}{mlang ES}Seguir a un alumno  {mlang}{mlang DE}Folgen Sie einem Schüler:{mlang}</h5>
						<p>{mlang EN}Students can choose other classmates to follow. A feed of activities would appear based on activities performed. E.g. Discussions, notes, assignment completion.  {mlang}{mlang FA} دانش آموزان قادر خواهند بود سایر همکلاسی های خود را  برای پیگیری انتخاب کنند. دسته ای از فعالیتها براساس فعالیت های انجام شدۀ قبلی همچون بحث ها، یادداشت ها  وتکمیل تکلیف ظاهر خواهد شد. {mlang}{mlang AR} يمكن للطلاب اختيار زملاء آخرين لمتابعتهم. سيظهر موجز للأنشطة بناءً على الأنشطة التي يتم تنفيذها. على سبيل المثال المناقشات والملاحظات وإكمال المهمة. {mlang}{mlang FR} Les élèves peuvent choisir d\'autres camarades de classe pour les suivre. Un flux d\'activités apparaîtra en fonction des activités réalisées. Par exemple, les discussions, les notes, l\'achèvement des devoirs. {mlang}{mlang ES} Los estudiantes pueden seguir a otros compañeros de clase. Aparecerá un muro de actividades en función de las actividades realizadas (p.ej. Debates, notas, finalización de tareas). {mlang}{mlang DE}Die Schüler können andere Klassenkameraden wählen, denen sie folgen möchten. Es erscheint ein Feed von Aktivitäten auf der Grundlage der durchgeführten Aktivitäten. Z.B. Diskussionen, Notizen, Aufgabenerledigung.{mlang}
						</p>
					</div>
					<div class="item_30">
						<h5>{mlang EN}Plagiarism   {mlang}{mlang FA} دزدی فکری یا پلجریزم {mlang}{mlang AR} السرقة الأدبية {mlang}{mlang FR} Plagiat {mlang}{mlang ES}Plagio  {mlang}{mlang DE}Plagiate{mlang}</h5>
						<p>{mlang EN}Calculation of the percentage of plagiarism of the assignments submitted by the students. Rules can be set to automatically reject assignment suspected of plagiarism.  {mlang}{mlang FA}محاسبۀ درصد دزدی فکری در تکالیف ارسالی توسط دانش آموزان. میتوان قوانینی تنظیم کرد که به کمک آنها تکالیف مشکوک به سرقت فکری  به طور خودکار رد شوند. {mlang}{mlang AR} حساب نسبة السرقة الأدبية من الواجبات المُقدَّمة من الطلاب. يمكن تعيين القواعد لرفض التعيين المشتبه به بالسرقة الأدبية تلقائيًا. {mlang}{mlang FR} Calcul du pourcentage de plagiat des travaux soumis par les élèves. Des règles peuvent être définies pour rejeter automatiquement les travaux soupçonnés de plagiat. {mlang}{mlang ES} Cálculo del porcentaje de plagio de las tareas enviadas por los estudiantes. Se pueden configurar reglas para rechazar automáticamente una tarea sospechosa de plagio.  {mlang}{mlang DE}Berechnung des prozentualen Anteils der Plagiate an den von den Schülern eingereichten Arbeiten. Es können Regeln festgelegt werden, um Aufgaben, bei denen der Verdacht des Plagiats besteht, automatisch abzulehnen.{mlang}
						</p>
					</div>
					<div class="item_30">
						<h5>{mlang EN}Automation   {mlang}{mlang FA} اتوماسیون  {mlang}{mlang AR}التشغيل التلقائي {mlang}{mlang FR} Automatisation {mlang}{mlang ES} Automatización {mlang}{mlang DE}Automatisierung{mlang}</h5>
						<p>{mlang EN}Allows the automation of reports and certain recurring tasks. E. g. Automatically enroll students who meet certain criteria in certain classes, based on rules.  {mlang}{mlang FA} گزارشها و ثبت نامها بصورت خودکار انجام خواهند شد . مثل ثبت نام دانش آموزان در بعضی کلاسهای خاص و بر اساس معیارهای خاص. {mlang}{mlang AR} يسمح بالتشغيل التلقائي للتقارير وبعض المهام المتكررة. على سبيل المثال التسجيل التلقائي للطلاب الذين يستوفون معايير معينة في فصول معينة و يكون بناءً على القواعد. {mlang}{mlang FR} Permet l\'automatisation des rapports et de certaines tâches récurrentes. Par exemple, permet d\'inscrire automatiquement les élèves qui répondent à certains critères dans certaines classes, en fonction des règles. {mlang}{mlang ES} Permite la automatización de informes y ciertas tareas recurrentes (p.ej. Inscribir automáticamente a estudiantes que cumplan determinados criterios en ciertas clases, basándose en reglas). {mlang}{mlang DE}Ermöglicht die Automatisierung von Berichten und bestimmten wiederkehrenden Aufgaben. Z. B. Automatische Einschreibung von Schülern in bestimmte Klassen, wenn sie bestimmte Kriterien erfüllen, basierend auf festgelegten Regeln.{mlang}</p>
					</div>
					<div class="item_30">
						<h5>{mlang EN}Add a guest lecture  {mlang}{mlang FA} امکان اضافه کردن سخنرانی مهمان {mlang}{mlang AR} إضافة محاضرة كزائر*غير مُستخدم للموقع {mlang}{mlang FR} Ajouter une conférence invité {mlang}{mlang ES} Añadir clase de invitado {mlang}{mlang DE}Eine Gastvorlesung hinzufügen{mlang}</h5>
						<p>{mlang EN}Allows the admin to add a guest teacher or lecture for a certain set of students.  {mlang}{mlang FA} به ادمین اجازه می دهد تا برای جمع خاصی از دانشجویان یک معلم مهمان یا سخنرانی خاصی را اضافه کند. {mlang}{mlang AR} يسمح للمسؤول بإضافة مدرس غير مُستخدم للموقع أو محاضرة لمجموعة معينة من الطلاب. {mlang}{mlang FR} Permet à l\'administrateur d\'ajouter un professeur invité ou une conférence pour un certain groupe d\'élèves. {mlang}{mlang ES} Permite al administrador añadir a un profesor invitado o clase para cierto conjunto de estudiantes.  {mlang}{mlang DE}Erlaubt dem Administrator, einen Gastlehrer oder eine Lehrveranstaltung für eine bestimmte Gruppe von Schülern hinzuzufügen.{mlang}</p>
					</div>
					<div class="item_30">
						<h5>{mlang EN} Follow-up / Pinned list {mlang}{mlang FA}لیست پیگیری / فهرست {mlang}{mlang AR}المتابعة / القائمة المُثبَّتة {mlang}{mlang FR}Suivi / Liste associee {mlang}{mlang ES} Seguimiento / Lista de seguimiento  {mlang}{mlang DE}Follow-up / Angeheftete Liste{mlang}</h5>
						<p>{mlang EN}Allows teachers to mark certain students in to follow up and take additional care.  {mlang}{mlang FA} به معلمان اجازه می دهد تا دانش آموزان خاصی را برای پیگیری و مراقبت بیشتر در نظر بگیرند. {mlang}{mlang AR} يسمح للمعلمين بتحديد طلاب معينين للمتابعة والعناية الإضافية. {mlang}{mlang FR}Permet aux enseignants de noter certains élèves afin d\'assurer un suivi et une attention particulière. {mlang}{mlang ES} Permite a los profesores marcar a ciertos estudiantes para hacerles un seguimiento y estar especialmente atentos a ellos. {mlang}{mlang DE}Ermöglicht es den Lehrern, bestimmte Schüler für die Nachbereitung und zusätzliche Betreuung zu markieren.{mlang}</p>
					</div>

					<div class="item_30">
						<h5>{mlang EN}Student Tags  {mlang}{mlang FA}برچسبها یا تگهای ی دانش آموزی {mlang}{mlang AR}شعار للطلاب {mlang}{mlang FR}Tags pour les élèves {mlang}{mlang ES} Etiquetas de estudiantes {mlang}{mlang DE}Schüler-Tags{mlang}</h5>
						<p>{mlang EN}A smarter way of grouping students based on their skills, activities they perform in the class, grades and more customizable options.   {mlang}{mlang FA} راه هوشمندانه تری برای دسته بندی دانش آموزان بر طبق مهارتها، فعالیتهایی که توسط آنها  در کلاس انجام میگیرد، نمرات و گزینه های دیگر {mlang}{mlang AR} طريقة أكثر ذكاءً لتجميع الطلاب بناءً على مهاراتهم والأنشطة التي يؤدونها في الفصل والدرجات والمزيد من الخيارات القابلة للتعديل والتغيير.{mlang}{mlang FR} Une façon plus intelligente de regrouper les élèves en fonction de leurs compétences, des activités qu\'ils réalisent en classe, des notes et des options plus personnalisables.    {mlang}{mlang ES}Una forma más inteligente de agrupar a los estudiantes basándonos en sus habilidades, actividades que realizan en clase, niveles y más opciones personalizables.  {mlang}{mlang DE}Eine intelligentere Art der Gruppierung von Schülern auf der Grundlage ihrer Fertigkeiten, der Aktivitäten, die sie in der Klasse ausführen, der Noten und anpassbarer Optionen.   {mlang}</p>
					</div>
				</div>

				 
			</div>

        </div>
       
    </div>';
echo format_text($html, FORMAT_HTML, array('trusted' => true, 'noclean' => true, 'filter' => true));

echo $OUTPUT->footer();
