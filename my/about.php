<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');

redirect_if_major_upgrade_required();

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/about.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_pagetype('About Us');
$PAGE->blocks->add_region('content');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/css/style.css'));
$PAGE->requires->js(new moodle_url('js/jquery.min.js'));
$PAGE->requires->js(new moodle_url('js/custom.js'));

echo $OUTPUT->header();

$html = '<div class="wrapper">
        
        <div class="main">

            <div class="item about res">

                <div class="right_sec">
                    <img src="images/about.png" />
                </div>
                <div class="left_sec">
                    <h1>{mlang EN}About Us{mlang}{mlang FA}دربارۀ ما {mlang}{mlang AR}صفحة المعلومات الخاصة بنا {mlang}{mlang FR}À propos de nous{mlang}{mlang ES}Sobre nosotros{mlang}{mlang DE}Über uns{mlang}</h1>
                    <p>{mlang EN}All In School is a student-centric online learning platform that allows a seamless interaction between students and teachers, either private tutors or educational institutions.{mlang}{mlang FA} روش یادگیری مورد استفاده در All In School  یک پلتفرم آنلاین دانش آموز محور است که به دانش آموزان و معلمان، چه معلمان خصوصی  و چه سازمانهای آموزشی، امکان بهره گیری از یک تعامل بی نظیر را میدهد.{mlang}{mlang AR}   All In School  هو منصة تعليمية عبر الانترنت تتمحور حول الطالب وتسمح بالتفاعل السلس بين الطلاب والمعلمين، سواء كانوا مُعلمين خاصين أو مؤسسات تعليمية. {mlang}{mlang FR}All In School est une plateforme d\'apprentissage en ligne centrée sur l\'élève qui permet une interaction fluide entre les élèves et les enseignants,que ca soit avec de tuteurs privés ou avec des établissements d\'enseignement. {mlang}{mlang ES}All In School es una plataforma de aprendizaje online centrada en el estudiante que permite una interacción fluida entre los alumnos y los profesores, adecuada tanto para tutores privados como para instituciones.{mlang}{mlang DE}All In School ist eine schülerzentrierte Online-Lernplattform, die eine nahtlose Interaktion zwischen Schülern und Lehrern, entweder Privatlehrer oder Bildungseinrichtungen, ermöglicht.{mlang}</p>

                    <h4>{mlang EN}Main Features:{mlang}{mlang FA}ویژگیهای مهم:{mlang}{mlang AR}الخصائص الرئيسية:{mlang}{mlang FR}Caractéristiques principales :{mlang}{mlang ES}Características principales:{mlang}{mlang DE}Hauptmerkmale:{mlang}</h4>
                    <hr class="purple">
                    <ul class="features">
                        <li> {mlang EN}AI enabled platform with Emotyx that measures the student’s attention span{mlang}{mlang FA}پلتفرم هوش مصنوعی AI با بهره گیری از روش آنالیز ویدیویی Emotyx که دارای قابلیت اندازه گیری دامنۀ توجه شاگرد به موضوع است. {mlang}{mlang AR}ملاحظة: Emotyx عبارة عن مجموعة برمجيات ذكية تدعم تحليلات الفيديو تم إنشاؤها باستخدام رؤية الكمبيوتر وتقنيات الذكاء الاصطناعي لتمكين تحليلات الفيديو في الوقت الفعلي للمراقبة وتتبع العملاء وإنشاء رؤى تجارية قائمة على الانفعالات والديموغرافيا من تحليل الحشود ببراعة.{mlang}{mlang FR}Plateforme d\'intelligence artificielle utilisant Emotyx qui mesure la capacité d\'attention de l\'élève{mlang}{mlang ES}Plataforma que integra IA con Emotyx que mide la capacidad de atención del estudiante{mlang}{mlang DE}KI-fähige Plattform mit Emotyx, die die Aufmerksamkeitsspanne des Schülers misst{mlang}</li>

                        <li>{mlang EN}Blockchain-based academic certificates{mlang}{mlang FA}گواهینامه های علمی مبتنی بر  Blockchain{mlang}{mlang AR}ملاحظة: سلسلة الكتل أو بلوك تشين (بالإنجليزية: Blockchain)‏ هي قاعدة بيانات موزعة تمتاز بقدرتها على إدارة قائمة متزايدة باستمرار من السجلات المسماة كُتلا (blocks). تحتوي كل كتلة على الطابع الزمني ورابط إلى الكتلة السابقة. صُممت سلسلة الكتل بحيث يمكنها المحافظة على البيانات المخزنة بها والحيلولة دون تعديلها، أي أنه عندما تخزن معلومة ما في سلسلة الكتلة لا يمكن لاحقاً القيام بتعديل هذه المعلومة.{mlang}{mlang FR}Certificats académiques basés sur une blockchain{mlang}{mlang ES}Certificados académicos basados en Blockchain{mlang}{mlang DE}Blockchainbasierte akademische Zertifikate{mlang}</li>

                        <li> {mlang EN}Virtual live classroom integrated with the LMS platform{mlang}{mlang FA}کلاس زندۀ مجازی که با پلتفرم سیستم مدیریت آموزشی LMS تلفیق شده است. {mlang}{mlang AR}فصل دراسي افتراضي مُدمَج مع منصة نظام إدارة التعلم (LMS ){mlang}{mlang FR}Classe virtuelle en direct intégrée à la plateforme LMS{mlang}{mlang ES}Aula virtual en directo integrada con plataforma LMS{mlang}{mlang DE}Virtuelles Live-Klassenzimmer integriert mit der LMS-Plattform{mlang}</li>

                        <li> {mlang EN}Customizable workflow{mlang}{mlang FA}گردش کار قابل برنامه ریزی{mlang}{mlang AR}طريقة عمل قابلة للتعديل{mlang}{mlang FR}Flux de travail personnalisable{mlang}{mlang ES}Flujo de trabajo personalizable{mlang}{mlang DE}Anpassbarer Arbeitsablauf{mlang}</li>

                        <li> {mlang EN}Multiple stakeholder dashboards: student, teacher, school admin and parent {mlang}{mlang FA} بهره گیری از  داشبوردهای متعدد برای افراد ذینفع: دانش آموز ، معلم ، مدیر مدرسه و والدین{mlang}{mlang AR}لوحات إلكترونية يُمكن استخدامها من قِبل : الطالب والمعلم ومسؤول المدرسة والوالد{mlang}{mlang FR}Tableaux de bord des différents acteurs : élève, enseignant, administrateur de l\'école et parent{mlang}{mlang ES}Varios paneles para los distintos usuarios: estudiante, profesor, administrador de la escuela y padres{mlang}{mlang DE}Mehrere Stakeholder-Dashboards: Schüler, Lehrer, Schulverwaltung und Eltern{mlang}</li>

                        <li> {mlang EN}Built on open source platform{mlang}{mlang FA}ساخته شده بر اساس پلت فرم منبع باز{mlang}{mlang AR}مبني على منصة مفتوحة المصدر {mlang}{mlang FR}Construit sur une plateforme source ouverte{mlang}{mlang ES}Desarrollado en plataforma de código abierto{mlang}{mlang DE}Auf Open-Source-Plattform aufgebaut{mlang}</li>

                        <li> {mlang EN}Custom built, with microservices based architecture for easy integration{mlang}{mlang FA}ساخت اختصاصی، با معماری مبتنی بر مایکروسرویسها برای یکپارچه سازی آسان{mlang}{mlang AR}مُصمَم بشكل مميز، مع بنية قائمة على الخدمات المصغرة لسهولة التعامل معها{mlang}{mlang FR}Conçue sur mesure, avec une architecture basée sur les micro-services pour une intégration facile{mlang}{mlang ES}Desarrollado de forma personalizada, con arquitectura basada en microservicios para una fácil integración{mlang}{mlang DE}Benutzerdefiniert, mit auf Mikrodiensten basierender Architektur für einfache Integration{mlang}</li>

                        <li> {mlang EN}Social learning features{mlang}{mlang FA}خصوصیات یادگیری جمعی {mlang}{mlang AR}مميزات التَعلُّم الالكتروني{mlang}{mlang FR}Fonctionnalités d\'apprentissage social{mlang}{mlang ES}Características de aprendizaje social{mlang}{mlang DE}Social Learning Features{mlang}</li>

                        <li> {mlang EN}Continuous innovative product updates with a clear roadmap{mlang}{mlang FA}به روز رسانی مداوم و خلاقانۀ محصول با استفاده از یک نقشۀ واضح و روشن{mlang}{mlang AR}تحديثات المستمرة والمبتكرة للمنتج مع خطة عمل واضحة{mlang}{mlang FR}Continuite de mises a jour  innovante du produit  avec une feuille de route claire {mlang}{mlang ES}Actualizaciones de producto continuas e innovadoras con una hoja de ruta definida{mlang}{mlang DE}Kontinuierliche innovative Produktaktualisierungen mit einem klaren Fahrplan{mlang}</li>

                        <li> {mlang EN}Ad hoc reporting{mlang}{mlang FA}گزارش موقت{mlang}{mlang AR}إعداد التقارير المُتخصِّصة{mlang}{mlang FR}Rapports ad hoc{mlang}{mlang ES}Informes ad hoc{mlang}{mlang DE}Ad-hoc-Berichterstattung{mlang}</li>

                        <li> {mlang EN}Flexible deployment options{mlang}{mlang FA}گزینه های انعطاف پذیر مدیریت{mlang}{mlang AR}خيارت مَرِنة للنشر{mlang}{mlang FR}Des options de déploiement flexibles{mlang}{mlang ES}Opciones de implementación flexibles{mlang}{mlang DE}Flexible Bereitstellungsoptionen{mlang}</li>
                    </ul>
                </div>
            </div>

        </div>
       
    </div>';
echo format_text($html, FORMAT_HTML, array('trusted' => true, 'noclean' => true, 'filter' => true));

echo $OUTPUT->footer();
