
   ![alt text <>](https://school.allinschool.com/pluginfile.php/1/theme_edumy/headerlogo1/1606123961/logowhite.png "")


#  B2BAllinschool

## Architecture

   ![alt text <>](https://www.aosabook.org/images/moodle/university.png "")

## Prerequisite  
 - Ubuntu 18.04 LTS  
 - PHP 7.1
 - MySQL 5.7
 - Nginx
  
  
## Deployment  

 - Install PHP 7.1
	```
	sudo apt install php7.1-fpm php7.1-common php7.1-mbstring php7.1-xmlrpc php7.1-soap php7.1-gd php7.1-xml php7.1-intl php7.1-mysql php7.1-cli php7.1-mcrypt php7.1-ldap php7.1-zip php7.1-curl
	```
 - Install MySQL Client
	```
	sudo apt install mysql-client
	```
 - Install Nginx web server
   ```
   sudo apt install nginx
   ``` 
 - Clone the project  
    ```
   cd /var/www/html

   git clone REPO_URL
   ```  
- Modify the directory owner and permissions
   ```
   sudo chown -R www-data:www-data /var/www/html/moodle/
   
   sudo chmod -R 755 /var/www/html/moodle/
   ```
- Create moodle data directory and modify permissions
   ```
   sudo mkdir /var/moodledata

   sudo chown -R www-data /var/moodledata

   sudo chmod -R 0770 /var/moodledata
   ```
- Add Nginx configs  
  
   ```
   cp ~/b2ballinschool/server-config/nginx-vhost.conf /etc/nginx/sites-available/domain.com.conf
   ```  
  
   ```
   ln -s /etc/nginx/sites-available/domain.com.conf /etc/nginx/sites-enabled/domain.com.conf
   ```  
  
   ``` 
   sudo service nginx restart
   ```  
  
## Updating Configs

- Update ```dbhost``` , ```dbname``` , ```dbuser```, ```dbpass```, ```wwwroot```, ```dataroot``` variables in  ```config.php```

- Update ```root``` with code path (/var/www/html/moodle) and ```server_name``` with domain names before copying to ```/etc/nginx/sites-available/```

  
 
