<?php

// This prerequisite is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'resource', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    mod_resource
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['clicktodownload'] = 'Click {$a} link to download the prerequisite.';
$string['clicktoopen2'] = 'Click {$a} link to view the prerequisite.';
$string['configdisplayoptions'] = 'Select all options that should be available, existing settings are not modified. Hold CTRL key to select multiple fields.';
$string['configframesize'] = 'When a web page or an uploaded prerequisite is displayed within a frame, this value is the height (in pixels) of the top frame (which contains the navigation).';
$string['configparametersettings'] = 'This sets the default value for the Parameter settings pane in the form when adding some new resources. After the first time, this becomes an individual user preference.';
$string['configpopup'] = 'When adding a new resource which is able to be shown in a popup window, should this option be enabled by default?';
$string['configpopupdirectories'] = 'Should popup windows show directory links by default?';
$string['configpopupheight'] = 'What height should be the default height for new popup windows?';
$string['configpopuplocation'] = 'Should popup windows show the location bar by default?';
$string['configpopupmenubar'] = 'Should popup windows show the menu bar by default?';
$string['configpopupresizable'] = 'Should popup windows be resizable by default?';
$string['configpopupscrollbars'] = 'Should popup windows be scrollable by default?';
$string['configpopupstatus'] = 'Should popup windows show the status bar by default?';
$string['configpopuptoolbar'] = 'Should popup windows show the tool bar by default?';
$string['configpopupwidth'] = 'What width should be the default width for new popup windows?';
$string['contentheader'] = 'Content';
$string['displayoptions'] = 'Available display options';
$string['displayselect'] = 'Display';
$string['displayselect_help'] = 'This setting, together with the prerequisite type and whether the browser allows embedding, determines how the prerequisite is displayed. Options may include:

* Automatic - The best display option for the prerequisite type is selected automatically
* Embed - The prerequisite is displayed within the page below the navigation bar together with the prerequisite description and any blocks
* Force download - The user is prompted to download the prerequisite
* Open - Only the prerequisite is displayed in the browser window
* In pop-up - The prerequisite is displayed in a new browser window without menus or an address bar
* In frame - The prerequisite is displayed within a frame below the navigation bar and prerequisite description
* New window - The prerequisite is displayed in a new browser window with menus and an address bar';
$string['displayselect_link'] = 'mod/prerequisite/mod';
$string['displayselectexplain'] = 'Choose display type, unfortunately not all types are suitable for all prerequisites.';
$string['dnduploadresource'] = 'Create prerequisite resource';
$string['encryptedcode'] = 'Encrypted code';
$string['prerequisitenotfound'] = 'Prerequisite not found, sorry.';
$string['filterprerequisites'] = 'Use filters on prerequisite content';
$string['filterprerequisitesexplain'] = 'Select type of prerequisite content filtering, please note this may cause problems for some Flash and Java applets. Please make sure that all text prerequisites are in UTF-8 encoding.';
$string['filtername'] = 'Resource names auto-linking';
$string['forcedownload'] = 'Force download';
$string['framesize'] = 'Frame height';
$string['indicator:cognitivedepth'] = 'Prerequisite cognitive';
$string['indicator:cognitivedepth_help'] = 'This indicator is based on the cognitive depth reached by the student in a Prerequisite resource.';
$string['indicator:cognitivedepthdef'] = 'Prerequisite cognitive';
$string['indicator:cognitivedepthdef_help'] = 'The participant has reached this percentage of the cognitive engagement offered by the Prerequisite resources during this analysis interval (Levels = No view, View)';
$string['indicator:cognitivedepthdef_link'] = 'Learning_analytics_indicators#Cognitive_depth';
$string['indicator:socialbreadth'] = 'Prerequisite social';
$string['indicator:socialbreadth_help'] = 'This indicator is based on the social breadth reached by the student in a Prerequisite resource.';
$string['indicator:socialbreadthdef'] = 'Prerequisite social';
$string['indicator:socialbreadthdef_help'] = 'The participant has reached this percentage of the social engagement offered by the Prerequisite resources during this analysis interval (Levels = No participation, Participant alone)';
$string['indicator:socialbreadthdef_link'] = 'Learning_analytics_indicators#Social_breadth';
$string['legacyprerequisites'] = 'Migration of old course prerequisite';
$string['legacyprerequisitesactive'] = 'Active';
$string['legacyprerequisitesdone'] = 'Finished';
$string['modifieddate'] = 'Modified {$a}';
$string['modulename'] = 'Prerequisite';
$string['modulename_help'] = 'The prerequisite module enables a teacher to provide a prerequisite as a course resource. Where possible, the prerequisite will be displayed within the course interface; otherwise students will be prompted to download it. The prerequisite may include supporting prerequisites, for example an HTML page may have embedded images.

Note that students need to have the appropriate software on their computers in order to open the prerequisite.

A prerequisite may be used

* To share presentations given in class
* To include a mini website as a course resource
* To provide draft prerequisites of software programs so students can edit and submit them for assessment';
$string['modulename_link'] = 'mod/resource/view';
$string['modulenameplural'] = 'Prerequisites';
$string['notmigrated'] = 'This legacy resource type ({$a}) was not yet migrated, sorry.';
$string['optionsheader'] = 'Display options';
$string['page-mod-resource-x'] = 'Any prerequisite module page';
$string['pluginadministration'] = 'Prerequisite module administration';
$string['pluginname'] = 'Prerequisite';
$string['popupheight'] = 'Pop-up height (in pixels)';
$string['popupheightexplain'] = 'Specifies default height of popup windows.';
$string['popupresource'] = 'This resource should appear in a popup window.';
$string['popupresourcelink'] = 'If it didn\'t, click here: {$a}';
$string['popupwidth'] = 'Pop-up width (in pixels)';
$string['popupwidthexplain'] = 'Specifies default width of popup windows.';
$string['printintro'] = 'Display resource description';
$string['printintroexplain'] = 'Display resource description below content? Some display types may not display description even if enabled.';
$string['privacy:metadata'] = 'The Prerequisite resource plugin does not store any personal data.';
$string['resource:addinstance'] = 'Add a new resource';
$string['resourcecontent'] = 'Prerequisites and subfolders';
$string['resourcedetails_sizetype'] = '{$a->size} {$a->type}';
$string['resourcedetails_sizedate'] = '{$a->size} {$a->date}';
$string['resourcedetails_typedate'] = '{$a->type} {$a->date}';
$string['resourcedetails_sizetypedate'] = '{$a->size} {$a->type} {$a->date}';
$string['resource:exportresource'] = 'Export resource';
$string['resource:view'] = 'View resource';
$string['search:activity'] = 'Prerequisite';
$string['selectmainprerequisite'] = 'Please select the main prerequisite by clicking the icon next to prerequisite name.';
$string['showdate'] = 'Show upload/modified date';
$string['showdate_desc'] = 'Display upload/modified date on course page?';
$string['showdate_help'] = 'Displays the upload/modified date beside links to the prerequisite.

If there are multiple prerequisites in this resource, the start prerequisite upload/modified date is displayed.';
$string['showsize'] = 'Show size';
$string['showsize_help'] = 'Displays the prerequisite size, such as \'3.1 MB\', beside links to the prerequisite.

If there are multiple prerequisites in this resource, the total size of all prerequisites is displayed.';
$string['showsize_desc'] = 'Display prerequisite size on course page?';
$string['showtype'] = 'Show type';
$string['showtype_desc'] = 'Display prerequisite type (e.g. \'Word document\') on course page?';
$string['showtype_help'] = 'Displays the type of the prerequisite, such as \'Word document\', beside links to the prerequisite.

If there are multiple prerequisites in this resource, the start prerequisite type is displayed.

If the prerequisite type is not known to the system, it will not display.';
$string['uploadeddate'] = 'Uploaded {$a}';
