<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'course', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   core_course
 * @copyright 2018 Adrian Greeve <adriangreeve.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['aria:coursecategory'] = 'Class category';
$string['aria:courseimage'] = 'Class image';
$string['aria:courseshortname'] = 'Class short name';
$string['aria:coursename'] = 'Class name';
$string['aria:favourite'] = 'Class is starred';
$string['coursealreadyfinished'] = 'Class already finished';
$string['coursenotyetstarted'] = 'The class has not yet started';
$string['coursenotyetfinished'] = 'The class has not yet finished';
$string['coursetoolong'] = 'The class is too long';
$string['customfield_islocked'] = 'Locked';
$string['customfield_islocked_help'] = 'If the field is locked, only users with the capability to change locked custom fields (by default users with the default role of manager only) will be able to change it in the class settings.';
$string['customfield_notvisible'] = 'Nobody';
$string['customfield_visibility'] = 'Visible to';
$string['customfield_visibility_help'] = 'This setting determines who can view the custom field name and value in the list of classes or in the available custom field filter of the Dashboard.';
$string['customfield_visibletoall'] = 'Everyone';
$string['customfield_visibletoteachers'] = 'Teachers';
$string['customfieldsettings'] = 'Common class custom fields settings';
$string['errorendbeforestart'] = 'The end date ({$a}) is before the class start date.';
$string['favourite'] = 'Highly Rated Class';
$string['gradetopassnotset'] = 'This class does not have a grade to pass set. It may be set in the grade item of the class (Gradebook setup).';
$string['nocourseactivity'] = 'Not enough class activity between the start and the end of the class';
$string['nocourseendtime'] = 'The class does not have an end time';
$string['nocoursesections'] = 'No class sections';
$string['nocoursestudents'] = 'No students';
$string['noaccesssincestartinfomessage'] = 'Hi {$a->userfirstname},

</br><br/>A number of students in {$a->coursename} have never accessed the class.';
$string['norecentaccessesinfomessage'] = 'Hi {$a->userfirstname},

</br><br/>A number of students in {$a->coursename} have not accessed the class recently.';
$string['noteachinginfomessage'] = 'Hi {$a->userfirstname},

</br><br/>Classes with start dates in the next week have been identified as having no teacher or student enrolments.';
$string['privacy:perpage'] = 'The number of classes to show per page.';
$string['privacy:completionpath'] = 'Class completion';
$string['privacy:favouritespath'] = 'Class starred information';
$string['privacy:metadata:completionsummary'] = 'The class contains completion information about the user.';
$string['privacy:metadata:favouritessummary'] = 'The class contains information relating to the class being starred by the user.';
$string['studentsatriskincourse'] = 'Students at risk in {$a} class';
$string['studentsatriskinfomessage'] = 'Hi {$a->userfirstname},

</br><br/>Students in the {$a->coursename} class have been identified as being at risk.';
$string['target:coursecompletion'] = 'Students at risk of not meeting the class completion conditions';
$string['target:coursecompletion_help'] = 'This target describes whether the student is considered at risk of not meeting the class completion conditions.';
$string['target:coursecompetencies'] = 'Students at risk of not achieving the competencies assigned to a class';
$string['target:coursecompetencies_help'] = 'This target describes whether a student is at risk of not achieving the competencies assigned to a class. This target considers that all competencies assigned to the class must be achieved by the end of the class.';
$string['target:coursedropout'] = 'Students at risk of dropping out';
$string['target:coursedropout_help'] = 'This target describes whether the student is considered at risk of dropping out.';
$string['target:coursegradetopass'] = 'Students at risk of not achieving the minimum grade to pass the class';
$string['target:coursegradetopass_help'] = 'This target describes whether the student is at risk of not achieving the minimum grade to pass the class.';
$string['target:noaccesssincecoursestart'] = 'Students who have not accessed the class yet';
$string['target:noaccesssincecoursestart_help'] = 'This target describes students who never accessed a class they are enrolled in.';
$string['target:noaccesssincecoursestartinfo'] = 'The following students are enrolled in a class which has started, but they have never accessed the class.';
$string['target:norecentaccesses'] = 'Students who have not accessed the class recently';
$string['target:norecentaccesses_help'] = 'This target identifies students who have not accessed a class they are enrolled in within the set analysis interval (by default the past month).';
$string['target:norecentaccessesinfo'] = 'The following students have not accessed a class they are enrolled in within the set analysis interval (by default the past month).';
$string['target:noteachingactivity'] = 'Classes at risk of not starting';
$string['target:noteachingactivity_help'] = 'This target describes whether classes due to start in the coming week will have teaching activity.';
$string['target:noteachingactivityinfo'] = 'The following classes due to start in the upcoming days are at risk of not starting because they don\'t have teachers or students enrolled.';
$string['targetlabelstudentcompletionno'] = 'Student who is likely to meet the class completion conditions';
$string['targetlabelstudentcompletionyes'] = 'Student at risk of not meeting the class completion conditions';
$string['targetlabelstudentcompetenciesno'] = 'Student who is likely to achieve the competencies assigned to a class';
$string['targetlabelstudentcompetenciesyes'] = 'Student at risk of not achieving the competencies assigned to a class';
$string['targetlabelstudentdropoutyes'] = 'Student at risk of dropping out';
$string['targetlabelstudentdropoutno'] = 'Not at risk';
$string['targetlabelstudentgradetopassno'] = 'Student who is likely to meet the minimum grade to pass the class.';
$string['targetlabelstudentgradetopassyes'] = 'Student at risk of not meeting the minimum grade to pass the class.';
$string['targetlabelteachingyes'] = 'Users with teaching capabilities who have access to the class';
$string['targetlabelteachingno'] = 'Classes at risk of not starting';
