<?php
/*
@ccnRef: @block_cocoon/block.php
*/

defined('MOODLE_INTERNAL') || die();

// if (!($this->config)) {
//   if(!($this->content)){
//     $this->content = new \stdClass();
//   }
//     $this->content->text = '<h5 class="mb30">'.$this->title.'</h5>';
//     return $this->content->text;
// }

// print_object($this);
$ccnBlockType = $this->instance->blockname;

$ccnCollectionFullwidthTop =  array(
  "cocoon_about_1",
  "cocoon_about_2",
  "cocoon_accordion",
  "cocoon_action_panels",
  "cocoon_boxes",
  "cocoon_faqs",
  "cocoon_features",
  "cocoon_parallax",
  "cocoon_parallax_apps",
  "cocoon_parallax_counters",
  "cocoon_parallax_features",
  // "cocoon_parallax_white",
  "cocoon_pills",
  "cocoon_price_tables",
  "cocoon_services",
  "cocoon_simple_counters",
  "cocoon_hero_1",
  "cocoon_hero_2",
  "cocoon_hero_3",
  "cocoon_slider_1",
  "cocoon_slider_1_v",
  "cocoon_slider_2",
  "cocoon_slider_3",
  "cocoon_slider_4",
  "cocoon_slider_5",
  "cocoon_slider_6",
  "cocoon_steps",
  "cocoon_subscribe",
  "cocoon_tablets",
  "cocoon_course_categories",
  "cocoon_course_categories_3",
  "cocoon_course_grid_3",
  "cocoon_users_slider_round",
  "cocoon_tstmnls",
);

$ccnCollectionAboveContent =  array(
  "cocoon_course_overview",
);

$ccnCollectionBelowContent =  array(
  "cocoon_course_rating",
);

$ccnCollection = array_merge($ccnCollectionFullwidthTop, $ccnCollectionAboveContent, $ccnCollectionBelowContent);

if (empty($this->config)) {
  if(in_array($ccnBlockType, $ccnCollectionFullwidthTop)) {
    $this->instance->defaultregion = 'fullwidth-top';
    $this->instance->region = 'fullwidth-top';
    $DB->update_record('block_instances', $this->instance);
  }
  if(in_array($ccnBlockType, $ccnCollectionAboveContent)) {
    $this->instance->defaultregion = 'above-content';
    $this->instance->region = 'above-content';
    $DB->update_record('block_instances', $this->instance);
  }
  if(in_array($ccnBlockType, $ccnCollectionBelowContent)) {
    $this->instance->defaultregion = 'below-content';
    $this->instance->region = 'below-content';
    $DB->update_record('block_instances', $this->instance);
  }
  /* Begin Legacy */
  if(!in_array($ccnBlockType, $ccnCollection)) {
    if(!($this->content)){
       $this->content = new \stdClass();
    }
    $this->content->text = '<h5 class="mb30">'.$this->title.'</h5>';
    return $this->content->text;
  }
  /* End Legacy */
}
