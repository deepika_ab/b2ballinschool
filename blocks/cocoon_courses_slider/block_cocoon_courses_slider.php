<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

//require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->dirroot. '/course/renderer.php');
require_once($CFG->dirroot. '/theme/edumy/ccn/course_handler/ccn_course_handler.php');

class block_cocoon_courses_slider extends block_base {

    public function init() {
        $this->title = get_string('pluginname', 'block_cocoon_courses_slider');
    }

    public function get_content() {
        global $CFG, $DB, $COURSE, $USER, $PAGE;

        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        // if (isset($PAGE->theme->settings->course_enrolment_payment) && ($PAGE->theme->settings->course_enrolment_payment == 1)) {
        //   $paymentForced = false;
        // } else {
        //   $paymentForced = true;
        // }

        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';
        $this->content->text = '';
        if(!empty($this->config->title)){$this->content->title = $this->config->title;}
        if(!empty($this->config->subtitle)){$this->content->subtitle = $this->config->subtitle;}
        if(!empty($this->config->hover_text)){$this->content->hover_text = $this->config->hover_text;}
        if(!empty($this->config->hover_accent)){$this->content->hover_accent = $this->config->hover_accent;}
        if(!empty($this->config->description)){$this->content->description = $this->config->description;}
        if(!empty($this->config->course_image)){$this->content->course_image = $this->config->course_image;}
        if(!empty($this->config->price)){$this->content->price = $this->config->price;}
        if(!empty($this->config->enrol_btn)){$this->content->enrol_btn = $this->config->enrol_btn;}
        if(!empty($this->config->enrol_btn_text)){$this->content->enrol_btn_text = $this->config->enrol_btn_text;}

        if(
          isset($this->content->description) &&
          $this->content->description != '0'
        ) {
          $ccnBlockShowDesc = 1;
        } else {
          $ccnBlockShowDesc = 0;
        }

        if(
          isset($this->content->course_image) &&
          $this->content->course_image == '1'
        ){
          $ccnBlockShowImg = 1;
        } else {
          $ccnBlockShowImg = 0;
        }
        if(
          isset($this->content->enrol_btn) &&
          isset($this->content->enrol_btn_text) &&
          $this->content->enrol_btn == '1'
        ){
          $ccnBlockShowEnrolBtn = 1;
        } else {
          $ccnBlockShowEnrolBtn = 0;
        }
        if(
          isset($this->content->price) &&
          $this->content->price == '1'
        ) {
          $ccnBlockShowPrice = 1;
        } else {
          $ccnBlockShowPrice = 0;
        }

        if(
          $PAGE->theme->settings->coursecat_enrolments != 1 ||
          $PAGE->theme->settings->coursecat_announcements != 1 ||
          isset($this->content->price) ||
          isset($this->content->enrol_btn_text) &&
          // ccnBreak
          ($this->content->price == '1' || $this->content->enrol_btn == '1')
        ){
          $ccnBlockShowBottomBar = 1;
          $topCoursesClass = 'ccnWithFoot';
        } else {
          $ccnBlockShowBottomBar = 0;
          $topCoursesClass = '';
        }

        if(!empty($this->content->description) && $this->content->description == '7'){
          $maxlength = 500;
        } elseif(!empty($this->content->description) && $this->content->description == '6'){
          $maxlength = 350;
        } elseif(!empty($this->content->description) && $this->content->description == '5'){
          $maxlength = 200;
        } elseif(!empty($this->content->description) && $this->content->description == '4'){
          $maxlength = 150;
        } elseif(!empty($this->content->description) && $this->content->description == '3'){
          $maxlength = 100;
        } elseif(!empty($this->content->description) && $this->content->description == '2'){
          $maxlength = 50;
        } else {
          $maxlength = null;
        }

        if(!empty($this->config->courses)){
          $coursesArr = $this->config->courses;
          $courses = new stdClass();
          foreach ($coursesArr as $key => $course) {
            $courseObj = new stdClass();
            $courseObj->id = $course;
            $courses->$course = $courseObj;
          }
        }

        if (!empty($this->config->style) && $this->config->style == 1) {  //background
          $this->content->text .= '
            <section class="popular-courses bgc-thm2">
          		<div class="container-fluid style2">
          			<div class="row">
          				<div class="col-lg-6 offset-lg-3">
          					<div class="main-title text-center">';
                    if(!empty($this->content->title)){
                      $this->content->text .='<h3 class="mt0 color-white">'.format_text($this->content->title, FORMAT_HTML, array('filter' => true)).'</h3>';
                    }
                    if(!empty($this->content->subtitle)){
                      $this->content->text .='<p class="color-white">'.format_text($this->content->subtitle, FORMAT_HTML, array('filter' => true)).'</p>';
                    }
                    $this->content->text .='
          					</div>
          				</div>
          			</div>
          			<div class="row">
          				<div class="col-lg-12">
          					<div class="popular_course_slider">';
                    if(empty($this->config->courses)){
                      $courses = self::get_featured_courses();
                    }
                    $chelper = new coursecat_helper();
                    foreach ($courses as $course) {
                      if ($DB->record_exists('course', array('id' => $course->id))) {

                        $ccnCourseHandler = new ccnCourseHandler();
                        $ccnCourse = $ccnCourseHandler->ccnGetCourseDetails($course->id);

                        $ccnCourseDescription = $ccnCourseHandler->ccnGetCourseDescription($course->id, $maxlength);


                      $this->content->text .='
                        <div class="item">
            							<div class="top_courses home2 mb0 '.$topCoursesClass.'">';
                          if($ccnBlockShowImg){
                            $this->content->text .='
            								<div class="thumb">
            									'.$ccnCourse->ccnRender->coverImage.'
            									<a class="overlay" href="'. $ccnCourse->url .'">';
                              if($this->content->hover_accent){
                               $this->content->text .='<div class="tag">'.format_text($this->content->hover_accent, FORMAT_HTML, array('filter' => true)).'</div>';
                             }
                             if($this->content->hover_text){
                              $this->content->text .='  <div class="tc_preview_course" href="'. $ccnCourse->url .'">'.format_text($this->content->hover_text, FORMAT_HTML, array('filter' => true)).'</div>';
                             }
            									$this->content->text .='
                              </a>
            								</div>';
                          }
                          $this->content->text .='
            								<div class="details">
            									<div class="tc_content">';
                              $this->content->text .= $ccnCourse->ccnRender->updatedDate;
                              $this->content->text .=  $ccnCourse->ccnRender->title;
                              if($ccnBlockShowDesc){
                                $this->content->text .='<p>'.$ccnCourseDescription.'</p>';
                              }
                              $this->content->text .= $ccnCourse->ccnRender->starRating;

                              $this->content->text .='
            									</div>
                              </div>';
                              if($ccnBlockShowBottomBar == 1){
                              $this->content->text .='
            									<div class="tc_footer">
                              <ul class="tc_meta float-left">'.$ccnCourse->ccnRender->enrolmentIcon . $ccnCourse->ccnRender->announcementsIcon.'</ul>';

                                if($ccnBlockShowEnrolBtn){
                                  $this->content->text .='<a href="'.$ccnCourse->enrolmentLink.'" class="tc_enrol_btn float-right">'.$this->content->enrol_btn_text.'</a>';
                                }
                                if($ccnBlockShowPrice){
                                  $this->content->text .= '<div class="tc_price float-right">'.$ccnCourse->price.'</div>';
                                }
                                $this->content->text .='
            									</div>';
                            }
                            $this->content->text .='
            							</div>
            						</div>';
                      }
                      }
                      $this->content->text .= '
                               </div>
    				                 </div>
    			                 </div>
                         </div>
    	                 </section>';

                    } else {
                      $this->content->text .= '



        <section class="features-course pb20">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 offset-lg-3">
        <div class="main-title text-center">';
        if(!empty($this->content->title)){
          $this->content->text .='<h3 class="mb0 mt0">'. format_text($this->content->title, FORMAT_HTML, array('filter' => true)) .'</h3>';
        }
        if(!empty($this->content->subtitle)){
          $this->content->text .='<p>'.format_text($this->content->subtitle, FORMAT_HTML, array('filter' => true)).'</p>';
        }
       $this->content->text .='
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="shop_product_slider">';



        if(empty($this->config->courses)){
          $courses = self::get_featured_courses();
        }
        $chelper = new coursecat_helper();
        foreach ($courses as $course) {
          if ($DB->record_exists('course', array('id' => $course->id))) {

            $ccnCourseHandler = new ccnCourseHandler();
            $ccnCourse = $ccnCourseHandler->ccnGetCourseDetails($course->id);

            $ccnCourseDescription = $ccnCourseHandler->ccnGetCourseDescription($course->id, $maxlength);

            $this->content->text .='
                       <div class="item">
           							<div class="top_courses '.$topCoursesClass.'">';
                         if($ccnBlockShowImg){
                           $this->content->text .='
           								<div class="thumb">
           									'.$ccnCourse->ccnRender->coverImage.'
           									<a class="overlay" href="'. $ccnCourse->url .'">';
                             if(!empty($this->content->hover_accent)){
           									  $this->content->text .='	<div class="tag">'.format_text($this->content->hover_accent, FORMAT_HTML, array('filter' => true)).'</div>';
                             }
                             if(!empty($this->content->hover_text)){
           										$this->content->text .='<div class="tc_preview_course">'.format_text($this->content->hover_text, FORMAT_HTML, array('filter' => true)).'</div>';
                             }
           									$this->content->text .='</a>
           								</div>';
                         }
                         $this->content->text .='
           								<div class="details">
           									<div class="tc_content">';
                               $this->content->text .= $ccnCourse->ccnRender->updatedDate;
                           $this->content->text .=  $ccnCourse->ccnRender->title;
                             if($ccnBlockShowDesc){
                               $this->content->text .='<p>'.$ccnCourseDescription.'</p>';
                             }
                             $this->content->text .= $ccnCourse->ccnRender->starRating;
                             $this->content->text .='
           									</div>
                             </div>';
                             if($ccnBlockShowBottomBar == 1){
                               $this->content->text .='
           									<div class="tc_footer">
           										<ul class="tc_meta float-left">'.$ccnCourse->ccnRender->enrolmentIcon.$ccnCourse->ccnRender->announcementsIcon.'</ul>';
                               if($ccnBlockShowEnrolBtn){
                                 $this->content->text .='<a href="'.$ccnCourse->enrolmentLink.'" class="tc_enrol_btn float-right">'.$this->content->enrol_btn_text.'</a>';
                               }
                               if($ccnBlockShowPrice){
                                 $this->content->text .= '<div class="tc_price float-right">'.$ccnCourse->price.'</div>';
                               }
                               $this->content->text .='
           									</div>';
                           }
                          $this->content->text .='
           							</div>
           						</div>
                       ';
          }
        }
$this->content->text .= '         </div>
      </div>
    </div>
  </div>
</section>';
  }

        return $this->content;
    }

    function applicable_formats() {
        return array(
          'all' => true,
          'my' => false,
        );
    }
    public function html_attributes() {
      global $CFG;
      $attributes = parent::html_attributes();
      include($CFG->dirroot . '/theme/edumy/ccn/block_handler/attributes.php');
      return $attributes;
    }


    public function instance_allow_multiple() {
          return false;
    }

    public function has_config() {
        return false;
    }

    public function cron() {
        return true;
    }

    public static function get_featured_courses() {
        global $DB;

        $sql = 'SELECT c.id, c.shortname, c.fullname, fc.sortorder
                  FROM {block_cocoon_courses_slider} fc
                  JOIN {course} c
                    ON (c.id = fc.courseid)
              ORDER BY sortorder';
        return $DB->get_records_sql($sql);
    }

    public static function delete_featuredcourse($courseid) {
        global $DB;
        return $DB->delete_records('block_cocoon_courses_slider', array('courseid' => $courseid));
    }
}
