<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_mycourses', language 'en'
 *
 * @package   block_mycourses
 * @copyright Daniel Neis <danielneis@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['archive'] = 'Older class history';
$string['archivecutofflabel'] = 'Number of days for archive';
$string['archivecutoffdesc'] = 'This sets the number of days after which a class is moved to the archive listing.';
$string['archivetitle'] = 'Archive classes';
$string['aria:courseprogress'] = 'Class progress:';
$string['availableheader'] = 'Available classes';
$string['complete'] = 'complete';
$string['completedheader'] = 'Completed classes';
$string['downloadcert'] = 'Download class certificate';
$string['finalscore'] = 'Final score - {$a}%';
$string['headerconfig'] = 'Configuration for the my classes block';
$string['headerdesc'] = 'This holds the configuration for the my classes block and controls how it is displayed';
$string['inprogressheader'] = 'Classes in progress';
$string['myarchiveheader'] = 'Archived class list';
$string['mycoursesheader'] = 'My Class Library';
$string['mycourses:addinstance'] = 'Add a myclasses block';
$string['mycourses:myaddinstance'] = 'Add a myclasses block to my moodle';
$string['noavailable'] = 'No classes are available';
$string['nocerttodownload'] = 'No certificate available';
$string['nocompleted'] = 'No completed classes';
$string['nocourses'] = '(No classes)';
$string['noinprogress'] = 'No classes in progress';
$string['notstartedheader'] = 'Classes not started';
$string['pluginname'] = 'My Classes';
$string['privacy:metadata'] = 'The My Classes block only shows data stored in other locations.';
$string['showsummarylabel'] = 'Show class summary';
$string['showsummarydesc'] = 'Show the class summary in the listing';
$string['startcourse'] = 'Click \'ok\' to start this class';
$string['title'] = 'My classes';
