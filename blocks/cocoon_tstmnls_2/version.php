<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2020062329;
$plugin->requires  = 2017051504;
$plugin->component = 'block_cocoon_tstmnls_2';
