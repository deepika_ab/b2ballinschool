<?php
require_once($CFG->dirroot. '/theme/edumy/ccn/user_handler/ccn_user_handler.php');
class block_cocoon_users_slider_round extends block_base {

    /**
     * Initializes class member variables.
     */
    public function init() {
        // Needed by Moodle to differentiate between blocks.
        $this->title = get_string('pluginname', 'block_cocoon_users_slider_round');
    }

    function specialization() {
        global $CFG, $DB;
        include($CFG->dirroot . '/theme/edumy/ccn/block_handler/specialization.php');
        if (empty($this->config)) {

          $ccnUserHandler = new ccnUserHandler();
          $ccnUsers = $ccnUserHandler->ccnGetExampleUsersIds(8);


          // $this->config->items = '4';
          // $this->config->title1 = 'Create Account';
          // $this->config->title2 = 'Create Account';
          // $this->config->title3 = 'Create Account';
          // $this->config->title4 = 'Create Account';
          // $this->config->body1 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          // $this->config->body2 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          // $this->config->body3 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          // $this->config->body4 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          // $this->config->icon1 = 'flaticon-student-3';
          // $this->config->icon2 = 'flaticon-trophy';
          // $this->config->icon3 = 'flaticon-student-1';
          // $this->config->icon4 = 'flaticon-rating';

          $this->config->title = 'Top Rating Instructors';
          $this->config->subtitle = 'Cum doctus civibus efficiantur in imperdiet deterruisset.';
          $this->config->users = $ccnUsers;


          $this->config->color_bg = 'rgb(255,255,255)';
          $this->config->color_title = '#0a0a0a';
          $this->config->color_subtitle = '#6f7074';
          $this->config->color_owl_dots = '#debf52';




          // $this->config->slide_btn_url1 = '#';
          // $this->config->image = $CFG->wwwroot.'/theme/edumy/images/home/1.jpg';
          // // $this->config->file_slide2 = $CFG->wwwroot.'/theme/edumy/home/1.jpg';
          // // $this->config->file_slide3 = $CFG->wwwroot.'/theme/edumy/home/1.jpg';
          // $this->config->slide_title2 = 'Self Education Resources and Infos';
          // $this->config->slide_subtitle2 = 'Technology is brining a massive wave of evolution on learning things on different ways.';
          // $this->config->slide_btn_text2 = 'Ready to Get Started?';
          // $this->config->slide_btn_url2 = '#';
          // $this->config->slide_title3 = 'Self Education Resources and Infos';
          // $this->config->slide_subtitle3 = 'Technology is brining a massive wave of evolution on learning things on different ways.';
          // $this->config->slide_btn_text3 = 'Ready to Get Started?';
          // $this->config->slide_btn_url3 = '#';
          // $this->config->prev_1 = 'PR';
          // $this->config->prev_2 = 'EV';
          // $this->config->next_1 = 'NE';
          // $this->config->next_2 = 'XT';
          // $this->config->arrow_style = 0;
        }

    }

//     private function get_users($ids)
//     {
//         global $DB, $OUTPUT, $PAGE;
//
//         $usernames = [];
//         if(empty($ids)) return [];
//
//         list($uids, $params) = $DB->get_in_or_equal($ids);
//         $rs = $DB->get_recordset_select('user', 'id ' . $uids, $params, '', 'id,firstname,lastname,email,picture,imagealt,lastnamephonetic,firstnamephonetic,middlename,alternatename,department,lastaccess');
//
//         foreach ($rs as $record)
//         {
//             $record->fullname = fullname($record);
//             $record->department = $record->department;
//             $record->identity = $record->email;
//             $record->hasidentity = true;
//
//             $url = new moodle_url('/user/profile.php', array('id' => $record->id));
//
//             // Get the user picture data - messaging has always shown these to the user.
//             $userpicture = new \user_picture($record);
//
//             $userpicture->size = 300; // Size f2.
//             $record->profileimageurlsmall = $userpicture->get_url($PAGE)->out(false);
//
//             $usernames[$record->id] = '
//             <div class="item">
//             <a href="'.$url.'">
//   <div class="instructor_col">
//     <div class="thumb">
//       <img class="img-fluid img-rounded-circle" src="'.$record->profileimageurlsmall.'" alt="'. $record->fullname.'">
//     </div>
//     <div class="details">
//       <ul>
//       <li class="list-inline-item"><i class="fa fa-star"></i></li>
//       <li class="list-inline-item"><i class="fa fa-star"></i></li>
//       <li class="list-inline-item"><i class="fa fa-star"></i></li>
//       <li class="list-inline-item"><i class="fa fa-star"></i></li>
//       <li class="list-inline-item"><i class="fa fa-star"></i></li>
//       </ul>
//       <h4>'. $record->fullname.'</h4>
//       <p>'. $record->department .'</p>
//     </div>
//   </div>
//   </a>
// </div>
//                     ';
//         }
//         $rs->close();
//
//         return $usernames;
//     }


    /**
     * Returns the block contents.
     *
     * @return stdClass The block contents.
     */
    public function get_content() {


        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

        if(!empty($this->config->title)){$this->content->title = $this->config->title;} else {$this->content->title = '';}
        if(!empty($this->config->subtitle)){$this->content->subtitle = $this->config->subtitle;} else {$this->content->subtitle = '';}
        if(!empty($this->config->users)){$this->content->users = $this->config->users;} else {$this->content->users = '';}
        if(!empty($this->config->color_bg)){$this->content->color_bg = $this->config->color_bg;} else {$this->content->color_bg = 'rgb(255,255,255)';}
        if(!empty($this->config->color_title)){$this->content->color_title = $this->config->color_title;} else {$this->content->color_title = '#0a0a0a';}
        if(!empty($this->config->color_subtitle)){$this->content->color_subtitle = $this->config->color_subtitle;} else {$this->content->color_subtitle = '#6f7074';}
        if(!empty($this->config->color_owl_dots)){$this->content->color_owl_dots = $this->config->color_owl_dots;} else {$this->content->color_owl_dots = '#debf52';}




        $this->content->text .='
        <section class="our-team home8 pb10 pt30" data-ccn-c="color_bg" data-ccn-co="bg" style="background-color: '.$this->content->color_bg.';">
         <div class="container">
           <div class="row">
             <div class="col-lg-6 offset-lg-3">
               <div class="main-title text-center">
                 <h3 class="mt0" data-ccn="title" data-ccn-c="color_title" data-ccn-co="content" style="color: '.$this->content->color_title.';">'.$this->content->title.'</h3>
                 <p data-ccn="subtitle" data-ccn-c="color_subtitle" data-ccn-co="content" style="color: '.$this->content->color_subtitle.';">'.$this->content->subtitle.'</p>
               </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-12">
               <div class="instructor_slider_home3 home8">';
                  if(!empty($this->content->users)){
                    foreach($this->content->users as $key => $ccnUserId){
                      $ccnUserHandler = new ccnUserHandler();
                      $ccnUser = $ccnUserHandler->ccnGetUserDetails($ccnUserId);
                      // print_object($ccnUser);
                      $this->content->text .='
                      <div class="item">
                        <a href="'.$ccnUser->profileUrl.'">
                          <div class="instructor_col">
                            <div class="thumb">
                              <img class="img-fluid img-rounded-circle" src="'.$ccnUser->rawAvatar.'" alt="">
                            </div>
                            <div class="details">
                              <ul>
                              <li class="list-inline-item"><i class="fa fa-star"></i></li>
                              <li class="list-inline-item"><i class="fa fa-star"></i></li>
                              <li class="list-inline-item"><i class="fa fa-star"></i></li>
                              <li class="list-inline-item"><i class="fa fa-star"></i></li>
                              <li class="list-inline-item"><i class="fa fa-star"></i></li>
                              </ul>
                              <h4>'. $ccnUser->fullname.'</h4>
                              <p>'. $ccnUser->ccnRender->profileCount .'</p>
                            </div>
                          </div>
                          </a>
                        </div>';
                    }
                  }
                  $this->content->text .='
                </div>
         			</div>
         		</div>
         	</div>
         </section>';





        // print_object($this->content->users);

 //        if (!empty($this->config->text)) {
 //            $this->content->text = $this->config->text;
 //        } else
 //        {
 //            $userconfig = null;
 //            if(!empty($this->config->users))
 //            {
 //                $userconfig = $this->config->users;
 //            }
 //            $users = $this->get_users($userconfig);
 //            if(empty($users))
 //            {
 //                $this->content->text = get_string('empty', 'block_cocoon_users_slider_round');
 //            }
 //            else
 //            {
 //                $list = [];
 //                foreach ($users as $id => $username)
 //                {
 //                    $link = $username;
 //                    $list[] = $link;
 //                }
 //                $this->content->text = '
 //                <section class="our-team home8 pb10 pt30">
 //  <div class="container">
 //    <div class="row">
 //      <div class="col-lg-6 offset-lg-3">
 //        <div class="main-title text-center">
 //          <h3 class="mt0">'.$this->content->title.'</h3>
 //          <p>'.$this->content->subtitle.'</p>
 //        </div>
 //      </div>
 //    </div>
 //    <div class="row">
 //      <div class="col-lg-12">
 //        <div class="instructor_slider_home3 home8">
 // '. implode('', $list) .' </div>
	// 			</div>
	// 		</div>
	// 	</div>
	// </section>
 //                ';
 //            }
 //        }

        return $this->content;
    }



    /**
     * Allow multiple instances in a single course?
     *
     * @return bool True if multiple instances are allowed, false otherwise.
     */
    public function instance_allow_multiple() {
        return true;
    }

    /**
     * Enables global configuration of the block in settings.php.
     *
     * @return bool True if the global configuration is enabled.
     */
    function has_config() {
        return true;
    }

    /**
     * Sets the applicable formats for the block.
     *
     * @return string[] Array of pages and permissions.
     */
     function applicable_formats() {
         return array(
           'all' => true,
           'my' => false,
         );
     }

     public function html_attributes() {
       global $CFG;
       $attributes = parent::html_attributes();
       include($CFG->dirroot . '/theme/edumy/ccn/block_handler/attributes.php');
       return $attributes;
     }

}
