<?php

// include_once($CFG->dirroot . '/course/lib.php');
// require_once($CFG->libdir . '/coursecatlib.php');
// require_once($CFG->dirroot. '/course/renderer.php');

class block_cocoon_boxes extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_cocoon_boxes');
    }

    function has_config() {
        return true;
    }

    function applicable_formats() {
        return array(
          'all' => true,
          'my' => false,
          // 'admin' => false,
        );
    }

    function specialization() {
        global $CFG, $DB;
        include($CFG->dirroot . '/theme/edumy/ccn/block_handler/specialization.php');
        if (empty($this->config)) {
          $this->config->items = '4';
          $this->config->title1 = 'Create Account';
          $this->config->title2 = 'Create Account';
          $this->config->title3 = 'Create Account';
          $this->config->title4 = 'Create Account';
          $this->config->body1 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          $this->config->body2 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          $this->config->body3 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          $this->config->body4 = 'Sed cursus turpis vitae tortor donec eaque ipsa quaeab illo.';
          $this->config->color1 = 'rgb(245, 245, 246)';
          $this->config->color2 = 'rgb(245, 245, 246)';
          $this->config->color3 = 'rgb(245, 245, 246)';
          $this->config->color4 = 'rgb(245, 245, 246)';
          $this->config->color_hover1 = 'rgb(62, 68, 72)';
          $this->config->color_hover2 = 'rgb(62, 68, 72)';
          $this->config->color_hover3 = 'rgb(62, 68, 72)';
          $this->config->color_hover4 = 'rgb(62, 68, 72)';
          $this->config->color_icon1 = 'rgb(205, 190, 156)';
          $this->config->color_icon2 = 'rgb(205, 190, 156)';
          $this->config->color_icon3 = 'rgb(205, 190, 156)';
          $this->config->color_icon4 = 'rgb(205, 190, 156)';

          $this->config->color_icon_hover1 = 'rgb(205, 190, 156)';
          $this->config->color_icon_hover2 = 'rgb(205, 190, 156)';
          $this->config->color_icon_hover3 = 'rgb(205, 190, 156)';
          $this->config->color_icon_hover4 = 'rgb(205, 190, 156)';


          $this->config->color_title1 = 'rgb(62, 68, 72)';
          $this->config->color_title2 = 'rgb(62, 68, 72)';
          $this->config->color_title3 = 'rgb(62, 68, 72)';
          $this->config->color_title4 = 'rgb(62, 68, 72)';

          $this->config->color_title_hover1 = 'rgb(255,255,255)';
          $this->config->color_title_hover2 = 'rgb(255,255,255)';
          $this->config->color_title_hover3 = 'rgb(255,255,255)';
          $this->config->color_title_hover4 = 'rgb(255,255,255)';

          $this->config->color_body_hover1 = 'rgb(255,255,255)';
          $this->config->color_body_hover2 = 'rgb(255,255,255)';
          $this->config->color_body_hover3 = 'rgb(255,255,255)';
          $this->config->color_body_hover4 = 'rgb(255,255,255)';
          $this->config->color_body1 = 'rgb(62, 68, 72)';
          $this->config->color_body2 = 'rgb(62, 68, 72)';
          $this->config->color_body3 = 'rgb(62, 68, 72)';
          $this->config->color_body4 = 'rgb(62, 68, 72)';
          $this->config->icon1 = 'flaticon-student-3';
          $this->config->icon2 = 'flaticon-trophy';
          $this->config->icon3 = 'flaticon-student-1';
          $this->config->icon4 = 'flaticon-rating';

          $this->config->title = 'What We Do';
          $this->config->subtitle = 'Cum doctus civibus efficiantur in imperdiet deterruisset.';
          // $this->config->slide_btn_url1 = '#';
          // $this->config->image = $CFG->wwwroot.'/theme/edumy/images/home/1.jpg';
          // // $this->config->file_slide2 = $CFG->wwwroot.'/theme/edumy/home/1.jpg';
          // // $this->config->file_slide3 = $CFG->wwwroot.'/theme/edumy/home/1.jpg';
          // $this->config->slide_title2 = 'Self Education Resources and Infos';
          // $this->config->slide_subtitle2 = 'Technology is brining a massive wave of evolution on learning things on different ways.';
          // $this->config->slide_btn_text2 = 'Ready to Get Started?';
          // $this->config->slide_btn_url2 = '#';
          // $this->config->slide_title3 = 'Self Education Resources and Infos';
          // $this->config->slide_subtitle3 = 'Technology is brining a massive wave of evolution on learning things on different ways.';
          // $this->config->slide_btn_text3 = 'Ready to Get Started?';
          // $this->config->slide_btn_url3 = '#';
          // $this->config->prev_1 = 'PR';
          // $this->config->prev_2 = 'EV';
          // $this->config->next_1 = 'NE';
          // $this->config->next_2 = 'XT';
          // $this->config->arrow_style = 0;
        }

    }


    function instance_allow_config() {
        return true;
    }

    function get_content() {
        global $CFG, $USER, $DB, $OUTPUT;

        if($this->content !== NULL) {
            return $this->content;
        }

        if (!empty($this->config) && is_object($this->config)) {
            $data = $this->config;
            $data->items = is_numeric($data->items) ? (int)$data->items : 4;
        } else {
            $data = new stdClass();
            $data->items = 4;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        if(!empty($this->config->title)){$this->content->title = $this->config->title;}
        if(!empty($this->config->subtitle)){$this->content->subtitle = $this->config->subtitle;}
        // if(!empty($this->config->body)){$this->content->body = $this->config->body;}
        // if(!empty($this->config->button_link)){$this->content->button_link = $this->config->button_link;}else{$this->content->button_link = "#our-courses";}
        // if(!empty($this->config->button_text)){$this->content->button_text = $this->config->button_text;}else{$this->content->button_text = "View All Courses";}
        // if (!empty($this->config->style) && $this->config->style == 1) {
        //   $this->content->style = '<a href="#our-courses">
				//     	<div class="mouse_scroll">
			  //       		<div class="icon"><span class="flaticon-download-arrow"></span></div>
				//     	</div>
				//     </a>';
        // } else {
        //   $this->content->style = '';
        // }


        // if ($data->items > 0) {
        $this->content->text = '

        <section id="our-courses" class="our-courses pt90 pt650-992">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					'.$this->content->style.'
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
      <div class="col-lg-6 offset-lg-3">
        <div class="main-title text-center">';
         $this->content->text .=' <h3 data-ccn="title" class="mt0">'. format_text($this->content->title, FORMAT_HTML, array('filter' => true)) .'</h3>';
          $this->content->text .='<p data-ccn="subtitle">'.format_text($this->content->subtitle, FORMAT_HTML, array('filter' => true)).'</p>';
        $this->content->text .='
        </div>
      </div>

			</div>
			<div class="row">
      <div class="col-xs-12">
      <div class="row">';

          $col_class = "";
          if ($data->items == 1) {
            $col_class = "col-sm-12 col-lg-12";
          } else if ($data->items == 2) {
            $col_class = "col-sm-6 col-lg-6";
          } else if ($data->items == 3) {
            $col_class = "col-sm-6 col-lg-4";
          } else {
            $col_class = "col-sm-6 col-lg-3";
          }
          if ($data->items > 0) {
            for ($i = 1; $i <= $data->items; $i++) {
              // if(++$i > $data->items) break;
              $icon = 'icon' . $i;
              $icon_color = 'color_icon' . $i;
              $icon_color_hover = 'color_icon_hover' . $i;
              $title_color = 'color_title' . $i;
              $title_color_hover = 'color_title_hover' . $i;
              $body_color = 'color_body' . $i;
              $body_color_hover = 'color_body_hover' . $i;
              $color = 'color' . $i;
              $color_hover = 'color_hover' . $i;
              $title = 'title' . $i;
              $body = 'body' . $i;

              $this->content->text .='
              <style type="text/css">
              .icon_hvr_img_box.ccn-box[data-ccn-c=color'.$i.']:hover {
                background-color:'.$data->$color_hover.'!important;
              }
              .icon_hvr_img_box.ccn-box[data-ccn-c=color'.$i.']:hover h5 {
                color:'.$data->$title_color_hover.'!important;
              }
              .icon_hvr_img_box.ccn-box[data-ccn-c=color'.$i.']:hover p {
                color:'.$data->$body_color_hover.'!important;
              }
              .icon_hvr_img_box.ccn-box[data-ccn-c=color'.$i.']:hover .icon span {
                color:'.$data->$icon_color_hover.'!important;
              }
              </style>


              <div class="col-sm-6 col-lg-3">
					<div class="icon_hvr_img_box ccn-box sbbg1" data-ccn-c="color'.$i.'" data-ccn-co="bg" style="background-color: '.$data->$color.';">
						<div class="overlay">
							<div class="ccn_icon_2 icon"><span data-ccn="icon'.$i.'" data-ccn-c="color_icon'.$i.'" data-ccn-co="content" class="'.$data->$icon.'" style="color:'.$data->$icon_color.'"></span></div>
							<div class="details">
								<h5 data-ccn="title'.$i.'" data-ccn-c="color_title'.$i.'" data-ccn-co="content" style="color:'.$data->$title_color.'">'.$data->$title.'</h5>
								<p data-ccn="body'.$i.'" data-ccn-c="color_body'.$i.'" data-ccn-co="content" style="color:'.$data->$body_color.'">'.$data->$body.'</p>
							</div>
						</div>
					</div>
				</div>';


              // $courseid = $course->id;
              // $chelper = new coursecat_helper();
              // print_object($category);

            }
          // if ($topcategory->is_uservisible() && ($categories = $topcategory->get_children())) { // Check we have categories.
          //     if (count($categories) > 1 || (count($categories) == 1 && $DB->count_records('course') > 200)) {     // Just print top level category links
          //         $i = 0;
          //         foreach ($categories as $category) {
          //           if(++$i > $data->items) break;
          //
          //           $outputimage = '';
          //           //ccnComm: Fetching the image manually added to the coursecat description via the Atto editor.
          //           $chelper = new coursecat_helper();
          //           $description = $chelper->get_category_formatted_description($category);
          //           if ($description) {
          //             $dom = new DOMDocument();
          //             $dom->loadHTML($description);
          //             $xpath = new DOMXPath($dom);
          //             $src = $xpath->evaluate("string(//img/@src)");
          //           }
          //           if ($src && $description){
          //             $outputimage = $src;
          //           } else {
          //             $children_courses = $category->get_courses();
          //             if($children_courses >= 1){
          //               $countNoOfCourses = '<p>'.get_string('number_of_courses', 'theme_edumy', count($children_courses)).'</p>';
          //             } else {
          //               $countNoOfCourses = '';
          //             }
          //             foreach($children_courses as $child_course) {
          //               if ($child_course === reset($children_courses)) {
          //                 foreach ($child_course->get_course_overviewfiles() as $file) {
          //                   if ($file->is_valid_image()) {
          //                     $imagepath = '/' . $file->get_contextid() . '/' . $file->get_component() . '/' . $file->get_filearea() . $file->get_filepath() . $file->get_filename();
          //                     $imageurl = file_encode_url($CFG->wwwroot . '/pluginfile.php', $imagepath, false);
          //                     $outputimage  =  $imageurl;
          //                     // Use the first image found.
          //                     break;
          //                   }
          //                 }
          //               }
          //             }
          //           }
          //             $categoryname = $category->get_formatted_name();
          //             $linkcss = $category->visible ? "" : " class=\"dimmed\" ";
          //             $this->content->text .= '
          //             <div class="'.$col_class.'">
          //               <a href="'.$CFG->wwwroot .'/course/index.php?categoryid='.$category->id.'" class="img_hvr_box" style="background-image: url('. $outputimage .');">
          //                 <div class="overlay">
          //                   <div class="details">
          //                     <h5>'. $categoryname .'</h5>';
          //                     if($this->content->body != '1'){
          //                       $this->content->text .='<p>'. format_string($category->description, $striplinks = true,$options = null).'</p>';
          //                     } else {
          //                       $this->content->text .= $countNoOfCourses;
          //                     }
          //                     $this->content->text .='
          //                   </div>
          //                 </div>
          //               </a>
          //             </div>';
          //
          //         }
          //       }
          //     }
              // else {                          // Just print course names of single category
              //   $category = array_shift($categories);
              //   $courses = $category->get_courses();
              //
              //   if ($courses) {
              //       foreach ($courses as $course) {
              //           $coursecontext = context_course::instance($course->id);
              //           $linkcss = $course->visible ? "" : " class=\"dimmed\" ";
              //
              //           $this->content->text .= '
              //           <li><a href="'.$CFG->wwwroot .'/course/view.php?id='.$course->id.'">'. $course->get_formatted_name() .' <span class="float-right">()</span></a></li>
              //
              //           ';
              //       }
              //     }
              //   }
              }
              $this->content->text .='
              </div>
            </div>
          </div>
        </div>
      </section>';
// }

        return $this->content;
    }

    // function get_remote_courses() {
    //     global $CFG, $USER, $OUTPUT;
    //
    //     if (!is_enabled_auth('mnet')) {
    //         // no need to query anything remote related
    //         return;
    //     }
    //
    //     $icon = $OUTPUT->pix_icon('i/mnethost', get_string('host', 'mnet'));
    //
    //     // shortcut - the rest is only for logged in users!
    //     if (!isloggedin() || isguestuser()) {
    //         return false;
    //     }
    //
    //     if ($courses = get_my_remotecourses()) {
    //         $this->content->items[] = get_string('remotecourses','mnet');
    //         $this->content->icons[] = '';
    //         foreach ($courses as $course) {
    //             $this->content->items[]="<a title=\"" . format_string($course->shortname, true) . "\" ".
    //                 "href=\"{$CFG->wwwroot}/auth/mnet/jump.php?hostid={$course->hostid}&amp;wantsurl=/course/view.php?id={$course->remoteid}\">"
    //                 .$icon. format_string(get_course_display_name_for_list($course)) . "</a>";
    //         }
    //         // if we listed courses, we are done
    //         return true;
    //     }
    //
    //     if ($hosts = get_my_remotehosts()) {
    //         $this->content->items[] = get_string('remotehosts', 'mnet');
    //         $this->content->icons[] = '';
    //         foreach($USER->mnet_foreign_host_array as $somehost) {
    //             $this->content->items[] = $somehost['count'].get_string('courseson','mnet').'<a title="'.$somehost['name'].'" href="'.$somehost['url'].'">'.$icon.$somehost['name'].'</a>';
    //         }
    //         // if we listed hosts, done
    //         return true;
    //     }
    //
    //     return false;
    // }

    /**
     * Returns the role that best describes the course list block.
     *
     * @return string
     */
    public function get_aria_role() {
        return 'navigation';
    }
    public function html_attributes() {
      global $CFG;
      $attributes = parent::html_attributes();
      include($CFG->dirroot . '/theme/edumy/ccn/block_handler/attributes.php');
      return $attributes;
    }
}
